package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_move_line;
import cn.ibizlab.odoo.core.client.service.Istock_move_lineClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_move_lineImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_move_lineOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_move_lineOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_move_line] 服务对象接口
 */
@Service
public class stock_move_lineClientServiceImpl implements Istock_move_lineClientService {
    @Autowired
    private  Istock_move_lineOdooClient  stock_move_lineOdooClient;

    public Istock_move_line createModel() {		
		return new stock_move_lineImpl();
	}


        public void removeBatch(List<Istock_move_line> stock_move_lines){
            
        }
        
        public void remove(Istock_move_line stock_move_line){
this.stock_move_lineOdooClient.remove(stock_move_line) ;
        }
        
        public void create(Istock_move_line stock_move_line){
this.stock_move_lineOdooClient.create(stock_move_line) ;
        }
        
        public void createBatch(List<Istock_move_line> stock_move_lines){
            
        }
        
        public Page<Istock_move_line> search(SearchContext context){
            return this.stock_move_lineOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Istock_move_line> stock_move_lines){
            
        }
        
        public void get(Istock_move_line stock_move_line){
            this.stock_move_lineOdooClient.get(stock_move_line) ;
        }
        
        public void update(Istock_move_line stock_move_line){
this.stock_move_lineOdooClient.update(stock_move_line) ;
        }
        
        public Page<Istock_move_line> select(SearchContext context){
            return null ;
        }
        

}

