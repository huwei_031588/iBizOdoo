package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_warehouse;
import cn.ibizlab.odoo.core.client.service.Istock_warehouseClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_warehouseImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_warehouseOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_warehouseOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_warehouse] 服务对象接口
 */
@Service
public class stock_warehouseClientServiceImpl implements Istock_warehouseClientService {
    @Autowired
    private  Istock_warehouseOdooClient  stock_warehouseOdooClient;

    public Istock_warehouse createModel() {		
		return new stock_warehouseImpl();
	}


        public void updateBatch(List<Istock_warehouse> stock_warehouses){
            
        }
        
        public void remove(Istock_warehouse stock_warehouse){
this.stock_warehouseOdooClient.remove(stock_warehouse) ;
        }
        
        public void update(Istock_warehouse stock_warehouse){
this.stock_warehouseOdooClient.update(stock_warehouse) ;
        }
        
        public Page<Istock_warehouse> search(SearchContext context){
            return this.stock_warehouseOdooClient.search(context) ;
        }
        
        public void create(Istock_warehouse stock_warehouse){
this.stock_warehouseOdooClient.create(stock_warehouse) ;
        }
        
        public void createBatch(List<Istock_warehouse> stock_warehouses){
            
        }
        
        public void removeBatch(List<Istock_warehouse> stock_warehouses){
            
        }
        
        public void get(Istock_warehouse stock_warehouse){
            this.stock_warehouseOdooClient.get(stock_warehouse) ;
        }
        
        public Page<Istock_warehouse> select(SearchContext context){
            return null ;
        }
        

}

