package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_scrap;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_scrap] 服务对象客户端接口
 */
public interface Istock_scrapOdooClient {
    
        public void updateBatch(Istock_scrap stock_scrap);

        public void createBatch(Istock_scrap stock_scrap);

        public void get(Istock_scrap stock_scrap);

        public Page<Istock_scrap> search(SearchContext context);

        public void remove(Istock_scrap stock_scrap);

        public void create(Istock_scrap stock_scrap);

        public void removeBatch(Istock_scrap stock_scrap);

        public void update(Istock_scrap stock_scrap);

        public List<Istock_scrap> select();


}