package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_warehouse_orderpoint;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_warehouse_orderpoint] 服务对象客户端接口
 */
public interface Istock_warehouse_orderpointOdooClient {
    
        public void createBatch(Istock_warehouse_orderpoint stock_warehouse_orderpoint);

        public void remove(Istock_warehouse_orderpoint stock_warehouse_orderpoint);

        public void updateBatch(Istock_warehouse_orderpoint stock_warehouse_orderpoint);

        public void get(Istock_warehouse_orderpoint stock_warehouse_orderpoint);

        public Page<Istock_warehouse_orderpoint> search(SearchContext context);

        public void removeBatch(Istock_warehouse_orderpoint stock_warehouse_orderpoint);

        public void update(Istock_warehouse_orderpoint stock_warehouse_orderpoint);

        public void create(Istock_warehouse_orderpoint stock_warehouse_orderpoint);

        public List<Istock_warehouse_orderpoint> select();


}