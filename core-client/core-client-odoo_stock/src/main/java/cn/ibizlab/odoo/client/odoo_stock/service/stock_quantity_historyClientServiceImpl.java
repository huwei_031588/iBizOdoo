package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_quantity_history;
import cn.ibizlab.odoo.core.client.service.Istock_quantity_historyClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_quantity_historyImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_quantity_historyOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_quantity_historyOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_quantity_history] 服务对象接口
 */
@Service
public class stock_quantity_historyClientServiceImpl implements Istock_quantity_historyClientService {
    @Autowired
    private  Istock_quantity_historyOdooClient  stock_quantity_historyOdooClient;

    public Istock_quantity_history createModel() {		
		return new stock_quantity_historyImpl();
	}


        public void update(Istock_quantity_history stock_quantity_history){
this.stock_quantity_historyOdooClient.update(stock_quantity_history) ;
        }
        
        public void create(Istock_quantity_history stock_quantity_history){
this.stock_quantity_historyOdooClient.create(stock_quantity_history) ;
        }
        
        public void updateBatch(List<Istock_quantity_history> stock_quantity_histories){
            
        }
        
        public void removeBatch(List<Istock_quantity_history> stock_quantity_histories){
            
        }
        
        public Page<Istock_quantity_history> search(SearchContext context){
            return this.stock_quantity_historyOdooClient.search(context) ;
        }
        
        public void get(Istock_quantity_history stock_quantity_history){
            this.stock_quantity_historyOdooClient.get(stock_quantity_history) ;
        }
        
        public void createBatch(List<Istock_quantity_history> stock_quantity_histories){
            
        }
        
        public void remove(Istock_quantity_history stock_quantity_history){
this.stock_quantity_historyOdooClient.remove(stock_quantity_history) ;
        }
        
        public Page<Istock_quantity_history> select(SearchContext context){
            return null ;
        }
        

}

