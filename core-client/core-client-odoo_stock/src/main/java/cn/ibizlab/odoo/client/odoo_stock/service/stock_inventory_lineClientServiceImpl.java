package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_inventory_line;
import cn.ibizlab.odoo.core.client.service.Istock_inventory_lineClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_inventory_lineImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_inventory_lineOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_inventory_lineOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_inventory_line] 服务对象接口
 */
@Service
public class stock_inventory_lineClientServiceImpl implements Istock_inventory_lineClientService {
    @Autowired
    private  Istock_inventory_lineOdooClient  stock_inventory_lineOdooClient;

    public Istock_inventory_line createModel() {		
		return new stock_inventory_lineImpl();
	}


        public void updateBatch(List<Istock_inventory_line> stock_inventory_lines){
            
        }
        
        public void removeBatch(List<Istock_inventory_line> stock_inventory_lines){
            
        }
        
        public void createBatch(List<Istock_inventory_line> stock_inventory_lines){
            
        }
        
        public void update(Istock_inventory_line stock_inventory_line){
this.stock_inventory_lineOdooClient.update(stock_inventory_line) ;
        }
        
        public void create(Istock_inventory_line stock_inventory_line){
this.stock_inventory_lineOdooClient.create(stock_inventory_line) ;
        }
        
        public void remove(Istock_inventory_line stock_inventory_line){
this.stock_inventory_lineOdooClient.remove(stock_inventory_line) ;
        }
        
        public Page<Istock_inventory_line> search(SearchContext context){
            return this.stock_inventory_lineOdooClient.search(context) ;
        }
        
        public void get(Istock_inventory_line stock_inventory_line){
            this.stock_inventory_lineOdooClient.get(stock_inventory_line) ;
        }
        
        public Page<Istock_inventory_line> select(SearchContext context){
            return null ;
        }
        

}

