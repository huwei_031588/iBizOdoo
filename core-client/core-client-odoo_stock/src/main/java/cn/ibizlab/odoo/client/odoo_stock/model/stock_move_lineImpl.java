package cn.ibizlab.odoo.client.odoo_stock.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Istock_move_line;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[stock_move_line] 对象
 */
public class stock_move_lineImpl implements Istock_move_line,Serializable{

    /**
     * 消耗行
     */
    public String consume_line_ids;

    @JsonIgnore
    public boolean consume_line_idsDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 移动完成
     */
    public String done_move;

    @JsonIgnore
    public boolean done_moveDirtyFlag;
    
    /**
     * 完成工单
     */
    public String done_wo;

    @JsonIgnore
    public boolean done_woDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 初始需求是否可以编辑
     */
    public String is_initial_demand_editable;

    @JsonIgnore
    public boolean is_initial_demand_editableDirtyFlag;
    
    /**
     * 是锁定
     */
    public String is_locked;

    @JsonIgnore
    public boolean is_lockedDirtyFlag;
    
    /**
     * 至
     */
    public Integer location_dest_id;

    @JsonIgnore
    public boolean location_dest_idDirtyFlag;
    
    /**
     * 至
     */
    public String location_dest_id_text;

    @JsonIgnore
    public boolean location_dest_id_textDirtyFlag;
    
    /**
     * 从
     */
    public Integer location_id;

    @JsonIgnore
    public boolean location_idDirtyFlag;
    
    /**
     * 从
     */
    public String location_id_text;

    @JsonIgnore
    public boolean location_id_textDirtyFlag;
    
    /**
     * 批次可见
     */
    public String lots_visible;

    @JsonIgnore
    public boolean lots_visibleDirtyFlag;
    
    /**
     * 批次/序列号码
     */
    public Integer lot_id;

    @JsonIgnore
    public boolean lot_idDirtyFlag;
    
    /**
     * 批次/序列号码
     */
    public String lot_id_text;

    @JsonIgnore
    public boolean lot_id_textDirtyFlag;
    
    /**
     * 批次/序列号 名称
     */
    public String lot_name;

    @JsonIgnore
    public boolean lot_nameDirtyFlag;
    
    /**
     * 完工批次/序列号
     */
    public Integer lot_produced_id;

    @JsonIgnore
    public boolean lot_produced_idDirtyFlag;
    
    /**
     * 完工批次/序列号
     */
    public String lot_produced_id_text;

    @JsonIgnore
    public boolean lot_produced_id_textDirtyFlag;
    
    /**
     * 产成品数量
     */
    public Double lot_produced_qty;

    @JsonIgnore
    public boolean lot_produced_qtyDirtyFlag;
    
    /**
     * 库存移动
     */
    public Integer move_id;

    @JsonIgnore
    public boolean move_idDirtyFlag;
    
    /**
     * 库存移动
     */
    public String move_id_text;

    @JsonIgnore
    public boolean move_id_textDirtyFlag;
    
    /**
     * 所有者
     */
    public Integer owner_id;

    @JsonIgnore
    public boolean owner_idDirtyFlag;
    
    /**
     * 所有者
     */
    public String owner_id_text;

    @JsonIgnore
    public boolean owner_id_textDirtyFlag;
    
    /**
     * 源包裹
     */
    public Integer package_id;

    @JsonIgnore
    public boolean package_idDirtyFlag;
    
    /**
     * 源包裹
     */
    public String package_id_text;

    @JsonIgnore
    public boolean package_id_textDirtyFlag;
    
    /**
     * 包裹层级
     */
    public Integer package_level_id;

    @JsonIgnore
    public boolean package_level_idDirtyFlag;
    
    /**
     * 库存拣货
     */
    public Integer picking_id;

    @JsonIgnore
    public boolean picking_idDirtyFlag;
    
    /**
     * 库存拣货
     */
    public String picking_id_text;

    @JsonIgnore
    public boolean picking_id_textDirtyFlag;
    
    /**
     * 移动整个包裹
     */
    public String picking_type_entire_packs;

    @JsonIgnore
    public boolean picking_type_entire_packsDirtyFlag;
    
    /**
     * 创建新批次/序列号码
     */
    public String picking_type_use_create_lots;

    @JsonIgnore
    public boolean picking_type_use_create_lotsDirtyFlag;
    
    /**
     * 使用已有批次/序列号码
     */
    public String picking_type_use_existing_lots;

    @JsonIgnore
    public boolean picking_type_use_existing_lotsDirtyFlag;
    
    /**
     * 生产行
     */
    public String produce_line_ids;

    @JsonIgnore
    public boolean produce_line_idsDirtyFlag;
    
    /**
     * 生产单
     */
    public Integer production_id;

    @JsonIgnore
    public boolean production_idDirtyFlag;
    
    /**
     * 生产单
     */
    public String production_id_text;

    @JsonIgnore
    public boolean production_id_textDirtyFlag;
    
    /**
     * 产品
     */
    public Integer product_id;

    @JsonIgnore
    public boolean product_idDirtyFlag;
    
    /**
     * 产品
     */
    public String product_id_text;

    @JsonIgnore
    public boolean product_id_textDirtyFlag;
    
    /**
     * 实际预留数量
     */
    public Double product_qty;

    @JsonIgnore
    public boolean product_qtyDirtyFlag;
    
    /**
     * 单位
     */
    public Integer product_uom_id;

    @JsonIgnore
    public boolean product_uom_idDirtyFlag;
    
    /**
     * 单位
     */
    public String product_uom_id_text;

    @JsonIgnore
    public boolean product_uom_id_textDirtyFlag;
    
    /**
     * 已保留
     */
    public Double product_uom_qty;

    @JsonIgnore
    public boolean product_uom_qtyDirtyFlag;
    
    /**
     * 完成
     */
    public Double qty_done;

    @JsonIgnore
    public boolean qty_doneDirtyFlag;
    
    /**
     * 编号
     */
    public String reference;

    @JsonIgnore
    public boolean referenceDirtyFlag;
    
    /**
     * 目的地包裹
     */
    public Integer result_package_id;

    @JsonIgnore
    public boolean result_package_idDirtyFlag;
    
    /**
     * 目的地包裹
     */
    public String result_package_id_text;

    @JsonIgnore
    public boolean result_package_id_textDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 追踪
     */
    public String tracking;

    @JsonIgnore
    public boolean trackingDirtyFlag;
    
    /**
     * 工单
     */
    public Integer workorder_id;

    @JsonIgnore
    public boolean workorder_idDirtyFlag;
    
    /**
     * 工单
     */
    public String workorder_id_text;

    @JsonIgnore
    public boolean workorder_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [消耗行]
     */
    @JsonProperty("consume_line_ids")
    public String getConsume_line_ids(){
        return this.consume_line_ids ;
    }

    /**
     * 设置 [消耗行]
     */
    @JsonProperty("consume_line_ids")
    public void setConsume_line_ids(String  consume_line_ids){
        this.consume_line_ids = consume_line_ids ;
        this.consume_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消耗行]脏标记
     */
    @JsonIgnore
    public boolean getConsume_line_idsDirtyFlag(){
        return this.consume_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [移动完成]
     */
    @JsonProperty("done_move")
    public String getDone_move(){
        return this.done_move ;
    }

    /**
     * 设置 [移动完成]
     */
    @JsonProperty("done_move")
    public void setDone_move(String  done_move){
        this.done_move = done_move ;
        this.done_moveDirtyFlag = true ;
    }

     /**
     * 获取 [移动完成]脏标记
     */
    @JsonIgnore
    public boolean getDone_moveDirtyFlag(){
        return this.done_moveDirtyFlag ;
    }   

    /**
     * 获取 [完成工单]
     */
    @JsonProperty("done_wo")
    public String getDone_wo(){
        return this.done_wo ;
    }

    /**
     * 设置 [完成工单]
     */
    @JsonProperty("done_wo")
    public void setDone_wo(String  done_wo){
        this.done_wo = done_wo ;
        this.done_woDirtyFlag = true ;
    }

     /**
     * 获取 [完成工单]脏标记
     */
    @JsonIgnore
    public boolean getDone_woDirtyFlag(){
        return this.done_woDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [初始需求是否可以编辑]
     */
    @JsonProperty("is_initial_demand_editable")
    public String getIs_initial_demand_editable(){
        return this.is_initial_demand_editable ;
    }

    /**
     * 设置 [初始需求是否可以编辑]
     */
    @JsonProperty("is_initial_demand_editable")
    public void setIs_initial_demand_editable(String  is_initial_demand_editable){
        this.is_initial_demand_editable = is_initial_demand_editable ;
        this.is_initial_demand_editableDirtyFlag = true ;
    }

     /**
     * 获取 [初始需求是否可以编辑]脏标记
     */
    @JsonIgnore
    public boolean getIs_initial_demand_editableDirtyFlag(){
        return this.is_initial_demand_editableDirtyFlag ;
    }   

    /**
     * 获取 [是锁定]
     */
    @JsonProperty("is_locked")
    public String getIs_locked(){
        return this.is_locked ;
    }

    /**
     * 设置 [是锁定]
     */
    @JsonProperty("is_locked")
    public void setIs_locked(String  is_locked){
        this.is_locked = is_locked ;
        this.is_lockedDirtyFlag = true ;
    }

     /**
     * 获取 [是锁定]脏标记
     */
    @JsonIgnore
    public boolean getIs_lockedDirtyFlag(){
        return this.is_lockedDirtyFlag ;
    }   

    /**
     * 获取 [至]
     */
    @JsonProperty("location_dest_id")
    public Integer getLocation_dest_id(){
        return this.location_dest_id ;
    }

    /**
     * 设置 [至]
     */
    @JsonProperty("location_dest_id")
    public void setLocation_dest_id(Integer  location_dest_id){
        this.location_dest_id = location_dest_id ;
        this.location_dest_idDirtyFlag = true ;
    }

     /**
     * 获取 [至]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_idDirtyFlag(){
        return this.location_dest_idDirtyFlag ;
    }   

    /**
     * 获取 [至]
     */
    @JsonProperty("location_dest_id_text")
    public String getLocation_dest_id_text(){
        return this.location_dest_id_text ;
    }

    /**
     * 设置 [至]
     */
    @JsonProperty("location_dest_id_text")
    public void setLocation_dest_id_text(String  location_dest_id_text){
        this.location_dest_id_text = location_dest_id_text ;
        this.location_dest_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [至]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_id_textDirtyFlag(){
        return this.location_dest_id_textDirtyFlag ;
    }   

    /**
     * 获取 [从]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return this.location_id ;
    }

    /**
     * 设置 [从]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

     /**
     * 获取 [从]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return this.location_idDirtyFlag ;
    }   

    /**
     * 获取 [从]
     */
    @JsonProperty("location_id_text")
    public String getLocation_id_text(){
        return this.location_id_text ;
    }

    /**
     * 设置 [从]
     */
    @JsonProperty("location_id_text")
    public void setLocation_id_text(String  location_id_text){
        this.location_id_text = location_id_text ;
        this.location_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [从]脏标记
     */
    @JsonIgnore
    public boolean getLocation_id_textDirtyFlag(){
        return this.location_id_textDirtyFlag ;
    }   

    /**
     * 获取 [批次可见]
     */
    @JsonProperty("lots_visible")
    public String getLots_visible(){
        return this.lots_visible ;
    }

    /**
     * 设置 [批次可见]
     */
    @JsonProperty("lots_visible")
    public void setLots_visible(String  lots_visible){
        this.lots_visible = lots_visible ;
        this.lots_visibleDirtyFlag = true ;
    }

     /**
     * 获取 [批次可见]脏标记
     */
    @JsonIgnore
    public boolean getLots_visibleDirtyFlag(){
        return this.lots_visibleDirtyFlag ;
    }   

    /**
     * 获取 [批次/序列号码]
     */
    @JsonProperty("lot_id")
    public Integer getLot_id(){
        return this.lot_id ;
    }

    /**
     * 设置 [批次/序列号码]
     */
    @JsonProperty("lot_id")
    public void setLot_id(Integer  lot_id){
        this.lot_id = lot_id ;
        this.lot_idDirtyFlag = true ;
    }

     /**
     * 获取 [批次/序列号码]脏标记
     */
    @JsonIgnore
    public boolean getLot_idDirtyFlag(){
        return this.lot_idDirtyFlag ;
    }   

    /**
     * 获取 [批次/序列号码]
     */
    @JsonProperty("lot_id_text")
    public String getLot_id_text(){
        return this.lot_id_text ;
    }

    /**
     * 设置 [批次/序列号码]
     */
    @JsonProperty("lot_id_text")
    public void setLot_id_text(String  lot_id_text){
        this.lot_id_text = lot_id_text ;
        this.lot_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [批次/序列号码]脏标记
     */
    @JsonIgnore
    public boolean getLot_id_textDirtyFlag(){
        return this.lot_id_textDirtyFlag ;
    }   

    /**
     * 获取 [批次/序列号 名称]
     */
    @JsonProperty("lot_name")
    public String getLot_name(){
        return this.lot_name ;
    }

    /**
     * 设置 [批次/序列号 名称]
     */
    @JsonProperty("lot_name")
    public void setLot_name(String  lot_name){
        this.lot_name = lot_name ;
        this.lot_nameDirtyFlag = true ;
    }

     /**
     * 获取 [批次/序列号 名称]脏标记
     */
    @JsonIgnore
    public boolean getLot_nameDirtyFlag(){
        return this.lot_nameDirtyFlag ;
    }   

    /**
     * 获取 [完工批次/序列号]
     */
    @JsonProperty("lot_produced_id")
    public Integer getLot_produced_id(){
        return this.lot_produced_id ;
    }

    /**
     * 设置 [完工批次/序列号]
     */
    @JsonProperty("lot_produced_id")
    public void setLot_produced_id(Integer  lot_produced_id){
        this.lot_produced_id = lot_produced_id ;
        this.lot_produced_idDirtyFlag = true ;
    }

     /**
     * 获取 [完工批次/序列号]脏标记
     */
    @JsonIgnore
    public boolean getLot_produced_idDirtyFlag(){
        return this.lot_produced_idDirtyFlag ;
    }   

    /**
     * 获取 [完工批次/序列号]
     */
    @JsonProperty("lot_produced_id_text")
    public String getLot_produced_id_text(){
        return this.lot_produced_id_text ;
    }

    /**
     * 设置 [完工批次/序列号]
     */
    @JsonProperty("lot_produced_id_text")
    public void setLot_produced_id_text(String  lot_produced_id_text){
        this.lot_produced_id_text = lot_produced_id_text ;
        this.lot_produced_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [完工批次/序列号]脏标记
     */
    @JsonIgnore
    public boolean getLot_produced_id_textDirtyFlag(){
        return this.lot_produced_id_textDirtyFlag ;
    }   

    /**
     * 获取 [产成品数量]
     */
    @JsonProperty("lot_produced_qty")
    public Double getLot_produced_qty(){
        return this.lot_produced_qty ;
    }

    /**
     * 设置 [产成品数量]
     */
    @JsonProperty("lot_produced_qty")
    public void setLot_produced_qty(Double  lot_produced_qty){
        this.lot_produced_qty = lot_produced_qty ;
        this.lot_produced_qtyDirtyFlag = true ;
    }

     /**
     * 获取 [产成品数量]脏标记
     */
    @JsonIgnore
    public boolean getLot_produced_qtyDirtyFlag(){
        return this.lot_produced_qtyDirtyFlag ;
    }   

    /**
     * 获取 [库存移动]
     */
    @JsonProperty("move_id")
    public Integer getMove_id(){
        return this.move_id ;
    }

    /**
     * 设置 [库存移动]
     */
    @JsonProperty("move_id")
    public void setMove_id(Integer  move_id){
        this.move_id = move_id ;
        this.move_idDirtyFlag = true ;
    }

     /**
     * 获取 [库存移动]脏标记
     */
    @JsonIgnore
    public boolean getMove_idDirtyFlag(){
        return this.move_idDirtyFlag ;
    }   

    /**
     * 获取 [库存移动]
     */
    @JsonProperty("move_id_text")
    public String getMove_id_text(){
        return this.move_id_text ;
    }

    /**
     * 设置 [库存移动]
     */
    @JsonProperty("move_id_text")
    public void setMove_id_text(String  move_id_text){
        this.move_id_text = move_id_text ;
        this.move_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [库存移动]脏标记
     */
    @JsonIgnore
    public boolean getMove_id_textDirtyFlag(){
        return this.move_id_textDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("owner_id")
    public Integer getOwner_id(){
        return this.owner_id ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("owner_id")
    public void setOwner_id(Integer  owner_id){
        this.owner_id = owner_id ;
        this.owner_idDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getOwner_idDirtyFlag(){
        return this.owner_idDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("owner_id_text")
    public String getOwner_id_text(){
        return this.owner_id_text ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("owner_id_text")
    public void setOwner_id_text(String  owner_id_text){
        this.owner_id_text = owner_id_text ;
        this.owner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getOwner_id_textDirtyFlag(){
        return this.owner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [源包裹]
     */
    @JsonProperty("package_id")
    public Integer getPackage_id(){
        return this.package_id ;
    }

    /**
     * 设置 [源包裹]
     */
    @JsonProperty("package_id")
    public void setPackage_id(Integer  package_id){
        this.package_id = package_id ;
        this.package_idDirtyFlag = true ;
    }

     /**
     * 获取 [源包裹]脏标记
     */
    @JsonIgnore
    public boolean getPackage_idDirtyFlag(){
        return this.package_idDirtyFlag ;
    }   

    /**
     * 获取 [源包裹]
     */
    @JsonProperty("package_id_text")
    public String getPackage_id_text(){
        return this.package_id_text ;
    }

    /**
     * 设置 [源包裹]
     */
    @JsonProperty("package_id_text")
    public void setPackage_id_text(String  package_id_text){
        this.package_id_text = package_id_text ;
        this.package_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [源包裹]脏标记
     */
    @JsonIgnore
    public boolean getPackage_id_textDirtyFlag(){
        return this.package_id_textDirtyFlag ;
    }   

    /**
     * 获取 [包裹层级]
     */
    @JsonProperty("package_level_id")
    public Integer getPackage_level_id(){
        return this.package_level_id ;
    }

    /**
     * 设置 [包裹层级]
     */
    @JsonProperty("package_level_id")
    public void setPackage_level_id(Integer  package_level_id){
        this.package_level_id = package_level_id ;
        this.package_level_idDirtyFlag = true ;
    }

     /**
     * 获取 [包裹层级]脏标记
     */
    @JsonIgnore
    public boolean getPackage_level_idDirtyFlag(){
        return this.package_level_idDirtyFlag ;
    }   

    /**
     * 获取 [库存拣货]
     */
    @JsonProperty("picking_id")
    public Integer getPicking_id(){
        return this.picking_id ;
    }

    /**
     * 设置 [库存拣货]
     */
    @JsonProperty("picking_id")
    public void setPicking_id(Integer  picking_id){
        this.picking_id = picking_id ;
        this.picking_idDirtyFlag = true ;
    }

     /**
     * 获取 [库存拣货]脏标记
     */
    @JsonIgnore
    public boolean getPicking_idDirtyFlag(){
        return this.picking_idDirtyFlag ;
    }   

    /**
     * 获取 [库存拣货]
     */
    @JsonProperty("picking_id_text")
    public String getPicking_id_text(){
        return this.picking_id_text ;
    }

    /**
     * 设置 [库存拣货]
     */
    @JsonProperty("picking_id_text")
    public void setPicking_id_text(String  picking_id_text){
        this.picking_id_text = picking_id_text ;
        this.picking_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [库存拣货]脏标记
     */
    @JsonIgnore
    public boolean getPicking_id_textDirtyFlag(){
        return this.picking_id_textDirtyFlag ;
    }   

    /**
     * 获取 [移动整个包裹]
     */
    @JsonProperty("picking_type_entire_packs")
    public String getPicking_type_entire_packs(){
        return this.picking_type_entire_packs ;
    }

    /**
     * 设置 [移动整个包裹]
     */
    @JsonProperty("picking_type_entire_packs")
    public void setPicking_type_entire_packs(String  picking_type_entire_packs){
        this.picking_type_entire_packs = picking_type_entire_packs ;
        this.picking_type_entire_packsDirtyFlag = true ;
    }

     /**
     * 获取 [移动整个包裹]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_entire_packsDirtyFlag(){
        return this.picking_type_entire_packsDirtyFlag ;
    }   

    /**
     * 获取 [创建新批次/序列号码]
     */
    @JsonProperty("picking_type_use_create_lots")
    public String getPicking_type_use_create_lots(){
        return this.picking_type_use_create_lots ;
    }

    /**
     * 设置 [创建新批次/序列号码]
     */
    @JsonProperty("picking_type_use_create_lots")
    public void setPicking_type_use_create_lots(String  picking_type_use_create_lots){
        this.picking_type_use_create_lots = picking_type_use_create_lots ;
        this.picking_type_use_create_lotsDirtyFlag = true ;
    }

     /**
     * 获取 [创建新批次/序列号码]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_use_create_lotsDirtyFlag(){
        return this.picking_type_use_create_lotsDirtyFlag ;
    }   

    /**
     * 获取 [使用已有批次/序列号码]
     */
    @JsonProperty("picking_type_use_existing_lots")
    public String getPicking_type_use_existing_lots(){
        return this.picking_type_use_existing_lots ;
    }

    /**
     * 设置 [使用已有批次/序列号码]
     */
    @JsonProperty("picking_type_use_existing_lots")
    public void setPicking_type_use_existing_lots(String  picking_type_use_existing_lots){
        this.picking_type_use_existing_lots = picking_type_use_existing_lots ;
        this.picking_type_use_existing_lotsDirtyFlag = true ;
    }

     /**
     * 获取 [使用已有批次/序列号码]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_use_existing_lotsDirtyFlag(){
        return this.picking_type_use_existing_lotsDirtyFlag ;
    }   

    /**
     * 获取 [生产行]
     */
    @JsonProperty("produce_line_ids")
    public String getProduce_line_ids(){
        return this.produce_line_ids ;
    }

    /**
     * 设置 [生产行]
     */
    @JsonProperty("produce_line_ids")
    public void setProduce_line_ids(String  produce_line_ids){
        this.produce_line_ids = produce_line_ids ;
        this.produce_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [生产行]脏标记
     */
    @JsonIgnore
    public boolean getProduce_line_idsDirtyFlag(){
        return this.produce_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [生产单]
     */
    @JsonProperty("production_id")
    public Integer getProduction_id(){
        return this.production_id ;
    }

    /**
     * 设置 [生产单]
     */
    @JsonProperty("production_id")
    public void setProduction_id(Integer  production_id){
        this.production_id = production_id ;
        this.production_idDirtyFlag = true ;
    }

     /**
     * 获取 [生产单]脏标记
     */
    @JsonIgnore
    public boolean getProduction_idDirtyFlag(){
        return this.production_idDirtyFlag ;
    }   

    /**
     * 获取 [生产单]
     */
    @JsonProperty("production_id_text")
    public String getProduction_id_text(){
        return this.production_id_text ;
    }

    /**
     * 设置 [生产单]
     */
    @JsonProperty("production_id_text")
    public void setProduction_id_text(String  production_id_text){
        this.production_id_text = production_id_text ;
        this.production_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [生产单]脏标记
     */
    @JsonIgnore
    public boolean getProduction_id_textDirtyFlag(){
        return this.production_id_textDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return this.product_id_text ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return this.product_id_textDirtyFlag ;
    }   

    /**
     * 获取 [实际预留数量]
     */
    @JsonProperty("product_qty")
    public Double getProduct_qty(){
        return this.product_qty ;
    }

    /**
     * 设置 [实际预留数量]
     */
    @JsonProperty("product_qty")
    public void setProduct_qty(Double  product_qty){
        this.product_qty = product_qty ;
        this.product_qtyDirtyFlag = true ;
    }

     /**
     * 获取 [实际预留数量]脏标记
     */
    @JsonIgnore
    public boolean getProduct_qtyDirtyFlag(){
        return this.product_qtyDirtyFlag ;
    }   

    /**
     * 获取 [单位]
     */
    @JsonProperty("product_uom_id")
    public Integer getProduct_uom_id(){
        return this.product_uom_id ;
    }

    /**
     * 设置 [单位]
     */
    @JsonProperty("product_uom_id")
    public void setProduct_uom_id(Integer  product_uom_id){
        this.product_uom_id = product_uom_id ;
        this.product_uom_idDirtyFlag = true ;
    }

     /**
     * 获取 [单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_idDirtyFlag(){
        return this.product_uom_idDirtyFlag ;
    }   

    /**
     * 获取 [单位]
     */
    @JsonProperty("product_uom_id_text")
    public String getProduct_uom_id_text(){
        return this.product_uom_id_text ;
    }

    /**
     * 设置 [单位]
     */
    @JsonProperty("product_uom_id_text")
    public void setProduct_uom_id_text(String  product_uom_id_text){
        this.product_uom_id_text = product_uom_id_text ;
        this.product_uom_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_id_textDirtyFlag(){
        return this.product_uom_id_textDirtyFlag ;
    }   

    /**
     * 获取 [已保留]
     */
    @JsonProperty("product_uom_qty")
    public Double getProduct_uom_qty(){
        return this.product_uom_qty ;
    }

    /**
     * 设置 [已保留]
     */
    @JsonProperty("product_uom_qty")
    public void setProduct_uom_qty(Double  product_uom_qty){
        this.product_uom_qty = product_uom_qty ;
        this.product_uom_qtyDirtyFlag = true ;
    }

     /**
     * 获取 [已保留]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_qtyDirtyFlag(){
        return this.product_uom_qtyDirtyFlag ;
    }   

    /**
     * 获取 [完成]
     */
    @JsonProperty("qty_done")
    public Double getQty_done(){
        return this.qty_done ;
    }

    /**
     * 设置 [完成]
     */
    @JsonProperty("qty_done")
    public void setQty_done(Double  qty_done){
        this.qty_done = qty_done ;
        this.qty_doneDirtyFlag = true ;
    }

     /**
     * 获取 [完成]脏标记
     */
    @JsonIgnore
    public boolean getQty_doneDirtyFlag(){
        return this.qty_doneDirtyFlag ;
    }   

    /**
     * 获取 [编号]
     */
    @JsonProperty("reference")
    public String getReference(){
        return this.reference ;
    }

    /**
     * 设置 [编号]
     */
    @JsonProperty("reference")
    public void setReference(String  reference){
        this.reference = reference ;
        this.referenceDirtyFlag = true ;
    }

     /**
     * 获取 [编号]脏标记
     */
    @JsonIgnore
    public boolean getReferenceDirtyFlag(){
        return this.referenceDirtyFlag ;
    }   

    /**
     * 获取 [目的地包裹]
     */
    @JsonProperty("result_package_id")
    public Integer getResult_package_id(){
        return this.result_package_id ;
    }

    /**
     * 设置 [目的地包裹]
     */
    @JsonProperty("result_package_id")
    public void setResult_package_id(Integer  result_package_id){
        this.result_package_id = result_package_id ;
        this.result_package_idDirtyFlag = true ;
    }

     /**
     * 获取 [目的地包裹]脏标记
     */
    @JsonIgnore
    public boolean getResult_package_idDirtyFlag(){
        return this.result_package_idDirtyFlag ;
    }   

    /**
     * 获取 [目的地包裹]
     */
    @JsonProperty("result_package_id_text")
    public String getResult_package_id_text(){
        return this.result_package_id_text ;
    }

    /**
     * 设置 [目的地包裹]
     */
    @JsonProperty("result_package_id_text")
    public void setResult_package_id_text(String  result_package_id_text){
        this.result_package_id_text = result_package_id_text ;
        this.result_package_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [目的地包裹]脏标记
     */
    @JsonIgnore
    public boolean getResult_package_id_textDirtyFlag(){
        return this.result_package_id_textDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [追踪]
     */
    @JsonProperty("tracking")
    public String getTracking(){
        return this.tracking ;
    }

    /**
     * 设置 [追踪]
     */
    @JsonProperty("tracking")
    public void setTracking(String  tracking){
        this.tracking = tracking ;
        this.trackingDirtyFlag = true ;
    }

     /**
     * 获取 [追踪]脏标记
     */
    @JsonIgnore
    public boolean getTrackingDirtyFlag(){
        return this.trackingDirtyFlag ;
    }   

    /**
     * 获取 [工单]
     */
    @JsonProperty("workorder_id")
    public Integer getWorkorder_id(){
        return this.workorder_id ;
    }

    /**
     * 设置 [工单]
     */
    @JsonProperty("workorder_id")
    public void setWorkorder_id(Integer  workorder_id){
        this.workorder_id = workorder_id ;
        this.workorder_idDirtyFlag = true ;
    }

     /**
     * 获取 [工单]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_idDirtyFlag(){
        return this.workorder_idDirtyFlag ;
    }   

    /**
     * 获取 [工单]
     */
    @JsonProperty("workorder_id_text")
    public String getWorkorder_id_text(){
        return this.workorder_id_text ;
    }

    /**
     * 设置 [工单]
     */
    @JsonProperty("workorder_id_text")
    public void setWorkorder_id_text(String  workorder_id_text){
        this.workorder_id_text = workorder_id_text ;
        this.workorder_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [工单]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_id_textDirtyFlag(){
        return this.workorder_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("consume_line_ids") instanceof Boolean)&& map.get("consume_line_ids")!=null){
			Object[] objs = (Object[])map.get("consume_line_ids");
			if(objs.length > 0){
				Integer[] consume_line_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setConsume_line_ids(Arrays.toString(consume_line_ids));
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("date") instanceof Boolean)&& map.get("date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date"));
   			this.setDate(new Timestamp(parse.getTime()));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(map.get("done_move") instanceof Boolean){
			this.setDone_move(((Boolean)map.get("done_move"))? "true" : "false");
		}
		if(map.get("done_wo") instanceof Boolean){
			this.setDone_wo(((Boolean)map.get("done_wo"))? "true" : "false");
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(map.get("is_initial_demand_editable") instanceof Boolean){
			this.setIs_initial_demand_editable(((Boolean)map.get("is_initial_demand_editable"))? "true" : "false");
		}
		if(map.get("is_locked") instanceof Boolean){
			this.setIs_locked(((Boolean)map.get("is_locked"))? "true" : "false");
		}
		if(!(map.get("location_dest_id") instanceof Boolean)&& map.get("location_dest_id")!=null){
			Object[] objs = (Object[])map.get("location_dest_id");
			if(objs.length > 0){
				this.setLocation_dest_id((Integer)objs[0]);
			}
		}
		if(!(map.get("location_dest_id") instanceof Boolean)&& map.get("location_dest_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("location_dest_id");
			if(objs.length > 1){
				this.setLocation_dest_id_text((String)objs[1]);
			}
		}
		if(!(map.get("location_id") instanceof Boolean)&& map.get("location_id")!=null){
			Object[] objs = (Object[])map.get("location_id");
			if(objs.length > 0){
				this.setLocation_id((Integer)objs[0]);
			}
		}
		if(!(map.get("location_id") instanceof Boolean)&& map.get("location_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("location_id");
			if(objs.length > 1){
				this.setLocation_id_text((String)objs[1]);
			}
		}
		if(map.get("lots_visible") instanceof Boolean){
			this.setLots_visible(((Boolean)map.get("lots_visible"))? "true" : "false");
		}
		if(!(map.get("lot_id") instanceof Boolean)&& map.get("lot_id")!=null){
			Object[] objs = (Object[])map.get("lot_id");
			if(objs.length > 0){
				this.setLot_id((Integer)objs[0]);
			}
		}
		if(!(map.get("lot_id") instanceof Boolean)&& map.get("lot_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("lot_id");
			if(objs.length > 1){
				this.setLot_id_text((String)objs[1]);
			}
		}
		if(!(map.get("lot_name") instanceof Boolean)&& map.get("lot_name")!=null){
			this.setLot_name((String)map.get("lot_name"));
		}
		if(!(map.get("lot_produced_id") instanceof Boolean)&& map.get("lot_produced_id")!=null){
			Object[] objs = (Object[])map.get("lot_produced_id");
			if(objs.length > 0){
				this.setLot_produced_id((Integer)objs[0]);
			}
		}
		if(!(map.get("lot_produced_id") instanceof Boolean)&& map.get("lot_produced_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("lot_produced_id");
			if(objs.length > 1){
				this.setLot_produced_id_text((String)objs[1]);
			}
		}
		if(!(map.get("lot_produced_qty") instanceof Boolean)&& map.get("lot_produced_qty")!=null){
			this.setLot_produced_qty((Double)map.get("lot_produced_qty"));
		}
		if(!(map.get("move_id") instanceof Boolean)&& map.get("move_id")!=null){
			Object[] objs = (Object[])map.get("move_id");
			if(objs.length > 0){
				this.setMove_id((Integer)objs[0]);
			}
		}
		if(!(map.get("move_id") instanceof Boolean)&& map.get("move_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("move_id");
			if(objs.length > 1){
				this.setMove_id_text((String)objs[1]);
			}
		}
		if(!(map.get("owner_id") instanceof Boolean)&& map.get("owner_id")!=null){
			Object[] objs = (Object[])map.get("owner_id");
			if(objs.length > 0){
				this.setOwner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("owner_id") instanceof Boolean)&& map.get("owner_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("owner_id");
			if(objs.length > 1){
				this.setOwner_id_text((String)objs[1]);
			}
		}
		if(!(map.get("package_id") instanceof Boolean)&& map.get("package_id")!=null){
			Object[] objs = (Object[])map.get("package_id");
			if(objs.length > 0){
				this.setPackage_id((Integer)objs[0]);
			}
		}
		if(!(map.get("package_id") instanceof Boolean)&& map.get("package_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("package_id");
			if(objs.length > 1){
				this.setPackage_id_text((String)objs[1]);
			}
		}
		if(!(map.get("package_level_id") instanceof Boolean)&& map.get("package_level_id")!=null){
			Object[] objs = (Object[])map.get("package_level_id");
			if(objs.length > 0){
				this.setPackage_level_id((Integer)objs[0]);
			}
		}
		if(!(map.get("picking_id") instanceof Boolean)&& map.get("picking_id")!=null){
			Object[] objs = (Object[])map.get("picking_id");
			if(objs.length > 0){
				this.setPicking_id((Integer)objs[0]);
			}
		}
		if(!(map.get("picking_id") instanceof Boolean)&& map.get("picking_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("picking_id");
			if(objs.length > 1){
				this.setPicking_id_text((String)objs[1]);
			}
		}
		if(map.get("picking_type_entire_packs") instanceof Boolean){
			this.setPicking_type_entire_packs(((Boolean)map.get("picking_type_entire_packs"))? "true" : "false");
		}
		if(map.get("picking_type_use_create_lots") instanceof Boolean){
			this.setPicking_type_use_create_lots(((Boolean)map.get("picking_type_use_create_lots"))? "true" : "false");
		}
		if(map.get("picking_type_use_existing_lots") instanceof Boolean){
			this.setPicking_type_use_existing_lots(((Boolean)map.get("picking_type_use_existing_lots"))? "true" : "false");
		}
		if(!(map.get("produce_line_ids") instanceof Boolean)&& map.get("produce_line_ids")!=null){
			Object[] objs = (Object[])map.get("produce_line_ids");
			if(objs.length > 0){
				Integer[] produce_line_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setProduce_line_ids(Arrays.toString(produce_line_ids));
			}
		}
		if(!(map.get("production_id") instanceof Boolean)&& map.get("production_id")!=null){
			Object[] objs = (Object[])map.get("production_id");
			if(objs.length > 0){
				this.setProduction_id((Integer)objs[0]);
			}
		}
		if(!(map.get("production_id") instanceof Boolean)&& map.get("production_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("production_id");
			if(objs.length > 1){
				this.setProduction_id_text((String)objs[1]);
			}
		}
		if(!(map.get("product_id") instanceof Boolean)&& map.get("product_id")!=null){
			Object[] objs = (Object[])map.get("product_id");
			if(objs.length > 0){
				this.setProduct_id((Integer)objs[0]);
			}
		}
		if(!(map.get("product_id") instanceof Boolean)&& map.get("product_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("product_id");
			if(objs.length > 1){
				this.setProduct_id_text((String)objs[1]);
			}
		}
		if(!(map.get("product_qty") instanceof Boolean)&& map.get("product_qty")!=null){
			this.setProduct_qty((Double)map.get("product_qty"));
		}
		if(!(map.get("product_uom_id") instanceof Boolean)&& map.get("product_uom_id")!=null){
			Object[] objs = (Object[])map.get("product_uom_id");
			if(objs.length > 0){
				this.setProduct_uom_id((Integer)objs[0]);
			}
		}
		if(!(map.get("product_uom_id") instanceof Boolean)&& map.get("product_uom_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("product_uom_id");
			if(objs.length > 1){
				this.setProduct_uom_id_text((String)objs[1]);
			}
		}
		if(!(map.get("product_uom_qty") instanceof Boolean)&& map.get("product_uom_qty")!=null){
			this.setProduct_uom_qty((Double)map.get("product_uom_qty"));
		}
		if(!(map.get("qty_done") instanceof Boolean)&& map.get("qty_done")!=null){
			this.setQty_done((Double)map.get("qty_done"));
		}
		if(!(map.get("reference") instanceof Boolean)&& map.get("reference")!=null){
			this.setReference((String)map.get("reference"));
		}
		if(!(map.get("result_package_id") instanceof Boolean)&& map.get("result_package_id")!=null){
			Object[] objs = (Object[])map.get("result_package_id");
			if(objs.length > 0){
				this.setResult_package_id((Integer)objs[0]);
			}
		}
		if(!(map.get("result_package_id") instanceof Boolean)&& map.get("result_package_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("result_package_id");
			if(objs.length > 1){
				this.setResult_package_id_text((String)objs[1]);
			}
		}
		if(!(map.get("state") instanceof Boolean)&& map.get("state")!=null){
			this.setState((String)map.get("state"));
		}
		if(!(map.get("tracking") instanceof Boolean)&& map.get("tracking")!=null){
			this.setTracking((String)map.get("tracking"));
		}
		if(!(map.get("workorder_id") instanceof Boolean)&& map.get("workorder_id")!=null){
			Object[] objs = (Object[])map.get("workorder_id");
			if(objs.length > 0){
				this.setWorkorder_id((Integer)objs[0]);
			}
		}
		if(!(map.get("workorder_id") instanceof Boolean)&& map.get("workorder_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("workorder_id");
			if(objs.length > 1){
				this.setWorkorder_id_text((String)objs[1]);
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getConsume_line_ids()!=null&&this.getConsume_line_idsDirtyFlag()){
			map.put("consume_line_ids",this.getConsume_line_ids());
		}else if(this.getConsume_line_idsDirtyFlag()){
			map.put("consume_line_ids",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDate()!=null&&this.getDateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate());
			map.put("date",datetimeStr);
		}else if(this.getDateDirtyFlag()){
			map.put("date",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getDone_move()!=null&&this.getDone_moveDirtyFlag()){
			map.put("done_move",Boolean.parseBoolean(this.getDone_move()));		
		}		if(this.getDone_wo()!=null&&this.getDone_woDirtyFlag()){
			map.put("done_wo",Boolean.parseBoolean(this.getDone_wo()));		
		}		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getIs_initial_demand_editable()!=null&&this.getIs_initial_demand_editableDirtyFlag()){
			map.put("is_initial_demand_editable",Boolean.parseBoolean(this.getIs_initial_demand_editable()));		
		}		if(this.getIs_locked()!=null&&this.getIs_lockedDirtyFlag()){
			map.put("is_locked",Boolean.parseBoolean(this.getIs_locked()));		
		}		if(this.getLocation_dest_id()!=null&&this.getLocation_dest_idDirtyFlag()){
			map.put("location_dest_id",this.getLocation_dest_id());
		}else if(this.getLocation_dest_idDirtyFlag()){
			map.put("location_dest_id",false);
		}
		if(this.getLocation_dest_id_text()!=null&&this.getLocation_dest_id_textDirtyFlag()){
			//忽略文本外键location_dest_id_text
		}else if(this.getLocation_dest_id_textDirtyFlag()){
			map.put("location_dest_id",false);
		}
		if(this.getLocation_id()!=null&&this.getLocation_idDirtyFlag()){
			map.put("location_id",this.getLocation_id());
		}else if(this.getLocation_idDirtyFlag()){
			map.put("location_id",false);
		}
		if(this.getLocation_id_text()!=null&&this.getLocation_id_textDirtyFlag()){
			//忽略文本外键location_id_text
		}else if(this.getLocation_id_textDirtyFlag()){
			map.put("location_id",false);
		}
		if(this.getLots_visible()!=null&&this.getLots_visibleDirtyFlag()){
			map.put("lots_visible",Boolean.parseBoolean(this.getLots_visible()));		
		}		if(this.getLot_id()!=null&&this.getLot_idDirtyFlag()){
			map.put("lot_id",this.getLot_id());
		}else if(this.getLot_idDirtyFlag()){
			map.put("lot_id",false);
		}
		if(this.getLot_id_text()!=null&&this.getLot_id_textDirtyFlag()){
			//忽略文本外键lot_id_text
		}else if(this.getLot_id_textDirtyFlag()){
			map.put("lot_id",false);
		}
		if(this.getLot_name()!=null&&this.getLot_nameDirtyFlag()){
			map.put("lot_name",this.getLot_name());
		}else if(this.getLot_nameDirtyFlag()){
			map.put("lot_name",false);
		}
		if(this.getLot_produced_id()!=null&&this.getLot_produced_idDirtyFlag()){
			map.put("lot_produced_id",this.getLot_produced_id());
		}else if(this.getLot_produced_idDirtyFlag()){
			map.put("lot_produced_id",false);
		}
		if(this.getLot_produced_id_text()!=null&&this.getLot_produced_id_textDirtyFlag()){
			//忽略文本外键lot_produced_id_text
		}else if(this.getLot_produced_id_textDirtyFlag()){
			map.put("lot_produced_id",false);
		}
		if(this.getLot_produced_qty()!=null&&this.getLot_produced_qtyDirtyFlag()){
			map.put("lot_produced_qty",this.getLot_produced_qty());
		}else if(this.getLot_produced_qtyDirtyFlag()){
			map.put("lot_produced_qty",false);
		}
		if(this.getMove_id()!=null&&this.getMove_idDirtyFlag()){
			map.put("move_id",this.getMove_id());
		}else if(this.getMove_idDirtyFlag()){
			map.put("move_id",false);
		}
		if(this.getMove_id_text()!=null&&this.getMove_id_textDirtyFlag()){
			//忽略文本外键move_id_text
		}else if(this.getMove_id_textDirtyFlag()){
			map.put("move_id",false);
		}
		if(this.getOwner_id()!=null&&this.getOwner_idDirtyFlag()){
			map.put("owner_id",this.getOwner_id());
		}else if(this.getOwner_idDirtyFlag()){
			map.put("owner_id",false);
		}
		if(this.getOwner_id_text()!=null&&this.getOwner_id_textDirtyFlag()){
			//忽略文本外键owner_id_text
		}else if(this.getOwner_id_textDirtyFlag()){
			map.put("owner_id",false);
		}
		if(this.getPackage_id()!=null&&this.getPackage_idDirtyFlag()){
			map.put("package_id",this.getPackage_id());
		}else if(this.getPackage_idDirtyFlag()){
			map.put("package_id",false);
		}
		if(this.getPackage_id_text()!=null&&this.getPackage_id_textDirtyFlag()){
			//忽略文本外键package_id_text
		}else if(this.getPackage_id_textDirtyFlag()){
			map.put("package_id",false);
		}
		if(this.getPackage_level_id()!=null&&this.getPackage_level_idDirtyFlag()){
			map.put("package_level_id",this.getPackage_level_id());
		}else if(this.getPackage_level_idDirtyFlag()){
			map.put("package_level_id",false);
		}
		if(this.getPicking_id()!=null&&this.getPicking_idDirtyFlag()){
			map.put("picking_id",this.getPicking_id());
		}else if(this.getPicking_idDirtyFlag()){
			map.put("picking_id",false);
		}
		if(this.getPicking_id_text()!=null&&this.getPicking_id_textDirtyFlag()){
			//忽略文本外键picking_id_text
		}else if(this.getPicking_id_textDirtyFlag()){
			map.put("picking_id",false);
		}
		if(this.getPicking_type_entire_packs()!=null&&this.getPicking_type_entire_packsDirtyFlag()){
			map.put("picking_type_entire_packs",Boolean.parseBoolean(this.getPicking_type_entire_packs()));		
		}		if(this.getPicking_type_use_create_lots()!=null&&this.getPicking_type_use_create_lotsDirtyFlag()){
			map.put("picking_type_use_create_lots",Boolean.parseBoolean(this.getPicking_type_use_create_lots()));		
		}		if(this.getPicking_type_use_existing_lots()!=null&&this.getPicking_type_use_existing_lotsDirtyFlag()){
			map.put("picking_type_use_existing_lots",Boolean.parseBoolean(this.getPicking_type_use_existing_lots()));		
		}		if(this.getProduce_line_ids()!=null&&this.getProduce_line_idsDirtyFlag()){
			map.put("produce_line_ids",this.getProduce_line_ids());
		}else if(this.getProduce_line_idsDirtyFlag()){
			map.put("produce_line_ids",false);
		}
		if(this.getProduction_id()!=null&&this.getProduction_idDirtyFlag()){
			map.put("production_id",this.getProduction_id());
		}else if(this.getProduction_idDirtyFlag()){
			map.put("production_id",false);
		}
		if(this.getProduction_id_text()!=null&&this.getProduction_id_textDirtyFlag()){
			//忽略文本外键production_id_text
		}else if(this.getProduction_id_textDirtyFlag()){
			map.put("production_id",false);
		}
		if(this.getProduct_id()!=null&&this.getProduct_idDirtyFlag()){
			map.put("product_id",this.getProduct_id());
		}else if(this.getProduct_idDirtyFlag()){
			map.put("product_id",false);
		}
		if(this.getProduct_id_text()!=null&&this.getProduct_id_textDirtyFlag()){
			//忽略文本外键product_id_text
		}else if(this.getProduct_id_textDirtyFlag()){
			map.put("product_id",false);
		}
		if(this.getProduct_qty()!=null&&this.getProduct_qtyDirtyFlag()){
			map.put("product_qty",this.getProduct_qty());
		}else if(this.getProduct_qtyDirtyFlag()){
			map.put("product_qty",false);
		}
		if(this.getProduct_uom_id()!=null&&this.getProduct_uom_idDirtyFlag()){
			map.put("product_uom_id",this.getProduct_uom_id());
		}else if(this.getProduct_uom_idDirtyFlag()){
			map.put("product_uom_id",false);
		}
		if(this.getProduct_uom_id_text()!=null&&this.getProduct_uom_id_textDirtyFlag()){
			//忽略文本外键product_uom_id_text
		}else if(this.getProduct_uom_id_textDirtyFlag()){
			map.put("product_uom_id",false);
		}
		if(this.getProduct_uom_qty()!=null&&this.getProduct_uom_qtyDirtyFlag()){
			map.put("product_uom_qty",this.getProduct_uom_qty());
		}else if(this.getProduct_uom_qtyDirtyFlag()){
			map.put("product_uom_qty",false);
		}
		if(this.getQty_done()!=null&&this.getQty_doneDirtyFlag()){
			map.put("qty_done",this.getQty_done());
		}else if(this.getQty_doneDirtyFlag()){
			map.put("qty_done",false);
		}
		if(this.getReference()!=null&&this.getReferenceDirtyFlag()){
			map.put("reference",this.getReference());
		}else if(this.getReferenceDirtyFlag()){
			map.put("reference",false);
		}
		if(this.getResult_package_id()!=null&&this.getResult_package_idDirtyFlag()){
			map.put("result_package_id",this.getResult_package_id());
		}else if(this.getResult_package_idDirtyFlag()){
			map.put("result_package_id",false);
		}
		if(this.getResult_package_id_text()!=null&&this.getResult_package_id_textDirtyFlag()){
			//忽略文本外键result_package_id_text
		}else if(this.getResult_package_id_textDirtyFlag()){
			map.put("result_package_id",false);
		}
		if(this.getState()!=null&&this.getStateDirtyFlag()){
			map.put("state",this.getState());
		}else if(this.getStateDirtyFlag()){
			map.put("state",false);
		}
		if(this.getTracking()!=null&&this.getTrackingDirtyFlag()){
			map.put("tracking",this.getTracking());
		}else if(this.getTrackingDirtyFlag()){
			map.put("tracking",false);
		}
		if(this.getWorkorder_id()!=null&&this.getWorkorder_idDirtyFlag()){
			map.put("workorder_id",this.getWorkorder_id());
		}else if(this.getWorkorder_idDirtyFlag()){
			map.put("workorder_id",false);
		}
		if(this.getWorkorder_id_text()!=null&&this.getWorkorder_id_textDirtyFlag()){
			//忽略文本外键workorder_id_text
		}else if(this.getWorkorder_id_textDirtyFlag()){
			map.put("workorder_id",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
