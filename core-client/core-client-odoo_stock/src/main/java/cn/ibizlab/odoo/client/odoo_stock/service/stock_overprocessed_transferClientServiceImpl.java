package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_overprocessed_transfer;
import cn.ibizlab.odoo.core.client.service.Istock_overprocessed_transferClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_overprocessed_transferImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_overprocessed_transferOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_overprocessed_transferOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_overprocessed_transfer] 服务对象接口
 */
@Service
public class stock_overprocessed_transferClientServiceImpl implements Istock_overprocessed_transferClientService {
    @Autowired
    private  Istock_overprocessed_transferOdooClient  stock_overprocessed_transferOdooClient;

    public Istock_overprocessed_transfer createModel() {		
		return new stock_overprocessed_transferImpl();
	}


        public void create(Istock_overprocessed_transfer stock_overprocessed_transfer){
this.stock_overprocessed_transferOdooClient.create(stock_overprocessed_transfer) ;
        }
        
        public void get(Istock_overprocessed_transfer stock_overprocessed_transfer){
            this.stock_overprocessed_transferOdooClient.get(stock_overprocessed_transfer) ;
        }
        
        public void removeBatch(List<Istock_overprocessed_transfer> stock_overprocessed_transfers){
            
        }
        
        public Page<Istock_overprocessed_transfer> search(SearchContext context){
            return this.stock_overprocessed_transferOdooClient.search(context) ;
        }
        
        public void update(Istock_overprocessed_transfer stock_overprocessed_transfer){
this.stock_overprocessed_transferOdooClient.update(stock_overprocessed_transfer) ;
        }
        
        public void updateBatch(List<Istock_overprocessed_transfer> stock_overprocessed_transfers){
            
        }
        
        public void remove(Istock_overprocessed_transfer stock_overprocessed_transfer){
this.stock_overprocessed_transferOdooClient.remove(stock_overprocessed_transfer) ;
        }
        
        public void createBatch(List<Istock_overprocessed_transfer> stock_overprocessed_transfers){
            
        }
        
        public Page<Istock_overprocessed_transfer> select(SearchContext context){
            return null ;
        }
        

}

