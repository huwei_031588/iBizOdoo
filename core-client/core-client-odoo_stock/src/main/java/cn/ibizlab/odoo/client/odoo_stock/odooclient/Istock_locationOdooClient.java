package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_location;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_location] 服务对象客户端接口
 */
public interface Istock_locationOdooClient {
    
        public Page<Istock_location> search(SearchContext context);

        public void update(Istock_location stock_location);

        public void create(Istock_location stock_location);

        public void remove(Istock_location stock_location);

        public void removeBatch(Istock_location stock_location);

        public void get(Istock_location stock_location);

        public void createBatch(Istock_location stock_location);

        public void updateBatch(Istock_location stock_location);

        public List<Istock_location> select();


}