package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_location;
import cn.ibizlab.odoo.core.client.service.Istock_locationClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_locationImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_locationOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_locationOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_location] 服务对象接口
 */
@Service
public class stock_locationClientServiceImpl implements Istock_locationClientService {
    @Autowired
    private  Istock_locationOdooClient  stock_locationOdooClient;

    public Istock_location createModel() {		
		return new stock_locationImpl();
	}


        public Page<Istock_location> search(SearchContext context){
            return this.stock_locationOdooClient.search(context) ;
        }
        
        public void update(Istock_location stock_location){
this.stock_locationOdooClient.update(stock_location) ;
        }
        
        public void create(Istock_location stock_location){
this.stock_locationOdooClient.create(stock_location) ;
        }
        
        public void remove(Istock_location stock_location){
this.stock_locationOdooClient.remove(stock_location) ;
        }
        
        public void removeBatch(List<Istock_location> stock_locations){
            
        }
        
        public void get(Istock_location stock_location){
            this.stock_locationOdooClient.get(stock_location) ;
        }
        
        public void createBatch(List<Istock_location> stock_locations){
            
        }
        
        public void updateBatch(List<Istock_location> stock_locations){
            
        }
        
        public Page<Istock_location> select(SearchContext context){
            return null ;
        }
        

}

