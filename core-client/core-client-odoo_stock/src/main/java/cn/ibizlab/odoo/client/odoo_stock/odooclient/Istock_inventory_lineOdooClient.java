package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_inventory_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_inventory_line] 服务对象客户端接口
 */
public interface Istock_inventory_lineOdooClient {
    
        public void updateBatch(Istock_inventory_line stock_inventory_line);

        public void removeBatch(Istock_inventory_line stock_inventory_line);

        public void createBatch(Istock_inventory_line stock_inventory_line);

        public void update(Istock_inventory_line stock_inventory_line);

        public void create(Istock_inventory_line stock_inventory_line);

        public void remove(Istock_inventory_line stock_inventory_line);

        public Page<Istock_inventory_line> search(SearchContext context);

        public void get(Istock_inventory_line stock_inventory_line);

        public List<Istock_inventory_line> select();


}