package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_package_level;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_package_level] 服务对象客户端接口
 */
public interface Istock_package_levelOdooClient {
    
        public void createBatch(Istock_package_level stock_package_level);

        public void create(Istock_package_level stock_package_level);

        public Page<Istock_package_level> search(SearchContext context);

        public void update(Istock_package_level stock_package_level);

        public void updateBatch(Istock_package_level stock_package_level);

        public void removeBatch(Istock_package_level stock_package_level);

        public void remove(Istock_package_level stock_package_level);

        public void get(Istock_package_level stock_package_level);

        public List<Istock_package_level> select();


}