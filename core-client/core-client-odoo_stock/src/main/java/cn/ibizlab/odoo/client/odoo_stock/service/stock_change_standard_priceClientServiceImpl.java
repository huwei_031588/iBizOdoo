package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_change_standard_price;
import cn.ibizlab.odoo.core.client.service.Istock_change_standard_priceClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_change_standard_priceImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_change_standard_priceOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_change_standard_priceOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_change_standard_price] 服务对象接口
 */
@Service
public class stock_change_standard_priceClientServiceImpl implements Istock_change_standard_priceClientService {
    @Autowired
    private  Istock_change_standard_priceOdooClient  stock_change_standard_priceOdooClient;

    public Istock_change_standard_price createModel() {		
		return new stock_change_standard_priceImpl();
	}


        public void get(Istock_change_standard_price stock_change_standard_price){
            this.stock_change_standard_priceOdooClient.get(stock_change_standard_price) ;
        }
        
        public void create(Istock_change_standard_price stock_change_standard_price){
this.stock_change_standard_priceOdooClient.create(stock_change_standard_price) ;
        }
        
        public void updateBatch(List<Istock_change_standard_price> stock_change_standard_prices){
            
        }
        
        public void remove(Istock_change_standard_price stock_change_standard_price){
this.stock_change_standard_priceOdooClient.remove(stock_change_standard_price) ;
        }
        
        public Page<Istock_change_standard_price> search(SearchContext context){
            return this.stock_change_standard_priceOdooClient.search(context) ;
        }
        
        public void update(Istock_change_standard_price stock_change_standard_price){
this.stock_change_standard_priceOdooClient.update(stock_change_standard_price) ;
        }
        
        public void removeBatch(List<Istock_change_standard_price> stock_change_standard_prices){
            
        }
        
        public void createBatch(List<Istock_change_standard_price> stock_change_standard_prices){
            
        }
        
        public Page<Istock_change_standard_price> select(SearchContext context){
            return null ;
        }
        

}

