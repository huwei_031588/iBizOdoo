package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_return_picking_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_return_picking_line] 服务对象客户端接口
 */
public interface Istock_return_picking_lineOdooClient {
    
        public void removeBatch(Istock_return_picking_line stock_return_picking_line);

        public void remove(Istock_return_picking_line stock_return_picking_line);

        public void createBatch(Istock_return_picking_line stock_return_picking_line);

        public void get(Istock_return_picking_line stock_return_picking_line);

        public Page<Istock_return_picking_line> search(SearchContext context);

        public void update(Istock_return_picking_line stock_return_picking_line);

        public void updateBatch(Istock_return_picking_line stock_return_picking_line);

        public void create(Istock_return_picking_line stock_return_picking_line);

        public List<Istock_return_picking_line> select();


}