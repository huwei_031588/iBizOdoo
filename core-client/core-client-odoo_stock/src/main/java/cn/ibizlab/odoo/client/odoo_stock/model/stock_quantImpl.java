package cn.ibizlab.odoo.client.odoo_stock.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Istock_quant;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[stock_quant] 对象
 */
public class stock_quantImpl implements Istock_quant,Serializable{

    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 送货日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp in_date;

    @JsonIgnore
    public boolean in_dateDirtyFlag;
    
    /**
     * 位置
     */
    public Integer location_id;

    @JsonIgnore
    public boolean location_idDirtyFlag;
    
    /**
     * 位置
     */
    public String location_id_text;

    @JsonIgnore
    public boolean location_id_textDirtyFlag;
    
    /**
     * 批次/序列号码
     */
    public Integer lot_id;

    @JsonIgnore
    public boolean lot_idDirtyFlag;
    
    /**
     * 批次/序列号码
     */
    public String lot_id_text;

    @JsonIgnore
    public boolean lot_id_textDirtyFlag;
    
    /**
     * 所有者
     */
    public Integer owner_id;

    @JsonIgnore
    public boolean owner_idDirtyFlag;
    
    /**
     * 所有者
     */
    public String owner_id_text;

    @JsonIgnore
    public boolean owner_id_textDirtyFlag;
    
    /**
     * 包裹
     */
    public Integer package_id;

    @JsonIgnore
    public boolean package_idDirtyFlag;
    
    /**
     * 包裹
     */
    public String package_id_text;

    @JsonIgnore
    public boolean package_id_textDirtyFlag;
    
    /**
     * 产品
     */
    public Integer product_id;

    @JsonIgnore
    public boolean product_idDirtyFlag;
    
    /**
     * 产品
     */
    public String product_id_text;

    @JsonIgnore
    public boolean product_id_textDirtyFlag;
    
    /**
     * 产品模板
     */
    public Integer product_tmpl_id;

    @JsonIgnore
    public boolean product_tmpl_idDirtyFlag;
    
    /**
     * 单位
     */
    public Integer product_uom_id;

    @JsonIgnore
    public boolean product_uom_idDirtyFlag;
    
    /**
     * 数量
     */
    public Double quantity;

    @JsonIgnore
    public boolean quantityDirtyFlag;
    
    /**
     * 预留数量
     */
    public Double reserved_quantity;

    @JsonIgnore
    public boolean reserved_quantityDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [送货日期]
     */
    @JsonProperty("in_date")
    public Timestamp getIn_date(){
        return this.in_date ;
    }

    /**
     * 设置 [送货日期]
     */
    @JsonProperty("in_date")
    public void setIn_date(Timestamp  in_date){
        this.in_date = in_date ;
        this.in_dateDirtyFlag = true ;
    }

     /**
     * 获取 [送货日期]脏标记
     */
    @JsonIgnore
    public boolean getIn_dateDirtyFlag(){
        return this.in_dateDirtyFlag ;
    }   

    /**
     * 获取 [位置]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return this.location_id ;
    }

    /**
     * 设置 [位置]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

     /**
     * 获取 [位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return this.location_idDirtyFlag ;
    }   

    /**
     * 获取 [位置]
     */
    @JsonProperty("location_id_text")
    public String getLocation_id_text(){
        return this.location_id_text ;
    }

    /**
     * 设置 [位置]
     */
    @JsonProperty("location_id_text")
    public void setLocation_id_text(String  location_id_text){
        this.location_id_text = location_id_text ;
        this.location_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_id_textDirtyFlag(){
        return this.location_id_textDirtyFlag ;
    }   

    /**
     * 获取 [批次/序列号码]
     */
    @JsonProperty("lot_id")
    public Integer getLot_id(){
        return this.lot_id ;
    }

    /**
     * 设置 [批次/序列号码]
     */
    @JsonProperty("lot_id")
    public void setLot_id(Integer  lot_id){
        this.lot_id = lot_id ;
        this.lot_idDirtyFlag = true ;
    }

     /**
     * 获取 [批次/序列号码]脏标记
     */
    @JsonIgnore
    public boolean getLot_idDirtyFlag(){
        return this.lot_idDirtyFlag ;
    }   

    /**
     * 获取 [批次/序列号码]
     */
    @JsonProperty("lot_id_text")
    public String getLot_id_text(){
        return this.lot_id_text ;
    }

    /**
     * 设置 [批次/序列号码]
     */
    @JsonProperty("lot_id_text")
    public void setLot_id_text(String  lot_id_text){
        this.lot_id_text = lot_id_text ;
        this.lot_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [批次/序列号码]脏标记
     */
    @JsonIgnore
    public boolean getLot_id_textDirtyFlag(){
        return this.lot_id_textDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("owner_id")
    public Integer getOwner_id(){
        return this.owner_id ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("owner_id")
    public void setOwner_id(Integer  owner_id){
        this.owner_id = owner_id ;
        this.owner_idDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getOwner_idDirtyFlag(){
        return this.owner_idDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("owner_id_text")
    public String getOwner_id_text(){
        return this.owner_id_text ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("owner_id_text")
    public void setOwner_id_text(String  owner_id_text){
        this.owner_id_text = owner_id_text ;
        this.owner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getOwner_id_textDirtyFlag(){
        return this.owner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [包裹]
     */
    @JsonProperty("package_id")
    public Integer getPackage_id(){
        return this.package_id ;
    }

    /**
     * 设置 [包裹]
     */
    @JsonProperty("package_id")
    public void setPackage_id(Integer  package_id){
        this.package_id = package_id ;
        this.package_idDirtyFlag = true ;
    }

     /**
     * 获取 [包裹]脏标记
     */
    @JsonIgnore
    public boolean getPackage_idDirtyFlag(){
        return this.package_idDirtyFlag ;
    }   

    /**
     * 获取 [包裹]
     */
    @JsonProperty("package_id_text")
    public String getPackage_id_text(){
        return this.package_id_text ;
    }

    /**
     * 设置 [包裹]
     */
    @JsonProperty("package_id_text")
    public void setPackage_id_text(String  package_id_text){
        this.package_id_text = package_id_text ;
        this.package_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [包裹]脏标记
     */
    @JsonIgnore
    public boolean getPackage_id_textDirtyFlag(){
        return this.package_id_textDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return this.product_id_text ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return this.product_id_textDirtyFlag ;
    }   

    /**
     * 获取 [产品模板]
     */
    @JsonProperty("product_tmpl_id")
    public Integer getProduct_tmpl_id(){
        return this.product_tmpl_id ;
    }

    /**
     * 设置 [产品模板]
     */
    @JsonProperty("product_tmpl_id")
    public void setProduct_tmpl_id(Integer  product_tmpl_id){
        this.product_tmpl_id = product_tmpl_id ;
        this.product_tmpl_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品模板]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_idDirtyFlag(){
        return this.product_tmpl_idDirtyFlag ;
    }   

    /**
     * 获取 [单位]
     */
    @JsonProperty("product_uom_id")
    public Integer getProduct_uom_id(){
        return this.product_uom_id ;
    }

    /**
     * 设置 [单位]
     */
    @JsonProperty("product_uom_id")
    public void setProduct_uom_id(Integer  product_uom_id){
        this.product_uom_id = product_uom_id ;
        this.product_uom_idDirtyFlag = true ;
    }

     /**
     * 获取 [单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_idDirtyFlag(){
        return this.product_uom_idDirtyFlag ;
    }   

    /**
     * 获取 [数量]
     */
    @JsonProperty("quantity")
    public Double getQuantity(){
        return this.quantity ;
    }

    /**
     * 设置 [数量]
     */
    @JsonProperty("quantity")
    public void setQuantity(Double  quantity){
        this.quantity = quantity ;
        this.quantityDirtyFlag = true ;
    }

     /**
     * 获取 [数量]脏标记
     */
    @JsonIgnore
    public boolean getQuantityDirtyFlag(){
        return this.quantityDirtyFlag ;
    }   

    /**
     * 获取 [预留数量]
     */
    @JsonProperty("reserved_quantity")
    public Double getReserved_quantity(){
        return this.reserved_quantity ;
    }

    /**
     * 设置 [预留数量]
     */
    @JsonProperty("reserved_quantity")
    public void setReserved_quantity(Double  reserved_quantity){
        this.reserved_quantity = reserved_quantity ;
        this.reserved_quantityDirtyFlag = true ;
    }

     /**
     * 获取 [预留数量]脏标记
     */
    @JsonIgnore
    public boolean getReserved_quantityDirtyFlag(){
        return this.reserved_quantityDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 1){
				this.setCompany_id_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("in_date") instanceof Boolean)&& map.get("in_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("in_date"));
   			this.setIn_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("location_id") instanceof Boolean)&& map.get("location_id")!=null){
			Object[] objs = (Object[])map.get("location_id");
			if(objs.length > 0){
				this.setLocation_id((Integer)objs[0]);
			}
		}
		if(!(map.get("location_id") instanceof Boolean)&& map.get("location_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("location_id");
			if(objs.length > 1){
				this.setLocation_id_text((String)objs[1]);
			}
		}
		if(!(map.get("lot_id") instanceof Boolean)&& map.get("lot_id")!=null){
			Object[] objs = (Object[])map.get("lot_id");
			if(objs.length > 0){
				this.setLot_id((Integer)objs[0]);
			}
		}
		if(!(map.get("lot_id") instanceof Boolean)&& map.get("lot_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("lot_id");
			if(objs.length > 1){
				this.setLot_id_text((String)objs[1]);
			}
		}
		if(!(map.get("owner_id") instanceof Boolean)&& map.get("owner_id")!=null){
			Object[] objs = (Object[])map.get("owner_id");
			if(objs.length > 0){
				this.setOwner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("owner_id") instanceof Boolean)&& map.get("owner_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("owner_id");
			if(objs.length > 1){
				this.setOwner_id_text((String)objs[1]);
			}
		}
		if(!(map.get("package_id") instanceof Boolean)&& map.get("package_id")!=null){
			Object[] objs = (Object[])map.get("package_id");
			if(objs.length > 0){
				this.setPackage_id((Integer)objs[0]);
			}
		}
		if(!(map.get("package_id") instanceof Boolean)&& map.get("package_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("package_id");
			if(objs.length > 1){
				this.setPackage_id_text((String)objs[1]);
			}
		}
		if(!(map.get("product_id") instanceof Boolean)&& map.get("product_id")!=null){
			Object[] objs = (Object[])map.get("product_id");
			if(objs.length > 0){
				this.setProduct_id((Integer)objs[0]);
			}
		}
		if(!(map.get("product_id") instanceof Boolean)&& map.get("product_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("product_id");
			if(objs.length > 1){
				this.setProduct_id_text((String)objs[1]);
			}
		}
		if(!(map.get("product_tmpl_id") instanceof Boolean)&& map.get("product_tmpl_id")!=null){
			Object[] objs = (Object[])map.get("product_tmpl_id");
			if(objs.length > 0){
				this.setProduct_tmpl_id((Integer)objs[0]);
			}
		}
		if(!(map.get("product_uom_id") instanceof Boolean)&& map.get("product_uom_id")!=null){
			Object[] objs = (Object[])map.get("product_uom_id");
			if(objs.length > 0){
				this.setProduct_uom_id((Integer)objs[0]);
			}
		}
		if(!(map.get("quantity") instanceof Boolean)&& map.get("quantity")!=null){
			this.setQuantity((Double)map.get("quantity"));
		}
		if(!(map.get("reserved_quantity") instanceof Boolean)&& map.get("reserved_quantity")!=null){
			this.setReserved_quantity((Double)map.get("reserved_quantity"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_id_text()!=null&&this.getCompany_id_textDirtyFlag()){
			//忽略文本外键company_id_text
		}else if(this.getCompany_id_textDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getIn_date()!=null&&this.getIn_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getIn_date());
			map.put("in_date",datetimeStr);
		}else if(this.getIn_dateDirtyFlag()){
			map.put("in_date",false);
		}
		if(this.getLocation_id()!=null&&this.getLocation_idDirtyFlag()){
			map.put("location_id",this.getLocation_id());
		}else if(this.getLocation_idDirtyFlag()){
			map.put("location_id",false);
		}
		if(this.getLocation_id_text()!=null&&this.getLocation_id_textDirtyFlag()){
			//忽略文本外键location_id_text
		}else if(this.getLocation_id_textDirtyFlag()){
			map.put("location_id",false);
		}
		if(this.getLot_id()!=null&&this.getLot_idDirtyFlag()){
			map.put("lot_id",this.getLot_id());
		}else if(this.getLot_idDirtyFlag()){
			map.put("lot_id",false);
		}
		if(this.getLot_id_text()!=null&&this.getLot_id_textDirtyFlag()){
			//忽略文本外键lot_id_text
		}else if(this.getLot_id_textDirtyFlag()){
			map.put("lot_id",false);
		}
		if(this.getOwner_id()!=null&&this.getOwner_idDirtyFlag()){
			map.put("owner_id",this.getOwner_id());
		}else if(this.getOwner_idDirtyFlag()){
			map.put("owner_id",false);
		}
		if(this.getOwner_id_text()!=null&&this.getOwner_id_textDirtyFlag()){
			//忽略文本外键owner_id_text
		}else if(this.getOwner_id_textDirtyFlag()){
			map.put("owner_id",false);
		}
		if(this.getPackage_id()!=null&&this.getPackage_idDirtyFlag()){
			map.put("package_id",this.getPackage_id());
		}else if(this.getPackage_idDirtyFlag()){
			map.put("package_id",false);
		}
		if(this.getPackage_id_text()!=null&&this.getPackage_id_textDirtyFlag()){
			//忽略文本外键package_id_text
		}else if(this.getPackage_id_textDirtyFlag()){
			map.put("package_id",false);
		}
		if(this.getProduct_id()!=null&&this.getProduct_idDirtyFlag()){
			map.put("product_id",this.getProduct_id());
		}else if(this.getProduct_idDirtyFlag()){
			map.put("product_id",false);
		}
		if(this.getProduct_id_text()!=null&&this.getProduct_id_textDirtyFlag()){
			//忽略文本外键product_id_text
		}else if(this.getProduct_id_textDirtyFlag()){
			map.put("product_id",false);
		}
		if(this.getProduct_tmpl_id()!=null&&this.getProduct_tmpl_idDirtyFlag()){
			map.put("product_tmpl_id",this.getProduct_tmpl_id());
		}else if(this.getProduct_tmpl_idDirtyFlag()){
			map.put("product_tmpl_id",false);
		}
		if(this.getProduct_uom_id()!=null&&this.getProduct_uom_idDirtyFlag()){
			map.put("product_uom_id",this.getProduct_uom_id());
		}else if(this.getProduct_uom_idDirtyFlag()){
			map.put("product_uom_id",false);
		}
		if(this.getQuantity()!=null&&this.getQuantityDirtyFlag()){
			map.put("quantity",this.getQuantity());
		}else if(this.getQuantityDirtyFlag()){
			map.put("quantity",false);
		}
		if(this.getReserved_quantity()!=null&&this.getReserved_quantityDirtyFlag()){
			map.put("reserved_quantity",this.getReserved_quantity());
		}else if(this.getReserved_quantityDirtyFlag()){
			map.put("reserved_quantity",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
