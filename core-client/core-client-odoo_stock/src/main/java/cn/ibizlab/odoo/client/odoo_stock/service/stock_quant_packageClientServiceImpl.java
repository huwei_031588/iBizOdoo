package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_quant_package;
import cn.ibizlab.odoo.core.client.service.Istock_quant_packageClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_quant_packageImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_quant_packageOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_quant_packageOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_quant_package] 服务对象接口
 */
@Service
public class stock_quant_packageClientServiceImpl implements Istock_quant_packageClientService {
    @Autowired
    private  Istock_quant_packageOdooClient  stock_quant_packageOdooClient;

    public Istock_quant_package createModel() {		
		return new stock_quant_packageImpl();
	}


        public void removeBatch(List<Istock_quant_package> stock_quant_packages){
            
        }
        
        public void updateBatch(List<Istock_quant_package> stock_quant_packages){
            
        }
        
        public void update(Istock_quant_package stock_quant_package){
this.stock_quant_packageOdooClient.update(stock_quant_package) ;
        }
        
        public void get(Istock_quant_package stock_quant_package){
            this.stock_quant_packageOdooClient.get(stock_quant_package) ;
        }
        
        public void create(Istock_quant_package stock_quant_package){
this.stock_quant_packageOdooClient.create(stock_quant_package) ;
        }
        
        public Page<Istock_quant_package> search(SearchContext context){
            return this.stock_quant_packageOdooClient.search(context) ;
        }
        
        public void remove(Istock_quant_package stock_quant_package){
this.stock_quant_packageOdooClient.remove(stock_quant_package) ;
        }
        
        public void createBatch(List<Istock_quant_package> stock_quant_packages){
            
        }
        
        public Page<Istock_quant_package> select(SearchContext context){
            return null ;
        }
        

}

