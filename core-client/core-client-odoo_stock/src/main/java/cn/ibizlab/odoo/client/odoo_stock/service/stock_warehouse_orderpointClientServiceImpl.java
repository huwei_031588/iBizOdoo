package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_warehouse_orderpoint;
import cn.ibizlab.odoo.core.client.service.Istock_warehouse_orderpointClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_warehouse_orderpointImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_warehouse_orderpointOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_warehouse_orderpointOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_warehouse_orderpoint] 服务对象接口
 */
@Service
public class stock_warehouse_orderpointClientServiceImpl implements Istock_warehouse_orderpointClientService {
    @Autowired
    private  Istock_warehouse_orderpointOdooClient  stock_warehouse_orderpointOdooClient;

    public Istock_warehouse_orderpoint createModel() {		
		return new stock_warehouse_orderpointImpl();
	}


        public void createBatch(List<Istock_warehouse_orderpoint> stock_warehouse_orderpoints){
            
        }
        
        public void remove(Istock_warehouse_orderpoint stock_warehouse_orderpoint){
this.stock_warehouse_orderpointOdooClient.remove(stock_warehouse_orderpoint) ;
        }
        
        public void updateBatch(List<Istock_warehouse_orderpoint> stock_warehouse_orderpoints){
            
        }
        
        public void get(Istock_warehouse_orderpoint stock_warehouse_orderpoint){
            this.stock_warehouse_orderpointOdooClient.get(stock_warehouse_orderpoint) ;
        }
        
        public Page<Istock_warehouse_orderpoint> search(SearchContext context){
            return this.stock_warehouse_orderpointOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Istock_warehouse_orderpoint> stock_warehouse_orderpoints){
            
        }
        
        public void update(Istock_warehouse_orderpoint stock_warehouse_orderpoint){
this.stock_warehouse_orderpointOdooClient.update(stock_warehouse_orderpoint) ;
        }
        
        public void create(Istock_warehouse_orderpoint stock_warehouse_orderpoint){
this.stock_warehouse_orderpointOdooClient.create(stock_warehouse_orderpoint) ;
        }
        
        public Page<Istock_warehouse_orderpoint> select(SearchContext context){
            return null ;
        }
        

}

