package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_warn_insufficient_qty_scrap;
import cn.ibizlab.odoo.core.client.service.Istock_warn_insufficient_qty_scrapClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_warn_insufficient_qty_scrapImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_warn_insufficient_qty_scrapOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_warn_insufficient_qty_scrapOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_warn_insufficient_qty_scrap] 服务对象接口
 */
@Service
public class stock_warn_insufficient_qty_scrapClientServiceImpl implements Istock_warn_insufficient_qty_scrapClientService {
    @Autowired
    private  Istock_warn_insufficient_qty_scrapOdooClient  stock_warn_insufficient_qty_scrapOdooClient;

    public Istock_warn_insufficient_qty_scrap createModel() {		
		return new stock_warn_insufficient_qty_scrapImpl();
	}


        public Page<Istock_warn_insufficient_qty_scrap> search(SearchContext context){
            return this.stock_warn_insufficient_qty_scrapOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Istock_warn_insufficient_qty_scrap> stock_warn_insufficient_qty_scraps){
            
        }
        
        public void create(Istock_warn_insufficient_qty_scrap stock_warn_insufficient_qty_scrap){
this.stock_warn_insufficient_qty_scrapOdooClient.create(stock_warn_insufficient_qty_scrap) ;
        }
        
        public void createBatch(List<Istock_warn_insufficient_qty_scrap> stock_warn_insufficient_qty_scraps){
            
        }
        
        public void remove(Istock_warn_insufficient_qty_scrap stock_warn_insufficient_qty_scrap){
this.stock_warn_insufficient_qty_scrapOdooClient.remove(stock_warn_insufficient_qty_scrap) ;
        }
        
        public void removeBatch(List<Istock_warn_insufficient_qty_scrap> stock_warn_insufficient_qty_scraps){
            
        }
        
        public void update(Istock_warn_insufficient_qty_scrap stock_warn_insufficient_qty_scrap){
this.stock_warn_insufficient_qty_scrapOdooClient.update(stock_warn_insufficient_qty_scrap) ;
        }
        
        public void get(Istock_warn_insufficient_qty_scrap stock_warn_insufficient_qty_scrap){
            this.stock_warn_insufficient_qty_scrapOdooClient.get(stock_warn_insufficient_qty_scrap) ;
        }
        
        public Page<Istock_warn_insufficient_qty_scrap> select(SearchContext context){
            return null ;
        }
        

}

