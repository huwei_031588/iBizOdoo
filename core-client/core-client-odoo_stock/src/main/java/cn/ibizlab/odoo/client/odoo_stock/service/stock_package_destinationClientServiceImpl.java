package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_package_destination;
import cn.ibizlab.odoo.core.client.service.Istock_package_destinationClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_package_destinationImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_package_destinationOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_package_destinationOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_package_destination] 服务对象接口
 */
@Service
public class stock_package_destinationClientServiceImpl implements Istock_package_destinationClientService {
    @Autowired
    private  Istock_package_destinationOdooClient  stock_package_destinationOdooClient;

    public Istock_package_destination createModel() {		
		return new stock_package_destinationImpl();
	}


        public void update(Istock_package_destination stock_package_destination){
this.stock_package_destinationOdooClient.update(stock_package_destination) ;
        }
        
        public Page<Istock_package_destination> search(SearchContext context){
            return this.stock_package_destinationOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Istock_package_destination> stock_package_destinations){
            
        }
        
        public void get(Istock_package_destination stock_package_destination){
            this.stock_package_destinationOdooClient.get(stock_package_destination) ;
        }
        
        public void create(Istock_package_destination stock_package_destination){
this.stock_package_destinationOdooClient.create(stock_package_destination) ;
        }
        
        public void remove(Istock_package_destination stock_package_destination){
this.stock_package_destinationOdooClient.remove(stock_package_destination) ;
        }
        
        public void updateBatch(List<Istock_package_destination> stock_package_destinations){
            
        }
        
        public void createBatch(List<Istock_package_destination> stock_package_destinations){
            
        }
        
        public Page<Istock_package_destination> select(SearchContext context){
            return null ;
        }
        

}

