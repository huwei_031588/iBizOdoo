package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_change_product_qty;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_change_product_qty] 服务对象客户端接口
 */
public interface Istock_change_product_qtyOdooClient {
    
        public void createBatch(Istock_change_product_qty stock_change_product_qty);

        public void removeBatch(Istock_change_product_qty stock_change_product_qty);

        public void update(Istock_change_product_qty stock_change_product_qty);

        public void remove(Istock_change_product_qty stock_change_product_qty);

        public void updateBatch(Istock_change_product_qty stock_change_product_qty);

        public void get(Istock_change_product_qty stock_change_product_qty);

        public Page<Istock_change_product_qty> search(SearchContext context);

        public void create(Istock_change_product_qty stock_change_product_qty);

        public List<Istock_change_product_qty> select();


}