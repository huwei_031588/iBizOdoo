package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_warn_insufficient_qty_repair;
import cn.ibizlab.odoo.core.client.service.Istock_warn_insufficient_qty_repairClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_warn_insufficient_qty_repairImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_warn_insufficient_qty_repairOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_warn_insufficient_qty_repairOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_warn_insufficient_qty_repair] 服务对象接口
 */
@Service
public class stock_warn_insufficient_qty_repairClientServiceImpl implements Istock_warn_insufficient_qty_repairClientService {
    @Autowired
    private  Istock_warn_insufficient_qty_repairOdooClient  stock_warn_insufficient_qty_repairOdooClient;

    public Istock_warn_insufficient_qty_repair createModel() {		
		return new stock_warn_insufficient_qty_repairImpl();
	}


        public void get(Istock_warn_insufficient_qty_repair stock_warn_insufficient_qty_repair){
            this.stock_warn_insufficient_qty_repairOdooClient.get(stock_warn_insufficient_qty_repair) ;
        }
        
        public void create(Istock_warn_insufficient_qty_repair stock_warn_insufficient_qty_repair){
this.stock_warn_insufficient_qty_repairOdooClient.create(stock_warn_insufficient_qty_repair) ;
        }
        
        public Page<Istock_warn_insufficient_qty_repair> search(SearchContext context){
            return this.stock_warn_insufficient_qty_repairOdooClient.search(context) ;
        }
        
        public void createBatch(List<Istock_warn_insufficient_qty_repair> stock_warn_insufficient_qty_repairs){
            
        }
        
        public void removeBatch(List<Istock_warn_insufficient_qty_repair> stock_warn_insufficient_qty_repairs){
            
        }
        
        public void updateBatch(List<Istock_warn_insufficient_qty_repair> stock_warn_insufficient_qty_repairs){
            
        }
        
        public void remove(Istock_warn_insufficient_qty_repair stock_warn_insufficient_qty_repair){
this.stock_warn_insufficient_qty_repairOdooClient.remove(stock_warn_insufficient_qty_repair) ;
        }
        
        public void update(Istock_warn_insufficient_qty_repair stock_warn_insufficient_qty_repair){
this.stock_warn_insufficient_qty_repairOdooClient.update(stock_warn_insufficient_qty_repair) ;
        }
        
        public Page<Istock_warn_insufficient_qty_repair> select(SearchContext context){
            return null ;
        }
        

}

