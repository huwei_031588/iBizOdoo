package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_picking;
import cn.ibizlab.odoo.core.client.service.Istock_pickingClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_pickingImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_pickingOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_pickingOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_picking] 服务对象接口
 */
@Service
public class stock_pickingClientServiceImpl implements Istock_pickingClientService {
    @Autowired
    private  Istock_pickingOdooClient  stock_pickingOdooClient;

    public Istock_picking createModel() {		
		return new stock_pickingImpl();
	}


        public void remove(Istock_picking stock_picking){
this.stock_pickingOdooClient.remove(stock_picking) ;
        }
        
        public void create(Istock_picking stock_picking){
this.stock_pickingOdooClient.create(stock_picking) ;
        }
        
        public void updateBatch(List<Istock_picking> stock_pickings){
            
        }
        
        public void get(Istock_picking stock_picking){
            this.stock_pickingOdooClient.get(stock_picking) ;
        }
        
        public void removeBatch(List<Istock_picking> stock_pickings){
            
        }
        
        public void update(Istock_picking stock_picking){
this.stock_pickingOdooClient.update(stock_picking) ;
        }
        
        public void createBatch(List<Istock_picking> stock_pickings){
            
        }
        
        public Page<Istock_picking> search(SearchContext context){
            return this.stock_pickingOdooClient.search(context) ;
        }
        
        public Page<Istock_picking> select(SearchContext context){
            return null ;
        }
        

}

