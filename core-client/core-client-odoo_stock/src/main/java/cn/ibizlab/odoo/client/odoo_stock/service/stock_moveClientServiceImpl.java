package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_move;
import cn.ibizlab.odoo.core.client.service.Istock_moveClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_moveImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_moveOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_moveOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_move] 服务对象接口
 */
@Service
public class stock_moveClientServiceImpl implements Istock_moveClientService {
    @Autowired
    private  Istock_moveOdooClient  stock_moveOdooClient;

    public Istock_move createModel() {		
		return new stock_moveImpl();
	}


        public void createBatch(List<Istock_move> stock_moves){
            
        }
        
        public void updateBatch(List<Istock_move> stock_moves){
            
        }
        
        public void get(Istock_move stock_move){
            this.stock_moveOdooClient.get(stock_move) ;
        }
        
        public void create(Istock_move stock_move){
this.stock_moveOdooClient.create(stock_move) ;
        }
        
        public void update(Istock_move stock_move){
this.stock_moveOdooClient.update(stock_move) ;
        }
        
        public Page<Istock_move> search(SearchContext context){
            return this.stock_moveOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Istock_move> stock_moves){
            
        }
        
        public void remove(Istock_move stock_move){
this.stock_moveOdooClient.remove(stock_move) ;
        }
        
        public Page<Istock_move> select(SearchContext context){
            return null ;
        }
        

}

