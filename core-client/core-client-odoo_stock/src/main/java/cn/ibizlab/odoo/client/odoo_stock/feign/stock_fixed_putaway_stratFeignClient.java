package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_fixed_putaway_strat;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_fixed_putaway_stratImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_fixed_putaway_strat] 服务对象接口
 */
public interface stock_fixed_putaway_stratFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_fixed_putaway_strats/updatebatch")
    public stock_fixed_putaway_stratImpl updateBatch(@RequestBody List<stock_fixed_putaway_stratImpl> stock_fixed_putaway_strats);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_fixed_putaway_strats/removebatch")
    public stock_fixed_putaway_stratImpl removeBatch(@RequestBody List<stock_fixed_putaway_stratImpl> stock_fixed_putaway_strats);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_fixed_putaway_strats/{id}")
    public stock_fixed_putaway_stratImpl update(@PathVariable("id") Integer id,@RequestBody stock_fixed_putaway_stratImpl stock_fixed_putaway_strat);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_fixed_putaway_strats")
    public stock_fixed_putaway_stratImpl create(@RequestBody stock_fixed_putaway_stratImpl stock_fixed_putaway_strat);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_fixed_putaway_strats/{id}")
    public stock_fixed_putaway_stratImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_fixed_putaway_strats/createbatch")
    public stock_fixed_putaway_stratImpl createBatch(@RequestBody List<stock_fixed_putaway_stratImpl> stock_fixed_putaway_strats);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_fixed_putaway_strats/search")
    public Page<stock_fixed_putaway_stratImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_fixed_putaway_strats/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_fixed_putaway_strats/select")
    public Page<stock_fixed_putaway_stratImpl> select();



}
