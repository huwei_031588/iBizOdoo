package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_traceability_report;
import cn.ibizlab.odoo.core.client.service.Istock_traceability_reportClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_traceability_reportImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_traceability_reportOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_traceability_reportOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_traceability_report] 服务对象接口
 */
@Service
public class stock_traceability_reportClientServiceImpl implements Istock_traceability_reportClientService {
    @Autowired
    private  Istock_traceability_reportOdooClient  stock_traceability_reportOdooClient;

    public Istock_traceability_report createModel() {		
		return new stock_traceability_reportImpl();
	}


        public Page<Istock_traceability_report> search(SearchContext context){
            return this.stock_traceability_reportOdooClient.search(context) ;
        }
        
        public void create(Istock_traceability_report stock_traceability_report){
this.stock_traceability_reportOdooClient.create(stock_traceability_report) ;
        }
        
        public void update(Istock_traceability_report stock_traceability_report){
this.stock_traceability_reportOdooClient.update(stock_traceability_report) ;
        }
        
        public void removeBatch(List<Istock_traceability_report> stock_traceability_reports){
            
        }
        
        public void createBatch(List<Istock_traceability_report> stock_traceability_reports){
            
        }
        
        public void remove(Istock_traceability_report stock_traceability_report){
this.stock_traceability_reportOdooClient.remove(stock_traceability_report) ;
        }
        
        public void updateBatch(List<Istock_traceability_report> stock_traceability_reports){
            
        }
        
        public void get(Istock_traceability_report stock_traceability_report){
            this.stock_traceability_reportOdooClient.get(stock_traceability_report) ;
        }
        
        public Page<Istock_traceability_report> select(SearchContext context){
            return null ;
        }
        

}

