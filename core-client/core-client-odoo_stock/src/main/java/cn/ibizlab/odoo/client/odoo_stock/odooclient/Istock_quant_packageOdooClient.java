package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_quant_package;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_quant_package] 服务对象客户端接口
 */
public interface Istock_quant_packageOdooClient {
    
        public void removeBatch(Istock_quant_package stock_quant_package);

        public void updateBatch(Istock_quant_package stock_quant_package);

        public void update(Istock_quant_package stock_quant_package);

        public void get(Istock_quant_package stock_quant_package);

        public void create(Istock_quant_package stock_quant_package);

        public Page<Istock_quant_package> search(SearchContext context);

        public void remove(Istock_quant_package stock_quant_package);

        public void createBatch(Istock_quant_package stock_quant_package);

        public List<Istock_quant_package> select();


}