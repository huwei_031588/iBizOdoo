package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_scheduler_compute;
import cn.ibizlab.odoo.core.client.service.Istock_scheduler_computeClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_scheduler_computeImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_scheduler_computeOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_scheduler_computeOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_scheduler_compute] 服务对象接口
 */
@Service
public class stock_scheduler_computeClientServiceImpl implements Istock_scheduler_computeClientService {
    @Autowired
    private  Istock_scheduler_computeOdooClient  stock_scheduler_computeOdooClient;

    public Istock_scheduler_compute createModel() {		
		return new stock_scheduler_computeImpl();
	}


        public void updateBatch(List<Istock_scheduler_compute> stock_scheduler_computes){
            
        }
        
        public void removeBatch(List<Istock_scheduler_compute> stock_scheduler_computes){
            
        }
        
        public void createBatch(List<Istock_scheduler_compute> stock_scheduler_computes){
            
        }
        
        public void create(Istock_scheduler_compute stock_scheduler_compute){
this.stock_scheduler_computeOdooClient.create(stock_scheduler_compute) ;
        }
        
        public void get(Istock_scheduler_compute stock_scheduler_compute){
            this.stock_scheduler_computeOdooClient.get(stock_scheduler_compute) ;
        }
        
        public Page<Istock_scheduler_compute> search(SearchContext context){
            return this.stock_scheduler_computeOdooClient.search(context) ;
        }
        
        public void remove(Istock_scheduler_compute stock_scheduler_compute){
this.stock_scheduler_computeOdooClient.remove(stock_scheduler_compute) ;
        }
        
        public void update(Istock_scheduler_compute stock_scheduler_compute){
this.stock_scheduler_computeOdooClient.update(stock_scheduler_compute) ;
        }
        
        public Page<Istock_scheduler_compute> select(SearchContext context){
            return null ;
        }
        

}

