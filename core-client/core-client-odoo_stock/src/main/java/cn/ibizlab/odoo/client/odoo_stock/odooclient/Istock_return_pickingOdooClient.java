package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_return_picking;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_return_picking] 服务对象客户端接口
 */
public interface Istock_return_pickingOdooClient {
    
        public void updateBatch(Istock_return_picking stock_return_picking);

        public void remove(Istock_return_picking stock_return_picking);

        public Page<Istock_return_picking> search(SearchContext context);

        public void removeBatch(Istock_return_picking stock_return_picking);

        public void createBatch(Istock_return_picking stock_return_picking);

        public void create(Istock_return_picking stock_return_picking);

        public void update(Istock_return_picking stock_return_picking);

        public void get(Istock_return_picking stock_return_picking);

        public List<Istock_return_picking> select();


}