package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_move;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_move] 服务对象客户端接口
 */
public interface Istock_moveOdooClient {
    
        public void createBatch(Istock_move stock_move);

        public void updateBatch(Istock_move stock_move);

        public void get(Istock_move stock_move);

        public void create(Istock_move stock_move);

        public void update(Istock_move stock_move);

        public Page<Istock_move> search(SearchContext context);

        public void removeBatch(Istock_move stock_move);

        public void remove(Istock_move stock_move);

        public List<Istock_move> select();


}