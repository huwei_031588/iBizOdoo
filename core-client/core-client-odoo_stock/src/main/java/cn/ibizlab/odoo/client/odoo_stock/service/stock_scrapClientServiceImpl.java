package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_scrap;
import cn.ibizlab.odoo.core.client.service.Istock_scrapClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_scrapImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_scrapOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_scrapOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_scrap] 服务对象接口
 */
@Service
public class stock_scrapClientServiceImpl implements Istock_scrapClientService {
    @Autowired
    private  Istock_scrapOdooClient  stock_scrapOdooClient;

    public Istock_scrap createModel() {		
		return new stock_scrapImpl();
	}


        public void updateBatch(List<Istock_scrap> stock_scraps){
            
        }
        
        public void createBatch(List<Istock_scrap> stock_scraps){
            
        }
        
        public void get(Istock_scrap stock_scrap){
            this.stock_scrapOdooClient.get(stock_scrap) ;
        }
        
        public Page<Istock_scrap> search(SearchContext context){
            return this.stock_scrapOdooClient.search(context) ;
        }
        
        public void remove(Istock_scrap stock_scrap){
this.stock_scrapOdooClient.remove(stock_scrap) ;
        }
        
        public void create(Istock_scrap stock_scrap){
this.stock_scrapOdooClient.create(stock_scrap) ;
        }
        
        public void removeBatch(List<Istock_scrap> stock_scraps){
            
        }
        
        public void update(Istock_scrap stock_scrap){
this.stock_scrapOdooClient.update(stock_scrap) ;
        }
        
        public Page<Istock_scrap> select(SearchContext context){
            return null ;
        }
        

}

