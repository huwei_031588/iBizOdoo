package cn.ibizlab.odoo.client.odoo_stock.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Istock_warehouse;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[stock_warehouse] 对象
 */
public class stock_warehouseImpl implements Istock_warehouse,Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 购买规则
     */
    public Integer buy_pull_id;

    @JsonIgnore
    public boolean buy_pull_idDirtyFlag;
    
    /**
     * 购买规则
     */
    public String buy_pull_id_text;

    @JsonIgnore
    public boolean buy_pull_id_textDirtyFlag;
    
    /**
     * 购买补给
     */
    public String buy_to_resupply;

    @JsonIgnore
    public boolean buy_to_resupplyDirtyFlag;
    
    /**
     * 缩写
     */
    public String code;

    @JsonIgnore
    public boolean codeDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 越库路线
     */
    public Integer crossdock_route_id;

    @JsonIgnore
    public boolean crossdock_route_idDirtyFlag;
    
    /**
     * 越库路线
     */
    public String crossdock_route_id_text;

    @JsonIgnore
    public boolean crossdock_route_id_textDirtyFlag;
    
    /**
     * 交货路线
     */
    public Integer delivery_route_id;

    @JsonIgnore
    public boolean delivery_route_idDirtyFlag;
    
    /**
     * 交货路线
     */
    public String delivery_route_id_text;

    @JsonIgnore
    public boolean delivery_route_id_textDirtyFlag;
    
    /**
     * 出向运输
     */
    public String delivery_steps;

    @JsonIgnore
    public boolean delivery_stepsDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 内部类型
     */
    public Integer int_type_id;

    @JsonIgnore
    public boolean int_type_idDirtyFlag;
    
    /**
     * 内部类型
     */
    public String int_type_id_text;

    @JsonIgnore
    public boolean int_type_id_textDirtyFlag;
    
    /**
     * 入库类型
     */
    public Integer in_type_id;

    @JsonIgnore
    public boolean in_type_idDirtyFlag;
    
    /**
     * 入库类型
     */
    public String in_type_id_text;

    @JsonIgnore
    public boolean in_type_id_textDirtyFlag;
    
    /**
     * 库存位置
     */
    public Integer lot_stock_id;

    @JsonIgnore
    public boolean lot_stock_idDirtyFlag;
    
    /**
     * 库存位置
     */
    public String lot_stock_id_text;

    @JsonIgnore
    public boolean lot_stock_id_textDirtyFlag;
    
    /**
     * 制造规则
     */
    public Integer manufacture_pull_id;

    @JsonIgnore
    public boolean manufacture_pull_idDirtyFlag;
    
    /**
     * 制造规则
     */
    public String manufacture_pull_id_text;

    @JsonIgnore
    public boolean manufacture_pull_id_textDirtyFlag;
    
    /**
     * 制造
     */
    public String manufacture_steps;

    @JsonIgnore
    public boolean manufacture_stepsDirtyFlag;
    
    /**
     * 制造补给
     */
    public String manufacture_to_resupply;

    @JsonIgnore
    public boolean manufacture_to_resupplyDirtyFlag;
    
    /**
     * 生产操作类型
     */
    public Integer manu_type_id;

    @JsonIgnore
    public boolean manu_type_idDirtyFlag;
    
    /**
     * 生产操作类型
     */
    public String manu_type_id_text;

    @JsonIgnore
    public boolean manu_type_id_textDirtyFlag;
    
    /**
     * MTO规则
     */
    public Integer mto_pull_id;

    @JsonIgnore
    public boolean mto_pull_idDirtyFlag;
    
    /**
     * MTO规则
     */
    public String mto_pull_id_text;

    @JsonIgnore
    public boolean mto_pull_id_textDirtyFlag;
    
    /**
     * 仓库
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 出库类型
     */
    public Integer out_type_id;

    @JsonIgnore
    public boolean out_type_idDirtyFlag;
    
    /**
     * 出库类型
     */
    public String out_type_id_text;

    @JsonIgnore
    public boolean out_type_id_textDirtyFlag;
    
    /**
     * 包裹类型
     */
    public Integer pack_type_id;

    @JsonIgnore
    public boolean pack_type_idDirtyFlag;
    
    /**
     * 包裹类型
     */
    public String pack_type_id_text;

    @JsonIgnore
    public boolean pack_type_id_textDirtyFlag;
    
    /**
     * 地址
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 地址
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 在制造位置前拣货
     */
    public Integer pbm_loc_id;

    @JsonIgnore
    public boolean pbm_loc_idDirtyFlag;
    
    /**
     * 在制造位置前拣货
     */
    public String pbm_loc_id_text;

    @JsonIgnore
    public boolean pbm_loc_id_textDirtyFlag;
    
    /**
     * 在制造（按订单补货）MTO规则之前拣货
     */
    public Integer pbm_mto_pull_id;

    @JsonIgnore
    public boolean pbm_mto_pull_idDirtyFlag;
    
    /**
     * 在制造（按订单补货）MTO规则之前拣货
     */
    public String pbm_mto_pull_id_text;

    @JsonIgnore
    public boolean pbm_mto_pull_id_textDirtyFlag;
    
    /**
     * 在制造路线前拣货
     */
    public Integer pbm_route_id;

    @JsonIgnore
    public boolean pbm_route_idDirtyFlag;
    
    /**
     * 在制造路线前拣货
     */
    public String pbm_route_id_text;

    @JsonIgnore
    public boolean pbm_route_id_textDirtyFlag;
    
    /**
     * 在制造作业类型前拣货
     */
    public Integer pbm_type_id;

    @JsonIgnore
    public boolean pbm_type_idDirtyFlag;
    
    /**
     * 在制造作业类型前拣货
     */
    public String pbm_type_id_text;

    @JsonIgnore
    public boolean pbm_type_id_textDirtyFlag;
    
    /**
     * 分拣类型
     */
    public Integer pick_type_id;

    @JsonIgnore
    public boolean pick_type_idDirtyFlag;
    
    /**
     * 分拣类型
     */
    public String pick_type_id_text;

    @JsonIgnore
    public boolean pick_type_id_textDirtyFlag;
    
    /**
     * 收货路线
     */
    public Integer reception_route_id;

    @JsonIgnore
    public boolean reception_route_idDirtyFlag;
    
    /**
     * 收货路线
     */
    public String reception_route_id_text;

    @JsonIgnore
    public boolean reception_route_id_textDirtyFlag;
    
    /**
     * 入库
     */
    public String reception_steps;

    @JsonIgnore
    public boolean reception_stepsDirtyFlag;
    
    /**
     * 补充路线
     */
    public String resupply_route_ids;

    @JsonIgnore
    public boolean resupply_route_idsDirtyFlag;
    
    /**
     * 补给 自
     */
    public String resupply_wh_ids;

    @JsonIgnore
    public boolean resupply_wh_idsDirtyFlag;
    
    /**
     * 路线
     */
    public String route_ids;

    @JsonIgnore
    public boolean route_idsDirtyFlag;
    
    /**
     * 制造地点后的库存
     */
    public Integer sam_loc_id;

    @JsonIgnore
    public boolean sam_loc_idDirtyFlag;
    
    /**
     * 制造地点后的库存
     */
    public String sam_loc_id_text;

    @JsonIgnore
    public boolean sam_loc_id_textDirtyFlag;
    
    /**
     * 制造规则后的库存
     */
    public Integer sam_rule_id;

    @JsonIgnore
    public boolean sam_rule_idDirtyFlag;
    
    /**
     * 制造规则后的库存
     */
    public String sam_rule_id_text;

    @JsonIgnore
    public boolean sam_rule_id_textDirtyFlag;
    
    /**
     * 制造运营类型后的库存
     */
    public Integer sam_type_id;

    @JsonIgnore
    public boolean sam_type_idDirtyFlag;
    
    /**
     * 制造运营类型后的库存
     */
    public String sam_type_id_text;

    @JsonIgnore
    public boolean sam_type_id_textDirtyFlag;
    
    /**
     * 显示补给
     */
    public String show_resupply;

    @JsonIgnore
    public boolean show_resupplyDirtyFlag;
    
    /**
     * 视图位置
     */
    public Integer view_location_id;

    @JsonIgnore
    public boolean view_location_idDirtyFlag;
    
    /**
     * 视图位置
     */
    public String view_location_id_text;

    @JsonIgnore
    public boolean view_location_id_textDirtyFlag;
    
    /**
     * 仓库个数
     */
    public Integer warehouse_count;

    @JsonIgnore
    public boolean warehouse_countDirtyFlag;
    
    /**
     * 进货位置
     */
    public Integer wh_input_stock_loc_id;

    @JsonIgnore
    public boolean wh_input_stock_loc_idDirtyFlag;
    
    /**
     * 进货位置
     */
    public String wh_input_stock_loc_id_text;

    @JsonIgnore
    public boolean wh_input_stock_loc_id_textDirtyFlag;
    
    /**
     * 出货位置
     */
    public Integer wh_output_stock_loc_id;

    @JsonIgnore
    public boolean wh_output_stock_loc_idDirtyFlag;
    
    /**
     * 出货位置
     */
    public String wh_output_stock_loc_id_text;

    @JsonIgnore
    public boolean wh_output_stock_loc_id_textDirtyFlag;
    
    /**
     * 打包位置
     */
    public Integer wh_pack_stock_loc_id;

    @JsonIgnore
    public boolean wh_pack_stock_loc_idDirtyFlag;
    
    /**
     * 打包位置
     */
    public String wh_pack_stock_loc_id_text;

    @JsonIgnore
    public boolean wh_pack_stock_loc_id_textDirtyFlag;
    
    /**
     * 质量管理位置
     */
    public Integer wh_qc_stock_loc_id;

    @JsonIgnore
    public boolean wh_qc_stock_loc_idDirtyFlag;
    
    /**
     * 质量管理位置
     */
    public String wh_qc_stock_loc_id_text;

    @JsonIgnore
    public boolean wh_qc_stock_loc_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [购买规则]
     */
    @JsonProperty("buy_pull_id")
    public Integer getBuy_pull_id(){
        return this.buy_pull_id ;
    }

    /**
     * 设置 [购买规则]
     */
    @JsonProperty("buy_pull_id")
    public void setBuy_pull_id(Integer  buy_pull_id){
        this.buy_pull_id = buy_pull_id ;
        this.buy_pull_idDirtyFlag = true ;
    }

     /**
     * 获取 [购买规则]脏标记
     */
    @JsonIgnore
    public boolean getBuy_pull_idDirtyFlag(){
        return this.buy_pull_idDirtyFlag ;
    }   

    /**
     * 获取 [购买规则]
     */
    @JsonProperty("buy_pull_id_text")
    public String getBuy_pull_id_text(){
        return this.buy_pull_id_text ;
    }

    /**
     * 设置 [购买规则]
     */
    @JsonProperty("buy_pull_id_text")
    public void setBuy_pull_id_text(String  buy_pull_id_text){
        this.buy_pull_id_text = buy_pull_id_text ;
        this.buy_pull_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [购买规则]脏标记
     */
    @JsonIgnore
    public boolean getBuy_pull_id_textDirtyFlag(){
        return this.buy_pull_id_textDirtyFlag ;
    }   

    /**
     * 获取 [购买补给]
     */
    @JsonProperty("buy_to_resupply")
    public String getBuy_to_resupply(){
        return this.buy_to_resupply ;
    }

    /**
     * 设置 [购买补给]
     */
    @JsonProperty("buy_to_resupply")
    public void setBuy_to_resupply(String  buy_to_resupply){
        this.buy_to_resupply = buy_to_resupply ;
        this.buy_to_resupplyDirtyFlag = true ;
    }

     /**
     * 获取 [购买补给]脏标记
     */
    @JsonIgnore
    public boolean getBuy_to_resupplyDirtyFlag(){
        return this.buy_to_resupplyDirtyFlag ;
    }   

    /**
     * 获取 [缩写]
     */
    @JsonProperty("code")
    public String getCode(){
        return this.code ;
    }

    /**
     * 设置 [缩写]
     */
    @JsonProperty("code")
    public void setCode(String  code){
        this.code = code ;
        this.codeDirtyFlag = true ;
    }

     /**
     * 获取 [缩写]脏标记
     */
    @JsonIgnore
    public boolean getCodeDirtyFlag(){
        return this.codeDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [越库路线]
     */
    @JsonProperty("crossdock_route_id")
    public Integer getCrossdock_route_id(){
        return this.crossdock_route_id ;
    }

    /**
     * 设置 [越库路线]
     */
    @JsonProperty("crossdock_route_id")
    public void setCrossdock_route_id(Integer  crossdock_route_id){
        this.crossdock_route_id = crossdock_route_id ;
        this.crossdock_route_idDirtyFlag = true ;
    }

     /**
     * 获取 [越库路线]脏标记
     */
    @JsonIgnore
    public boolean getCrossdock_route_idDirtyFlag(){
        return this.crossdock_route_idDirtyFlag ;
    }   

    /**
     * 获取 [越库路线]
     */
    @JsonProperty("crossdock_route_id_text")
    public String getCrossdock_route_id_text(){
        return this.crossdock_route_id_text ;
    }

    /**
     * 设置 [越库路线]
     */
    @JsonProperty("crossdock_route_id_text")
    public void setCrossdock_route_id_text(String  crossdock_route_id_text){
        this.crossdock_route_id_text = crossdock_route_id_text ;
        this.crossdock_route_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [越库路线]脏标记
     */
    @JsonIgnore
    public boolean getCrossdock_route_id_textDirtyFlag(){
        return this.crossdock_route_id_textDirtyFlag ;
    }   

    /**
     * 获取 [交货路线]
     */
    @JsonProperty("delivery_route_id")
    public Integer getDelivery_route_id(){
        return this.delivery_route_id ;
    }

    /**
     * 设置 [交货路线]
     */
    @JsonProperty("delivery_route_id")
    public void setDelivery_route_id(Integer  delivery_route_id){
        this.delivery_route_id = delivery_route_id ;
        this.delivery_route_idDirtyFlag = true ;
    }

     /**
     * 获取 [交货路线]脏标记
     */
    @JsonIgnore
    public boolean getDelivery_route_idDirtyFlag(){
        return this.delivery_route_idDirtyFlag ;
    }   

    /**
     * 获取 [交货路线]
     */
    @JsonProperty("delivery_route_id_text")
    public String getDelivery_route_id_text(){
        return this.delivery_route_id_text ;
    }

    /**
     * 设置 [交货路线]
     */
    @JsonProperty("delivery_route_id_text")
    public void setDelivery_route_id_text(String  delivery_route_id_text){
        this.delivery_route_id_text = delivery_route_id_text ;
        this.delivery_route_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [交货路线]脏标记
     */
    @JsonIgnore
    public boolean getDelivery_route_id_textDirtyFlag(){
        return this.delivery_route_id_textDirtyFlag ;
    }   

    /**
     * 获取 [出向运输]
     */
    @JsonProperty("delivery_steps")
    public String getDelivery_steps(){
        return this.delivery_steps ;
    }

    /**
     * 设置 [出向运输]
     */
    @JsonProperty("delivery_steps")
    public void setDelivery_steps(String  delivery_steps){
        this.delivery_steps = delivery_steps ;
        this.delivery_stepsDirtyFlag = true ;
    }

     /**
     * 获取 [出向运输]脏标记
     */
    @JsonIgnore
    public boolean getDelivery_stepsDirtyFlag(){
        return this.delivery_stepsDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [内部类型]
     */
    @JsonProperty("int_type_id")
    public Integer getInt_type_id(){
        return this.int_type_id ;
    }

    /**
     * 设置 [内部类型]
     */
    @JsonProperty("int_type_id")
    public void setInt_type_id(Integer  int_type_id){
        this.int_type_id = int_type_id ;
        this.int_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [内部类型]脏标记
     */
    @JsonIgnore
    public boolean getInt_type_idDirtyFlag(){
        return this.int_type_idDirtyFlag ;
    }   

    /**
     * 获取 [内部类型]
     */
    @JsonProperty("int_type_id_text")
    public String getInt_type_id_text(){
        return this.int_type_id_text ;
    }

    /**
     * 设置 [内部类型]
     */
    @JsonProperty("int_type_id_text")
    public void setInt_type_id_text(String  int_type_id_text){
        this.int_type_id_text = int_type_id_text ;
        this.int_type_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [内部类型]脏标记
     */
    @JsonIgnore
    public boolean getInt_type_id_textDirtyFlag(){
        return this.int_type_id_textDirtyFlag ;
    }   

    /**
     * 获取 [入库类型]
     */
    @JsonProperty("in_type_id")
    public Integer getIn_type_id(){
        return this.in_type_id ;
    }

    /**
     * 设置 [入库类型]
     */
    @JsonProperty("in_type_id")
    public void setIn_type_id(Integer  in_type_id){
        this.in_type_id = in_type_id ;
        this.in_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [入库类型]脏标记
     */
    @JsonIgnore
    public boolean getIn_type_idDirtyFlag(){
        return this.in_type_idDirtyFlag ;
    }   

    /**
     * 获取 [入库类型]
     */
    @JsonProperty("in_type_id_text")
    public String getIn_type_id_text(){
        return this.in_type_id_text ;
    }

    /**
     * 设置 [入库类型]
     */
    @JsonProperty("in_type_id_text")
    public void setIn_type_id_text(String  in_type_id_text){
        this.in_type_id_text = in_type_id_text ;
        this.in_type_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [入库类型]脏标记
     */
    @JsonIgnore
    public boolean getIn_type_id_textDirtyFlag(){
        return this.in_type_id_textDirtyFlag ;
    }   

    /**
     * 获取 [库存位置]
     */
    @JsonProperty("lot_stock_id")
    public Integer getLot_stock_id(){
        return this.lot_stock_id ;
    }

    /**
     * 设置 [库存位置]
     */
    @JsonProperty("lot_stock_id")
    public void setLot_stock_id(Integer  lot_stock_id){
        this.lot_stock_id = lot_stock_id ;
        this.lot_stock_idDirtyFlag = true ;
    }

     /**
     * 获取 [库存位置]脏标记
     */
    @JsonIgnore
    public boolean getLot_stock_idDirtyFlag(){
        return this.lot_stock_idDirtyFlag ;
    }   

    /**
     * 获取 [库存位置]
     */
    @JsonProperty("lot_stock_id_text")
    public String getLot_stock_id_text(){
        return this.lot_stock_id_text ;
    }

    /**
     * 设置 [库存位置]
     */
    @JsonProperty("lot_stock_id_text")
    public void setLot_stock_id_text(String  lot_stock_id_text){
        this.lot_stock_id_text = lot_stock_id_text ;
        this.lot_stock_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [库存位置]脏标记
     */
    @JsonIgnore
    public boolean getLot_stock_id_textDirtyFlag(){
        return this.lot_stock_id_textDirtyFlag ;
    }   

    /**
     * 获取 [制造规则]
     */
    @JsonProperty("manufacture_pull_id")
    public Integer getManufacture_pull_id(){
        return this.manufacture_pull_id ;
    }

    /**
     * 设置 [制造规则]
     */
    @JsonProperty("manufacture_pull_id")
    public void setManufacture_pull_id(Integer  manufacture_pull_id){
        this.manufacture_pull_id = manufacture_pull_id ;
        this.manufacture_pull_idDirtyFlag = true ;
    }

     /**
     * 获取 [制造规则]脏标记
     */
    @JsonIgnore
    public boolean getManufacture_pull_idDirtyFlag(){
        return this.manufacture_pull_idDirtyFlag ;
    }   

    /**
     * 获取 [制造规则]
     */
    @JsonProperty("manufacture_pull_id_text")
    public String getManufacture_pull_id_text(){
        return this.manufacture_pull_id_text ;
    }

    /**
     * 设置 [制造规则]
     */
    @JsonProperty("manufacture_pull_id_text")
    public void setManufacture_pull_id_text(String  manufacture_pull_id_text){
        this.manufacture_pull_id_text = manufacture_pull_id_text ;
        this.manufacture_pull_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [制造规则]脏标记
     */
    @JsonIgnore
    public boolean getManufacture_pull_id_textDirtyFlag(){
        return this.manufacture_pull_id_textDirtyFlag ;
    }   

    /**
     * 获取 [制造]
     */
    @JsonProperty("manufacture_steps")
    public String getManufacture_steps(){
        return this.manufacture_steps ;
    }

    /**
     * 设置 [制造]
     */
    @JsonProperty("manufacture_steps")
    public void setManufacture_steps(String  manufacture_steps){
        this.manufacture_steps = manufacture_steps ;
        this.manufacture_stepsDirtyFlag = true ;
    }

     /**
     * 获取 [制造]脏标记
     */
    @JsonIgnore
    public boolean getManufacture_stepsDirtyFlag(){
        return this.manufacture_stepsDirtyFlag ;
    }   

    /**
     * 获取 [制造补给]
     */
    @JsonProperty("manufacture_to_resupply")
    public String getManufacture_to_resupply(){
        return this.manufacture_to_resupply ;
    }

    /**
     * 设置 [制造补给]
     */
    @JsonProperty("manufacture_to_resupply")
    public void setManufacture_to_resupply(String  manufacture_to_resupply){
        this.manufacture_to_resupply = manufacture_to_resupply ;
        this.manufacture_to_resupplyDirtyFlag = true ;
    }

     /**
     * 获取 [制造补给]脏标记
     */
    @JsonIgnore
    public boolean getManufacture_to_resupplyDirtyFlag(){
        return this.manufacture_to_resupplyDirtyFlag ;
    }   

    /**
     * 获取 [生产操作类型]
     */
    @JsonProperty("manu_type_id")
    public Integer getManu_type_id(){
        return this.manu_type_id ;
    }

    /**
     * 设置 [生产操作类型]
     */
    @JsonProperty("manu_type_id")
    public void setManu_type_id(Integer  manu_type_id){
        this.manu_type_id = manu_type_id ;
        this.manu_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [生产操作类型]脏标记
     */
    @JsonIgnore
    public boolean getManu_type_idDirtyFlag(){
        return this.manu_type_idDirtyFlag ;
    }   

    /**
     * 获取 [生产操作类型]
     */
    @JsonProperty("manu_type_id_text")
    public String getManu_type_id_text(){
        return this.manu_type_id_text ;
    }

    /**
     * 设置 [生产操作类型]
     */
    @JsonProperty("manu_type_id_text")
    public void setManu_type_id_text(String  manu_type_id_text){
        this.manu_type_id_text = manu_type_id_text ;
        this.manu_type_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [生产操作类型]脏标记
     */
    @JsonIgnore
    public boolean getManu_type_id_textDirtyFlag(){
        return this.manu_type_id_textDirtyFlag ;
    }   

    /**
     * 获取 [MTO规则]
     */
    @JsonProperty("mto_pull_id")
    public Integer getMto_pull_id(){
        return this.mto_pull_id ;
    }

    /**
     * 设置 [MTO规则]
     */
    @JsonProperty("mto_pull_id")
    public void setMto_pull_id(Integer  mto_pull_id){
        this.mto_pull_id = mto_pull_id ;
        this.mto_pull_idDirtyFlag = true ;
    }

     /**
     * 获取 [MTO规则]脏标记
     */
    @JsonIgnore
    public boolean getMto_pull_idDirtyFlag(){
        return this.mto_pull_idDirtyFlag ;
    }   

    /**
     * 获取 [MTO规则]
     */
    @JsonProperty("mto_pull_id_text")
    public String getMto_pull_id_text(){
        return this.mto_pull_id_text ;
    }

    /**
     * 设置 [MTO规则]
     */
    @JsonProperty("mto_pull_id_text")
    public void setMto_pull_id_text(String  mto_pull_id_text){
        this.mto_pull_id_text = mto_pull_id_text ;
        this.mto_pull_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [MTO规则]脏标记
     */
    @JsonIgnore
    public boolean getMto_pull_id_textDirtyFlag(){
        return this.mto_pull_id_textDirtyFlag ;
    }   

    /**
     * 获取 [仓库]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [仓库]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [仓库]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [出库类型]
     */
    @JsonProperty("out_type_id")
    public Integer getOut_type_id(){
        return this.out_type_id ;
    }

    /**
     * 设置 [出库类型]
     */
    @JsonProperty("out_type_id")
    public void setOut_type_id(Integer  out_type_id){
        this.out_type_id = out_type_id ;
        this.out_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [出库类型]脏标记
     */
    @JsonIgnore
    public boolean getOut_type_idDirtyFlag(){
        return this.out_type_idDirtyFlag ;
    }   

    /**
     * 获取 [出库类型]
     */
    @JsonProperty("out_type_id_text")
    public String getOut_type_id_text(){
        return this.out_type_id_text ;
    }

    /**
     * 设置 [出库类型]
     */
    @JsonProperty("out_type_id_text")
    public void setOut_type_id_text(String  out_type_id_text){
        this.out_type_id_text = out_type_id_text ;
        this.out_type_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [出库类型]脏标记
     */
    @JsonIgnore
    public boolean getOut_type_id_textDirtyFlag(){
        return this.out_type_id_textDirtyFlag ;
    }   

    /**
     * 获取 [包裹类型]
     */
    @JsonProperty("pack_type_id")
    public Integer getPack_type_id(){
        return this.pack_type_id ;
    }

    /**
     * 设置 [包裹类型]
     */
    @JsonProperty("pack_type_id")
    public void setPack_type_id(Integer  pack_type_id){
        this.pack_type_id = pack_type_id ;
        this.pack_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [包裹类型]脏标记
     */
    @JsonIgnore
    public boolean getPack_type_idDirtyFlag(){
        return this.pack_type_idDirtyFlag ;
    }   

    /**
     * 获取 [包裹类型]
     */
    @JsonProperty("pack_type_id_text")
    public String getPack_type_id_text(){
        return this.pack_type_id_text ;
    }

    /**
     * 设置 [包裹类型]
     */
    @JsonProperty("pack_type_id_text")
    public void setPack_type_id_text(String  pack_type_id_text){
        this.pack_type_id_text = pack_type_id_text ;
        this.pack_type_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [包裹类型]脏标记
     */
    @JsonIgnore
    public boolean getPack_type_id_textDirtyFlag(){
        return this.pack_type_id_textDirtyFlag ;
    }   

    /**
     * 获取 [地址]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [地址]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [地址]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [地址]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [地址]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [地址]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [在制造位置前拣货]
     */
    @JsonProperty("pbm_loc_id")
    public Integer getPbm_loc_id(){
        return this.pbm_loc_id ;
    }

    /**
     * 设置 [在制造位置前拣货]
     */
    @JsonProperty("pbm_loc_id")
    public void setPbm_loc_id(Integer  pbm_loc_id){
        this.pbm_loc_id = pbm_loc_id ;
        this.pbm_loc_idDirtyFlag = true ;
    }

     /**
     * 获取 [在制造位置前拣货]脏标记
     */
    @JsonIgnore
    public boolean getPbm_loc_idDirtyFlag(){
        return this.pbm_loc_idDirtyFlag ;
    }   

    /**
     * 获取 [在制造位置前拣货]
     */
    @JsonProperty("pbm_loc_id_text")
    public String getPbm_loc_id_text(){
        return this.pbm_loc_id_text ;
    }

    /**
     * 设置 [在制造位置前拣货]
     */
    @JsonProperty("pbm_loc_id_text")
    public void setPbm_loc_id_text(String  pbm_loc_id_text){
        this.pbm_loc_id_text = pbm_loc_id_text ;
        this.pbm_loc_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [在制造位置前拣货]脏标记
     */
    @JsonIgnore
    public boolean getPbm_loc_id_textDirtyFlag(){
        return this.pbm_loc_id_textDirtyFlag ;
    }   

    /**
     * 获取 [在制造（按订单补货）MTO规则之前拣货]
     */
    @JsonProperty("pbm_mto_pull_id")
    public Integer getPbm_mto_pull_id(){
        return this.pbm_mto_pull_id ;
    }

    /**
     * 设置 [在制造（按订单补货）MTO规则之前拣货]
     */
    @JsonProperty("pbm_mto_pull_id")
    public void setPbm_mto_pull_id(Integer  pbm_mto_pull_id){
        this.pbm_mto_pull_id = pbm_mto_pull_id ;
        this.pbm_mto_pull_idDirtyFlag = true ;
    }

     /**
     * 获取 [在制造（按订单补货）MTO规则之前拣货]脏标记
     */
    @JsonIgnore
    public boolean getPbm_mto_pull_idDirtyFlag(){
        return this.pbm_mto_pull_idDirtyFlag ;
    }   

    /**
     * 获取 [在制造（按订单补货）MTO规则之前拣货]
     */
    @JsonProperty("pbm_mto_pull_id_text")
    public String getPbm_mto_pull_id_text(){
        return this.pbm_mto_pull_id_text ;
    }

    /**
     * 设置 [在制造（按订单补货）MTO规则之前拣货]
     */
    @JsonProperty("pbm_mto_pull_id_text")
    public void setPbm_mto_pull_id_text(String  pbm_mto_pull_id_text){
        this.pbm_mto_pull_id_text = pbm_mto_pull_id_text ;
        this.pbm_mto_pull_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [在制造（按订单补货）MTO规则之前拣货]脏标记
     */
    @JsonIgnore
    public boolean getPbm_mto_pull_id_textDirtyFlag(){
        return this.pbm_mto_pull_id_textDirtyFlag ;
    }   

    /**
     * 获取 [在制造路线前拣货]
     */
    @JsonProperty("pbm_route_id")
    public Integer getPbm_route_id(){
        return this.pbm_route_id ;
    }

    /**
     * 设置 [在制造路线前拣货]
     */
    @JsonProperty("pbm_route_id")
    public void setPbm_route_id(Integer  pbm_route_id){
        this.pbm_route_id = pbm_route_id ;
        this.pbm_route_idDirtyFlag = true ;
    }

     /**
     * 获取 [在制造路线前拣货]脏标记
     */
    @JsonIgnore
    public boolean getPbm_route_idDirtyFlag(){
        return this.pbm_route_idDirtyFlag ;
    }   

    /**
     * 获取 [在制造路线前拣货]
     */
    @JsonProperty("pbm_route_id_text")
    public String getPbm_route_id_text(){
        return this.pbm_route_id_text ;
    }

    /**
     * 设置 [在制造路线前拣货]
     */
    @JsonProperty("pbm_route_id_text")
    public void setPbm_route_id_text(String  pbm_route_id_text){
        this.pbm_route_id_text = pbm_route_id_text ;
        this.pbm_route_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [在制造路线前拣货]脏标记
     */
    @JsonIgnore
    public boolean getPbm_route_id_textDirtyFlag(){
        return this.pbm_route_id_textDirtyFlag ;
    }   

    /**
     * 获取 [在制造作业类型前拣货]
     */
    @JsonProperty("pbm_type_id")
    public Integer getPbm_type_id(){
        return this.pbm_type_id ;
    }

    /**
     * 设置 [在制造作业类型前拣货]
     */
    @JsonProperty("pbm_type_id")
    public void setPbm_type_id(Integer  pbm_type_id){
        this.pbm_type_id = pbm_type_id ;
        this.pbm_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [在制造作业类型前拣货]脏标记
     */
    @JsonIgnore
    public boolean getPbm_type_idDirtyFlag(){
        return this.pbm_type_idDirtyFlag ;
    }   

    /**
     * 获取 [在制造作业类型前拣货]
     */
    @JsonProperty("pbm_type_id_text")
    public String getPbm_type_id_text(){
        return this.pbm_type_id_text ;
    }

    /**
     * 设置 [在制造作业类型前拣货]
     */
    @JsonProperty("pbm_type_id_text")
    public void setPbm_type_id_text(String  pbm_type_id_text){
        this.pbm_type_id_text = pbm_type_id_text ;
        this.pbm_type_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [在制造作业类型前拣货]脏标记
     */
    @JsonIgnore
    public boolean getPbm_type_id_textDirtyFlag(){
        return this.pbm_type_id_textDirtyFlag ;
    }   

    /**
     * 获取 [分拣类型]
     */
    @JsonProperty("pick_type_id")
    public Integer getPick_type_id(){
        return this.pick_type_id ;
    }

    /**
     * 设置 [分拣类型]
     */
    @JsonProperty("pick_type_id")
    public void setPick_type_id(Integer  pick_type_id){
        this.pick_type_id = pick_type_id ;
        this.pick_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [分拣类型]脏标记
     */
    @JsonIgnore
    public boolean getPick_type_idDirtyFlag(){
        return this.pick_type_idDirtyFlag ;
    }   

    /**
     * 获取 [分拣类型]
     */
    @JsonProperty("pick_type_id_text")
    public String getPick_type_id_text(){
        return this.pick_type_id_text ;
    }

    /**
     * 设置 [分拣类型]
     */
    @JsonProperty("pick_type_id_text")
    public void setPick_type_id_text(String  pick_type_id_text){
        this.pick_type_id_text = pick_type_id_text ;
        this.pick_type_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [分拣类型]脏标记
     */
    @JsonIgnore
    public boolean getPick_type_id_textDirtyFlag(){
        return this.pick_type_id_textDirtyFlag ;
    }   

    /**
     * 获取 [收货路线]
     */
    @JsonProperty("reception_route_id")
    public Integer getReception_route_id(){
        return this.reception_route_id ;
    }

    /**
     * 设置 [收货路线]
     */
    @JsonProperty("reception_route_id")
    public void setReception_route_id(Integer  reception_route_id){
        this.reception_route_id = reception_route_id ;
        this.reception_route_idDirtyFlag = true ;
    }

     /**
     * 获取 [收货路线]脏标记
     */
    @JsonIgnore
    public boolean getReception_route_idDirtyFlag(){
        return this.reception_route_idDirtyFlag ;
    }   

    /**
     * 获取 [收货路线]
     */
    @JsonProperty("reception_route_id_text")
    public String getReception_route_id_text(){
        return this.reception_route_id_text ;
    }

    /**
     * 设置 [收货路线]
     */
    @JsonProperty("reception_route_id_text")
    public void setReception_route_id_text(String  reception_route_id_text){
        this.reception_route_id_text = reception_route_id_text ;
        this.reception_route_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [收货路线]脏标记
     */
    @JsonIgnore
    public boolean getReception_route_id_textDirtyFlag(){
        return this.reception_route_id_textDirtyFlag ;
    }   

    /**
     * 获取 [入库]
     */
    @JsonProperty("reception_steps")
    public String getReception_steps(){
        return this.reception_steps ;
    }

    /**
     * 设置 [入库]
     */
    @JsonProperty("reception_steps")
    public void setReception_steps(String  reception_steps){
        this.reception_steps = reception_steps ;
        this.reception_stepsDirtyFlag = true ;
    }

     /**
     * 获取 [入库]脏标记
     */
    @JsonIgnore
    public boolean getReception_stepsDirtyFlag(){
        return this.reception_stepsDirtyFlag ;
    }   

    /**
     * 获取 [补充路线]
     */
    @JsonProperty("resupply_route_ids")
    public String getResupply_route_ids(){
        return this.resupply_route_ids ;
    }

    /**
     * 设置 [补充路线]
     */
    @JsonProperty("resupply_route_ids")
    public void setResupply_route_ids(String  resupply_route_ids){
        this.resupply_route_ids = resupply_route_ids ;
        this.resupply_route_idsDirtyFlag = true ;
    }

     /**
     * 获取 [补充路线]脏标记
     */
    @JsonIgnore
    public boolean getResupply_route_idsDirtyFlag(){
        return this.resupply_route_idsDirtyFlag ;
    }   

    /**
     * 获取 [补给 自]
     */
    @JsonProperty("resupply_wh_ids")
    public String getResupply_wh_ids(){
        return this.resupply_wh_ids ;
    }

    /**
     * 设置 [补给 自]
     */
    @JsonProperty("resupply_wh_ids")
    public void setResupply_wh_ids(String  resupply_wh_ids){
        this.resupply_wh_ids = resupply_wh_ids ;
        this.resupply_wh_idsDirtyFlag = true ;
    }

     /**
     * 获取 [补给 自]脏标记
     */
    @JsonIgnore
    public boolean getResupply_wh_idsDirtyFlag(){
        return this.resupply_wh_idsDirtyFlag ;
    }   

    /**
     * 获取 [路线]
     */
    @JsonProperty("route_ids")
    public String getRoute_ids(){
        return this.route_ids ;
    }

    /**
     * 设置 [路线]
     */
    @JsonProperty("route_ids")
    public void setRoute_ids(String  route_ids){
        this.route_ids = route_ids ;
        this.route_idsDirtyFlag = true ;
    }

     /**
     * 获取 [路线]脏标记
     */
    @JsonIgnore
    public boolean getRoute_idsDirtyFlag(){
        return this.route_idsDirtyFlag ;
    }   

    /**
     * 获取 [制造地点后的库存]
     */
    @JsonProperty("sam_loc_id")
    public Integer getSam_loc_id(){
        return this.sam_loc_id ;
    }

    /**
     * 设置 [制造地点后的库存]
     */
    @JsonProperty("sam_loc_id")
    public void setSam_loc_id(Integer  sam_loc_id){
        this.sam_loc_id = sam_loc_id ;
        this.sam_loc_idDirtyFlag = true ;
    }

     /**
     * 获取 [制造地点后的库存]脏标记
     */
    @JsonIgnore
    public boolean getSam_loc_idDirtyFlag(){
        return this.sam_loc_idDirtyFlag ;
    }   

    /**
     * 获取 [制造地点后的库存]
     */
    @JsonProperty("sam_loc_id_text")
    public String getSam_loc_id_text(){
        return this.sam_loc_id_text ;
    }

    /**
     * 设置 [制造地点后的库存]
     */
    @JsonProperty("sam_loc_id_text")
    public void setSam_loc_id_text(String  sam_loc_id_text){
        this.sam_loc_id_text = sam_loc_id_text ;
        this.sam_loc_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [制造地点后的库存]脏标记
     */
    @JsonIgnore
    public boolean getSam_loc_id_textDirtyFlag(){
        return this.sam_loc_id_textDirtyFlag ;
    }   

    /**
     * 获取 [制造规则后的库存]
     */
    @JsonProperty("sam_rule_id")
    public Integer getSam_rule_id(){
        return this.sam_rule_id ;
    }

    /**
     * 设置 [制造规则后的库存]
     */
    @JsonProperty("sam_rule_id")
    public void setSam_rule_id(Integer  sam_rule_id){
        this.sam_rule_id = sam_rule_id ;
        this.sam_rule_idDirtyFlag = true ;
    }

     /**
     * 获取 [制造规则后的库存]脏标记
     */
    @JsonIgnore
    public boolean getSam_rule_idDirtyFlag(){
        return this.sam_rule_idDirtyFlag ;
    }   

    /**
     * 获取 [制造规则后的库存]
     */
    @JsonProperty("sam_rule_id_text")
    public String getSam_rule_id_text(){
        return this.sam_rule_id_text ;
    }

    /**
     * 设置 [制造规则后的库存]
     */
    @JsonProperty("sam_rule_id_text")
    public void setSam_rule_id_text(String  sam_rule_id_text){
        this.sam_rule_id_text = sam_rule_id_text ;
        this.sam_rule_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [制造规则后的库存]脏标记
     */
    @JsonIgnore
    public boolean getSam_rule_id_textDirtyFlag(){
        return this.sam_rule_id_textDirtyFlag ;
    }   

    /**
     * 获取 [制造运营类型后的库存]
     */
    @JsonProperty("sam_type_id")
    public Integer getSam_type_id(){
        return this.sam_type_id ;
    }

    /**
     * 设置 [制造运营类型后的库存]
     */
    @JsonProperty("sam_type_id")
    public void setSam_type_id(Integer  sam_type_id){
        this.sam_type_id = sam_type_id ;
        this.sam_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [制造运营类型后的库存]脏标记
     */
    @JsonIgnore
    public boolean getSam_type_idDirtyFlag(){
        return this.sam_type_idDirtyFlag ;
    }   

    /**
     * 获取 [制造运营类型后的库存]
     */
    @JsonProperty("sam_type_id_text")
    public String getSam_type_id_text(){
        return this.sam_type_id_text ;
    }

    /**
     * 设置 [制造运营类型后的库存]
     */
    @JsonProperty("sam_type_id_text")
    public void setSam_type_id_text(String  sam_type_id_text){
        this.sam_type_id_text = sam_type_id_text ;
        this.sam_type_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [制造运营类型后的库存]脏标记
     */
    @JsonIgnore
    public boolean getSam_type_id_textDirtyFlag(){
        return this.sam_type_id_textDirtyFlag ;
    }   

    /**
     * 获取 [显示补给]
     */
    @JsonProperty("show_resupply")
    public String getShow_resupply(){
        return this.show_resupply ;
    }

    /**
     * 设置 [显示补给]
     */
    @JsonProperty("show_resupply")
    public void setShow_resupply(String  show_resupply){
        this.show_resupply = show_resupply ;
        this.show_resupplyDirtyFlag = true ;
    }

     /**
     * 获取 [显示补给]脏标记
     */
    @JsonIgnore
    public boolean getShow_resupplyDirtyFlag(){
        return this.show_resupplyDirtyFlag ;
    }   

    /**
     * 获取 [视图位置]
     */
    @JsonProperty("view_location_id")
    public Integer getView_location_id(){
        return this.view_location_id ;
    }

    /**
     * 设置 [视图位置]
     */
    @JsonProperty("view_location_id")
    public void setView_location_id(Integer  view_location_id){
        this.view_location_id = view_location_id ;
        this.view_location_idDirtyFlag = true ;
    }

     /**
     * 获取 [视图位置]脏标记
     */
    @JsonIgnore
    public boolean getView_location_idDirtyFlag(){
        return this.view_location_idDirtyFlag ;
    }   

    /**
     * 获取 [视图位置]
     */
    @JsonProperty("view_location_id_text")
    public String getView_location_id_text(){
        return this.view_location_id_text ;
    }

    /**
     * 设置 [视图位置]
     */
    @JsonProperty("view_location_id_text")
    public void setView_location_id_text(String  view_location_id_text){
        this.view_location_id_text = view_location_id_text ;
        this.view_location_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [视图位置]脏标记
     */
    @JsonIgnore
    public boolean getView_location_id_textDirtyFlag(){
        return this.view_location_id_textDirtyFlag ;
    }   

    /**
     * 获取 [仓库个数]
     */
    @JsonProperty("warehouse_count")
    public Integer getWarehouse_count(){
        return this.warehouse_count ;
    }

    /**
     * 设置 [仓库个数]
     */
    @JsonProperty("warehouse_count")
    public void setWarehouse_count(Integer  warehouse_count){
        this.warehouse_count = warehouse_count ;
        this.warehouse_countDirtyFlag = true ;
    }

     /**
     * 获取 [仓库个数]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_countDirtyFlag(){
        return this.warehouse_countDirtyFlag ;
    }   

    /**
     * 获取 [进货位置]
     */
    @JsonProperty("wh_input_stock_loc_id")
    public Integer getWh_input_stock_loc_id(){
        return this.wh_input_stock_loc_id ;
    }

    /**
     * 设置 [进货位置]
     */
    @JsonProperty("wh_input_stock_loc_id")
    public void setWh_input_stock_loc_id(Integer  wh_input_stock_loc_id){
        this.wh_input_stock_loc_id = wh_input_stock_loc_id ;
        this.wh_input_stock_loc_idDirtyFlag = true ;
    }

     /**
     * 获取 [进货位置]脏标记
     */
    @JsonIgnore
    public boolean getWh_input_stock_loc_idDirtyFlag(){
        return this.wh_input_stock_loc_idDirtyFlag ;
    }   

    /**
     * 获取 [进货位置]
     */
    @JsonProperty("wh_input_stock_loc_id_text")
    public String getWh_input_stock_loc_id_text(){
        return this.wh_input_stock_loc_id_text ;
    }

    /**
     * 设置 [进货位置]
     */
    @JsonProperty("wh_input_stock_loc_id_text")
    public void setWh_input_stock_loc_id_text(String  wh_input_stock_loc_id_text){
        this.wh_input_stock_loc_id_text = wh_input_stock_loc_id_text ;
        this.wh_input_stock_loc_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [进货位置]脏标记
     */
    @JsonIgnore
    public boolean getWh_input_stock_loc_id_textDirtyFlag(){
        return this.wh_input_stock_loc_id_textDirtyFlag ;
    }   

    /**
     * 获取 [出货位置]
     */
    @JsonProperty("wh_output_stock_loc_id")
    public Integer getWh_output_stock_loc_id(){
        return this.wh_output_stock_loc_id ;
    }

    /**
     * 设置 [出货位置]
     */
    @JsonProperty("wh_output_stock_loc_id")
    public void setWh_output_stock_loc_id(Integer  wh_output_stock_loc_id){
        this.wh_output_stock_loc_id = wh_output_stock_loc_id ;
        this.wh_output_stock_loc_idDirtyFlag = true ;
    }

     /**
     * 获取 [出货位置]脏标记
     */
    @JsonIgnore
    public boolean getWh_output_stock_loc_idDirtyFlag(){
        return this.wh_output_stock_loc_idDirtyFlag ;
    }   

    /**
     * 获取 [出货位置]
     */
    @JsonProperty("wh_output_stock_loc_id_text")
    public String getWh_output_stock_loc_id_text(){
        return this.wh_output_stock_loc_id_text ;
    }

    /**
     * 设置 [出货位置]
     */
    @JsonProperty("wh_output_stock_loc_id_text")
    public void setWh_output_stock_loc_id_text(String  wh_output_stock_loc_id_text){
        this.wh_output_stock_loc_id_text = wh_output_stock_loc_id_text ;
        this.wh_output_stock_loc_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [出货位置]脏标记
     */
    @JsonIgnore
    public boolean getWh_output_stock_loc_id_textDirtyFlag(){
        return this.wh_output_stock_loc_id_textDirtyFlag ;
    }   

    /**
     * 获取 [打包位置]
     */
    @JsonProperty("wh_pack_stock_loc_id")
    public Integer getWh_pack_stock_loc_id(){
        return this.wh_pack_stock_loc_id ;
    }

    /**
     * 设置 [打包位置]
     */
    @JsonProperty("wh_pack_stock_loc_id")
    public void setWh_pack_stock_loc_id(Integer  wh_pack_stock_loc_id){
        this.wh_pack_stock_loc_id = wh_pack_stock_loc_id ;
        this.wh_pack_stock_loc_idDirtyFlag = true ;
    }

     /**
     * 获取 [打包位置]脏标记
     */
    @JsonIgnore
    public boolean getWh_pack_stock_loc_idDirtyFlag(){
        return this.wh_pack_stock_loc_idDirtyFlag ;
    }   

    /**
     * 获取 [打包位置]
     */
    @JsonProperty("wh_pack_stock_loc_id_text")
    public String getWh_pack_stock_loc_id_text(){
        return this.wh_pack_stock_loc_id_text ;
    }

    /**
     * 设置 [打包位置]
     */
    @JsonProperty("wh_pack_stock_loc_id_text")
    public void setWh_pack_stock_loc_id_text(String  wh_pack_stock_loc_id_text){
        this.wh_pack_stock_loc_id_text = wh_pack_stock_loc_id_text ;
        this.wh_pack_stock_loc_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [打包位置]脏标记
     */
    @JsonIgnore
    public boolean getWh_pack_stock_loc_id_textDirtyFlag(){
        return this.wh_pack_stock_loc_id_textDirtyFlag ;
    }   

    /**
     * 获取 [质量管理位置]
     */
    @JsonProperty("wh_qc_stock_loc_id")
    public Integer getWh_qc_stock_loc_id(){
        return this.wh_qc_stock_loc_id ;
    }

    /**
     * 设置 [质量管理位置]
     */
    @JsonProperty("wh_qc_stock_loc_id")
    public void setWh_qc_stock_loc_id(Integer  wh_qc_stock_loc_id){
        this.wh_qc_stock_loc_id = wh_qc_stock_loc_id ;
        this.wh_qc_stock_loc_idDirtyFlag = true ;
    }

     /**
     * 获取 [质量管理位置]脏标记
     */
    @JsonIgnore
    public boolean getWh_qc_stock_loc_idDirtyFlag(){
        return this.wh_qc_stock_loc_idDirtyFlag ;
    }   

    /**
     * 获取 [质量管理位置]
     */
    @JsonProperty("wh_qc_stock_loc_id_text")
    public String getWh_qc_stock_loc_id_text(){
        return this.wh_qc_stock_loc_id_text ;
    }

    /**
     * 设置 [质量管理位置]
     */
    @JsonProperty("wh_qc_stock_loc_id_text")
    public void setWh_qc_stock_loc_id_text(String  wh_qc_stock_loc_id_text){
        this.wh_qc_stock_loc_id_text = wh_qc_stock_loc_id_text ;
        this.wh_qc_stock_loc_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [质量管理位置]脏标记
     */
    @JsonIgnore
    public boolean getWh_qc_stock_loc_id_textDirtyFlag(){
        return this.wh_qc_stock_loc_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(map.get("active") instanceof Boolean){
			this.setActive(((Boolean)map.get("active"))? "true" : "false");
		}
		if(!(map.get("buy_pull_id") instanceof Boolean)&& map.get("buy_pull_id")!=null){
			Object[] objs = (Object[])map.get("buy_pull_id");
			if(objs.length > 0){
				this.setBuy_pull_id((Integer)objs[0]);
			}
		}
		if(!(map.get("buy_pull_id") instanceof Boolean)&& map.get("buy_pull_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("buy_pull_id");
			if(objs.length > 1){
				this.setBuy_pull_id_text((String)objs[1]);
			}
		}
		if(map.get("buy_to_resupply") instanceof Boolean){
			this.setBuy_to_resupply(((Boolean)map.get("buy_to_resupply"))? "true" : "false");
		}
		if(!(map.get("code") instanceof Boolean)&& map.get("code")!=null){
			this.setCode((String)map.get("code"));
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 1){
				this.setCompany_id_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("crossdock_route_id") instanceof Boolean)&& map.get("crossdock_route_id")!=null){
			Object[] objs = (Object[])map.get("crossdock_route_id");
			if(objs.length > 0){
				this.setCrossdock_route_id((Integer)objs[0]);
			}
		}
		if(!(map.get("crossdock_route_id") instanceof Boolean)&& map.get("crossdock_route_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("crossdock_route_id");
			if(objs.length > 1){
				this.setCrossdock_route_id_text((String)objs[1]);
			}
		}
		if(!(map.get("delivery_route_id") instanceof Boolean)&& map.get("delivery_route_id")!=null){
			Object[] objs = (Object[])map.get("delivery_route_id");
			if(objs.length > 0){
				this.setDelivery_route_id((Integer)objs[0]);
			}
		}
		if(!(map.get("delivery_route_id") instanceof Boolean)&& map.get("delivery_route_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("delivery_route_id");
			if(objs.length > 1){
				this.setDelivery_route_id_text((String)objs[1]);
			}
		}
		if(!(map.get("delivery_steps") instanceof Boolean)&& map.get("delivery_steps")!=null){
			this.setDelivery_steps((String)map.get("delivery_steps"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("int_type_id") instanceof Boolean)&& map.get("int_type_id")!=null){
			Object[] objs = (Object[])map.get("int_type_id");
			if(objs.length > 0){
				this.setInt_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("int_type_id") instanceof Boolean)&& map.get("int_type_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("int_type_id");
			if(objs.length > 1){
				this.setInt_type_id_text((String)objs[1]);
			}
		}
		if(!(map.get("in_type_id") instanceof Boolean)&& map.get("in_type_id")!=null){
			Object[] objs = (Object[])map.get("in_type_id");
			if(objs.length > 0){
				this.setIn_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("in_type_id") instanceof Boolean)&& map.get("in_type_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("in_type_id");
			if(objs.length > 1){
				this.setIn_type_id_text((String)objs[1]);
			}
		}
		if(!(map.get("lot_stock_id") instanceof Boolean)&& map.get("lot_stock_id")!=null){
			Object[] objs = (Object[])map.get("lot_stock_id");
			if(objs.length > 0){
				this.setLot_stock_id((Integer)objs[0]);
			}
		}
		if(!(map.get("lot_stock_id") instanceof Boolean)&& map.get("lot_stock_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("lot_stock_id");
			if(objs.length > 1){
				this.setLot_stock_id_text((String)objs[1]);
			}
		}
		if(!(map.get("manufacture_pull_id") instanceof Boolean)&& map.get("manufacture_pull_id")!=null){
			Object[] objs = (Object[])map.get("manufacture_pull_id");
			if(objs.length > 0){
				this.setManufacture_pull_id((Integer)objs[0]);
			}
		}
		if(!(map.get("manufacture_pull_id") instanceof Boolean)&& map.get("manufacture_pull_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("manufacture_pull_id");
			if(objs.length > 1){
				this.setManufacture_pull_id_text((String)objs[1]);
			}
		}
		if(!(map.get("manufacture_steps") instanceof Boolean)&& map.get("manufacture_steps")!=null){
			this.setManufacture_steps((String)map.get("manufacture_steps"));
		}
		if(map.get("manufacture_to_resupply") instanceof Boolean){
			this.setManufacture_to_resupply(((Boolean)map.get("manufacture_to_resupply"))? "true" : "false");
		}
		if(!(map.get("manu_type_id") instanceof Boolean)&& map.get("manu_type_id")!=null){
			Object[] objs = (Object[])map.get("manu_type_id");
			if(objs.length > 0){
				this.setManu_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("manu_type_id") instanceof Boolean)&& map.get("manu_type_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("manu_type_id");
			if(objs.length > 1){
				this.setManu_type_id_text((String)objs[1]);
			}
		}
		if(!(map.get("mto_pull_id") instanceof Boolean)&& map.get("mto_pull_id")!=null){
			Object[] objs = (Object[])map.get("mto_pull_id");
			if(objs.length > 0){
				this.setMto_pull_id((Integer)objs[0]);
			}
		}
		if(!(map.get("mto_pull_id") instanceof Boolean)&& map.get("mto_pull_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("mto_pull_id");
			if(objs.length > 1){
				this.setMto_pull_id_text((String)objs[1]);
			}
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("out_type_id") instanceof Boolean)&& map.get("out_type_id")!=null){
			Object[] objs = (Object[])map.get("out_type_id");
			if(objs.length > 0){
				this.setOut_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("out_type_id") instanceof Boolean)&& map.get("out_type_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("out_type_id");
			if(objs.length > 1){
				this.setOut_type_id_text((String)objs[1]);
			}
		}
		if(!(map.get("pack_type_id") instanceof Boolean)&& map.get("pack_type_id")!=null){
			Object[] objs = (Object[])map.get("pack_type_id");
			if(objs.length > 0){
				this.setPack_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("pack_type_id") instanceof Boolean)&& map.get("pack_type_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("pack_type_id");
			if(objs.length > 1){
				this.setPack_type_id_text((String)objs[1]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 0){
				this.setPartner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 1){
				this.setPartner_id_text((String)objs[1]);
			}
		}
		if(!(map.get("pbm_loc_id") instanceof Boolean)&& map.get("pbm_loc_id")!=null){
			Object[] objs = (Object[])map.get("pbm_loc_id");
			if(objs.length > 0){
				this.setPbm_loc_id((Integer)objs[0]);
			}
		}
		if(!(map.get("pbm_loc_id") instanceof Boolean)&& map.get("pbm_loc_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("pbm_loc_id");
			if(objs.length > 1){
				this.setPbm_loc_id_text((String)objs[1]);
			}
		}
		if(!(map.get("pbm_mto_pull_id") instanceof Boolean)&& map.get("pbm_mto_pull_id")!=null){
			Object[] objs = (Object[])map.get("pbm_mto_pull_id");
			if(objs.length > 0){
				this.setPbm_mto_pull_id((Integer)objs[0]);
			}
		}
		if(!(map.get("pbm_mto_pull_id") instanceof Boolean)&& map.get("pbm_mto_pull_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("pbm_mto_pull_id");
			if(objs.length > 1){
				this.setPbm_mto_pull_id_text((String)objs[1]);
			}
		}
		if(!(map.get("pbm_route_id") instanceof Boolean)&& map.get("pbm_route_id")!=null){
			Object[] objs = (Object[])map.get("pbm_route_id");
			if(objs.length > 0){
				this.setPbm_route_id((Integer)objs[0]);
			}
		}
		if(!(map.get("pbm_route_id") instanceof Boolean)&& map.get("pbm_route_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("pbm_route_id");
			if(objs.length > 1){
				this.setPbm_route_id_text((String)objs[1]);
			}
		}
		if(!(map.get("pbm_type_id") instanceof Boolean)&& map.get("pbm_type_id")!=null){
			Object[] objs = (Object[])map.get("pbm_type_id");
			if(objs.length > 0){
				this.setPbm_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("pbm_type_id") instanceof Boolean)&& map.get("pbm_type_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("pbm_type_id");
			if(objs.length > 1){
				this.setPbm_type_id_text((String)objs[1]);
			}
		}
		if(!(map.get("pick_type_id") instanceof Boolean)&& map.get("pick_type_id")!=null){
			Object[] objs = (Object[])map.get("pick_type_id");
			if(objs.length > 0){
				this.setPick_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("pick_type_id") instanceof Boolean)&& map.get("pick_type_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("pick_type_id");
			if(objs.length > 1){
				this.setPick_type_id_text((String)objs[1]);
			}
		}
		if(!(map.get("reception_route_id") instanceof Boolean)&& map.get("reception_route_id")!=null){
			Object[] objs = (Object[])map.get("reception_route_id");
			if(objs.length > 0){
				this.setReception_route_id((Integer)objs[0]);
			}
		}
		if(!(map.get("reception_route_id") instanceof Boolean)&& map.get("reception_route_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("reception_route_id");
			if(objs.length > 1){
				this.setReception_route_id_text((String)objs[1]);
			}
		}
		if(!(map.get("reception_steps") instanceof Boolean)&& map.get("reception_steps")!=null){
			this.setReception_steps((String)map.get("reception_steps"));
		}
		if(!(map.get("resupply_route_ids") instanceof Boolean)&& map.get("resupply_route_ids")!=null){
			Object[] objs = (Object[])map.get("resupply_route_ids");
			if(objs.length > 0){
				Integer[] resupply_route_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setResupply_route_ids(Arrays.toString(resupply_route_ids));
			}
		}
		if(!(map.get("resupply_wh_ids") instanceof Boolean)&& map.get("resupply_wh_ids")!=null){
			Object[] objs = (Object[])map.get("resupply_wh_ids");
			if(objs.length > 0){
				Integer[] resupply_wh_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setResupply_wh_ids(Arrays.toString(resupply_wh_ids));
			}
		}
		if(!(map.get("route_ids") instanceof Boolean)&& map.get("route_ids")!=null){
			Object[] objs = (Object[])map.get("route_ids");
			if(objs.length > 0){
				Integer[] route_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setRoute_ids(Arrays.toString(route_ids));
			}
		}
		if(!(map.get("sam_loc_id") instanceof Boolean)&& map.get("sam_loc_id")!=null){
			Object[] objs = (Object[])map.get("sam_loc_id");
			if(objs.length > 0){
				this.setSam_loc_id((Integer)objs[0]);
			}
		}
		if(!(map.get("sam_loc_id") instanceof Boolean)&& map.get("sam_loc_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("sam_loc_id");
			if(objs.length > 1){
				this.setSam_loc_id_text((String)objs[1]);
			}
		}
		if(!(map.get("sam_rule_id") instanceof Boolean)&& map.get("sam_rule_id")!=null){
			Object[] objs = (Object[])map.get("sam_rule_id");
			if(objs.length > 0){
				this.setSam_rule_id((Integer)objs[0]);
			}
		}
		if(!(map.get("sam_rule_id") instanceof Boolean)&& map.get("sam_rule_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("sam_rule_id");
			if(objs.length > 1){
				this.setSam_rule_id_text((String)objs[1]);
			}
		}
		if(!(map.get("sam_type_id") instanceof Boolean)&& map.get("sam_type_id")!=null){
			Object[] objs = (Object[])map.get("sam_type_id");
			if(objs.length > 0){
				this.setSam_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("sam_type_id") instanceof Boolean)&& map.get("sam_type_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("sam_type_id");
			if(objs.length > 1){
				this.setSam_type_id_text((String)objs[1]);
			}
		}
		if(map.get("show_resupply") instanceof Boolean){
			this.setShow_resupply(((Boolean)map.get("show_resupply"))? "true" : "false");
		}
		if(!(map.get("view_location_id") instanceof Boolean)&& map.get("view_location_id")!=null){
			Object[] objs = (Object[])map.get("view_location_id");
			if(objs.length > 0){
				this.setView_location_id((Integer)objs[0]);
			}
		}
		if(!(map.get("view_location_id") instanceof Boolean)&& map.get("view_location_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("view_location_id");
			if(objs.length > 1){
				this.setView_location_id_text((String)objs[1]);
			}
		}
		if(!(map.get("warehouse_count") instanceof Boolean)&& map.get("warehouse_count")!=null){
			this.setWarehouse_count((Integer)map.get("warehouse_count"));
		}
		if(!(map.get("wh_input_stock_loc_id") instanceof Boolean)&& map.get("wh_input_stock_loc_id")!=null){
			Object[] objs = (Object[])map.get("wh_input_stock_loc_id");
			if(objs.length > 0){
				this.setWh_input_stock_loc_id((Integer)objs[0]);
			}
		}
		if(!(map.get("wh_input_stock_loc_id") instanceof Boolean)&& map.get("wh_input_stock_loc_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("wh_input_stock_loc_id");
			if(objs.length > 1){
				this.setWh_input_stock_loc_id_text((String)objs[1]);
			}
		}
		if(!(map.get("wh_output_stock_loc_id") instanceof Boolean)&& map.get("wh_output_stock_loc_id")!=null){
			Object[] objs = (Object[])map.get("wh_output_stock_loc_id");
			if(objs.length > 0){
				this.setWh_output_stock_loc_id((Integer)objs[0]);
			}
		}
		if(!(map.get("wh_output_stock_loc_id") instanceof Boolean)&& map.get("wh_output_stock_loc_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("wh_output_stock_loc_id");
			if(objs.length > 1){
				this.setWh_output_stock_loc_id_text((String)objs[1]);
			}
		}
		if(!(map.get("wh_pack_stock_loc_id") instanceof Boolean)&& map.get("wh_pack_stock_loc_id")!=null){
			Object[] objs = (Object[])map.get("wh_pack_stock_loc_id");
			if(objs.length > 0){
				this.setWh_pack_stock_loc_id((Integer)objs[0]);
			}
		}
		if(!(map.get("wh_pack_stock_loc_id") instanceof Boolean)&& map.get("wh_pack_stock_loc_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("wh_pack_stock_loc_id");
			if(objs.length > 1){
				this.setWh_pack_stock_loc_id_text((String)objs[1]);
			}
		}
		if(!(map.get("wh_qc_stock_loc_id") instanceof Boolean)&& map.get("wh_qc_stock_loc_id")!=null){
			Object[] objs = (Object[])map.get("wh_qc_stock_loc_id");
			if(objs.length > 0){
				this.setWh_qc_stock_loc_id((Integer)objs[0]);
			}
		}
		if(!(map.get("wh_qc_stock_loc_id") instanceof Boolean)&& map.get("wh_qc_stock_loc_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("wh_qc_stock_loc_id");
			if(objs.length > 1){
				this.setWh_qc_stock_loc_id_text((String)objs[1]);
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getActive()!=null&&this.getActiveDirtyFlag()){
			map.put("active",Boolean.parseBoolean(this.getActive()));		
		}		if(this.getBuy_pull_id()!=null&&this.getBuy_pull_idDirtyFlag()){
			map.put("buy_pull_id",this.getBuy_pull_id());
		}else if(this.getBuy_pull_idDirtyFlag()){
			map.put("buy_pull_id",false);
		}
		if(this.getBuy_pull_id_text()!=null&&this.getBuy_pull_id_textDirtyFlag()){
			//忽略文本外键buy_pull_id_text
		}else if(this.getBuy_pull_id_textDirtyFlag()){
			map.put("buy_pull_id",false);
		}
		if(this.getBuy_to_resupply()!=null&&this.getBuy_to_resupplyDirtyFlag()){
			map.put("buy_to_resupply",Boolean.parseBoolean(this.getBuy_to_resupply()));		
		}		if(this.getCode()!=null&&this.getCodeDirtyFlag()){
			map.put("code",this.getCode());
		}else if(this.getCodeDirtyFlag()){
			map.put("code",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_id_text()!=null&&this.getCompany_id_textDirtyFlag()){
			//忽略文本外键company_id_text
		}else if(this.getCompany_id_textDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCrossdock_route_id()!=null&&this.getCrossdock_route_idDirtyFlag()){
			map.put("crossdock_route_id",this.getCrossdock_route_id());
		}else if(this.getCrossdock_route_idDirtyFlag()){
			map.put("crossdock_route_id",false);
		}
		if(this.getCrossdock_route_id_text()!=null&&this.getCrossdock_route_id_textDirtyFlag()){
			//忽略文本外键crossdock_route_id_text
		}else if(this.getCrossdock_route_id_textDirtyFlag()){
			map.put("crossdock_route_id",false);
		}
		if(this.getDelivery_route_id()!=null&&this.getDelivery_route_idDirtyFlag()){
			map.put("delivery_route_id",this.getDelivery_route_id());
		}else if(this.getDelivery_route_idDirtyFlag()){
			map.put("delivery_route_id",false);
		}
		if(this.getDelivery_route_id_text()!=null&&this.getDelivery_route_id_textDirtyFlag()){
			//忽略文本外键delivery_route_id_text
		}else if(this.getDelivery_route_id_textDirtyFlag()){
			map.put("delivery_route_id",false);
		}
		if(this.getDelivery_steps()!=null&&this.getDelivery_stepsDirtyFlag()){
			map.put("delivery_steps",this.getDelivery_steps());
		}else if(this.getDelivery_stepsDirtyFlag()){
			map.put("delivery_steps",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getInt_type_id()!=null&&this.getInt_type_idDirtyFlag()){
			map.put("int_type_id",this.getInt_type_id());
		}else if(this.getInt_type_idDirtyFlag()){
			map.put("int_type_id",false);
		}
		if(this.getInt_type_id_text()!=null&&this.getInt_type_id_textDirtyFlag()){
			//忽略文本外键int_type_id_text
		}else if(this.getInt_type_id_textDirtyFlag()){
			map.put("int_type_id",false);
		}
		if(this.getIn_type_id()!=null&&this.getIn_type_idDirtyFlag()){
			map.put("in_type_id",this.getIn_type_id());
		}else if(this.getIn_type_idDirtyFlag()){
			map.put("in_type_id",false);
		}
		if(this.getIn_type_id_text()!=null&&this.getIn_type_id_textDirtyFlag()){
			//忽略文本外键in_type_id_text
		}else if(this.getIn_type_id_textDirtyFlag()){
			map.put("in_type_id",false);
		}
		if(this.getLot_stock_id()!=null&&this.getLot_stock_idDirtyFlag()){
			map.put("lot_stock_id",this.getLot_stock_id());
		}else if(this.getLot_stock_idDirtyFlag()){
			map.put("lot_stock_id",false);
		}
		if(this.getLot_stock_id_text()!=null&&this.getLot_stock_id_textDirtyFlag()){
			//忽略文本外键lot_stock_id_text
		}else if(this.getLot_stock_id_textDirtyFlag()){
			map.put("lot_stock_id",false);
		}
		if(this.getManufacture_pull_id()!=null&&this.getManufacture_pull_idDirtyFlag()){
			map.put("manufacture_pull_id",this.getManufacture_pull_id());
		}else if(this.getManufacture_pull_idDirtyFlag()){
			map.put("manufacture_pull_id",false);
		}
		if(this.getManufacture_pull_id_text()!=null&&this.getManufacture_pull_id_textDirtyFlag()){
			//忽略文本外键manufacture_pull_id_text
		}else if(this.getManufacture_pull_id_textDirtyFlag()){
			map.put("manufacture_pull_id",false);
		}
		if(this.getManufacture_steps()!=null&&this.getManufacture_stepsDirtyFlag()){
			map.put("manufacture_steps",this.getManufacture_steps());
		}else if(this.getManufacture_stepsDirtyFlag()){
			map.put("manufacture_steps",false);
		}
		if(this.getManufacture_to_resupply()!=null&&this.getManufacture_to_resupplyDirtyFlag()){
			map.put("manufacture_to_resupply",Boolean.parseBoolean(this.getManufacture_to_resupply()));		
		}		if(this.getManu_type_id()!=null&&this.getManu_type_idDirtyFlag()){
			map.put("manu_type_id",this.getManu_type_id());
		}else if(this.getManu_type_idDirtyFlag()){
			map.put("manu_type_id",false);
		}
		if(this.getManu_type_id_text()!=null&&this.getManu_type_id_textDirtyFlag()){
			//忽略文本外键manu_type_id_text
		}else if(this.getManu_type_id_textDirtyFlag()){
			map.put("manu_type_id",false);
		}
		if(this.getMto_pull_id()!=null&&this.getMto_pull_idDirtyFlag()){
			map.put("mto_pull_id",this.getMto_pull_id());
		}else if(this.getMto_pull_idDirtyFlag()){
			map.put("mto_pull_id",false);
		}
		if(this.getMto_pull_id_text()!=null&&this.getMto_pull_id_textDirtyFlag()){
			//忽略文本外键mto_pull_id_text
		}else if(this.getMto_pull_id_textDirtyFlag()){
			map.put("mto_pull_id",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getOut_type_id()!=null&&this.getOut_type_idDirtyFlag()){
			map.put("out_type_id",this.getOut_type_id());
		}else if(this.getOut_type_idDirtyFlag()){
			map.put("out_type_id",false);
		}
		if(this.getOut_type_id_text()!=null&&this.getOut_type_id_textDirtyFlag()){
			//忽略文本外键out_type_id_text
		}else if(this.getOut_type_id_textDirtyFlag()){
			map.put("out_type_id",false);
		}
		if(this.getPack_type_id()!=null&&this.getPack_type_idDirtyFlag()){
			map.put("pack_type_id",this.getPack_type_id());
		}else if(this.getPack_type_idDirtyFlag()){
			map.put("pack_type_id",false);
		}
		if(this.getPack_type_id_text()!=null&&this.getPack_type_id_textDirtyFlag()){
			//忽略文本外键pack_type_id_text
		}else if(this.getPack_type_id_textDirtyFlag()){
			map.put("pack_type_id",false);
		}
		if(this.getPartner_id()!=null&&this.getPartner_idDirtyFlag()){
			map.put("partner_id",this.getPartner_id());
		}else if(this.getPartner_idDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPartner_id_text()!=null&&this.getPartner_id_textDirtyFlag()){
			//忽略文本外键partner_id_text
		}else if(this.getPartner_id_textDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPbm_loc_id()!=null&&this.getPbm_loc_idDirtyFlag()){
			map.put("pbm_loc_id",this.getPbm_loc_id());
		}else if(this.getPbm_loc_idDirtyFlag()){
			map.put("pbm_loc_id",false);
		}
		if(this.getPbm_loc_id_text()!=null&&this.getPbm_loc_id_textDirtyFlag()){
			//忽略文本外键pbm_loc_id_text
		}else if(this.getPbm_loc_id_textDirtyFlag()){
			map.put("pbm_loc_id",false);
		}
		if(this.getPbm_mto_pull_id()!=null&&this.getPbm_mto_pull_idDirtyFlag()){
			map.put("pbm_mto_pull_id",this.getPbm_mto_pull_id());
		}else if(this.getPbm_mto_pull_idDirtyFlag()){
			map.put("pbm_mto_pull_id",false);
		}
		if(this.getPbm_mto_pull_id_text()!=null&&this.getPbm_mto_pull_id_textDirtyFlag()){
			//忽略文本外键pbm_mto_pull_id_text
		}else if(this.getPbm_mto_pull_id_textDirtyFlag()){
			map.put("pbm_mto_pull_id",false);
		}
		if(this.getPbm_route_id()!=null&&this.getPbm_route_idDirtyFlag()){
			map.put("pbm_route_id",this.getPbm_route_id());
		}else if(this.getPbm_route_idDirtyFlag()){
			map.put("pbm_route_id",false);
		}
		if(this.getPbm_route_id_text()!=null&&this.getPbm_route_id_textDirtyFlag()){
			//忽略文本外键pbm_route_id_text
		}else if(this.getPbm_route_id_textDirtyFlag()){
			map.put("pbm_route_id",false);
		}
		if(this.getPbm_type_id()!=null&&this.getPbm_type_idDirtyFlag()){
			map.put("pbm_type_id",this.getPbm_type_id());
		}else if(this.getPbm_type_idDirtyFlag()){
			map.put("pbm_type_id",false);
		}
		if(this.getPbm_type_id_text()!=null&&this.getPbm_type_id_textDirtyFlag()){
			//忽略文本外键pbm_type_id_text
		}else if(this.getPbm_type_id_textDirtyFlag()){
			map.put("pbm_type_id",false);
		}
		if(this.getPick_type_id()!=null&&this.getPick_type_idDirtyFlag()){
			map.put("pick_type_id",this.getPick_type_id());
		}else if(this.getPick_type_idDirtyFlag()){
			map.put("pick_type_id",false);
		}
		if(this.getPick_type_id_text()!=null&&this.getPick_type_id_textDirtyFlag()){
			//忽略文本外键pick_type_id_text
		}else if(this.getPick_type_id_textDirtyFlag()){
			map.put("pick_type_id",false);
		}
		if(this.getReception_route_id()!=null&&this.getReception_route_idDirtyFlag()){
			map.put("reception_route_id",this.getReception_route_id());
		}else if(this.getReception_route_idDirtyFlag()){
			map.put("reception_route_id",false);
		}
		if(this.getReception_route_id_text()!=null&&this.getReception_route_id_textDirtyFlag()){
			//忽略文本外键reception_route_id_text
		}else if(this.getReception_route_id_textDirtyFlag()){
			map.put("reception_route_id",false);
		}
		if(this.getReception_steps()!=null&&this.getReception_stepsDirtyFlag()){
			map.put("reception_steps",this.getReception_steps());
		}else if(this.getReception_stepsDirtyFlag()){
			map.put("reception_steps",false);
		}
		if(this.getResupply_route_ids()!=null&&this.getResupply_route_idsDirtyFlag()){
			map.put("resupply_route_ids",this.getResupply_route_ids());
		}else if(this.getResupply_route_idsDirtyFlag()){
			map.put("resupply_route_ids",false);
		}
		if(this.getResupply_wh_ids()!=null&&this.getResupply_wh_idsDirtyFlag()){
			map.put("resupply_wh_ids",this.getResupply_wh_ids());
		}else if(this.getResupply_wh_idsDirtyFlag()){
			map.put("resupply_wh_ids",false);
		}
		if(this.getRoute_ids()!=null&&this.getRoute_idsDirtyFlag()){
			map.put("route_ids",this.getRoute_ids());
		}else if(this.getRoute_idsDirtyFlag()){
			map.put("route_ids",false);
		}
		if(this.getSam_loc_id()!=null&&this.getSam_loc_idDirtyFlag()){
			map.put("sam_loc_id",this.getSam_loc_id());
		}else if(this.getSam_loc_idDirtyFlag()){
			map.put("sam_loc_id",false);
		}
		if(this.getSam_loc_id_text()!=null&&this.getSam_loc_id_textDirtyFlag()){
			//忽略文本外键sam_loc_id_text
		}else if(this.getSam_loc_id_textDirtyFlag()){
			map.put("sam_loc_id",false);
		}
		if(this.getSam_rule_id()!=null&&this.getSam_rule_idDirtyFlag()){
			map.put("sam_rule_id",this.getSam_rule_id());
		}else if(this.getSam_rule_idDirtyFlag()){
			map.put("sam_rule_id",false);
		}
		if(this.getSam_rule_id_text()!=null&&this.getSam_rule_id_textDirtyFlag()){
			//忽略文本外键sam_rule_id_text
		}else if(this.getSam_rule_id_textDirtyFlag()){
			map.put("sam_rule_id",false);
		}
		if(this.getSam_type_id()!=null&&this.getSam_type_idDirtyFlag()){
			map.put("sam_type_id",this.getSam_type_id());
		}else if(this.getSam_type_idDirtyFlag()){
			map.put("sam_type_id",false);
		}
		if(this.getSam_type_id_text()!=null&&this.getSam_type_id_textDirtyFlag()){
			//忽略文本外键sam_type_id_text
		}else if(this.getSam_type_id_textDirtyFlag()){
			map.put("sam_type_id",false);
		}
		if(this.getShow_resupply()!=null&&this.getShow_resupplyDirtyFlag()){
			map.put("show_resupply",Boolean.parseBoolean(this.getShow_resupply()));		
		}		if(this.getView_location_id()!=null&&this.getView_location_idDirtyFlag()){
			map.put("view_location_id",this.getView_location_id());
		}else if(this.getView_location_idDirtyFlag()){
			map.put("view_location_id",false);
		}
		if(this.getView_location_id_text()!=null&&this.getView_location_id_textDirtyFlag()){
			//忽略文本外键view_location_id_text
		}else if(this.getView_location_id_textDirtyFlag()){
			map.put("view_location_id",false);
		}
		if(this.getWarehouse_count()!=null&&this.getWarehouse_countDirtyFlag()){
			map.put("warehouse_count",this.getWarehouse_count());
		}else if(this.getWarehouse_countDirtyFlag()){
			map.put("warehouse_count",false);
		}
		if(this.getWh_input_stock_loc_id()!=null&&this.getWh_input_stock_loc_idDirtyFlag()){
			map.put("wh_input_stock_loc_id",this.getWh_input_stock_loc_id());
		}else if(this.getWh_input_stock_loc_idDirtyFlag()){
			map.put("wh_input_stock_loc_id",false);
		}
		if(this.getWh_input_stock_loc_id_text()!=null&&this.getWh_input_stock_loc_id_textDirtyFlag()){
			//忽略文本外键wh_input_stock_loc_id_text
		}else if(this.getWh_input_stock_loc_id_textDirtyFlag()){
			map.put("wh_input_stock_loc_id",false);
		}
		if(this.getWh_output_stock_loc_id()!=null&&this.getWh_output_stock_loc_idDirtyFlag()){
			map.put("wh_output_stock_loc_id",this.getWh_output_stock_loc_id());
		}else if(this.getWh_output_stock_loc_idDirtyFlag()){
			map.put("wh_output_stock_loc_id",false);
		}
		if(this.getWh_output_stock_loc_id_text()!=null&&this.getWh_output_stock_loc_id_textDirtyFlag()){
			//忽略文本外键wh_output_stock_loc_id_text
		}else if(this.getWh_output_stock_loc_id_textDirtyFlag()){
			map.put("wh_output_stock_loc_id",false);
		}
		if(this.getWh_pack_stock_loc_id()!=null&&this.getWh_pack_stock_loc_idDirtyFlag()){
			map.put("wh_pack_stock_loc_id",this.getWh_pack_stock_loc_id());
		}else if(this.getWh_pack_stock_loc_idDirtyFlag()){
			map.put("wh_pack_stock_loc_id",false);
		}
		if(this.getWh_pack_stock_loc_id_text()!=null&&this.getWh_pack_stock_loc_id_textDirtyFlag()){
			//忽略文本外键wh_pack_stock_loc_id_text
		}else if(this.getWh_pack_stock_loc_id_textDirtyFlag()){
			map.put("wh_pack_stock_loc_id",false);
		}
		if(this.getWh_qc_stock_loc_id()!=null&&this.getWh_qc_stock_loc_idDirtyFlag()){
			map.put("wh_qc_stock_loc_id",this.getWh_qc_stock_loc_id());
		}else if(this.getWh_qc_stock_loc_idDirtyFlag()){
			map.put("wh_qc_stock_loc_id",false);
		}
		if(this.getWh_qc_stock_loc_id_text()!=null&&this.getWh_qc_stock_loc_id_textDirtyFlag()){
			//忽略文本外键wh_qc_stock_loc_id_text
		}else if(this.getWh_qc_stock_loc_id_textDirtyFlag()){
			map.put("wh_qc_stock_loc_id",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
