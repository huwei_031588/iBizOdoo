package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_production_lot;
import cn.ibizlab.odoo.core.client.service.Istock_production_lotClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_production_lotImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_production_lotOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_production_lotOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_production_lot] 服务对象接口
 */
@Service
public class stock_production_lotClientServiceImpl implements Istock_production_lotClientService {
    @Autowired
    private  Istock_production_lotOdooClient  stock_production_lotOdooClient;

    public Istock_production_lot createModel() {		
		return new stock_production_lotImpl();
	}


        public void update(Istock_production_lot stock_production_lot){
this.stock_production_lotOdooClient.update(stock_production_lot) ;
        }
        
        public void updateBatch(List<Istock_production_lot> stock_production_lots){
            
        }
        
        public void createBatch(List<Istock_production_lot> stock_production_lots){
            
        }
        
        public void get(Istock_production_lot stock_production_lot){
            this.stock_production_lotOdooClient.get(stock_production_lot) ;
        }
        
        public void removeBatch(List<Istock_production_lot> stock_production_lots){
            
        }
        
        public void create(Istock_production_lot stock_production_lot){
this.stock_production_lotOdooClient.create(stock_production_lot) ;
        }
        
        public Page<Istock_production_lot> search(SearchContext context){
            return this.stock_production_lotOdooClient.search(context) ;
        }
        
        public void remove(Istock_production_lot stock_production_lot){
this.stock_production_lotOdooClient.remove(stock_production_lot) ;
        }
        
        public Page<Istock_production_lot> select(SearchContext context){
            return null ;
        }
        

}

