package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_warn_insufficient_qty;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_warn_insufficient_qty] 服务对象客户端接口
 */
public interface Istock_warn_insufficient_qtyOdooClient {
    
        public void removeBatch(Istock_warn_insufficient_qty stock_warn_insufficient_qty);

        public void update(Istock_warn_insufficient_qty stock_warn_insufficient_qty);

        public void updateBatch(Istock_warn_insufficient_qty stock_warn_insufficient_qty);

        public void remove(Istock_warn_insufficient_qty stock_warn_insufficient_qty);

        public void createBatch(Istock_warn_insufficient_qty stock_warn_insufficient_qty);

        public void get(Istock_warn_insufficient_qty stock_warn_insufficient_qty);

        public Page<Istock_warn_insufficient_qty> search(SearchContext context);

        public void create(Istock_warn_insufficient_qty stock_warn_insufficient_qty);

        public List<Istock_warn_insufficient_qty> select();


}