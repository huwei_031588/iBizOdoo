package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_warn_insufficient_qty_unbuild;
import cn.ibizlab.odoo.core.client.service.Istock_warn_insufficient_qty_unbuildClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_warn_insufficient_qty_unbuildImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_warn_insufficient_qty_unbuildOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_warn_insufficient_qty_unbuildOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_warn_insufficient_qty_unbuild] 服务对象接口
 */
@Service
public class stock_warn_insufficient_qty_unbuildClientServiceImpl implements Istock_warn_insufficient_qty_unbuildClientService {
    @Autowired
    private  Istock_warn_insufficient_qty_unbuildOdooClient  stock_warn_insufficient_qty_unbuildOdooClient;

    public Istock_warn_insufficient_qty_unbuild createModel() {		
		return new stock_warn_insufficient_qty_unbuildImpl();
	}


        public void remove(Istock_warn_insufficient_qty_unbuild stock_warn_insufficient_qty_unbuild){
this.stock_warn_insufficient_qty_unbuildOdooClient.remove(stock_warn_insufficient_qty_unbuild) ;
        }
        
        public void updateBatch(List<Istock_warn_insufficient_qty_unbuild> stock_warn_insufficient_qty_unbuilds){
            
        }
        
        public void get(Istock_warn_insufficient_qty_unbuild stock_warn_insufficient_qty_unbuild){
            this.stock_warn_insufficient_qty_unbuildOdooClient.get(stock_warn_insufficient_qty_unbuild) ;
        }
        
        public Page<Istock_warn_insufficient_qty_unbuild> search(SearchContext context){
            return this.stock_warn_insufficient_qty_unbuildOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Istock_warn_insufficient_qty_unbuild> stock_warn_insufficient_qty_unbuilds){
            
        }
        
        public void create(Istock_warn_insufficient_qty_unbuild stock_warn_insufficient_qty_unbuild){
this.stock_warn_insufficient_qty_unbuildOdooClient.create(stock_warn_insufficient_qty_unbuild) ;
        }
        
        public void createBatch(List<Istock_warn_insufficient_qty_unbuild> stock_warn_insufficient_qty_unbuilds){
            
        }
        
        public void update(Istock_warn_insufficient_qty_unbuild stock_warn_insufficient_qty_unbuild){
this.stock_warn_insufficient_qty_unbuildOdooClient.update(stock_warn_insufficient_qty_unbuild) ;
        }
        
        public Page<Istock_warn_insufficient_qty_unbuild> select(SearchContext context){
            return null ;
        }
        

}

