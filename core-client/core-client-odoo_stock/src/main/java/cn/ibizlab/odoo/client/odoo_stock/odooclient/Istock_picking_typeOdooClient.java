package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_picking_type;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_picking_type] 服务对象客户端接口
 */
public interface Istock_picking_typeOdooClient {
    
        public Page<Istock_picking_type> search(SearchContext context);

        public void createBatch(Istock_picking_type stock_picking_type);

        public void removeBatch(Istock_picking_type stock_picking_type);

        public void create(Istock_picking_type stock_picking_type);

        public void update(Istock_picking_type stock_picking_type);

        public void get(Istock_picking_type stock_picking_type);

        public void updateBatch(Istock_picking_type stock_picking_type);

        public void remove(Istock_picking_type stock_picking_type);

        public List<Istock_picking_type> select();


}