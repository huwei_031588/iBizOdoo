package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_change_product_qty;
import cn.ibizlab.odoo.core.client.service.Istock_change_product_qtyClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_change_product_qtyImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_change_product_qtyOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_change_product_qtyOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_change_product_qty] 服务对象接口
 */
@Service
public class stock_change_product_qtyClientServiceImpl implements Istock_change_product_qtyClientService {
    @Autowired
    private  Istock_change_product_qtyOdooClient  stock_change_product_qtyOdooClient;

    public Istock_change_product_qty createModel() {		
		return new stock_change_product_qtyImpl();
	}


        public void createBatch(List<Istock_change_product_qty> stock_change_product_qties){
            
        }
        
        public void removeBatch(List<Istock_change_product_qty> stock_change_product_qties){
            
        }
        
        public void update(Istock_change_product_qty stock_change_product_qty){
this.stock_change_product_qtyOdooClient.update(stock_change_product_qty) ;
        }
        
        public void remove(Istock_change_product_qty stock_change_product_qty){
this.stock_change_product_qtyOdooClient.remove(stock_change_product_qty) ;
        }
        
        public void updateBatch(List<Istock_change_product_qty> stock_change_product_qties){
            
        }
        
        public void get(Istock_change_product_qty stock_change_product_qty){
            this.stock_change_product_qtyOdooClient.get(stock_change_product_qty) ;
        }
        
        public Page<Istock_change_product_qty> search(SearchContext context){
            return this.stock_change_product_qtyOdooClient.search(context) ;
        }
        
        public void create(Istock_change_product_qty stock_change_product_qty){
this.stock_change_product_qtyOdooClient.create(stock_change_product_qty) ;
        }
        
        public Page<Istock_change_product_qty> select(SearchContext context){
            return null ;
        }
        

}

