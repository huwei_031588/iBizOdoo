package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_return_picking_line;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_return_picking_lineImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_return_picking_line] 服务对象接口
 */
public interface stock_return_picking_lineFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_return_picking_lines/removebatch")
    public stock_return_picking_lineImpl removeBatch(@RequestBody List<stock_return_picking_lineImpl> stock_return_picking_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_return_picking_lines/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_return_picking_lines/createbatch")
    public stock_return_picking_lineImpl createBatch(@RequestBody List<stock_return_picking_lineImpl> stock_return_picking_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_return_picking_lines/{id}")
    public stock_return_picking_lineImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_return_picking_lines/search")
    public Page<stock_return_picking_lineImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_return_picking_lines/{id}")
    public stock_return_picking_lineImpl update(@PathVariable("id") Integer id,@RequestBody stock_return_picking_lineImpl stock_return_picking_line);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_return_picking_lines/updatebatch")
    public stock_return_picking_lineImpl updateBatch(@RequestBody List<stock_return_picking_lineImpl> stock_return_picking_lines);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_return_picking_lines")
    public stock_return_picking_lineImpl create(@RequestBody stock_return_picking_lineImpl stock_return_picking_line);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_return_picking_lines/select")
    public Page<stock_return_picking_lineImpl> select();



}
