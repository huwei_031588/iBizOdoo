package cn.ibizlab.odoo.client.odoo_lunch.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ilunch_order;
import cn.ibizlab.odoo.core.client.service.Ilunch_orderClientService;
import cn.ibizlab.odoo.client.odoo_lunch.model.lunch_orderImpl;
import cn.ibizlab.odoo.client.odoo_lunch.odooclient.Ilunch_orderOdooClient;
import cn.ibizlab.odoo.client.odoo_lunch.odooclient.impl.lunch_orderOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[lunch_order] 服务对象接口
 */
@Service
public class lunch_orderClientServiceImpl implements Ilunch_orderClientService {
    @Autowired
    private  Ilunch_orderOdooClient  lunch_orderOdooClient;

    public Ilunch_order createModel() {		
		return new lunch_orderImpl();
	}


        public void removeBatch(List<Ilunch_order> lunch_orders){
            
        }
        
        public void createBatch(List<Ilunch_order> lunch_orders){
            
        }
        
        public void remove(Ilunch_order lunch_order){
this.lunch_orderOdooClient.remove(lunch_order) ;
        }
        
        public void create(Ilunch_order lunch_order){
this.lunch_orderOdooClient.create(lunch_order) ;
        }
        
        public void updateBatch(List<Ilunch_order> lunch_orders){
            
        }
        
        public void get(Ilunch_order lunch_order){
            this.lunch_orderOdooClient.get(lunch_order) ;
        }
        
        public Page<Ilunch_order> search(SearchContext context){
            return this.lunch_orderOdooClient.search(context) ;
        }
        
        public void update(Ilunch_order lunch_order){
this.lunch_orderOdooClient.update(lunch_order) ;
        }
        
        public Page<Ilunch_order> select(SearchContext context){
            return null ;
        }
        

}

