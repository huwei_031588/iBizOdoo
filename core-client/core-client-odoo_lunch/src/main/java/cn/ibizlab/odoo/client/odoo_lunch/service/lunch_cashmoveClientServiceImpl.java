package cn.ibizlab.odoo.client.odoo_lunch.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ilunch_cashmove;
import cn.ibizlab.odoo.core.client.service.Ilunch_cashmoveClientService;
import cn.ibizlab.odoo.client.odoo_lunch.model.lunch_cashmoveImpl;
import cn.ibizlab.odoo.client.odoo_lunch.odooclient.Ilunch_cashmoveOdooClient;
import cn.ibizlab.odoo.client.odoo_lunch.odooclient.impl.lunch_cashmoveOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[lunch_cashmove] 服务对象接口
 */
@Service
public class lunch_cashmoveClientServiceImpl implements Ilunch_cashmoveClientService {
    @Autowired
    private  Ilunch_cashmoveOdooClient  lunch_cashmoveOdooClient;

    public Ilunch_cashmove createModel() {		
		return new lunch_cashmoveImpl();
	}


        public void createBatch(List<Ilunch_cashmove> lunch_cashmoves){
            
        }
        
        public void updateBatch(List<Ilunch_cashmove> lunch_cashmoves){
            
        }
        
        public void removeBatch(List<Ilunch_cashmove> lunch_cashmoves){
            
        }
        
        public void remove(Ilunch_cashmove lunch_cashmove){
this.lunch_cashmoveOdooClient.remove(lunch_cashmove) ;
        }
        
        public void create(Ilunch_cashmove lunch_cashmove){
this.lunch_cashmoveOdooClient.create(lunch_cashmove) ;
        }
        
        public void update(Ilunch_cashmove lunch_cashmove){
this.lunch_cashmoveOdooClient.update(lunch_cashmove) ;
        }
        
        public void get(Ilunch_cashmove lunch_cashmove){
            this.lunch_cashmoveOdooClient.get(lunch_cashmove) ;
        }
        
        public Page<Ilunch_cashmove> search(SearchContext context){
            return this.lunch_cashmoveOdooClient.search(context) ;
        }
        
        public Page<Ilunch_cashmove> select(SearchContext context){
            return null ;
        }
        

}

