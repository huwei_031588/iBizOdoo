package cn.ibizlab.odoo.client.odoo_lunch.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ilunch_product_category;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[lunch_product_category] 服务对象客户端接口
 */
public interface Ilunch_product_categoryOdooClient {
    
        public void create(Ilunch_product_category lunch_product_category);

        public void remove(Ilunch_product_category lunch_product_category);

        public void updateBatch(Ilunch_product_category lunch_product_category);

        public void update(Ilunch_product_category lunch_product_category);

        public void get(Ilunch_product_category lunch_product_category);

        public void createBatch(Ilunch_product_category lunch_product_category);

        public void removeBatch(Ilunch_product_category lunch_product_category);

        public Page<Ilunch_product_category> search(SearchContext context);

        public List<Ilunch_product_category> select();


}