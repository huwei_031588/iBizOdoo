package cn.ibizlab.odoo.client.odoo_lunch.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ilunch_alert;
import cn.ibizlab.odoo.core.client.service.Ilunch_alertClientService;
import cn.ibizlab.odoo.client.odoo_lunch.model.lunch_alertImpl;
import cn.ibizlab.odoo.client.odoo_lunch.odooclient.Ilunch_alertOdooClient;
import cn.ibizlab.odoo.client.odoo_lunch.odooclient.impl.lunch_alertOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[lunch_alert] 服务对象接口
 */
@Service
public class lunch_alertClientServiceImpl implements Ilunch_alertClientService {
    @Autowired
    private  Ilunch_alertOdooClient  lunch_alertOdooClient;

    public Ilunch_alert createModel() {		
		return new lunch_alertImpl();
	}


        public void removeBatch(List<Ilunch_alert> lunch_alerts){
            
        }
        
        public void createBatch(List<Ilunch_alert> lunch_alerts){
            
        }
        
        public Page<Ilunch_alert> search(SearchContext context){
            return this.lunch_alertOdooClient.search(context) ;
        }
        
        public void remove(Ilunch_alert lunch_alert){
this.lunch_alertOdooClient.remove(lunch_alert) ;
        }
        
        public void get(Ilunch_alert lunch_alert){
            this.lunch_alertOdooClient.get(lunch_alert) ;
        }
        
        public void updateBatch(List<Ilunch_alert> lunch_alerts){
            
        }
        
        public void update(Ilunch_alert lunch_alert){
this.lunch_alertOdooClient.update(lunch_alert) ;
        }
        
        public void create(Ilunch_alert lunch_alert){
this.lunch_alertOdooClient.create(lunch_alert) ;
        }
        
        public Page<Ilunch_alert> select(SearchContext context){
            return null ;
        }
        

}

