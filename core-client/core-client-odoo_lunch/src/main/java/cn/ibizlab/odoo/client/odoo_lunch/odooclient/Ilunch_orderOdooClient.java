package cn.ibizlab.odoo.client.odoo_lunch.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ilunch_order;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[lunch_order] 服务对象客户端接口
 */
public interface Ilunch_orderOdooClient {
    
        public void removeBatch(Ilunch_order lunch_order);

        public void createBatch(Ilunch_order lunch_order);

        public void remove(Ilunch_order lunch_order);

        public void create(Ilunch_order lunch_order);

        public void updateBatch(Ilunch_order lunch_order);

        public void get(Ilunch_order lunch_order);

        public Page<Ilunch_order> search(SearchContext context);

        public void update(Ilunch_order lunch_order);

        public List<Ilunch_order> select();


}