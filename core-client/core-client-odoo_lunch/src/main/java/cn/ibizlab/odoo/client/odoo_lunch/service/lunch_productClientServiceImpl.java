package cn.ibizlab.odoo.client.odoo_lunch.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ilunch_product;
import cn.ibizlab.odoo.core.client.service.Ilunch_productClientService;
import cn.ibizlab.odoo.client.odoo_lunch.model.lunch_productImpl;
import cn.ibizlab.odoo.client.odoo_lunch.odooclient.Ilunch_productOdooClient;
import cn.ibizlab.odoo.client.odoo_lunch.odooclient.impl.lunch_productOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[lunch_product] 服务对象接口
 */
@Service
public class lunch_productClientServiceImpl implements Ilunch_productClientService {
    @Autowired
    private  Ilunch_productOdooClient  lunch_productOdooClient;

    public Ilunch_product createModel() {		
		return new lunch_productImpl();
	}


        public void create(Ilunch_product lunch_product){
this.lunch_productOdooClient.create(lunch_product) ;
        }
        
        public void get(Ilunch_product lunch_product){
            this.lunch_productOdooClient.get(lunch_product) ;
        }
        
        public void createBatch(List<Ilunch_product> lunch_products){
            
        }
        
        public void updateBatch(List<Ilunch_product> lunch_products){
            
        }
        
        public Page<Ilunch_product> search(SearchContext context){
            return this.lunch_productOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Ilunch_product> lunch_products){
            
        }
        
        public void update(Ilunch_product lunch_product){
this.lunch_productOdooClient.update(lunch_product) ;
        }
        
        public void remove(Ilunch_product lunch_product){
this.lunch_productOdooClient.remove(lunch_product) ;
        }
        
        public Page<Ilunch_product> select(SearchContext context){
            return null ;
        }
        

}

