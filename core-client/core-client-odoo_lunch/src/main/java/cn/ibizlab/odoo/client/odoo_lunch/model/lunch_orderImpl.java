package cn.ibizlab.odoo.client.odoo_lunch.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ilunch_order;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[lunch_order] 对象
 */
public class lunch_orderImpl implements Ilunch_order,Serializable{

    /**
     * 警告
     */
    public String alerts;

    @JsonIgnore
    public boolean alertsDirtyFlag;
    
    /**
     * 余额可见
     */
    public String balance_visible;

    @JsonIgnore
    public boolean balance_visibleDirtyFlag;
    
    /**
     * 现金划转余额
     */
    public Double cash_move_balance;

    @JsonIgnore
    public boolean cash_move_balanceDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 币种
     */
    public String currency_id_text;

    @JsonIgnore
    public boolean currency_id_textDirtyFlag;
    
    /**
     * 日期
     */
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 产品
     */
    public String order_line_ids;

    @JsonIgnore
    public boolean order_line_idsDirtyFlag;
    
    /**
     * 先前的订单
     */
    public String previous_order_ids;

    @JsonIgnore
    public boolean previous_order_idsDirtyFlag;
    
    /**
     * 先前的订购小工具
     */
    public String previous_order_widget;

    @JsonIgnore
    public boolean previous_order_widgetDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 总计
     */
    public Double total;

    @JsonIgnore
    public boolean totalDirtyFlag;
    
    /**
     * 用户
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 用户
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [警告]
     */
    @JsonProperty("alerts")
    public String getAlerts(){
        return this.alerts ;
    }

    /**
     * 设置 [警告]
     */
    @JsonProperty("alerts")
    public void setAlerts(String  alerts){
        this.alerts = alerts ;
        this.alertsDirtyFlag = true ;
    }

     /**
     * 获取 [警告]脏标记
     */
    @JsonIgnore
    public boolean getAlertsDirtyFlag(){
        return this.alertsDirtyFlag ;
    }   

    /**
     * 获取 [余额可见]
     */
    @JsonProperty("balance_visible")
    public String getBalance_visible(){
        return this.balance_visible ;
    }

    /**
     * 设置 [余额可见]
     */
    @JsonProperty("balance_visible")
    public void setBalance_visible(String  balance_visible){
        this.balance_visible = balance_visible ;
        this.balance_visibleDirtyFlag = true ;
    }

     /**
     * 获取 [余额可见]脏标记
     */
    @JsonIgnore
    public boolean getBalance_visibleDirtyFlag(){
        return this.balance_visibleDirtyFlag ;
    }   

    /**
     * 获取 [现金划转余额]
     */
    @JsonProperty("cash_move_balance")
    public Double getCash_move_balance(){
        return this.cash_move_balance ;
    }

    /**
     * 设置 [现金划转余额]
     */
    @JsonProperty("cash_move_balance")
    public void setCash_move_balance(Double  cash_move_balance){
        this.cash_move_balance = cash_move_balance ;
        this.cash_move_balanceDirtyFlag = true ;
    }

     /**
     * 获取 [现金划转余额]脏标记
     */
    @JsonIgnore
    public boolean getCash_move_balanceDirtyFlag(){
        return this.cash_move_balanceDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }   

    /**
     * 获取 [日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("order_line_ids")
    public String getOrder_line_ids(){
        return this.order_line_ids ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("order_line_ids")
    public void setOrder_line_ids(String  order_line_ids){
        this.order_line_ids = order_line_ids ;
        this.order_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getOrder_line_idsDirtyFlag(){
        return this.order_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [先前的订单]
     */
    @JsonProperty("previous_order_ids")
    public String getPrevious_order_ids(){
        return this.previous_order_ids ;
    }

    /**
     * 设置 [先前的订单]
     */
    @JsonProperty("previous_order_ids")
    public void setPrevious_order_ids(String  previous_order_ids){
        this.previous_order_ids = previous_order_ids ;
        this.previous_order_idsDirtyFlag = true ;
    }

     /**
     * 获取 [先前的订单]脏标记
     */
    @JsonIgnore
    public boolean getPrevious_order_idsDirtyFlag(){
        return this.previous_order_idsDirtyFlag ;
    }   

    /**
     * 获取 [先前的订购小工具]
     */
    @JsonProperty("previous_order_widget")
    public String getPrevious_order_widget(){
        return this.previous_order_widget ;
    }

    /**
     * 设置 [先前的订购小工具]
     */
    @JsonProperty("previous_order_widget")
    public void setPrevious_order_widget(String  previous_order_widget){
        this.previous_order_widget = previous_order_widget ;
        this.previous_order_widgetDirtyFlag = true ;
    }

     /**
     * 获取 [先前的订购小工具]脏标记
     */
    @JsonIgnore
    public boolean getPrevious_order_widgetDirtyFlag(){
        return this.previous_order_widgetDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [总计]
     */
    @JsonProperty("total")
    public Double getTotal(){
        return this.total ;
    }

    /**
     * 设置 [总计]
     */
    @JsonProperty("total")
    public void setTotal(Double  total){
        this.total = total ;
        this.totalDirtyFlag = true ;
    }

     /**
     * 获取 [总计]脏标记
     */
    @JsonIgnore
    public boolean getTotalDirtyFlag(){
        return this.totalDirtyFlag ;
    }   

    /**
     * 获取 [用户]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [用户]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [用户]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [用户]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [用户]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [用户]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("alerts") instanceof Boolean)&& map.get("alerts")!=null){
			this.setAlerts((String)map.get("alerts"));
		}
		if(map.get("balance_visible") instanceof Boolean){
			this.setBalance_visible(((Boolean)map.get("balance_visible"))? "true" : "false");
		}
		if(!(map.get("cash_move_balance") instanceof Boolean)&& map.get("cash_move_balance")!=null){
			this.setCash_move_balance((Double)map.get("cash_move_balance"));
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 1){
				this.setCompany_id_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 0){
				this.setCurrency_id((Integer)objs[0]);
			}
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 1){
				this.setCurrency_id_text((String)objs[1]);
			}
		}
		if(!(map.get("date") instanceof Boolean)&& map.get("date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("date"));
   			this.setDate(new Timestamp(parse.getTime()));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("order_line_ids") instanceof Boolean)&& map.get("order_line_ids")!=null){
			Object[] objs = (Object[])map.get("order_line_ids");
			if(objs.length > 0){
				Integer[] order_line_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setOrder_line_ids(Arrays.toString(order_line_ids));
			}
		}
		if(!(map.get("previous_order_ids") instanceof Boolean)&& map.get("previous_order_ids")!=null){
			Object[] objs = (Object[])map.get("previous_order_ids");
			if(objs.length > 0){
				Integer[] previous_order_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setPrevious_order_ids(Arrays.toString(previous_order_ids));
			}
		}
		if(!(map.get("previous_order_widget") instanceof Boolean)&& map.get("previous_order_widget")!=null){
			this.setPrevious_order_widget((String)map.get("previous_order_widget"));
		}
		if(!(map.get("state") instanceof Boolean)&& map.get("state")!=null){
			this.setState((String)map.get("state"));
		}
		if(!(map.get("total") instanceof Boolean)&& map.get("total")!=null){
			this.setTotal((Double)map.get("total"));
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 0){
				this.setUser_id((Integer)objs[0]);
			}
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 1){
				this.setUser_id_text((String)objs[1]);
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAlerts()!=null&&this.getAlertsDirtyFlag()){
			map.put("alerts",this.getAlerts());
		}else if(this.getAlertsDirtyFlag()){
			map.put("alerts",false);
		}
		if(this.getBalance_visible()!=null&&this.getBalance_visibleDirtyFlag()){
			map.put("balance_visible",Boolean.parseBoolean(this.getBalance_visible()));		
		}		if(this.getCash_move_balance()!=null&&this.getCash_move_balanceDirtyFlag()){
			map.put("cash_move_balance",this.getCash_move_balance());
		}else if(this.getCash_move_balanceDirtyFlag()){
			map.put("cash_move_balance",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_id_text()!=null&&this.getCompany_id_textDirtyFlag()){
			//忽略文本外键company_id_text
		}else if(this.getCompany_id_textDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCurrency_id()!=null&&this.getCurrency_idDirtyFlag()){
			map.put("currency_id",this.getCurrency_id());
		}else if(this.getCurrency_idDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getCurrency_id_text()!=null&&this.getCurrency_id_textDirtyFlag()){
			//忽略文本外键currency_id_text
		}else if(this.getCurrency_id_textDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getDate()!=null&&this.getDateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getDate());
			map.put("date",datetimeStr);
		}else if(this.getDateDirtyFlag()){
			map.put("date",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getOrder_line_ids()!=null&&this.getOrder_line_idsDirtyFlag()){
			map.put("order_line_ids",this.getOrder_line_ids());
		}else if(this.getOrder_line_idsDirtyFlag()){
			map.put("order_line_ids",false);
		}
		if(this.getPrevious_order_ids()!=null&&this.getPrevious_order_idsDirtyFlag()){
			map.put("previous_order_ids",this.getPrevious_order_ids());
		}else if(this.getPrevious_order_idsDirtyFlag()){
			map.put("previous_order_ids",false);
		}
		if(this.getPrevious_order_widget()!=null&&this.getPrevious_order_widgetDirtyFlag()){
			map.put("previous_order_widget",this.getPrevious_order_widget());
		}else if(this.getPrevious_order_widgetDirtyFlag()){
			map.put("previous_order_widget",false);
		}
		if(this.getState()!=null&&this.getStateDirtyFlag()){
			map.put("state",this.getState());
		}else if(this.getStateDirtyFlag()){
			map.put("state",false);
		}
		if(this.getTotal()!=null&&this.getTotalDirtyFlag()){
			map.put("total",this.getTotal());
		}else if(this.getTotalDirtyFlag()){
			map.put("total",false);
		}
		if(this.getUser_id()!=null&&this.getUser_idDirtyFlag()){
			map.put("user_id",this.getUser_id());
		}else if(this.getUser_idDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getUser_id_text()!=null&&this.getUser_id_textDirtyFlag()){
			//忽略文本外键user_id_text
		}else if(this.getUser_id_textDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
