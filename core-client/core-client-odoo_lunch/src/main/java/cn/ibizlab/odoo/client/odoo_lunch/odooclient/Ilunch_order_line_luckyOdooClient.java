package cn.ibizlab.odoo.client.odoo_lunch.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ilunch_order_line_lucky;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[lunch_order_line_lucky] 服务对象客户端接口
 */
public interface Ilunch_order_line_luckyOdooClient {
    
        public Page<Ilunch_order_line_lucky> search(SearchContext context);

        public void get(Ilunch_order_line_lucky lunch_order_line_lucky);

        public void createBatch(Ilunch_order_line_lucky lunch_order_line_lucky);

        public void updateBatch(Ilunch_order_line_lucky lunch_order_line_lucky);

        public void create(Ilunch_order_line_lucky lunch_order_line_lucky);

        public void update(Ilunch_order_line_lucky lunch_order_line_lucky);

        public void remove(Ilunch_order_line_lucky lunch_order_line_lucky);

        public void removeBatch(Ilunch_order_line_lucky lunch_order_line_lucky);

        public List<Ilunch_order_line_lucky> select();


}