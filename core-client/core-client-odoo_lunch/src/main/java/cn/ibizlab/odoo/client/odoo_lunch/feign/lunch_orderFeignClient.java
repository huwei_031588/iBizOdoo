package cn.ibizlab.odoo.client.odoo_lunch.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ilunch_order;
import cn.ibizlab.odoo.client.odoo_lunch.model.lunch_orderImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[lunch_order] 服务对象接口
 */
public interface lunch_orderFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_orders/removebatch")
    public lunch_orderImpl removeBatch(@RequestBody List<lunch_orderImpl> lunch_orders);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_orders/createbatch")
    public lunch_orderImpl createBatch(@RequestBody List<lunch_orderImpl> lunch_orders);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_orders/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_orders")
    public lunch_orderImpl create(@RequestBody lunch_orderImpl lunch_order);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_orders/updatebatch")
    public lunch_orderImpl updateBatch(@RequestBody List<lunch_orderImpl> lunch_orders);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_orders/{id}")
    public lunch_orderImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_orders/search")
    public Page<lunch_orderImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_orders/{id}")
    public lunch_orderImpl update(@PathVariable("id") Integer id,@RequestBody lunch_orderImpl lunch_order);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_orders/select")
    public Page<lunch_orderImpl> select();



}
