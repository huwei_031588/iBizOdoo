package cn.ibizlab.odoo.client.odoo_lunch.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.lunch")
@Data
public class odoo_lunchClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
