package cn.ibizlab.odoo.client.odoo_lunch.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ilunch_order_line;
import cn.ibizlab.odoo.core.client.service.Ilunch_order_lineClientService;
import cn.ibizlab.odoo.client.odoo_lunch.model.lunch_order_lineImpl;
import cn.ibizlab.odoo.client.odoo_lunch.odooclient.Ilunch_order_lineOdooClient;
import cn.ibizlab.odoo.client.odoo_lunch.odooclient.impl.lunch_order_lineOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[lunch_order_line] 服务对象接口
 */
@Service
public class lunch_order_lineClientServiceImpl implements Ilunch_order_lineClientService {
    @Autowired
    private  Ilunch_order_lineOdooClient  lunch_order_lineOdooClient;

    public Ilunch_order_line createModel() {		
		return new lunch_order_lineImpl();
	}


        public void get(Ilunch_order_line lunch_order_line){
            this.lunch_order_lineOdooClient.get(lunch_order_line) ;
        }
        
        public void createBatch(List<Ilunch_order_line> lunch_order_lines){
            
        }
        
        public void remove(Ilunch_order_line lunch_order_line){
this.lunch_order_lineOdooClient.remove(lunch_order_line) ;
        }
        
        public void removeBatch(List<Ilunch_order_line> lunch_order_lines){
            
        }
        
        public void update(Ilunch_order_line lunch_order_line){
this.lunch_order_lineOdooClient.update(lunch_order_line) ;
        }
        
        public Page<Ilunch_order_line> search(SearchContext context){
            return this.lunch_order_lineOdooClient.search(context) ;
        }
        
        public void create(Ilunch_order_line lunch_order_line){
this.lunch_order_lineOdooClient.create(lunch_order_line) ;
        }
        
        public void updateBatch(List<Ilunch_order_line> lunch_order_lines){
            
        }
        
        public Page<Ilunch_order_line> select(SearchContext context){
            return null ;
        }
        

}

