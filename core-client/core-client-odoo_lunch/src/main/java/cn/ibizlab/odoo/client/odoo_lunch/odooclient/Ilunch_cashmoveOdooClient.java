package cn.ibizlab.odoo.client.odoo_lunch.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ilunch_cashmove;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[lunch_cashmove] 服务对象客户端接口
 */
public interface Ilunch_cashmoveOdooClient {
    
        public void createBatch(Ilunch_cashmove lunch_cashmove);

        public void updateBatch(Ilunch_cashmove lunch_cashmove);

        public void removeBatch(Ilunch_cashmove lunch_cashmove);

        public void remove(Ilunch_cashmove lunch_cashmove);

        public void create(Ilunch_cashmove lunch_cashmove);

        public void update(Ilunch_cashmove lunch_cashmove);

        public void get(Ilunch_cashmove lunch_cashmove);

        public Page<Ilunch_cashmove> search(SearchContext context);

        public List<Ilunch_cashmove> select();


}