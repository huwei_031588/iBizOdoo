package cn.ibizlab.odoo.client.odoo_payment.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ipayment_transaction;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[payment_transaction] 服务对象客户端接口
 */
public interface Ipayment_transactionOdooClient {
    
        public void removeBatch(Ipayment_transaction payment_transaction);

        public Page<Ipayment_transaction> search(SearchContext context);

        public void create(Ipayment_transaction payment_transaction);

        public void createBatch(Ipayment_transaction payment_transaction);

        public void updateBatch(Ipayment_transaction payment_transaction);

        public void get(Ipayment_transaction payment_transaction);

        public void remove(Ipayment_transaction payment_transaction);

        public void update(Ipayment_transaction payment_transaction);

        public List<Ipayment_transaction> select();


}