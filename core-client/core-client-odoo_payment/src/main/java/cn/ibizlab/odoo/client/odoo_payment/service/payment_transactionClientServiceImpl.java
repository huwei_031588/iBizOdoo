package cn.ibizlab.odoo.client.odoo_payment.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ipayment_transaction;
import cn.ibizlab.odoo.core.client.service.Ipayment_transactionClientService;
import cn.ibizlab.odoo.client.odoo_payment.model.payment_transactionImpl;
import cn.ibizlab.odoo.client.odoo_payment.odooclient.Ipayment_transactionOdooClient;
import cn.ibizlab.odoo.client.odoo_payment.odooclient.impl.payment_transactionOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[payment_transaction] 服务对象接口
 */
@Service
public class payment_transactionClientServiceImpl implements Ipayment_transactionClientService {
    @Autowired
    private  Ipayment_transactionOdooClient  payment_transactionOdooClient;

    public Ipayment_transaction createModel() {		
		return new payment_transactionImpl();
	}


        public void removeBatch(List<Ipayment_transaction> payment_transactions){
            
        }
        
        public Page<Ipayment_transaction> search(SearchContext context){
            return this.payment_transactionOdooClient.search(context) ;
        }
        
        public void create(Ipayment_transaction payment_transaction){
this.payment_transactionOdooClient.create(payment_transaction) ;
        }
        
        public void createBatch(List<Ipayment_transaction> payment_transactions){
            
        }
        
        public void updateBatch(List<Ipayment_transaction> payment_transactions){
            
        }
        
        public void get(Ipayment_transaction payment_transaction){
            this.payment_transactionOdooClient.get(payment_transaction) ;
        }
        
        public void remove(Ipayment_transaction payment_transaction){
this.payment_transactionOdooClient.remove(payment_transaction) ;
        }
        
        public void update(Ipayment_transaction payment_transaction){
this.payment_transactionOdooClient.update(payment_transaction) ;
        }
        
        public Page<Ipayment_transaction> select(SearchContext context){
            return null ;
        }
        

}

