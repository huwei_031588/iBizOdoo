package cn.ibizlab.odoo.client.odoo_payment.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.payment")
@Data
public class odoo_paymentClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
