package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_wizard_invite;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_wizard_invite] 服务对象客户端接口
 */
public interface Imail_wizard_inviteOdooClient {
    
        public void update(Imail_wizard_invite mail_wizard_invite);

        public Page<Imail_wizard_invite> search(SearchContext context);

        public void updateBatch(Imail_wizard_invite mail_wizard_invite);

        public void create(Imail_wizard_invite mail_wizard_invite);

        public void removeBatch(Imail_wizard_invite mail_wizard_invite);

        public void get(Imail_wizard_invite mail_wizard_invite);

        public void remove(Imail_wizard_invite mail_wizard_invite);

        public void createBatch(Imail_wizard_invite mail_wizard_invite);

        public List<Imail_wizard_invite> select();


}