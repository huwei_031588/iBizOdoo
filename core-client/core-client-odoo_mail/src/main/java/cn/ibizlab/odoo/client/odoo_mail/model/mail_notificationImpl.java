package cn.ibizlab.odoo.client.odoo_mail.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Imail_notification;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[mail_notification] 对象
 */
public class mail_notificationImpl implements Imail_notification,Serializable{

    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * EMail状态
     */
    public String email_status;

    @JsonIgnore
    public boolean email_statusDirtyFlag;
    
    /**
     * 失败原因
     */
    public String failure_reason;

    @JsonIgnore
    public boolean failure_reasonDirtyFlag;
    
    /**
     * 失败类型
     */
    public String failure_type;

    @JsonIgnore
    public boolean failure_typeDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 以邮件发送
     */
    public String is_email;

    @JsonIgnore
    public boolean is_emailDirtyFlag;
    
    /**
     * 已读
     */
    public String is_read;

    @JsonIgnore
    public boolean is_readDirtyFlag;
    
    /**
     * 邮件
     */
    public Integer mail_id;

    @JsonIgnore
    public boolean mail_idDirtyFlag;
    
    /**
     * 消息
     */
    public Integer mail_message_id;

    @JsonIgnore
    public boolean mail_message_idDirtyFlag;
    
    /**
     * 需收件人
     */
    public Integer res_partner_id;

    @JsonIgnore
    public boolean res_partner_idDirtyFlag;
    
    /**
     * 需收件人
     */
    public String res_partner_id_text;

    @JsonIgnore
    public boolean res_partner_id_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [EMail状态]
     */
    @JsonProperty("email_status")
    public String getEmail_status(){
        return this.email_status ;
    }

    /**
     * 设置 [EMail状态]
     */
    @JsonProperty("email_status")
    public void setEmail_status(String  email_status){
        this.email_status = email_status ;
        this.email_statusDirtyFlag = true ;
    }

     /**
     * 获取 [EMail状态]脏标记
     */
    @JsonIgnore
    public boolean getEmail_statusDirtyFlag(){
        return this.email_statusDirtyFlag ;
    }   

    /**
     * 获取 [失败原因]
     */
    @JsonProperty("failure_reason")
    public String getFailure_reason(){
        return this.failure_reason ;
    }

    /**
     * 设置 [失败原因]
     */
    @JsonProperty("failure_reason")
    public void setFailure_reason(String  failure_reason){
        this.failure_reason = failure_reason ;
        this.failure_reasonDirtyFlag = true ;
    }

     /**
     * 获取 [失败原因]脏标记
     */
    @JsonIgnore
    public boolean getFailure_reasonDirtyFlag(){
        return this.failure_reasonDirtyFlag ;
    }   

    /**
     * 获取 [失败类型]
     */
    @JsonProperty("failure_type")
    public String getFailure_type(){
        return this.failure_type ;
    }

    /**
     * 设置 [失败类型]
     */
    @JsonProperty("failure_type")
    public void setFailure_type(String  failure_type){
        this.failure_type = failure_type ;
        this.failure_typeDirtyFlag = true ;
    }

     /**
     * 获取 [失败类型]脏标记
     */
    @JsonIgnore
    public boolean getFailure_typeDirtyFlag(){
        return this.failure_typeDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [以邮件发送]
     */
    @JsonProperty("is_email")
    public String getIs_email(){
        return this.is_email ;
    }

    /**
     * 设置 [以邮件发送]
     */
    @JsonProperty("is_email")
    public void setIs_email(String  is_email){
        this.is_email = is_email ;
        this.is_emailDirtyFlag = true ;
    }

     /**
     * 获取 [以邮件发送]脏标记
     */
    @JsonIgnore
    public boolean getIs_emailDirtyFlag(){
        return this.is_emailDirtyFlag ;
    }   

    /**
     * 获取 [已读]
     */
    @JsonProperty("is_read")
    public String getIs_read(){
        return this.is_read ;
    }

    /**
     * 设置 [已读]
     */
    @JsonProperty("is_read")
    public void setIs_read(String  is_read){
        this.is_read = is_read ;
        this.is_readDirtyFlag = true ;
    }

     /**
     * 获取 [已读]脏标记
     */
    @JsonIgnore
    public boolean getIs_readDirtyFlag(){
        return this.is_readDirtyFlag ;
    }   

    /**
     * 获取 [邮件]
     */
    @JsonProperty("mail_id")
    public Integer getMail_id(){
        return this.mail_id ;
    }

    /**
     * 设置 [邮件]
     */
    @JsonProperty("mail_id")
    public void setMail_id(Integer  mail_id){
        this.mail_id = mail_id ;
        this.mail_idDirtyFlag = true ;
    }

     /**
     * 获取 [邮件]脏标记
     */
    @JsonIgnore
    public boolean getMail_idDirtyFlag(){
        return this.mail_idDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("mail_message_id")
    public Integer getMail_message_id(){
        return this.mail_message_id ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("mail_message_id")
    public void setMail_message_id(Integer  mail_message_id){
        this.mail_message_id = mail_message_id ;
        this.mail_message_idDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMail_message_idDirtyFlag(){
        return this.mail_message_idDirtyFlag ;
    }   

    /**
     * 获取 [需收件人]
     */
    @JsonProperty("res_partner_id")
    public Integer getRes_partner_id(){
        return this.res_partner_id ;
    }

    /**
     * 设置 [需收件人]
     */
    @JsonProperty("res_partner_id")
    public void setRes_partner_id(Integer  res_partner_id){
        this.res_partner_id = res_partner_id ;
        this.res_partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [需收件人]脏标记
     */
    @JsonIgnore
    public boolean getRes_partner_idDirtyFlag(){
        return this.res_partner_idDirtyFlag ;
    }   

    /**
     * 获取 [需收件人]
     */
    @JsonProperty("res_partner_id_text")
    public String getRes_partner_id_text(){
        return this.res_partner_id_text ;
    }

    /**
     * 设置 [需收件人]
     */
    @JsonProperty("res_partner_id_text")
    public void setRes_partner_id_text(String  res_partner_id_text){
        this.res_partner_id_text = res_partner_id_text ;
        this.res_partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [需收件人]脏标记
     */
    @JsonIgnore
    public boolean getRes_partner_id_textDirtyFlag(){
        return this.res_partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("email_status") instanceof Boolean)&& map.get("email_status")!=null){
			this.setEmail_status((String)map.get("email_status"));
		}
		if(!(map.get("failure_reason") instanceof Boolean)&& map.get("failure_reason")!=null){
			this.setFailure_reason((String)map.get("failure_reason"));
		}
		if(!(map.get("failure_type") instanceof Boolean)&& map.get("failure_type")!=null){
			this.setFailure_type((String)map.get("failure_type"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(map.get("is_email") instanceof Boolean){
			this.setIs_email(((Boolean)map.get("is_email"))? "true" : "false");
		}
		if(map.get("is_read") instanceof Boolean){
			this.setIs_read(((Boolean)map.get("is_read"))? "true" : "false");
		}
		if(!(map.get("mail_id") instanceof Boolean)&& map.get("mail_id")!=null){
			Object[] objs = (Object[])map.get("mail_id");
			if(objs.length > 0){
				this.setMail_id((Integer)objs[0]);
			}
		}
		if(!(map.get("mail_message_id") instanceof Boolean)&& map.get("mail_message_id")!=null){
			Object[] objs = (Object[])map.get("mail_message_id");
			if(objs.length > 0){
				this.setMail_message_id((Integer)objs[0]);
			}
		}
		if(!(map.get("res_partner_id") instanceof Boolean)&& map.get("res_partner_id")!=null){
			Object[] objs = (Object[])map.get("res_partner_id");
			if(objs.length > 0){
				this.setRes_partner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("res_partner_id") instanceof Boolean)&& map.get("res_partner_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("res_partner_id");
			if(objs.length > 1){
				this.setRes_partner_id_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getEmail_status()!=null&&this.getEmail_statusDirtyFlag()){
			map.put("email_status",this.getEmail_status());
		}else if(this.getEmail_statusDirtyFlag()){
			map.put("email_status",false);
		}
		if(this.getFailure_reason()!=null&&this.getFailure_reasonDirtyFlag()){
			map.put("failure_reason",this.getFailure_reason());
		}else if(this.getFailure_reasonDirtyFlag()){
			map.put("failure_reason",false);
		}
		if(this.getFailure_type()!=null&&this.getFailure_typeDirtyFlag()){
			map.put("failure_type",this.getFailure_type());
		}else if(this.getFailure_typeDirtyFlag()){
			map.put("failure_type",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getIs_email()!=null&&this.getIs_emailDirtyFlag()){
			map.put("is_email",Boolean.parseBoolean(this.getIs_email()));		
		}		if(this.getIs_read()!=null&&this.getIs_readDirtyFlag()){
			map.put("is_read",Boolean.parseBoolean(this.getIs_read()));		
		}		if(this.getMail_id()!=null&&this.getMail_idDirtyFlag()){
			map.put("mail_id",this.getMail_id());
		}else if(this.getMail_idDirtyFlag()){
			map.put("mail_id",false);
		}
		if(this.getMail_message_id()!=null&&this.getMail_message_idDirtyFlag()){
			map.put("mail_message_id",this.getMail_message_id());
		}else if(this.getMail_message_idDirtyFlag()){
			map.put("mail_message_id",false);
		}
		if(this.getRes_partner_id()!=null&&this.getRes_partner_idDirtyFlag()){
			map.put("res_partner_id",this.getRes_partner_id());
		}else if(this.getRes_partner_idDirtyFlag()){
			map.put("res_partner_id",false);
		}
		if(this.getRes_partner_id_text()!=null&&this.getRes_partner_id_textDirtyFlag()){
			//忽略文本外键res_partner_id_text
		}else if(this.getRes_partner_id_textDirtyFlag()){
			map.put("res_partner_id",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
