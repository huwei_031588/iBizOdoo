package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_statistics_report;
import cn.ibizlab.odoo.core.client.service.Imail_statistics_reportClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_statistics_reportImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_statistics_reportOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_statistics_reportOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_statistics_report] 服务对象接口
 */
@Service
public class mail_statistics_reportClientServiceImpl implements Imail_statistics_reportClientService {
    @Autowired
    private  Imail_statistics_reportOdooClient  mail_statistics_reportOdooClient;

    public Imail_statistics_report createModel() {		
		return new mail_statistics_reportImpl();
	}


        public void get(Imail_statistics_report mail_statistics_report){
            this.mail_statistics_reportOdooClient.get(mail_statistics_report) ;
        }
        
        public void remove(Imail_statistics_report mail_statistics_report){
this.mail_statistics_reportOdooClient.remove(mail_statistics_report) ;
        }
        
        public Page<Imail_statistics_report> search(SearchContext context){
            return this.mail_statistics_reportOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Imail_statistics_report> mail_statistics_reports){
            
        }
        
        public void removeBatch(List<Imail_statistics_report> mail_statistics_reports){
            
        }
        
        public void create(Imail_statistics_report mail_statistics_report){
this.mail_statistics_reportOdooClient.create(mail_statistics_report) ;
        }
        
        public void createBatch(List<Imail_statistics_report> mail_statistics_reports){
            
        }
        
        public void update(Imail_statistics_report mail_statistics_report){
this.mail_statistics_reportOdooClient.update(mail_statistics_report) ;
        }
        
        public Page<Imail_statistics_report> select(SearchContext context){
            return null ;
        }
        

}

