package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_wizard_invite;
import cn.ibizlab.odoo.core.client.service.Imail_wizard_inviteClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_wizard_inviteImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_wizard_inviteOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_wizard_inviteOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_wizard_invite] 服务对象接口
 */
@Service
public class mail_wizard_inviteClientServiceImpl implements Imail_wizard_inviteClientService {
    @Autowired
    private  Imail_wizard_inviteOdooClient  mail_wizard_inviteOdooClient;

    public Imail_wizard_invite createModel() {		
		return new mail_wizard_inviteImpl();
	}


        public void update(Imail_wizard_invite mail_wizard_invite){
this.mail_wizard_inviteOdooClient.update(mail_wizard_invite) ;
        }
        
        public Page<Imail_wizard_invite> search(SearchContext context){
            return this.mail_wizard_inviteOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Imail_wizard_invite> mail_wizard_invites){
            
        }
        
        public void create(Imail_wizard_invite mail_wizard_invite){
this.mail_wizard_inviteOdooClient.create(mail_wizard_invite) ;
        }
        
        public void removeBatch(List<Imail_wizard_invite> mail_wizard_invites){
            
        }
        
        public void get(Imail_wizard_invite mail_wizard_invite){
            this.mail_wizard_inviteOdooClient.get(mail_wizard_invite) ;
        }
        
        public void remove(Imail_wizard_invite mail_wizard_invite){
this.mail_wizard_inviteOdooClient.remove(mail_wizard_invite) ;
        }
        
        public void createBatch(List<Imail_wizard_invite> mail_wizard_invites){
            
        }
        
        public Page<Imail_wizard_invite> select(SearchContext context){
            return null ;
        }
        

}

