package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_resend_cancel;
import cn.ibizlab.odoo.core.client.service.Imail_resend_cancelClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_resend_cancelImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_resend_cancelOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_resend_cancelOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_resend_cancel] 服务对象接口
 */
@Service
public class mail_resend_cancelClientServiceImpl implements Imail_resend_cancelClientService {
    @Autowired
    private  Imail_resend_cancelOdooClient  mail_resend_cancelOdooClient;

    public Imail_resend_cancel createModel() {		
		return new mail_resend_cancelImpl();
	}


        public void remove(Imail_resend_cancel mail_resend_cancel){
this.mail_resend_cancelOdooClient.remove(mail_resend_cancel) ;
        }
        
        public void update(Imail_resend_cancel mail_resend_cancel){
this.mail_resend_cancelOdooClient.update(mail_resend_cancel) ;
        }
        
        public void createBatch(List<Imail_resend_cancel> mail_resend_cancels){
            
        }
        
        public void removeBatch(List<Imail_resend_cancel> mail_resend_cancels){
            
        }
        
        public void updateBatch(List<Imail_resend_cancel> mail_resend_cancels){
            
        }
        
        public void create(Imail_resend_cancel mail_resend_cancel){
this.mail_resend_cancelOdooClient.create(mail_resend_cancel) ;
        }
        
        public void get(Imail_resend_cancel mail_resend_cancel){
            this.mail_resend_cancelOdooClient.get(mail_resend_cancel) ;
        }
        
        public Page<Imail_resend_cancel> search(SearchContext context){
            return this.mail_resend_cancelOdooClient.search(context) ;
        }
        
        public Page<Imail_resend_cancel> select(SearchContext context){
            return null ;
        }
        

}

