package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_list_contact_rel;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mass_mailing_list_contact_relImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_mass_mailing_list_contact_rel] 服务对象接口
 */
public interface mail_mass_mailing_list_contact_relFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_list_contact_rels/search")
    public Page<mail_mass_mailing_list_contact_relImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_list_contact_rels")
    public mail_mass_mailing_list_contact_relImpl create(@RequestBody mail_mass_mailing_list_contact_relImpl mail_mass_mailing_list_contact_rel);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_list_contact_rels/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_list_contact_rels/removebatch")
    public mail_mass_mailing_list_contact_relImpl removeBatch(@RequestBody List<mail_mass_mailing_list_contact_relImpl> mail_mass_mailing_list_contact_rels);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_list_contact_rels/createbatch")
    public mail_mass_mailing_list_contact_relImpl createBatch(@RequestBody List<mail_mass_mailing_list_contact_relImpl> mail_mass_mailing_list_contact_rels);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_list_contact_rels/{id}")
    public mail_mass_mailing_list_contact_relImpl update(@PathVariable("id") Integer id,@RequestBody mail_mass_mailing_list_contact_relImpl mail_mass_mailing_list_contact_rel);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_list_contact_rels/updatebatch")
    public mail_mass_mailing_list_contact_relImpl updateBatch(@RequestBody List<mail_mass_mailing_list_contact_relImpl> mail_mass_mailing_list_contact_rels);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_list_contact_rels/{id}")
    public mail_mass_mailing_list_contact_relImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_list_contact_rels/select")
    public Page<mail_mass_mailing_list_contact_relImpl> select();



}
