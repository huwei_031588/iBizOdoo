package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_tag;
import cn.ibizlab.odoo.core.client.service.Imail_mass_mailing_tagClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mass_mailing_tagImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_mass_mailing_tagOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_mass_mailing_tagOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_mass_mailing_tag] 服务对象接口
 */
@Service
public class mail_mass_mailing_tagClientServiceImpl implements Imail_mass_mailing_tagClientService {
    @Autowired
    private  Imail_mass_mailing_tagOdooClient  mail_mass_mailing_tagOdooClient;

    public Imail_mass_mailing_tag createModel() {		
		return new mail_mass_mailing_tagImpl();
	}


        public Page<Imail_mass_mailing_tag> search(SearchContext context){
            return this.mail_mass_mailing_tagOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Imail_mass_mailing_tag> mail_mass_mailing_tags){
            
        }
        
        public void remove(Imail_mass_mailing_tag mail_mass_mailing_tag){
this.mail_mass_mailing_tagOdooClient.remove(mail_mass_mailing_tag) ;
        }
        
        public void updateBatch(List<Imail_mass_mailing_tag> mail_mass_mailing_tags){
            
        }
        
        public void create(Imail_mass_mailing_tag mail_mass_mailing_tag){
this.mail_mass_mailing_tagOdooClient.create(mail_mass_mailing_tag) ;
        }
        
        public void update(Imail_mass_mailing_tag mail_mass_mailing_tag){
this.mail_mass_mailing_tagOdooClient.update(mail_mass_mailing_tag) ;
        }
        
        public void createBatch(List<Imail_mass_mailing_tag> mail_mass_mailing_tags){
            
        }
        
        public void get(Imail_mass_mailing_tag mail_mass_mailing_tag){
            this.mail_mass_mailing_tagOdooClient.get(mail_mass_mailing_tag) ;
        }
        
        public Page<Imail_mass_mailing_tag> select(SearchContext context){
            return null ;
        }
        

}

