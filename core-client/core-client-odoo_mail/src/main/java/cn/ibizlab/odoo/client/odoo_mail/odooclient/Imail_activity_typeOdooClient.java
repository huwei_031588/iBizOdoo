package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_activity_type;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_activity_type] 服务对象客户端接口
 */
public interface Imail_activity_typeOdooClient {
    
        public void update(Imail_activity_type mail_activity_type);

        public void create(Imail_activity_type mail_activity_type);

        public void get(Imail_activity_type mail_activity_type);

        public void updateBatch(Imail_activity_type mail_activity_type);

        public void removeBatch(Imail_activity_type mail_activity_type);

        public Page<Imail_activity_type> search(SearchContext context);

        public void remove(Imail_activity_type mail_activity_type);

        public void createBatch(Imail_activity_type mail_activity_type);

        public List<Imail_activity_type> select();


}