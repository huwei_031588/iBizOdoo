package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_thread;
import cn.ibizlab.odoo.core.client.service.Imail_threadClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_threadImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_threadOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_threadOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_thread] 服务对象接口
 */
@Service
public class mail_threadClientServiceImpl implements Imail_threadClientService {
    @Autowired
    private  Imail_threadOdooClient  mail_threadOdooClient;

    public Imail_thread createModel() {		
		return new mail_threadImpl();
	}


        public void updateBatch(List<Imail_thread> mail_threads){
            
        }
        
        public Page<Imail_thread> search(SearchContext context){
            return this.mail_threadOdooClient.search(context) ;
        }
        
        public void update(Imail_thread mail_thread){
this.mail_threadOdooClient.update(mail_thread) ;
        }
        
        public void get(Imail_thread mail_thread){
            this.mail_threadOdooClient.get(mail_thread) ;
        }
        
        public void remove(Imail_thread mail_thread){
this.mail_threadOdooClient.remove(mail_thread) ;
        }
        
        public void createBatch(List<Imail_thread> mail_threads){
            
        }
        
        public void removeBatch(List<Imail_thread> mail_threads){
            
        }
        
        public void create(Imail_thread mail_thread){
this.mail_threadOdooClient.create(mail_thread) ;
        }
        
        public Page<Imail_thread> select(SearchContext context){
            return null ;
        }
        

}

