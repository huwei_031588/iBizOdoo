package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_activity_mixin;
import cn.ibizlab.odoo.core.client.service.Imail_activity_mixinClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_activity_mixinImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_activity_mixinOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_activity_mixinOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_activity_mixin] 服务对象接口
 */
@Service
public class mail_activity_mixinClientServiceImpl implements Imail_activity_mixinClientService {
    @Autowired
    private  Imail_activity_mixinOdooClient  mail_activity_mixinOdooClient;

    public Imail_activity_mixin createModel() {		
		return new mail_activity_mixinImpl();
	}


        public void create(Imail_activity_mixin mail_activity_mixin){
this.mail_activity_mixinOdooClient.create(mail_activity_mixin) ;
        }
        
        public void get(Imail_activity_mixin mail_activity_mixin){
            this.mail_activity_mixinOdooClient.get(mail_activity_mixin) ;
        }
        
        public void createBatch(List<Imail_activity_mixin> mail_activity_mixins){
            
        }
        
        public void update(Imail_activity_mixin mail_activity_mixin){
this.mail_activity_mixinOdooClient.update(mail_activity_mixin) ;
        }
        
        public void updateBatch(List<Imail_activity_mixin> mail_activity_mixins){
            
        }
        
        public Page<Imail_activity_mixin> search(SearchContext context){
            return this.mail_activity_mixinOdooClient.search(context) ;
        }
        
        public void remove(Imail_activity_mixin mail_activity_mixin){
this.mail_activity_mixinOdooClient.remove(mail_activity_mixin) ;
        }
        
        public void removeBatch(List<Imail_activity_mixin> mail_activity_mixins){
            
        }
        
        public Page<Imail_activity_mixin> select(SearchContext context){
            return null ;
        }
        

}

