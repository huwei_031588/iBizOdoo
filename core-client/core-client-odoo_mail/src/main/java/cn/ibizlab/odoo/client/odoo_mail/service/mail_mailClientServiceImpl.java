package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_mail;
import cn.ibizlab.odoo.core.client.service.Imail_mailClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mailImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_mailOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_mailOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_mail] 服务对象接口
 */
@Service
public class mail_mailClientServiceImpl implements Imail_mailClientService {
    @Autowired
    private  Imail_mailOdooClient  mail_mailOdooClient;

    public Imail_mail createModel() {		
		return new mail_mailImpl();
	}


        public void removeBatch(List<Imail_mail> mail_mails){
            
        }
        
        public void updateBatch(List<Imail_mail> mail_mails){
            
        }
        
        public void remove(Imail_mail mail_mail){
this.mail_mailOdooClient.remove(mail_mail) ;
        }
        
        public void update(Imail_mail mail_mail){
this.mail_mailOdooClient.update(mail_mail) ;
        }
        
        public void createBatch(List<Imail_mail> mail_mails){
            
        }
        
        public void get(Imail_mail mail_mail){
            this.mail_mailOdooClient.get(mail_mail) ;
        }
        
        public void create(Imail_mail mail_mail){
this.mail_mailOdooClient.create(mail_mail) ;
        }
        
        public Page<Imail_mail> search(SearchContext context){
            return this.mail_mailOdooClient.search(context) ;
        }
        
        public Page<Imail_mail> select(SearchContext context){
            return null ;
        }
        

}

