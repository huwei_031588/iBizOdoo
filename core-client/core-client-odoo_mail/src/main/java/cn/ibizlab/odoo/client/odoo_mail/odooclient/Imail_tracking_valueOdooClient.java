package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_tracking_value;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_tracking_value] 服务对象客户端接口
 */
public interface Imail_tracking_valueOdooClient {
    
        public Page<Imail_tracking_value> search(SearchContext context);

        public void get(Imail_tracking_value mail_tracking_value);

        public void update(Imail_tracking_value mail_tracking_value);

        public void removeBatch(Imail_tracking_value mail_tracking_value);

        public void remove(Imail_tracking_value mail_tracking_value);

        public void createBatch(Imail_tracking_value mail_tracking_value);

        public void create(Imail_tracking_value mail_tracking_value);

        public void updateBatch(Imail_tracking_value mail_tracking_value);

        public List<Imail_tracking_value> select();


}