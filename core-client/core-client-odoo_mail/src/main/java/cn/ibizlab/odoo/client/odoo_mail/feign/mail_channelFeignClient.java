package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_channel;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_channelImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_channel] 服务对象接口
 */
public interface mail_channelFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_channels/updatebatch")
    public mail_channelImpl updateBatch(@RequestBody List<mail_channelImpl> mail_channels);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_channels")
    public mail_channelImpl create(@RequestBody mail_channelImpl mail_channel);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_channels/search")
    public Page<mail_channelImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_channels/{id}")
    public mail_channelImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_channels/removebatch")
    public mail_channelImpl removeBatch(@RequestBody List<mail_channelImpl> mail_channels);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_channels/createbatch")
    public mail_channelImpl createBatch(@RequestBody List<mail_channelImpl> mail_channels);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_channels/{id}")
    public mail_channelImpl update(@PathVariable("id") Integer id,@RequestBody mail_channelImpl mail_channel);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_channels/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_channels/select")
    public Page<mail_channelImpl> select();



}
