package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mass_mailingImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_mass_mailing] 服务对象接口
 */
public interface mail_mass_mailingFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailings/search")
    public Page<mail_mass_mailingImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailings/removebatch")
    public mail_mass_mailingImpl removeBatch(@RequestBody List<mail_mass_mailingImpl> mail_mass_mailings);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailings")
    public mail_mass_mailingImpl create(@RequestBody mail_mass_mailingImpl mail_mass_mailing);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailings/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailings/updatebatch")
    public mail_mass_mailingImpl updateBatch(@RequestBody List<mail_mass_mailingImpl> mail_mass_mailings);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailings/{id}")
    public mail_mass_mailingImpl update(@PathVariable("id") Integer id,@RequestBody mail_mass_mailingImpl mail_mass_mailing);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailings/createbatch")
    public mail_mass_mailingImpl createBatch(@RequestBody List<mail_mass_mailingImpl> mail_mass_mailings);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailings/{id}")
    public mail_mass_mailingImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailings/select")
    public Page<mail_mass_mailingImpl> select();



}
