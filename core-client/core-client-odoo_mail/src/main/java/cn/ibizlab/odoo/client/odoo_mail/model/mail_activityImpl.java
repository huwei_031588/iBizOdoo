package cn.ibizlab.odoo.client.odoo_mail.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Imail_activity;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[mail_activity] 对象
 */
public class mail_activityImpl implements Imail_activity,Serializable{

    /**
     * 类别
     */
    public String activity_category;

    @JsonIgnore
    public boolean activity_categoryDirtyFlag;
    
    /**
     * 排版类型
     */
    public String activity_decoration;

    @JsonIgnore
    public boolean activity_decorationDirtyFlag;
    
    /**
     * 活动
     */
    public Integer activity_type_id;

    @JsonIgnore
    public boolean activity_type_idDirtyFlag;
    
    /**
     * 活动
     */
    public String activity_type_id_text;

    @JsonIgnore
    public boolean activity_type_id_textDirtyFlag;
    
    /**
     * 自动活动
     */
    public String automated;

    @JsonIgnore
    public boolean automatedDirtyFlag;
    
    /**
     * 日历会议
     */
    public Integer calendar_event_id;

    @JsonIgnore
    public boolean calendar_event_idDirtyFlag;
    
    /**
     * 日历会议
     */
    public String calendar_event_id_text;

    @JsonIgnore
    public boolean calendar_event_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 建立者
     */
    public Integer create_user_id;

    @JsonIgnore
    public boolean create_user_idDirtyFlag;
    
    /**
     * 建立者
     */
    public String create_user_id_text;

    @JsonIgnore
    public boolean create_user_id_textDirtyFlag;
    
    /**
     * 到期时间
     */
    public Timestamp date_deadline;

    @JsonIgnore
    public boolean date_deadlineDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 反馈
     */
    public String feedback;

    @JsonIgnore
    public boolean feedbackDirtyFlag;
    
    /**
     * 自动安排下一个活动
     */
    public String force_next;

    @JsonIgnore
    public boolean force_nextDirtyFlag;
    
    /**
     * 下一活动可用
     */
    public String has_recommended_activities;

    @JsonIgnore
    public boolean has_recommended_activitiesDirtyFlag;
    
    /**
     * 图标
     */
    public String icon;

    @JsonIgnore
    public boolean iconDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 邮件模板
     */
    public String mail_template_ids;

    @JsonIgnore
    public boolean mail_template_idsDirtyFlag;
    
    /**
     * 备注
     */
    public String note;

    @JsonIgnore
    public boolean noteDirtyFlag;
    
    /**
     * 相关便签
     */
    public Integer note_id;

    @JsonIgnore
    public boolean note_idDirtyFlag;
    
    /**
     * 相关便签
     */
    public String note_id_text;

    @JsonIgnore
    public boolean note_id_textDirtyFlag;
    
    /**
     * 前一活动类型
     */
    public Integer previous_activity_type_id;

    @JsonIgnore
    public boolean previous_activity_type_idDirtyFlag;
    
    /**
     * 前一活动类型
     */
    public String previous_activity_type_id_text;

    @JsonIgnore
    public boolean previous_activity_type_id_textDirtyFlag;
    
    /**
     * 推荐的活动类型
     */
    public Integer recommended_activity_type_id;

    @JsonIgnore
    public boolean recommended_activity_type_idDirtyFlag;
    
    /**
     * 推荐的活动类型
     */
    public String recommended_activity_type_id_text;

    @JsonIgnore
    public boolean recommended_activity_type_id_textDirtyFlag;
    
    /**
     * 相关文档编号
     */
    public Integer res_id;

    @JsonIgnore
    public boolean res_idDirtyFlag;
    
    /**
     * 相关的文档模型
     */
    public String res_model;

    @JsonIgnore
    public boolean res_modelDirtyFlag;
    
    /**
     * 文档模型
     */
    public Integer res_model_id;

    @JsonIgnore
    public boolean res_model_idDirtyFlag;
    
    /**
     * 文档名称
     */
    public String res_name;

    @JsonIgnore
    public boolean res_nameDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 摘要
     */
    public String summary;

    @JsonIgnore
    public boolean summaryDirtyFlag;
    
    /**
     * 分派给
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 分派给
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [类别]
     */
    @JsonProperty("activity_category")
    public String getActivity_category(){
        return this.activity_category ;
    }

    /**
     * 设置 [类别]
     */
    @JsonProperty("activity_category")
    public void setActivity_category(String  activity_category){
        this.activity_category = activity_category ;
        this.activity_categoryDirtyFlag = true ;
    }

     /**
     * 获取 [类别]脏标记
     */
    @JsonIgnore
    public boolean getActivity_categoryDirtyFlag(){
        return this.activity_categoryDirtyFlag ;
    }   

    /**
     * 获取 [排版类型]
     */
    @JsonProperty("activity_decoration")
    public String getActivity_decoration(){
        return this.activity_decoration ;
    }

    /**
     * 设置 [排版类型]
     */
    @JsonProperty("activity_decoration")
    public void setActivity_decoration(String  activity_decoration){
        this.activity_decoration = activity_decoration ;
        this.activity_decorationDirtyFlag = true ;
    }

     /**
     * 获取 [排版类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_decorationDirtyFlag(){
        return this.activity_decorationDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_type_id_text")
    public String getActivity_type_id_text(){
        return this.activity_type_id_text ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_type_id_text")
    public void setActivity_type_id_text(String  activity_type_id_text){
        this.activity_type_id_text = activity_type_id_text ;
        this.activity_type_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_id_textDirtyFlag(){
        return this.activity_type_id_textDirtyFlag ;
    }   

    /**
     * 获取 [自动活动]
     */
    @JsonProperty("automated")
    public String getAutomated(){
        return this.automated ;
    }

    /**
     * 设置 [自动活动]
     */
    @JsonProperty("automated")
    public void setAutomated(String  automated){
        this.automated = automated ;
        this.automatedDirtyFlag = true ;
    }

     /**
     * 获取 [自动活动]脏标记
     */
    @JsonIgnore
    public boolean getAutomatedDirtyFlag(){
        return this.automatedDirtyFlag ;
    }   

    /**
     * 获取 [日历会议]
     */
    @JsonProperty("calendar_event_id")
    public Integer getCalendar_event_id(){
        return this.calendar_event_id ;
    }

    /**
     * 设置 [日历会议]
     */
    @JsonProperty("calendar_event_id")
    public void setCalendar_event_id(Integer  calendar_event_id){
        this.calendar_event_id = calendar_event_id ;
        this.calendar_event_idDirtyFlag = true ;
    }

     /**
     * 获取 [日历会议]脏标记
     */
    @JsonIgnore
    public boolean getCalendar_event_idDirtyFlag(){
        return this.calendar_event_idDirtyFlag ;
    }   

    /**
     * 获取 [日历会议]
     */
    @JsonProperty("calendar_event_id_text")
    public String getCalendar_event_id_text(){
        return this.calendar_event_id_text ;
    }

    /**
     * 设置 [日历会议]
     */
    @JsonProperty("calendar_event_id_text")
    public void setCalendar_event_id_text(String  calendar_event_id_text){
        this.calendar_event_id_text = calendar_event_id_text ;
        this.calendar_event_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [日历会议]脏标记
     */
    @JsonIgnore
    public boolean getCalendar_event_id_textDirtyFlag(){
        return this.calendar_event_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [建立者]
     */
    @JsonProperty("create_user_id")
    public Integer getCreate_user_id(){
        return this.create_user_id ;
    }

    /**
     * 设置 [建立者]
     */
    @JsonProperty("create_user_id")
    public void setCreate_user_id(Integer  create_user_id){
        this.create_user_id = create_user_id ;
        this.create_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [建立者]脏标记
     */
    @JsonIgnore
    public boolean getCreate_user_idDirtyFlag(){
        return this.create_user_idDirtyFlag ;
    }   

    /**
     * 获取 [建立者]
     */
    @JsonProperty("create_user_id_text")
    public String getCreate_user_id_text(){
        return this.create_user_id_text ;
    }

    /**
     * 设置 [建立者]
     */
    @JsonProperty("create_user_id_text")
    public void setCreate_user_id_text(String  create_user_id_text){
        this.create_user_id_text = create_user_id_text ;
        this.create_user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [建立者]脏标记
     */
    @JsonIgnore
    public boolean getCreate_user_id_textDirtyFlag(){
        return this.create_user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [到期时间]
     */
    @JsonProperty("date_deadline")
    public Timestamp getDate_deadline(){
        return this.date_deadline ;
    }

    /**
     * 设置 [到期时间]
     */
    @JsonProperty("date_deadline")
    public void setDate_deadline(Timestamp  date_deadline){
        this.date_deadline = date_deadline ;
        this.date_deadlineDirtyFlag = true ;
    }

     /**
     * 获取 [到期时间]脏标记
     */
    @JsonIgnore
    public boolean getDate_deadlineDirtyFlag(){
        return this.date_deadlineDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [反馈]
     */
    @JsonProperty("feedback")
    public String getFeedback(){
        return this.feedback ;
    }

    /**
     * 设置 [反馈]
     */
    @JsonProperty("feedback")
    public void setFeedback(String  feedback){
        this.feedback = feedback ;
        this.feedbackDirtyFlag = true ;
    }

     /**
     * 获取 [反馈]脏标记
     */
    @JsonIgnore
    public boolean getFeedbackDirtyFlag(){
        return this.feedbackDirtyFlag ;
    }   

    /**
     * 获取 [自动安排下一个活动]
     */
    @JsonProperty("force_next")
    public String getForce_next(){
        return this.force_next ;
    }

    /**
     * 设置 [自动安排下一个活动]
     */
    @JsonProperty("force_next")
    public void setForce_next(String  force_next){
        this.force_next = force_next ;
        this.force_nextDirtyFlag = true ;
    }

     /**
     * 获取 [自动安排下一个活动]脏标记
     */
    @JsonIgnore
    public boolean getForce_nextDirtyFlag(){
        return this.force_nextDirtyFlag ;
    }   

    /**
     * 获取 [下一活动可用]
     */
    @JsonProperty("has_recommended_activities")
    public String getHas_recommended_activities(){
        return this.has_recommended_activities ;
    }

    /**
     * 设置 [下一活动可用]
     */
    @JsonProperty("has_recommended_activities")
    public void setHas_recommended_activities(String  has_recommended_activities){
        this.has_recommended_activities = has_recommended_activities ;
        this.has_recommended_activitiesDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动可用]脏标记
     */
    @JsonIgnore
    public boolean getHas_recommended_activitiesDirtyFlag(){
        return this.has_recommended_activitiesDirtyFlag ;
    }   

    /**
     * 获取 [图标]
     */
    @JsonProperty("icon")
    public String getIcon(){
        return this.icon ;
    }

    /**
     * 设置 [图标]
     */
    @JsonProperty("icon")
    public void setIcon(String  icon){
        this.icon = icon ;
        this.iconDirtyFlag = true ;
    }

     /**
     * 获取 [图标]脏标记
     */
    @JsonIgnore
    public boolean getIconDirtyFlag(){
        return this.iconDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [邮件模板]
     */
    @JsonProperty("mail_template_ids")
    public String getMail_template_ids(){
        return this.mail_template_ids ;
    }

    /**
     * 设置 [邮件模板]
     */
    @JsonProperty("mail_template_ids")
    public void setMail_template_ids(String  mail_template_ids){
        this.mail_template_ids = mail_template_ids ;
        this.mail_template_idsDirtyFlag = true ;
    }

     /**
     * 获取 [邮件模板]脏标记
     */
    @JsonIgnore
    public boolean getMail_template_idsDirtyFlag(){
        return this.mail_template_idsDirtyFlag ;
    }   

    /**
     * 获取 [备注]
     */
    @JsonProperty("note")
    public String getNote(){
        return this.note ;
    }

    /**
     * 设置 [备注]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

     /**
     * 获取 [备注]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return this.noteDirtyFlag ;
    }   

    /**
     * 获取 [相关便签]
     */
    @JsonProperty("note_id")
    public Integer getNote_id(){
        return this.note_id ;
    }

    /**
     * 设置 [相关便签]
     */
    @JsonProperty("note_id")
    public void setNote_id(Integer  note_id){
        this.note_id = note_id ;
        this.note_idDirtyFlag = true ;
    }

     /**
     * 获取 [相关便签]脏标记
     */
    @JsonIgnore
    public boolean getNote_idDirtyFlag(){
        return this.note_idDirtyFlag ;
    }   

    /**
     * 获取 [相关便签]
     */
    @JsonProperty("note_id_text")
    public String getNote_id_text(){
        return this.note_id_text ;
    }

    /**
     * 设置 [相关便签]
     */
    @JsonProperty("note_id_text")
    public void setNote_id_text(String  note_id_text){
        this.note_id_text = note_id_text ;
        this.note_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [相关便签]脏标记
     */
    @JsonIgnore
    public boolean getNote_id_textDirtyFlag(){
        return this.note_id_textDirtyFlag ;
    }   

    /**
     * 获取 [前一活动类型]
     */
    @JsonProperty("previous_activity_type_id")
    public Integer getPrevious_activity_type_id(){
        return this.previous_activity_type_id ;
    }

    /**
     * 设置 [前一活动类型]
     */
    @JsonProperty("previous_activity_type_id")
    public void setPrevious_activity_type_id(Integer  previous_activity_type_id){
        this.previous_activity_type_id = previous_activity_type_id ;
        this.previous_activity_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [前一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getPrevious_activity_type_idDirtyFlag(){
        return this.previous_activity_type_idDirtyFlag ;
    }   

    /**
     * 获取 [前一活动类型]
     */
    @JsonProperty("previous_activity_type_id_text")
    public String getPrevious_activity_type_id_text(){
        return this.previous_activity_type_id_text ;
    }

    /**
     * 设置 [前一活动类型]
     */
    @JsonProperty("previous_activity_type_id_text")
    public void setPrevious_activity_type_id_text(String  previous_activity_type_id_text){
        this.previous_activity_type_id_text = previous_activity_type_id_text ;
        this.previous_activity_type_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [前一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getPrevious_activity_type_id_textDirtyFlag(){
        return this.previous_activity_type_id_textDirtyFlag ;
    }   

    /**
     * 获取 [推荐的活动类型]
     */
    @JsonProperty("recommended_activity_type_id")
    public Integer getRecommended_activity_type_id(){
        return this.recommended_activity_type_id ;
    }

    /**
     * 设置 [推荐的活动类型]
     */
    @JsonProperty("recommended_activity_type_id")
    public void setRecommended_activity_type_id(Integer  recommended_activity_type_id){
        this.recommended_activity_type_id = recommended_activity_type_id ;
        this.recommended_activity_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [推荐的活动类型]脏标记
     */
    @JsonIgnore
    public boolean getRecommended_activity_type_idDirtyFlag(){
        return this.recommended_activity_type_idDirtyFlag ;
    }   

    /**
     * 获取 [推荐的活动类型]
     */
    @JsonProperty("recommended_activity_type_id_text")
    public String getRecommended_activity_type_id_text(){
        return this.recommended_activity_type_id_text ;
    }

    /**
     * 设置 [推荐的活动类型]
     */
    @JsonProperty("recommended_activity_type_id_text")
    public void setRecommended_activity_type_id_text(String  recommended_activity_type_id_text){
        this.recommended_activity_type_id_text = recommended_activity_type_id_text ;
        this.recommended_activity_type_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [推荐的活动类型]脏标记
     */
    @JsonIgnore
    public boolean getRecommended_activity_type_id_textDirtyFlag(){
        return this.recommended_activity_type_id_textDirtyFlag ;
    }   

    /**
     * 获取 [相关文档编号]
     */
    @JsonProperty("res_id")
    public Integer getRes_id(){
        return this.res_id ;
    }

    /**
     * 设置 [相关文档编号]
     */
    @JsonProperty("res_id")
    public void setRes_id(Integer  res_id){
        this.res_id = res_id ;
        this.res_idDirtyFlag = true ;
    }

     /**
     * 获取 [相关文档编号]脏标记
     */
    @JsonIgnore
    public boolean getRes_idDirtyFlag(){
        return this.res_idDirtyFlag ;
    }   

    /**
     * 获取 [相关的文档模型]
     */
    @JsonProperty("res_model")
    public String getRes_model(){
        return this.res_model ;
    }

    /**
     * 设置 [相关的文档模型]
     */
    @JsonProperty("res_model")
    public void setRes_model(String  res_model){
        this.res_model = res_model ;
        this.res_modelDirtyFlag = true ;
    }

     /**
     * 获取 [相关的文档模型]脏标记
     */
    @JsonIgnore
    public boolean getRes_modelDirtyFlag(){
        return this.res_modelDirtyFlag ;
    }   

    /**
     * 获取 [文档模型]
     */
    @JsonProperty("res_model_id")
    public Integer getRes_model_id(){
        return this.res_model_id ;
    }

    /**
     * 设置 [文档模型]
     */
    @JsonProperty("res_model_id")
    public void setRes_model_id(Integer  res_model_id){
        this.res_model_id = res_model_id ;
        this.res_model_idDirtyFlag = true ;
    }

     /**
     * 获取 [文档模型]脏标记
     */
    @JsonIgnore
    public boolean getRes_model_idDirtyFlag(){
        return this.res_model_idDirtyFlag ;
    }   

    /**
     * 获取 [文档名称]
     */
    @JsonProperty("res_name")
    public String getRes_name(){
        return this.res_name ;
    }

    /**
     * 设置 [文档名称]
     */
    @JsonProperty("res_name")
    public void setRes_name(String  res_name){
        this.res_name = res_name ;
        this.res_nameDirtyFlag = true ;
    }

     /**
     * 获取 [文档名称]脏标记
     */
    @JsonIgnore
    public boolean getRes_nameDirtyFlag(){
        return this.res_nameDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [摘要]
     */
    @JsonProperty("summary")
    public String getSummary(){
        return this.summary ;
    }

    /**
     * 设置 [摘要]
     */
    @JsonProperty("summary")
    public void setSummary(String  summary){
        this.summary = summary ;
        this.summaryDirtyFlag = true ;
    }

     /**
     * 获取 [摘要]脏标记
     */
    @JsonIgnore
    public boolean getSummaryDirtyFlag(){
        return this.summaryDirtyFlag ;
    }   

    /**
     * 获取 [分派给]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [分派给]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [分派给]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [分派给]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [分派给]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [分派给]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("activity_category") instanceof Boolean)&& map.get("activity_category")!=null){
			this.setActivity_category((String)map.get("activity_category"));
		}
		if(!(map.get("activity_decoration") instanceof Boolean)&& map.get("activity_decoration")!=null){
			this.setActivity_decoration((String)map.get("activity_decoration"));
		}
		if(!(map.get("activity_type_id") instanceof Boolean)&& map.get("activity_type_id")!=null){
			Object[] objs = (Object[])map.get("activity_type_id");
			if(objs.length > 0){
				this.setActivity_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("activity_type_id") instanceof Boolean)&& map.get("activity_type_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("activity_type_id");
			if(objs.length > 1){
				this.setActivity_type_id_text((String)objs[1]);
			}
		}
		if(map.get("automated") instanceof Boolean){
			this.setAutomated(((Boolean)map.get("automated"))? "true" : "false");
		}
		if(!(map.get("calendar_event_id") instanceof Boolean)&& map.get("calendar_event_id")!=null){
			Object[] objs = (Object[])map.get("calendar_event_id");
			if(objs.length > 0){
				this.setCalendar_event_id((Integer)objs[0]);
			}
		}
		if(!(map.get("calendar_event_id") instanceof Boolean)&& map.get("calendar_event_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("calendar_event_id");
			if(objs.length > 1){
				this.setCalendar_event_id_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("create_user_id") instanceof Boolean)&& map.get("create_user_id")!=null){
			Object[] objs = (Object[])map.get("create_user_id");
			if(objs.length > 0){
				this.setCreate_user_id((Integer)objs[0]);
			}
		}
		if(!(map.get("create_user_id") instanceof Boolean)&& map.get("create_user_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_user_id");
			if(objs.length > 1){
				this.setCreate_user_id_text((String)objs[1]);
			}
		}
		if(!(map.get("date_deadline") instanceof Boolean)&& map.get("date_deadline")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("date_deadline"));
   			this.setDate_deadline(new Timestamp(parse.getTime()));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("feedback") instanceof Boolean)&& map.get("feedback")!=null){
			this.setFeedback((String)map.get("feedback"));
		}
		if(map.get("force_next") instanceof Boolean){
			this.setForce_next(((Boolean)map.get("force_next"))? "true" : "false");
		}
		if(map.get("has_recommended_activities") instanceof Boolean){
			this.setHas_recommended_activities(((Boolean)map.get("has_recommended_activities"))? "true" : "false");
		}
		if(!(map.get("icon") instanceof Boolean)&& map.get("icon")!=null){
			this.setIcon((String)map.get("icon"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("mail_template_ids") instanceof Boolean)&& map.get("mail_template_ids")!=null){
			Object[] objs = (Object[])map.get("mail_template_ids");
			if(objs.length > 0){
				Integer[] mail_template_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMail_template_ids(Arrays.toString(mail_template_ids));
			}
		}
		if(!(map.get("note") instanceof Boolean)&& map.get("note")!=null){
			this.setNote((String)map.get("note"));
		}
		if(!(map.get("note_id") instanceof Boolean)&& map.get("note_id")!=null){
			Object[] objs = (Object[])map.get("note_id");
			if(objs.length > 0){
				this.setNote_id((Integer)objs[0]);
			}
		}
		if(!(map.get("note_id") instanceof Boolean)&& map.get("note_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("note_id");
			if(objs.length > 1){
				this.setNote_id_text((String)objs[1]);
			}
		}
		if(!(map.get("previous_activity_type_id") instanceof Boolean)&& map.get("previous_activity_type_id")!=null){
			Object[] objs = (Object[])map.get("previous_activity_type_id");
			if(objs.length > 0){
				this.setPrevious_activity_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("previous_activity_type_id") instanceof Boolean)&& map.get("previous_activity_type_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("previous_activity_type_id");
			if(objs.length > 1){
				this.setPrevious_activity_type_id_text((String)objs[1]);
			}
		}
		if(!(map.get("recommended_activity_type_id") instanceof Boolean)&& map.get("recommended_activity_type_id")!=null){
			Object[] objs = (Object[])map.get("recommended_activity_type_id");
			if(objs.length > 0){
				this.setRecommended_activity_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("recommended_activity_type_id") instanceof Boolean)&& map.get("recommended_activity_type_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("recommended_activity_type_id");
			if(objs.length > 1){
				this.setRecommended_activity_type_id_text((String)objs[1]);
			}
		}
		if(!(map.get("res_id") instanceof Boolean)&& map.get("res_id")!=null){
			this.setRes_id((Integer)map.get("res_id"));
		}
		if(!(map.get("res_model") instanceof Boolean)&& map.get("res_model")!=null){
			this.setRes_model((String)map.get("res_model"));
		}
		if(!(map.get("res_model_id") instanceof Boolean)&& map.get("res_model_id")!=null){
			Object[] objs = (Object[])map.get("res_model_id");
			if(objs.length > 0){
				this.setRes_model_id((Integer)objs[0]);
			}
		}
		if(!(map.get("res_name") instanceof Boolean)&& map.get("res_name")!=null){
			this.setRes_name((String)map.get("res_name"));
		}
		if(!(map.get("state") instanceof Boolean)&& map.get("state")!=null){
			this.setState((String)map.get("state"));
		}
		if(!(map.get("summary") instanceof Boolean)&& map.get("summary")!=null){
			this.setSummary((String)map.get("summary"));
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 0){
				this.setUser_id((Integer)objs[0]);
			}
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 1){
				this.setUser_id_text((String)objs[1]);
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getActivity_category()!=null&&this.getActivity_categoryDirtyFlag()){
			map.put("activity_category",this.getActivity_category());
		}else if(this.getActivity_categoryDirtyFlag()){
			map.put("activity_category",false);
		}
		if(this.getActivity_decoration()!=null&&this.getActivity_decorationDirtyFlag()){
			map.put("activity_decoration",this.getActivity_decoration());
		}else if(this.getActivity_decorationDirtyFlag()){
			map.put("activity_decoration",false);
		}
		if(this.getActivity_type_id()!=null&&this.getActivity_type_idDirtyFlag()){
			map.put("activity_type_id",this.getActivity_type_id());
		}else if(this.getActivity_type_idDirtyFlag()){
			map.put("activity_type_id",false);
		}
		if(this.getActivity_type_id_text()!=null&&this.getActivity_type_id_textDirtyFlag()){
			//忽略文本外键activity_type_id_text
		}else if(this.getActivity_type_id_textDirtyFlag()){
			map.put("activity_type_id",false);
		}
		if(this.getAutomated()!=null&&this.getAutomatedDirtyFlag()){
			map.put("automated",Boolean.parseBoolean(this.getAutomated()));		
		}		if(this.getCalendar_event_id()!=null&&this.getCalendar_event_idDirtyFlag()){
			map.put("calendar_event_id",this.getCalendar_event_id());
		}else if(this.getCalendar_event_idDirtyFlag()){
			map.put("calendar_event_id",false);
		}
		if(this.getCalendar_event_id_text()!=null&&this.getCalendar_event_id_textDirtyFlag()){
			//忽略文本外键calendar_event_id_text
		}else if(this.getCalendar_event_id_textDirtyFlag()){
			map.put("calendar_event_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_user_id()!=null&&this.getCreate_user_idDirtyFlag()){
			map.put("create_user_id",this.getCreate_user_id());
		}else if(this.getCreate_user_idDirtyFlag()){
			map.put("create_user_id",false);
		}
		if(this.getCreate_user_id_text()!=null&&this.getCreate_user_id_textDirtyFlag()){
			//忽略文本外键create_user_id_text
		}else if(this.getCreate_user_id_textDirtyFlag()){
			map.put("create_user_id",false);
		}
		if(this.getDate_deadline()!=null&&this.getDate_deadlineDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getDate_deadline());
			map.put("date_deadline",datetimeStr);
		}else if(this.getDate_deadlineDirtyFlag()){
			map.put("date_deadline",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getFeedback()!=null&&this.getFeedbackDirtyFlag()){
			map.put("feedback",this.getFeedback());
		}else if(this.getFeedbackDirtyFlag()){
			map.put("feedback",false);
		}
		if(this.getForce_next()!=null&&this.getForce_nextDirtyFlag()){
			map.put("force_next",Boolean.parseBoolean(this.getForce_next()));		
		}		if(this.getHas_recommended_activities()!=null&&this.getHas_recommended_activitiesDirtyFlag()){
			map.put("has_recommended_activities",Boolean.parseBoolean(this.getHas_recommended_activities()));		
		}		if(this.getIcon()!=null&&this.getIconDirtyFlag()){
			map.put("icon",this.getIcon());
		}else if(this.getIconDirtyFlag()){
			map.put("icon",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getMail_template_ids()!=null&&this.getMail_template_idsDirtyFlag()){
			map.put("mail_template_ids",this.getMail_template_ids());
		}else if(this.getMail_template_idsDirtyFlag()){
			map.put("mail_template_ids",false);
		}
		if(this.getNote()!=null&&this.getNoteDirtyFlag()){
			map.put("note",this.getNote());
		}else if(this.getNoteDirtyFlag()){
			map.put("note",false);
		}
		if(this.getNote_id()!=null&&this.getNote_idDirtyFlag()){
			map.put("note_id",this.getNote_id());
		}else if(this.getNote_idDirtyFlag()){
			map.put("note_id",false);
		}
		if(this.getNote_id_text()!=null&&this.getNote_id_textDirtyFlag()){
			//忽略文本外键note_id_text
		}else if(this.getNote_id_textDirtyFlag()){
			map.put("note_id",false);
		}
		if(this.getPrevious_activity_type_id()!=null&&this.getPrevious_activity_type_idDirtyFlag()){
			map.put("previous_activity_type_id",this.getPrevious_activity_type_id());
		}else if(this.getPrevious_activity_type_idDirtyFlag()){
			map.put("previous_activity_type_id",false);
		}
		if(this.getPrevious_activity_type_id_text()!=null&&this.getPrevious_activity_type_id_textDirtyFlag()){
			//忽略文本外键previous_activity_type_id_text
		}else if(this.getPrevious_activity_type_id_textDirtyFlag()){
			map.put("previous_activity_type_id",false);
		}
		if(this.getRecommended_activity_type_id()!=null&&this.getRecommended_activity_type_idDirtyFlag()){
			map.put("recommended_activity_type_id",this.getRecommended_activity_type_id());
		}else if(this.getRecommended_activity_type_idDirtyFlag()){
			map.put("recommended_activity_type_id",false);
		}
		if(this.getRecommended_activity_type_id_text()!=null&&this.getRecommended_activity_type_id_textDirtyFlag()){
			//忽略文本外键recommended_activity_type_id_text
		}else if(this.getRecommended_activity_type_id_textDirtyFlag()){
			map.put("recommended_activity_type_id",false);
		}
		if(this.getRes_id()!=null&&this.getRes_idDirtyFlag()){
			map.put("res_id",this.getRes_id());
		}else if(this.getRes_idDirtyFlag()){
			map.put("res_id",false);
		}
		if(this.getRes_model()!=null&&this.getRes_modelDirtyFlag()){
			map.put("res_model",this.getRes_model());
		}else if(this.getRes_modelDirtyFlag()){
			map.put("res_model",false);
		}
		if(this.getRes_model_id()!=null&&this.getRes_model_idDirtyFlag()){
			map.put("res_model_id",this.getRes_model_id());
		}else if(this.getRes_model_idDirtyFlag()){
			map.put("res_model_id",false);
		}
		if(this.getRes_name()!=null&&this.getRes_nameDirtyFlag()){
			map.put("res_name",this.getRes_name());
		}else if(this.getRes_nameDirtyFlag()){
			map.put("res_name",false);
		}
		if(this.getState()!=null&&this.getStateDirtyFlag()){
			map.put("state",this.getState());
		}else if(this.getStateDirtyFlag()){
			map.put("state",false);
		}
		if(this.getSummary()!=null&&this.getSummaryDirtyFlag()){
			map.put("summary",this.getSummary());
		}else if(this.getSummaryDirtyFlag()){
			map.put("summary",false);
		}
		if(this.getUser_id()!=null&&this.getUser_idDirtyFlag()){
			map.put("user_id",this.getUser_id());
		}else if(this.getUser_idDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getUser_id_text()!=null&&this.getUser_id_textDirtyFlag()){
			//忽略文本外键user_id_text
		}else if(this.getUser_id_textDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
