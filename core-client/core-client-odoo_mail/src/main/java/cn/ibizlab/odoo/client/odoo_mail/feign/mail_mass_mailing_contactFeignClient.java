package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_contact;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mass_mailing_contactImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_mass_mailing_contact] 服务对象接口
 */
public interface mail_mass_mailing_contactFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_contacts/removebatch")
    public mail_mass_mailing_contactImpl removeBatch(@RequestBody List<mail_mass_mailing_contactImpl> mail_mass_mailing_contacts);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_contacts/updatebatch")
    public mail_mass_mailing_contactImpl updateBatch(@RequestBody List<mail_mass_mailing_contactImpl> mail_mass_mailing_contacts);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_contacts/search")
    public Page<mail_mass_mailing_contactImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_contacts")
    public mail_mass_mailing_contactImpl create(@RequestBody mail_mass_mailing_contactImpl mail_mass_mailing_contact);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_contacts/createbatch")
    public mail_mass_mailing_contactImpl createBatch(@RequestBody List<mail_mass_mailing_contactImpl> mail_mass_mailing_contacts);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_contacts/{id}")
    public mail_mass_mailing_contactImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_contacts/{id}")
    public mail_mass_mailing_contactImpl update(@PathVariable("id") Integer id,@RequestBody mail_mass_mailing_contactImpl mail_mass_mailing_contact);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_contacts/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_contacts/select")
    public Page<mail_mass_mailing_contactImpl> select();



}
