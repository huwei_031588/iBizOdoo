package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_blacklist_mixin;
import cn.ibizlab.odoo.core.client.service.Imail_blacklist_mixinClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_blacklist_mixinImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_blacklist_mixinOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_blacklist_mixinOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_blacklist_mixin] 服务对象接口
 */
@Service
public class mail_blacklist_mixinClientServiceImpl implements Imail_blacklist_mixinClientService {
    @Autowired
    private  Imail_blacklist_mixinOdooClient  mail_blacklist_mixinOdooClient;

    public Imail_blacklist_mixin createModel() {		
		return new mail_blacklist_mixinImpl();
	}


        public void create(Imail_blacklist_mixin mail_blacklist_mixin){
this.mail_blacklist_mixinOdooClient.create(mail_blacklist_mixin) ;
        }
        
        public void createBatch(List<Imail_blacklist_mixin> mail_blacklist_mixins){
            
        }
        
        public Page<Imail_blacklist_mixin> search(SearchContext context){
            return this.mail_blacklist_mixinOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Imail_blacklist_mixin> mail_blacklist_mixins){
            
        }
        
        public void remove(Imail_blacklist_mixin mail_blacklist_mixin){
this.mail_blacklist_mixinOdooClient.remove(mail_blacklist_mixin) ;
        }
        
        public void removeBatch(List<Imail_blacklist_mixin> mail_blacklist_mixins){
            
        }
        
        public void update(Imail_blacklist_mixin mail_blacklist_mixin){
this.mail_blacklist_mixinOdooClient.update(mail_blacklist_mixin) ;
        }
        
        public void get(Imail_blacklist_mixin mail_blacklist_mixin){
            this.mail_blacklist_mixinOdooClient.get(mail_blacklist_mixin) ;
        }
        
        public Page<Imail_blacklist_mixin> select(SearchContext context){
            return null ;
        }
        

}

