package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_blacklist;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_blacklist] 服务对象客户端接口
 */
public interface Imail_blacklistOdooClient {
    
        public void createBatch(Imail_blacklist mail_blacklist);

        public void get(Imail_blacklist mail_blacklist);

        public void removeBatch(Imail_blacklist mail_blacklist);

        public void remove(Imail_blacklist mail_blacklist);

        public void update(Imail_blacklist mail_blacklist);

        public void create(Imail_blacklist mail_blacklist);

        public void updateBatch(Imail_blacklist mail_blacklist);

        public Page<Imail_blacklist> search(SearchContext context);

        public List<Imail_blacklist> select();


}