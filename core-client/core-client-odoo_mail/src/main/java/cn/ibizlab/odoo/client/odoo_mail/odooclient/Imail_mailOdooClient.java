package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_mail;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_mail] 服务对象客户端接口
 */
public interface Imail_mailOdooClient {
    
        public void removeBatch(Imail_mail mail_mail);

        public void updateBatch(Imail_mail mail_mail);

        public void remove(Imail_mail mail_mail);

        public void update(Imail_mail mail_mail);

        public void createBatch(Imail_mail mail_mail);

        public void get(Imail_mail mail_mail);

        public void create(Imail_mail mail_mail);

        public Page<Imail_mail> search(SearchContext context);

        public List<Imail_mail> select();


}