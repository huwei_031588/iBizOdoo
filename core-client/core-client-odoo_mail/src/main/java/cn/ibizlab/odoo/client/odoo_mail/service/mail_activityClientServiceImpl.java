package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_activity;
import cn.ibizlab.odoo.core.client.service.Imail_activityClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_activityImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_activityOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_activityOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_activity] 服务对象接口
 */
@Service
public class mail_activityClientServiceImpl implements Imail_activityClientService {
    @Autowired
    private  Imail_activityOdooClient  mail_activityOdooClient;

    public Imail_activity createModel() {		
		return new mail_activityImpl();
	}


        public Page<Imail_activity> search(SearchContext context){
            return this.mail_activityOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Imail_activity> mail_activities){
            
        }
        
        public void get(Imail_activity mail_activity){
            this.mail_activityOdooClient.get(mail_activity) ;
        }
        
        public void remove(Imail_activity mail_activity){
this.mail_activityOdooClient.remove(mail_activity) ;
        }
        
        public void createBatch(List<Imail_activity> mail_activities){
            
        }
        
        public void update(Imail_activity mail_activity){
this.mail_activityOdooClient.update(mail_activity) ;
        }
        
        public void create(Imail_activity mail_activity){
this.mail_activityOdooClient.create(mail_activity) ;
        }
        
        public void updateBatch(List<Imail_activity> mail_activities){
            
        }
        
        public Page<Imail_activity> select(SearchContext context){
            return null ;
        }
        

}

