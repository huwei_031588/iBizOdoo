package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_blacklist;
import cn.ibizlab.odoo.core.client.service.Imail_blacklistClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_blacklistImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_blacklistOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_blacklistOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_blacklist] 服务对象接口
 */
@Service
public class mail_blacklistClientServiceImpl implements Imail_blacklistClientService {
    @Autowired
    private  Imail_blacklistOdooClient  mail_blacklistOdooClient;

    public Imail_blacklist createModel() {		
		return new mail_blacklistImpl();
	}


        public void createBatch(List<Imail_blacklist> mail_blacklists){
            
        }
        
        public void get(Imail_blacklist mail_blacklist){
            this.mail_blacklistOdooClient.get(mail_blacklist) ;
        }
        
        public void removeBatch(List<Imail_blacklist> mail_blacklists){
            
        }
        
        public void remove(Imail_blacklist mail_blacklist){
this.mail_blacklistOdooClient.remove(mail_blacklist) ;
        }
        
        public void update(Imail_blacklist mail_blacklist){
this.mail_blacklistOdooClient.update(mail_blacklist) ;
        }
        
        public void create(Imail_blacklist mail_blacklist){
this.mail_blacklistOdooClient.create(mail_blacklist) ;
        }
        
        public void updateBatch(List<Imail_blacklist> mail_blacklists){
            
        }
        
        public Page<Imail_blacklist> search(SearchContext context){
            return this.mail_blacklistOdooClient.search(context) ;
        }
        
        public Page<Imail_blacklist> select(SearchContext context){
            return null ;
        }
        

}

