package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_statistics_report;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_statistics_reportImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_statistics_report] 服务对象接口
 */
public interface mail_statistics_reportFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_statistics_reports/{id}")
    public mail_statistics_reportImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_statistics_reports/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_statistics_reports/search")
    public Page<mail_statistics_reportImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_statistics_reports/updatebatch")
    public mail_statistics_reportImpl updateBatch(@RequestBody List<mail_statistics_reportImpl> mail_statistics_reports);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_statistics_reports/removebatch")
    public mail_statistics_reportImpl removeBatch(@RequestBody List<mail_statistics_reportImpl> mail_statistics_reports);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_statistics_reports")
    public mail_statistics_reportImpl create(@RequestBody mail_statistics_reportImpl mail_statistics_report);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_statistics_reports/createbatch")
    public mail_statistics_reportImpl createBatch(@RequestBody List<mail_statistics_reportImpl> mail_statistics_reports);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_statistics_reports/{id}")
    public mail_statistics_reportImpl update(@PathVariable("id") Integer id,@RequestBody mail_statistics_reportImpl mail_statistics_report);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_statistics_reports/select")
    public Page<mail_statistics_reportImpl> select();



}
