package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_moderation;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_moderation] 服务对象客户端接口
 */
public interface Imail_moderationOdooClient {
    
        public void remove(Imail_moderation mail_moderation);

        public void removeBatch(Imail_moderation mail_moderation);

        public Page<Imail_moderation> search(SearchContext context);

        public void update(Imail_moderation mail_moderation);

        public void get(Imail_moderation mail_moderation);

        public void create(Imail_moderation mail_moderation);

        public void createBatch(Imail_moderation mail_moderation);

        public void updateBatch(Imail_moderation mail_moderation);

        public List<Imail_moderation> select();


}