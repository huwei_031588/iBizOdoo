package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_moderation;
import cn.ibizlab.odoo.core.client.service.Imail_moderationClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_moderationImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_moderationOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_moderationOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_moderation] 服务对象接口
 */
@Service
public class mail_moderationClientServiceImpl implements Imail_moderationClientService {
    @Autowired
    private  Imail_moderationOdooClient  mail_moderationOdooClient;

    public Imail_moderation createModel() {		
		return new mail_moderationImpl();
	}


        public void remove(Imail_moderation mail_moderation){
this.mail_moderationOdooClient.remove(mail_moderation) ;
        }
        
        public void removeBatch(List<Imail_moderation> mail_moderations){
            
        }
        
        public Page<Imail_moderation> search(SearchContext context){
            return this.mail_moderationOdooClient.search(context) ;
        }
        
        public void update(Imail_moderation mail_moderation){
this.mail_moderationOdooClient.update(mail_moderation) ;
        }
        
        public void get(Imail_moderation mail_moderation){
            this.mail_moderationOdooClient.get(mail_moderation) ;
        }
        
        public void create(Imail_moderation mail_moderation){
this.mail_moderationOdooClient.create(mail_moderation) ;
        }
        
        public void createBatch(List<Imail_moderation> mail_moderations){
            
        }
        
        public void updateBatch(List<Imail_moderation> mail_moderations){
            
        }
        
        public Page<Imail_moderation> select(SearchContext context){
            return null ;
        }
        

}

