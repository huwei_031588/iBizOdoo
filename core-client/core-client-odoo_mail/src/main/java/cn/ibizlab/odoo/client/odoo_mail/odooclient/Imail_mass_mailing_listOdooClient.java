package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_list;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_mass_mailing_list] 服务对象客户端接口
 */
public interface Imail_mass_mailing_listOdooClient {
    
        public void update(Imail_mass_mailing_list mail_mass_mailing_list);

        public Page<Imail_mass_mailing_list> search(SearchContext context);

        public void create(Imail_mass_mailing_list mail_mass_mailing_list);

        public void createBatch(Imail_mass_mailing_list mail_mass_mailing_list);

        public void remove(Imail_mass_mailing_list mail_mass_mailing_list);

        public void updateBatch(Imail_mass_mailing_list mail_mass_mailing_list);

        public void removeBatch(Imail_mass_mailing_list mail_mass_mailing_list);

        public void get(Imail_mass_mailing_list mail_mass_mailing_list);

        public List<Imail_mass_mailing_list> select();


}