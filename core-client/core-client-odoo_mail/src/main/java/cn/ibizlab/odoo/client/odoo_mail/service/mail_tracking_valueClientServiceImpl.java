package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_tracking_value;
import cn.ibizlab.odoo.core.client.service.Imail_tracking_valueClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_tracking_valueImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_tracking_valueOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_tracking_valueOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_tracking_value] 服务对象接口
 */
@Service
public class mail_tracking_valueClientServiceImpl implements Imail_tracking_valueClientService {
    @Autowired
    private  Imail_tracking_valueOdooClient  mail_tracking_valueOdooClient;

    public Imail_tracking_value createModel() {		
		return new mail_tracking_valueImpl();
	}


        public Page<Imail_tracking_value> search(SearchContext context){
            return this.mail_tracking_valueOdooClient.search(context) ;
        }
        
        public void get(Imail_tracking_value mail_tracking_value){
            this.mail_tracking_valueOdooClient.get(mail_tracking_value) ;
        }
        
        public void update(Imail_tracking_value mail_tracking_value){
this.mail_tracking_valueOdooClient.update(mail_tracking_value) ;
        }
        
        public void removeBatch(List<Imail_tracking_value> mail_tracking_values){
            
        }
        
        public void remove(Imail_tracking_value mail_tracking_value){
this.mail_tracking_valueOdooClient.remove(mail_tracking_value) ;
        }
        
        public void createBatch(List<Imail_tracking_value> mail_tracking_values){
            
        }
        
        public void create(Imail_tracking_value mail_tracking_value){
this.mail_tracking_valueOdooClient.create(mail_tracking_value) ;
        }
        
        public void updateBatch(List<Imail_tracking_value> mail_tracking_values){
            
        }
        
        public Page<Imail_tracking_value> select(SearchContext context){
            return null ;
        }
        

}

