package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_contact;
import cn.ibizlab.odoo.core.client.service.Imail_mass_mailing_contactClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mass_mailing_contactImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_mass_mailing_contactOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_mass_mailing_contactOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_mass_mailing_contact] 服务对象接口
 */
@Service
public class mail_mass_mailing_contactClientServiceImpl implements Imail_mass_mailing_contactClientService {
    @Autowired
    private  Imail_mass_mailing_contactOdooClient  mail_mass_mailing_contactOdooClient;

    public Imail_mass_mailing_contact createModel() {		
		return new mail_mass_mailing_contactImpl();
	}


        public void removeBatch(List<Imail_mass_mailing_contact> mail_mass_mailing_contacts){
            
        }
        
        public void updateBatch(List<Imail_mass_mailing_contact> mail_mass_mailing_contacts){
            
        }
        
        public Page<Imail_mass_mailing_contact> search(SearchContext context){
            return this.mail_mass_mailing_contactOdooClient.search(context) ;
        }
        
        public void create(Imail_mass_mailing_contact mail_mass_mailing_contact){
this.mail_mass_mailing_contactOdooClient.create(mail_mass_mailing_contact) ;
        }
        
        public void createBatch(List<Imail_mass_mailing_contact> mail_mass_mailing_contacts){
            
        }
        
        public void get(Imail_mass_mailing_contact mail_mass_mailing_contact){
            this.mail_mass_mailing_contactOdooClient.get(mail_mass_mailing_contact) ;
        }
        
        public void update(Imail_mass_mailing_contact mail_mass_mailing_contact){
this.mail_mass_mailing_contactOdooClient.update(mail_mass_mailing_contact) ;
        }
        
        public void remove(Imail_mass_mailing_contact mail_mass_mailing_contact){
this.mail_mass_mailing_contactOdooClient.remove(mail_mass_mailing_contact) ;
        }
        
        public Page<Imail_mass_mailing_contact> select(SearchContext context){
            return null ;
        }
        

}

