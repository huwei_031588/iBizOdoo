package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_statistics_report;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_statistics_report] 服务对象客户端接口
 */
public interface Imail_statistics_reportOdooClient {
    
        public void get(Imail_statistics_report mail_statistics_report);

        public void remove(Imail_statistics_report mail_statistics_report);

        public Page<Imail_statistics_report> search(SearchContext context);

        public void updateBatch(Imail_statistics_report mail_statistics_report);

        public void removeBatch(Imail_statistics_report mail_statistics_report);

        public void create(Imail_statistics_report mail_statistics_report);

        public void createBatch(Imail_statistics_report mail_statistics_report);

        public void update(Imail_statistics_report mail_statistics_report);

        public List<Imail_statistics_report> select();


}