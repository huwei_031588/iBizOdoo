package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_followers;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_followersImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_followers] 服务对象接口
 */
public interface mail_followersFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_followers/createbatch")
    public mail_followersImpl createBatch(@RequestBody List<mail_followersImpl> mail_followers);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_followers/{id}")
    public mail_followersImpl update(@PathVariable("id") Integer id,@RequestBody mail_followersImpl mail_followers);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_followers/{id}")
    public mail_followersImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_followers/updatebatch")
    public mail_followersImpl updateBatch(@RequestBody List<mail_followersImpl> mail_followers);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_followers/removebatch")
    public mail_followersImpl removeBatch(@RequestBody List<mail_followersImpl> mail_followers);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_followers/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_followers/search")
    public Page<mail_followersImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_followers")
    public mail_followersImpl create(@RequestBody mail_followersImpl mail_followers);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_followers/select")
    public Page<mail_followersImpl> select();



}
