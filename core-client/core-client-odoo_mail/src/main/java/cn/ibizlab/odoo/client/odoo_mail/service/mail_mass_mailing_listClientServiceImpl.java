package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_list;
import cn.ibizlab.odoo.core.client.service.Imail_mass_mailing_listClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mass_mailing_listImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_mass_mailing_listOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_mass_mailing_listOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_mass_mailing_list] 服务对象接口
 */
@Service
public class mail_mass_mailing_listClientServiceImpl implements Imail_mass_mailing_listClientService {
    @Autowired
    private  Imail_mass_mailing_listOdooClient  mail_mass_mailing_listOdooClient;

    public Imail_mass_mailing_list createModel() {		
		return new mail_mass_mailing_listImpl();
	}


        public void update(Imail_mass_mailing_list mail_mass_mailing_list){
this.mail_mass_mailing_listOdooClient.update(mail_mass_mailing_list) ;
        }
        
        public Page<Imail_mass_mailing_list> search(SearchContext context){
            return this.mail_mass_mailing_listOdooClient.search(context) ;
        }
        
        public void create(Imail_mass_mailing_list mail_mass_mailing_list){
this.mail_mass_mailing_listOdooClient.create(mail_mass_mailing_list) ;
        }
        
        public void createBatch(List<Imail_mass_mailing_list> mail_mass_mailing_lists){
            
        }
        
        public void remove(Imail_mass_mailing_list mail_mass_mailing_list){
this.mail_mass_mailing_listOdooClient.remove(mail_mass_mailing_list) ;
        }
        
        public void updateBatch(List<Imail_mass_mailing_list> mail_mass_mailing_lists){
            
        }
        
        public void removeBatch(List<Imail_mass_mailing_list> mail_mass_mailing_lists){
            
        }
        
        public void get(Imail_mass_mailing_list mail_mass_mailing_list){
            this.mail_mass_mailing_listOdooClient.get(mail_mass_mailing_list) ;
        }
        
        public Page<Imail_mass_mailing_list> select(SearchContext context){
            return null ;
        }
        

}

