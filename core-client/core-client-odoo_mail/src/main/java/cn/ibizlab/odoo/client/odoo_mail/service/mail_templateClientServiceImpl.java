package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_template;
import cn.ibizlab.odoo.core.client.service.Imail_templateClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_templateImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_templateOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_templateOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_template] 服务对象接口
 */
@Service
public class mail_templateClientServiceImpl implements Imail_templateClientService {
    @Autowired
    private  Imail_templateOdooClient  mail_templateOdooClient;

    public Imail_template createModel() {		
		return new mail_templateImpl();
	}


        public void updateBatch(List<Imail_template> mail_templates){
            
        }
        
        public void create(Imail_template mail_template){
this.mail_templateOdooClient.create(mail_template) ;
        }
        
        public void createBatch(List<Imail_template> mail_templates){
            
        }
        
        public void remove(Imail_template mail_template){
this.mail_templateOdooClient.remove(mail_template) ;
        }
        
        public void update(Imail_template mail_template){
this.mail_templateOdooClient.update(mail_template) ;
        }
        
        public void get(Imail_template mail_template){
            this.mail_templateOdooClient.get(mail_template) ;
        }
        
        public Page<Imail_template> search(SearchContext context){
            return this.mail_templateOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Imail_template> mail_templates){
            
        }
        
        public Page<Imail_template> select(SearchContext context){
            return null ;
        }
        

}

