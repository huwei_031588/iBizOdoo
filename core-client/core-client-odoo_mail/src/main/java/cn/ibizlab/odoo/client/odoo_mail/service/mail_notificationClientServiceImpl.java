package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_notification;
import cn.ibizlab.odoo.core.client.service.Imail_notificationClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_notificationImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_notificationOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_notificationOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_notification] 服务对象接口
 */
@Service
public class mail_notificationClientServiceImpl implements Imail_notificationClientService {
    @Autowired
    private  Imail_notificationOdooClient  mail_notificationOdooClient;

    public Imail_notification createModel() {		
		return new mail_notificationImpl();
	}


        public void createBatch(List<Imail_notification> mail_notifications){
            
        }
        
        public void updateBatch(List<Imail_notification> mail_notifications){
            
        }
        
        public void create(Imail_notification mail_notification){
this.mail_notificationOdooClient.create(mail_notification) ;
        }
        
        public void remove(Imail_notification mail_notification){
this.mail_notificationOdooClient.remove(mail_notification) ;
        }
        
        public Page<Imail_notification> search(SearchContext context){
            return this.mail_notificationOdooClient.search(context) ;
        }
        
        public void get(Imail_notification mail_notification){
            this.mail_notificationOdooClient.get(mail_notification) ;
        }
        
        public void update(Imail_notification mail_notification){
this.mail_notificationOdooClient.update(mail_notification) ;
        }
        
        public void removeBatch(List<Imail_notification> mail_notifications){
            
        }
        
        public Page<Imail_notification> select(SearchContext context){
            return null ;
        }
        

}

