package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_channel;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_channel] 服务对象客户端接口
 */
public interface Imail_channelOdooClient {
    
        public void updateBatch(Imail_channel mail_channel);

        public void create(Imail_channel mail_channel);

        public Page<Imail_channel> search(SearchContext context);

        public void get(Imail_channel mail_channel);

        public void removeBatch(Imail_channel mail_channel);

        public void createBatch(Imail_channel mail_channel);

        public void update(Imail_channel mail_channel);

        public void remove(Imail_channel mail_channel);

        public List<Imail_channel> select();


}