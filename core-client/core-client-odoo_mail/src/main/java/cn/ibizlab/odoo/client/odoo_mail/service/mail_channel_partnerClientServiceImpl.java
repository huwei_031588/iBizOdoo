package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_channel_partner;
import cn.ibizlab.odoo.core.client.service.Imail_channel_partnerClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_channel_partnerImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_channel_partnerOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_channel_partnerOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_channel_partner] 服务对象接口
 */
@Service
public class mail_channel_partnerClientServiceImpl implements Imail_channel_partnerClientService {
    @Autowired
    private  Imail_channel_partnerOdooClient  mail_channel_partnerOdooClient;

    public Imail_channel_partner createModel() {		
		return new mail_channel_partnerImpl();
	}


        public void updateBatch(List<Imail_channel_partner> mail_channel_partners){
            
        }
        
        public void removeBatch(List<Imail_channel_partner> mail_channel_partners){
            
        }
        
        public void createBatch(List<Imail_channel_partner> mail_channel_partners){
            
        }
        
        public void create(Imail_channel_partner mail_channel_partner){
this.mail_channel_partnerOdooClient.create(mail_channel_partner) ;
        }
        
        public Page<Imail_channel_partner> search(SearchContext context){
            return this.mail_channel_partnerOdooClient.search(context) ;
        }
        
        public void update(Imail_channel_partner mail_channel_partner){
this.mail_channel_partnerOdooClient.update(mail_channel_partner) ;
        }
        
        public void remove(Imail_channel_partner mail_channel_partner){
this.mail_channel_partnerOdooClient.remove(mail_channel_partner) ;
        }
        
        public void get(Imail_channel_partner mail_channel_partner){
            this.mail_channel_partnerOdooClient.get(mail_channel_partner) ;
        }
        
        public Page<Imail_channel_partner> select(SearchContext context){
            return null ;
        }
        

}

