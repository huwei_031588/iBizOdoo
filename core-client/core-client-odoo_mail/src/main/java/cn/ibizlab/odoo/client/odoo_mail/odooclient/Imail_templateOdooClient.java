package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_template;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_template] 服务对象客户端接口
 */
public interface Imail_templateOdooClient {
    
        public void updateBatch(Imail_template mail_template);

        public void create(Imail_template mail_template);

        public void createBatch(Imail_template mail_template);

        public void remove(Imail_template mail_template);

        public void update(Imail_template mail_template);

        public void get(Imail_template mail_template);

        public Page<Imail_template> search(SearchContext context);

        public void removeBatch(Imail_template mail_template);

        public List<Imail_template> select();


}