package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_thread;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_threadImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_thread] 服务对象接口
 */
public interface mail_threadFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_threads/updatebatch")
    public mail_threadImpl updateBatch(@RequestBody List<mail_threadImpl> mail_threads);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_threads/search")
    public Page<mail_threadImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_threads/{id}")
    public mail_threadImpl update(@PathVariable("id") Integer id,@RequestBody mail_threadImpl mail_thread);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_threads/{id}")
    public mail_threadImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_threads/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_threads/createbatch")
    public mail_threadImpl createBatch(@RequestBody List<mail_threadImpl> mail_threads);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_threads/removebatch")
    public mail_threadImpl removeBatch(@RequestBody List<mail_threadImpl> mail_threads);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_threads")
    public mail_threadImpl create(@RequestBody mail_threadImpl mail_thread);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_threads/select")
    public Page<mail_threadImpl> select();



}
