package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_mail_statistics;
import cn.ibizlab.odoo.core.client.service.Imail_mail_statisticsClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mail_statisticsImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_mail_statisticsOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_mail_statisticsOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_mail_statistics] 服务对象接口
 */
@Service
public class mail_mail_statisticsClientServiceImpl implements Imail_mail_statisticsClientService {
    @Autowired
    private  Imail_mail_statisticsOdooClient  mail_mail_statisticsOdooClient;

    public Imail_mail_statistics createModel() {		
		return new mail_mail_statisticsImpl();
	}


        public void remove(Imail_mail_statistics mail_mail_statistics){
this.mail_mail_statisticsOdooClient.remove(mail_mail_statistics) ;
        }
        
        public Page<Imail_mail_statistics> search(SearchContext context){
            return this.mail_mail_statisticsOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Imail_mail_statistics> mail_mail_statistics){
            
        }
        
        public void create(Imail_mail_statistics mail_mail_statistics){
this.mail_mail_statisticsOdooClient.create(mail_mail_statistics) ;
        }
        
        public void removeBatch(List<Imail_mail_statistics> mail_mail_statistics){
            
        }
        
        public void createBatch(List<Imail_mail_statistics> mail_mail_statistics){
            
        }
        
        public void update(Imail_mail_statistics mail_mail_statistics){
this.mail_mail_statisticsOdooClient.update(mail_mail_statistics) ;
        }
        
        public void get(Imail_mail_statistics mail_mail_statistics){
            this.mail_mail_statisticsOdooClient.get(mail_mail_statistics) ;
        }
        
        public Page<Imail_mail_statistics> select(SearchContext context){
            return null ;
        }
        

}

