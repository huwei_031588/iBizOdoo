package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_activity_mixin;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_activity_mixin] 服务对象客户端接口
 */
public interface Imail_activity_mixinOdooClient {
    
        public void create(Imail_activity_mixin mail_activity_mixin);

        public void get(Imail_activity_mixin mail_activity_mixin);

        public void createBatch(Imail_activity_mixin mail_activity_mixin);

        public void update(Imail_activity_mixin mail_activity_mixin);

        public void updateBatch(Imail_activity_mixin mail_activity_mixin);

        public Page<Imail_activity_mixin> search(SearchContext context);

        public void remove(Imail_activity_mixin mail_activity_mixin);

        public void removeBatch(Imail_activity_mixin mail_activity_mixin);

        public List<Imail_activity_mixin> select();


}