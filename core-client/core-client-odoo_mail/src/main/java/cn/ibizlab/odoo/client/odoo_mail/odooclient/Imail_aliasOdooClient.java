package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_alias;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_alias] 服务对象客户端接口
 */
public interface Imail_aliasOdooClient {
    
        public void removeBatch(Imail_alias mail_alias);

        public void update(Imail_alias mail_alias);

        public void updateBatch(Imail_alias mail_alias);

        public void get(Imail_alias mail_alias);

        public void remove(Imail_alias mail_alias);

        public void create(Imail_alias mail_alias);

        public void createBatch(Imail_alias mail_alias);

        public Page<Imail_alias> search(SearchContext context);

        public List<Imail_alias> select();


}