package cn.ibizlab.odoo.client.odoo_gamification.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Igamification_goal;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[gamification_goal] 服务对象客户端接口
 */
public interface Igamification_goalOdooClient {
    
        public void updateBatch(Igamification_goal gamification_goal);

        public void removeBatch(Igamification_goal gamification_goal);

        public Page<Igamification_goal> search(SearchContext context);

        public void get(Igamification_goal gamification_goal);

        public void createBatch(Igamification_goal gamification_goal);

        public void update(Igamification_goal gamification_goal);

        public void create(Igamification_goal gamification_goal);

        public void remove(Igamification_goal gamification_goal);

        public List<Igamification_goal> select();


}