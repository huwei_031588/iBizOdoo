package cn.ibizlab.odoo.client.odoo_gamification.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Igamification_goal;
import cn.ibizlab.odoo.core.client.service.Igamification_goalClientService;
import cn.ibizlab.odoo.client.odoo_gamification.model.gamification_goalImpl;
import cn.ibizlab.odoo.client.odoo_gamification.odooclient.Igamification_goalOdooClient;
import cn.ibizlab.odoo.client.odoo_gamification.odooclient.impl.gamification_goalOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[gamification_goal] 服务对象接口
 */
@Service
public class gamification_goalClientServiceImpl implements Igamification_goalClientService {
    @Autowired
    private  Igamification_goalOdooClient  gamification_goalOdooClient;

    public Igamification_goal createModel() {		
		return new gamification_goalImpl();
	}


        public void updateBatch(List<Igamification_goal> gamification_goals){
            
        }
        
        public void removeBatch(List<Igamification_goal> gamification_goals){
            
        }
        
        public Page<Igamification_goal> search(SearchContext context){
            return this.gamification_goalOdooClient.search(context) ;
        }
        
        public void get(Igamification_goal gamification_goal){
            this.gamification_goalOdooClient.get(gamification_goal) ;
        }
        
        public void createBatch(List<Igamification_goal> gamification_goals){
            
        }
        
        public void update(Igamification_goal gamification_goal){
this.gamification_goalOdooClient.update(gamification_goal) ;
        }
        
        public void create(Igamification_goal gamification_goal){
this.gamification_goalOdooClient.create(gamification_goal) ;
        }
        
        public void remove(Igamification_goal gamification_goal){
this.gamification_goalOdooClient.remove(gamification_goal) ;
        }
        
        public Page<Igamification_goal> select(SearchContext context){
            return null ;
        }
        

}

