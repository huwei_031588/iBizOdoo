package cn.ibizlab.odoo.client.odoo_gamification.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Igamification_goal_definition;
import cn.ibizlab.odoo.core.client.service.Igamification_goal_definitionClientService;
import cn.ibizlab.odoo.client.odoo_gamification.model.gamification_goal_definitionImpl;
import cn.ibizlab.odoo.client.odoo_gamification.odooclient.Igamification_goal_definitionOdooClient;
import cn.ibizlab.odoo.client.odoo_gamification.odooclient.impl.gamification_goal_definitionOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[gamification_goal_definition] 服务对象接口
 */
@Service
public class gamification_goal_definitionClientServiceImpl implements Igamification_goal_definitionClientService {
    @Autowired
    private  Igamification_goal_definitionOdooClient  gamification_goal_definitionOdooClient;

    public Igamification_goal_definition createModel() {		
		return new gamification_goal_definitionImpl();
	}


        public void updateBatch(List<Igamification_goal_definition> gamification_goal_definitions){
            
        }
        
        public void update(Igamification_goal_definition gamification_goal_definition){
this.gamification_goal_definitionOdooClient.update(gamification_goal_definition) ;
        }
        
        public void get(Igamification_goal_definition gamification_goal_definition){
            this.gamification_goal_definitionOdooClient.get(gamification_goal_definition) ;
        }
        
        public Page<Igamification_goal_definition> search(SearchContext context){
            return this.gamification_goal_definitionOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Igamification_goal_definition> gamification_goal_definitions){
            
        }
        
        public void create(Igamification_goal_definition gamification_goal_definition){
this.gamification_goal_definitionOdooClient.create(gamification_goal_definition) ;
        }
        
        public void remove(Igamification_goal_definition gamification_goal_definition){
this.gamification_goal_definitionOdooClient.remove(gamification_goal_definition) ;
        }
        
        public void createBatch(List<Igamification_goal_definition> gamification_goal_definitions){
            
        }
        
        public Page<Igamification_goal_definition> select(SearchContext context){
            return null ;
        }
        

}

