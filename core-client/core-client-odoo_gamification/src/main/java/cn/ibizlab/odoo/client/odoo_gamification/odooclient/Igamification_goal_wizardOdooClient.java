package cn.ibizlab.odoo.client.odoo_gamification.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Igamification_goal_wizard;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[gamification_goal_wizard] 服务对象客户端接口
 */
public interface Igamification_goal_wizardOdooClient {
    
        public void removeBatch(Igamification_goal_wizard gamification_goal_wizard);

        public void update(Igamification_goal_wizard gamification_goal_wizard);

        public void create(Igamification_goal_wizard gamification_goal_wizard);

        public void get(Igamification_goal_wizard gamification_goal_wizard);

        public void createBatch(Igamification_goal_wizard gamification_goal_wizard);

        public void updateBatch(Igamification_goal_wizard gamification_goal_wizard);

        public Page<Igamification_goal_wizard> search(SearchContext context);

        public void remove(Igamification_goal_wizard gamification_goal_wizard);

        public List<Igamification_goal_wizard> select();


}