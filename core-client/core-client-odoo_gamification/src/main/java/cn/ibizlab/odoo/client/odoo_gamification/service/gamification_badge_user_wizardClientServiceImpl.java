package cn.ibizlab.odoo.client.odoo_gamification.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Igamification_badge_user_wizard;
import cn.ibizlab.odoo.core.client.service.Igamification_badge_user_wizardClientService;
import cn.ibizlab.odoo.client.odoo_gamification.model.gamification_badge_user_wizardImpl;
import cn.ibizlab.odoo.client.odoo_gamification.odooclient.Igamification_badge_user_wizardOdooClient;
import cn.ibizlab.odoo.client.odoo_gamification.odooclient.impl.gamification_badge_user_wizardOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[gamification_badge_user_wizard] 服务对象接口
 */
@Service
public class gamification_badge_user_wizardClientServiceImpl implements Igamification_badge_user_wizardClientService {
    @Autowired
    private  Igamification_badge_user_wizardOdooClient  gamification_badge_user_wizardOdooClient;

    public Igamification_badge_user_wizard createModel() {		
		return new gamification_badge_user_wizardImpl();
	}


        public void updateBatch(List<Igamification_badge_user_wizard> gamification_badge_user_wizards){
            
        }
        
        public void removeBatch(List<Igamification_badge_user_wizard> gamification_badge_user_wizards){
            
        }
        
        public void get(Igamification_badge_user_wizard gamification_badge_user_wizard){
            this.gamification_badge_user_wizardOdooClient.get(gamification_badge_user_wizard) ;
        }
        
        public void remove(Igamification_badge_user_wizard gamification_badge_user_wizard){
this.gamification_badge_user_wizardOdooClient.remove(gamification_badge_user_wizard) ;
        }
        
        public void create(Igamification_badge_user_wizard gamification_badge_user_wizard){
this.gamification_badge_user_wizardOdooClient.create(gamification_badge_user_wizard) ;
        }
        
        public Page<Igamification_badge_user_wizard> search(SearchContext context){
            return this.gamification_badge_user_wizardOdooClient.search(context) ;
        }
        
        public void update(Igamification_badge_user_wizard gamification_badge_user_wizard){
this.gamification_badge_user_wizardOdooClient.update(gamification_badge_user_wizard) ;
        }
        
        public void createBatch(List<Igamification_badge_user_wizard> gamification_badge_user_wizards){
            
        }
        
        public Page<Igamification_badge_user_wizard> select(SearchContext context){
            return null ;
        }
        

}

