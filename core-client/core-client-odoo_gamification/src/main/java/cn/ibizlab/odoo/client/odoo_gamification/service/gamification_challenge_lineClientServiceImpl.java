package cn.ibizlab.odoo.client.odoo_gamification.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Igamification_challenge_line;
import cn.ibizlab.odoo.core.client.service.Igamification_challenge_lineClientService;
import cn.ibizlab.odoo.client.odoo_gamification.model.gamification_challenge_lineImpl;
import cn.ibizlab.odoo.client.odoo_gamification.odooclient.Igamification_challenge_lineOdooClient;
import cn.ibizlab.odoo.client.odoo_gamification.odooclient.impl.gamification_challenge_lineOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[gamification_challenge_line] 服务对象接口
 */
@Service
public class gamification_challenge_lineClientServiceImpl implements Igamification_challenge_lineClientService {
    @Autowired
    private  Igamification_challenge_lineOdooClient  gamification_challenge_lineOdooClient;

    public Igamification_challenge_line createModel() {		
		return new gamification_challenge_lineImpl();
	}


        public void create(Igamification_challenge_line gamification_challenge_line){
this.gamification_challenge_lineOdooClient.create(gamification_challenge_line) ;
        }
        
        public void update(Igamification_challenge_line gamification_challenge_line){
this.gamification_challenge_lineOdooClient.update(gamification_challenge_line) ;
        }
        
        public Page<Igamification_challenge_line> search(SearchContext context){
            return this.gamification_challenge_lineOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Igamification_challenge_line> gamification_challenge_lines){
            
        }
        
        public void removeBatch(List<Igamification_challenge_line> gamification_challenge_lines){
            
        }
        
        public void get(Igamification_challenge_line gamification_challenge_line){
            this.gamification_challenge_lineOdooClient.get(gamification_challenge_line) ;
        }
        
        public void createBatch(List<Igamification_challenge_line> gamification_challenge_lines){
            
        }
        
        public void remove(Igamification_challenge_line gamification_challenge_line){
this.gamification_challenge_lineOdooClient.remove(gamification_challenge_line) ;
        }
        
        public Page<Igamification_challenge_line> select(SearchContext context){
            return null ;
        }
        

}

