package cn.ibizlab.odoo.client.odoo_base_import.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_char_readonly;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_char_readonlyImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_import_tests_models_char_readonly] 服务对象接口
 */
public interface base_import_tests_models_char_readonlyFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_char_readonlies/{id}")
    public base_import_tests_models_char_readonlyImpl update(@PathVariable("id") Integer id,@RequestBody base_import_tests_models_char_readonlyImpl base_import_tests_models_char_readonly);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_char_readonlies/createbatch")
    public base_import_tests_models_char_readonlyImpl createBatch(@RequestBody List<base_import_tests_models_char_readonlyImpl> base_import_tests_models_char_readonlies);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_char_readonlies/removebatch")
    public base_import_tests_models_char_readonlyImpl removeBatch(@RequestBody List<base_import_tests_models_char_readonlyImpl> base_import_tests_models_char_readonlies);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_char_readonlies")
    public base_import_tests_models_char_readonlyImpl create(@RequestBody base_import_tests_models_char_readonlyImpl base_import_tests_models_char_readonly);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_char_readonlies/updatebatch")
    public base_import_tests_models_char_readonlyImpl updateBatch(@RequestBody List<base_import_tests_models_char_readonlyImpl> base_import_tests_models_char_readonlies);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_char_readonlies/search")
    public Page<base_import_tests_models_char_readonlyImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_char_readonlies/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_char_readonlies/{id}")
    public base_import_tests_models_char_readonlyImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_char_readonlies/select")
    public Page<base_import_tests_models_char_readonlyImpl> select();



}
