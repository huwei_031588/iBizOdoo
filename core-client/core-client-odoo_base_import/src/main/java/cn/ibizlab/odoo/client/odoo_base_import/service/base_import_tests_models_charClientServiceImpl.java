package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_char;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_charClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_charImpl;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.Ibase_import_tests_models_charOdooClient;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.impl.base_import_tests_models_charOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_import_tests_models_char] 服务对象接口
 */
@Service
public class base_import_tests_models_charClientServiceImpl implements Ibase_import_tests_models_charClientService {
    @Autowired
    private  Ibase_import_tests_models_charOdooClient  base_import_tests_models_charOdooClient;

    public Ibase_import_tests_models_char createModel() {		
		return new base_import_tests_models_charImpl();
	}


        public void create(Ibase_import_tests_models_char base_import_tests_models_char){
this.base_import_tests_models_charOdooClient.create(base_import_tests_models_char) ;
        }
        
        public void createBatch(List<Ibase_import_tests_models_char> base_import_tests_models_chars){
            
        }
        
        public void remove(Ibase_import_tests_models_char base_import_tests_models_char){
this.base_import_tests_models_charOdooClient.remove(base_import_tests_models_char) ;
        }
        
        public void get(Ibase_import_tests_models_char base_import_tests_models_char){
            this.base_import_tests_models_charOdooClient.get(base_import_tests_models_char) ;
        }
        
        public void removeBatch(List<Ibase_import_tests_models_char> base_import_tests_models_chars){
            
        }
        
        public void update(Ibase_import_tests_models_char base_import_tests_models_char){
this.base_import_tests_models_charOdooClient.update(base_import_tests_models_char) ;
        }
        
        public void updateBatch(List<Ibase_import_tests_models_char> base_import_tests_models_chars){
            
        }
        
        public Page<Ibase_import_tests_models_char> search(SearchContext context){
            return this.base_import_tests_models_charOdooClient.search(context) ;
        }
        
        public Page<Ibase_import_tests_models_char> select(SearchContext context){
            return null ;
        }
        

}

