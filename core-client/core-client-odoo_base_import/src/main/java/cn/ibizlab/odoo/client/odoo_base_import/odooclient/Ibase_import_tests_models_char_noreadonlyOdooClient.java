package cn.ibizlab.odoo.client.odoo_base_import.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_char_noreadonly;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_import_tests_models_char_noreadonly] 服务对象客户端接口
 */
public interface Ibase_import_tests_models_char_noreadonlyOdooClient {
    
        public void update(Ibase_import_tests_models_char_noreadonly base_import_tests_models_char_noreadonly);

        public void removeBatch(Ibase_import_tests_models_char_noreadonly base_import_tests_models_char_noreadonly);

        public void createBatch(Ibase_import_tests_models_char_noreadonly base_import_tests_models_char_noreadonly);

        public void updateBatch(Ibase_import_tests_models_char_noreadonly base_import_tests_models_char_noreadonly);

        public void get(Ibase_import_tests_models_char_noreadonly base_import_tests_models_char_noreadonly);

        public void create(Ibase_import_tests_models_char_noreadonly base_import_tests_models_char_noreadonly);

        public void remove(Ibase_import_tests_models_char_noreadonly base_import_tests_models_char_noreadonly);

        public Page<Ibase_import_tests_models_char_noreadonly> search(SearchContext context);

        public List<Ibase_import_tests_models_char_noreadonly> select();


}