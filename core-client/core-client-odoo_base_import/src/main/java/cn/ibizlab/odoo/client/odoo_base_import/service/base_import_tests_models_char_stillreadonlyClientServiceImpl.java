package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_char_stillreadonly;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_char_stillreadonlyClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_char_stillreadonlyImpl;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.Ibase_import_tests_models_char_stillreadonlyOdooClient;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.impl.base_import_tests_models_char_stillreadonlyOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_import_tests_models_char_stillreadonly] 服务对象接口
 */
@Service
public class base_import_tests_models_char_stillreadonlyClientServiceImpl implements Ibase_import_tests_models_char_stillreadonlyClientService {
    @Autowired
    private  Ibase_import_tests_models_char_stillreadonlyOdooClient  base_import_tests_models_char_stillreadonlyOdooClient;

    public Ibase_import_tests_models_char_stillreadonly createModel() {		
		return new base_import_tests_models_char_stillreadonlyImpl();
	}


        public void remove(Ibase_import_tests_models_char_stillreadonly base_import_tests_models_char_stillreadonly){
this.base_import_tests_models_char_stillreadonlyOdooClient.remove(base_import_tests_models_char_stillreadonly) ;
        }
        
        public void get(Ibase_import_tests_models_char_stillreadonly base_import_tests_models_char_stillreadonly){
            this.base_import_tests_models_char_stillreadonlyOdooClient.get(base_import_tests_models_char_stillreadonly) ;
        }
        
        public Page<Ibase_import_tests_models_char_stillreadonly> search(SearchContext context){
            return this.base_import_tests_models_char_stillreadonlyOdooClient.search(context) ;
        }
        
        public void create(Ibase_import_tests_models_char_stillreadonly base_import_tests_models_char_stillreadonly){
this.base_import_tests_models_char_stillreadonlyOdooClient.create(base_import_tests_models_char_stillreadonly) ;
        }
        
        public void removeBatch(List<Ibase_import_tests_models_char_stillreadonly> base_import_tests_models_char_stillreadonlies){
            
        }
        
        public void updateBatch(List<Ibase_import_tests_models_char_stillreadonly> base_import_tests_models_char_stillreadonlies){
            
        }
        
        public void createBatch(List<Ibase_import_tests_models_char_stillreadonly> base_import_tests_models_char_stillreadonlies){
            
        }
        
        public void update(Ibase_import_tests_models_char_stillreadonly base_import_tests_models_char_stillreadonly){
this.base_import_tests_models_char_stillreadonlyOdooClient.update(base_import_tests_models_char_stillreadonly) ;
        }
        
        public Page<Ibase_import_tests_models_char_stillreadonly> select(SearchContext context){
            return null ;
        }
        

}

