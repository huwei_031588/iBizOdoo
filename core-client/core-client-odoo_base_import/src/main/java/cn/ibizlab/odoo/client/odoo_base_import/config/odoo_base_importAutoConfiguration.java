package cn.ibizlab.odoo.client.odoo_base_import.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ConditionalOnClass(odoo_base_importClientConfiguration.class)
@ConditionalOnWebApplication
@EnableConfigurationProperties(odoo_base_importClientProperties.class)
@Import({
    FeignClientsConfiguration.class
})
public class odoo_base_importAutoConfiguration {

}
