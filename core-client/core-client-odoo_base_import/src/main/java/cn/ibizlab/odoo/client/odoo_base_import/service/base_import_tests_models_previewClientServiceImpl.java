package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_preview;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_previewClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_previewImpl;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.Ibase_import_tests_models_previewOdooClient;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.impl.base_import_tests_models_previewOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_import_tests_models_preview] 服务对象接口
 */
@Service
public class base_import_tests_models_previewClientServiceImpl implements Ibase_import_tests_models_previewClientService {
    @Autowired
    private  Ibase_import_tests_models_previewOdooClient  base_import_tests_models_previewOdooClient;

    public Ibase_import_tests_models_preview createModel() {		
		return new base_import_tests_models_previewImpl();
	}


        public Page<Ibase_import_tests_models_preview> search(SearchContext context){
            return this.base_import_tests_models_previewOdooClient.search(context) ;
        }
        
        public void createBatch(List<Ibase_import_tests_models_preview> base_import_tests_models_previews){
            
        }
        
        public void updateBatch(List<Ibase_import_tests_models_preview> base_import_tests_models_previews){
            
        }
        
        public void create(Ibase_import_tests_models_preview base_import_tests_models_preview){
this.base_import_tests_models_previewOdooClient.create(base_import_tests_models_preview) ;
        }
        
        public void update(Ibase_import_tests_models_preview base_import_tests_models_preview){
this.base_import_tests_models_previewOdooClient.update(base_import_tests_models_preview) ;
        }
        
        public void get(Ibase_import_tests_models_preview base_import_tests_models_preview){
            this.base_import_tests_models_previewOdooClient.get(base_import_tests_models_preview) ;
        }
        
        public void removeBatch(List<Ibase_import_tests_models_preview> base_import_tests_models_previews){
            
        }
        
        public void remove(Ibase_import_tests_models_preview base_import_tests_models_preview){
this.base_import_tests_models_previewOdooClient.remove(base_import_tests_models_preview) ;
        }
        
        public Page<Ibase_import_tests_models_preview> select(SearchContext context){
            return null ;
        }
        

}

