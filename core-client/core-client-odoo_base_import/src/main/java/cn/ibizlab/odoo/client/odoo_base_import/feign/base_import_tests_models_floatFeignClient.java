package cn.ibizlab.odoo.client.odoo_base_import.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_float;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_floatImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_import_tests_models_float] 服务对象接口
 */
public interface base_import_tests_models_floatFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_floats")
    public base_import_tests_models_floatImpl create(@RequestBody base_import_tests_models_floatImpl base_import_tests_models_float);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_floats/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_floats/{id}")
    public base_import_tests_models_floatImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_floats/createbatch")
    public base_import_tests_models_floatImpl createBatch(@RequestBody List<base_import_tests_models_floatImpl> base_import_tests_models_floats);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_floats/removebatch")
    public base_import_tests_models_floatImpl removeBatch(@RequestBody List<base_import_tests_models_floatImpl> base_import_tests_models_floats);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_floats/{id}")
    public base_import_tests_models_floatImpl update(@PathVariable("id") Integer id,@RequestBody base_import_tests_models_floatImpl base_import_tests_models_float);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_floats/updatebatch")
    public base_import_tests_models_floatImpl updateBatch(@RequestBody List<base_import_tests_models_floatImpl> base_import_tests_models_floats);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_floats/search")
    public Page<base_import_tests_models_floatImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_floats/select")
    public Page<base_import_tests_models_floatImpl> select();



}
