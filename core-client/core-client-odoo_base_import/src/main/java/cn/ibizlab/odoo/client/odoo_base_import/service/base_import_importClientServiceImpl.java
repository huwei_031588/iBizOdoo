package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_import;
import cn.ibizlab.odoo.core.client.service.Ibase_import_importClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_importImpl;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.Ibase_import_importOdooClient;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.impl.base_import_importOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_import_import] 服务对象接口
 */
@Service
public class base_import_importClientServiceImpl implements Ibase_import_importClientService {
    @Autowired
    private  Ibase_import_importOdooClient  base_import_importOdooClient;

    public Ibase_import_import createModel() {		
		return new base_import_importImpl();
	}


        public void remove(Ibase_import_import base_import_import){
this.base_import_importOdooClient.remove(base_import_import) ;
        }
        
        public void update(Ibase_import_import base_import_import){
this.base_import_importOdooClient.update(base_import_import) ;
        }
        
        public Page<Ibase_import_import> search(SearchContext context){
            return this.base_import_importOdooClient.search(context) ;
        }
        
        public void create(Ibase_import_import base_import_import){
this.base_import_importOdooClient.create(base_import_import) ;
        }
        
        public void get(Ibase_import_import base_import_import){
            this.base_import_importOdooClient.get(base_import_import) ;
        }
        
        public void updateBatch(List<Ibase_import_import> base_import_imports){
            
        }
        
        public void removeBatch(List<Ibase_import_import> base_import_imports){
            
        }
        
        public void createBatch(List<Ibase_import_import> base_import_imports){
            
        }
        
        public Page<Ibase_import_import> select(SearchContext context){
            return null ;
        }
        

}

