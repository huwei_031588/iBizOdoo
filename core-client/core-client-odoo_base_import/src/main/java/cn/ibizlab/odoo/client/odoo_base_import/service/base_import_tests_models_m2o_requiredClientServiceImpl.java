package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_m2o_required;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_m2o_requiredClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_m2o_requiredImpl;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.Ibase_import_tests_models_m2o_requiredOdooClient;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.impl.base_import_tests_models_m2o_requiredOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_import_tests_models_m2o_required] 服务对象接口
 */
@Service
public class base_import_tests_models_m2o_requiredClientServiceImpl implements Ibase_import_tests_models_m2o_requiredClientService {
    @Autowired
    private  Ibase_import_tests_models_m2o_requiredOdooClient  base_import_tests_models_m2o_requiredOdooClient;

    public Ibase_import_tests_models_m2o_required createModel() {		
		return new base_import_tests_models_m2o_requiredImpl();
	}


        public Page<Ibase_import_tests_models_m2o_required> search(SearchContext context){
            return this.base_import_tests_models_m2o_requiredOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Ibase_import_tests_models_m2o_required> base_import_tests_models_m2o_requireds){
            
        }
        
        public void create(Ibase_import_tests_models_m2o_required base_import_tests_models_m2o_required){
this.base_import_tests_models_m2o_requiredOdooClient.create(base_import_tests_models_m2o_required) ;
        }
        
        public void removeBatch(List<Ibase_import_tests_models_m2o_required> base_import_tests_models_m2o_requireds){
            
        }
        
        public void remove(Ibase_import_tests_models_m2o_required base_import_tests_models_m2o_required){
this.base_import_tests_models_m2o_requiredOdooClient.remove(base_import_tests_models_m2o_required) ;
        }
        
        public void createBatch(List<Ibase_import_tests_models_m2o_required> base_import_tests_models_m2o_requireds){
            
        }
        
        public void update(Ibase_import_tests_models_m2o_required base_import_tests_models_m2o_required){
this.base_import_tests_models_m2o_requiredOdooClient.update(base_import_tests_models_m2o_required) ;
        }
        
        public void get(Ibase_import_tests_models_m2o_required base_import_tests_models_m2o_required){
            this.base_import_tests_models_m2o_requiredOdooClient.get(base_import_tests_models_m2o_required) ;
        }
        
        public Page<Ibase_import_tests_models_m2o_required> select(SearchContext context){
            return null ;
        }
        

}

