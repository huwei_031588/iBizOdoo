package cn.ibizlab.odoo.client.odoo_fetchmail.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ifetchmail_server;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[fetchmail_server] 对象
 */
public class fetchmail_serverImpl implements Ifetchmail_server,Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 保存附件
     */
    public String attach;

    @JsonIgnore
    public boolean attachDirtyFlag;
    
    /**
     * 配置
     */
    public String configuration;

    @JsonIgnore
    public boolean configurationDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 最后收取日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * SSL/TLS
     */
    public String is_ssl;

    @JsonIgnore
    public boolean is_sslDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 创建新记录
     */
    public Integer object_id;

    @JsonIgnore
    public boolean object_idDirtyFlag;
    
    /**
     * 保留原件
     */
    public String original;

    @JsonIgnore
    public boolean originalDirtyFlag;
    
    /**
     * 密码
     */
    public String password;

    @JsonIgnore
    public boolean passwordDirtyFlag;
    
    /**
     * 端口
     */
    public Integer port;

    @JsonIgnore
    public boolean portDirtyFlag;
    
    /**
     * 服务器优先级
     */
    public Integer priority;

    @JsonIgnore
    public boolean priorityDirtyFlag;
    
    /**
     * 脚本
     */
    public String script;

    @JsonIgnore
    public boolean scriptDirtyFlag;
    
    /**
     * 服务器名称
     */
    public String server;

    @JsonIgnore
    public boolean serverDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 服务器类型
     */
    public String type;

    @JsonIgnore
    public boolean typeDirtyFlag;
    
    /**
     * 用户名
     */
    public String user;

    @JsonIgnore
    public boolean userDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [保存附件]
     */
    @JsonProperty("attach")
    public String getAttach(){
        return this.attach ;
    }

    /**
     * 设置 [保存附件]
     */
    @JsonProperty("attach")
    public void setAttach(String  attach){
        this.attach = attach ;
        this.attachDirtyFlag = true ;
    }

     /**
     * 获取 [保存附件]脏标记
     */
    @JsonIgnore
    public boolean getAttachDirtyFlag(){
        return this.attachDirtyFlag ;
    }   

    /**
     * 获取 [配置]
     */
    @JsonProperty("configuration")
    public String getConfiguration(){
        return this.configuration ;
    }

    /**
     * 设置 [配置]
     */
    @JsonProperty("configuration")
    public void setConfiguration(String  configuration){
        this.configuration = configuration ;
        this.configurationDirtyFlag = true ;
    }

     /**
     * 获取 [配置]脏标记
     */
    @JsonIgnore
    public boolean getConfigurationDirtyFlag(){
        return this.configurationDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后收取日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [最后收取日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后收取日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [SSL/TLS]
     */
    @JsonProperty("is_ssl")
    public String getIs_ssl(){
        return this.is_ssl ;
    }

    /**
     * 设置 [SSL/TLS]
     */
    @JsonProperty("is_ssl")
    public void setIs_ssl(String  is_ssl){
        this.is_ssl = is_ssl ;
        this.is_sslDirtyFlag = true ;
    }

     /**
     * 获取 [SSL/TLS]脏标记
     */
    @JsonIgnore
    public boolean getIs_sslDirtyFlag(){
        return this.is_sslDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [创建新记录]
     */
    @JsonProperty("object_id")
    public Integer getObject_id(){
        return this.object_id ;
    }

    /**
     * 设置 [创建新记录]
     */
    @JsonProperty("object_id")
    public void setObject_id(Integer  object_id){
        this.object_id = object_id ;
        this.object_idDirtyFlag = true ;
    }

     /**
     * 获取 [创建新记录]脏标记
     */
    @JsonIgnore
    public boolean getObject_idDirtyFlag(){
        return this.object_idDirtyFlag ;
    }   

    /**
     * 获取 [保留原件]
     */
    @JsonProperty("original")
    public String getOriginal(){
        return this.original ;
    }

    /**
     * 设置 [保留原件]
     */
    @JsonProperty("original")
    public void setOriginal(String  original){
        this.original = original ;
        this.originalDirtyFlag = true ;
    }

     /**
     * 获取 [保留原件]脏标记
     */
    @JsonIgnore
    public boolean getOriginalDirtyFlag(){
        return this.originalDirtyFlag ;
    }   

    /**
     * 获取 [密码]
     */
    @JsonProperty("password")
    public String getPassword(){
        return this.password ;
    }

    /**
     * 设置 [密码]
     */
    @JsonProperty("password")
    public void setPassword(String  password){
        this.password = password ;
        this.passwordDirtyFlag = true ;
    }

     /**
     * 获取 [密码]脏标记
     */
    @JsonIgnore
    public boolean getPasswordDirtyFlag(){
        return this.passwordDirtyFlag ;
    }   

    /**
     * 获取 [端口]
     */
    @JsonProperty("port")
    public Integer getPort(){
        return this.port ;
    }

    /**
     * 设置 [端口]
     */
    @JsonProperty("port")
    public void setPort(Integer  port){
        this.port = port ;
        this.portDirtyFlag = true ;
    }

     /**
     * 获取 [端口]脏标记
     */
    @JsonIgnore
    public boolean getPortDirtyFlag(){
        return this.portDirtyFlag ;
    }   

    /**
     * 获取 [服务器优先级]
     */
    @JsonProperty("priority")
    public Integer getPriority(){
        return this.priority ;
    }

    /**
     * 设置 [服务器优先级]
     */
    @JsonProperty("priority")
    public void setPriority(Integer  priority){
        this.priority = priority ;
        this.priorityDirtyFlag = true ;
    }

     /**
     * 获取 [服务器优先级]脏标记
     */
    @JsonIgnore
    public boolean getPriorityDirtyFlag(){
        return this.priorityDirtyFlag ;
    }   

    /**
     * 获取 [脚本]
     */
    @JsonProperty("script")
    public String getScript(){
        return this.script ;
    }

    /**
     * 设置 [脚本]
     */
    @JsonProperty("script")
    public void setScript(String  script){
        this.script = script ;
        this.scriptDirtyFlag = true ;
    }

     /**
     * 获取 [脚本]脏标记
     */
    @JsonIgnore
    public boolean getScriptDirtyFlag(){
        return this.scriptDirtyFlag ;
    }   

    /**
     * 获取 [服务器名称]
     */
    @JsonProperty("server")
    public String getServer(){
        return this.server ;
    }

    /**
     * 设置 [服务器名称]
     */
    @JsonProperty("server")
    public void setServer(String  server){
        this.server = server ;
        this.serverDirtyFlag = true ;
    }

     /**
     * 获取 [服务器名称]脏标记
     */
    @JsonIgnore
    public boolean getServerDirtyFlag(){
        return this.serverDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [服务器类型]
     */
    @JsonProperty("type")
    public String getType(){
        return this.type ;
    }

    /**
     * 设置 [服务器类型]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

     /**
     * 获取 [服务器类型]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return this.typeDirtyFlag ;
    }   

    /**
     * 获取 [用户名]
     */
    @JsonProperty("user")
    public String getUser(){
        return this.user ;
    }

    /**
     * 设置 [用户名]
     */
    @JsonProperty("user")
    public void setUser(String  user){
        this.user = user ;
        this.userDirtyFlag = true ;
    }

     /**
     * 获取 [用户名]脏标记
     */
    @JsonIgnore
    public boolean getUserDirtyFlag(){
        return this.userDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(map.get("active") instanceof Boolean){
			this.setActive(((Boolean)map.get("active"))? "true" : "false");
		}
		if(map.get("attach") instanceof Boolean){
			this.setAttach(((Boolean)map.get("attach"))? "true" : "false");
		}
		if(!(map.get("configuration") instanceof Boolean)&& map.get("configuration")!=null){
			this.setConfiguration((String)map.get("configuration"));
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("date") instanceof Boolean)&& map.get("date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date"));
   			this.setDate(new Timestamp(parse.getTime()));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(map.get("is_ssl") instanceof Boolean){
			this.setIs_ssl(((Boolean)map.get("is_ssl"))? "true" : "false");
		}
		if(!(map.get("message_ids") instanceof Boolean)&& map.get("message_ids")!=null){
			Object[] objs = (Object[])map.get("message_ids");
			if(objs.length > 0){
				Integer[] message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_ids(Arrays.toString(message_ids));
			}
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("object_id") instanceof Boolean)&& map.get("object_id")!=null){
			Object[] objs = (Object[])map.get("object_id");
			if(objs.length > 0){
				this.setObject_id((Integer)objs[0]);
			}
		}
		if(map.get("original") instanceof Boolean){
			this.setOriginal(((Boolean)map.get("original"))? "true" : "false");
		}
		if(!(map.get("password") instanceof Boolean)&& map.get("password")!=null){
			this.setPassword((String)map.get("password"));
		}
		if(!(map.get("port") instanceof Boolean)&& map.get("port")!=null){
			this.setPort((Integer)map.get("port"));
		}
		if(!(map.get("priority") instanceof Boolean)&& map.get("priority")!=null){
			this.setPriority((Integer)map.get("priority"));
		}
		if(!(map.get("script") instanceof Boolean)&& map.get("script")!=null){
			this.setScript((String)map.get("script"));
		}
		if(!(map.get("server") instanceof Boolean)&& map.get("server")!=null){
			this.setServer((String)map.get("server"));
		}
		if(!(map.get("state") instanceof Boolean)&& map.get("state")!=null){
			this.setState((String)map.get("state"));
		}
		if(!(map.get("type") instanceof Boolean)&& map.get("type")!=null){
			this.setType((String)map.get("type"));
		}
		if(!(map.get("user") instanceof Boolean)&& map.get("user")!=null){
			this.setUser((String)map.get("user"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getActive()!=null&&this.getActiveDirtyFlag()){
			map.put("active",Boolean.parseBoolean(this.getActive()));		
		}		if(this.getAttach()!=null&&this.getAttachDirtyFlag()){
			map.put("attach",Boolean.parseBoolean(this.getAttach()));		
		}		if(this.getConfiguration()!=null&&this.getConfigurationDirtyFlag()){
			map.put("configuration",this.getConfiguration());
		}else if(this.getConfigurationDirtyFlag()){
			map.put("configuration",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDate()!=null&&this.getDateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate());
			map.put("date",datetimeStr);
		}else if(this.getDateDirtyFlag()){
			map.put("date",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getIs_ssl()!=null&&this.getIs_sslDirtyFlag()){
			map.put("is_ssl",Boolean.parseBoolean(this.getIs_ssl()));		
		}		if(this.getMessage_ids()!=null&&this.getMessage_idsDirtyFlag()){
			map.put("message_ids",this.getMessage_ids());
		}else if(this.getMessage_idsDirtyFlag()){
			map.put("message_ids",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getObject_id()!=null&&this.getObject_idDirtyFlag()){
			map.put("object_id",this.getObject_id());
		}else if(this.getObject_idDirtyFlag()){
			map.put("object_id",false);
		}
		if(this.getOriginal()!=null&&this.getOriginalDirtyFlag()){
			map.put("original",Boolean.parseBoolean(this.getOriginal()));		
		}		if(this.getPassword()!=null&&this.getPasswordDirtyFlag()){
			map.put("password",this.getPassword());
		}else if(this.getPasswordDirtyFlag()){
			map.put("password",false);
		}
		if(this.getPort()!=null&&this.getPortDirtyFlag()){
			map.put("port",this.getPort());
		}else if(this.getPortDirtyFlag()){
			map.put("port",false);
		}
		if(this.getPriority()!=null&&this.getPriorityDirtyFlag()){
			map.put("priority",this.getPriority());
		}else if(this.getPriorityDirtyFlag()){
			map.put("priority",false);
		}
		if(this.getScript()!=null&&this.getScriptDirtyFlag()){
			map.put("script",this.getScript());
		}else if(this.getScriptDirtyFlag()){
			map.put("script",false);
		}
		if(this.getServer()!=null&&this.getServerDirtyFlag()){
			map.put("server",this.getServer());
		}else if(this.getServerDirtyFlag()){
			map.put("server",false);
		}
		if(this.getState()!=null&&this.getStateDirtyFlag()){
			map.put("state",this.getState());
		}else if(this.getStateDirtyFlag()){
			map.put("state",false);
		}
		if(this.getType()!=null&&this.getTypeDirtyFlag()){
			map.put("type",this.getType());
		}else if(this.getTypeDirtyFlag()){
			map.put("type",false);
		}
		if(this.getUser()!=null&&this.getUserDirtyFlag()){
			map.put("user",this.getUser());
		}else if(this.getUserDirtyFlag()){
			map.put("user",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
