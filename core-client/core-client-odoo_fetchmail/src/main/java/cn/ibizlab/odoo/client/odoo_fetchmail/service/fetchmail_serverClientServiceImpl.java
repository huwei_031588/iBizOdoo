package cn.ibizlab.odoo.client.odoo_fetchmail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifetchmail_server;
import cn.ibizlab.odoo.core.client.service.Ifetchmail_serverClientService;
import cn.ibizlab.odoo.client.odoo_fetchmail.model.fetchmail_serverImpl;
import cn.ibizlab.odoo.client.odoo_fetchmail.odooclient.Ifetchmail_serverOdooClient;
import cn.ibizlab.odoo.client.odoo_fetchmail.odooclient.impl.fetchmail_serverOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[fetchmail_server] 服务对象接口
 */
@Service
public class fetchmail_serverClientServiceImpl implements Ifetchmail_serverClientService {
    @Autowired
    private  Ifetchmail_serverOdooClient  fetchmail_serverOdooClient;

    public Ifetchmail_server createModel() {		
		return new fetchmail_serverImpl();
	}


        public void remove(Ifetchmail_server fetchmail_server){
this.fetchmail_serverOdooClient.remove(fetchmail_server) ;
        }
        
        public void createBatch(List<Ifetchmail_server> fetchmail_servers){
            
        }
        
        public void create(Ifetchmail_server fetchmail_server){
this.fetchmail_serverOdooClient.create(fetchmail_server) ;
        }
        
        public void update(Ifetchmail_server fetchmail_server){
this.fetchmail_serverOdooClient.update(fetchmail_server) ;
        }
        
        public Page<Ifetchmail_server> search(SearchContext context){
            return this.fetchmail_serverOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Ifetchmail_server> fetchmail_servers){
            
        }
        
        public void updateBatch(List<Ifetchmail_server> fetchmail_servers){
            
        }
        
        public void get(Ifetchmail_server fetchmail_server){
            this.fetchmail_serverOdooClient.get(fetchmail_server) ;
        }
        
        public Page<Ifetchmail_server> select(SearchContext context){
            return null ;
        }
        

}

