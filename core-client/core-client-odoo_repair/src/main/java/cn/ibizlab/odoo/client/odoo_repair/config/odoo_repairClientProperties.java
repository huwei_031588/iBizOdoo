package cn.ibizlab.odoo.client.odoo_repair.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.repair")
@Data
public class odoo_repairClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
