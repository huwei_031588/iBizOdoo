package cn.ibizlab.odoo.client.odoo_repair.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Irepair_line;
import cn.ibizlab.odoo.core.client.service.Irepair_lineClientService;
import cn.ibizlab.odoo.client.odoo_repair.model.repair_lineImpl;
import cn.ibizlab.odoo.client.odoo_repair.odooclient.Irepair_lineOdooClient;
import cn.ibizlab.odoo.client.odoo_repair.odooclient.impl.repair_lineOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[repair_line] 服务对象接口
 */
@Service
public class repair_lineClientServiceImpl implements Irepair_lineClientService {
    @Autowired
    private  Irepair_lineOdooClient  repair_lineOdooClient;

    public Irepair_line createModel() {		
		return new repair_lineImpl();
	}


        public void updateBatch(List<Irepair_line> repair_lines){
            
        }
        
        public void createBatch(List<Irepair_line> repair_lines){
            
        }
        
        public void get(Irepair_line repair_line){
            this.repair_lineOdooClient.get(repair_line) ;
        }
        
        public void update(Irepair_line repair_line){
this.repair_lineOdooClient.update(repair_line) ;
        }
        
        public Page<Irepair_line> search(SearchContext context){
            return this.repair_lineOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Irepair_line> repair_lines){
            
        }
        
        public void create(Irepair_line repair_line){
this.repair_lineOdooClient.create(repair_line) ;
        }
        
        public void remove(Irepair_line repair_line){
this.repair_lineOdooClient.remove(repair_line) ;
        }
        
        public Page<Irepair_line> select(SearchContext context){
            return null ;
        }
        

}

