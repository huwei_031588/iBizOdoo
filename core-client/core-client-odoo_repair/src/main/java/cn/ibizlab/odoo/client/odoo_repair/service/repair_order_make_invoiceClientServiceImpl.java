package cn.ibizlab.odoo.client.odoo_repair.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Irepair_order_make_invoice;
import cn.ibizlab.odoo.core.client.service.Irepair_order_make_invoiceClientService;
import cn.ibizlab.odoo.client.odoo_repair.model.repair_order_make_invoiceImpl;
import cn.ibizlab.odoo.client.odoo_repair.odooclient.Irepair_order_make_invoiceOdooClient;
import cn.ibizlab.odoo.client.odoo_repair.odooclient.impl.repair_order_make_invoiceOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[repair_order_make_invoice] 服务对象接口
 */
@Service
public class repair_order_make_invoiceClientServiceImpl implements Irepair_order_make_invoiceClientService {
    @Autowired
    private  Irepair_order_make_invoiceOdooClient  repair_order_make_invoiceOdooClient;

    public Irepair_order_make_invoice createModel() {		
		return new repair_order_make_invoiceImpl();
	}


        public Page<Irepair_order_make_invoice> search(SearchContext context){
            return this.repair_order_make_invoiceOdooClient.search(context) ;
        }
        
        public void update(Irepair_order_make_invoice repair_order_make_invoice){
this.repair_order_make_invoiceOdooClient.update(repair_order_make_invoice) ;
        }
        
        public void create(Irepair_order_make_invoice repair_order_make_invoice){
this.repair_order_make_invoiceOdooClient.create(repair_order_make_invoice) ;
        }
        
        public void get(Irepair_order_make_invoice repair_order_make_invoice){
            this.repair_order_make_invoiceOdooClient.get(repair_order_make_invoice) ;
        }
        
        public void updateBatch(List<Irepair_order_make_invoice> repair_order_make_invoices){
            
        }
        
        public void createBatch(List<Irepair_order_make_invoice> repair_order_make_invoices){
            
        }
        
        public void removeBatch(List<Irepair_order_make_invoice> repair_order_make_invoices){
            
        }
        
        public void remove(Irepair_order_make_invoice repair_order_make_invoice){
this.repair_order_make_invoiceOdooClient.remove(repair_order_make_invoice) ;
        }
        
        public Page<Irepair_order_make_invoice> select(SearchContext context){
            return null ;
        }
        

}

