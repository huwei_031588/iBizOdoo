package cn.ibizlab.odoo.client.odoo_repair.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Irepair_order;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[repair_order] 服务对象客户端接口
 */
public interface Irepair_orderOdooClient {
    
        public void createBatch(Irepair_order repair_order);

        public void update(Irepair_order repair_order);

        public Page<Irepair_order> search(SearchContext context);

        public void removeBatch(Irepair_order repair_order);

        public void updateBatch(Irepair_order repair_order);

        public void create(Irepair_order repair_order);

        public void get(Irepair_order repair_order);

        public void remove(Irepair_order repair_order);

        public List<Irepair_order> select();


}