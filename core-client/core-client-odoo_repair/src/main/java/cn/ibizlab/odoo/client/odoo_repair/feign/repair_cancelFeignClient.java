package cn.ibizlab.odoo.client.odoo_repair.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Irepair_cancel;
import cn.ibizlab.odoo.client.odoo_repair.model.repair_cancelImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[repair_cancel] 服务对象接口
 */
public interface repair_cancelFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_repair/repair_cancels")
    public repair_cancelImpl create(@RequestBody repair_cancelImpl repair_cancel);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_repair/repair_cancels/createbatch")
    public repair_cancelImpl createBatch(@RequestBody List<repair_cancelImpl> repair_cancels);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_repair/repair_cancels/search")
    public Page<repair_cancelImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_repair/repair_cancels/{id}")
    public repair_cancelImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_repair/repair_cancels/{id}")
    public repair_cancelImpl update(@PathVariable("id") Integer id,@RequestBody repair_cancelImpl repair_cancel);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_repair/repair_cancels/updatebatch")
    public repair_cancelImpl updateBatch(@RequestBody List<repair_cancelImpl> repair_cancels);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_repair/repair_cancels/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_repair/repair_cancels/removebatch")
    public repair_cancelImpl removeBatch(@RequestBody List<repair_cancelImpl> repair_cancels);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_repair/repair_cancels/select")
    public Page<repair_cancelImpl> select();



}
