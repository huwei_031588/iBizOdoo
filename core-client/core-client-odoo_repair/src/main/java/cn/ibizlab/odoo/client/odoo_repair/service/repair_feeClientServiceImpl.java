package cn.ibizlab.odoo.client.odoo_repair.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Irepair_fee;
import cn.ibizlab.odoo.core.client.service.Irepair_feeClientService;
import cn.ibizlab.odoo.client.odoo_repair.model.repair_feeImpl;
import cn.ibizlab.odoo.client.odoo_repair.odooclient.Irepair_feeOdooClient;
import cn.ibizlab.odoo.client.odoo_repair.odooclient.impl.repair_feeOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[repair_fee] 服务对象接口
 */
@Service
public class repair_feeClientServiceImpl implements Irepair_feeClientService {
    @Autowired
    private  Irepair_feeOdooClient  repair_feeOdooClient;

    public Irepair_fee createModel() {		
		return new repair_feeImpl();
	}


        public void get(Irepair_fee repair_fee){
            this.repair_feeOdooClient.get(repair_fee) ;
        }
        
        public void create(Irepair_fee repair_fee){
this.repair_feeOdooClient.create(repair_fee) ;
        }
        
        public void updateBatch(List<Irepair_fee> repair_fees){
            
        }
        
        public void createBatch(List<Irepair_fee> repair_fees){
            
        }
        
        public void update(Irepair_fee repair_fee){
this.repair_feeOdooClient.update(repair_fee) ;
        }
        
        public void removeBatch(List<Irepair_fee> repair_fees){
            
        }
        
        public void remove(Irepair_fee repair_fee){
this.repair_feeOdooClient.remove(repair_fee) ;
        }
        
        public Page<Irepair_fee> search(SearchContext context){
            return this.repair_feeOdooClient.search(context) ;
        }
        
        public Page<Irepair_fee> select(SearchContext context){
            return null ;
        }
        

}

