package cn.ibizlab.odoo.client.odoo_mrp.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Imrp_bom_line;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[mrp_bom_line] 对象
 */
public class mrp_bom_lineImpl implements Imrp_bom_line,Serializable{

    /**
     * 应用于变体
     */
    public String attribute_value_ids;

    @JsonIgnore
    public boolean attribute_value_idsDirtyFlag;
    
    /**
     * 父级 BoM
     */
    public Integer bom_id;

    @JsonIgnore
    public boolean bom_idDirtyFlag;
    
    /**
     * 子 BOM
     */
    public Integer child_bom_id;

    @JsonIgnore
    public boolean child_bom_idDirtyFlag;
    
    /**
     * 参考BOM中的BOM行
     */
    public String child_line_ids;

    @JsonIgnore
    public boolean child_line_idsDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 有附件
     */
    public String has_attachments;

    @JsonIgnore
    public boolean has_attachmentsDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 投料作业
     */
    public Integer operation_id;

    @JsonIgnore
    public boolean operation_idDirtyFlag;
    
    /**
     * 投料作业
     */
    public String operation_id_text;

    @JsonIgnore
    public boolean operation_id_textDirtyFlag;
    
    /**
     * 父产品模板
     */
    public Integer parent_product_tmpl_id;

    @JsonIgnore
    public boolean parent_product_tmpl_idDirtyFlag;
    
    /**
     * 零件
     */
    public Integer product_id;

    @JsonIgnore
    public boolean product_idDirtyFlag;
    
    /**
     * 零件
     */
    public String product_id_text;

    @JsonIgnore
    public boolean product_id_textDirtyFlag;
    
    /**
     * 数量
     */
    public Double product_qty;

    @JsonIgnore
    public boolean product_qtyDirtyFlag;
    
    /**
     * 产品模板
     */
    public Integer product_tmpl_id;

    @JsonIgnore
    public boolean product_tmpl_idDirtyFlag;
    
    /**
     * 计量单位
     */
    public Integer product_uom_id;

    @JsonIgnore
    public boolean product_uom_idDirtyFlag;
    
    /**
     * 计量单位
     */
    public String product_uom_id_text;

    @JsonIgnore
    public boolean product_uom_id_textDirtyFlag;
    
    /**
     * 工艺
     */
    public Integer routing_id;

    @JsonIgnore
    public boolean routing_idDirtyFlag;
    
    /**
     * 工艺
     */
    public String routing_id_text;

    @JsonIgnore
    public boolean routing_id_textDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 有效的产品属性值
     */
    public String valid_product_attribute_value_ids;

    @JsonIgnore
    public boolean valid_product_attribute_value_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [应用于变体]
     */
    @JsonProperty("attribute_value_ids")
    public String getAttribute_value_ids(){
        return this.attribute_value_ids ;
    }

    /**
     * 设置 [应用于变体]
     */
    @JsonProperty("attribute_value_ids")
    public void setAttribute_value_ids(String  attribute_value_ids){
        this.attribute_value_ids = attribute_value_ids ;
        this.attribute_value_idsDirtyFlag = true ;
    }

     /**
     * 获取 [应用于变体]脏标记
     */
    @JsonIgnore
    public boolean getAttribute_value_idsDirtyFlag(){
        return this.attribute_value_idsDirtyFlag ;
    }   

    /**
     * 获取 [父级 BoM]
     */
    @JsonProperty("bom_id")
    public Integer getBom_id(){
        return this.bom_id ;
    }

    /**
     * 设置 [父级 BoM]
     */
    @JsonProperty("bom_id")
    public void setBom_id(Integer  bom_id){
        this.bom_id = bom_id ;
        this.bom_idDirtyFlag = true ;
    }

     /**
     * 获取 [父级 BoM]脏标记
     */
    @JsonIgnore
    public boolean getBom_idDirtyFlag(){
        return this.bom_idDirtyFlag ;
    }   

    /**
     * 获取 [子 BOM]
     */
    @JsonProperty("child_bom_id")
    public Integer getChild_bom_id(){
        return this.child_bom_id ;
    }

    /**
     * 设置 [子 BOM]
     */
    @JsonProperty("child_bom_id")
    public void setChild_bom_id(Integer  child_bom_id){
        this.child_bom_id = child_bom_id ;
        this.child_bom_idDirtyFlag = true ;
    }

     /**
     * 获取 [子 BOM]脏标记
     */
    @JsonIgnore
    public boolean getChild_bom_idDirtyFlag(){
        return this.child_bom_idDirtyFlag ;
    }   

    /**
     * 获取 [参考BOM中的BOM行]
     */
    @JsonProperty("child_line_ids")
    public String getChild_line_ids(){
        return this.child_line_ids ;
    }

    /**
     * 设置 [参考BOM中的BOM行]
     */
    @JsonProperty("child_line_ids")
    public void setChild_line_ids(String  child_line_ids){
        this.child_line_ids = child_line_ids ;
        this.child_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [参考BOM中的BOM行]脏标记
     */
    @JsonIgnore
    public boolean getChild_line_idsDirtyFlag(){
        return this.child_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [有附件]
     */
    @JsonProperty("has_attachments")
    public String getHas_attachments(){
        return this.has_attachments ;
    }

    /**
     * 设置 [有附件]
     */
    @JsonProperty("has_attachments")
    public void setHas_attachments(String  has_attachments){
        this.has_attachments = has_attachments ;
        this.has_attachmentsDirtyFlag = true ;
    }

     /**
     * 获取 [有附件]脏标记
     */
    @JsonIgnore
    public boolean getHas_attachmentsDirtyFlag(){
        return this.has_attachmentsDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [投料作业]
     */
    @JsonProperty("operation_id")
    public Integer getOperation_id(){
        return this.operation_id ;
    }

    /**
     * 设置 [投料作业]
     */
    @JsonProperty("operation_id")
    public void setOperation_id(Integer  operation_id){
        this.operation_id = operation_id ;
        this.operation_idDirtyFlag = true ;
    }

     /**
     * 获取 [投料作业]脏标记
     */
    @JsonIgnore
    public boolean getOperation_idDirtyFlag(){
        return this.operation_idDirtyFlag ;
    }   

    /**
     * 获取 [投料作业]
     */
    @JsonProperty("operation_id_text")
    public String getOperation_id_text(){
        return this.operation_id_text ;
    }

    /**
     * 设置 [投料作业]
     */
    @JsonProperty("operation_id_text")
    public void setOperation_id_text(String  operation_id_text){
        this.operation_id_text = operation_id_text ;
        this.operation_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [投料作业]脏标记
     */
    @JsonIgnore
    public boolean getOperation_id_textDirtyFlag(){
        return this.operation_id_textDirtyFlag ;
    }   

    /**
     * 获取 [父产品模板]
     */
    @JsonProperty("parent_product_tmpl_id")
    public Integer getParent_product_tmpl_id(){
        return this.parent_product_tmpl_id ;
    }

    /**
     * 设置 [父产品模板]
     */
    @JsonProperty("parent_product_tmpl_id")
    public void setParent_product_tmpl_id(Integer  parent_product_tmpl_id){
        this.parent_product_tmpl_id = parent_product_tmpl_id ;
        this.parent_product_tmpl_idDirtyFlag = true ;
    }

     /**
     * 获取 [父产品模板]脏标记
     */
    @JsonIgnore
    public boolean getParent_product_tmpl_idDirtyFlag(){
        return this.parent_product_tmpl_idDirtyFlag ;
    }   

    /**
     * 获取 [零件]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [零件]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

     /**
     * 获取 [零件]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }   

    /**
     * 获取 [零件]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return this.product_id_text ;
    }

    /**
     * 设置 [零件]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [零件]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return this.product_id_textDirtyFlag ;
    }   

    /**
     * 获取 [数量]
     */
    @JsonProperty("product_qty")
    public Double getProduct_qty(){
        return this.product_qty ;
    }

    /**
     * 设置 [数量]
     */
    @JsonProperty("product_qty")
    public void setProduct_qty(Double  product_qty){
        this.product_qty = product_qty ;
        this.product_qtyDirtyFlag = true ;
    }

     /**
     * 获取 [数量]脏标记
     */
    @JsonIgnore
    public boolean getProduct_qtyDirtyFlag(){
        return this.product_qtyDirtyFlag ;
    }   

    /**
     * 获取 [产品模板]
     */
    @JsonProperty("product_tmpl_id")
    public Integer getProduct_tmpl_id(){
        return this.product_tmpl_id ;
    }

    /**
     * 设置 [产品模板]
     */
    @JsonProperty("product_tmpl_id")
    public void setProduct_tmpl_id(Integer  product_tmpl_id){
        this.product_tmpl_id = product_tmpl_id ;
        this.product_tmpl_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品模板]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_idDirtyFlag(){
        return this.product_tmpl_idDirtyFlag ;
    }   

    /**
     * 获取 [计量单位]
     */
    @JsonProperty("product_uom_id")
    public Integer getProduct_uom_id(){
        return this.product_uom_id ;
    }

    /**
     * 设置 [计量单位]
     */
    @JsonProperty("product_uom_id")
    public void setProduct_uom_id(Integer  product_uom_id){
        this.product_uom_id = product_uom_id ;
        this.product_uom_idDirtyFlag = true ;
    }

     /**
     * 获取 [计量单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_idDirtyFlag(){
        return this.product_uom_idDirtyFlag ;
    }   

    /**
     * 获取 [计量单位]
     */
    @JsonProperty("product_uom_id_text")
    public String getProduct_uom_id_text(){
        return this.product_uom_id_text ;
    }

    /**
     * 设置 [计量单位]
     */
    @JsonProperty("product_uom_id_text")
    public void setProduct_uom_id_text(String  product_uom_id_text){
        this.product_uom_id_text = product_uom_id_text ;
        this.product_uom_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [计量单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_id_textDirtyFlag(){
        return this.product_uom_id_textDirtyFlag ;
    }   

    /**
     * 获取 [工艺]
     */
    @JsonProperty("routing_id")
    public Integer getRouting_id(){
        return this.routing_id ;
    }

    /**
     * 设置 [工艺]
     */
    @JsonProperty("routing_id")
    public void setRouting_id(Integer  routing_id){
        this.routing_id = routing_id ;
        this.routing_idDirtyFlag = true ;
    }

     /**
     * 获取 [工艺]脏标记
     */
    @JsonIgnore
    public boolean getRouting_idDirtyFlag(){
        return this.routing_idDirtyFlag ;
    }   

    /**
     * 获取 [工艺]
     */
    @JsonProperty("routing_id_text")
    public String getRouting_id_text(){
        return this.routing_id_text ;
    }

    /**
     * 设置 [工艺]
     */
    @JsonProperty("routing_id_text")
    public void setRouting_id_text(String  routing_id_text){
        this.routing_id_text = routing_id_text ;
        this.routing_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [工艺]脏标记
     */
    @JsonIgnore
    public boolean getRouting_id_textDirtyFlag(){
        return this.routing_id_textDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [有效的产品属性值]
     */
    @JsonProperty("valid_product_attribute_value_ids")
    public String getValid_product_attribute_value_ids(){
        return this.valid_product_attribute_value_ids ;
    }

    /**
     * 设置 [有效的产品属性值]
     */
    @JsonProperty("valid_product_attribute_value_ids")
    public void setValid_product_attribute_value_ids(String  valid_product_attribute_value_ids){
        this.valid_product_attribute_value_ids = valid_product_attribute_value_ids ;
        this.valid_product_attribute_value_idsDirtyFlag = true ;
    }

     /**
     * 获取 [有效的产品属性值]脏标记
     */
    @JsonIgnore
    public boolean getValid_product_attribute_value_idsDirtyFlag(){
        return this.valid_product_attribute_value_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("attribute_value_ids") instanceof Boolean)&& map.get("attribute_value_ids")!=null){
			Object[] objs = (Object[])map.get("attribute_value_ids");
			if(objs.length > 0){
				Integer[] attribute_value_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setAttribute_value_ids(Arrays.toString(attribute_value_ids));
			}
		}
		if(!(map.get("bom_id") instanceof Boolean)&& map.get("bom_id")!=null){
			Object[] objs = (Object[])map.get("bom_id");
			if(objs.length > 0){
				this.setBom_id((Integer)objs[0]);
			}
		}
		if(!(map.get("child_bom_id") instanceof Boolean)&& map.get("child_bom_id")!=null){
			Object[] objs = (Object[])map.get("child_bom_id");
			if(objs.length > 0){
				this.setChild_bom_id((Integer)objs[0]);
			}
		}
		if(!(map.get("child_line_ids") instanceof Boolean)&& map.get("child_line_ids")!=null){
			Object[] objs = (Object[])map.get("child_line_ids");
			if(objs.length > 0){
				Integer[] child_line_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setChild_line_ids(Arrays.toString(child_line_ids));
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(map.get("has_attachments") instanceof Boolean){
			this.setHas_attachments(((Boolean)map.get("has_attachments"))? "true" : "false");
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("operation_id") instanceof Boolean)&& map.get("operation_id")!=null){
			Object[] objs = (Object[])map.get("operation_id");
			if(objs.length > 0){
				this.setOperation_id((Integer)objs[0]);
			}
		}
		if(!(map.get("operation_id") instanceof Boolean)&& map.get("operation_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("operation_id");
			if(objs.length > 1){
				this.setOperation_id_text((String)objs[1]);
			}
		}
		if(!(map.get("parent_product_tmpl_id") instanceof Boolean)&& map.get("parent_product_tmpl_id")!=null){
			Object[] objs = (Object[])map.get("parent_product_tmpl_id");
			if(objs.length > 0){
				this.setParent_product_tmpl_id((Integer)objs[0]);
			}
		}
		if(!(map.get("product_id") instanceof Boolean)&& map.get("product_id")!=null){
			Object[] objs = (Object[])map.get("product_id");
			if(objs.length > 0){
				this.setProduct_id((Integer)objs[0]);
			}
		}
		if(!(map.get("product_id") instanceof Boolean)&& map.get("product_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("product_id");
			if(objs.length > 1){
				this.setProduct_id_text((String)objs[1]);
			}
		}
		if(!(map.get("product_qty") instanceof Boolean)&& map.get("product_qty")!=null){
			this.setProduct_qty((Double)map.get("product_qty"));
		}
		if(!(map.get("product_tmpl_id") instanceof Boolean)&& map.get("product_tmpl_id")!=null){
			Object[] objs = (Object[])map.get("product_tmpl_id");
			if(objs.length > 0){
				this.setProduct_tmpl_id((Integer)objs[0]);
			}
		}
		if(!(map.get("product_uom_id") instanceof Boolean)&& map.get("product_uom_id")!=null){
			Object[] objs = (Object[])map.get("product_uom_id");
			if(objs.length > 0){
				this.setProduct_uom_id((Integer)objs[0]);
			}
		}
		if(!(map.get("product_uom_id") instanceof Boolean)&& map.get("product_uom_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("product_uom_id");
			if(objs.length > 1){
				this.setProduct_uom_id_text((String)objs[1]);
			}
		}
		if(!(map.get("routing_id") instanceof Boolean)&& map.get("routing_id")!=null){
			Object[] objs = (Object[])map.get("routing_id");
			if(objs.length > 0){
				this.setRouting_id((Integer)objs[0]);
			}
		}
		if(!(map.get("routing_id") instanceof Boolean)&& map.get("routing_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("routing_id");
			if(objs.length > 1){
				this.setRouting_id_text((String)objs[1]);
			}
		}
		if(!(map.get("sequence") instanceof Boolean)&& map.get("sequence")!=null){
			this.setSequence((Integer)map.get("sequence"));
		}
		if(!(map.get("valid_product_attribute_value_ids") instanceof Boolean)&& map.get("valid_product_attribute_value_ids")!=null){
			Object[] objs = (Object[])map.get("valid_product_attribute_value_ids");
			if(objs.length > 0){
				Integer[] valid_product_attribute_value_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setValid_product_attribute_value_ids(Arrays.toString(valid_product_attribute_value_ids));
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAttribute_value_ids()!=null&&this.getAttribute_value_idsDirtyFlag()){
			map.put("attribute_value_ids",this.getAttribute_value_ids());
		}else if(this.getAttribute_value_idsDirtyFlag()){
			map.put("attribute_value_ids",false);
		}
		if(this.getBom_id()!=null&&this.getBom_idDirtyFlag()){
			map.put("bom_id",this.getBom_id());
		}else if(this.getBom_idDirtyFlag()){
			map.put("bom_id",false);
		}
		if(this.getChild_bom_id()!=null&&this.getChild_bom_idDirtyFlag()){
			map.put("child_bom_id",this.getChild_bom_id());
		}else if(this.getChild_bom_idDirtyFlag()){
			map.put("child_bom_id",false);
		}
		if(this.getChild_line_ids()!=null&&this.getChild_line_idsDirtyFlag()){
			map.put("child_line_ids",this.getChild_line_ids());
		}else if(this.getChild_line_idsDirtyFlag()){
			map.put("child_line_ids",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getHas_attachments()!=null&&this.getHas_attachmentsDirtyFlag()){
			map.put("has_attachments",Boolean.parseBoolean(this.getHas_attachments()));		
		}		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getOperation_id()!=null&&this.getOperation_idDirtyFlag()){
			map.put("operation_id",this.getOperation_id());
		}else if(this.getOperation_idDirtyFlag()){
			map.put("operation_id",false);
		}
		if(this.getOperation_id_text()!=null&&this.getOperation_id_textDirtyFlag()){
			//忽略文本外键operation_id_text
		}else if(this.getOperation_id_textDirtyFlag()){
			map.put("operation_id",false);
		}
		if(this.getParent_product_tmpl_id()!=null&&this.getParent_product_tmpl_idDirtyFlag()){
			map.put("parent_product_tmpl_id",this.getParent_product_tmpl_id());
		}else if(this.getParent_product_tmpl_idDirtyFlag()){
			map.put("parent_product_tmpl_id",false);
		}
		if(this.getProduct_id()!=null&&this.getProduct_idDirtyFlag()){
			map.put("product_id",this.getProduct_id());
		}else if(this.getProduct_idDirtyFlag()){
			map.put("product_id",false);
		}
		if(this.getProduct_id_text()!=null&&this.getProduct_id_textDirtyFlag()){
			//忽略文本外键product_id_text
		}else if(this.getProduct_id_textDirtyFlag()){
			map.put("product_id",false);
		}
		if(this.getProduct_qty()!=null&&this.getProduct_qtyDirtyFlag()){
			map.put("product_qty",this.getProduct_qty());
		}else if(this.getProduct_qtyDirtyFlag()){
			map.put("product_qty",false);
		}
		if(this.getProduct_tmpl_id()!=null&&this.getProduct_tmpl_idDirtyFlag()){
			map.put("product_tmpl_id",this.getProduct_tmpl_id());
		}else if(this.getProduct_tmpl_idDirtyFlag()){
			map.put("product_tmpl_id",false);
		}
		if(this.getProduct_uom_id()!=null&&this.getProduct_uom_idDirtyFlag()){
			map.put("product_uom_id",this.getProduct_uom_id());
		}else if(this.getProduct_uom_idDirtyFlag()){
			map.put("product_uom_id",false);
		}
		if(this.getProduct_uom_id_text()!=null&&this.getProduct_uom_id_textDirtyFlag()){
			//忽略文本外键product_uom_id_text
		}else if(this.getProduct_uom_id_textDirtyFlag()){
			map.put("product_uom_id",false);
		}
		if(this.getRouting_id()!=null&&this.getRouting_idDirtyFlag()){
			map.put("routing_id",this.getRouting_id());
		}else if(this.getRouting_idDirtyFlag()){
			map.put("routing_id",false);
		}
		if(this.getRouting_id_text()!=null&&this.getRouting_id_textDirtyFlag()){
			//忽略文本外键routing_id_text
		}else if(this.getRouting_id_textDirtyFlag()){
			map.put("routing_id",false);
		}
		if(this.getSequence()!=null&&this.getSequenceDirtyFlag()){
			map.put("sequence",this.getSequence());
		}else if(this.getSequenceDirtyFlag()){
			map.put("sequence",false);
		}
		if(this.getValid_product_attribute_value_ids()!=null&&this.getValid_product_attribute_value_idsDirtyFlag()){
			map.put("valid_product_attribute_value_ids",this.getValid_product_attribute_value_ids());
		}else if(this.getValid_product_attribute_value_idsDirtyFlag()){
			map.put("valid_product_attribute_value_ids",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
