package cn.ibizlab.odoo.client.odoo_mrp.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Imrp_workcenter;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[mrp_workcenter] 对象
 */
public class mrp_workcenterImpl implements Imrp_workcenter,Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 阻塞时间
     */
    public Double blocked_time;

    @JsonIgnore
    public boolean blocked_timeDirtyFlag;
    
    /**
     * 容量
     */
    public Double capacity;

    @JsonIgnore
    public boolean capacityDirtyFlag;
    
    /**
     * 代码
     */
    public String code;

    @JsonIgnore
    public boolean codeDirtyFlag;
    
    /**
     * 颜色
     */
    public Integer color;

    @JsonIgnore
    public boolean colorDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 每小时成本
     */
    public Double costs_hour;

    @JsonIgnore
    public boolean costs_hourDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 工作中心
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 说明
     */
    public String note;

    @JsonIgnore
    public boolean noteDirtyFlag;
    
    /**
     * OEE
     */
    public Double oee;

    @JsonIgnore
    public boolean oeeDirtyFlag;
    
    /**
     * OEE 目标
     */
    public Double oee_target;

    @JsonIgnore
    public boolean oee_targetDirtyFlag;
    
    /**
     * 订单
     */
    public String order_ids;

    @JsonIgnore
    public boolean order_idsDirtyFlag;
    
    /**
     * 效能
     */
    public Integer performance;

    @JsonIgnore
    public boolean performanceDirtyFlag;
    
    /**
     * 生产性时间
     */
    public Double productive_time;

    @JsonIgnore
    public boolean productive_timeDirtyFlag;
    
    /**
     * 工作时间
     */
    public Integer resource_calendar_id;

    @JsonIgnore
    public boolean resource_calendar_idDirtyFlag;
    
    /**
     * 工作时间
     */
    public String resource_calendar_id_text;

    @JsonIgnore
    public boolean resource_calendar_id_textDirtyFlag;
    
    /**
     * 资源
     */
    public Integer resource_id;

    @JsonIgnore
    public boolean resource_idDirtyFlag;
    
    /**
     * 工艺行
     */
    public String routing_line_ids;

    @JsonIgnore
    public boolean routing_line_idsDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 时间效率
     */
    public Double time_efficiency;

    @JsonIgnore
    public boolean time_efficiencyDirtyFlag;
    
    /**
     * 时间日志
     */
    public String time_ids;

    @JsonIgnore
    public boolean time_idsDirtyFlag;
    
    /**
     * 生产前时间
     */
    public Double time_start;

    @JsonIgnore
    public boolean time_startDirtyFlag;
    
    /**
     * 生产后时间
     */
    public Double time_stop;

    @JsonIgnore
    public boolean time_stopDirtyFlag;
    
    /**
     * 时区
     */
    public String tz;

    @JsonIgnore
    public boolean tzDirtyFlag;
    
    /**
     * 工作中心负载
     */
    public Double workcenter_load;

    @JsonIgnore
    public boolean workcenter_loadDirtyFlag;
    
    /**
     * 工作中心状态
     */
    public String working_state;

    @JsonIgnore
    public boolean working_stateDirtyFlag;
    
    /**
     * # 工单
     */
    public Integer workorder_count;

    @JsonIgnore
    public boolean workorder_countDirtyFlag;
    
    /**
     * 延迟的订单合计
     */
    public Integer workorder_late_count;

    @JsonIgnore
    public boolean workorder_late_countDirtyFlag;
    
    /**
     * 待定的订单合计
     */
    public Integer workorder_pending_count;

    @JsonIgnore
    public boolean workorder_pending_countDirtyFlag;
    
    /**
     * 运行的订单合计
     */
    public Integer workorder_progress_count;

    @JsonIgnore
    public boolean workorder_progress_countDirtyFlag;
    
    /**
     * # 读取工单
     */
    public Integer workorder_ready_count;

    @JsonIgnore
    public boolean workorder_ready_countDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [阻塞时间]
     */
    @JsonProperty("blocked_time")
    public Double getBlocked_time(){
        return this.blocked_time ;
    }

    /**
     * 设置 [阻塞时间]
     */
    @JsonProperty("blocked_time")
    public void setBlocked_time(Double  blocked_time){
        this.blocked_time = blocked_time ;
        this.blocked_timeDirtyFlag = true ;
    }

     /**
     * 获取 [阻塞时间]脏标记
     */
    @JsonIgnore
    public boolean getBlocked_timeDirtyFlag(){
        return this.blocked_timeDirtyFlag ;
    }   

    /**
     * 获取 [容量]
     */
    @JsonProperty("capacity")
    public Double getCapacity(){
        return this.capacity ;
    }

    /**
     * 设置 [容量]
     */
    @JsonProperty("capacity")
    public void setCapacity(Double  capacity){
        this.capacity = capacity ;
        this.capacityDirtyFlag = true ;
    }

     /**
     * 获取 [容量]脏标记
     */
    @JsonIgnore
    public boolean getCapacityDirtyFlag(){
        return this.capacityDirtyFlag ;
    }   

    /**
     * 获取 [代码]
     */
    @JsonProperty("code")
    public String getCode(){
        return this.code ;
    }

    /**
     * 设置 [代码]
     */
    @JsonProperty("code")
    public void setCode(String  code){
        this.code = code ;
        this.codeDirtyFlag = true ;
    }

     /**
     * 获取 [代码]脏标记
     */
    @JsonIgnore
    public boolean getCodeDirtyFlag(){
        return this.codeDirtyFlag ;
    }   

    /**
     * 获取 [颜色]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return this.color ;
    }

    /**
     * 设置 [颜色]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

     /**
     * 获取 [颜色]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return this.colorDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [每小时成本]
     */
    @JsonProperty("costs_hour")
    public Double getCosts_hour(){
        return this.costs_hour ;
    }

    /**
     * 设置 [每小时成本]
     */
    @JsonProperty("costs_hour")
    public void setCosts_hour(Double  costs_hour){
        this.costs_hour = costs_hour ;
        this.costs_hourDirtyFlag = true ;
    }

     /**
     * 获取 [每小时成本]脏标记
     */
    @JsonIgnore
    public boolean getCosts_hourDirtyFlag(){
        return this.costs_hourDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [工作中心]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [工作中心]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [工作中心]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [说明]
     */
    @JsonProperty("note")
    public String getNote(){
        return this.note ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

     /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return this.noteDirtyFlag ;
    }   

    /**
     * 获取 [OEE]
     */
    @JsonProperty("oee")
    public Double getOee(){
        return this.oee ;
    }

    /**
     * 设置 [OEE]
     */
    @JsonProperty("oee")
    public void setOee(Double  oee){
        this.oee = oee ;
        this.oeeDirtyFlag = true ;
    }

     /**
     * 获取 [OEE]脏标记
     */
    @JsonIgnore
    public boolean getOeeDirtyFlag(){
        return this.oeeDirtyFlag ;
    }   

    /**
     * 获取 [OEE 目标]
     */
    @JsonProperty("oee_target")
    public Double getOee_target(){
        return this.oee_target ;
    }

    /**
     * 设置 [OEE 目标]
     */
    @JsonProperty("oee_target")
    public void setOee_target(Double  oee_target){
        this.oee_target = oee_target ;
        this.oee_targetDirtyFlag = true ;
    }

     /**
     * 获取 [OEE 目标]脏标记
     */
    @JsonIgnore
    public boolean getOee_targetDirtyFlag(){
        return this.oee_targetDirtyFlag ;
    }   

    /**
     * 获取 [订单]
     */
    @JsonProperty("order_ids")
    public String getOrder_ids(){
        return this.order_ids ;
    }

    /**
     * 设置 [订单]
     */
    @JsonProperty("order_ids")
    public void setOrder_ids(String  order_ids){
        this.order_ids = order_ids ;
        this.order_idsDirtyFlag = true ;
    }

     /**
     * 获取 [订单]脏标记
     */
    @JsonIgnore
    public boolean getOrder_idsDirtyFlag(){
        return this.order_idsDirtyFlag ;
    }   

    /**
     * 获取 [效能]
     */
    @JsonProperty("performance")
    public Integer getPerformance(){
        return this.performance ;
    }

    /**
     * 设置 [效能]
     */
    @JsonProperty("performance")
    public void setPerformance(Integer  performance){
        this.performance = performance ;
        this.performanceDirtyFlag = true ;
    }

     /**
     * 获取 [效能]脏标记
     */
    @JsonIgnore
    public boolean getPerformanceDirtyFlag(){
        return this.performanceDirtyFlag ;
    }   

    /**
     * 获取 [生产性时间]
     */
    @JsonProperty("productive_time")
    public Double getProductive_time(){
        return this.productive_time ;
    }

    /**
     * 设置 [生产性时间]
     */
    @JsonProperty("productive_time")
    public void setProductive_time(Double  productive_time){
        this.productive_time = productive_time ;
        this.productive_timeDirtyFlag = true ;
    }

     /**
     * 获取 [生产性时间]脏标记
     */
    @JsonIgnore
    public boolean getProductive_timeDirtyFlag(){
        return this.productive_timeDirtyFlag ;
    }   

    /**
     * 获取 [工作时间]
     */
    @JsonProperty("resource_calendar_id")
    public Integer getResource_calendar_id(){
        return this.resource_calendar_id ;
    }

    /**
     * 设置 [工作时间]
     */
    @JsonProperty("resource_calendar_id")
    public void setResource_calendar_id(Integer  resource_calendar_id){
        this.resource_calendar_id = resource_calendar_id ;
        this.resource_calendar_idDirtyFlag = true ;
    }

     /**
     * 获取 [工作时间]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_idDirtyFlag(){
        return this.resource_calendar_idDirtyFlag ;
    }   

    /**
     * 获取 [工作时间]
     */
    @JsonProperty("resource_calendar_id_text")
    public String getResource_calendar_id_text(){
        return this.resource_calendar_id_text ;
    }

    /**
     * 设置 [工作时间]
     */
    @JsonProperty("resource_calendar_id_text")
    public void setResource_calendar_id_text(String  resource_calendar_id_text){
        this.resource_calendar_id_text = resource_calendar_id_text ;
        this.resource_calendar_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [工作时间]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_id_textDirtyFlag(){
        return this.resource_calendar_id_textDirtyFlag ;
    }   

    /**
     * 获取 [资源]
     */
    @JsonProperty("resource_id")
    public Integer getResource_id(){
        return this.resource_id ;
    }

    /**
     * 设置 [资源]
     */
    @JsonProperty("resource_id")
    public void setResource_id(Integer  resource_id){
        this.resource_id = resource_id ;
        this.resource_idDirtyFlag = true ;
    }

     /**
     * 获取 [资源]脏标记
     */
    @JsonIgnore
    public boolean getResource_idDirtyFlag(){
        return this.resource_idDirtyFlag ;
    }   

    /**
     * 获取 [工艺行]
     */
    @JsonProperty("routing_line_ids")
    public String getRouting_line_ids(){
        return this.routing_line_ids ;
    }

    /**
     * 设置 [工艺行]
     */
    @JsonProperty("routing_line_ids")
    public void setRouting_line_ids(String  routing_line_ids){
        this.routing_line_ids = routing_line_ids ;
        this.routing_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [工艺行]脏标记
     */
    @JsonIgnore
    public boolean getRouting_line_idsDirtyFlag(){
        return this.routing_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [时间效率]
     */
    @JsonProperty("time_efficiency")
    public Double getTime_efficiency(){
        return this.time_efficiency ;
    }

    /**
     * 设置 [时间效率]
     */
    @JsonProperty("time_efficiency")
    public void setTime_efficiency(Double  time_efficiency){
        this.time_efficiency = time_efficiency ;
        this.time_efficiencyDirtyFlag = true ;
    }

     /**
     * 获取 [时间效率]脏标记
     */
    @JsonIgnore
    public boolean getTime_efficiencyDirtyFlag(){
        return this.time_efficiencyDirtyFlag ;
    }   

    /**
     * 获取 [时间日志]
     */
    @JsonProperty("time_ids")
    public String getTime_ids(){
        return this.time_ids ;
    }

    /**
     * 设置 [时间日志]
     */
    @JsonProperty("time_ids")
    public void setTime_ids(String  time_ids){
        this.time_ids = time_ids ;
        this.time_idsDirtyFlag = true ;
    }

     /**
     * 获取 [时间日志]脏标记
     */
    @JsonIgnore
    public boolean getTime_idsDirtyFlag(){
        return this.time_idsDirtyFlag ;
    }   

    /**
     * 获取 [生产前时间]
     */
    @JsonProperty("time_start")
    public Double getTime_start(){
        return this.time_start ;
    }

    /**
     * 设置 [生产前时间]
     */
    @JsonProperty("time_start")
    public void setTime_start(Double  time_start){
        this.time_start = time_start ;
        this.time_startDirtyFlag = true ;
    }

     /**
     * 获取 [生产前时间]脏标记
     */
    @JsonIgnore
    public boolean getTime_startDirtyFlag(){
        return this.time_startDirtyFlag ;
    }   

    /**
     * 获取 [生产后时间]
     */
    @JsonProperty("time_stop")
    public Double getTime_stop(){
        return this.time_stop ;
    }

    /**
     * 设置 [生产后时间]
     */
    @JsonProperty("time_stop")
    public void setTime_stop(Double  time_stop){
        this.time_stop = time_stop ;
        this.time_stopDirtyFlag = true ;
    }

     /**
     * 获取 [生产后时间]脏标记
     */
    @JsonIgnore
    public boolean getTime_stopDirtyFlag(){
        return this.time_stopDirtyFlag ;
    }   

    /**
     * 获取 [时区]
     */
    @JsonProperty("tz")
    public String getTz(){
        return this.tz ;
    }

    /**
     * 设置 [时区]
     */
    @JsonProperty("tz")
    public void setTz(String  tz){
        this.tz = tz ;
        this.tzDirtyFlag = true ;
    }

     /**
     * 获取 [时区]脏标记
     */
    @JsonIgnore
    public boolean getTzDirtyFlag(){
        return this.tzDirtyFlag ;
    }   

    /**
     * 获取 [工作中心负载]
     */
    @JsonProperty("workcenter_load")
    public Double getWorkcenter_load(){
        return this.workcenter_load ;
    }

    /**
     * 设置 [工作中心负载]
     */
    @JsonProperty("workcenter_load")
    public void setWorkcenter_load(Double  workcenter_load){
        this.workcenter_load = workcenter_load ;
        this.workcenter_loadDirtyFlag = true ;
    }

     /**
     * 获取 [工作中心负载]脏标记
     */
    @JsonIgnore
    public boolean getWorkcenter_loadDirtyFlag(){
        return this.workcenter_loadDirtyFlag ;
    }   

    /**
     * 获取 [工作中心状态]
     */
    @JsonProperty("working_state")
    public String getWorking_state(){
        return this.working_state ;
    }

    /**
     * 设置 [工作中心状态]
     */
    @JsonProperty("working_state")
    public void setWorking_state(String  working_state){
        this.working_state = working_state ;
        this.working_stateDirtyFlag = true ;
    }

     /**
     * 获取 [工作中心状态]脏标记
     */
    @JsonIgnore
    public boolean getWorking_stateDirtyFlag(){
        return this.working_stateDirtyFlag ;
    }   

    /**
     * 获取 [# 工单]
     */
    @JsonProperty("workorder_count")
    public Integer getWorkorder_count(){
        return this.workorder_count ;
    }

    /**
     * 设置 [# 工单]
     */
    @JsonProperty("workorder_count")
    public void setWorkorder_count(Integer  workorder_count){
        this.workorder_count = workorder_count ;
        this.workorder_countDirtyFlag = true ;
    }

     /**
     * 获取 [# 工单]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_countDirtyFlag(){
        return this.workorder_countDirtyFlag ;
    }   

    /**
     * 获取 [延迟的订单合计]
     */
    @JsonProperty("workorder_late_count")
    public Integer getWorkorder_late_count(){
        return this.workorder_late_count ;
    }

    /**
     * 设置 [延迟的订单合计]
     */
    @JsonProperty("workorder_late_count")
    public void setWorkorder_late_count(Integer  workorder_late_count){
        this.workorder_late_count = workorder_late_count ;
        this.workorder_late_countDirtyFlag = true ;
    }

     /**
     * 获取 [延迟的订单合计]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_late_countDirtyFlag(){
        return this.workorder_late_countDirtyFlag ;
    }   

    /**
     * 获取 [待定的订单合计]
     */
    @JsonProperty("workorder_pending_count")
    public Integer getWorkorder_pending_count(){
        return this.workorder_pending_count ;
    }

    /**
     * 设置 [待定的订单合计]
     */
    @JsonProperty("workorder_pending_count")
    public void setWorkorder_pending_count(Integer  workorder_pending_count){
        this.workorder_pending_count = workorder_pending_count ;
        this.workorder_pending_countDirtyFlag = true ;
    }

     /**
     * 获取 [待定的订单合计]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_pending_countDirtyFlag(){
        return this.workorder_pending_countDirtyFlag ;
    }   

    /**
     * 获取 [运行的订单合计]
     */
    @JsonProperty("workorder_progress_count")
    public Integer getWorkorder_progress_count(){
        return this.workorder_progress_count ;
    }

    /**
     * 设置 [运行的订单合计]
     */
    @JsonProperty("workorder_progress_count")
    public void setWorkorder_progress_count(Integer  workorder_progress_count){
        this.workorder_progress_count = workorder_progress_count ;
        this.workorder_progress_countDirtyFlag = true ;
    }

     /**
     * 获取 [运行的订单合计]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_progress_countDirtyFlag(){
        return this.workorder_progress_countDirtyFlag ;
    }   

    /**
     * 获取 [# 读取工单]
     */
    @JsonProperty("workorder_ready_count")
    public Integer getWorkorder_ready_count(){
        return this.workorder_ready_count ;
    }

    /**
     * 设置 [# 读取工单]
     */
    @JsonProperty("workorder_ready_count")
    public void setWorkorder_ready_count(Integer  workorder_ready_count){
        this.workorder_ready_count = workorder_ready_count ;
        this.workorder_ready_countDirtyFlag = true ;
    }

     /**
     * 获取 [# 读取工单]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_ready_countDirtyFlag(){
        return this.workorder_ready_countDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(map.get("active") instanceof Boolean){
			this.setActive(((Boolean)map.get("active"))? "true" : "false");
		}
		if(!(map.get("blocked_time") instanceof Boolean)&& map.get("blocked_time")!=null){
			this.setBlocked_time((Double)map.get("blocked_time"));
		}
		if(!(map.get("capacity") instanceof Boolean)&& map.get("capacity")!=null){
			this.setCapacity((Double)map.get("capacity"));
		}
		if(!(map.get("code") instanceof Boolean)&& map.get("code")!=null){
			this.setCode((String)map.get("code"));
		}
		if(!(map.get("color") instanceof Boolean)&& map.get("color")!=null){
			this.setColor((Integer)map.get("color"));
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 1){
				this.setCompany_id_text((String)objs[1]);
			}
		}
		if(!(map.get("costs_hour") instanceof Boolean)&& map.get("costs_hour")!=null){
			this.setCosts_hour((Double)map.get("costs_hour"));
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("note") instanceof Boolean)&& map.get("note")!=null){
			this.setNote((String)map.get("note"));
		}
		if(!(map.get("oee") instanceof Boolean)&& map.get("oee")!=null){
			this.setOee((Double)map.get("oee"));
		}
		if(!(map.get("oee_target") instanceof Boolean)&& map.get("oee_target")!=null){
			this.setOee_target((Double)map.get("oee_target"));
		}
		if(!(map.get("order_ids") instanceof Boolean)&& map.get("order_ids")!=null){
			Object[] objs = (Object[])map.get("order_ids");
			if(objs.length > 0){
				Integer[] order_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setOrder_ids(Arrays.toString(order_ids));
			}
		}
		if(!(map.get("performance") instanceof Boolean)&& map.get("performance")!=null){
			this.setPerformance((Integer)map.get("performance"));
		}
		if(!(map.get("productive_time") instanceof Boolean)&& map.get("productive_time")!=null){
			this.setProductive_time((Double)map.get("productive_time"));
		}
		if(!(map.get("resource_calendar_id") instanceof Boolean)&& map.get("resource_calendar_id")!=null){
			Object[] objs = (Object[])map.get("resource_calendar_id");
			if(objs.length > 0){
				this.setResource_calendar_id((Integer)objs[0]);
			}
		}
		if(!(map.get("resource_calendar_id") instanceof Boolean)&& map.get("resource_calendar_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("resource_calendar_id");
			if(objs.length > 1){
				this.setResource_calendar_id_text((String)objs[1]);
			}
		}
		if(!(map.get("resource_id") instanceof Boolean)&& map.get("resource_id")!=null){
			Object[] objs = (Object[])map.get("resource_id");
			if(objs.length > 0){
				this.setResource_id((Integer)objs[0]);
			}
		}
		if(!(map.get("routing_line_ids") instanceof Boolean)&& map.get("routing_line_ids")!=null){
			Object[] objs = (Object[])map.get("routing_line_ids");
			if(objs.length > 0){
				Integer[] routing_line_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setRouting_line_ids(Arrays.toString(routing_line_ids));
			}
		}
		if(!(map.get("sequence") instanceof Boolean)&& map.get("sequence")!=null){
			this.setSequence((Integer)map.get("sequence"));
		}
		if(!(map.get("time_efficiency") instanceof Boolean)&& map.get("time_efficiency")!=null){
			this.setTime_efficiency((Double)map.get("time_efficiency"));
		}
		if(!(map.get("time_ids") instanceof Boolean)&& map.get("time_ids")!=null){
			Object[] objs = (Object[])map.get("time_ids");
			if(objs.length > 0){
				Integer[] time_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setTime_ids(Arrays.toString(time_ids));
			}
		}
		if(!(map.get("time_start") instanceof Boolean)&& map.get("time_start")!=null){
			this.setTime_start((Double)map.get("time_start"));
		}
		if(!(map.get("time_stop") instanceof Boolean)&& map.get("time_stop")!=null){
			this.setTime_stop((Double)map.get("time_stop"));
		}
		if(!(map.get("tz") instanceof Boolean)&& map.get("tz")!=null){
			this.setTz((String)map.get("tz"));
		}
		if(!(map.get("workcenter_load") instanceof Boolean)&& map.get("workcenter_load")!=null){
			this.setWorkcenter_load((Double)map.get("workcenter_load"));
		}
		if(!(map.get("working_state") instanceof Boolean)&& map.get("working_state")!=null){
			this.setWorking_state((String)map.get("working_state"));
		}
		if(!(map.get("workorder_count") instanceof Boolean)&& map.get("workorder_count")!=null){
			this.setWorkorder_count((Integer)map.get("workorder_count"));
		}
		if(!(map.get("workorder_late_count") instanceof Boolean)&& map.get("workorder_late_count")!=null){
			this.setWorkorder_late_count((Integer)map.get("workorder_late_count"));
		}
		if(!(map.get("workorder_pending_count") instanceof Boolean)&& map.get("workorder_pending_count")!=null){
			this.setWorkorder_pending_count((Integer)map.get("workorder_pending_count"));
		}
		if(!(map.get("workorder_progress_count") instanceof Boolean)&& map.get("workorder_progress_count")!=null){
			this.setWorkorder_progress_count((Integer)map.get("workorder_progress_count"));
		}
		if(!(map.get("workorder_ready_count") instanceof Boolean)&& map.get("workorder_ready_count")!=null){
			this.setWorkorder_ready_count((Integer)map.get("workorder_ready_count"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getActive()!=null&&this.getActiveDirtyFlag()){
			map.put("active",Boolean.parseBoolean(this.getActive()));		
		}		if(this.getBlocked_time()!=null&&this.getBlocked_timeDirtyFlag()){
			map.put("blocked_time",this.getBlocked_time());
		}else if(this.getBlocked_timeDirtyFlag()){
			map.put("blocked_time",false);
		}
		if(this.getCapacity()!=null&&this.getCapacityDirtyFlag()){
			map.put("capacity",this.getCapacity());
		}else if(this.getCapacityDirtyFlag()){
			map.put("capacity",false);
		}
		if(this.getCode()!=null&&this.getCodeDirtyFlag()){
			map.put("code",this.getCode());
		}else if(this.getCodeDirtyFlag()){
			map.put("code",false);
		}
		if(this.getColor()!=null&&this.getColorDirtyFlag()){
			map.put("color",this.getColor());
		}else if(this.getColorDirtyFlag()){
			map.put("color",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_id_text()!=null&&this.getCompany_id_textDirtyFlag()){
			//忽略文本外键company_id_text
		}else if(this.getCompany_id_textDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCosts_hour()!=null&&this.getCosts_hourDirtyFlag()){
			map.put("costs_hour",this.getCosts_hour());
		}else if(this.getCosts_hourDirtyFlag()){
			map.put("costs_hour",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getNote()!=null&&this.getNoteDirtyFlag()){
			map.put("note",this.getNote());
		}else if(this.getNoteDirtyFlag()){
			map.put("note",false);
		}
		if(this.getOee()!=null&&this.getOeeDirtyFlag()){
			map.put("oee",this.getOee());
		}else if(this.getOeeDirtyFlag()){
			map.put("oee",false);
		}
		if(this.getOee_target()!=null&&this.getOee_targetDirtyFlag()){
			map.put("oee_target",this.getOee_target());
		}else if(this.getOee_targetDirtyFlag()){
			map.put("oee_target",false);
		}
		if(this.getOrder_ids()!=null&&this.getOrder_idsDirtyFlag()){
			map.put("order_ids",this.getOrder_ids());
		}else if(this.getOrder_idsDirtyFlag()){
			map.put("order_ids",false);
		}
		if(this.getPerformance()!=null&&this.getPerformanceDirtyFlag()){
			map.put("performance",this.getPerformance());
		}else if(this.getPerformanceDirtyFlag()){
			map.put("performance",false);
		}
		if(this.getProductive_time()!=null&&this.getProductive_timeDirtyFlag()){
			map.put("productive_time",this.getProductive_time());
		}else if(this.getProductive_timeDirtyFlag()){
			map.put("productive_time",false);
		}
		if(this.getResource_calendar_id()!=null&&this.getResource_calendar_idDirtyFlag()){
			map.put("resource_calendar_id",this.getResource_calendar_id());
		}else if(this.getResource_calendar_idDirtyFlag()){
			map.put("resource_calendar_id",false);
		}
		if(this.getResource_calendar_id_text()!=null&&this.getResource_calendar_id_textDirtyFlag()){
			//忽略文本外键resource_calendar_id_text
		}else if(this.getResource_calendar_id_textDirtyFlag()){
			map.put("resource_calendar_id",false);
		}
		if(this.getResource_id()!=null&&this.getResource_idDirtyFlag()){
			map.put("resource_id",this.getResource_id());
		}else if(this.getResource_idDirtyFlag()){
			map.put("resource_id",false);
		}
		if(this.getRouting_line_ids()!=null&&this.getRouting_line_idsDirtyFlag()){
			map.put("routing_line_ids",this.getRouting_line_ids());
		}else if(this.getRouting_line_idsDirtyFlag()){
			map.put("routing_line_ids",false);
		}
		if(this.getSequence()!=null&&this.getSequenceDirtyFlag()){
			map.put("sequence",this.getSequence());
		}else if(this.getSequenceDirtyFlag()){
			map.put("sequence",false);
		}
		if(this.getTime_efficiency()!=null&&this.getTime_efficiencyDirtyFlag()){
			map.put("time_efficiency",this.getTime_efficiency());
		}else if(this.getTime_efficiencyDirtyFlag()){
			map.put("time_efficiency",false);
		}
		if(this.getTime_ids()!=null&&this.getTime_idsDirtyFlag()){
			map.put("time_ids",this.getTime_ids());
		}else if(this.getTime_idsDirtyFlag()){
			map.put("time_ids",false);
		}
		if(this.getTime_start()!=null&&this.getTime_startDirtyFlag()){
			map.put("time_start",this.getTime_start());
		}else if(this.getTime_startDirtyFlag()){
			map.put("time_start",false);
		}
		if(this.getTime_stop()!=null&&this.getTime_stopDirtyFlag()){
			map.put("time_stop",this.getTime_stop());
		}else if(this.getTime_stopDirtyFlag()){
			map.put("time_stop",false);
		}
		if(this.getTz()!=null&&this.getTzDirtyFlag()){
			map.put("tz",this.getTz());
		}else if(this.getTzDirtyFlag()){
			map.put("tz",false);
		}
		if(this.getWorkcenter_load()!=null&&this.getWorkcenter_loadDirtyFlag()){
			map.put("workcenter_load",this.getWorkcenter_load());
		}else if(this.getWorkcenter_loadDirtyFlag()){
			map.put("workcenter_load",false);
		}
		if(this.getWorking_state()!=null&&this.getWorking_stateDirtyFlag()){
			map.put("working_state",this.getWorking_state());
		}else if(this.getWorking_stateDirtyFlag()){
			map.put("working_state",false);
		}
		if(this.getWorkorder_count()!=null&&this.getWorkorder_countDirtyFlag()){
			map.put("workorder_count",this.getWorkorder_count());
		}else if(this.getWorkorder_countDirtyFlag()){
			map.put("workorder_count",false);
		}
		if(this.getWorkorder_late_count()!=null&&this.getWorkorder_late_countDirtyFlag()){
			map.put("workorder_late_count",this.getWorkorder_late_count());
		}else if(this.getWorkorder_late_countDirtyFlag()){
			map.put("workorder_late_count",false);
		}
		if(this.getWorkorder_pending_count()!=null&&this.getWorkorder_pending_countDirtyFlag()){
			map.put("workorder_pending_count",this.getWorkorder_pending_count());
		}else if(this.getWorkorder_pending_countDirtyFlag()){
			map.put("workorder_pending_count",false);
		}
		if(this.getWorkorder_progress_count()!=null&&this.getWorkorder_progress_countDirtyFlag()){
			map.put("workorder_progress_count",this.getWorkorder_progress_count());
		}else if(this.getWorkorder_progress_countDirtyFlag()){
			map.put("workorder_progress_count",false);
		}
		if(this.getWorkorder_ready_count()!=null&&this.getWorkorder_ready_countDirtyFlag()){
			map.put("workorder_ready_count",this.getWorkorder_ready_count());
		}else if(this.getWorkorder_ready_countDirtyFlag()){
			map.put("workorder_ready_count",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
