package cn.ibizlab.odoo.client.odoo_mrp.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imrp_workorder;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_workorder] 服务对象客户端接口
 */
public interface Imrp_workorderOdooClient {
    
        public void updateBatch(Imrp_workorder mrp_workorder);

        public Page<Imrp_workorder> search(SearchContext context);

        public void removeBatch(Imrp_workorder mrp_workorder);

        public void create(Imrp_workorder mrp_workorder);

        public void createBatch(Imrp_workorder mrp_workorder);

        public void remove(Imrp_workorder mrp_workorder);

        public void update(Imrp_workorder mrp_workorder);

        public void get(Imrp_workorder mrp_workorder);

        public List<Imrp_workorder> select();


}