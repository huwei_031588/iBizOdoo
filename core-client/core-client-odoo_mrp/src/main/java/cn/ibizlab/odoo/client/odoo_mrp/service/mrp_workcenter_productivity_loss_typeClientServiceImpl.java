package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_workcenter_productivity_loss_type;
import cn.ibizlab.odoo.core.client.service.Imrp_workcenter_productivity_loss_typeClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_workcenter_productivity_loss_typeImpl;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.Imrp_workcenter_productivity_loss_typeOdooClient;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.impl.mrp_workcenter_productivity_loss_typeOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mrp_workcenter_productivity_loss_type] 服务对象接口
 */
@Service
public class mrp_workcenter_productivity_loss_typeClientServiceImpl implements Imrp_workcenter_productivity_loss_typeClientService {
    @Autowired
    private  Imrp_workcenter_productivity_loss_typeOdooClient  mrp_workcenter_productivity_loss_typeOdooClient;

    public Imrp_workcenter_productivity_loss_type createModel() {		
		return new mrp_workcenter_productivity_loss_typeImpl();
	}


        public void createBatch(List<Imrp_workcenter_productivity_loss_type> mrp_workcenter_productivity_loss_types){
            
        }
        
        public void create(Imrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type){
this.mrp_workcenter_productivity_loss_typeOdooClient.create(mrp_workcenter_productivity_loss_type) ;
        }
        
        public void removeBatch(List<Imrp_workcenter_productivity_loss_type> mrp_workcenter_productivity_loss_types){
            
        }
        
        public void updateBatch(List<Imrp_workcenter_productivity_loss_type> mrp_workcenter_productivity_loss_types){
            
        }
        
        public void get(Imrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type){
            this.mrp_workcenter_productivity_loss_typeOdooClient.get(mrp_workcenter_productivity_loss_type) ;
        }
        
        public void update(Imrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type){
this.mrp_workcenter_productivity_loss_typeOdooClient.update(mrp_workcenter_productivity_loss_type) ;
        }
        
        public void remove(Imrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type){
this.mrp_workcenter_productivity_loss_typeOdooClient.remove(mrp_workcenter_productivity_loss_type) ;
        }
        
        public Page<Imrp_workcenter_productivity_loss_type> search(SearchContext context){
            return this.mrp_workcenter_productivity_loss_typeOdooClient.search(context) ;
        }
        
        public Page<Imrp_workcenter_productivity_loss_type> select(SearchContext context){
            return null ;
        }
        

}

