package cn.ibizlab.odoo.client.odoo_mrp.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imrp_unbuild;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_unbuild] 服务对象客户端接口
 */
public interface Imrp_unbuildOdooClient {
    
        public void createBatch(Imrp_unbuild mrp_unbuild);

        public void updateBatch(Imrp_unbuild mrp_unbuild);

        public void create(Imrp_unbuild mrp_unbuild);

        public Page<Imrp_unbuild> search(SearchContext context);

        public void removeBatch(Imrp_unbuild mrp_unbuild);

        public void update(Imrp_unbuild mrp_unbuild);

        public void remove(Imrp_unbuild mrp_unbuild);

        public void get(Imrp_unbuild mrp_unbuild);

        public List<Imrp_unbuild> select();


}