package cn.ibizlab.odoo.client.odoo_mrp.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imrp_product_produce_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_product_produce_line] 服务对象客户端接口
 */
public interface Imrp_product_produce_lineOdooClient {
    
        public void createBatch(Imrp_product_produce_line mrp_product_produce_line);

        public void removeBatch(Imrp_product_produce_line mrp_product_produce_line);

        public void update(Imrp_product_produce_line mrp_product_produce_line);

        public Page<Imrp_product_produce_line> search(SearchContext context);

        public void remove(Imrp_product_produce_line mrp_product_produce_line);

        public void get(Imrp_product_produce_line mrp_product_produce_line);

        public void updateBatch(Imrp_product_produce_line mrp_product_produce_line);

        public void create(Imrp_product_produce_line mrp_product_produce_line);

        public List<Imrp_product_produce_line> select();


}