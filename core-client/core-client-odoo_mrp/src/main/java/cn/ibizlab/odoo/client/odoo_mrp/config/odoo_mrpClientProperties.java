package cn.ibizlab.odoo.client.odoo_mrp.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.mrp")
@Data
public class odoo_mrpClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
