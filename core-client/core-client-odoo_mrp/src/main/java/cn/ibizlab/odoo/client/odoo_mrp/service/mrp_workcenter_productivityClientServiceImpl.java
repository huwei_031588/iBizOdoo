package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_workcenter_productivity;
import cn.ibizlab.odoo.core.client.service.Imrp_workcenter_productivityClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_workcenter_productivityImpl;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.Imrp_workcenter_productivityOdooClient;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.impl.mrp_workcenter_productivityOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mrp_workcenter_productivity] 服务对象接口
 */
@Service
public class mrp_workcenter_productivityClientServiceImpl implements Imrp_workcenter_productivityClientService {
    @Autowired
    private  Imrp_workcenter_productivityOdooClient  mrp_workcenter_productivityOdooClient;

    public Imrp_workcenter_productivity createModel() {		
		return new mrp_workcenter_productivityImpl();
	}


        public void updateBatch(List<Imrp_workcenter_productivity> mrp_workcenter_productivities){
            
        }
        
        public void remove(Imrp_workcenter_productivity mrp_workcenter_productivity){
this.mrp_workcenter_productivityOdooClient.remove(mrp_workcenter_productivity) ;
        }
        
        public void update(Imrp_workcenter_productivity mrp_workcenter_productivity){
this.mrp_workcenter_productivityOdooClient.update(mrp_workcenter_productivity) ;
        }
        
        public Page<Imrp_workcenter_productivity> search(SearchContext context){
            return this.mrp_workcenter_productivityOdooClient.search(context) ;
        }
        
        public void create(Imrp_workcenter_productivity mrp_workcenter_productivity){
this.mrp_workcenter_productivityOdooClient.create(mrp_workcenter_productivity) ;
        }
        
        public void createBatch(List<Imrp_workcenter_productivity> mrp_workcenter_productivities){
            
        }
        
        public void removeBatch(List<Imrp_workcenter_productivity> mrp_workcenter_productivities){
            
        }
        
        public void get(Imrp_workcenter_productivity mrp_workcenter_productivity){
            this.mrp_workcenter_productivityOdooClient.get(mrp_workcenter_productivity) ;
        }
        
        public Page<Imrp_workcenter_productivity> select(SearchContext context){
            return null ;
        }
        

}

