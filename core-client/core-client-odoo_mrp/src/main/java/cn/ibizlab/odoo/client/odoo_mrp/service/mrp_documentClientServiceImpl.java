package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_document;
import cn.ibizlab.odoo.core.client.service.Imrp_documentClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_documentImpl;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.Imrp_documentOdooClient;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.impl.mrp_documentOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mrp_document] 服务对象接口
 */
@Service
public class mrp_documentClientServiceImpl implements Imrp_documentClientService {
    @Autowired
    private  Imrp_documentOdooClient  mrp_documentOdooClient;

    public Imrp_document createModel() {		
		return new mrp_documentImpl();
	}


        public void removeBatch(List<Imrp_document> mrp_documents){
            
        }
        
        public void create(Imrp_document mrp_document){
this.mrp_documentOdooClient.create(mrp_document) ;
        }
        
        public Page<Imrp_document> search(SearchContext context){
            return this.mrp_documentOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Imrp_document> mrp_documents){
            
        }
        
        public void createBatch(List<Imrp_document> mrp_documents){
            
        }
        
        public void get(Imrp_document mrp_document){
            this.mrp_documentOdooClient.get(mrp_document) ;
        }
        
        public void remove(Imrp_document mrp_document){
this.mrp_documentOdooClient.remove(mrp_document) ;
        }
        
        public void update(Imrp_document mrp_document){
this.mrp_documentOdooClient.update(mrp_document) ;
        }
        
        public Page<Imrp_document> select(SearchContext context){
            return null ;
        }
        

}

