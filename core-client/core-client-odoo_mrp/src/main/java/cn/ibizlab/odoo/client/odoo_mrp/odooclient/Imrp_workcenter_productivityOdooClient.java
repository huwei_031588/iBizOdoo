package cn.ibizlab.odoo.client.odoo_mrp.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imrp_workcenter_productivity;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_workcenter_productivity] 服务对象客户端接口
 */
public interface Imrp_workcenter_productivityOdooClient {
    
        public void updateBatch(Imrp_workcenter_productivity mrp_workcenter_productivity);

        public void remove(Imrp_workcenter_productivity mrp_workcenter_productivity);

        public void update(Imrp_workcenter_productivity mrp_workcenter_productivity);

        public Page<Imrp_workcenter_productivity> search(SearchContext context);

        public void create(Imrp_workcenter_productivity mrp_workcenter_productivity);

        public void createBatch(Imrp_workcenter_productivity mrp_workcenter_productivity);

        public void removeBatch(Imrp_workcenter_productivity mrp_workcenter_productivity);

        public void get(Imrp_workcenter_productivity mrp_workcenter_productivity);

        public List<Imrp_workcenter_productivity> select();


}