package cn.ibizlab.odoo.client.odoo_mrp.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imrp_bom;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_bom] 服务对象客户端接口
 */
public interface Imrp_bomOdooClient {
    
        public void create(Imrp_bom mrp_bom);

        public void remove(Imrp_bom mrp_bom);

        public void update(Imrp_bom mrp_bom);

        public void get(Imrp_bom mrp_bom);

        public void createBatch(Imrp_bom mrp_bom);

        public void updateBatch(Imrp_bom mrp_bom);

        public void removeBatch(Imrp_bom mrp_bom);

        public Page<Imrp_bom> search(SearchContext context);

        public List<Imrp_bom> select();


}