package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_bom;
import cn.ibizlab.odoo.core.client.service.Imrp_bomClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_bomImpl;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.Imrp_bomOdooClient;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.impl.mrp_bomOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mrp_bom] 服务对象接口
 */
@Service
public class mrp_bomClientServiceImpl implements Imrp_bomClientService {
    @Autowired
    private  Imrp_bomOdooClient  mrp_bomOdooClient;

    public Imrp_bom createModel() {		
		return new mrp_bomImpl();
	}


        public void create(Imrp_bom mrp_bom){
this.mrp_bomOdooClient.create(mrp_bom) ;
        }
        
        public void remove(Imrp_bom mrp_bom){
this.mrp_bomOdooClient.remove(mrp_bom) ;
        }
        
        public void update(Imrp_bom mrp_bom){
this.mrp_bomOdooClient.update(mrp_bom) ;
        }
        
        public void get(Imrp_bom mrp_bom){
            this.mrp_bomOdooClient.get(mrp_bom) ;
        }
        
        public void createBatch(List<Imrp_bom> mrp_boms){
            
        }
        
        public void updateBatch(List<Imrp_bom> mrp_boms){
            
        }
        
        public void removeBatch(List<Imrp_bom> mrp_boms){
            
        }
        
        public Page<Imrp_bom> search(SearchContext context){
            return this.mrp_bomOdooClient.search(context) ;
        }
        
        public Page<Imrp_bom> select(SearchContext context){
            return null ;
        }
        

}

