package cn.ibizlab.odoo.client.odoo_mrp.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imrp_product_produce;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_product_produce] 服务对象客户端接口
 */
public interface Imrp_product_produceOdooClient {
    
        public void createBatch(Imrp_product_produce mrp_product_produce);

        public Page<Imrp_product_produce> search(SearchContext context);

        public void removeBatch(Imrp_product_produce mrp_product_produce);

        public void updateBatch(Imrp_product_produce mrp_product_produce);

        public void get(Imrp_product_produce mrp_product_produce);

        public void remove(Imrp_product_produce mrp_product_produce);

        public void create(Imrp_product_produce mrp_product_produce);

        public void update(Imrp_product_produce mrp_product_produce);

        public List<Imrp_product_produce> select();


}