package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_production;
import cn.ibizlab.odoo.core.client.service.Imrp_productionClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_productionImpl;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.Imrp_productionOdooClient;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.impl.mrp_productionOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mrp_production] 服务对象接口
 */
@Service
public class mrp_productionClientServiceImpl implements Imrp_productionClientService {
    @Autowired
    private  Imrp_productionOdooClient  mrp_productionOdooClient;

    public Imrp_production createModel() {		
		return new mrp_productionImpl();
	}


        public void removeBatch(List<Imrp_production> mrp_productions){
            
        }
        
        public void createBatch(List<Imrp_production> mrp_productions){
            
        }
        
        public void create(Imrp_production mrp_production){
this.mrp_productionOdooClient.create(mrp_production) ;
        }
        
        public void updateBatch(List<Imrp_production> mrp_productions){
            
        }
        
        public void get(Imrp_production mrp_production){
            this.mrp_productionOdooClient.get(mrp_production) ;
        }
        
        public Page<Imrp_production> search(SearchContext context){
            return this.mrp_productionOdooClient.search(context) ;
        }
        
        public void remove(Imrp_production mrp_production){
this.mrp_productionOdooClient.remove(mrp_production) ;
        }
        
        public void update(Imrp_production mrp_production){
this.mrp_productionOdooClient.update(mrp_production) ;
        }
        
        public Page<Imrp_production> select(SearchContext context){
            return null ;
        }
        

}

