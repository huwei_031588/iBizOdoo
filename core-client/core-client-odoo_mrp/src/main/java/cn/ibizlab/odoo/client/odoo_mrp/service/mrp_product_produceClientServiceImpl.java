package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_product_produce;
import cn.ibizlab.odoo.core.client.service.Imrp_product_produceClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_product_produceImpl;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.Imrp_product_produceOdooClient;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.impl.mrp_product_produceOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mrp_product_produce] 服务对象接口
 */
@Service
public class mrp_product_produceClientServiceImpl implements Imrp_product_produceClientService {
    @Autowired
    private  Imrp_product_produceOdooClient  mrp_product_produceOdooClient;

    public Imrp_product_produce createModel() {		
		return new mrp_product_produceImpl();
	}


        public void createBatch(List<Imrp_product_produce> mrp_product_produces){
            
        }
        
        public Page<Imrp_product_produce> search(SearchContext context){
            return this.mrp_product_produceOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Imrp_product_produce> mrp_product_produces){
            
        }
        
        public void updateBatch(List<Imrp_product_produce> mrp_product_produces){
            
        }
        
        public void get(Imrp_product_produce mrp_product_produce){
            this.mrp_product_produceOdooClient.get(mrp_product_produce) ;
        }
        
        public void remove(Imrp_product_produce mrp_product_produce){
this.mrp_product_produceOdooClient.remove(mrp_product_produce) ;
        }
        
        public void create(Imrp_product_produce mrp_product_produce){
this.mrp_product_produceOdooClient.create(mrp_product_produce) ;
        }
        
        public void update(Imrp_product_produce mrp_product_produce){
this.mrp_product_produceOdooClient.update(mrp_product_produce) ;
        }
        
        public Page<Imrp_product_produce> select(SearchContext context){
            return null ;
        }
        

}

