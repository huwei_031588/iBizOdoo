package cn.ibizlab.odoo.client.odoo_mrp.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imrp_production;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_production] 服务对象客户端接口
 */
public interface Imrp_productionOdooClient {
    
        public void removeBatch(Imrp_production mrp_production);

        public void createBatch(Imrp_production mrp_production);

        public void create(Imrp_production mrp_production);

        public void updateBatch(Imrp_production mrp_production);

        public void get(Imrp_production mrp_production);

        public Page<Imrp_production> search(SearchContext context);

        public void remove(Imrp_production mrp_production);

        public void update(Imrp_production mrp_production);

        public List<Imrp_production> select();


}