package cn.ibizlab.odoo.client.odoo_mrp.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imrp_workcenter_productivity_loss_type;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_workcenter_productivity_loss_type] 服务对象客户端接口
 */
public interface Imrp_workcenter_productivity_loss_typeOdooClient {
    
        public void createBatch(Imrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type);

        public void create(Imrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type);

        public void removeBatch(Imrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type);

        public void updateBatch(Imrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type);

        public void get(Imrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type);

        public void update(Imrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type);

        public void remove(Imrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type);

        public Page<Imrp_workcenter_productivity_loss_type> search(SearchContext context);

        public List<Imrp_workcenter_productivity_loss_type> select();


}