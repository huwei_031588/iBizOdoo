package cn.ibizlab.odoo.client.odoo_mrp.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imrp_workcenter_productivity_loss;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_workcenter_productivity_loss] 服务对象客户端接口
 */
public interface Imrp_workcenter_productivity_lossOdooClient {
    
        public Page<Imrp_workcenter_productivity_loss> search(SearchContext context);

        public void update(Imrp_workcenter_productivity_loss mrp_workcenter_productivity_loss);

        public void create(Imrp_workcenter_productivity_loss mrp_workcenter_productivity_loss);

        public void remove(Imrp_workcenter_productivity_loss mrp_workcenter_productivity_loss);

        public void removeBatch(Imrp_workcenter_productivity_loss mrp_workcenter_productivity_loss);

        public void createBatch(Imrp_workcenter_productivity_loss mrp_workcenter_productivity_loss);

        public void updateBatch(Imrp_workcenter_productivity_loss mrp_workcenter_productivity_loss);

        public void get(Imrp_workcenter_productivity_loss mrp_workcenter_productivity_loss);

        public List<Imrp_workcenter_productivity_loss> select();


}