package cn.ibizlab.odoo.client.odoo_mrp.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imrp_routing_workcenter;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_routing_workcenter] 服务对象客户端接口
 */
public interface Imrp_routing_workcenterOdooClient {
    
        public void createBatch(Imrp_routing_workcenter mrp_routing_workcenter);

        public void remove(Imrp_routing_workcenter mrp_routing_workcenter);

        public Page<Imrp_routing_workcenter> search(SearchContext context);

        public void get(Imrp_routing_workcenter mrp_routing_workcenter);

        public void updateBatch(Imrp_routing_workcenter mrp_routing_workcenter);

        public void removeBatch(Imrp_routing_workcenter mrp_routing_workcenter);

        public void update(Imrp_routing_workcenter mrp_routing_workcenter);

        public void create(Imrp_routing_workcenter mrp_routing_workcenter);

        public List<Imrp_routing_workcenter> select();


}