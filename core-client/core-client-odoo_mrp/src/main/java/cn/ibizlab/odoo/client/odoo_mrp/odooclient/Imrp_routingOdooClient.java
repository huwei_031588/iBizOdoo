package cn.ibizlab.odoo.client.odoo_mrp.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imrp_routing;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_routing] 服务对象客户端接口
 */
public interface Imrp_routingOdooClient {
    
        public void update(Imrp_routing mrp_routing);

        public void get(Imrp_routing mrp_routing);

        public void create(Imrp_routing mrp_routing);

        public void createBatch(Imrp_routing mrp_routing);

        public Page<Imrp_routing> search(SearchContext context);

        public void removeBatch(Imrp_routing mrp_routing);

        public void remove(Imrp_routing mrp_routing);

        public void updateBatch(Imrp_routing mrp_routing);

        public List<Imrp_routing> select();


}