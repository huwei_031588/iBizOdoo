package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_routing_workcenter;
import cn.ibizlab.odoo.core.client.service.Imrp_routing_workcenterClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_routing_workcenterImpl;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.Imrp_routing_workcenterOdooClient;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.impl.mrp_routing_workcenterOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mrp_routing_workcenter] 服务对象接口
 */
@Service
public class mrp_routing_workcenterClientServiceImpl implements Imrp_routing_workcenterClientService {
    @Autowired
    private  Imrp_routing_workcenterOdooClient  mrp_routing_workcenterOdooClient;

    public Imrp_routing_workcenter createModel() {		
		return new mrp_routing_workcenterImpl();
	}


        public void createBatch(List<Imrp_routing_workcenter> mrp_routing_workcenters){
            
        }
        
        public void remove(Imrp_routing_workcenter mrp_routing_workcenter){
this.mrp_routing_workcenterOdooClient.remove(mrp_routing_workcenter) ;
        }
        
        public Page<Imrp_routing_workcenter> search(SearchContext context){
            return this.mrp_routing_workcenterOdooClient.search(context) ;
        }
        
        public void get(Imrp_routing_workcenter mrp_routing_workcenter){
            this.mrp_routing_workcenterOdooClient.get(mrp_routing_workcenter) ;
        }
        
        public void updateBatch(List<Imrp_routing_workcenter> mrp_routing_workcenters){
            
        }
        
        public void removeBatch(List<Imrp_routing_workcenter> mrp_routing_workcenters){
            
        }
        
        public void update(Imrp_routing_workcenter mrp_routing_workcenter){
this.mrp_routing_workcenterOdooClient.update(mrp_routing_workcenter) ;
        }
        
        public void create(Imrp_routing_workcenter mrp_routing_workcenter){
this.mrp_routing_workcenterOdooClient.create(mrp_routing_workcenter) ;
        }
        
        public Page<Imrp_routing_workcenter> select(SearchContext context){
            return null ;
        }
        

}

