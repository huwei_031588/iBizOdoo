package cn.ibizlab.odoo.client.odoo_mrp.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imrp_document;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_document] 服务对象客户端接口
 */
public interface Imrp_documentOdooClient {
    
        public void removeBatch(Imrp_document mrp_document);

        public void create(Imrp_document mrp_document);

        public Page<Imrp_document> search(SearchContext context);

        public void updateBatch(Imrp_document mrp_document);

        public void createBatch(Imrp_document mrp_document);

        public void get(Imrp_document mrp_document);

        public void remove(Imrp_document mrp_document);

        public void update(Imrp_document mrp_document);

        public List<Imrp_document> select();


}