package cn.ibizlab.odoo.client.odoo_asset.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iasset_state;
import cn.ibizlab.odoo.core.client.service.Iasset_stateClientService;
import cn.ibizlab.odoo.client.odoo_asset.model.asset_stateImpl;
import cn.ibizlab.odoo.client.odoo_asset.odooclient.Iasset_stateOdooClient;
import cn.ibizlab.odoo.client.odoo_asset.odooclient.impl.asset_stateOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[asset_state] 服务对象接口
 */
@Service
public class asset_stateClientServiceImpl implements Iasset_stateClientService {
    @Autowired
    private  Iasset_stateOdooClient  asset_stateOdooClient;

    public Iasset_state createModel() {		
		return new asset_stateImpl();
	}


        public void updateBatch(List<Iasset_state> asset_states){
            
        }
        
        public void get(Iasset_state asset_state){
            this.asset_stateOdooClient.get(asset_state) ;
        }
        
        public void create(Iasset_state asset_state){
this.asset_stateOdooClient.create(asset_state) ;
        }
        
        public void removeBatch(List<Iasset_state> asset_states){
            
        }
        
        public void remove(Iasset_state asset_state){
this.asset_stateOdooClient.remove(asset_state) ;
        }
        
        public void createBatch(List<Iasset_state> asset_states){
            
        }
        
        public Page<Iasset_state> search(SearchContext context){
            return this.asset_stateOdooClient.search(context) ;
        }
        
        public void update(Iasset_state asset_state){
this.asset_stateOdooClient.update(asset_state) ;
        }
        
        public Page<Iasset_state> select(SearchContext context){
            return null ;
        }
        

}

