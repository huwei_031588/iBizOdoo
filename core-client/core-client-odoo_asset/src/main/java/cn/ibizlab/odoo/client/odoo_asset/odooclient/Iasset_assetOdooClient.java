package cn.ibizlab.odoo.client.odoo_asset.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iasset_asset;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[asset_asset] 服务对象客户端接口
 */
public interface Iasset_assetOdooClient {
    
        public void get(Iasset_asset asset_asset);

        public Page<Iasset_asset> search(SearchContext context);

        public void create(Iasset_asset asset_asset);

        public void update(Iasset_asset asset_asset);

        public void createBatch(Iasset_asset asset_asset);

        public void updateBatch(Iasset_asset asset_asset);

        public void remove(Iasset_asset asset_asset);

        public void removeBatch(Iasset_asset asset_asset);

        public List<Iasset_asset> select();


}