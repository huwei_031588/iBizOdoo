package cn.ibizlab.odoo.client.odoo_asset.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.asset")
@Data
public class odoo_assetClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
