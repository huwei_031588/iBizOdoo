package cn.ibizlab.odoo.client.odoo_asset.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iasset_state;
import cn.ibizlab.odoo.client.odoo_asset.model.asset_stateImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[asset_state] 服务对象接口
 */
public interface asset_stateFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_asset/asset_states/updatebatch")
    public asset_stateImpl updateBatch(@RequestBody List<asset_stateImpl> asset_states);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_asset/asset_states/{id}")
    public asset_stateImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_asset/asset_states")
    public asset_stateImpl create(@RequestBody asset_stateImpl asset_state);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_asset/asset_states/removebatch")
    public asset_stateImpl removeBatch(@RequestBody List<asset_stateImpl> asset_states);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_asset/asset_states/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_asset/asset_states/createbatch")
    public asset_stateImpl createBatch(@RequestBody List<asset_stateImpl> asset_states);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_asset/asset_states/search")
    public Page<asset_stateImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_asset/asset_states/{id}")
    public asset_stateImpl update(@PathVariable("id") Integer id,@RequestBody asset_stateImpl asset_state);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_asset/asset_states/select")
    public Page<asset_stateImpl> select();



}
