package cn.ibizlab.odoo.client.odoo_asset.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iasset_category;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[asset_category] 服务对象客户端接口
 */
public interface Iasset_categoryOdooClient {
    
        public void create(Iasset_category asset_category);

        public void get(Iasset_category asset_category);

        public void remove(Iasset_category asset_category);

        public Page<Iasset_category> search(SearchContext context);

        public void removeBatch(Iasset_category asset_category);

        public void createBatch(Iasset_category asset_category);

        public void update(Iasset_category asset_category);

        public void updateBatch(Iasset_category asset_category);

        public List<Iasset_category> select();


}