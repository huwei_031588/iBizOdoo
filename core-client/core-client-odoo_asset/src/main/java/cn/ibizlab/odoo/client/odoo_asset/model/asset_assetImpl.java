package cn.ibizlab.odoo.client.odoo_asset.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iasset_asset;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[asset_asset] 对象
 */
public class asset_assetImpl implements Iasset_asset,Serializable{

    /**
     * 省/ 州
     */
    public Integer accounting_state_id;

    @JsonIgnore
    public boolean accounting_state_idDirtyFlag;
    
    /**
     * 省/ 州
     */
    public String accounting_state_id_text;

    @JsonIgnore
    public boolean accounting_state_id_textDirtyFlag;
    
    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * Asset Number
     */
    public String asset_number;

    @JsonIgnore
    public boolean asset_numberDirtyFlag;
    
    /**
     * 标签
     */
    public String category_ids;

    @JsonIgnore
    public boolean category_idsDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * Criticality
     */
    public String criticality;

    @JsonIgnore
    public boolean criticalityDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 省/ 州
     */
    public Integer finance_state_id;

    @JsonIgnore
    public boolean finance_state_idDirtyFlag;
    
    /**
     * 省/ 州
     */
    public String finance_state_id_text;

    @JsonIgnore
    public boolean finance_state_id_textDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 图像
     */
    public byte[] image;

    @JsonIgnore
    public boolean imageDirtyFlag;
    
    /**
     * 中等尺寸图像
     */
    public byte[] image_medium;

    @JsonIgnore
    public boolean image_mediumDirtyFlag;
    
    /**
     * 小尺寸图像
     */
    public byte[] image_small;

    @JsonIgnore
    public boolean image_smallDirtyFlag;
    
    /**
     * Maintenance Date
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp maintenance_date;

    @JsonIgnore
    public boolean maintenance_dateDirtyFlag;
    
    /**
     * 颜色
     */
    public String maintenance_state_color;

    @JsonIgnore
    public boolean maintenance_state_colorDirtyFlag;
    
    /**
     * 省/ 州
     */
    public Integer maintenance_state_id;

    @JsonIgnore
    public boolean maintenance_state_idDirtyFlag;
    
    /**
     * 省/ 州
     */
    public String maintenance_state_id_text;

    @JsonIgnore
    public boolean maintenance_state_id_textDirtyFlag;
    
    /**
     * Manufacturer
     */
    public Integer manufacturer_id;

    @JsonIgnore
    public boolean manufacturer_idDirtyFlag;
    
    /**
     * Manufacturer
     */
    public String manufacturer_id_text;

    @JsonIgnore
    public boolean manufacturer_id_textDirtyFlag;
    
    /**
     * 省/ 州
     */
    public Integer manufacture_state_id;

    @JsonIgnore
    public boolean manufacture_state_idDirtyFlag;
    
    /**
     * 省/ 州
     */
    public String manufacture_state_id_text;

    @JsonIgnore
    public boolean manufacture_state_id_textDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误个数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 前置操作
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 操作次数
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * Meter
     */
    public String meter_ids;

    @JsonIgnore
    public boolean meter_idsDirtyFlag;
    
    /**
     * 模型
     */
    public String model;

    @JsonIgnore
    public boolean modelDirtyFlag;
    
    /**
     * # Maintenance
     */
    public Integer mro_count;

    @JsonIgnore
    public boolean mro_countDirtyFlag;
    
    /**
     * Asset Name
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * Map
     */
    public String position;

    @JsonIgnore
    public boolean positionDirtyFlag;
    
    /**
     * Asset Location
     */
    public Integer property_stock_asset;

    @JsonIgnore
    public boolean property_stock_assetDirtyFlag;
    
    /**
     * Purchase Date
     */
    public Timestamp purchase_date;

    @JsonIgnore
    public boolean purchase_dateDirtyFlag;
    
    /**
     * Serial no.
     */
    public String serial;

    @JsonIgnore
    public boolean serialDirtyFlag;
    
    /**
     * 开始日期
     */
    public Timestamp start_date;

    @JsonIgnore
    public boolean start_dateDirtyFlag;
    
    /**
     * 分派给
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 分派给
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 供应商
     */
    public Integer vendor_id;

    @JsonIgnore
    public boolean vendor_idDirtyFlag;
    
    /**
     * 供应商
     */
    public String vendor_id_text;

    @JsonIgnore
    public boolean vendor_id_textDirtyFlag;
    
    /**
     * 省/ 州
     */
    public Integer warehouse_state_id;

    @JsonIgnore
    public boolean warehouse_state_idDirtyFlag;
    
    /**
     * 省/ 州
     */
    public String warehouse_state_id_text;

    @JsonIgnore
    public boolean warehouse_state_id_textDirtyFlag;
    
    /**
     * Warranty End
     */
    public Timestamp warranty_end_date;

    @JsonIgnore
    public boolean warranty_end_dateDirtyFlag;
    
    /**
     * Warranty Start
     */
    public Timestamp warranty_start_date;

    @JsonIgnore
    public boolean warranty_start_dateDirtyFlag;
    
    /**
     * 网站消息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [省/ 州]
     */
    @JsonProperty("accounting_state_id")
    public Integer getAccounting_state_id(){
        return this.accounting_state_id ;
    }

    /**
     * 设置 [省/ 州]
     */
    @JsonProperty("accounting_state_id")
    public void setAccounting_state_id(Integer  accounting_state_id){
        this.accounting_state_id = accounting_state_id ;
        this.accounting_state_idDirtyFlag = true ;
    }

     /**
     * 获取 [省/ 州]脏标记
     */
    @JsonIgnore
    public boolean getAccounting_state_idDirtyFlag(){
        return this.accounting_state_idDirtyFlag ;
    }   

    /**
     * 获取 [省/ 州]
     */
    @JsonProperty("accounting_state_id_text")
    public String getAccounting_state_id_text(){
        return this.accounting_state_id_text ;
    }

    /**
     * 设置 [省/ 州]
     */
    @JsonProperty("accounting_state_id_text")
    public void setAccounting_state_id_text(String  accounting_state_id_text){
        this.accounting_state_id_text = accounting_state_id_text ;
        this.accounting_state_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [省/ 州]脏标记
     */
    @JsonIgnore
    public boolean getAccounting_state_id_textDirtyFlag(){
        return this.accounting_state_id_textDirtyFlag ;
    }   

    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [Asset Number]
     */
    @JsonProperty("asset_number")
    public String getAsset_number(){
        return this.asset_number ;
    }

    /**
     * 设置 [Asset Number]
     */
    @JsonProperty("asset_number")
    public void setAsset_number(String  asset_number){
        this.asset_number = asset_number ;
        this.asset_numberDirtyFlag = true ;
    }

     /**
     * 获取 [Asset Number]脏标记
     */
    @JsonIgnore
    public boolean getAsset_numberDirtyFlag(){
        return this.asset_numberDirtyFlag ;
    }   

    /**
     * 获取 [标签]
     */
    @JsonProperty("category_ids")
    public String getCategory_ids(){
        return this.category_ids ;
    }

    /**
     * 设置 [标签]
     */
    @JsonProperty("category_ids")
    public void setCategory_ids(String  category_ids){
        this.category_ids = category_ids ;
        this.category_idsDirtyFlag = true ;
    }

     /**
     * 获取 [标签]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idsDirtyFlag(){
        return this.category_idsDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [Criticality]
     */
    @JsonProperty("criticality")
    public String getCriticality(){
        return this.criticality ;
    }

    /**
     * 设置 [Criticality]
     */
    @JsonProperty("criticality")
    public void setCriticality(String  criticality){
        this.criticality = criticality ;
        this.criticalityDirtyFlag = true ;
    }

     /**
     * 获取 [Criticality]脏标记
     */
    @JsonIgnore
    public boolean getCriticalityDirtyFlag(){
        return this.criticalityDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [省/ 州]
     */
    @JsonProperty("finance_state_id")
    public Integer getFinance_state_id(){
        return this.finance_state_id ;
    }

    /**
     * 设置 [省/ 州]
     */
    @JsonProperty("finance_state_id")
    public void setFinance_state_id(Integer  finance_state_id){
        this.finance_state_id = finance_state_id ;
        this.finance_state_idDirtyFlag = true ;
    }

     /**
     * 获取 [省/ 州]脏标记
     */
    @JsonIgnore
    public boolean getFinance_state_idDirtyFlag(){
        return this.finance_state_idDirtyFlag ;
    }   

    /**
     * 获取 [省/ 州]
     */
    @JsonProperty("finance_state_id_text")
    public String getFinance_state_id_text(){
        return this.finance_state_id_text ;
    }

    /**
     * 设置 [省/ 州]
     */
    @JsonProperty("finance_state_id_text")
    public void setFinance_state_id_text(String  finance_state_id_text){
        this.finance_state_id_text = finance_state_id_text ;
        this.finance_state_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [省/ 州]脏标记
     */
    @JsonIgnore
    public boolean getFinance_state_id_textDirtyFlag(){
        return this.finance_state_id_textDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [图像]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return this.image ;
    }

    /**
     * 设置 [图像]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

     /**
     * 获取 [图像]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return this.imageDirtyFlag ;
    }   

    /**
     * 获取 [中等尺寸图像]
     */
    @JsonProperty("image_medium")
    public byte[] getImage_medium(){
        return this.image_medium ;
    }

    /**
     * 设置 [中等尺寸图像]
     */
    @JsonProperty("image_medium")
    public void setImage_medium(byte[]  image_medium){
        this.image_medium = image_medium ;
        this.image_mediumDirtyFlag = true ;
    }

     /**
     * 获取 [中等尺寸图像]脏标记
     */
    @JsonIgnore
    public boolean getImage_mediumDirtyFlag(){
        return this.image_mediumDirtyFlag ;
    }   

    /**
     * 获取 [小尺寸图像]
     */
    @JsonProperty("image_small")
    public byte[] getImage_small(){
        return this.image_small ;
    }

    /**
     * 设置 [小尺寸图像]
     */
    @JsonProperty("image_small")
    public void setImage_small(byte[]  image_small){
        this.image_small = image_small ;
        this.image_smallDirtyFlag = true ;
    }

     /**
     * 获取 [小尺寸图像]脏标记
     */
    @JsonIgnore
    public boolean getImage_smallDirtyFlag(){
        return this.image_smallDirtyFlag ;
    }   

    /**
     * 获取 [Maintenance Date]
     */
    @JsonProperty("maintenance_date")
    public Timestamp getMaintenance_date(){
        return this.maintenance_date ;
    }

    /**
     * 设置 [Maintenance Date]
     */
    @JsonProperty("maintenance_date")
    public void setMaintenance_date(Timestamp  maintenance_date){
        this.maintenance_date = maintenance_date ;
        this.maintenance_dateDirtyFlag = true ;
    }

     /**
     * 获取 [Maintenance Date]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_dateDirtyFlag(){
        return this.maintenance_dateDirtyFlag ;
    }   

    /**
     * 获取 [颜色]
     */
    @JsonProperty("maintenance_state_color")
    public String getMaintenance_state_color(){
        return this.maintenance_state_color ;
    }

    /**
     * 设置 [颜色]
     */
    @JsonProperty("maintenance_state_color")
    public void setMaintenance_state_color(String  maintenance_state_color){
        this.maintenance_state_color = maintenance_state_color ;
        this.maintenance_state_colorDirtyFlag = true ;
    }

     /**
     * 获取 [颜色]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_state_colorDirtyFlag(){
        return this.maintenance_state_colorDirtyFlag ;
    }   

    /**
     * 获取 [省/ 州]
     */
    @JsonProperty("maintenance_state_id")
    public Integer getMaintenance_state_id(){
        return this.maintenance_state_id ;
    }

    /**
     * 设置 [省/ 州]
     */
    @JsonProperty("maintenance_state_id")
    public void setMaintenance_state_id(Integer  maintenance_state_id){
        this.maintenance_state_id = maintenance_state_id ;
        this.maintenance_state_idDirtyFlag = true ;
    }

     /**
     * 获取 [省/ 州]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_state_idDirtyFlag(){
        return this.maintenance_state_idDirtyFlag ;
    }   

    /**
     * 获取 [省/ 州]
     */
    @JsonProperty("maintenance_state_id_text")
    public String getMaintenance_state_id_text(){
        return this.maintenance_state_id_text ;
    }

    /**
     * 设置 [省/ 州]
     */
    @JsonProperty("maintenance_state_id_text")
    public void setMaintenance_state_id_text(String  maintenance_state_id_text){
        this.maintenance_state_id_text = maintenance_state_id_text ;
        this.maintenance_state_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [省/ 州]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_state_id_textDirtyFlag(){
        return this.maintenance_state_id_textDirtyFlag ;
    }   

    /**
     * 获取 [Manufacturer]
     */
    @JsonProperty("manufacturer_id")
    public Integer getManufacturer_id(){
        return this.manufacturer_id ;
    }

    /**
     * 设置 [Manufacturer]
     */
    @JsonProperty("manufacturer_id")
    public void setManufacturer_id(Integer  manufacturer_id){
        this.manufacturer_id = manufacturer_id ;
        this.manufacturer_idDirtyFlag = true ;
    }

     /**
     * 获取 [Manufacturer]脏标记
     */
    @JsonIgnore
    public boolean getManufacturer_idDirtyFlag(){
        return this.manufacturer_idDirtyFlag ;
    }   

    /**
     * 获取 [Manufacturer]
     */
    @JsonProperty("manufacturer_id_text")
    public String getManufacturer_id_text(){
        return this.manufacturer_id_text ;
    }

    /**
     * 设置 [Manufacturer]
     */
    @JsonProperty("manufacturer_id_text")
    public void setManufacturer_id_text(String  manufacturer_id_text){
        this.manufacturer_id_text = manufacturer_id_text ;
        this.manufacturer_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [Manufacturer]脏标记
     */
    @JsonIgnore
    public boolean getManufacturer_id_textDirtyFlag(){
        return this.manufacturer_id_textDirtyFlag ;
    }   

    /**
     * 获取 [省/ 州]
     */
    @JsonProperty("manufacture_state_id")
    public Integer getManufacture_state_id(){
        return this.manufacture_state_id ;
    }

    /**
     * 设置 [省/ 州]
     */
    @JsonProperty("manufacture_state_id")
    public void setManufacture_state_id(Integer  manufacture_state_id){
        this.manufacture_state_id = manufacture_state_id ;
        this.manufacture_state_idDirtyFlag = true ;
    }

     /**
     * 获取 [省/ 州]脏标记
     */
    @JsonIgnore
    public boolean getManufacture_state_idDirtyFlag(){
        return this.manufacture_state_idDirtyFlag ;
    }   

    /**
     * 获取 [省/ 州]
     */
    @JsonProperty("manufacture_state_id_text")
    public String getManufacture_state_id_text(){
        return this.manufacture_state_id_text ;
    }

    /**
     * 设置 [省/ 州]
     */
    @JsonProperty("manufacture_state_id_text")
    public void setManufacture_state_id_text(String  manufacture_state_id_text){
        this.manufacture_state_id_text = manufacture_state_id_text ;
        this.manufacture_state_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [省/ 州]脏标记
     */
    @JsonIgnore
    public boolean getManufacture_state_id_textDirtyFlag(){
        return this.manufacture_state_id_textDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误个数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误个数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误个数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [前置操作]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [前置操作]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [前置操作]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [操作次数]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [操作次数]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [操作次数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [Meter]
     */
    @JsonProperty("meter_ids")
    public String getMeter_ids(){
        return this.meter_ids ;
    }

    /**
     * 设置 [Meter]
     */
    @JsonProperty("meter_ids")
    public void setMeter_ids(String  meter_ids){
        this.meter_ids = meter_ids ;
        this.meter_idsDirtyFlag = true ;
    }

     /**
     * 获取 [Meter]脏标记
     */
    @JsonIgnore
    public boolean getMeter_idsDirtyFlag(){
        return this.meter_idsDirtyFlag ;
    }   

    /**
     * 获取 [模型]
     */
    @JsonProperty("model")
    public String getModel(){
        return this.model ;
    }

    /**
     * 设置 [模型]
     */
    @JsonProperty("model")
    public void setModel(String  model){
        this.model = model ;
        this.modelDirtyFlag = true ;
    }

     /**
     * 获取 [模型]脏标记
     */
    @JsonIgnore
    public boolean getModelDirtyFlag(){
        return this.modelDirtyFlag ;
    }   

    /**
     * 获取 [# Maintenance]
     */
    @JsonProperty("mro_count")
    public Integer getMro_count(){
        return this.mro_count ;
    }

    /**
     * 设置 [# Maintenance]
     */
    @JsonProperty("mro_count")
    public void setMro_count(Integer  mro_count){
        this.mro_count = mro_count ;
        this.mro_countDirtyFlag = true ;
    }

     /**
     * 获取 [# Maintenance]脏标记
     */
    @JsonIgnore
    public boolean getMro_countDirtyFlag(){
        return this.mro_countDirtyFlag ;
    }   

    /**
     * 获取 [Asset Name]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [Asset Name]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [Asset Name]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [Map]
     */
    @JsonProperty("position")
    public String getPosition(){
        return this.position ;
    }

    /**
     * 设置 [Map]
     */
    @JsonProperty("position")
    public void setPosition(String  position){
        this.position = position ;
        this.positionDirtyFlag = true ;
    }

     /**
     * 获取 [Map]脏标记
     */
    @JsonIgnore
    public boolean getPositionDirtyFlag(){
        return this.positionDirtyFlag ;
    }   

    /**
     * 获取 [Asset Location]
     */
    @JsonProperty("property_stock_asset")
    public Integer getProperty_stock_asset(){
        return this.property_stock_asset ;
    }

    /**
     * 设置 [Asset Location]
     */
    @JsonProperty("property_stock_asset")
    public void setProperty_stock_asset(Integer  property_stock_asset){
        this.property_stock_asset = property_stock_asset ;
        this.property_stock_assetDirtyFlag = true ;
    }

     /**
     * 获取 [Asset Location]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_assetDirtyFlag(){
        return this.property_stock_assetDirtyFlag ;
    }   

    /**
     * 获取 [Purchase Date]
     */
    @JsonProperty("purchase_date")
    public Timestamp getPurchase_date(){
        return this.purchase_date ;
    }

    /**
     * 设置 [Purchase Date]
     */
    @JsonProperty("purchase_date")
    public void setPurchase_date(Timestamp  purchase_date){
        this.purchase_date = purchase_date ;
        this.purchase_dateDirtyFlag = true ;
    }

     /**
     * 获取 [Purchase Date]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_dateDirtyFlag(){
        return this.purchase_dateDirtyFlag ;
    }   

    /**
     * 获取 [Serial no.]
     */
    @JsonProperty("serial")
    public String getSerial(){
        return this.serial ;
    }

    /**
     * 设置 [Serial no.]
     */
    @JsonProperty("serial")
    public void setSerial(String  serial){
        this.serial = serial ;
        this.serialDirtyFlag = true ;
    }

     /**
     * 获取 [Serial no.]脏标记
     */
    @JsonIgnore
    public boolean getSerialDirtyFlag(){
        return this.serialDirtyFlag ;
    }   

    /**
     * 获取 [开始日期]
     */
    @JsonProperty("start_date")
    public Timestamp getStart_date(){
        return this.start_date ;
    }

    /**
     * 设置 [开始日期]
     */
    @JsonProperty("start_date")
    public void setStart_date(Timestamp  start_date){
        this.start_date = start_date ;
        this.start_dateDirtyFlag = true ;
    }

     /**
     * 获取 [开始日期]脏标记
     */
    @JsonIgnore
    public boolean getStart_dateDirtyFlag(){
        return this.start_dateDirtyFlag ;
    }   

    /**
     * 获取 [分派给]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [分派给]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [分派给]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [分派给]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [分派给]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [分派给]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [供应商]
     */
    @JsonProperty("vendor_id")
    public Integer getVendor_id(){
        return this.vendor_id ;
    }

    /**
     * 设置 [供应商]
     */
    @JsonProperty("vendor_id")
    public void setVendor_id(Integer  vendor_id){
        this.vendor_id = vendor_id ;
        this.vendor_idDirtyFlag = true ;
    }

     /**
     * 获取 [供应商]脏标记
     */
    @JsonIgnore
    public boolean getVendor_idDirtyFlag(){
        return this.vendor_idDirtyFlag ;
    }   

    /**
     * 获取 [供应商]
     */
    @JsonProperty("vendor_id_text")
    public String getVendor_id_text(){
        return this.vendor_id_text ;
    }

    /**
     * 设置 [供应商]
     */
    @JsonProperty("vendor_id_text")
    public void setVendor_id_text(String  vendor_id_text){
        this.vendor_id_text = vendor_id_text ;
        this.vendor_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [供应商]脏标记
     */
    @JsonIgnore
    public boolean getVendor_id_textDirtyFlag(){
        return this.vendor_id_textDirtyFlag ;
    }   

    /**
     * 获取 [省/ 州]
     */
    @JsonProperty("warehouse_state_id")
    public Integer getWarehouse_state_id(){
        return this.warehouse_state_id ;
    }

    /**
     * 设置 [省/ 州]
     */
    @JsonProperty("warehouse_state_id")
    public void setWarehouse_state_id(Integer  warehouse_state_id){
        this.warehouse_state_id = warehouse_state_id ;
        this.warehouse_state_idDirtyFlag = true ;
    }

     /**
     * 获取 [省/ 州]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_state_idDirtyFlag(){
        return this.warehouse_state_idDirtyFlag ;
    }   

    /**
     * 获取 [省/ 州]
     */
    @JsonProperty("warehouse_state_id_text")
    public String getWarehouse_state_id_text(){
        return this.warehouse_state_id_text ;
    }

    /**
     * 设置 [省/ 州]
     */
    @JsonProperty("warehouse_state_id_text")
    public void setWarehouse_state_id_text(String  warehouse_state_id_text){
        this.warehouse_state_id_text = warehouse_state_id_text ;
        this.warehouse_state_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [省/ 州]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_state_id_textDirtyFlag(){
        return this.warehouse_state_id_textDirtyFlag ;
    }   

    /**
     * 获取 [Warranty End]
     */
    @JsonProperty("warranty_end_date")
    public Timestamp getWarranty_end_date(){
        return this.warranty_end_date ;
    }

    /**
     * 设置 [Warranty End]
     */
    @JsonProperty("warranty_end_date")
    public void setWarranty_end_date(Timestamp  warranty_end_date){
        this.warranty_end_date = warranty_end_date ;
        this.warranty_end_dateDirtyFlag = true ;
    }

     /**
     * 获取 [Warranty End]脏标记
     */
    @JsonIgnore
    public boolean getWarranty_end_dateDirtyFlag(){
        return this.warranty_end_dateDirtyFlag ;
    }   

    /**
     * 获取 [Warranty Start]
     */
    @JsonProperty("warranty_start_date")
    public Timestamp getWarranty_start_date(){
        return this.warranty_start_date ;
    }

    /**
     * 设置 [Warranty Start]
     */
    @JsonProperty("warranty_start_date")
    public void setWarranty_start_date(Timestamp  warranty_start_date){
        this.warranty_start_date = warranty_start_date ;
        this.warranty_start_dateDirtyFlag = true ;
    }

     /**
     * 获取 [Warranty Start]脏标记
     */
    @JsonIgnore
    public boolean getWarranty_start_dateDirtyFlag(){
        return this.warranty_start_dateDirtyFlag ;
    }   

    /**
     * 获取 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站消息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("accounting_state_id") instanceof Boolean)&& map.get("accounting_state_id")!=null){
			Object[] objs = (Object[])map.get("accounting_state_id");
			if(objs.length > 0){
				this.setAccounting_state_id((Integer)objs[0]);
			}
		}
		if(!(map.get("accounting_state_id") instanceof Boolean)&& map.get("accounting_state_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("accounting_state_id");
			if(objs.length > 1){
				this.setAccounting_state_id_text((String)objs[1]);
			}
		}
		if(map.get("active") instanceof Boolean){
			this.setActive(((Boolean)map.get("active"))? "true" : "false");
		}
		if(!(map.get("asset_number") instanceof Boolean)&& map.get("asset_number")!=null){
			this.setAsset_number((String)map.get("asset_number"));
		}
		if(!(map.get("category_ids") instanceof Boolean)&& map.get("category_ids")!=null){
			Object[] objs = (Object[])map.get("category_ids");
			if(objs.length > 0){
				Integer[] category_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setCategory_ids(Arrays.toString(category_ids));
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("criticality") instanceof Boolean)&& map.get("criticality")!=null){
			this.setCriticality((String)map.get("criticality"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("finance_state_id") instanceof Boolean)&& map.get("finance_state_id")!=null){
			Object[] objs = (Object[])map.get("finance_state_id");
			if(objs.length > 0){
				this.setFinance_state_id((Integer)objs[0]);
			}
		}
		if(!(map.get("finance_state_id") instanceof Boolean)&& map.get("finance_state_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("finance_state_id");
			if(objs.length > 1){
				this.setFinance_state_id_text((String)objs[1]);
			}
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("image") instanceof Boolean)&& map.get("image")!=null){
			//暂时忽略
			//this.setImage(((String)map.get("image")).getBytes("UTF-8"));
		}
		if(!(map.get("image_medium") instanceof Boolean)&& map.get("image_medium")!=null){
			//暂时忽略
			//this.setImage_medium(((String)map.get("image_medium")).getBytes("UTF-8"));
		}
		if(!(map.get("image_small") instanceof Boolean)&& map.get("image_small")!=null){
			//暂时忽略
			//this.setImage_small(((String)map.get("image_small")).getBytes("UTF-8"));
		}
		if(!(map.get("maintenance_date") instanceof Boolean)&& map.get("maintenance_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("maintenance_date"));
   			this.setMaintenance_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("maintenance_state_color") instanceof Boolean)&& map.get("maintenance_state_color")!=null){
			this.setMaintenance_state_color((String)map.get("maintenance_state_color"));
		}
		if(!(map.get("maintenance_state_id") instanceof Boolean)&& map.get("maintenance_state_id")!=null){
			Object[] objs = (Object[])map.get("maintenance_state_id");
			if(objs.length > 0){
				this.setMaintenance_state_id((Integer)objs[0]);
			}
		}
		if(!(map.get("maintenance_state_id") instanceof Boolean)&& map.get("maintenance_state_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("maintenance_state_id");
			if(objs.length > 1){
				this.setMaintenance_state_id_text((String)objs[1]);
			}
		}
		if(!(map.get("manufacturer_id") instanceof Boolean)&& map.get("manufacturer_id")!=null){
			Object[] objs = (Object[])map.get("manufacturer_id");
			if(objs.length > 0){
				this.setManufacturer_id((Integer)objs[0]);
			}
		}
		if(!(map.get("manufacturer_id") instanceof Boolean)&& map.get("manufacturer_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("manufacturer_id");
			if(objs.length > 1){
				this.setManufacturer_id_text((String)objs[1]);
			}
		}
		if(!(map.get("manufacture_state_id") instanceof Boolean)&& map.get("manufacture_state_id")!=null){
			Object[] objs = (Object[])map.get("manufacture_state_id");
			if(objs.length > 0){
				this.setManufacture_state_id((Integer)objs[0]);
			}
		}
		if(!(map.get("manufacture_state_id") instanceof Boolean)&& map.get("manufacture_state_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("manufacture_state_id");
			if(objs.length > 1){
				this.setManufacture_state_id_text((String)objs[1]);
			}
		}
		if(!(map.get("message_attachment_count") instanceof Boolean)&& map.get("message_attachment_count")!=null){
			this.setMessage_attachment_count((Integer)map.get("message_attachment_count"));
		}
		if(!(map.get("message_channel_ids") instanceof Boolean)&& map.get("message_channel_ids")!=null){
			Object[] objs = (Object[])map.get("message_channel_ids");
			if(objs.length > 0){
				Integer[] message_channel_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_channel_ids(Arrays.toString(message_channel_ids));
			}
		}
		if(!(map.get("message_follower_ids") instanceof Boolean)&& map.get("message_follower_ids")!=null){
			Object[] objs = (Object[])map.get("message_follower_ids");
			if(objs.length > 0){
				Integer[] message_follower_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_follower_ids(Arrays.toString(message_follower_ids));
			}
		}
		if(map.get("message_has_error") instanceof Boolean){
			this.setMessage_has_error(((Boolean)map.get("message_has_error"))? "true" : "false");
		}
		if(!(map.get("message_has_error_counter") instanceof Boolean)&& map.get("message_has_error_counter")!=null){
			this.setMessage_has_error_counter((Integer)map.get("message_has_error_counter"));
		}
		if(!(map.get("message_ids") instanceof Boolean)&& map.get("message_ids")!=null){
			Object[] objs = (Object[])map.get("message_ids");
			if(objs.length > 0){
				Integer[] message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_ids(Arrays.toString(message_ids));
			}
		}
		if(map.get("message_is_follower") instanceof Boolean){
			this.setMessage_is_follower(((Boolean)map.get("message_is_follower"))? "true" : "false");
		}
		if(!(map.get("message_main_attachment_id") instanceof Boolean)&& map.get("message_main_attachment_id")!=null){
			Object[] objs = (Object[])map.get("message_main_attachment_id");
			if(objs.length > 0){
				this.setMessage_main_attachment_id((Integer)objs[0]);
			}
		}
		if(map.get("message_needaction") instanceof Boolean){
			this.setMessage_needaction(((Boolean)map.get("message_needaction"))? "true" : "false");
		}
		if(!(map.get("message_needaction_counter") instanceof Boolean)&& map.get("message_needaction_counter")!=null){
			this.setMessage_needaction_counter((Integer)map.get("message_needaction_counter"));
		}
		if(!(map.get("message_partner_ids") instanceof Boolean)&& map.get("message_partner_ids")!=null){
			Object[] objs = (Object[])map.get("message_partner_ids");
			if(objs.length > 0){
				Integer[] message_partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_partner_ids(Arrays.toString(message_partner_ids));
			}
		}
		if(map.get("message_unread") instanceof Boolean){
			this.setMessage_unread(((Boolean)map.get("message_unread"))? "true" : "false");
		}
		if(!(map.get("message_unread_counter") instanceof Boolean)&& map.get("message_unread_counter")!=null){
			this.setMessage_unread_counter((Integer)map.get("message_unread_counter"));
		}
		if(!(map.get("meter_ids") instanceof Boolean)&& map.get("meter_ids")!=null){
			Object[] objs = (Object[])map.get("meter_ids");
			if(objs.length > 0){
				Integer[] meter_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMeter_ids(Arrays.toString(meter_ids));
			}
		}
		if(!(map.get("model") instanceof Boolean)&& map.get("model")!=null){
			this.setModel((String)map.get("model"));
		}
		if(!(map.get("mro_count") instanceof Boolean)&& map.get("mro_count")!=null){
			this.setMro_count((Integer)map.get("mro_count"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("position") instanceof Boolean)&& map.get("position")!=null){
			this.setPosition((String)map.get("position"));
		}
		if(!(map.get("property_stock_asset") instanceof Boolean)&& map.get("property_stock_asset")!=null){
			Object[] objs = (Object[])map.get("property_stock_asset");
			if(objs.length > 0){
				this.setProperty_stock_asset((Integer)objs[0]);
			}
		}
		if(!(map.get("purchase_date") instanceof Boolean)&& map.get("purchase_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("purchase_date"));
   			this.setPurchase_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("serial") instanceof Boolean)&& map.get("serial")!=null){
			this.setSerial((String)map.get("serial"));
		}
		if(!(map.get("start_date") instanceof Boolean)&& map.get("start_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("start_date"));
   			this.setStart_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 0){
				this.setUser_id((Integer)objs[0]);
			}
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 1){
				this.setUser_id_text((String)objs[1]);
			}
		}
		if(!(map.get("vendor_id") instanceof Boolean)&& map.get("vendor_id")!=null){
			Object[] objs = (Object[])map.get("vendor_id");
			if(objs.length > 0){
				this.setVendor_id((Integer)objs[0]);
			}
		}
		if(!(map.get("vendor_id") instanceof Boolean)&& map.get("vendor_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("vendor_id");
			if(objs.length > 1){
				this.setVendor_id_text((String)objs[1]);
			}
		}
		if(!(map.get("warehouse_state_id") instanceof Boolean)&& map.get("warehouse_state_id")!=null){
			Object[] objs = (Object[])map.get("warehouse_state_id");
			if(objs.length > 0){
				this.setWarehouse_state_id((Integer)objs[0]);
			}
		}
		if(!(map.get("warehouse_state_id") instanceof Boolean)&& map.get("warehouse_state_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("warehouse_state_id");
			if(objs.length > 1){
				this.setWarehouse_state_id_text((String)objs[1]);
			}
		}
		if(!(map.get("warranty_end_date") instanceof Boolean)&& map.get("warranty_end_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("warranty_end_date"));
   			this.setWarranty_end_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("warranty_start_date") instanceof Boolean)&& map.get("warranty_start_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("warranty_start_date"));
   			this.setWarranty_start_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("website_message_ids") instanceof Boolean)&& map.get("website_message_ids")!=null){
			Object[] objs = (Object[])map.get("website_message_ids");
			if(objs.length > 0){
				Integer[] website_message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setWebsite_message_ids(Arrays.toString(website_message_ids));
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAccounting_state_id()!=null&&this.getAccounting_state_idDirtyFlag()){
			map.put("accounting_state_id",this.getAccounting_state_id());
		}else if(this.getAccounting_state_idDirtyFlag()){
			map.put("accounting_state_id",false);
		}
		if(this.getAccounting_state_id_text()!=null&&this.getAccounting_state_id_textDirtyFlag()){
			//忽略文本外键accounting_state_id_text
		}else if(this.getAccounting_state_id_textDirtyFlag()){
			map.put("accounting_state_id",false);
		}
		if(this.getActive()!=null&&this.getActiveDirtyFlag()){
			map.put("active",Boolean.parseBoolean(this.getActive()));		
		}		if(this.getAsset_number()!=null&&this.getAsset_numberDirtyFlag()){
			map.put("asset_number",this.getAsset_number());
		}else if(this.getAsset_numberDirtyFlag()){
			map.put("asset_number",false);
		}
		if(this.getCategory_ids()!=null&&this.getCategory_idsDirtyFlag()){
			map.put("category_ids",this.getCategory_ids());
		}else if(this.getCategory_idsDirtyFlag()){
			map.put("category_ids",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCriticality()!=null&&this.getCriticalityDirtyFlag()){
			map.put("criticality",this.getCriticality());
		}else if(this.getCriticalityDirtyFlag()){
			map.put("criticality",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getFinance_state_id()!=null&&this.getFinance_state_idDirtyFlag()){
			map.put("finance_state_id",this.getFinance_state_id());
		}else if(this.getFinance_state_idDirtyFlag()){
			map.put("finance_state_id",false);
		}
		if(this.getFinance_state_id_text()!=null&&this.getFinance_state_id_textDirtyFlag()){
			//忽略文本外键finance_state_id_text
		}else if(this.getFinance_state_id_textDirtyFlag()){
			map.put("finance_state_id",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getImage()!=null&&this.getImageDirtyFlag()){
			//暂不支持binary类型image
		}else if(this.getImageDirtyFlag()){
			map.put("image",false);
		}
		if(this.getImage_medium()!=null&&this.getImage_mediumDirtyFlag()){
			//暂不支持binary类型image_medium
		}else if(this.getImage_mediumDirtyFlag()){
			map.put("image_medium",false);
		}
		if(this.getImage_small()!=null&&this.getImage_smallDirtyFlag()){
			//暂不支持binary类型image_small
		}else if(this.getImage_smallDirtyFlag()){
			map.put("image_small",false);
		}
		if(this.getMaintenance_date()!=null&&this.getMaintenance_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getMaintenance_date());
			map.put("maintenance_date",datetimeStr);
		}else if(this.getMaintenance_dateDirtyFlag()){
			map.put("maintenance_date",false);
		}
		if(this.getMaintenance_state_color()!=null&&this.getMaintenance_state_colorDirtyFlag()){
			map.put("maintenance_state_color",this.getMaintenance_state_color());
		}else if(this.getMaintenance_state_colorDirtyFlag()){
			map.put("maintenance_state_color",false);
		}
		if(this.getMaintenance_state_id()!=null&&this.getMaintenance_state_idDirtyFlag()){
			map.put("maintenance_state_id",this.getMaintenance_state_id());
		}else if(this.getMaintenance_state_idDirtyFlag()){
			map.put("maintenance_state_id",false);
		}
		if(this.getMaintenance_state_id_text()!=null&&this.getMaintenance_state_id_textDirtyFlag()){
			//忽略文本外键maintenance_state_id_text
		}else if(this.getMaintenance_state_id_textDirtyFlag()){
			map.put("maintenance_state_id",false);
		}
		if(this.getManufacturer_id()!=null&&this.getManufacturer_idDirtyFlag()){
			map.put("manufacturer_id",this.getManufacturer_id());
		}else if(this.getManufacturer_idDirtyFlag()){
			map.put("manufacturer_id",false);
		}
		if(this.getManufacturer_id_text()!=null&&this.getManufacturer_id_textDirtyFlag()){
			//忽略文本外键manufacturer_id_text
		}else if(this.getManufacturer_id_textDirtyFlag()){
			map.put("manufacturer_id",false);
		}
		if(this.getManufacture_state_id()!=null&&this.getManufacture_state_idDirtyFlag()){
			map.put("manufacture_state_id",this.getManufacture_state_id());
		}else if(this.getManufacture_state_idDirtyFlag()){
			map.put("manufacture_state_id",false);
		}
		if(this.getManufacture_state_id_text()!=null&&this.getManufacture_state_id_textDirtyFlag()){
			//忽略文本外键manufacture_state_id_text
		}else if(this.getManufacture_state_id_textDirtyFlag()){
			map.put("manufacture_state_id",false);
		}
		if(this.getMessage_attachment_count()!=null&&this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",this.getMessage_attachment_count());
		}else if(this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",false);
		}
		if(this.getMessage_channel_ids()!=null&&this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",this.getMessage_channel_ids());
		}else if(this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",false);
		}
		if(this.getMessage_follower_ids()!=null&&this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",this.getMessage_follower_ids());
		}else if(this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",false);
		}
		if(this.getMessage_has_error()!=null&&this.getMessage_has_errorDirtyFlag()){
			map.put("message_has_error",Boolean.parseBoolean(this.getMessage_has_error()));		
		}		if(this.getMessage_has_error_counter()!=null&&this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",this.getMessage_has_error_counter());
		}else if(this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",false);
		}
		if(this.getMessage_ids()!=null&&this.getMessage_idsDirtyFlag()){
			map.put("message_ids",this.getMessage_ids());
		}else if(this.getMessage_idsDirtyFlag()){
			map.put("message_ids",false);
		}
		if(this.getMessage_is_follower()!=null&&this.getMessage_is_followerDirtyFlag()){
			map.put("message_is_follower",Boolean.parseBoolean(this.getMessage_is_follower()));		
		}		if(this.getMessage_main_attachment_id()!=null&&this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",this.getMessage_main_attachment_id());
		}else if(this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",false);
		}
		if(this.getMessage_needaction()!=null&&this.getMessage_needactionDirtyFlag()){
			map.put("message_needaction",Boolean.parseBoolean(this.getMessage_needaction()));		
		}		if(this.getMessage_needaction_counter()!=null&&this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",this.getMessage_needaction_counter());
		}else if(this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",false);
		}
		if(this.getMessage_partner_ids()!=null&&this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",this.getMessage_partner_ids());
		}else if(this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",false);
		}
		if(this.getMessage_unread()!=null&&this.getMessage_unreadDirtyFlag()){
			map.put("message_unread",Boolean.parseBoolean(this.getMessage_unread()));		
		}		if(this.getMessage_unread_counter()!=null&&this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",this.getMessage_unread_counter());
		}else if(this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",false);
		}
		if(this.getMeter_ids()!=null&&this.getMeter_idsDirtyFlag()){
			map.put("meter_ids",this.getMeter_ids());
		}else if(this.getMeter_idsDirtyFlag()){
			map.put("meter_ids",false);
		}
		if(this.getModel()!=null&&this.getModelDirtyFlag()){
			map.put("model",this.getModel());
		}else if(this.getModelDirtyFlag()){
			map.put("model",false);
		}
		if(this.getMro_count()!=null&&this.getMro_countDirtyFlag()){
			map.put("mro_count",this.getMro_count());
		}else if(this.getMro_countDirtyFlag()){
			map.put("mro_count",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getPosition()!=null&&this.getPositionDirtyFlag()){
			map.put("position",this.getPosition());
		}else if(this.getPositionDirtyFlag()){
			map.put("position",false);
		}
		if(this.getProperty_stock_asset()!=null&&this.getProperty_stock_assetDirtyFlag()){
			map.put("property_stock_asset",this.getProperty_stock_asset());
		}else if(this.getProperty_stock_assetDirtyFlag()){
			map.put("property_stock_asset",false);
		}
		if(this.getPurchase_date()!=null&&this.getPurchase_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getPurchase_date());
			map.put("purchase_date",datetimeStr);
		}else if(this.getPurchase_dateDirtyFlag()){
			map.put("purchase_date",false);
		}
		if(this.getSerial()!=null&&this.getSerialDirtyFlag()){
			map.put("serial",this.getSerial());
		}else if(this.getSerialDirtyFlag()){
			map.put("serial",false);
		}
		if(this.getStart_date()!=null&&this.getStart_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getStart_date());
			map.put("start_date",datetimeStr);
		}else if(this.getStart_dateDirtyFlag()){
			map.put("start_date",false);
		}
		if(this.getUser_id()!=null&&this.getUser_idDirtyFlag()){
			map.put("user_id",this.getUser_id());
		}else if(this.getUser_idDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getUser_id_text()!=null&&this.getUser_id_textDirtyFlag()){
			//忽略文本外键user_id_text
		}else if(this.getUser_id_textDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getVendor_id()!=null&&this.getVendor_idDirtyFlag()){
			map.put("vendor_id",this.getVendor_id());
		}else if(this.getVendor_idDirtyFlag()){
			map.put("vendor_id",false);
		}
		if(this.getVendor_id_text()!=null&&this.getVendor_id_textDirtyFlag()){
			//忽略文本外键vendor_id_text
		}else if(this.getVendor_id_textDirtyFlag()){
			map.put("vendor_id",false);
		}
		if(this.getWarehouse_state_id()!=null&&this.getWarehouse_state_idDirtyFlag()){
			map.put("warehouse_state_id",this.getWarehouse_state_id());
		}else if(this.getWarehouse_state_idDirtyFlag()){
			map.put("warehouse_state_id",false);
		}
		if(this.getWarehouse_state_id_text()!=null&&this.getWarehouse_state_id_textDirtyFlag()){
			//忽略文本外键warehouse_state_id_text
		}else if(this.getWarehouse_state_id_textDirtyFlag()){
			map.put("warehouse_state_id",false);
		}
		if(this.getWarranty_end_date()!=null&&this.getWarranty_end_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getWarranty_end_date());
			map.put("warranty_end_date",datetimeStr);
		}else if(this.getWarranty_end_dateDirtyFlag()){
			map.put("warranty_end_date",false);
		}
		if(this.getWarranty_start_date()!=null&&this.getWarranty_start_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getWarranty_start_date());
			map.put("warranty_start_date",datetimeStr);
		}else if(this.getWarranty_start_dateDirtyFlag()){
			map.put("warranty_start_date",false);
		}
		if(this.getWebsite_message_ids()!=null&&this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",this.getWebsite_message_ids());
		}else if(this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
