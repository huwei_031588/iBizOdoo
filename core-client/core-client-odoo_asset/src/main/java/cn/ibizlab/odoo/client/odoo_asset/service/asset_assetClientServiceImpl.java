package cn.ibizlab.odoo.client.odoo_asset.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iasset_asset;
import cn.ibizlab.odoo.core.client.service.Iasset_assetClientService;
import cn.ibizlab.odoo.client.odoo_asset.model.asset_assetImpl;
import cn.ibizlab.odoo.client.odoo_asset.odooclient.Iasset_assetOdooClient;
import cn.ibizlab.odoo.client.odoo_asset.odooclient.impl.asset_assetOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[asset_asset] 服务对象接口
 */
@Service
public class asset_assetClientServiceImpl implements Iasset_assetClientService {
    @Autowired
    private  Iasset_assetOdooClient  asset_assetOdooClient;

    public Iasset_asset createModel() {		
		return new asset_assetImpl();
	}


        public void get(Iasset_asset asset_asset){
            this.asset_assetOdooClient.get(asset_asset) ;
        }
        
        public Page<Iasset_asset> search(SearchContext context){
            return this.asset_assetOdooClient.search(context) ;
        }
        
        public void create(Iasset_asset asset_asset){
this.asset_assetOdooClient.create(asset_asset) ;
        }
        
        public void update(Iasset_asset asset_asset){
this.asset_assetOdooClient.update(asset_asset) ;
        }
        
        public void createBatch(List<Iasset_asset> asset_assets){
            
        }
        
        public void updateBatch(List<Iasset_asset> asset_assets){
            
        }
        
        public void remove(Iasset_asset asset_asset){
this.asset_assetOdooClient.remove(asset_asset) ;
        }
        
        public void removeBatch(List<Iasset_asset> asset_assets){
            
        }
        
        public Page<Iasset_asset> select(SearchContext context){
            return null ;
        }
        

}

