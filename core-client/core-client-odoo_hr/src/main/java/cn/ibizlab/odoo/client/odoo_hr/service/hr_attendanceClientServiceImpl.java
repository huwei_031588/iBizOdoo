package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_attendance;
import cn.ibizlab.odoo.core.client.service.Ihr_attendanceClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_attendanceImpl;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.Ihr_attendanceOdooClient;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.impl.hr_attendanceOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[hr_attendance] 服务对象接口
 */
@Service
public class hr_attendanceClientServiceImpl implements Ihr_attendanceClientService {
    @Autowired
    private  Ihr_attendanceOdooClient  hr_attendanceOdooClient;

    public Ihr_attendance createModel() {		
		return new hr_attendanceImpl();
	}


        public Page<Ihr_attendance> search(SearchContext context){
            return this.hr_attendanceOdooClient.search(context) ;
        }
        
        public void update(Ihr_attendance hr_attendance){
this.hr_attendanceOdooClient.update(hr_attendance) ;
        }
        
        public void createBatch(List<Ihr_attendance> hr_attendances){
            
        }
        
        public void create(Ihr_attendance hr_attendance){
this.hr_attendanceOdooClient.create(hr_attendance) ;
        }
        
        public void remove(Ihr_attendance hr_attendance){
this.hr_attendanceOdooClient.remove(hr_attendance) ;
        }
        
        public void updateBatch(List<Ihr_attendance> hr_attendances){
            
        }
        
        public void removeBatch(List<Ihr_attendance> hr_attendances){
            
        }
        
        public void get(Ihr_attendance hr_attendance){
            this.hr_attendanceOdooClient.get(hr_attendance) ;
        }
        
        public Page<Ihr_attendance> select(SearchContext context){
            return null ;
        }
        

}

