package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_employee;
import cn.ibizlab.odoo.core.client.service.Ihr_employeeClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_employeeImpl;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.Ihr_employeeOdooClient;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.impl.hr_employeeOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[hr_employee] 服务对象接口
 */
@Service
public class hr_employeeClientServiceImpl implements Ihr_employeeClientService {
    @Autowired
    private  Ihr_employeeOdooClient  hr_employeeOdooClient;

    public Ihr_employee createModel() {		
		return new hr_employeeImpl();
	}


        public void createBatch(List<Ihr_employee> hr_employees){
            
        }
        
        public void update(Ihr_employee hr_employee){
this.hr_employeeOdooClient.update(hr_employee) ;
        }
        
        public void updateBatch(List<Ihr_employee> hr_employees){
            
        }
        
        public Page<Ihr_employee> search(SearchContext context){
            return this.hr_employeeOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Ihr_employee> hr_employees){
            
        }
        
        public void remove(Ihr_employee hr_employee){
this.hr_employeeOdooClient.remove(hr_employee) ;
        }
        
        public void create(Ihr_employee hr_employee){
this.hr_employeeOdooClient.create(hr_employee) ;
        }
        
        public void get(Ihr_employee hr_employee){
            this.hr_employeeOdooClient.get(hr_employee) ;
        }
        
        public Page<Ihr_employee> select(SearchContext context){
            return null ;
        }
        

}

