package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_recruitment_stage;
import cn.ibizlab.odoo.core.client.service.Ihr_recruitment_stageClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_recruitment_stageImpl;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.Ihr_recruitment_stageOdooClient;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.impl.hr_recruitment_stageOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[hr_recruitment_stage] 服务对象接口
 */
@Service
public class hr_recruitment_stageClientServiceImpl implements Ihr_recruitment_stageClientService {
    @Autowired
    private  Ihr_recruitment_stageOdooClient  hr_recruitment_stageOdooClient;

    public Ihr_recruitment_stage createModel() {		
		return new hr_recruitment_stageImpl();
	}


        public void updateBatch(List<Ihr_recruitment_stage> hr_recruitment_stages){
            
        }
        
        public Page<Ihr_recruitment_stage> search(SearchContext context){
            return this.hr_recruitment_stageOdooClient.search(context) ;
        }
        
        public void create(Ihr_recruitment_stage hr_recruitment_stage){
this.hr_recruitment_stageOdooClient.create(hr_recruitment_stage) ;
        }
        
        public void remove(Ihr_recruitment_stage hr_recruitment_stage){
this.hr_recruitment_stageOdooClient.remove(hr_recruitment_stage) ;
        }
        
        public void get(Ihr_recruitment_stage hr_recruitment_stage){
            this.hr_recruitment_stageOdooClient.get(hr_recruitment_stage) ;
        }
        
        public void createBatch(List<Ihr_recruitment_stage> hr_recruitment_stages){
            
        }
        
        public void update(Ihr_recruitment_stage hr_recruitment_stage){
this.hr_recruitment_stageOdooClient.update(hr_recruitment_stage) ;
        }
        
        public void removeBatch(List<Ihr_recruitment_stage> hr_recruitment_stages){
            
        }
        
        public Page<Ihr_recruitment_stage> select(SearchContext context){
            return null ;
        }
        

}

