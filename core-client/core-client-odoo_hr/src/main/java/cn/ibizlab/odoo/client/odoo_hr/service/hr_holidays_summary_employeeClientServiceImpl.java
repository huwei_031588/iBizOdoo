package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_holidays_summary_employee;
import cn.ibizlab.odoo.core.client.service.Ihr_holidays_summary_employeeClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_holidays_summary_employeeImpl;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.Ihr_holidays_summary_employeeOdooClient;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.impl.hr_holidays_summary_employeeOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[hr_holidays_summary_employee] 服务对象接口
 */
@Service
public class hr_holidays_summary_employeeClientServiceImpl implements Ihr_holidays_summary_employeeClientService {
    @Autowired
    private  Ihr_holidays_summary_employeeOdooClient  hr_holidays_summary_employeeOdooClient;

    public Ihr_holidays_summary_employee createModel() {		
		return new hr_holidays_summary_employeeImpl();
	}


        public void remove(Ihr_holidays_summary_employee hr_holidays_summary_employee){
this.hr_holidays_summary_employeeOdooClient.remove(hr_holidays_summary_employee) ;
        }
        
        public void updateBatch(List<Ihr_holidays_summary_employee> hr_holidays_summary_employees){
            
        }
        
        public void removeBatch(List<Ihr_holidays_summary_employee> hr_holidays_summary_employees){
            
        }
        
        public void createBatch(List<Ihr_holidays_summary_employee> hr_holidays_summary_employees){
            
        }
        
        public void update(Ihr_holidays_summary_employee hr_holidays_summary_employee){
this.hr_holidays_summary_employeeOdooClient.update(hr_holidays_summary_employee) ;
        }
        
        public void get(Ihr_holidays_summary_employee hr_holidays_summary_employee){
            this.hr_holidays_summary_employeeOdooClient.get(hr_holidays_summary_employee) ;
        }
        
        public Page<Ihr_holidays_summary_employee> search(SearchContext context){
            return this.hr_holidays_summary_employeeOdooClient.search(context) ;
        }
        
        public void create(Ihr_holidays_summary_employee hr_holidays_summary_employee){
this.hr_holidays_summary_employeeOdooClient.create(hr_holidays_summary_employee) ;
        }
        
        public Page<Ihr_holidays_summary_employee> select(SearchContext context){
            return null ;
        }
        

}

