package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_expense;
import cn.ibizlab.odoo.core.client.service.Ihr_expenseClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_expenseImpl;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.Ihr_expenseOdooClient;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.impl.hr_expenseOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[hr_expense] 服务对象接口
 */
@Service
public class hr_expenseClientServiceImpl implements Ihr_expenseClientService {
    @Autowired
    private  Ihr_expenseOdooClient  hr_expenseOdooClient;

    public Ihr_expense createModel() {		
		return new hr_expenseImpl();
	}


        public void removeBatch(List<Ihr_expense> hr_expenses){
            
        }
        
        public void create(Ihr_expense hr_expense){
this.hr_expenseOdooClient.create(hr_expense) ;
        }
        
        public void get(Ihr_expense hr_expense){
            this.hr_expenseOdooClient.get(hr_expense) ;
        }
        
        public void createBatch(List<Ihr_expense> hr_expenses){
            
        }
        
        public void updateBatch(List<Ihr_expense> hr_expenses){
            
        }
        
        public Page<Ihr_expense> search(SearchContext context){
            return this.hr_expenseOdooClient.search(context) ;
        }
        
        public void remove(Ihr_expense hr_expense){
this.hr_expenseOdooClient.remove(hr_expense) ;
        }
        
        public void update(Ihr_expense hr_expense){
this.hr_expenseOdooClient.update(hr_expense) ;
        }
        
        public Page<Ihr_expense> select(SearchContext context){
            return null ;
        }
        

}

