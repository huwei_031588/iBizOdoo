package cn.ibizlab.odoo.client.odoo_hr.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ihr_recruitment_source;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_recruitment_source] 服务对象客户端接口
 */
public interface Ihr_recruitment_sourceOdooClient {
    
        public void createBatch(Ihr_recruitment_source hr_recruitment_source);

        public void get(Ihr_recruitment_source hr_recruitment_source);

        public Page<Ihr_recruitment_source> search(SearchContext context);

        public void update(Ihr_recruitment_source hr_recruitment_source);

        public void remove(Ihr_recruitment_source hr_recruitment_source);

        public void updateBatch(Ihr_recruitment_source hr_recruitment_source);

        public void removeBatch(Ihr_recruitment_source hr_recruitment_source);

        public void create(Ihr_recruitment_source hr_recruitment_source);

        public List<Ihr_recruitment_source> select();


}