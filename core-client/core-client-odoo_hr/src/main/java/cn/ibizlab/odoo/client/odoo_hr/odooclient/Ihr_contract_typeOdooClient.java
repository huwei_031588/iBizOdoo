package cn.ibizlab.odoo.client.odoo_hr.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ihr_contract_type;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_contract_type] 服务对象客户端接口
 */
public interface Ihr_contract_typeOdooClient {
    
        public void removeBatch(Ihr_contract_type hr_contract_type);

        public void remove(Ihr_contract_type hr_contract_type);

        public void createBatch(Ihr_contract_type hr_contract_type);

        public void updateBatch(Ihr_contract_type hr_contract_type);

        public void update(Ihr_contract_type hr_contract_type);

        public void create(Ihr_contract_type hr_contract_type);

        public void get(Ihr_contract_type hr_contract_type);

        public Page<Ihr_contract_type> search(SearchContext context);

        public List<Ihr_contract_type> select();


}