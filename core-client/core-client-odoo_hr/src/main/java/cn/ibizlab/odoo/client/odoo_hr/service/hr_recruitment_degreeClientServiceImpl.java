package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_recruitment_degree;
import cn.ibizlab.odoo.core.client.service.Ihr_recruitment_degreeClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_recruitment_degreeImpl;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.Ihr_recruitment_degreeOdooClient;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.impl.hr_recruitment_degreeOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[hr_recruitment_degree] 服务对象接口
 */
@Service
public class hr_recruitment_degreeClientServiceImpl implements Ihr_recruitment_degreeClientService {
    @Autowired
    private  Ihr_recruitment_degreeOdooClient  hr_recruitment_degreeOdooClient;

    public Ihr_recruitment_degree createModel() {		
		return new hr_recruitment_degreeImpl();
	}


        public Page<Ihr_recruitment_degree> search(SearchContext context){
            return this.hr_recruitment_degreeOdooClient.search(context) ;
        }
        
        public void create(Ihr_recruitment_degree hr_recruitment_degree){
this.hr_recruitment_degreeOdooClient.create(hr_recruitment_degree) ;
        }
        
        public void updateBatch(List<Ihr_recruitment_degree> hr_recruitment_degrees){
            
        }
        
        public void removeBatch(List<Ihr_recruitment_degree> hr_recruitment_degrees){
            
        }
        
        public void remove(Ihr_recruitment_degree hr_recruitment_degree){
this.hr_recruitment_degreeOdooClient.remove(hr_recruitment_degree) ;
        }
        
        public void update(Ihr_recruitment_degree hr_recruitment_degree){
this.hr_recruitment_degreeOdooClient.update(hr_recruitment_degree) ;
        }
        
        public void get(Ihr_recruitment_degree hr_recruitment_degree){
            this.hr_recruitment_degreeOdooClient.get(hr_recruitment_degree) ;
        }
        
        public void createBatch(List<Ihr_recruitment_degree> hr_recruitment_degrees){
            
        }
        
        public Page<Ihr_recruitment_degree> select(SearchContext context){
            return null ;
        }
        

}

