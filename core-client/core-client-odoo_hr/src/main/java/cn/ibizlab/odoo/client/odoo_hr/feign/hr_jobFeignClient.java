package cn.ibizlab.odoo.client.odoo_hr.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ihr_job;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_jobImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[hr_job] 服务对象接口
 */
public interface hr_jobFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_jobs/{id}")
    public hr_jobImpl update(@PathVariable("id") Integer id,@RequestBody hr_jobImpl hr_job);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_jobs/createbatch")
    public hr_jobImpl createBatch(@RequestBody List<hr_jobImpl> hr_jobs);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_jobs/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_jobs/removebatch")
    public hr_jobImpl removeBatch(@RequestBody List<hr_jobImpl> hr_jobs);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_jobs")
    public hr_jobImpl create(@RequestBody hr_jobImpl hr_job);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_jobs/search")
    public Page<hr_jobImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_jobs/updatebatch")
    public hr_jobImpl updateBatch(@RequestBody List<hr_jobImpl> hr_jobs);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_jobs/{id}")
    public hr_jobImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_jobs/select")
    public Page<hr_jobImpl> select();



}
