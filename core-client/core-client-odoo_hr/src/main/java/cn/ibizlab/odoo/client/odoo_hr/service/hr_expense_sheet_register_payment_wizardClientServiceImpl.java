package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_expense_sheet_register_payment_wizard;
import cn.ibizlab.odoo.core.client.service.Ihr_expense_sheet_register_payment_wizardClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_expense_sheet_register_payment_wizardImpl;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.Ihr_expense_sheet_register_payment_wizardOdooClient;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.impl.hr_expense_sheet_register_payment_wizardOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[hr_expense_sheet_register_payment_wizard] 服务对象接口
 */
@Service
public class hr_expense_sheet_register_payment_wizardClientServiceImpl implements Ihr_expense_sheet_register_payment_wizardClientService {
    @Autowired
    private  Ihr_expense_sheet_register_payment_wizardOdooClient  hr_expense_sheet_register_payment_wizardOdooClient;

    public Ihr_expense_sheet_register_payment_wizard createModel() {		
		return new hr_expense_sheet_register_payment_wizardImpl();
	}


        public void updateBatch(List<Ihr_expense_sheet_register_payment_wizard> hr_expense_sheet_register_payment_wizards){
            
        }
        
        public void update(Ihr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard){
this.hr_expense_sheet_register_payment_wizardOdooClient.update(hr_expense_sheet_register_payment_wizard) ;
        }
        
        public void removeBatch(List<Ihr_expense_sheet_register_payment_wizard> hr_expense_sheet_register_payment_wizards){
            
        }
        
        public void create(Ihr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard){
this.hr_expense_sheet_register_payment_wizardOdooClient.create(hr_expense_sheet_register_payment_wizard) ;
        }
        
        public Page<Ihr_expense_sheet_register_payment_wizard> search(SearchContext context){
            return this.hr_expense_sheet_register_payment_wizardOdooClient.search(context) ;
        }
        
        public void remove(Ihr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard){
this.hr_expense_sheet_register_payment_wizardOdooClient.remove(hr_expense_sheet_register_payment_wizard) ;
        }
        
        public void createBatch(List<Ihr_expense_sheet_register_payment_wizard> hr_expense_sheet_register_payment_wizards){
            
        }
        
        public void get(Ihr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard){
            this.hr_expense_sheet_register_payment_wizardOdooClient.get(hr_expense_sheet_register_payment_wizard) ;
        }
        
        public Page<Ihr_expense_sheet_register_payment_wizard> select(SearchContext context){
            return null ;
        }
        

}

