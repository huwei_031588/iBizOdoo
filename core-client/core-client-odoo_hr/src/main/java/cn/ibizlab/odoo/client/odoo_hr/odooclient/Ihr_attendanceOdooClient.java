package cn.ibizlab.odoo.client.odoo_hr.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ihr_attendance;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_attendance] 服务对象客户端接口
 */
public interface Ihr_attendanceOdooClient {
    
        public Page<Ihr_attendance> search(SearchContext context);

        public void update(Ihr_attendance hr_attendance);

        public void createBatch(Ihr_attendance hr_attendance);

        public void create(Ihr_attendance hr_attendance);

        public void remove(Ihr_attendance hr_attendance);

        public void updateBatch(Ihr_attendance hr_attendance);

        public void removeBatch(Ihr_attendance hr_attendance);

        public void get(Ihr_attendance hr_attendance);

        public List<Ihr_attendance> select();


}