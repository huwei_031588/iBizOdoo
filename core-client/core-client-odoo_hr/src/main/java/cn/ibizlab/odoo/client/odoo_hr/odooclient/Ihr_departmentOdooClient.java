package cn.ibizlab.odoo.client.odoo_hr.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ihr_department;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_department] 服务对象客户端接口
 */
public interface Ihr_departmentOdooClient {
    
        public void remove(Ihr_department hr_department);

        public Page<Ihr_department> search(SearchContext context);

        public void get(Ihr_department hr_department);

        public void update(Ihr_department hr_department);

        public void removeBatch(Ihr_department hr_department);

        public void createBatch(Ihr_department hr_department);

        public void create(Ihr_department hr_department);

        public void updateBatch(Ihr_department hr_department);

        public List<Ihr_department> select();


}