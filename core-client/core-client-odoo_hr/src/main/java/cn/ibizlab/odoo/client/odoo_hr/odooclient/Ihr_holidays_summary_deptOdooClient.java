package cn.ibizlab.odoo.client.odoo_hr.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ihr_holidays_summary_dept;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_holidays_summary_dept] 服务对象客户端接口
 */
public interface Ihr_holidays_summary_deptOdooClient {
    
        public void remove(Ihr_holidays_summary_dept hr_holidays_summary_dept);

        public void get(Ihr_holidays_summary_dept hr_holidays_summary_dept);

        public Page<Ihr_holidays_summary_dept> search(SearchContext context);

        public void createBatch(Ihr_holidays_summary_dept hr_holidays_summary_dept);

        public void create(Ihr_holidays_summary_dept hr_holidays_summary_dept);

        public void removeBatch(Ihr_holidays_summary_dept hr_holidays_summary_dept);

        public void update(Ihr_holidays_summary_dept hr_holidays_summary_dept);

        public void updateBatch(Ihr_holidays_summary_dept hr_holidays_summary_dept);

        public List<Ihr_holidays_summary_dept> select();


}