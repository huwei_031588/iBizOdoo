package cn.ibizlab.odoo.client.odoo_hr.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ihr_leave_report;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[hr_leave_report] 对象
 */
public class hr_leave_reportImpl implements Ihr_leave_report,Serializable{

    /**
     * 员工标签
     */
    public Integer category_id;

    @JsonIgnore
    public boolean category_idDirtyFlag;
    
    /**
     * 员工标签
     */
    public String category_id_text;

    @JsonIgnore
    public boolean category_id_textDirtyFlag;
    
    /**
     * 开始日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_from;

    @JsonIgnore
    public boolean date_fromDirtyFlag;
    
    /**
     * 结束日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_to;

    @JsonIgnore
    public boolean date_toDirtyFlag;
    
    /**
     * 部门
     */
    public Integer department_id;

    @JsonIgnore
    public boolean department_idDirtyFlag;
    
    /**
     * 部门
     */
    public String department_id_text;

    @JsonIgnore
    public boolean department_id_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 员工
     */
    public Integer employee_id;

    @JsonIgnore
    public boolean employee_idDirtyFlag;
    
    /**
     * 员工
     */
    public String employee_id_text;

    @JsonIgnore
    public boolean employee_id_textDirtyFlag;
    
    /**
     * 休假类型
     */
    public Integer holiday_status_id;

    @JsonIgnore
    public boolean holiday_status_idDirtyFlag;
    
    /**
     * 休假类型
     */
    public String holiday_status_id_text;

    @JsonIgnore
    public boolean holiday_status_id_textDirtyFlag;
    
    /**
     * 分配模式
     */
    public String holiday_type;

    @JsonIgnore
    public boolean holiday_typeDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 说明
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 天数
     */
    public Double number_of_days;

    @JsonIgnore
    public boolean number_of_daysDirtyFlag;
    
    /**
     * 反映在最近的工资单中
     */
    public String payslip_status;

    @JsonIgnore
    public boolean payslip_statusDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 申请类型
     */
    public String type;

    @JsonIgnore
    public boolean typeDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [员工标签]
     */
    @JsonProperty("category_id")
    public Integer getCategory_id(){
        return this.category_id ;
    }

    /**
     * 设置 [员工标签]
     */
    @JsonProperty("category_id")
    public void setCategory_id(Integer  category_id){
        this.category_id = category_id ;
        this.category_idDirtyFlag = true ;
    }

     /**
     * 获取 [员工标签]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idDirtyFlag(){
        return this.category_idDirtyFlag ;
    }   

    /**
     * 获取 [员工标签]
     */
    @JsonProperty("category_id_text")
    public String getCategory_id_text(){
        return this.category_id_text ;
    }

    /**
     * 设置 [员工标签]
     */
    @JsonProperty("category_id_text")
    public void setCategory_id_text(String  category_id_text){
        this.category_id_text = category_id_text ;
        this.category_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [员工标签]脏标记
     */
    @JsonIgnore
    public boolean getCategory_id_textDirtyFlag(){
        return this.category_id_textDirtyFlag ;
    }   

    /**
     * 获取 [开始日期]
     */
    @JsonProperty("date_from")
    public Timestamp getDate_from(){
        return this.date_from ;
    }

    /**
     * 设置 [开始日期]
     */
    @JsonProperty("date_from")
    public void setDate_from(Timestamp  date_from){
        this.date_from = date_from ;
        this.date_fromDirtyFlag = true ;
    }

     /**
     * 获取 [开始日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_fromDirtyFlag(){
        return this.date_fromDirtyFlag ;
    }   

    /**
     * 获取 [结束日期]
     */
    @JsonProperty("date_to")
    public Timestamp getDate_to(){
        return this.date_to ;
    }

    /**
     * 设置 [结束日期]
     */
    @JsonProperty("date_to")
    public void setDate_to(Timestamp  date_to){
        this.date_to = date_to ;
        this.date_toDirtyFlag = true ;
    }

     /**
     * 获取 [结束日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_toDirtyFlag(){
        return this.date_toDirtyFlag ;
    }   

    /**
     * 获取 [部门]
     */
    @JsonProperty("department_id")
    public Integer getDepartment_id(){
        return this.department_id ;
    }

    /**
     * 设置 [部门]
     */
    @JsonProperty("department_id")
    public void setDepartment_id(Integer  department_id){
        this.department_id = department_id ;
        this.department_idDirtyFlag = true ;
    }

     /**
     * 获取 [部门]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_idDirtyFlag(){
        return this.department_idDirtyFlag ;
    }   

    /**
     * 获取 [部门]
     */
    @JsonProperty("department_id_text")
    public String getDepartment_id_text(){
        return this.department_id_text ;
    }

    /**
     * 设置 [部门]
     */
    @JsonProperty("department_id_text")
    public void setDepartment_id_text(String  department_id_text){
        this.department_id_text = department_id_text ;
        this.department_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [部门]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_id_textDirtyFlag(){
        return this.department_id_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [员工]
     */
    @JsonProperty("employee_id")
    public Integer getEmployee_id(){
        return this.employee_id ;
    }

    /**
     * 设置 [员工]
     */
    @JsonProperty("employee_id")
    public void setEmployee_id(Integer  employee_id){
        this.employee_id = employee_id ;
        this.employee_idDirtyFlag = true ;
    }

     /**
     * 获取 [员工]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_idDirtyFlag(){
        return this.employee_idDirtyFlag ;
    }   

    /**
     * 获取 [员工]
     */
    @JsonProperty("employee_id_text")
    public String getEmployee_id_text(){
        return this.employee_id_text ;
    }

    /**
     * 设置 [员工]
     */
    @JsonProperty("employee_id_text")
    public void setEmployee_id_text(String  employee_id_text){
        this.employee_id_text = employee_id_text ;
        this.employee_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [员工]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_id_textDirtyFlag(){
        return this.employee_id_textDirtyFlag ;
    }   

    /**
     * 获取 [休假类型]
     */
    @JsonProperty("holiday_status_id")
    public Integer getHoliday_status_id(){
        return this.holiday_status_id ;
    }

    /**
     * 设置 [休假类型]
     */
    @JsonProperty("holiday_status_id")
    public void setHoliday_status_id(Integer  holiday_status_id){
        this.holiday_status_id = holiday_status_id ;
        this.holiday_status_idDirtyFlag = true ;
    }

     /**
     * 获取 [休假类型]脏标记
     */
    @JsonIgnore
    public boolean getHoliday_status_idDirtyFlag(){
        return this.holiday_status_idDirtyFlag ;
    }   

    /**
     * 获取 [休假类型]
     */
    @JsonProperty("holiday_status_id_text")
    public String getHoliday_status_id_text(){
        return this.holiday_status_id_text ;
    }

    /**
     * 设置 [休假类型]
     */
    @JsonProperty("holiday_status_id_text")
    public void setHoliday_status_id_text(String  holiday_status_id_text){
        this.holiday_status_id_text = holiday_status_id_text ;
        this.holiday_status_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [休假类型]脏标记
     */
    @JsonIgnore
    public boolean getHoliday_status_id_textDirtyFlag(){
        return this.holiday_status_id_textDirtyFlag ;
    }   

    /**
     * 获取 [分配模式]
     */
    @JsonProperty("holiday_type")
    public String getHoliday_type(){
        return this.holiday_type ;
    }

    /**
     * 设置 [分配模式]
     */
    @JsonProperty("holiday_type")
    public void setHoliday_type(String  holiday_type){
        this.holiday_type = holiday_type ;
        this.holiday_typeDirtyFlag = true ;
    }

     /**
     * 获取 [分配模式]脏标记
     */
    @JsonIgnore
    public boolean getHoliday_typeDirtyFlag(){
        return this.holiday_typeDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [说明]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [天数]
     */
    @JsonProperty("number_of_days")
    public Double getNumber_of_days(){
        return this.number_of_days ;
    }

    /**
     * 设置 [天数]
     */
    @JsonProperty("number_of_days")
    public void setNumber_of_days(Double  number_of_days){
        this.number_of_days = number_of_days ;
        this.number_of_daysDirtyFlag = true ;
    }

     /**
     * 获取 [天数]脏标记
     */
    @JsonIgnore
    public boolean getNumber_of_daysDirtyFlag(){
        return this.number_of_daysDirtyFlag ;
    }   

    /**
     * 获取 [反映在最近的工资单中]
     */
    @JsonProperty("payslip_status")
    public String getPayslip_status(){
        return this.payslip_status ;
    }

    /**
     * 设置 [反映在最近的工资单中]
     */
    @JsonProperty("payslip_status")
    public void setPayslip_status(String  payslip_status){
        this.payslip_status = payslip_status ;
        this.payslip_statusDirtyFlag = true ;
    }

     /**
     * 获取 [反映在最近的工资单中]脏标记
     */
    @JsonIgnore
    public boolean getPayslip_statusDirtyFlag(){
        return this.payslip_statusDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [申请类型]
     */
    @JsonProperty("type")
    public String getType(){
        return this.type ;
    }

    /**
     * 设置 [申请类型]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

     /**
     * 获取 [申请类型]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return this.typeDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("category_id") instanceof Boolean)&& map.get("category_id")!=null){
			Object[] objs = (Object[])map.get("category_id");
			if(objs.length > 0){
				this.setCategory_id((Integer)objs[0]);
			}
		}
		if(!(map.get("category_id") instanceof Boolean)&& map.get("category_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("category_id");
			if(objs.length > 1){
				this.setCategory_id_text((String)objs[1]);
			}
		}
		if(!(map.get("date_from") instanceof Boolean)&& map.get("date_from")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date_from"));
   			this.setDate_from(new Timestamp(parse.getTime()));
		}
		if(!(map.get("date_to") instanceof Boolean)&& map.get("date_to")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date_to"));
   			this.setDate_to(new Timestamp(parse.getTime()));
		}
		if(!(map.get("department_id") instanceof Boolean)&& map.get("department_id")!=null){
			Object[] objs = (Object[])map.get("department_id");
			if(objs.length > 0){
				this.setDepartment_id((Integer)objs[0]);
			}
		}
		if(!(map.get("department_id") instanceof Boolean)&& map.get("department_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("department_id");
			if(objs.length > 1){
				this.setDepartment_id_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("employee_id") instanceof Boolean)&& map.get("employee_id")!=null){
			Object[] objs = (Object[])map.get("employee_id");
			if(objs.length > 0){
				this.setEmployee_id((Integer)objs[0]);
			}
		}
		if(!(map.get("employee_id") instanceof Boolean)&& map.get("employee_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("employee_id");
			if(objs.length > 1){
				this.setEmployee_id_text((String)objs[1]);
			}
		}
		if(!(map.get("holiday_status_id") instanceof Boolean)&& map.get("holiday_status_id")!=null){
			Object[] objs = (Object[])map.get("holiday_status_id");
			if(objs.length > 0){
				this.setHoliday_status_id((Integer)objs[0]);
			}
		}
		if(!(map.get("holiday_status_id") instanceof Boolean)&& map.get("holiday_status_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("holiday_status_id");
			if(objs.length > 1){
				this.setHoliday_status_id_text((String)objs[1]);
			}
		}
		if(!(map.get("holiday_type") instanceof Boolean)&& map.get("holiday_type")!=null){
			this.setHoliday_type((String)map.get("holiday_type"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("number_of_days") instanceof Boolean)&& map.get("number_of_days")!=null){
			this.setNumber_of_days((Double)map.get("number_of_days"));
		}
		if(map.get("payslip_status") instanceof Boolean){
			this.setPayslip_status(((Boolean)map.get("payslip_status"))? "true" : "false");
		}
		if(!(map.get("state") instanceof Boolean)&& map.get("state")!=null){
			this.setState((String)map.get("state"));
		}
		if(!(map.get("type") instanceof Boolean)&& map.get("type")!=null){
			this.setType((String)map.get("type"));
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getCategory_id()!=null&&this.getCategory_idDirtyFlag()){
			map.put("category_id",this.getCategory_id());
		}else if(this.getCategory_idDirtyFlag()){
			map.put("category_id",false);
		}
		if(this.getCategory_id_text()!=null&&this.getCategory_id_textDirtyFlag()){
			//忽略文本外键category_id_text
		}else if(this.getCategory_id_textDirtyFlag()){
			map.put("category_id",false);
		}
		if(this.getDate_from()!=null&&this.getDate_fromDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate_from());
			map.put("date_from",datetimeStr);
		}else if(this.getDate_fromDirtyFlag()){
			map.put("date_from",false);
		}
		if(this.getDate_to()!=null&&this.getDate_toDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate_to());
			map.put("date_to",datetimeStr);
		}else if(this.getDate_toDirtyFlag()){
			map.put("date_to",false);
		}
		if(this.getDepartment_id()!=null&&this.getDepartment_idDirtyFlag()){
			map.put("department_id",this.getDepartment_id());
		}else if(this.getDepartment_idDirtyFlag()){
			map.put("department_id",false);
		}
		if(this.getDepartment_id_text()!=null&&this.getDepartment_id_textDirtyFlag()){
			//忽略文本外键department_id_text
		}else if(this.getDepartment_id_textDirtyFlag()){
			map.put("department_id",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getEmployee_id()!=null&&this.getEmployee_idDirtyFlag()){
			map.put("employee_id",this.getEmployee_id());
		}else if(this.getEmployee_idDirtyFlag()){
			map.put("employee_id",false);
		}
		if(this.getEmployee_id_text()!=null&&this.getEmployee_id_textDirtyFlag()){
			//忽略文本外键employee_id_text
		}else if(this.getEmployee_id_textDirtyFlag()){
			map.put("employee_id",false);
		}
		if(this.getHoliday_status_id()!=null&&this.getHoliday_status_idDirtyFlag()){
			map.put("holiday_status_id",this.getHoliday_status_id());
		}else if(this.getHoliday_status_idDirtyFlag()){
			map.put("holiday_status_id",false);
		}
		if(this.getHoliday_status_id_text()!=null&&this.getHoliday_status_id_textDirtyFlag()){
			//忽略文本外键holiday_status_id_text
		}else if(this.getHoliday_status_id_textDirtyFlag()){
			map.put("holiday_status_id",false);
		}
		if(this.getHoliday_type()!=null&&this.getHoliday_typeDirtyFlag()){
			map.put("holiday_type",this.getHoliday_type());
		}else if(this.getHoliday_typeDirtyFlag()){
			map.put("holiday_type",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getNumber_of_days()!=null&&this.getNumber_of_daysDirtyFlag()){
			map.put("number_of_days",this.getNumber_of_days());
		}else if(this.getNumber_of_daysDirtyFlag()){
			map.put("number_of_days",false);
		}
		if(this.getPayslip_status()!=null&&this.getPayslip_statusDirtyFlag()){
			map.put("payslip_status",Boolean.parseBoolean(this.getPayslip_status()));		
		}		if(this.getState()!=null&&this.getStateDirtyFlag()){
			map.put("state",this.getState());
		}else if(this.getStateDirtyFlag()){
			map.put("state",false);
		}
		if(this.getType()!=null&&this.getTypeDirtyFlag()){
			map.put("type",this.getType());
		}else if(this.getTypeDirtyFlag()){
			map.put("type",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
