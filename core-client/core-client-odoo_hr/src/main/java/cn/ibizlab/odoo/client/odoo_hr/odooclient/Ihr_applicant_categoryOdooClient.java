package cn.ibizlab.odoo.client.odoo_hr.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ihr_applicant_category;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_applicant_category] 服务对象客户端接口
 */
public interface Ihr_applicant_categoryOdooClient {
    
        public void updateBatch(Ihr_applicant_category hr_applicant_category);

        public Page<Ihr_applicant_category> search(SearchContext context);

        public void get(Ihr_applicant_category hr_applicant_category);

        public void create(Ihr_applicant_category hr_applicant_category);

        public void removeBatch(Ihr_applicant_category hr_applicant_category);

        public void remove(Ihr_applicant_category hr_applicant_category);

        public void update(Ihr_applicant_category hr_applicant_category);

        public void createBatch(Ihr_applicant_category hr_applicant_category);

        public List<Ihr_applicant_category> select();


}