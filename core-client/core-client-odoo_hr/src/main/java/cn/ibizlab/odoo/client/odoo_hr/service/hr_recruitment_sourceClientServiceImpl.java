package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_recruitment_source;
import cn.ibizlab.odoo.core.client.service.Ihr_recruitment_sourceClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_recruitment_sourceImpl;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.Ihr_recruitment_sourceOdooClient;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.impl.hr_recruitment_sourceOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[hr_recruitment_source] 服务对象接口
 */
@Service
public class hr_recruitment_sourceClientServiceImpl implements Ihr_recruitment_sourceClientService {
    @Autowired
    private  Ihr_recruitment_sourceOdooClient  hr_recruitment_sourceOdooClient;

    public Ihr_recruitment_source createModel() {		
		return new hr_recruitment_sourceImpl();
	}


        public void createBatch(List<Ihr_recruitment_source> hr_recruitment_sources){
            
        }
        
        public void get(Ihr_recruitment_source hr_recruitment_source){
            this.hr_recruitment_sourceOdooClient.get(hr_recruitment_source) ;
        }
        
        public Page<Ihr_recruitment_source> search(SearchContext context){
            return this.hr_recruitment_sourceOdooClient.search(context) ;
        }
        
        public void update(Ihr_recruitment_source hr_recruitment_source){
this.hr_recruitment_sourceOdooClient.update(hr_recruitment_source) ;
        }
        
        public void remove(Ihr_recruitment_source hr_recruitment_source){
this.hr_recruitment_sourceOdooClient.remove(hr_recruitment_source) ;
        }
        
        public void updateBatch(List<Ihr_recruitment_source> hr_recruitment_sources){
            
        }
        
        public void removeBatch(List<Ihr_recruitment_source> hr_recruitment_sources){
            
        }
        
        public void create(Ihr_recruitment_source hr_recruitment_source){
this.hr_recruitment_sourceOdooClient.create(hr_recruitment_source) ;
        }
        
        public Page<Ihr_recruitment_source> select(SearchContext context){
            return null ;
        }
        

}

