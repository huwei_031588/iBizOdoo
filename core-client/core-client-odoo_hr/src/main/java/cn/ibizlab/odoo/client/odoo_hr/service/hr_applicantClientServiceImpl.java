package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_applicant;
import cn.ibizlab.odoo.core.client.service.Ihr_applicantClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_applicantImpl;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.Ihr_applicantOdooClient;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.impl.hr_applicantOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[hr_applicant] 服务对象接口
 */
@Service
public class hr_applicantClientServiceImpl implements Ihr_applicantClientService {
    @Autowired
    private  Ihr_applicantOdooClient  hr_applicantOdooClient;

    public Ihr_applicant createModel() {		
		return new hr_applicantImpl();
	}


        public void get(Ihr_applicant hr_applicant){
            this.hr_applicantOdooClient.get(hr_applicant) ;
        }
        
        public void createBatch(List<Ihr_applicant> hr_applicants){
            
        }
        
        public void create(Ihr_applicant hr_applicant){
this.hr_applicantOdooClient.create(hr_applicant) ;
        }
        
        public void remove(Ihr_applicant hr_applicant){
this.hr_applicantOdooClient.remove(hr_applicant) ;
        }
        
        public Page<Ihr_applicant> search(SearchContext context){
            return this.hr_applicantOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Ihr_applicant> hr_applicants){
            
        }
        
        public void update(Ihr_applicant hr_applicant){
this.hr_applicantOdooClient.update(hr_applicant) ;
        }
        
        public void removeBatch(List<Ihr_applicant> hr_applicants){
            
        }
        
        public Page<Ihr_applicant> select(SearchContext context){
            return null ;
        }
        

}

