package cn.ibizlab.odoo.client.odoo_hr.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.hr")
@Data
public class odoo_hrClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
