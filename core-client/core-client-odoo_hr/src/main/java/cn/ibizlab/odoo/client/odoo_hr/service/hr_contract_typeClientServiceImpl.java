package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_contract_type;
import cn.ibizlab.odoo.core.client.service.Ihr_contract_typeClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_contract_typeImpl;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.Ihr_contract_typeOdooClient;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.impl.hr_contract_typeOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[hr_contract_type] 服务对象接口
 */
@Service
public class hr_contract_typeClientServiceImpl implements Ihr_contract_typeClientService {
    @Autowired
    private  Ihr_contract_typeOdooClient  hr_contract_typeOdooClient;

    public Ihr_contract_type createModel() {		
		return new hr_contract_typeImpl();
	}


        public void removeBatch(List<Ihr_contract_type> hr_contract_types){
            
        }
        
        public void remove(Ihr_contract_type hr_contract_type){
this.hr_contract_typeOdooClient.remove(hr_contract_type) ;
        }
        
        public void createBatch(List<Ihr_contract_type> hr_contract_types){
            
        }
        
        public void updateBatch(List<Ihr_contract_type> hr_contract_types){
            
        }
        
        public void update(Ihr_contract_type hr_contract_type){
this.hr_contract_typeOdooClient.update(hr_contract_type) ;
        }
        
        public void create(Ihr_contract_type hr_contract_type){
this.hr_contract_typeOdooClient.create(hr_contract_type) ;
        }
        
        public void get(Ihr_contract_type hr_contract_type){
            this.hr_contract_typeOdooClient.get(hr_contract_type) ;
        }
        
        public Page<Ihr_contract_type> search(SearchContext context){
            return this.hr_contract_typeOdooClient.search(context) ;
        }
        
        public Page<Ihr_contract_type> select(SearchContext context){
            return null ;
        }
        

}

