package cn.ibizlab.odoo.client.odoo_hr.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ihr_recruitment_source;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_recruitment_sourceImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[hr_recruitment_source] 服务对象接口
 */
public interface hr_recruitment_sourceFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_recruitment_sources/createbatch")
    public hr_recruitment_sourceImpl createBatch(@RequestBody List<hr_recruitment_sourceImpl> hr_recruitment_sources);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_recruitment_sources/{id}")
    public hr_recruitment_sourceImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_recruitment_sources/search")
    public Page<hr_recruitment_sourceImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_recruitment_sources/{id}")
    public hr_recruitment_sourceImpl update(@PathVariable("id") Integer id,@RequestBody hr_recruitment_sourceImpl hr_recruitment_source);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_recruitment_sources/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_recruitment_sources/updatebatch")
    public hr_recruitment_sourceImpl updateBatch(@RequestBody List<hr_recruitment_sourceImpl> hr_recruitment_sources);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_recruitment_sources/removebatch")
    public hr_recruitment_sourceImpl removeBatch(@RequestBody List<hr_recruitment_sourceImpl> hr_recruitment_sources);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_recruitment_sources")
    public hr_recruitment_sourceImpl create(@RequestBody hr_recruitment_sourceImpl hr_recruitment_source);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_recruitment_sources/select")
    public Page<hr_recruitment_sourceImpl> select();



}
