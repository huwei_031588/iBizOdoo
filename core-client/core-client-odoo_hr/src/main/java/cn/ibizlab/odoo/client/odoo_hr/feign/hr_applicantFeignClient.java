package cn.ibizlab.odoo.client.odoo_hr.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ihr_applicant;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_applicantImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[hr_applicant] 服务对象接口
 */
public interface hr_applicantFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_applicants/{id}")
    public hr_applicantImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_applicants/createbatch")
    public hr_applicantImpl createBatch(@RequestBody List<hr_applicantImpl> hr_applicants);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_applicants")
    public hr_applicantImpl create(@RequestBody hr_applicantImpl hr_applicant);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_applicants/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_applicants/search")
    public Page<hr_applicantImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_applicants/updatebatch")
    public hr_applicantImpl updateBatch(@RequestBody List<hr_applicantImpl> hr_applicants);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_applicants/{id}")
    public hr_applicantImpl update(@PathVariable("id") Integer id,@RequestBody hr_applicantImpl hr_applicant);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_applicants/removebatch")
    public hr_applicantImpl removeBatch(@RequestBody List<hr_applicantImpl> hr_applicants);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_applicants/select")
    public Page<hr_applicantImpl> select();



}
