package cn.ibizlab.odoo.client.odoo_hr.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ihr_job;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_job] 服务对象客户端接口
 */
public interface Ihr_jobOdooClient {
    
        public void update(Ihr_job hr_job);

        public void createBatch(Ihr_job hr_job);

        public void remove(Ihr_job hr_job);

        public void removeBatch(Ihr_job hr_job);

        public void create(Ihr_job hr_job);

        public Page<Ihr_job> search(SearchContext context);

        public void updateBatch(Ihr_job hr_job);

        public void get(Ihr_job hr_job);

        public List<Ihr_job> select();


}