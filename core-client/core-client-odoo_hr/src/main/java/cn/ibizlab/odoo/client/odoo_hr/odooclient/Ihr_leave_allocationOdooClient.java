package cn.ibizlab.odoo.client.odoo_hr.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ihr_leave_allocation;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_leave_allocation] 服务对象客户端接口
 */
public interface Ihr_leave_allocationOdooClient {
    
        public void removeBatch(Ihr_leave_allocation hr_leave_allocation);

        public void remove(Ihr_leave_allocation hr_leave_allocation);

        public void get(Ihr_leave_allocation hr_leave_allocation);

        public void updateBatch(Ihr_leave_allocation hr_leave_allocation);

        public void update(Ihr_leave_allocation hr_leave_allocation);

        public void create(Ihr_leave_allocation hr_leave_allocation);

        public void createBatch(Ihr_leave_allocation hr_leave_allocation);

        public Page<Ihr_leave_allocation> search(SearchContext context);

        public List<Ihr_leave_allocation> select();


}