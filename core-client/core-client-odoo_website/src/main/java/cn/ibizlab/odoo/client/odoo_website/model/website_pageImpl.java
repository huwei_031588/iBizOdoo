package cn.ibizlab.odoo.client.odoo_website.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iwebsite_page;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[website_page] 对象
 */
public class website_pageImpl implements Iwebsite_page,Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 视图结构
     */
    public String arch;

    @JsonIgnore
    public boolean archDirtyFlag;
    
    /**
     * 基础视图结构
     */
    public String arch_base;

    @JsonIgnore
    public boolean arch_baseDirtyFlag;
    
    /**
     * Arch Blob
     */
    public String arch_db;

    @JsonIgnore
    public boolean arch_dbDirtyFlag;
    
    /**
     * Arch 文件名
     */
    public String arch_fs;

    @JsonIgnore
    public boolean arch_fsDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 作为可选继承显示
     */
    public String customize_show;

    @JsonIgnore
    public boolean customize_showDirtyFlag;
    
    /**
     * 发布日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_publish;

    @JsonIgnore
    public boolean date_publishDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 下级字段
     */
    public String field_parent;

    @JsonIgnore
    public boolean field_parentDirtyFlag;
    
    /**
     * 网站页面
     */
    public Integer first_page_id;

    @JsonIgnore
    public boolean first_page_idDirtyFlag;
    
    /**
     * 群组
     */
    public String groups_id;

    @JsonIgnore
    public boolean groups_idDirtyFlag;
    
    /**
     * 标题颜色
     */
    public String header_color;

    @JsonIgnore
    public boolean header_colorDirtyFlag;
    
    /**
     * 标题覆盖层
     */
    public String header_overlay;

    @JsonIgnore
    public boolean header_overlayDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 继承于此的视图
     */
    public String inherit_children_ids;

    @JsonIgnore
    public boolean inherit_children_idsDirtyFlag;
    
    /**
     * 继承的视图
     */
    public Integer inherit_id;

    @JsonIgnore
    public boolean inherit_idDirtyFlag;
    
    /**
     * 主页
     */
    public String is_homepage;

    @JsonIgnore
    public boolean is_homepageDirtyFlag;
    
    /**
     * 已发布
     */
    public String is_published;

    @JsonIgnore
    public boolean is_publishedDirtyFlag;
    
    /**
     * SEO优化
     */
    public String is_seo_optimized;

    @JsonIgnore
    public boolean is_seo_optimizedDirtyFlag;
    
    /**
     * 可见
     */
    public String is_visible;

    @JsonIgnore
    public boolean is_visibleDirtyFlag;
    
    /**
     * 键
     */
    public String key;

    @JsonIgnore
    public boolean keyDirtyFlag;
    
    /**
     * 相关菜单
     */
    public String menu_ids;

    @JsonIgnore
    public boolean menu_idsDirtyFlag;
    
    /**
     * 视图继承模式
     */
    public String mode;

    @JsonIgnore
    public boolean modeDirtyFlag;
    
    /**
     * 模型
     */
    public String model;

    @JsonIgnore
    public boolean modelDirtyFlag;
    
    /**
     * 模型数据
     */
    public Integer model_data_id;

    @JsonIgnore
    public boolean model_data_idDirtyFlag;
    
    /**
     * 模型
     */
    public String model_ids;

    @JsonIgnore
    public boolean model_idsDirtyFlag;
    
    /**
     * 视图名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 页
     */
    public String page_ids;

    @JsonIgnore
    public boolean page_idsDirtyFlag;
    
    /**
     * 序号
     */
    public Integer priority;

    @JsonIgnore
    public boolean priorityDirtyFlag;
    
    /**
     * 主题模板
     */
    public Integer theme_template_id;

    @JsonIgnore
    public boolean theme_template_idDirtyFlag;
    
    /**
     * 视图类型
     */
    public String type;

    @JsonIgnore
    public boolean typeDirtyFlag;
    
    /**
     * 页面 URL
     */
    public String url;

    @JsonIgnore
    public boolean urlDirtyFlag;
    
    /**
     * 视图
     */
    public Integer view_id;

    @JsonIgnore
    public boolean view_idDirtyFlag;
    
    /**
     * 网站
     */
    public Integer website_id;

    @JsonIgnore
    public boolean website_idDirtyFlag;
    
    /**
     * 页面已索引
     */
    public String website_indexed;

    @JsonIgnore
    public boolean website_indexedDirtyFlag;
    
    /**
     * 网站元说明
     */
    public String website_meta_description;

    @JsonIgnore
    public boolean website_meta_descriptionDirtyFlag;
    
    /**
     * 网站meta关键词
     */
    public String website_meta_keywords;

    @JsonIgnore
    public boolean website_meta_keywordsDirtyFlag;
    
    /**
     * 网站opengraph图像
     */
    public String website_meta_og_img;

    @JsonIgnore
    public boolean website_meta_og_imgDirtyFlag;
    
    /**
     * 网站meta标题
     */
    public String website_meta_title;

    @JsonIgnore
    public boolean website_meta_titleDirtyFlag;
    
    /**
     * 在当前网站显示
     */
    public String website_published;

    @JsonIgnore
    public boolean website_publishedDirtyFlag;
    
    /**
     * 网站网址
     */
    public String website_url;

    @JsonIgnore
    public boolean website_urlDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 外部 ID
     */
    public String xml_id;

    @JsonIgnore
    public boolean xml_idDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [视图结构]
     */
    @JsonProperty("arch")
    public String getArch(){
        return this.arch ;
    }

    /**
     * 设置 [视图结构]
     */
    @JsonProperty("arch")
    public void setArch(String  arch){
        this.arch = arch ;
        this.archDirtyFlag = true ;
    }

     /**
     * 获取 [视图结构]脏标记
     */
    @JsonIgnore
    public boolean getArchDirtyFlag(){
        return this.archDirtyFlag ;
    }   

    /**
     * 获取 [基础视图结构]
     */
    @JsonProperty("arch_base")
    public String getArch_base(){
        return this.arch_base ;
    }

    /**
     * 设置 [基础视图结构]
     */
    @JsonProperty("arch_base")
    public void setArch_base(String  arch_base){
        this.arch_base = arch_base ;
        this.arch_baseDirtyFlag = true ;
    }

     /**
     * 获取 [基础视图结构]脏标记
     */
    @JsonIgnore
    public boolean getArch_baseDirtyFlag(){
        return this.arch_baseDirtyFlag ;
    }   

    /**
     * 获取 [Arch Blob]
     */
    @JsonProperty("arch_db")
    public String getArch_db(){
        return this.arch_db ;
    }

    /**
     * 设置 [Arch Blob]
     */
    @JsonProperty("arch_db")
    public void setArch_db(String  arch_db){
        this.arch_db = arch_db ;
        this.arch_dbDirtyFlag = true ;
    }

     /**
     * 获取 [Arch Blob]脏标记
     */
    @JsonIgnore
    public boolean getArch_dbDirtyFlag(){
        return this.arch_dbDirtyFlag ;
    }   

    /**
     * 获取 [Arch 文件名]
     */
    @JsonProperty("arch_fs")
    public String getArch_fs(){
        return this.arch_fs ;
    }

    /**
     * 设置 [Arch 文件名]
     */
    @JsonProperty("arch_fs")
    public void setArch_fs(String  arch_fs){
        this.arch_fs = arch_fs ;
        this.arch_fsDirtyFlag = true ;
    }

     /**
     * 获取 [Arch 文件名]脏标记
     */
    @JsonIgnore
    public boolean getArch_fsDirtyFlag(){
        return this.arch_fsDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [作为可选继承显示]
     */
    @JsonProperty("customize_show")
    public String getCustomize_show(){
        return this.customize_show ;
    }

    /**
     * 设置 [作为可选继承显示]
     */
    @JsonProperty("customize_show")
    public void setCustomize_show(String  customize_show){
        this.customize_show = customize_show ;
        this.customize_showDirtyFlag = true ;
    }

     /**
     * 获取 [作为可选继承显示]脏标记
     */
    @JsonIgnore
    public boolean getCustomize_showDirtyFlag(){
        return this.customize_showDirtyFlag ;
    }   

    /**
     * 获取 [发布日期]
     */
    @JsonProperty("date_publish")
    public Timestamp getDate_publish(){
        return this.date_publish ;
    }

    /**
     * 设置 [发布日期]
     */
    @JsonProperty("date_publish")
    public void setDate_publish(Timestamp  date_publish){
        this.date_publish = date_publish ;
        this.date_publishDirtyFlag = true ;
    }

     /**
     * 获取 [发布日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_publishDirtyFlag(){
        return this.date_publishDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [下级字段]
     */
    @JsonProperty("field_parent")
    public String getField_parent(){
        return this.field_parent ;
    }

    /**
     * 设置 [下级字段]
     */
    @JsonProperty("field_parent")
    public void setField_parent(String  field_parent){
        this.field_parent = field_parent ;
        this.field_parentDirtyFlag = true ;
    }

     /**
     * 获取 [下级字段]脏标记
     */
    @JsonIgnore
    public boolean getField_parentDirtyFlag(){
        return this.field_parentDirtyFlag ;
    }   

    /**
     * 获取 [网站页面]
     */
    @JsonProperty("first_page_id")
    public Integer getFirst_page_id(){
        return this.first_page_id ;
    }

    /**
     * 设置 [网站页面]
     */
    @JsonProperty("first_page_id")
    public void setFirst_page_id(Integer  first_page_id){
        this.first_page_id = first_page_id ;
        this.first_page_idDirtyFlag = true ;
    }

     /**
     * 获取 [网站页面]脏标记
     */
    @JsonIgnore
    public boolean getFirst_page_idDirtyFlag(){
        return this.first_page_idDirtyFlag ;
    }   

    /**
     * 获取 [群组]
     */
    @JsonProperty("groups_id")
    public String getGroups_id(){
        return this.groups_id ;
    }

    /**
     * 设置 [群组]
     */
    @JsonProperty("groups_id")
    public void setGroups_id(String  groups_id){
        this.groups_id = groups_id ;
        this.groups_idDirtyFlag = true ;
    }

     /**
     * 获取 [群组]脏标记
     */
    @JsonIgnore
    public boolean getGroups_idDirtyFlag(){
        return this.groups_idDirtyFlag ;
    }   

    /**
     * 获取 [标题颜色]
     */
    @JsonProperty("header_color")
    public String getHeader_color(){
        return this.header_color ;
    }

    /**
     * 设置 [标题颜色]
     */
    @JsonProperty("header_color")
    public void setHeader_color(String  header_color){
        this.header_color = header_color ;
        this.header_colorDirtyFlag = true ;
    }

     /**
     * 获取 [标题颜色]脏标记
     */
    @JsonIgnore
    public boolean getHeader_colorDirtyFlag(){
        return this.header_colorDirtyFlag ;
    }   

    /**
     * 获取 [标题覆盖层]
     */
    @JsonProperty("header_overlay")
    public String getHeader_overlay(){
        return this.header_overlay ;
    }

    /**
     * 设置 [标题覆盖层]
     */
    @JsonProperty("header_overlay")
    public void setHeader_overlay(String  header_overlay){
        this.header_overlay = header_overlay ;
        this.header_overlayDirtyFlag = true ;
    }

     /**
     * 获取 [标题覆盖层]脏标记
     */
    @JsonIgnore
    public boolean getHeader_overlayDirtyFlag(){
        return this.header_overlayDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [继承于此的视图]
     */
    @JsonProperty("inherit_children_ids")
    public String getInherit_children_ids(){
        return this.inherit_children_ids ;
    }

    /**
     * 设置 [继承于此的视图]
     */
    @JsonProperty("inherit_children_ids")
    public void setInherit_children_ids(String  inherit_children_ids){
        this.inherit_children_ids = inherit_children_ids ;
        this.inherit_children_idsDirtyFlag = true ;
    }

     /**
     * 获取 [继承于此的视图]脏标记
     */
    @JsonIgnore
    public boolean getInherit_children_idsDirtyFlag(){
        return this.inherit_children_idsDirtyFlag ;
    }   

    /**
     * 获取 [继承的视图]
     */
    @JsonProperty("inherit_id")
    public Integer getInherit_id(){
        return this.inherit_id ;
    }

    /**
     * 设置 [继承的视图]
     */
    @JsonProperty("inherit_id")
    public void setInherit_id(Integer  inherit_id){
        this.inherit_id = inherit_id ;
        this.inherit_idDirtyFlag = true ;
    }

     /**
     * 获取 [继承的视图]脏标记
     */
    @JsonIgnore
    public boolean getInherit_idDirtyFlag(){
        return this.inherit_idDirtyFlag ;
    }   

    /**
     * 获取 [主页]
     */
    @JsonProperty("is_homepage")
    public String getIs_homepage(){
        return this.is_homepage ;
    }

    /**
     * 设置 [主页]
     */
    @JsonProperty("is_homepage")
    public void setIs_homepage(String  is_homepage){
        this.is_homepage = is_homepage ;
        this.is_homepageDirtyFlag = true ;
    }

     /**
     * 获取 [主页]脏标记
     */
    @JsonIgnore
    public boolean getIs_homepageDirtyFlag(){
        return this.is_homepageDirtyFlag ;
    }   

    /**
     * 获取 [已发布]
     */
    @JsonProperty("is_published")
    public String getIs_published(){
        return this.is_published ;
    }

    /**
     * 设置 [已发布]
     */
    @JsonProperty("is_published")
    public void setIs_published(String  is_published){
        this.is_published = is_published ;
        this.is_publishedDirtyFlag = true ;
    }

     /**
     * 获取 [已发布]脏标记
     */
    @JsonIgnore
    public boolean getIs_publishedDirtyFlag(){
        return this.is_publishedDirtyFlag ;
    }   

    /**
     * 获取 [SEO优化]
     */
    @JsonProperty("is_seo_optimized")
    public String getIs_seo_optimized(){
        return this.is_seo_optimized ;
    }

    /**
     * 设置 [SEO优化]
     */
    @JsonProperty("is_seo_optimized")
    public void setIs_seo_optimized(String  is_seo_optimized){
        this.is_seo_optimized = is_seo_optimized ;
        this.is_seo_optimizedDirtyFlag = true ;
    }

     /**
     * 获取 [SEO优化]脏标记
     */
    @JsonIgnore
    public boolean getIs_seo_optimizedDirtyFlag(){
        return this.is_seo_optimizedDirtyFlag ;
    }   

    /**
     * 获取 [可见]
     */
    @JsonProperty("is_visible")
    public String getIs_visible(){
        return this.is_visible ;
    }

    /**
     * 设置 [可见]
     */
    @JsonProperty("is_visible")
    public void setIs_visible(String  is_visible){
        this.is_visible = is_visible ;
        this.is_visibleDirtyFlag = true ;
    }

     /**
     * 获取 [可见]脏标记
     */
    @JsonIgnore
    public boolean getIs_visibleDirtyFlag(){
        return this.is_visibleDirtyFlag ;
    }   

    /**
     * 获取 [键]
     */
    @JsonProperty("key")
    public String getKey(){
        return this.key ;
    }

    /**
     * 设置 [键]
     */
    @JsonProperty("key")
    public void setKey(String  key){
        this.key = key ;
        this.keyDirtyFlag = true ;
    }

     /**
     * 获取 [键]脏标记
     */
    @JsonIgnore
    public boolean getKeyDirtyFlag(){
        return this.keyDirtyFlag ;
    }   

    /**
     * 获取 [相关菜单]
     */
    @JsonProperty("menu_ids")
    public String getMenu_ids(){
        return this.menu_ids ;
    }

    /**
     * 设置 [相关菜单]
     */
    @JsonProperty("menu_ids")
    public void setMenu_ids(String  menu_ids){
        this.menu_ids = menu_ids ;
        this.menu_idsDirtyFlag = true ;
    }

     /**
     * 获取 [相关菜单]脏标记
     */
    @JsonIgnore
    public boolean getMenu_idsDirtyFlag(){
        return this.menu_idsDirtyFlag ;
    }   

    /**
     * 获取 [视图继承模式]
     */
    @JsonProperty("mode")
    public String getMode(){
        return this.mode ;
    }

    /**
     * 设置 [视图继承模式]
     */
    @JsonProperty("mode")
    public void setMode(String  mode){
        this.mode = mode ;
        this.modeDirtyFlag = true ;
    }

     /**
     * 获取 [视图继承模式]脏标记
     */
    @JsonIgnore
    public boolean getModeDirtyFlag(){
        return this.modeDirtyFlag ;
    }   

    /**
     * 获取 [模型]
     */
    @JsonProperty("model")
    public String getModel(){
        return this.model ;
    }

    /**
     * 设置 [模型]
     */
    @JsonProperty("model")
    public void setModel(String  model){
        this.model = model ;
        this.modelDirtyFlag = true ;
    }

     /**
     * 获取 [模型]脏标记
     */
    @JsonIgnore
    public boolean getModelDirtyFlag(){
        return this.modelDirtyFlag ;
    }   

    /**
     * 获取 [模型数据]
     */
    @JsonProperty("model_data_id")
    public Integer getModel_data_id(){
        return this.model_data_id ;
    }

    /**
     * 设置 [模型数据]
     */
    @JsonProperty("model_data_id")
    public void setModel_data_id(Integer  model_data_id){
        this.model_data_id = model_data_id ;
        this.model_data_idDirtyFlag = true ;
    }

     /**
     * 获取 [模型数据]脏标记
     */
    @JsonIgnore
    public boolean getModel_data_idDirtyFlag(){
        return this.model_data_idDirtyFlag ;
    }   

    /**
     * 获取 [模型]
     */
    @JsonProperty("model_ids")
    public String getModel_ids(){
        return this.model_ids ;
    }

    /**
     * 设置 [模型]
     */
    @JsonProperty("model_ids")
    public void setModel_ids(String  model_ids){
        this.model_ids = model_ids ;
        this.model_idsDirtyFlag = true ;
    }

     /**
     * 获取 [模型]脏标记
     */
    @JsonIgnore
    public boolean getModel_idsDirtyFlag(){
        return this.model_idsDirtyFlag ;
    }   

    /**
     * 获取 [视图名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [视图名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [视图名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [页]
     */
    @JsonProperty("page_ids")
    public String getPage_ids(){
        return this.page_ids ;
    }

    /**
     * 设置 [页]
     */
    @JsonProperty("page_ids")
    public void setPage_ids(String  page_ids){
        this.page_ids = page_ids ;
        this.page_idsDirtyFlag = true ;
    }

     /**
     * 获取 [页]脏标记
     */
    @JsonIgnore
    public boolean getPage_idsDirtyFlag(){
        return this.page_idsDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("priority")
    public Integer getPriority(){
        return this.priority ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("priority")
    public void setPriority(Integer  priority){
        this.priority = priority ;
        this.priorityDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getPriorityDirtyFlag(){
        return this.priorityDirtyFlag ;
    }   

    /**
     * 获取 [主题模板]
     */
    @JsonProperty("theme_template_id")
    public Integer getTheme_template_id(){
        return this.theme_template_id ;
    }

    /**
     * 设置 [主题模板]
     */
    @JsonProperty("theme_template_id")
    public void setTheme_template_id(Integer  theme_template_id){
        this.theme_template_id = theme_template_id ;
        this.theme_template_idDirtyFlag = true ;
    }

     /**
     * 获取 [主题模板]脏标记
     */
    @JsonIgnore
    public boolean getTheme_template_idDirtyFlag(){
        return this.theme_template_idDirtyFlag ;
    }   

    /**
     * 获取 [视图类型]
     */
    @JsonProperty("type")
    public String getType(){
        return this.type ;
    }

    /**
     * 设置 [视图类型]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

     /**
     * 获取 [视图类型]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return this.typeDirtyFlag ;
    }   

    /**
     * 获取 [页面 URL]
     */
    @JsonProperty("url")
    public String getUrl(){
        return this.url ;
    }

    /**
     * 设置 [页面 URL]
     */
    @JsonProperty("url")
    public void setUrl(String  url){
        this.url = url ;
        this.urlDirtyFlag = true ;
    }

     /**
     * 获取 [页面 URL]脏标记
     */
    @JsonIgnore
    public boolean getUrlDirtyFlag(){
        return this.urlDirtyFlag ;
    }   

    /**
     * 获取 [视图]
     */
    @JsonProperty("view_id")
    public Integer getView_id(){
        return this.view_id ;
    }

    /**
     * 设置 [视图]
     */
    @JsonProperty("view_id")
    public void setView_id(Integer  view_id){
        this.view_id = view_id ;
        this.view_idDirtyFlag = true ;
    }

     /**
     * 获取 [视图]脏标记
     */
    @JsonIgnore
    public boolean getView_idDirtyFlag(){
        return this.view_idDirtyFlag ;
    }   

    /**
     * 获取 [网站]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return this.website_id ;
    }

    /**
     * 设置 [网站]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

     /**
     * 获取 [网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return this.website_idDirtyFlag ;
    }   

    /**
     * 获取 [页面已索引]
     */
    @JsonProperty("website_indexed")
    public String getWebsite_indexed(){
        return this.website_indexed ;
    }

    /**
     * 设置 [页面已索引]
     */
    @JsonProperty("website_indexed")
    public void setWebsite_indexed(String  website_indexed){
        this.website_indexed = website_indexed ;
        this.website_indexedDirtyFlag = true ;
    }

     /**
     * 获取 [页面已索引]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_indexedDirtyFlag(){
        return this.website_indexedDirtyFlag ;
    }   

    /**
     * 获取 [网站元说明]
     */
    @JsonProperty("website_meta_description")
    public String getWebsite_meta_description(){
        return this.website_meta_description ;
    }

    /**
     * 设置 [网站元说明]
     */
    @JsonProperty("website_meta_description")
    public void setWebsite_meta_description(String  website_meta_description){
        this.website_meta_description = website_meta_description ;
        this.website_meta_descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [网站元说明]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_descriptionDirtyFlag(){
        return this.website_meta_descriptionDirtyFlag ;
    }   

    /**
     * 获取 [网站meta关键词]
     */
    @JsonProperty("website_meta_keywords")
    public String getWebsite_meta_keywords(){
        return this.website_meta_keywords ;
    }

    /**
     * 设置 [网站meta关键词]
     */
    @JsonProperty("website_meta_keywords")
    public void setWebsite_meta_keywords(String  website_meta_keywords){
        this.website_meta_keywords = website_meta_keywords ;
        this.website_meta_keywordsDirtyFlag = true ;
    }

     /**
     * 获取 [网站meta关键词]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_keywordsDirtyFlag(){
        return this.website_meta_keywordsDirtyFlag ;
    }   

    /**
     * 获取 [网站opengraph图像]
     */
    @JsonProperty("website_meta_og_img")
    public String getWebsite_meta_og_img(){
        return this.website_meta_og_img ;
    }

    /**
     * 设置 [网站opengraph图像]
     */
    @JsonProperty("website_meta_og_img")
    public void setWebsite_meta_og_img(String  website_meta_og_img){
        this.website_meta_og_img = website_meta_og_img ;
        this.website_meta_og_imgDirtyFlag = true ;
    }

     /**
     * 获取 [网站opengraph图像]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_og_imgDirtyFlag(){
        return this.website_meta_og_imgDirtyFlag ;
    }   

    /**
     * 获取 [网站meta标题]
     */
    @JsonProperty("website_meta_title")
    public String getWebsite_meta_title(){
        return this.website_meta_title ;
    }

    /**
     * 设置 [网站meta标题]
     */
    @JsonProperty("website_meta_title")
    public void setWebsite_meta_title(String  website_meta_title){
        this.website_meta_title = website_meta_title ;
        this.website_meta_titleDirtyFlag = true ;
    }

     /**
     * 获取 [网站meta标题]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_titleDirtyFlag(){
        return this.website_meta_titleDirtyFlag ;
    }   

    /**
     * 获取 [在当前网站显示]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return this.website_published ;
    }

    /**
     * 设置 [在当前网站显示]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

     /**
     * 获取 [在当前网站显示]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return this.website_publishedDirtyFlag ;
    }   

    /**
     * 获取 [网站网址]
     */
    @JsonProperty("website_url")
    public String getWebsite_url(){
        return this.website_url ;
    }

    /**
     * 设置 [网站网址]
     */
    @JsonProperty("website_url")
    public void setWebsite_url(String  website_url){
        this.website_url = website_url ;
        this.website_urlDirtyFlag = true ;
    }

     /**
     * 获取 [网站网址]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_urlDirtyFlag(){
        return this.website_urlDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [外部 ID]
     */
    @JsonProperty("xml_id")
    public String getXml_id(){
        return this.xml_id ;
    }

    /**
     * 设置 [外部 ID]
     */
    @JsonProperty("xml_id")
    public void setXml_id(String  xml_id){
        this.xml_id = xml_id ;
        this.xml_idDirtyFlag = true ;
    }

     /**
     * 获取 [外部 ID]脏标记
     */
    @JsonIgnore
    public boolean getXml_idDirtyFlag(){
        return this.xml_idDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(map.get("active") instanceof Boolean){
			this.setActive(((Boolean)map.get("active"))? "true" : "false");
		}
		if(!(map.get("arch") instanceof Boolean)&& map.get("arch")!=null){
			this.setArch((String)map.get("arch"));
		}
		if(!(map.get("arch_base") instanceof Boolean)&& map.get("arch_base")!=null){
			this.setArch_base((String)map.get("arch_base"));
		}
		if(!(map.get("arch_db") instanceof Boolean)&& map.get("arch_db")!=null){
			this.setArch_db((String)map.get("arch_db"));
		}
		if(!(map.get("arch_fs") instanceof Boolean)&& map.get("arch_fs")!=null){
			this.setArch_fs((String)map.get("arch_fs"));
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(map.get("customize_show") instanceof Boolean){
			this.setCustomize_show(((Boolean)map.get("customize_show"))? "true" : "false");
		}
		if(!(map.get("date_publish") instanceof Boolean)&& map.get("date_publish")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date_publish"));
   			this.setDate_publish(new Timestamp(parse.getTime()));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("field_parent") instanceof Boolean)&& map.get("field_parent")!=null){
			this.setField_parent((String)map.get("field_parent"));
		}
		if(!(map.get("first_page_id") instanceof Boolean)&& map.get("first_page_id")!=null){
			Object[] objs = (Object[])map.get("first_page_id");
			if(objs.length > 0){
				this.setFirst_page_id((Integer)objs[0]);
			}
		}
		if(!(map.get("groups_id") instanceof Boolean)&& map.get("groups_id")!=null){
			Object[] objs = (Object[])map.get("groups_id");
			if(objs.length > 0){
				Integer[] groups_id = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setGroups_id(Arrays.toString(groups_id));
			}
		}
		if(!(map.get("header_color") instanceof Boolean)&& map.get("header_color")!=null){
			this.setHeader_color((String)map.get("header_color"));
		}
		if(map.get("header_overlay") instanceof Boolean){
			this.setHeader_overlay(((Boolean)map.get("header_overlay"))? "true" : "false");
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("inherit_children_ids") instanceof Boolean)&& map.get("inherit_children_ids")!=null){
			Object[] objs = (Object[])map.get("inherit_children_ids");
			if(objs.length > 0){
				Integer[] inherit_children_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setInherit_children_ids(Arrays.toString(inherit_children_ids));
			}
		}
		if(!(map.get("inherit_id") instanceof Boolean)&& map.get("inherit_id")!=null){
			Object[] objs = (Object[])map.get("inherit_id");
			if(objs.length > 0){
				this.setInherit_id((Integer)objs[0]);
			}
		}
		if(map.get("is_homepage") instanceof Boolean){
			this.setIs_homepage(((Boolean)map.get("is_homepage"))? "true" : "false");
		}
		if(map.get("is_published") instanceof Boolean){
			this.setIs_published(((Boolean)map.get("is_published"))? "true" : "false");
		}
		if(map.get("is_seo_optimized") instanceof Boolean){
			this.setIs_seo_optimized(((Boolean)map.get("is_seo_optimized"))? "true" : "false");
		}
		if(map.get("is_visible") instanceof Boolean){
			this.setIs_visible(((Boolean)map.get("is_visible"))? "true" : "false");
		}
		if(!(map.get("key") instanceof Boolean)&& map.get("key")!=null){
			this.setKey((String)map.get("key"));
		}
		if(!(map.get("menu_ids") instanceof Boolean)&& map.get("menu_ids")!=null){
			Object[] objs = (Object[])map.get("menu_ids");
			if(objs.length > 0){
				Integer[] menu_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMenu_ids(Arrays.toString(menu_ids));
			}
		}
		if(!(map.get("mode") instanceof Boolean)&& map.get("mode")!=null){
			this.setMode((String)map.get("mode"));
		}
		if(!(map.get("model") instanceof Boolean)&& map.get("model")!=null){
			this.setModel((String)map.get("model"));
		}
		if(!(map.get("model_data_id") instanceof Boolean)&& map.get("model_data_id")!=null){
			Object[] objs = (Object[])map.get("model_data_id");
			if(objs.length > 0){
				this.setModel_data_id((Integer)objs[0]);
			}
		}
		if(!(map.get("model_ids") instanceof Boolean)&& map.get("model_ids")!=null){
			Object[] objs = (Object[])map.get("model_ids");
			if(objs.length > 0){
				Integer[] model_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setModel_ids(Arrays.toString(model_ids));
			}
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("page_ids") instanceof Boolean)&& map.get("page_ids")!=null){
			Object[] objs = (Object[])map.get("page_ids");
			if(objs.length > 0){
				Integer[] page_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setPage_ids(Arrays.toString(page_ids));
			}
		}
		if(!(map.get("priority") instanceof Boolean)&& map.get("priority")!=null){
			this.setPriority((Integer)map.get("priority"));
		}
		if(!(map.get("theme_template_id") instanceof Boolean)&& map.get("theme_template_id")!=null){
			Object[] objs = (Object[])map.get("theme_template_id");
			if(objs.length > 0){
				this.setTheme_template_id((Integer)objs[0]);
			}
		}
		if(!(map.get("type") instanceof Boolean)&& map.get("type")!=null){
			this.setType((String)map.get("type"));
		}
		if(!(map.get("url") instanceof Boolean)&& map.get("url")!=null){
			this.setUrl((String)map.get("url"));
		}
		if(!(map.get("view_id") instanceof Boolean)&& map.get("view_id")!=null){
			Object[] objs = (Object[])map.get("view_id");
			if(objs.length > 0){
				this.setView_id((Integer)objs[0]);
			}
		}
		if(!(map.get("website_id") instanceof Boolean)&& map.get("website_id")!=null){
			Object[] objs = (Object[])map.get("website_id");
			if(objs.length > 0){
				this.setWebsite_id((Integer)objs[0]);
			}
		}
		if(map.get("website_indexed") instanceof Boolean){
			this.setWebsite_indexed(((Boolean)map.get("website_indexed"))? "true" : "false");
		}
		if(!(map.get("website_meta_description") instanceof Boolean)&& map.get("website_meta_description")!=null){
			this.setWebsite_meta_description((String)map.get("website_meta_description"));
		}
		if(!(map.get("website_meta_keywords") instanceof Boolean)&& map.get("website_meta_keywords")!=null){
			this.setWebsite_meta_keywords((String)map.get("website_meta_keywords"));
		}
		if(!(map.get("website_meta_og_img") instanceof Boolean)&& map.get("website_meta_og_img")!=null){
			this.setWebsite_meta_og_img((String)map.get("website_meta_og_img"));
		}
		if(!(map.get("website_meta_title") instanceof Boolean)&& map.get("website_meta_title")!=null){
			this.setWebsite_meta_title((String)map.get("website_meta_title"));
		}
		if(map.get("website_published") instanceof Boolean){
			this.setWebsite_published(((Boolean)map.get("website_published"))? "true" : "false");
		}
		if(!(map.get("website_url") instanceof Boolean)&& map.get("website_url")!=null){
			this.setWebsite_url((String)map.get("website_url"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("xml_id") instanceof Boolean)&& map.get("xml_id")!=null){
			this.setXml_id((String)map.get("xml_id"));
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getActive()!=null&&this.getActiveDirtyFlag()){
			map.put("active",Boolean.parseBoolean(this.getActive()));		
		}		if(this.getArch()!=null&&this.getArchDirtyFlag()){
			map.put("arch",this.getArch());
		}else if(this.getArchDirtyFlag()){
			map.put("arch",false);
		}
		if(this.getArch_base()!=null&&this.getArch_baseDirtyFlag()){
			map.put("arch_base",this.getArch_base());
		}else if(this.getArch_baseDirtyFlag()){
			map.put("arch_base",false);
		}
		if(this.getArch_db()!=null&&this.getArch_dbDirtyFlag()){
			map.put("arch_db",this.getArch_db());
		}else if(this.getArch_dbDirtyFlag()){
			map.put("arch_db",false);
		}
		if(this.getArch_fs()!=null&&this.getArch_fsDirtyFlag()){
			map.put("arch_fs",this.getArch_fs());
		}else if(this.getArch_fsDirtyFlag()){
			map.put("arch_fs",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCustomize_show()!=null&&this.getCustomize_showDirtyFlag()){
			map.put("customize_show",Boolean.parseBoolean(this.getCustomize_show()));		
		}		if(this.getDate_publish()!=null&&this.getDate_publishDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate_publish());
			map.put("date_publish",datetimeStr);
		}else if(this.getDate_publishDirtyFlag()){
			map.put("date_publish",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getField_parent()!=null&&this.getField_parentDirtyFlag()){
			map.put("field_parent",this.getField_parent());
		}else if(this.getField_parentDirtyFlag()){
			map.put("field_parent",false);
		}
		if(this.getFirst_page_id()!=null&&this.getFirst_page_idDirtyFlag()){
			map.put("first_page_id",this.getFirst_page_id());
		}else if(this.getFirst_page_idDirtyFlag()){
			map.put("first_page_id",false);
		}
		if(this.getGroups_id()!=null&&this.getGroups_idDirtyFlag()){
			map.put("groups_id",this.getGroups_id());
		}else if(this.getGroups_idDirtyFlag()){
			map.put("groups_id",false);
		}
		if(this.getHeader_color()!=null&&this.getHeader_colorDirtyFlag()){
			map.put("header_color",this.getHeader_color());
		}else if(this.getHeader_colorDirtyFlag()){
			map.put("header_color",false);
		}
		if(this.getHeader_overlay()!=null&&this.getHeader_overlayDirtyFlag()){
			map.put("header_overlay",Boolean.parseBoolean(this.getHeader_overlay()));		
		}		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getInherit_children_ids()!=null&&this.getInherit_children_idsDirtyFlag()){
			map.put("inherit_children_ids",this.getInherit_children_ids());
		}else if(this.getInherit_children_idsDirtyFlag()){
			map.put("inherit_children_ids",false);
		}
		if(this.getInherit_id()!=null&&this.getInherit_idDirtyFlag()){
			map.put("inherit_id",this.getInherit_id());
		}else if(this.getInherit_idDirtyFlag()){
			map.put("inherit_id",false);
		}
		if(this.getIs_homepage()!=null&&this.getIs_homepageDirtyFlag()){
			map.put("is_homepage",Boolean.parseBoolean(this.getIs_homepage()));		
		}		if(this.getIs_published()!=null&&this.getIs_publishedDirtyFlag()){
			map.put("is_published",Boolean.parseBoolean(this.getIs_published()));		
		}		if(this.getIs_seo_optimized()!=null&&this.getIs_seo_optimizedDirtyFlag()){
			map.put("is_seo_optimized",Boolean.parseBoolean(this.getIs_seo_optimized()));		
		}		if(this.getIs_visible()!=null&&this.getIs_visibleDirtyFlag()){
			map.put("is_visible",Boolean.parseBoolean(this.getIs_visible()));		
		}		if(this.getKey()!=null&&this.getKeyDirtyFlag()){
			map.put("key",this.getKey());
		}else if(this.getKeyDirtyFlag()){
			map.put("key",false);
		}
		if(this.getMenu_ids()!=null&&this.getMenu_idsDirtyFlag()){
			map.put("menu_ids",this.getMenu_ids());
		}else if(this.getMenu_idsDirtyFlag()){
			map.put("menu_ids",false);
		}
		if(this.getMode()!=null&&this.getModeDirtyFlag()){
			map.put("mode",this.getMode());
		}else if(this.getModeDirtyFlag()){
			map.put("mode",false);
		}
		if(this.getModel()!=null&&this.getModelDirtyFlag()){
			map.put("model",this.getModel());
		}else if(this.getModelDirtyFlag()){
			map.put("model",false);
		}
		if(this.getModel_data_id()!=null&&this.getModel_data_idDirtyFlag()){
			map.put("model_data_id",this.getModel_data_id());
		}else if(this.getModel_data_idDirtyFlag()){
			map.put("model_data_id",false);
		}
		if(this.getModel_ids()!=null&&this.getModel_idsDirtyFlag()){
			map.put("model_ids",this.getModel_ids());
		}else if(this.getModel_idsDirtyFlag()){
			map.put("model_ids",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getPage_ids()!=null&&this.getPage_idsDirtyFlag()){
			map.put("page_ids",this.getPage_ids());
		}else if(this.getPage_idsDirtyFlag()){
			map.put("page_ids",false);
		}
		if(this.getPriority()!=null&&this.getPriorityDirtyFlag()){
			map.put("priority",this.getPriority());
		}else if(this.getPriorityDirtyFlag()){
			map.put("priority",false);
		}
		if(this.getTheme_template_id()!=null&&this.getTheme_template_idDirtyFlag()){
			map.put("theme_template_id",this.getTheme_template_id());
		}else if(this.getTheme_template_idDirtyFlag()){
			map.put("theme_template_id",false);
		}
		if(this.getType()!=null&&this.getTypeDirtyFlag()){
			map.put("type",this.getType());
		}else if(this.getTypeDirtyFlag()){
			map.put("type",false);
		}
		if(this.getUrl()!=null&&this.getUrlDirtyFlag()){
			map.put("url",this.getUrl());
		}else if(this.getUrlDirtyFlag()){
			map.put("url",false);
		}
		if(this.getView_id()!=null&&this.getView_idDirtyFlag()){
			map.put("view_id",this.getView_id());
		}else if(this.getView_idDirtyFlag()){
			map.put("view_id",false);
		}
		if(this.getWebsite_id()!=null&&this.getWebsite_idDirtyFlag()){
			map.put("website_id",this.getWebsite_id());
		}else if(this.getWebsite_idDirtyFlag()){
			map.put("website_id",false);
		}
		if(this.getWebsite_indexed()!=null&&this.getWebsite_indexedDirtyFlag()){
			map.put("website_indexed",Boolean.parseBoolean(this.getWebsite_indexed()));		
		}		if(this.getWebsite_meta_description()!=null&&this.getWebsite_meta_descriptionDirtyFlag()){
			map.put("website_meta_description",this.getWebsite_meta_description());
		}else if(this.getWebsite_meta_descriptionDirtyFlag()){
			map.put("website_meta_description",false);
		}
		if(this.getWebsite_meta_keywords()!=null&&this.getWebsite_meta_keywordsDirtyFlag()){
			map.put("website_meta_keywords",this.getWebsite_meta_keywords());
		}else if(this.getWebsite_meta_keywordsDirtyFlag()){
			map.put("website_meta_keywords",false);
		}
		if(this.getWebsite_meta_og_img()!=null&&this.getWebsite_meta_og_imgDirtyFlag()){
			map.put("website_meta_og_img",this.getWebsite_meta_og_img());
		}else if(this.getWebsite_meta_og_imgDirtyFlag()){
			map.put("website_meta_og_img",false);
		}
		if(this.getWebsite_meta_title()!=null&&this.getWebsite_meta_titleDirtyFlag()){
			map.put("website_meta_title",this.getWebsite_meta_title());
		}else if(this.getWebsite_meta_titleDirtyFlag()){
			map.put("website_meta_title",false);
		}
		if(this.getWebsite_published()!=null&&this.getWebsite_publishedDirtyFlag()){
			map.put("website_published",Boolean.parseBoolean(this.getWebsite_published()));		
		}		if(this.getWebsite_url()!=null&&this.getWebsite_urlDirtyFlag()){
			map.put("website_url",this.getWebsite_url());
		}else if(this.getWebsite_urlDirtyFlag()){
			map.put("website_url",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getXml_id()!=null&&this.getXml_idDirtyFlag()){
			map.put("xml_id",this.getXml_id());
		}else if(this.getXml_idDirtyFlag()){
			map.put("xml_id",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
