package cn.ibizlab.odoo.client.odoo_website.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.website")
@Data
public class odoo_websiteClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
