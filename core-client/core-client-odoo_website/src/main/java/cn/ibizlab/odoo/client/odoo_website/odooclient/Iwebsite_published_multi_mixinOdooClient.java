package cn.ibizlab.odoo.client.odoo_website.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iwebsite_published_multi_mixin;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[website_published_multi_mixin] 服务对象客户端接口
 */
public interface Iwebsite_published_multi_mixinOdooClient {
    
        public void createBatch(Iwebsite_published_multi_mixin website_published_multi_mixin);

        public void get(Iwebsite_published_multi_mixin website_published_multi_mixin);

        public Page<Iwebsite_published_multi_mixin> search(SearchContext context);

        public void remove(Iwebsite_published_multi_mixin website_published_multi_mixin);

        public void updateBatch(Iwebsite_published_multi_mixin website_published_multi_mixin);

        public void create(Iwebsite_published_multi_mixin website_published_multi_mixin);

        public void update(Iwebsite_published_multi_mixin website_published_multi_mixin);

        public void removeBatch(Iwebsite_published_multi_mixin website_published_multi_mixin);

        public List<Iwebsite_published_multi_mixin> select();


}