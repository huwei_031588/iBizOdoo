package cn.ibizlab.odoo.client.odoo_website.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iwebsite_published_mixin;
import cn.ibizlab.odoo.core.client.service.Iwebsite_published_mixinClientService;
import cn.ibizlab.odoo.client.odoo_website.model.website_published_mixinImpl;
import cn.ibizlab.odoo.client.odoo_website.odooclient.Iwebsite_published_mixinOdooClient;
import cn.ibizlab.odoo.client.odoo_website.odooclient.impl.website_published_mixinOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[website_published_mixin] 服务对象接口
 */
@Service
public class website_published_mixinClientServiceImpl implements Iwebsite_published_mixinClientService {
    @Autowired
    private  Iwebsite_published_mixinOdooClient  website_published_mixinOdooClient;

    public Iwebsite_published_mixin createModel() {		
		return new website_published_mixinImpl();
	}


        public Page<Iwebsite_published_mixin> search(SearchContext context){
            return this.website_published_mixinOdooClient.search(context) ;
        }
        
        public void remove(Iwebsite_published_mixin website_published_mixin){
this.website_published_mixinOdooClient.remove(website_published_mixin) ;
        }
        
        public void updateBatch(List<Iwebsite_published_mixin> website_published_mixins){
            
        }
        
        public void createBatch(List<Iwebsite_published_mixin> website_published_mixins){
            
        }
        
        public void create(Iwebsite_published_mixin website_published_mixin){
this.website_published_mixinOdooClient.create(website_published_mixin) ;
        }
        
        public void update(Iwebsite_published_mixin website_published_mixin){
this.website_published_mixinOdooClient.update(website_published_mixin) ;
        }
        
        public void get(Iwebsite_published_mixin website_published_mixin){
            this.website_published_mixinOdooClient.get(website_published_mixin) ;
        }
        
        public void removeBatch(List<Iwebsite_published_mixin> website_published_mixins){
            
        }
        
        public Page<Iwebsite_published_mixin> select(SearchContext context){
            return null ;
        }
        

}

