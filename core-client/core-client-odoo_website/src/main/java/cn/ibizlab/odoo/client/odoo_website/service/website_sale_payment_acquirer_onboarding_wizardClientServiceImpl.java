package cn.ibizlab.odoo.client.odoo_website.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iwebsite_sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.core.client.service.Iwebsite_sale_payment_acquirer_onboarding_wizardClientService;
import cn.ibizlab.odoo.client.odoo_website.model.website_sale_payment_acquirer_onboarding_wizardImpl;
import cn.ibizlab.odoo.client.odoo_website.odooclient.Iwebsite_sale_payment_acquirer_onboarding_wizardOdooClient;
import cn.ibizlab.odoo.client.odoo_website.odooclient.impl.website_sale_payment_acquirer_onboarding_wizardOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[website_sale_payment_acquirer_onboarding_wizard] 服务对象接口
 */
@Service
public class website_sale_payment_acquirer_onboarding_wizardClientServiceImpl implements Iwebsite_sale_payment_acquirer_onboarding_wizardClientService {
    @Autowired
    private  Iwebsite_sale_payment_acquirer_onboarding_wizardOdooClient  website_sale_payment_acquirer_onboarding_wizardOdooClient;

    public Iwebsite_sale_payment_acquirer_onboarding_wizard createModel() {		
		return new website_sale_payment_acquirer_onboarding_wizardImpl();
	}


        public void create(Iwebsite_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard){
this.website_sale_payment_acquirer_onboarding_wizardOdooClient.create(website_sale_payment_acquirer_onboarding_wizard) ;
        }
        
        public void updateBatch(List<Iwebsite_sale_payment_acquirer_onboarding_wizard> website_sale_payment_acquirer_onboarding_wizards){
            
        }
        
        public void removeBatch(List<Iwebsite_sale_payment_acquirer_onboarding_wizard> website_sale_payment_acquirer_onboarding_wizards){
            
        }
        
        public void get(Iwebsite_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard){
            this.website_sale_payment_acquirer_onboarding_wizardOdooClient.get(website_sale_payment_acquirer_onboarding_wizard) ;
        }
        
        public void update(Iwebsite_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard){
this.website_sale_payment_acquirer_onboarding_wizardOdooClient.update(website_sale_payment_acquirer_onboarding_wizard) ;
        }
        
        public void remove(Iwebsite_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard){
this.website_sale_payment_acquirer_onboarding_wizardOdooClient.remove(website_sale_payment_acquirer_onboarding_wizard) ;
        }
        
        public Page<Iwebsite_sale_payment_acquirer_onboarding_wizard> search(SearchContext context){
            return this.website_sale_payment_acquirer_onboarding_wizardOdooClient.search(context) ;
        }
        
        public void createBatch(List<Iwebsite_sale_payment_acquirer_onboarding_wizard> website_sale_payment_acquirer_onboarding_wizards){
            
        }
        
        public Page<Iwebsite_sale_payment_acquirer_onboarding_wizard> select(SearchContext context){
            return null ;
        }
        

}

