package cn.ibizlab.odoo.client.odoo_website.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iwebsite_menu;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[website_menu] 对象
 */
public class website_menuImpl implements Iwebsite_menu,Serializable{

    /**
     * 下级菜单
     */
    public String child_id;

    @JsonIgnore
    public boolean child_idDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 可见
     */
    public String is_visible;

    @JsonIgnore
    public boolean is_visibleDirtyFlag;
    
    /**
     * 菜单
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 新窗口
     */
    public String new_window;

    @JsonIgnore
    public boolean new_windowDirtyFlag;
    
    /**
     * 相关页面
     */
    public Integer page_id;

    @JsonIgnore
    public boolean page_idDirtyFlag;
    
    /**
     * 相关页面
     */
    public String page_id_text;

    @JsonIgnore
    public boolean page_id_textDirtyFlag;
    
    /**
     * 上级菜单
     */
    public Integer parent_id;

    @JsonIgnore
    public boolean parent_idDirtyFlag;
    
    /**
     * 上级菜单
     */
    public String parent_id_text;

    @JsonIgnore
    public boolean parent_id_textDirtyFlag;
    
    /**
     * 上级路径
     */
    public String parent_path;

    @JsonIgnore
    public boolean parent_pathDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 主题模板
     */
    public Integer theme_template_id;

    @JsonIgnore
    public boolean theme_template_idDirtyFlag;
    
    /**
     * Url
     */
    public String url;

    @JsonIgnore
    public boolean urlDirtyFlag;
    
    /**
     * 网站
     */
    public Integer website_id;

    @JsonIgnore
    public boolean website_idDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [下级菜单]
     */
    @JsonProperty("child_id")
    public String getChild_id(){
        return this.child_id ;
    }

    /**
     * 设置 [下级菜单]
     */
    @JsonProperty("child_id")
    public void setChild_id(String  child_id){
        this.child_id = child_id ;
        this.child_idDirtyFlag = true ;
    }

     /**
     * 获取 [下级菜单]脏标记
     */
    @JsonIgnore
    public boolean getChild_idDirtyFlag(){
        return this.child_idDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [可见]
     */
    @JsonProperty("is_visible")
    public String getIs_visible(){
        return this.is_visible ;
    }

    /**
     * 设置 [可见]
     */
    @JsonProperty("is_visible")
    public void setIs_visible(String  is_visible){
        this.is_visible = is_visible ;
        this.is_visibleDirtyFlag = true ;
    }

     /**
     * 获取 [可见]脏标记
     */
    @JsonIgnore
    public boolean getIs_visibleDirtyFlag(){
        return this.is_visibleDirtyFlag ;
    }   

    /**
     * 获取 [菜单]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [菜单]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [菜单]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [新窗口]
     */
    @JsonProperty("new_window")
    public String getNew_window(){
        return this.new_window ;
    }

    /**
     * 设置 [新窗口]
     */
    @JsonProperty("new_window")
    public void setNew_window(String  new_window){
        this.new_window = new_window ;
        this.new_windowDirtyFlag = true ;
    }

     /**
     * 获取 [新窗口]脏标记
     */
    @JsonIgnore
    public boolean getNew_windowDirtyFlag(){
        return this.new_windowDirtyFlag ;
    }   

    /**
     * 获取 [相关页面]
     */
    @JsonProperty("page_id")
    public Integer getPage_id(){
        return this.page_id ;
    }

    /**
     * 设置 [相关页面]
     */
    @JsonProperty("page_id")
    public void setPage_id(Integer  page_id){
        this.page_id = page_id ;
        this.page_idDirtyFlag = true ;
    }

     /**
     * 获取 [相关页面]脏标记
     */
    @JsonIgnore
    public boolean getPage_idDirtyFlag(){
        return this.page_idDirtyFlag ;
    }   

    /**
     * 获取 [相关页面]
     */
    @JsonProperty("page_id_text")
    public String getPage_id_text(){
        return this.page_id_text ;
    }

    /**
     * 设置 [相关页面]
     */
    @JsonProperty("page_id_text")
    public void setPage_id_text(String  page_id_text){
        this.page_id_text = page_id_text ;
        this.page_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [相关页面]脏标记
     */
    @JsonIgnore
    public boolean getPage_id_textDirtyFlag(){
        return this.page_id_textDirtyFlag ;
    }   

    /**
     * 获取 [上级菜单]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return this.parent_id ;
    }

    /**
     * 设置 [上级菜单]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级菜单]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return this.parent_idDirtyFlag ;
    }   

    /**
     * 获取 [上级菜单]
     */
    @JsonProperty("parent_id_text")
    public String getParent_id_text(){
        return this.parent_id_text ;
    }

    /**
     * 设置 [上级菜单]
     */
    @JsonProperty("parent_id_text")
    public void setParent_id_text(String  parent_id_text){
        this.parent_id_text = parent_id_text ;
        this.parent_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [上级菜单]脏标记
     */
    @JsonIgnore
    public boolean getParent_id_textDirtyFlag(){
        return this.parent_id_textDirtyFlag ;
    }   

    /**
     * 获取 [上级路径]
     */
    @JsonProperty("parent_path")
    public String getParent_path(){
        return this.parent_path ;
    }

    /**
     * 设置 [上级路径]
     */
    @JsonProperty("parent_path")
    public void setParent_path(String  parent_path){
        this.parent_path = parent_path ;
        this.parent_pathDirtyFlag = true ;
    }

     /**
     * 获取 [上级路径]脏标记
     */
    @JsonIgnore
    public boolean getParent_pathDirtyFlag(){
        return this.parent_pathDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [主题模板]
     */
    @JsonProperty("theme_template_id")
    public Integer getTheme_template_id(){
        return this.theme_template_id ;
    }

    /**
     * 设置 [主题模板]
     */
    @JsonProperty("theme_template_id")
    public void setTheme_template_id(Integer  theme_template_id){
        this.theme_template_id = theme_template_id ;
        this.theme_template_idDirtyFlag = true ;
    }

     /**
     * 获取 [主题模板]脏标记
     */
    @JsonIgnore
    public boolean getTheme_template_idDirtyFlag(){
        return this.theme_template_idDirtyFlag ;
    }   

    /**
     * 获取 [Url]
     */
    @JsonProperty("url")
    public String getUrl(){
        return this.url ;
    }

    /**
     * 设置 [Url]
     */
    @JsonProperty("url")
    public void setUrl(String  url){
        this.url = url ;
        this.urlDirtyFlag = true ;
    }

     /**
     * 获取 [Url]脏标记
     */
    @JsonIgnore
    public boolean getUrlDirtyFlag(){
        return this.urlDirtyFlag ;
    }   

    /**
     * 获取 [网站]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return this.website_id ;
    }

    /**
     * 设置 [网站]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

     /**
     * 获取 [网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return this.website_idDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("child_id") instanceof Boolean)&& map.get("child_id")!=null){
			Object[] objs = (Object[])map.get("child_id");
			if(objs.length > 0){
				Integer[] child_id = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setChild_id(Arrays.toString(child_id));
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(map.get("is_visible") instanceof Boolean){
			this.setIs_visible(((Boolean)map.get("is_visible"))? "true" : "false");
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(map.get("new_window") instanceof Boolean){
			this.setNew_window(((Boolean)map.get("new_window"))? "true" : "false");
		}
		if(!(map.get("page_id") instanceof Boolean)&& map.get("page_id")!=null){
			Object[] objs = (Object[])map.get("page_id");
			if(objs.length > 0){
				this.setPage_id((Integer)objs[0]);
			}
		}
		if(!(map.get("page_id") instanceof Boolean)&& map.get("page_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("page_id");
			if(objs.length > 1){
				this.setPage_id_text((String)objs[1]);
			}
		}
		if(!(map.get("parent_id") instanceof Boolean)&& map.get("parent_id")!=null){
			Object[] objs = (Object[])map.get("parent_id");
			if(objs.length > 0){
				this.setParent_id((Integer)objs[0]);
			}
		}
		if(!(map.get("parent_id") instanceof Boolean)&& map.get("parent_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("parent_id");
			if(objs.length > 1){
				this.setParent_id_text((String)objs[1]);
			}
		}
		if(!(map.get("parent_path") instanceof Boolean)&& map.get("parent_path")!=null){
			this.setParent_path((String)map.get("parent_path"));
		}
		if(!(map.get("sequence") instanceof Boolean)&& map.get("sequence")!=null){
			this.setSequence((Integer)map.get("sequence"));
		}
		if(!(map.get("theme_template_id") instanceof Boolean)&& map.get("theme_template_id")!=null){
			Object[] objs = (Object[])map.get("theme_template_id");
			if(objs.length > 0){
				this.setTheme_template_id((Integer)objs[0]);
			}
		}
		if(!(map.get("url") instanceof Boolean)&& map.get("url")!=null){
			this.setUrl((String)map.get("url"));
		}
		if(!(map.get("website_id") instanceof Boolean)&& map.get("website_id")!=null){
			Object[] objs = (Object[])map.get("website_id");
			if(objs.length > 0){
				this.setWebsite_id((Integer)objs[0]);
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getChild_id()!=null&&this.getChild_idDirtyFlag()){
			map.put("child_id",this.getChild_id());
		}else if(this.getChild_idDirtyFlag()){
			map.put("child_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getIs_visible()!=null&&this.getIs_visibleDirtyFlag()){
			map.put("is_visible",Boolean.parseBoolean(this.getIs_visible()));		
		}		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getNew_window()!=null&&this.getNew_windowDirtyFlag()){
			map.put("new_window",Boolean.parseBoolean(this.getNew_window()));		
		}		if(this.getPage_id()!=null&&this.getPage_idDirtyFlag()){
			map.put("page_id",this.getPage_id());
		}else if(this.getPage_idDirtyFlag()){
			map.put("page_id",false);
		}
		if(this.getPage_id_text()!=null&&this.getPage_id_textDirtyFlag()){
			//忽略文本外键page_id_text
		}else if(this.getPage_id_textDirtyFlag()){
			map.put("page_id",false);
		}
		if(this.getParent_id()!=null&&this.getParent_idDirtyFlag()){
			map.put("parent_id",this.getParent_id());
		}else if(this.getParent_idDirtyFlag()){
			map.put("parent_id",false);
		}
		if(this.getParent_id_text()!=null&&this.getParent_id_textDirtyFlag()){
			//忽略文本外键parent_id_text
		}else if(this.getParent_id_textDirtyFlag()){
			map.put("parent_id",false);
		}
		if(this.getParent_path()!=null&&this.getParent_pathDirtyFlag()){
			map.put("parent_path",this.getParent_path());
		}else if(this.getParent_pathDirtyFlag()){
			map.put("parent_path",false);
		}
		if(this.getSequence()!=null&&this.getSequenceDirtyFlag()){
			map.put("sequence",this.getSequence());
		}else if(this.getSequenceDirtyFlag()){
			map.put("sequence",false);
		}
		if(this.getTheme_template_id()!=null&&this.getTheme_template_idDirtyFlag()){
			map.put("theme_template_id",this.getTheme_template_id());
		}else if(this.getTheme_template_idDirtyFlag()){
			map.put("theme_template_id",false);
		}
		if(this.getUrl()!=null&&this.getUrlDirtyFlag()){
			map.put("url",this.getUrl());
		}else if(this.getUrlDirtyFlag()){
			map.put("url",false);
		}
		if(this.getWebsite_id()!=null&&this.getWebsite_idDirtyFlag()){
			map.put("website_id",this.getWebsite_id());
		}else if(this.getWebsite_idDirtyFlag()){
			map.put("website_id",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
