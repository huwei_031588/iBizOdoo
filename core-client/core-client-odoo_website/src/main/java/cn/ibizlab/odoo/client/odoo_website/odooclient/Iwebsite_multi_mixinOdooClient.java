package cn.ibizlab.odoo.client.odoo_website.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iwebsite_multi_mixin;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[website_multi_mixin] 服务对象客户端接口
 */
public interface Iwebsite_multi_mixinOdooClient {
    
        public void get(Iwebsite_multi_mixin website_multi_mixin);

        public void create(Iwebsite_multi_mixin website_multi_mixin);

        public void remove(Iwebsite_multi_mixin website_multi_mixin);

        public void createBatch(Iwebsite_multi_mixin website_multi_mixin);

        public Page<Iwebsite_multi_mixin> search(SearchContext context);

        public void updateBatch(Iwebsite_multi_mixin website_multi_mixin);

        public void update(Iwebsite_multi_mixin website_multi_mixin);

        public void removeBatch(Iwebsite_multi_mixin website_multi_mixin);

        public List<Iwebsite_multi_mixin> select();


}