package cn.ibizlab.odoo.client.odoo_website.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iwebsite_seo_metadata;
import cn.ibizlab.odoo.core.client.service.Iwebsite_seo_metadataClientService;
import cn.ibizlab.odoo.client.odoo_website.model.website_seo_metadataImpl;
import cn.ibizlab.odoo.client.odoo_website.odooclient.Iwebsite_seo_metadataOdooClient;
import cn.ibizlab.odoo.client.odoo_website.odooclient.impl.website_seo_metadataOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[website_seo_metadata] 服务对象接口
 */
@Service
public class website_seo_metadataClientServiceImpl implements Iwebsite_seo_metadataClientService {
    @Autowired
    private  Iwebsite_seo_metadataOdooClient  website_seo_metadataOdooClient;

    public Iwebsite_seo_metadata createModel() {		
		return new website_seo_metadataImpl();
	}


        public Page<Iwebsite_seo_metadata> search(SearchContext context){
            return this.website_seo_metadataOdooClient.search(context) ;
        }
        
        public void createBatch(List<Iwebsite_seo_metadata> website_seo_metadata){
            
        }
        
        public void create(Iwebsite_seo_metadata website_seo_metadata){
this.website_seo_metadataOdooClient.create(website_seo_metadata) ;
        }
        
        public void get(Iwebsite_seo_metadata website_seo_metadata){
            this.website_seo_metadataOdooClient.get(website_seo_metadata) ;
        }
        
        public void updateBatch(List<Iwebsite_seo_metadata> website_seo_metadata){
            
        }
        
        public void removeBatch(List<Iwebsite_seo_metadata> website_seo_metadata){
            
        }
        
        public void update(Iwebsite_seo_metadata website_seo_metadata){
this.website_seo_metadataOdooClient.update(website_seo_metadata) ;
        }
        
        public void remove(Iwebsite_seo_metadata website_seo_metadata){
this.website_seo_metadataOdooClient.remove(website_seo_metadata) ;
        }
        
        public Page<Iwebsite_seo_metadata> select(SearchContext context){
            return null ;
        }
        

}

