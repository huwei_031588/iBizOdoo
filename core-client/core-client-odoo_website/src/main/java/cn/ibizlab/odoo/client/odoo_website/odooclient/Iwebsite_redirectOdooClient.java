package cn.ibizlab.odoo.client.odoo_website.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iwebsite_redirect;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[website_redirect] 服务对象客户端接口
 */
public interface Iwebsite_redirectOdooClient {
    
        public void remove(Iwebsite_redirect website_redirect);

        public void update(Iwebsite_redirect website_redirect);

        public Page<Iwebsite_redirect> search(SearchContext context);

        public void get(Iwebsite_redirect website_redirect);

        public void createBatch(Iwebsite_redirect website_redirect);

        public void updateBatch(Iwebsite_redirect website_redirect);

        public void create(Iwebsite_redirect website_redirect);

        public void removeBatch(Iwebsite_redirect website_redirect);

        public List<Iwebsite_redirect> select();


}