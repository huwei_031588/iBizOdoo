package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_template_attribute_exclusion;
import cn.ibizlab.odoo.core.client.service.Iproduct_template_attribute_exclusionClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_template_attribute_exclusionImpl;
import cn.ibizlab.odoo.client.odoo_product.odooclient.Iproduct_template_attribute_exclusionOdooClient;
import cn.ibizlab.odoo.client.odoo_product.odooclient.impl.product_template_attribute_exclusionOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[product_template_attribute_exclusion] 服务对象接口
 */
@Service
public class product_template_attribute_exclusionClientServiceImpl implements Iproduct_template_attribute_exclusionClientService {
    @Autowired
    private  Iproduct_template_attribute_exclusionOdooClient  product_template_attribute_exclusionOdooClient;

    public Iproduct_template_attribute_exclusion createModel() {		
		return new product_template_attribute_exclusionImpl();
	}


        public void updateBatch(List<Iproduct_template_attribute_exclusion> product_template_attribute_exclusions){
            
        }
        
        public void get(Iproduct_template_attribute_exclusion product_template_attribute_exclusion){
            this.product_template_attribute_exclusionOdooClient.get(product_template_attribute_exclusion) ;
        }
        
        public void create(Iproduct_template_attribute_exclusion product_template_attribute_exclusion){
this.product_template_attribute_exclusionOdooClient.create(product_template_attribute_exclusion) ;
        }
        
        public void update(Iproduct_template_attribute_exclusion product_template_attribute_exclusion){
this.product_template_attribute_exclusionOdooClient.update(product_template_attribute_exclusion) ;
        }
        
        public Page<Iproduct_template_attribute_exclusion> search(SearchContext context){
            return this.product_template_attribute_exclusionOdooClient.search(context) ;
        }
        
        public void remove(Iproduct_template_attribute_exclusion product_template_attribute_exclusion){
this.product_template_attribute_exclusionOdooClient.remove(product_template_attribute_exclusion) ;
        }
        
        public void removeBatch(List<Iproduct_template_attribute_exclusion> product_template_attribute_exclusions){
            
        }
        
        public void createBatch(List<Iproduct_template_attribute_exclusion> product_template_attribute_exclusions){
            
        }
        
        public Page<Iproduct_template_attribute_exclusion> select(SearchContext context){
            return null ;
        }
        

}

