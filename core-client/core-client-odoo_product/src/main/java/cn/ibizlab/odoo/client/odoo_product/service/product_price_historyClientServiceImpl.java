package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_price_history;
import cn.ibizlab.odoo.core.client.service.Iproduct_price_historyClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_price_historyImpl;
import cn.ibizlab.odoo.client.odoo_product.odooclient.Iproduct_price_historyOdooClient;
import cn.ibizlab.odoo.client.odoo_product.odooclient.impl.product_price_historyOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[product_price_history] 服务对象接口
 */
@Service
public class product_price_historyClientServiceImpl implements Iproduct_price_historyClientService {
    @Autowired
    private  Iproduct_price_historyOdooClient  product_price_historyOdooClient;

    public Iproduct_price_history createModel() {		
		return new product_price_historyImpl();
	}


        public Page<Iproduct_price_history> search(SearchContext context){
            return this.product_price_historyOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Iproduct_price_history> product_price_histories){
            
        }
        
        public void createBatch(List<Iproduct_price_history> product_price_histories){
            
        }
        
        public void remove(Iproduct_price_history product_price_history){
this.product_price_historyOdooClient.remove(product_price_history) ;
        }
        
        public void create(Iproduct_price_history product_price_history){
this.product_price_historyOdooClient.create(product_price_history) ;
        }
        
        public void get(Iproduct_price_history product_price_history){
            this.product_price_historyOdooClient.get(product_price_history) ;
        }
        
        public void update(Iproduct_price_history product_price_history){
this.product_price_historyOdooClient.update(product_price_history) ;
        }
        
        public void removeBatch(List<Iproduct_price_history> product_price_histories){
            
        }
        
        public Page<Iproduct_price_history> select(SearchContext context){
            return null ;
        }
        

}

