package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_removal;
import cn.ibizlab.odoo.core.client.service.Iproduct_removalClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_removalImpl;
import cn.ibizlab.odoo.client.odoo_product.odooclient.Iproduct_removalOdooClient;
import cn.ibizlab.odoo.client.odoo_product.odooclient.impl.product_removalOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[product_removal] 服务对象接口
 */
@Service
public class product_removalClientServiceImpl implements Iproduct_removalClientService {
    @Autowired
    private  Iproduct_removalOdooClient  product_removalOdooClient;

    public Iproduct_removal createModel() {		
		return new product_removalImpl();
	}


        public void remove(Iproduct_removal product_removal){
this.product_removalOdooClient.remove(product_removal) ;
        }
        
        public void removeBatch(List<Iproduct_removal> product_removals){
            
        }
        
        public void createBatch(List<Iproduct_removal> product_removals){
            
        }
        
        public Page<Iproduct_removal> search(SearchContext context){
            return this.product_removalOdooClient.search(context) ;
        }
        
        public void create(Iproduct_removal product_removal){
this.product_removalOdooClient.create(product_removal) ;
        }
        
        public void updateBatch(List<Iproduct_removal> product_removals){
            
        }
        
        public void get(Iproduct_removal product_removal){
            this.product_removalOdooClient.get(product_removal) ;
        }
        
        public void update(Iproduct_removal product_removal){
this.product_removalOdooClient.update(product_removal) ;
        }
        
        public Page<Iproduct_removal> select(SearchContext context){
            return null ;
        }
        

}

