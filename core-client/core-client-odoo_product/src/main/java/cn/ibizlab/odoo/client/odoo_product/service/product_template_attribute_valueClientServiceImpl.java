package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_template_attribute_value;
import cn.ibizlab.odoo.core.client.service.Iproduct_template_attribute_valueClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_template_attribute_valueImpl;
import cn.ibizlab.odoo.client.odoo_product.odooclient.Iproduct_template_attribute_valueOdooClient;
import cn.ibizlab.odoo.client.odoo_product.odooclient.impl.product_template_attribute_valueOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[product_template_attribute_value] 服务对象接口
 */
@Service
public class product_template_attribute_valueClientServiceImpl implements Iproduct_template_attribute_valueClientService {
    @Autowired
    private  Iproduct_template_attribute_valueOdooClient  product_template_attribute_valueOdooClient;

    public Iproduct_template_attribute_value createModel() {		
		return new product_template_attribute_valueImpl();
	}


        public void updateBatch(List<Iproduct_template_attribute_value> product_template_attribute_values){
            
        }
        
        public void createBatch(List<Iproduct_template_attribute_value> product_template_attribute_values){
            
        }
        
        public void get(Iproduct_template_attribute_value product_template_attribute_value){
            this.product_template_attribute_valueOdooClient.get(product_template_attribute_value) ;
        }
        
        public void removeBatch(List<Iproduct_template_attribute_value> product_template_attribute_values){
            
        }
        
        public void create(Iproduct_template_attribute_value product_template_attribute_value){
this.product_template_attribute_valueOdooClient.create(product_template_attribute_value) ;
        }
        
        public Page<Iproduct_template_attribute_value> search(SearchContext context){
            return this.product_template_attribute_valueOdooClient.search(context) ;
        }
        
        public void remove(Iproduct_template_attribute_value product_template_attribute_value){
this.product_template_attribute_valueOdooClient.remove(product_template_attribute_value) ;
        }
        
        public void update(Iproduct_template_attribute_value product_template_attribute_value){
this.product_template_attribute_valueOdooClient.update(product_template_attribute_value) ;
        }
        
        public Page<Iproduct_template_attribute_value> select(SearchContext context){
            return null ;
        }
        

}

