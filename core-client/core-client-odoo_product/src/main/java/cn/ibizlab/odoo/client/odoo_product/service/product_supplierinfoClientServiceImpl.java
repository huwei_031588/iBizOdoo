package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_supplierinfo;
import cn.ibizlab.odoo.core.client.service.Iproduct_supplierinfoClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_supplierinfoImpl;
import cn.ibizlab.odoo.client.odoo_product.odooclient.Iproduct_supplierinfoOdooClient;
import cn.ibizlab.odoo.client.odoo_product.odooclient.impl.product_supplierinfoOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[product_supplierinfo] 服务对象接口
 */
@Service
public class product_supplierinfoClientServiceImpl implements Iproduct_supplierinfoClientService {
    @Autowired
    private  Iproduct_supplierinfoOdooClient  product_supplierinfoOdooClient;

    public Iproduct_supplierinfo createModel() {		
		return new product_supplierinfoImpl();
	}


        public void createBatch(List<Iproduct_supplierinfo> product_supplierinfos){
            
        }
        
        public void update(Iproduct_supplierinfo product_supplierinfo){
this.product_supplierinfoOdooClient.update(product_supplierinfo) ;
        }
        
        public void remove(Iproduct_supplierinfo product_supplierinfo){
this.product_supplierinfoOdooClient.remove(product_supplierinfo) ;
        }
        
        public void removeBatch(List<Iproduct_supplierinfo> product_supplierinfos){
            
        }
        
        public void updateBatch(List<Iproduct_supplierinfo> product_supplierinfos){
            
        }
        
        public Page<Iproduct_supplierinfo> search(SearchContext context){
            return this.product_supplierinfoOdooClient.search(context) ;
        }
        
        public void create(Iproduct_supplierinfo product_supplierinfo){
this.product_supplierinfoOdooClient.create(product_supplierinfo) ;
        }
        
        public void get(Iproduct_supplierinfo product_supplierinfo){
            this.product_supplierinfoOdooClient.get(product_supplierinfo) ;
        }
        
        public Page<Iproduct_supplierinfo> select(SearchContext context){
            return null ;
        }
        

}

