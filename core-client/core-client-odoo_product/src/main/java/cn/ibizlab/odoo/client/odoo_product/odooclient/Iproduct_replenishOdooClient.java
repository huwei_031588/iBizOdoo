package cn.ibizlab.odoo.client.odoo_product.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iproduct_replenish;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_replenish] 服务对象客户端接口
 */
public interface Iproduct_replenishOdooClient {
    
        public void removeBatch(Iproduct_replenish product_replenish);

        public Page<Iproduct_replenish> search(SearchContext context);

        public void createBatch(Iproduct_replenish product_replenish);

        public void updateBatch(Iproduct_replenish product_replenish);

        public void remove(Iproduct_replenish product_replenish);

        public void update(Iproduct_replenish product_replenish);

        public void get(Iproduct_replenish product_replenish);

        public void create(Iproduct_replenish product_replenish);

        public List<Iproduct_replenish> select();


}