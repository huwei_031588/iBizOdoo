package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_pricelist;
import cn.ibizlab.odoo.core.client.service.Iproduct_pricelistClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_pricelistImpl;
import cn.ibizlab.odoo.client.odoo_product.odooclient.Iproduct_pricelistOdooClient;
import cn.ibizlab.odoo.client.odoo_product.odooclient.impl.product_pricelistOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[product_pricelist] 服务对象接口
 */
@Service
public class product_pricelistClientServiceImpl implements Iproduct_pricelistClientService {
    @Autowired
    private  Iproduct_pricelistOdooClient  product_pricelistOdooClient;

    public Iproduct_pricelist createModel() {		
		return new product_pricelistImpl();
	}


        public void remove(Iproduct_pricelist product_pricelist){
this.product_pricelistOdooClient.remove(product_pricelist) ;
        }
        
        public void get(Iproduct_pricelist product_pricelist){
            this.product_pricelistOdooClient.get(product_pricelist) ;
        }
        
        public Page<Iproduct_pricelist> search(SearchContext context){
            return this.product_pricelistOdooClient.search(context) ;
        }
        
        public void createBatch(List<Iproduct_pricelist> product_pricelists){
            
        }
        
        public void updateBatch(List<Iproduct_pricelist> product_pricelists){
            
        }
        
        public void removeBatch(List<Iproduct_pricelist> product_pricelists){
            
        }
        
        public void create(Iproduct_pricelist product_pricelist){
this.product_pricelistOdooClient.create(product_pricelist) ;
        }
        
        public void update(Iproduct_pricelist product_pricelist){
this.product_pricelistOdooClient.update(product_pricelist) ;
        }
        
        public Page<Iproduct_pricelist> select(SearchContext context){
            return null ;
        }
        

}

