package cn.ibizlab.odoo.client.odoo_product.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iproduct_price_history;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_price_history] 服务对象客户端接口
 */
public interface Iproduct_price_historyOdooClient {
    
        public Page<Iproduct_price_history> search(SearchContext context);

        public void updateBatch(Iproduct_price_history product_price_history);

        public void createBatch(Iproduct_price_history product_price_history);

        public void remove(Iproduct_price_history product_price_history);

        public void create(Iproduct_price_history product_price_history);

        public void get(Iproduct_price_history product_price_history);

        public void update(Iproduct_price_history product_price_history);

        public void removeBatch(Iproduct_price_history product_price_history);

        public List<Iproduct_price_history> select();


}