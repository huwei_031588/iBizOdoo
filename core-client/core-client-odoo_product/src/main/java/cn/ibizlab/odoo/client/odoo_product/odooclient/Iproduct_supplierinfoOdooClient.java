package cn.ibizlab.odoo.client.odoo_product.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iproduct_supplierinfo;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_supplierinfo] 服务对象客户端接口
 */
public interface Iproduct_supplierinfoOdooClient {
    
        public void createBatch(Iproduct_supplierinfo product_supplierinfo);

        public void update(Iproduct_supplierinfo product_supplierinfo);

        public void remove(Iproduct_supplierinfo product_supplierinfo);

        public void removeBatch(Iproduct_supplierinfo product_supplierinfo);

        public void updateBatch(Iproduct_supplierinfo product_supplierinfo);

        public Page<Iproduct_supplierinfo> search(SearchContext context);

        public void create(Iproduct_supplierinfo product_supplierinfo);

        public void get(Iproduct_supplierinfo product_supplierinfo);

        public List<Iproduct_supplierinfo> select();


}