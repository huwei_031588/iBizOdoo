package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_packaging;
import cn.ibizlab.odoo.core.client.service.Iproduct_packagingClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_packagingImpl;
import cn.ibizlab.odoo.client.odoo_product.odooclient.Iproduct_packagingOdooClient;
import cn.ibizlab.odoo.client.odoo_product.odooclient.impl.product_packagingOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[product_packaging] 服务对象接口
 */
@Service
public class product_packagingClientServiceImpl implements Iproduct_packagingClientService {
    @Autowired
    private  Iproduct_packagingOdooClient  product_packagingOdooClient;

    public Iproduct_packaging createModel() {		
		return new product_packagingImpl();
	}


        public void updateBatch(List<Iproduct_packaging> product_packagings){
            
        }
        
        public Page<Iproduct_packaging> search(SearchContext context){
            return this.product_packagingOdooClient.search(context) ;
        }
        
        public void createBatch(List<Iproduct_packaging> product_packagings){
            
        }
        
        public void get(Iproduct_packaging product_packaging){
            this.product_packagingOdooClient.get(product_packaging) ;
        }
        
        public void removeBatch(List<Iproduct_packaging> product_packagings){
            
        }
        
        public void create(Iproduct_packaging product_packaging){
this.product_packagingOdooClient.create(product_packaging) ;
        }
        
        public void remove(Iproduct_packaging product_packaging){
this.product_packagingOdooClient.remove(product_packaging) ;
        }
        
        public void update(Iproduct_packaging product_packaging){
this.product_packagingOdooClient.update(product_packaging) ;
        }
        
        public Page<Iproduct_packaging> select(SearchContext context){
            return null ;
        }
        

}

