package cn.ibizlab.odoo.client.odoo_product.odooclient.impl;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.sql.Timestamp;
import java.security.Principal;
import java.net.URL;
import java.util.*;

import cn.ibizlab.odoo.client.odoo_product.model.product_putawayImpl;
import cn.ibizlab.odoo.client.odoo_product.odooclient.Iproduct_putawayOdooClient;
import cn.ibizlab.odoo.core.client.model.Iproduct_putaway;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.util.helper.OdooSearchContextHelper;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;
import cn.ibizlab.odoo.util.config.OdooClientProperties;

import com.google.common.collect.Lists;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageImpl;

@Component
public class product_putawayOdooClient implements Iproduct_putawayOdooClient {

	@Autowired
	OdooClientProperties odooClientProperties;

	public static String URL ;
	public static String DB ;
	public static Integer USERID  ;
	public static String PASS ;

	public static XmlRpcClient client ;

	public product_putawayOdooClient(){
		
	}

	public void initClient () {
		if(client == null) {
			URL = odooClientProperties.getUrl();
			DB = odooClientProperties.getDb();
			USERID = odooClientProperties.getUserId();
			PASS = odooClientProperties.getPass();
			XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
			config.setEnabledForExtensions(true);
			client = new XmlRpcClient();
			try{
				config.setServerURL(new URL(String.format("%s/xmlrpc/2/object", URL)));
			}catch(Exception e){
				System.out.println(e.getMessage());
			}
			client.setConfig(config);
		}
	}

	public void createBatch(Iproduct_putaway product_putaway){
		initClient();
		try{
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void create(Iproduct_putaway product_putaway){
		initClient();
		try{
			Map map = product_putaway.toMap();
			Integer id = (Integer) client.execute(
				"execute_kw", Arrays.asList(DB, USERID, PASS, "product.putaway", "create",Arrays.asList(map)));
			List<Object> maps = Arrays.asList((Object[]) client.execute(
				"execute_kw", Arrays.asList(DB, USERID, PASS, "product.putaway", "search_read",
					Arrays.asList(Arrays.asList(
								 Arrays.asList("id", "=", id)
					)),
					new HashMap() {
						{
								put("limit",1);
						}
			})));
			if (maps != null && maps.size() > 0) {
				product_putaway.fromMap((Map<String, Object>)maps.get(0));
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void update(Iproduct_putaway product_putaway){
		initClient();
		try{
			Integer id =product_putaway.getId();
			Map map = product_putaway.toMap();
			client.execute(
				"execute_kw", Arrays.asList(DB, USERID, PASS, "product.putaway", "write",Arrays.asList(Arrays.asList(id),map)));
			List<Object> maps = Arrays.asList((Object[]) client.execute(
				"execute_kw", Arrays.asList(DB, USERID, PASS, "product.putaway", "search_read",
					Arrays.asList(Arrays.asList(
								 Arrays.asList("id", "=", id)
					)),
					new HashMap() {
						{
								put("limit",1);
						}
			})));
			if (maps != null && maps.size() > 0) {
				product_putaway.fromMap((Map<String, Object>)maps.get(0));
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

    public Page<Iproduct_putaway> search(SearchContext context) {
		initClient();
		List<Iproduct_putaway> product_putawayList = new ArrayList<>();
		long totalnum = 0;
		try{
			totalnum = (Integer)client.execute(
				"execute_kw", Arrays.asList(
   						DB, USERID, PASS, "product.putaway", "search_count",
    					OdooSearchContextHelper.getConditions(context)
			));
			if(totalnum<1)
				return new PageImpl(product_putawayList,context.getPageable(),totalnum);
			List<Object> maps = Arrays.asList((Object[]) client.execute(	
				"execute_kw", Arrays.asList(
						DB, USERID, PASS, "product.putaway", "search_read",
						OdooSearchContextHelper.getConditions(context),
						new HashMap() {
							{				
								put("order",OdooSearchContextHelper.getSorts(context));	
								put("offset",(int)context.getPageable().getOffset());
								put("limit", context.getPageable().getPageSize());
							}
			})));
			product_putawayList = mapsToObjects(maps);
		}catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		
		return new PageImpl(product_putawayList,context.getPageable(),totalnum);
	}
	public void removeBatch(Iproduct_putaway product_putaway){
		initClient();
		try{
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void remove(Iproduct_putaway product_putaway){
		initClient();
		try{
			Integer id = product_putaway.getId();
			client.execute("execute_kw", Arrays.asList(
				DB, USERID, PASS,
				"product.putaway", "unlink",
				Arrays.asList(Arrays.asList(id))));
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void updateBatch(Iproduct_putaway product_putaway){
		initClient();
		try{
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void get(Iproduct_putaway product_putaway){
		initClient();
		try{
		 	List<Object> maps = Arrays.asList((Object[]) client.execute(
				"execute_kw", Arrays.asList(DB, USERID, PASS, "product.putaway", "search_read",
					Arrays.asList(Arrays.asList(
								 Arrays.asList("id", "=", product_putaway.getId())
					)),
					new HashMap() {
						{
								put("limit",1);
						}
			})));
			if (maps != null && maps.size() > 0) {
				product_putaway.fromMap((Map<String, Object>)maps.get(0));
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

    public List<Iproduct_putaway> select() {
			return null;
	}




	private List<Iproduct_putaway> mapsToObjects(List<Object> maps) throws Exception {
		List<Iproduct_putaway> list = Lists.newArrayList();
		if (maps != null && maps.size() > 0) {
			Map<String, Object> map = null;
			for (int i = 0,size = maps.size(); i < size; i++) {
				map = (Map<String, Object>)maps.get(i);
				Iproduct_putaway product_putaway = new product_putawayImpl();
				product_putaway.fromMap(map);
				list.add(product_putaway);
			}
		}
		return list;
	}

	public void preAction (Principal principal,OdooClientHelper.CurOdooUser curUser) {
		String curUserName = principal.getName();
		int curUserId = 0;
		String dyncPassWord = OdooClientHelper.getCurUserdynaPass(curUserName,PASS);
		try{
			final XmlRpcClientConfigImpl common_config = new XmlRpcClientConfigImpl();
			common_config.setServerURL(
				new URL(String.format("%s/xmlrpc/2/common", URL)));
			//获取用户id
			curUserId = (int)client.execute(
				common_config, "authenticate", Arrays.asList(
						DB, curUserName, dyncPassWord, new HashMap()));
		}catch(Exception e){
			System.out.println("用户校验失败，可能无用户或密码错误"+ e.getMessage());
		}

		if(curUserId == 0) {
			try {
				//修改动态密码
				client.execute("execute_kw", Arrays.asList(
						DB, USERID, PASS,
						"res.users", "write",
						Arrays.asList(Arrays.asList(6),
							new HashMap() {{
								put("name", curUserName);
								put("new_password", dyncPassWord);
							}}
						)
				));
			} catch (Exception e) {
				System.out.println("用户密码重置，可能无用户" + e.getMessage());
			}
		}
		curUser.setUserId(curUserId);
		curUser.setPass(dyncPassWord);
	}

}
