package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_public_category;
import cn.ibizlab.odoo.core.client.service.Iproduct_public_categoryClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_public_categoryImpl;
import cn.ibizlab.odoo.client.odoo_product.odooclient.Iproduct_public_categoryOdooClient;
import cn.ibizlab.odoo.client.odoo_product.odooclient.impl.product_public_categoryOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[product_public_category] 服务对象接口
 */
@Service
public class product_public_categoryClientServiceImpl implements Iproduct_public_categoryClientService {
    @Autowired
    private  Iproduct_public_categoryOdooClient  product_public_categoryOdooClient;

    public Iproduct_public_category createModel() {		
		return new product_public_categoryImpl();
	}


        public void get(Iproduct_public_category product_public_category){
            this.product_public_categoryOdooClient.get(product_public_category) ;
        }
        
        public void create(Iproduct_public_category product_public_category){
this.product_public_categoryOdooClient.create(product_public_category) ;
        }
        
        public void removeBatch(List<Iproduct_public_category> product_public_categories){
            
        }
        
        public void createBatch(List<Iproduct_public_category> product_public_categories){
            
        }
        
        public void update(Iproduct_public_category product_public_category){
this.product_public_categoryOdooClient.update(product_public_category) ;
        }
        
        public void updateBatch(List<Iproduct_public_category> product_public_categories){
            
        }
        
        public Page<Iproduct_public_category> search(SearchContext context){
            return this.product_public_categoryOdooClient.search(context) ;
        }
        
        public void remove(Iproduct_public_category product_public_category){
this.product_public_categoryOdooClient.remove(product_public_category) ;
        }
        
        public Page<Iproduct_public_category> select(SearchContext context){
            return null ;
        }
        

}

