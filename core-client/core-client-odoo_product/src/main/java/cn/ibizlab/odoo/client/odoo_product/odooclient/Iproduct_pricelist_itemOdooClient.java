package cn.ibizlab.odoo.client.odoo_product.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iproduct_pricelist_item;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_pricelist_item] 服务对象客户端接口
 */
public interface Iproduct_pricelist_itemOdooClient {
    
        public void removeBatch(Iproduct_pricelist_item product_pricelist_item);

        public void remove(Iproduct_pricelist_item product_pricelist_item);

        public void update(Iproduct_pricelist_item product_pricelist_item);

        public void get(Iproduct_pricelist_item product_pricelist_item);

        public void create(Iproduct_pricelist_item product_pricelist_item);

        public void updateBatch(Iproduct_pricelist_item product_pricelist_item);

        public Page<Iproduct_pricelist_item> search(SearchContext context);

        public void createBatch(Iproduct_pricelist_item product_pricelist_item);

        public List<Iproduct_pricelist_item> select();


}