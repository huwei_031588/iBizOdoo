package cn.ibizlab.odoo.client.odoo_product.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iproduct_removal;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_removal] 服务对象客户端接口
 */
public interface Iproduct_removalOdooClient {
    
        public void remove(Iproduct_removal product_removal);

        public void removeBatch(Iproduct_removal product_removal);

        public void createBatch(Iproduct_removal product_removal);

        public Page<Iproduct_removal> search(SearchContext context);

        public void create(Iproduct_removal product_removal);

        public void updateBatch(Iproduct_removal product_removal);

        public void get(Iproduct_removal product_removal);

        public void update(Iproduct_removal product_removal);

        public List<Iproduct_removal> select();


}