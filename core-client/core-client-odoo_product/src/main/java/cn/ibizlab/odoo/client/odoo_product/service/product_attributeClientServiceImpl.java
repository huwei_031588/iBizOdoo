package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_attribute;
import cn.ibizlab.odoo.core.client.service.Iproduct_attributeClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_attributeImpl;
import cn.ibizlab.odoo.client.odoo_product.odooclient.Iproduct_attributeOdooClient;
import cn.ibizlab.odoo.client.odoo_product.odooclient.impl.product_attributeOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[product_attribute] 服务对象接口
 */
@Service
public class product_attributeClientServiceImpl implements Iproduct_attributeClientService {
    @Autowired
    private  Iproduct_attributeOdooClient  product_attributeOdooClient;

    public Iproduct_attribute createModel() {		
		return new product_attributeImpl();
	}


        public void get(Iproduct_attribute product_attribute){
            this.product_attributeOdooClient.get(product_attribute) ;
        }
        
        public void updateBatch(List<Iproduct_attribute> product_attributes){
            
        }
        
        public void remove(Iproduct_attribute product_attribute){
this.product_attributeOdooClient.remove(product_attribute) ;
        }
        
        public void update(Iproduct_attribute product_attribute){
this.product_attributeOdooClient.update(product_attribute) ;
        }
        
        public Page<Iproduct_attribute> search(SearchContext context){
            return this.product_attributeOdooClient.search(context) ;
        }
        
        public void createBatch(List<Iproduct_attribute> product_attributes){
            
        }
        
        public void create(Iproduct_attribute product_attribute){
this.product_attributeOdooClient.create(product_attribute) ;
        }
        
        public void removeBatch(List<Iproduct_attribute> product_attributes){
            
        }
        
        public Page<Iproduct_attribute> select(SearchContext context){
            return null ;
        }
        

}

