package cn.ibizlab.odoo.client.odoo_product.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iproduct_public_category;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_public_category] 服务对象客户端接口
 */
public interface Iproduct_public_categoryOdooClient {
    
        public void get(Iproduct_public_category product_public_category);

        public void create(Iproduct_public_category product_public_category);

        public void removeBatch(Iproduct_public_category product_public_category);

        public void createBatch(Iproduct_public_category product_public_category);

        public void update(Iproduct_public_category product_public_category);

        public void updateBatch(Iproduct_public_category product_public_category);

        public Page<Iproduct_public_category> search(SearchContext context);

        public void remove(Iproduct_public_category product_public_category);

        public List<Iproduct_public_category> select();


}