package cn.ibizlab.odoo.client.odoo_product.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iproduct_template;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_template] 服务对象客户端接口
 */
public interface Iproduct_templateOdooClient {
    
        public void remove(Iproduct_template product_template);

        public void create(Iproduct_template product_template);

        public void update(Iproduct_template product_template);

        public void removeBatch(Iproduct_template product_template);

        public void updateBatch(Iproduct_template product_template);

        public void get(Iproduct_template product_template);

        public Page<Iproduct_template> search(SearchContext context);

        public void createBatch(Iproduct_template product_template);

        public List<Iproduct_template> select();


}