package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_product;
import cn.ibizlab.odoo.core.client.service.Iproduct_productClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_productImpl;
import cn.ibizlab.odoo.client.odoo_product.odooclient.Iproduct_productOdooClient;
import cn.ibizlab.odoo.client.odoo_product.odooclient.impl.product_productOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[product_product] 服务对象接口
 */
@Service
public class product_productClientServiceImpl implements Iproduct_productClientService {
    @Autowired
    private  Iproduct_productOdooClient  product_productOdooClient;

    public Iproduct_product createModel() {		
		return new product_productImpl();
	}


        public void create(Iproduct_product product_product){
this.product_productOdooClient.create(product_product) ;
        }
        
        public void get(Iproduct_product product_product){
            this.product_productOdooClient.get(product_product) ;
        }
        
        public Page<Iproduct_product> search(SearchContext context){
            return this.product_productOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Iproduct_product> product_products){
            
        }
        
        public void createBatch(List<Iproduct_product> product_products){
            
        }
        
        public void remove(Iproduct_product product_product){
this.product_productOdooClient.remove(product_product) ;
        }
        
        public void updateBatch(List<Iproduct_product> product_products){
            
        }
        
        public void update(Iproduct_product product_product){
this.product_productOdooClient.update(product_product) ;
        }
        
        public Page<Iproduct_product> select(SearchContext context){
            return null ;
        }
        

}

