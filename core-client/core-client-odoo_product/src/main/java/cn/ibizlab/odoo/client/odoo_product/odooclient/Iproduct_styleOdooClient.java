package cn.ibizlab.odoo.client.odoo_product.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iproduct_style;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_style] 服务对象客户端接口
 */
public interface Iproduct_styleOdooClient {
    
        public Page<Iproduct_style> search(SearchContext context);

        public void update(Iproduct_style product_style);

        public void createBatch(Iproduct_style product_style);

        public void create(Iproduct_style product_style);

        public void remove(Iproduct_style product_style);

        public void removeBatch(Iproduct_style product_style);

        public void get(Iproduct_style product_style);

        public void updateBatch(Iproduct_style product_style);

        public List<Iproduct_style> select();


}