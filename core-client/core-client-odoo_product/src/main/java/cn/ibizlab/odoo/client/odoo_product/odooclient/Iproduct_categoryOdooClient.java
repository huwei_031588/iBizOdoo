package cn.ibizlab.odoo.client.odoo_product.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iproduct_category;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_category] 服务对象客户端接口
 */
public interface Iproduct_categoryOdooClient {
    
        public void update(Iproduct_category product_category);

        public void removeBatch(Iproduct_category product_category);

        public void create(Iproduct_category product_category);

        public void createBatch(Iproduct_category product_category);

        public void remove(Iproduct_category product_category);

        public void get(Iproduct_category product_category);

        public Page<Iproduct_category> search(SearchContext context);

        public void updateBatch(Iproduct_category product_category);

        public List<Iproduct_category> select();


}