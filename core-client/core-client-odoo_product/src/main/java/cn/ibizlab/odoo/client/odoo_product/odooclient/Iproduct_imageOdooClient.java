package cn.ibizlab.odoo.client.odoo_product.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iproduct_image;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_image] 服务对象客户端接口
 */
public interface Iproduct_imageOdooClient {
    
        public void create(Iproduct_image product_image);

        public void remove(Iproduct_image product_image);

        public void update(Iproduct_image product_image);

        public Page<Iproduct_image> search(SearchContext context);

        public void updateBatch(Iproduct_image product_image);

        public void removeBatch(Iproduct_image product_image);

        public void get(Iproduct_image product_image);

        public void createBatch(Iproduct_image product_image);

        public List<Iproduct_image> select();


}