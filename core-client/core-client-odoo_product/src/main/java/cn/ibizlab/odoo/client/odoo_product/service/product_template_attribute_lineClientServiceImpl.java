package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_template_attribute_line;
import cn.ibizlab.odoo.core.client.service.Iproduct_template_attribute_lineClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_template_attribute_lineImpl;
import cn.ibizlab.odoo.client.odoo_product.odooclient.Iproduct_template_attribute_lineOdooClient;
import cn.ibizlab.odoo.client.odoo_product.odooclient.impl.product_template_attribute_lineOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[product_template_attribute_line] 服务对象接口
 */
@Service
public class product_template_attribute_lineClientServiceImpl implements Iproduct_template_attribute_lineClientService {
    @Autowired
    private  Iproduct_template_attribute_lineOdooClient  product_template_attribute_lineOdooClient;

    public Iproduct_template_attribute_line createModel() {		
		return new product_template_attribute_lineImpl();
	}


        public void removeBatch(List<Iproduct_template_attribute_line> product_template_attribute_lines){
            
        }
        
        public void createBatch(List<Iproduct_template_attribute_line> product_template_attribute_lines){
            
        }
        
        public Page<Iproduct_template_attribute_line> search(SearchContext context){
            return this.product_template_attribute_lineOdooClient.search(context) ;
        }
        
        public void update(Iproduct_template_attribute_line product_template_attribute_line){
this.product_template_attribute_lineOdooClient.update(product_template_attribute_line) ;
        }
        
        public void remove(Iproduct_template_attribute_line product_template_attribute_line){
this.product_template_attribute_lineOdooClient.remove(product_template_attribute_line) ;
        }
        
        public void create(Iproduct_template_attribute_line product_template_attribute_line){
this.product_template_attribute_lineOdooClient.create(product_template_attribute_line) ;
        }
        
        public void updateBatch(List<Iproduct_template_attribute_line> product_template_attribute_lines){
            
        }
        
        public void get(Iproduct_template_attribute_line product_template_attribute_line){
            this.product_template_attribute_lineOdooClient.get(product_template_attribute_line) ;
        }
        
        public Page<Iproduct_template_attribute_line> select(SearchContext context){
            return null ;
        }
        

}

