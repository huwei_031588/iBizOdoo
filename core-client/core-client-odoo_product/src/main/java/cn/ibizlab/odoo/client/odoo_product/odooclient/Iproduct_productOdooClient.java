package cn.ibizlab.odoo.client.odoo_product.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iproduct_product;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_product] 服务对象客户端接口
 */
public interface Iproduct_productOdooClient {
    
        public void create(Iproduct_product product_product);

        public void get(Iproduct_product product_product);

        public Page<Iproduct_product> search(SearchContext context);

        public void removeBatch(Iproduct_product product_product);

        public void createBatch(Iproduct_product product_product);

        public void remove(Iproduct_product product_product);

        public void updateBatch(Iproduct_product product_product);

        public void update(Iproduct_product product_product);

        public List<Iproduct_product> select();


}