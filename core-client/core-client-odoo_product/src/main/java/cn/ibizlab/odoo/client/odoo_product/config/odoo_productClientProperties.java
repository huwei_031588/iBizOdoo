package cn.ibizlab.odoo.client.odoo_product.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.product")
@Data
public class odoo_productClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
