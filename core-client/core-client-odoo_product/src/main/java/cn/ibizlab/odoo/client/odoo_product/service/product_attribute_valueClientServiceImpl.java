package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_attribute_value;
import cn.ibizlab.odoo.core.client.service.Iproduct_attribute_valueClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_attribute_valueImpl;
import cn.ibizlab.odoo.client.odoo_product.odooclient.Iproduct_attribute_valueOdooClient;
import cn.ibizlab.odoo.client.odoo_product.odooclient.impl.product_attribute_valueOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[product_attribute_value] 服务对象接口
 */
@Service
public class product_attribute_valueClientServiceImpl implements Iproduct_attribute_valueClientService {
    @Autowired
    private  Iproduct_attribute_valueOdooClient  product_attribute_valueOdooClient;

    public Iproduct_attribute_value createModel() {		
		return new product_attribute_valueImpl();
	}


        public void update(Iproduct_attribute_value product_attribute_value){
this.product_attribute_valueOdooClient.update(product_attribute_value) ;
        }
        
        public void removeBatch(List<Iproduct_attribute_value> product_attribute_values){
            
        }
        
        public void updateBatch(List<Iproduct_attribute_value> product_attribute_values){
            
        }
        
        public void remove(Iproduct_attribute_value product_attribute_value){
this.product_attribute_valueOdooClient.remove(product_attribute_value) ;
        }
        
        public void createBatch(List<Iproduct_attribute_value> product_attribute_values){
            
        }
        
        public Page<Iproduct_attribute_value> search(SearchContext context){
            return this.product_attribute_valueOdooClient.search(context) ;
        }
        
        public void get(Iproduct_attribute_value product_attribute_value){
            this.product_attribute_valueOdooClient.get(product_attribute_value) ;
        }
        
        public void create(Iproduct_attribute_value product_attribute_value){
this.product_attribute_valueOdooClient.create(product_attribute_value) ;
        }
        
        public Page<Iproduct_attribute_value> select(SearchContext context){
            return null ;
        }
        

}

