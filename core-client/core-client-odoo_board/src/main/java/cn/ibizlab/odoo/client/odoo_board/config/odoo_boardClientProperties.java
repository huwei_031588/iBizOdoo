package cn.ibizlab.odoo.client.odoo_board.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.board")
@Data
public class odoo_boardClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
