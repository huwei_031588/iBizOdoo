package cn.ibizlab.odoo.client.odoo_board.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iboard_board;
import cn.ibizlab.odoo.core.client.service.Iboard_boardClientService;
import cn.ibizlab.odoo.client.odoo_board.model.board_boardImpl;
import cn.ibizlab.odoo.client.odoo_board.odooclient.Iboard_boardOdooClient;
import cn.ibizlab.odoo.client.odoo_board.odooclient.impl.board_boardOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[board_board] 服务对象接口
 */
@Service
public class board_boardClientServiceImpl implements Iboard_boardClientService {
    @Autowired
    private  Iboard_boardOdooClient  board_boardOdooClient;

    public Iboard_board createModel() {		
		return new board_boardImpl();
	}


        public void removeBatch(List<Iboard_board> board_boards){
            
        }
        
        public void remove(Iboard_board board_board){
this.board_boardOdooClient.remove(board_board) ;
        }
        
        public void create(Iboard_board board_board){
this.board_boardOdooClient.create(board_board) ;
        }
        
        public void update(Iboard_board board_board){
this.board_boardOdooClient.update(board_board) ;
        }
        
        public Page<Iboard_board> search(SearchContext context){
            return this.board_boardOdooClient.search(context) ;
        }
        
        public void createBatch(List<Iboard_board> board_boards){
            
        }
        
        public void get(Iboard_board board_board){
            this.board_boardOdooClient.get(board_board) ;
        }
        
        public void updateBatch(List<Iboard_board> board_boards){
            
        }
        
        public Page<Iboard_board> select(SearchContext context){
            return null ;
        }
        

}

