package cn.ibizlab.odoo.client.odoo_note.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Inote_note;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[note_note] 服务对象客户端接口
 */
public interface Inote_noteOdooClient {
    
        public void updateBatch(Inote_note note_note);

        public void update(Inote_note note_note);

        public void get(Inote_note note_note);

        public void createBatch(Inote_note note_note);

        public void remove(Inote_note note_note);

        public Page<Inote_note> search(SearchContext context);

        public void removeBatch(Inote_note note_note);

        public void create(Inote_note note_note);

        public List<Inote_note> select();


}