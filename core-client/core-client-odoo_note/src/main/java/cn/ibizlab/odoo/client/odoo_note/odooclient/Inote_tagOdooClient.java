package cn.ibizlab.odoo.client.odoo_note.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Inote_tag;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[note_tag] 服务对象客户端接口
 */
public interface Inote_tagOdooClient {
    
        public void removeBatch(Inote_tag note_tag);

        public Page<Inote_tag> search(SearchContext context);

        public void get(Inote_tag note_tag);

        public void create(Inote_tag note_tag);

        public void remove(Inote_tag note_tag);

        public void createBatch(Inote_tag note_tag);

        public void updateBatch(Inote_tag note_tag);

        public void update(Inote_tag note_tag);

        public List<Inote_tag> select();


}