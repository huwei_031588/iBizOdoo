package cn.ibizlab.odoo.client.odoo_note.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Inote_note;
import cn.ibizlab.odoo.core.client.service.Inote_noteClientService;
import cn.ibizlab.odoo.client.odoo_note.model.note_noteImpl;
import cn.ibizlab.odoo.client.odoo_note.odooclient.Inote_noteOdooClient;
import cn.ibizlab.odoo.client.odoo_note.odooclient.impl.note_noteOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[note_note] 服务对象接口
 */
@Service
public class note_noteClientServiceImpl implements Inote_noteClientService {
    @Autowired
    private  Inote_noteOdooClient  note_noteOdooClient;

    public Inote_note createModel() {		
		return new note_noteImpl();
	}


        public void updateBatch(List<Inote_note> note_notes){
            
        }
        
        public void update(Inote_note note_note){
this.note_noteOdooClient.update(note_note) ;
        }
        
        public void get(Inote_note note_note){
            this.note_noteOdooClient.get(note_note) ;
        }
        
        public void createBatch(List<Inote_note> note_notes){
            
        }
        
        public void remove(Inote_note note_note){
this.note_noteOdooClient.remove(note_note) ;
        }
        
        public Page<Inote_note> search(SearchContext context){
            return this.note_noteOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Inote_note> note_notes){
            
        }
        
        public void create(Inote_note note_note){
this.note_noteOdooClient.create(note_note) ;
        }
        
        public Page<Inote_note> select(SearchContext context){
            return null ;
        }
        

}

