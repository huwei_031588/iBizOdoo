package cn.ibizlab.odoo.client.odoo_note.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Inote_stage;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[note_stage] 服务对象客户端接口
 */
public interface Inote_stageOdooClient {
    
        public void removeBatch(Inote_stage note_stage);

        public void get(Inote_stage note_stage);

        public void update(Inote_stage note_stage);

        public void create(Inote_stage note_stage);

        public void remove(Inote_stage note_stage);

        public void updateBatch(Inote_stage note_stage);

        public void createBatch(Inote_stage note_stage);

        public Page<Inote_stage> search(SearchContext context);

        public List<Inote_stage> select();


}