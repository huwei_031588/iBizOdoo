package cn.ibizlab.odoo.client.odoo_note.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.note")
@Data
public class odoo_noteClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
