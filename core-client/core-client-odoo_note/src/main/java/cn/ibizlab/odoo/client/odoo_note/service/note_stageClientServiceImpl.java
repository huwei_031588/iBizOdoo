package cn.ibizlab.odoo.client.odoo_note.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Inote_stage;
import cn.ibizlab.odoo.core.client.service.Inote_stageClientService;
import cn.ibizlab.odoo.client.odoo_note.model.note_stageImpl;
import cn.ibizlab.odoo.client.odoo_note.odooclient.Inote_stageOdooClient;
import cn.ibizlab.odoo.client.odoo_note.odooclient.impl.note_stageOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[note_stage] 服务对象接口
 */
@Service
public class note_stageClientServiceImpl implements Inote_stageClientService {
    @Autowired
    private  Inote_stageOdooClient  note_stageOdooClient;

    public Inote_stage createModel() {		
		return new note_stageImpl();
	}


        public void removeBatch(List<Inote_stage> note_stages){
            
        }
        
        public void get(Inote_stage note_stage){
            this.note_stageOdooClient.get(note_stage) ;
        }
        
        public void update(Inote_stage note_stage){
this.note_stageOdooClient.update(note_stage) ;
        }
        
        public void create(Inote_stage note_stage){
this.note_stageOdooClient.create(note_stage) ;
        }
        
        public void remove(Inote_stage note_stage){
this.note_stageOdooClient.remove(note_stage) ;
        }
        
        public void updateBatch(List<Inote_stage> note_stages){
            
        }
        
        public void createBatch(List<Inote_stage> note_stages){
            
        }
        
        public Page<Inote_stage> search(SearchContext context){
            return this.note_stageOdooClient.search(context) ;
        }
        
        public Page<Inote_stage> select(SearchContext context){
            return null ;
        }
        

}

