package cn.ibizlab.odoo.client.odoo_barcodes.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ibarcodes_barcode_events_mixin;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[barcodes_barcode_events_mixin] 服务对象客户端接口
 */
public interface Ibarcodes_barcode_events_mixinOdooClient {
    
        public void updateBatch(Ibarcodes_barcode_events_mixin barcodes_barcode_events_mixin);

        public void create(Ibarcodes_barcode_events_mixin barcodes_barcode_events_mixin);

        public void update(Ibarcodes_barcode_events_mixin barcodes_barcode_events_mixin);

        public void remove(Ibarcodes_barcode_events_mixin barcodes_barcode_events_mixin);

        public Page<Ibarcodes_barcode_events_mixin> search(SearchContext context);

        public void get(Ibarcodes_barcode_events_mixin barcodes_barcode_events_mixin);

        public void removeBatch(Ibarcodes_barcode_events_mixin barcodes_barcode_events_mixin);

        public void createBatch(Ibarcodes_barcode_events_mixin barcodes_barcode_events_mixin);

        public List<Ibarcodes_barcode_events_mixin> select();


}