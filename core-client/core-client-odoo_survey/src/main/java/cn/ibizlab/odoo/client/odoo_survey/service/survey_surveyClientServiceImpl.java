package cn.ibizlab.odoo.client.odoo_survey.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isurvey_survey;
import cn.ibizlab.odoo.core.client.service.Isurvey_surveyClientService;
import cn.ibizlab.odoo.client.odoo_survey.model.survey_surveyImpl;
import cn.ibizlab.odoo.client.odoo_survey.odooclient.Isurvey_surveyOdooClient;
import cn.ibizlab.odoo.client.odoo_survey.odooclient.impl.survey_surveyOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[survey_survey] 服务对象接口
 */
@Service
public class survey_surveyClientServiceImpl implements Isurvey_surveyClientService {
    @Autowired
    private  Isurvey_surveyOdooClient  survey_surveyOdooClient;

    public Isurvey_survey createModel() {		
		return new survey_surveyImpl();
	}


        public void create(Isurvey_survey survey_survey){
this.survey_surveyOdooClient.create(survey_survey) ;
        }
        
        public Page<Isurvey_survey> search(SearchContext context){
            return this.survey_surveyOdooClient.search(context) ;
        }
        
        public void remove(Isurvey_survey survey_survey){
this.survey_surveyOdooClient.remove(survey_survey) ;
        }
        
        public void createBatch(List<Isurvey_survey> survey_surveys){
            
        }
        
        public void update(Isurvey_survey survey_survey){
this.survey_surveyOdooClient.update(survey_survey) ;
        }
        
        public void updateBatch(List<Isurvey_survey> survey_surveys){
            
        }
        
        public void removeBatch(List<Isurvey_survey> survey_surveys){
            
        }
        
        public void get(Isurvey_survey survey_survey){
            this.survey_surveyOdooClient.get(survey_survey) ;
        }
        
        public Page<Isurvey_survey> select(SearchContext context){
            return null ;
        }
        

}

