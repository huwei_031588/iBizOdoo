package cn.ibizlab.odoo.client.odoo_survey.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isurvey_user_input_line;
import cn.ibizlab.odoo.core.client.service.Isurvey_user_input_lineClientService;
import cn.ibizlab.odoo.client.odoo_survey.model.survey_user_input_lineImpl;
import cn.ibizlab.odoo.client.odoo_survey.odooclient.Isurvey_user_input_lineOdooClient;
import cn.ibizlab.odoo.client.odoo_survey.odooclient.impl.survey_user_input_lineOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[survey_user_input_line] 服务对象接口
 */
@Service
public class survey_user_input_lineClientServiceImpl implements Isurvey_user_input_lineClientService {
    @Autowired
    private  Isurvey_user_input_lineOdooClient  survey_user_input_lineOdooClient;

    public Isurvey_user_input_line createModel() {		
		return new survey_user_input_lineImpl();
	}


        public void remove(Isurvey_user_input_line survey_user_input_line){
this.survey_user_input_lineOdooClient.remove(survey_user_input_line) ;
        }
        
        public void updateBatch(List<Isurvey_user_input_line> survey_user_input_lines){
            
        }
        
        public void get(Isurvey_user_input_line survey_user_input_line){
            this.survey_user_input_lineOdooClient.get(survey_user_input_line) ;
        }
        
        public void update(Isurvey_user_input_line survey_user_input_line){
this.survey_user_input_lineOdooClient.update(survey_user_input_line) ;
        }
        
        public void createBatch(List<Isurvey_user_input_line> survey_user_input_lines){
            
        }
        
        public void create(Isurvey_user_input_line survey_user_input_line){
this.survey_user_input_lineOdooClient.create(survey_user_input_line) ;
        }
        
        public Page<Isurvey_user_input_line> search(SearchContext context){
            return this.survey_user_input_lineOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Isurvey_user_input_line> survey_user_input_lines){
            
        }
        
        public Page<Isurvey_user_input_line> select(SearchContext context){
            return null ;
        }
        

}

