package cn.ibizlab.odoo.client.odoo_survey.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Isurvey_label;
import cn.ibizlab.odoo.client.odoo_survey.model.survey_labelImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[survey_label] 服务对象接口
 */
public interface survey_labelFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_labels/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_labels/updatebatch")
    public survey_labelImpl updateBatch(@RequestBody List<survey_labelImpl> survey_labels);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_labels/{id}")
    public survey_labelImpl update(@PathVariable("id") Integer id,@RequestBody survey_labelImpl survey_label);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_labels/{id}")
    public survey_labelImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_labels/removebatch")
    public survey_labelImpl removeBatch(@RequestBody List<survey_labelImpl> survey_labels);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_labels/createbatch")
    public survey_labelImpl createBatch(@RequestBody List<survey_labelImpl> survey_labels);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_labels/search")
    public Page<survey_labelImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_labels")
    public survey_labelImpl create(@RequestBody survey_labelImpl survey_label);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_labels/select")
    public Page<survey_labelImpl> select();



}
