package cn.ibizlab.odoo.client.odoo_survey.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isurvey_mail_compose_message;
import cn.ibizlab.odoo.core.client.service.Isurvey_mail_compose_messageClientService;
import cn.ibizlab.odoo.client.odoo_survey.model.survey_mail_compose_messageImpl;
import cn.ibizlab.odoo.client.odoo_survey.odooclient.Isurvey_mail_compose_messageOdooClient;
import cn.ibizlab.odoo.client.odoo_survey.odooclient.impl.survey_mail_compose_messageOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[survey_mail_compose_message] 服务对象接口
 */
@Service
public class survey_mail_compose_messageClientServiceImpl implements Isurvey_mail_compose_messageClientService {
    @Autowired
    private  Isurvey_mail_compose_messageOdooClient  survey_mail_compose_messageOdooClient;

    public Isurvey_mail_compose_message createModel() {		
		return new survey_mail_compose_messageImpl();
	}


        public void remove(Isurvey_mail_compose_message survey_mail_compose_message){
this.survey_mail_compose_messageOdooClient.remove(survey_mail_compose_message) ;
        }
        
        public void createBatch(List<Isurvey_mail_compose_message> survey_mail_compose_messages){
            
        }
        
        public void update(Isurvey_mail_compose_message survey_mail_compose_message){
this.survey_mail_compose_messageOdooClient.update(survey_mail_compose_message) ;
        }
        
        public void get(Isurvey_mail_compose_message survey_mail_compose_message){
            this.survey_mail_compose_messageOdooClient.get(survey_mail_compose_message) ;
        }
        
        public void removeBatch(List<Isurvey_mail_compose_message> survey_mail_compose_messages){
            
        }
        
        public void updateBatch(List<Isurvey_mail_compose_message> survey_mail_compose_messages){
            
        }
        
        public void create(Isurvey_mail_compose_message survey_mail_compose_message){
this.survey_mail_compose_messageOdooClient.create(survey_mail_compose_message) ;
        }
        
        public Page<Isurvey_mail_compose_message> search(SearchContext context){
            return this.survey_mail_compose_messageOdooClient.search(context) ;
        }
        
        public Page<Isurvey_mail_compose_message> select(SearchContext context){
            return null ;
        }
        

}

