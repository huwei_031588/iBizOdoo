package cn.ibizlab.odoo.client.odoo_survey.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isurvey_stage;
import cn.ibizlab.odoo.core.client.service.Isurvey_stageClientService;
import cn.ibizlab.odoo.client.odoo_survey.model.survey_stageImpl;
import cn.ibizlab.odoo.client.odoo_survey.odooclient.Isurvey_stageOdooClient;
import cn.ibizlab.odoo.client.odoo_survey.odooclient.impl.survey_stageOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[survey_stage] 服务对象接口
 */
@Service
public class survey_stageClientServiceImpl implements Isurvey_stageClientService {
    @Autowired
    private  Isurvey_stageOdooClient  survey_stageOdooClient;

    public Isurvey_stage createModel() {		
		return new survey_stageImpl();
	}


        public Page<Isurvey_stage> search(SearchContext context){
            return this.survey_stageOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Isurvey_stage> survey_stages){
            
        }
        
        public void removeBatch(List<Isurvey_stage> survey_stages){
            
        }
        
        public void create(Isurvey_stage survey_stage){
this.survey_stageOdooClient.create(survey_stage) ;
        }
        
        public void remove(Isurvey_stage survey_stage){
this.survey_stageOdooClient.remove(survey_stage) ;
        }
        
        public void createBatch(List<Isurvey_stage> survey_stages){
            
        }
        
        public void update(Isurvey_stage survey_stage){
this.survey_stageOdooClient.update(survey_stage) ;
        }
        
        public void get(Isurvey_stage survey_stage){
            this.survey_stageOdooClient.get(survey_stage) ;
        }
        
        public Page<Isurvey_stage> select(SearchContext context){
            return null ;
        }
        

}

