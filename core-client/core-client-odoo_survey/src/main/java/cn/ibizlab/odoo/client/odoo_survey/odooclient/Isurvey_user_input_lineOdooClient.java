package cn.ibizlab.odoo.client.odoo_survey.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Isurvey_user_input_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[survey_user_input_line] 服务对象客户端接口
 */
public interface Isurvey_user_input_lineOdooClient {
    
        public void remove(Isurvey_user_input_line survey_user_input_line);

        public void updateBatch(Isurvey_user_input_line survey_user_input_line);

        public void get(Isurvey_user_input_line survey_user_input_line);

        public void update(Isurvey_user_input_line survey_user_input_line);

        public void createBatch(Isurvey_user_input_line survey_user_input_line);

        public void create(Isurvey_user_input_line survey_user_input_line);

        public Page<Isurvey_user_input_line> search(SearchContext context);

        public void removeBatch(Isurvey_user_input_line survey_user_input_line);

        public List<Isurvey_user_input_line> select();


}