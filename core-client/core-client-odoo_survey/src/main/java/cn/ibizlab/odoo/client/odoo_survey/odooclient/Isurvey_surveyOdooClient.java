package cn.ibizlab.odoo.client.odoo_survey.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Isurvey_survey;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[survey_survey] 服务对象客户端接口
 */
public interface Isurvey_surveyOdooClient {
    
        public void create(Isurvey_survey survey_survey);

        public Page<Isurvey_survey> search(SearchContext context);

        public void remove(Isurvey_survey survey_survey);

        public void createBatch(Isurvey_survey survey_survey);

        public void update(Isurvey_survey survey_survey);

        public void updateBatch(Isurvey_survey survey_survey);

        public void removeBatch(Isurvey_survey survey_survey);

        public void get(Isurvey_survey survey_survey);

        public List<Isurvey_survey> select();


}