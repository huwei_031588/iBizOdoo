package cn.ibizlab.odoo.client.odoo_web_tour.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iweb_tour_tour;
import cn.ibizlab.odoo.core.client.service.Iweb_tour_tourClientService;
import cn.ibizlab.odoo.client.odoo_web_tour.model.web_tour_tourImpl;
import cn.ibizlab.odoo.client.odoo_web_tour.odooclient.Iweb_tour_tourOdooClient;
import cn.ibizlab.odoo.client.odoo_web_tour.odooclient.impl.web_tour_tourOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[web_tour_tour] 服务对象接口
 */
@Service
public class web_tour_tourClientServiceImpl implements Iweb_tour_tourClientService {
    @Autowired
    private  Iweb_tour_tourOdooClient  web_tour_tourOdooClient;

    public Iweb_tour_tour createModel() {		
		return new web_tour_tourImpl();
	}


        public void createBatch(List<Iweb_tour_tour> web_tour_tours){
            
        }
        
        public Page<Iweb_tour_tour> search(SearchContext context){
            return this.web_tour_tourOdooClient.search(context) ;
        }
        
        public void get(Iweb_tour_tour web_tour_tour){
            this.web_tour_tourOdooClient.get(web_tour_tour) ;
        }
        
        public void removeBatch(List<Iweb_tour_tour> web_tour_tours){
            
        }
        
        public void updateBatch(List<Iweb_tour_tour> web_tour_tours){
            
        }
        
        public void update(Iweb_tour_tour web_tour_tour){
this.web_tour_tourOdooClient.update(web_tour_tour) ;
        }
        
        public void remove(Iweb_tour_tour web_tour_tour){
this.web_tour_tourOdooClient.remove(web_tour_tour) ;
        }
        
        public void create(Iweb_tour_tour web_tour_tour){
this.web_tour_tourOdooClient.create(web_tour_tour) ;
        }
        
        public Page<Iweb_tour_tour> select(SearchContext context){
            return null ;
        }
        

}

