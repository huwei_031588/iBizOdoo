package cn.ibizlab.odoo.client.odoo_web_tour.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iweb_tour_tour;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[web_tour_tour] 服务对象客户端接口
 */
public interface Iweb_tour_tourOdooClient {
    
        public void createBatch(Iweb_tour_tour web_tour_tour);

        public Page<Iweb_tour_tour> search(SearchContext context);

        public void get(Iweb_tour_tour web_tour_tour);

        public void removeBatch(Iweb_tour_tour web_tour_tour);

        public void updateBatch(Iweb_tour_tour web_tour_tour);

        public void update(Iweb_tour_tour web_tour_tour);

        public void remove(Iweb_tour_tour web_tour_tour);

        public void create(Iweb_tour_tour web_tour_tour);

        public List<Iweb_tour_tour> select();


}