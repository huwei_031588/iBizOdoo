package cn.ibizlab.odoo.client.odoo_web_tour.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.web.tour")
@Data
public class odoo_web_tourClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
