package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_cashbox_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_cashbox_line] 服务对象客户端接口
 */
public interface Iaccount_cashbox_lineOdooClient {
    
        public void update(Iaccount_cashbox_line account_cashbox_line);

        public Page<Iaccount_cashbox_line> search(SearchContext context);

        public void createBatch(Iaccount_cashbox_line account_cashbox_line);

        public void get(Iaccount_cashbox_line account_cashbox_line);

        public void create(Iaccount_cashbox_line account_cashbox_line);

        public void updateBatch(Iaccount_cashbox_line account_cashbox_line);

        public void removeBatch(Iaccount_cashbox_line account_cashbox_line);

        public void remove(Iaccount_cashbox_line account_cashbox_line);

        public List<Iaccount_cashbox_line> select();


}