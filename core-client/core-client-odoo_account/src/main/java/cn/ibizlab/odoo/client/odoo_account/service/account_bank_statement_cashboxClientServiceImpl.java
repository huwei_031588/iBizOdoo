package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement_cashbox;
import cn.ibizlab.odoo.core.client.service.Iaccount_bank_statement_cashboxClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_bank_statement_cashboxImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_bank_statement_cashboxOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_bank_statement_cashboxOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_bank_statement_cashbox] 服务对象接口
 */
@Service
public class account_bank_statement_cashboxClientServiceImpl implements Iaccount_bank_statement_cashboxClientService {
    @Autowired
    private  Iaccount_bank_statement_cashboxOdooClient  account_bank_statement_cashboxOdooClient;

    public Iaccount_bank_statement_cashbox createModel() {		
		return new account_bank_statement_cashboxImpl();
	}


        public void update(Iaccount_bank_statement_cashbox account_bank_statement_cashbox){
this.account_bank_statement_cashboxOdooClient.update(account_bank_statement_cashbox) ;
        }
        
        public void get(Iaccount_bank_statement_cashbox account_bank_statement_cashbox){
            this.account_bank_statement_cashboxOdooClient.get(account_bank_statement_cashbox) ;
        }
        
        public void removeBatch(List<Iaccount_bank_statement_cashbox> account_bank_statement_cashboxes){
            
        }
        
        public Page<Iaccount_bank_statement_cashbox> search(SearchContext context){
            return this.account_bank_statement_cashboxOdooClient.search(context) ;
        }
        
        public void createBatch(List<Iaccount_bank_statement_cashbox> account_bank_statement_cashboxes){
            
        }
        
        public void remove(Iaccount_bank_statement_cashbox account_bank_statement_cashbox){
this.account_bank_statement_cashboxOdooClient.remove(account_bank_statement_cashbox) ;
        }
        
        public void updateBatch(List<Iaccount_bank_statement_cashbox> account_bank_statement_cashboxes){
            
        }
        
        public void create(Iaccount_bank_statement_cashbox account_bank_statement_cashbox){
this.account_bank_statement_cashboxOdooClient.create(account_bank_statement_cashbox) ;
        }
        
        public Page<Iaccount_bank_statement_cashbox> select(SearchContext context){
            return null ;
        }
        

}

