package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_confirm;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_invoice_confirm] 服务对象客户端接口
 */
public interface Iaccount_invoice_confirmOdooClient {
    
        public Page<Iaccount_invoice_confirm> search(SearchContext context);

        public void create(Iaccount_invoice_confirm account_invoice_confirm);

        public void updateBatch(Iaccount_invoice_confirm account_invoice_confirm);

        public void removeBatch(Iaccount_invoice_confirm account_invoice_confirm);

        public void update(Iaccount_invoice_confirm account_invoice_confirm);

        public void createBatch(Iaccount_invoice_confirm account_invoice_confirm);

        public void get(Iaccount_invoice_confirm account_invoice_confirm);

        public void remove(Iaccount_invoice_confirm account_invoice_confirm);

        public List<Iaccount_invoice_confirm> select();


}