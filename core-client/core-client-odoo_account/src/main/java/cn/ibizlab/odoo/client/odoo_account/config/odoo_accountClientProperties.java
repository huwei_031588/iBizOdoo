package cn.ibizlab.odoo.client.odoo_account.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.account")
@Data
public class odoo_accountClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
