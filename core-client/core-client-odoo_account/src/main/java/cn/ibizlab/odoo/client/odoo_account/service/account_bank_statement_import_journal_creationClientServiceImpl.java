package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement_import_journal_creation;
import cn.ibizlab.odoo.core.client.service.Iaccount_bank_statement_import_journal_creationClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_bank_statement_import_journal_creationImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_bank_statement_import_journal_creationOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_bank_statement_import_journal_creationOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_bank_statement_import_journal_creation] 服务对象接口
 */
@Service
public class account_bank_statement_import_journal_creationClientServiceImpl implements Iaccount_bank_statement_import_journal_creationClientService {
    @Autowired
    private  Iaccount_bank_statement_import_journal_creationOdooClient  account_bank_statement_import_journal_creationOdooClient;

    public Iaccount_bank_statement_import_journal_creation createModel() {		
		return new account_bank_statement_import_journal_creationImpl();
	}


        public void create(Iaccount_bank_statement_import_journal_creation account_bank_statement_import_journal_creation){
this.account_bank_statement_import_journal_creationOdooClient.create(account_bank_statement_import_journal_creation) ;
        }
        
        public void get(Iaccount_bank_statement_import_journal_creation account_bank_statement_import_journal_creation){
            this.account_bank_statement_import_journal_creationOdooClient.get(account_bank_statement_import_journal_creation) ;
        }
        
        public void updateBatch(List<Iaccount_bank_statement_import_journal_creation> account_bank_statement_import_journal_creations){
            
        }
        
        public void removeBatch(List<Iaccount_bank_statement_import_journal_creation> account_bank_statement_import_journal_creations){
            
        }
        
        public void createBatch(List<Iaccount_bank_statement_import_journal_creation> account_bank_statement_import_journal_creations){
            
        }
        
        public void remove(Iaccount_bank_statement_import_journal_creation account_bank_statement_import_journal_creation){
this.account_bank_statement_import_journal_creationOdooClient.remove(account_bank_statement_import_journal_creation) ;
        }
        
        public void update(Iaccount_bank_statement_import_journal_creation account_bank_statement_import_journal_creation){
this.account_bank_statement_import_journal_creationOdooClient.update(account_bank_statement_import_journal_creation) ;
        }
        
        public Page<Iaccount_bank_statement_import_journal_creation> search(SearchContext context){
            return this.account_bank_statement_import_journal_creationOdooClient.search(context) ;
        }
        
        public Page<Iaccount_bank_statement_import_journal_creation> select(SearchContext context){
            return null ;
        }
        

}

