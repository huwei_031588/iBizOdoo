package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_abstract_payment;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_abstract_payment] 服务对象客户端接口
 */
public interface Iaccount_abstract_paymentOdooClient {
    
        public void create(Iaccount_abstract_payment account_abstract_payment);

        public void removeBatch(Iaccount_abstract_payment account_abstract_payment);

        public void get(Iaccount_abstract_payment account_abstract_payment);

        public void remove(Iaccount_abstract_payment account_abstract_payment);

        public Page<Iaccount_abstract_payment> search(SearchContext context);

        public void updateBatch(Iaccount_abstract_payment account_abstract_payment);

        public void update(Iaccount_abstract_payment account_abstract_payment);

        public void createBatch(Iaccount_abstract_payment account_abstract_payment);

        public List<Iaccount_abstract_payment> select();


}