package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice;
import cn.ibizlab.odoo.core.client.service.Iaccount_invoiceClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_invoiceImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_invoiceOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_invoiceOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_invoice] 服务对象接口
 */
@Service
public class account_invoiceClientServiceImpl implements Iaccount_invoiceClientService {
    @Autowired
    private  Iaccount_invoiceOdooClient  account_invoiceOdooClient;

    public Iaccount_invoice createModel() {		
		return new account_invoiceImpl();
	}


        public void update(Iaccount_invoice account_invoice){
this.account_invoiceOdooClient.update(account_invoice) ;
        }
        
        public void removeBatch(List<Iaccount_invoice> account_invoices){
            
        }
        
        public void createBatch(List<Iaccount_invoice> account_invoices){
            
        }
        
        public Page<Iaccount_invoice> search(SearchContext context){
            return this.account_invoiceOdooClient.search(context) ;
        }
        
        public void create(Iaccount_invoice account_invoice){
this.account_invoiceOdooClient.create(account_invoice) ;
        }
        
        public void updateBatch(List<Iaccount_invoice> account_invoices){
            
        }
        
        public void remove(Iaccount_invoice account_invoice){
this.account_invoiceOdooClient.remove(account_invoice) ;
        }
        
        public void get(Iaccount_invoice account_invoice){
            this.account_invoiceOdooClient.get(account_invoice) ;
        }
        
        public Page<Iaccount_invoice> select(SearchContext context){
            return null ;
        }
        

}

