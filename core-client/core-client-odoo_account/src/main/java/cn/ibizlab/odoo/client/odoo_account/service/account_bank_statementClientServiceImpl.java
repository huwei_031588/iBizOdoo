package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement;
import cn.ibizlab.odoo.core.client.service.Iaccount_bank_statementClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_bank_statementImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_bank_statementOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_bank_statementOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_bank_statement] 服务对象接口
 */
@Service
public class account_bank_statementClientServiceImpl implements Iaccount_bank_statementClientService {
    @Autowired
    private  Iaccount_bank_statementOdooClient  account_bank_statementOdooClient;

    public Iaccount_bank_statement createModel() {		
		return new account_bank_statementImpl();
	}


        public void remove(Iaccount_bank_statement account_bank_statement){
this.account_bank_statementOdooClient.remove(account_bank_statement) ;
        }
        
        public Page<Iaccount_bank_statement> search(SearchContext context){
            return this.account_bank_statementOdooClient.search(context) ;
        }
        
        public void createBatch(List<Iaccount_bank_statement> account_bank_statements){
            
        }
        
        public void get(Iaccount_bank_statement account_bank_statement){
            this.account_bank_statementOdooClient.get(account_bank_statement) ;
        }
        
        public void removeBatch(List<Iaccount_bank_statement> account_bank_statements){
            
        }
        
        public void create(Iaccount_bank_statement account_bank_statement){
this.account_bank_statementOdooClient.create(account_bank_statement) ;
        }
        
        public void update(Iaccount_bank_statement account_bank_statement){
this.account_bank_statementOdooClient.update(account_bank_statement) ;
        }
        
        public void updateBatch(List<Iaccount_bank_statement> account_bank_statements){
            
        }
        
        public Page<Iaccount_bank_statement> select(SearchContext context){
            return null ;
        }
        

}

