package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_account;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_account] 服务对象客户端接口
 */
public interface Iaccount_accountOdooClient {
    
        public void remove(Iaccount_account account_account);

        public Page<Iaccount_account> search(SearchContext context);

        public void createBatch(Iaccount_account account_account);

        public void get(Iaccount_account account_account);

        public void update(Iaccount_account account_account);

        public void updateBatch(Iaccount_account account_account);

        public void create(Iaccount_account account_account);

        public void removeBatch(Iaccount_account account_account);

        public List<Iaccount_account> select();


}