package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_setup_bank_manual_config;
import cn.ibizlab.odoo.core.client.service.Iaccount_setup_bank_manual_configClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_setup_bank_manual_configImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_setup_bank_manual_configOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_setup_bank_manual_configOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_setup_bank_manual_config] 服务对象接口
 */
@Service
public class account_setup_bank_manual_configClientServiceImpl implements Iaccount_setup_bank_manual_configClientService {
    @Autowired
    private  Iaccount_setup_bank_manual_configOdooClient  account_setup_bank_manual_configOdooClient;

    public Iaccount_setup_bank_manual_config createModel() {		
		return new account_setup_bank_manual_configImpl();
	}


        public void update(Iaccount_setup_bank_manual_config account_setup_bank_manual_config){
this.account_setup_bank_manual_configOdooClient.update(account_setup_bank_manual_config) ;
        }
        
        public void create(Iaccount_setup_bank_manual_config account_setup_bank_manual_config){
this.account_setup_bank_manual_configOdooClient.create(account_setup_bank_manual_config) ;
        }
        
        public void get(Iaccount_setup_bank_manual_config account_setup_bank_manual_config){
            this.account_setup_bank_manual_configOdooClient.get(account_setup_bank_manual_config) ;
        }
        
        public void updateBatch(List<Iaccount_setup_bank_manual_config> account_setup_bank_manual_configs){
            
        }
        
        public void remove(Iaccount_setup_bank_manual_config account_setup_bank_manual_config){
this.account_setup_bank_manual_configOdooClient.remove(account_setup_bank_manual_config) ;
        }
        
        public void removeBatch(List<Iaccount_setup_bank_manual_config> account_setup_bank_manual_configs){
            
        }
        
        public void createBatch(List<Iaccount_setup_bank_manual_config> account_setup_bank_manual_configs){
            
        }
        
        public Page<Iaccount_setup_bank_manual_config> search(SearchContext context){
            return this.account_setup_bank_manual_configOdooClient.search(context) ;
        }
        
        public Page<Iaccount_setup_bank_manual_config> select(SearchContext context){
            return null ;
        }
        

}

