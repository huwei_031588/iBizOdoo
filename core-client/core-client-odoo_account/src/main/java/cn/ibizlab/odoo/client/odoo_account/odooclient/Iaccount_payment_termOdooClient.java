package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_payment_term;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_payment_term] 服务对象客户端接口
 */
public interface Iaccount_payment_termOdooClient {
    
        public Page<Iaccount_payment_term> search(SearchContext context);

        public void create(Iaccount_payment_term account_payment_term);

        public void createBatch(Iaccount_payment_term account_payment_term);

        public void updateBatch(Iaccount_payment_term account_payment_term);

        public void remove(Iaccount_payment_term account_payment_term);

        public void removeBatch(Iaccount_payment_term account_payment_term);

        public void update(Iaccount_payment_term account_payment_term);

        public void get(Iaccount_payment_term account_payment_term);

        public List<Iaccount_payment_term> select();


}