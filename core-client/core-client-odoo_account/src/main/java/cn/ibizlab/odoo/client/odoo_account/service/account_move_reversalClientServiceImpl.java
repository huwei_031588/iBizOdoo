package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_move_reversal;
import cn.ibizlab.odoo.core.client.service.Iaccount_move_reversalClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_move_reversalImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_move_reversalOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_move_reversalOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_move_reversal] 服务对象接口
 */
@Service
public class account_move_reversalClientServiceImpl implements Iaccount_move_reversalClientService {
    @Autowired
    private  Iaccount_move_reversalOdooClient  account_move_reversalOdooClient;

    public Iaccount_move_reversal createModel() {		
		return new account_move_reversalImpl();
	}


        public void createBatch(List<Iaccount_move_reversal> account_move_reversals){
            
        }
        
        public void update(Iaccount_move_reversal account_move_reversal){
this.account_move_reversalOdooClient.update(account_move_reversal) ;
        }
        
        public void removeBatch(List<Iaccount_move_reversal> account_move_reversals){
            
        }
        
        public void create(Iaccount_move_reversal account_move_reversal){
this.account_move_reversalOdooClient.create(account_move_reversal) ;
        }
        
        public void updateBatch(List<Iaccount_move_reversal> account_move_reversals){
            
        }
        
        public void get(Iaccount_move_reversal account_move_reversal){
            this.account_move_reversalOdooClient.get(account_move_reversal) ;
        }
        
        public Page<Iaccount_move_reversal> search(SearchContext context){
            return this.account_move_reversalOdooClient.search(context) ;
        }
        
        public void remove(Iaccount_move_reversal account_move_reversal){
this.account_move_reversalOdooClient.remove(account_move_reversal) ;
        }
        
        public Page<Iaccount_move_reversal> select(SearchContext context){
            return null ;
        }
        

}

