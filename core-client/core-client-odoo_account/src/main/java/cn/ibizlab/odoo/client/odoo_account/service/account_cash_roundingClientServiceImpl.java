package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_cash_rounding;
import cn.ibizlab.odoo.core.client.service.Iaccount_cash_roundingClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_cash_roundingImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_cash_roundingOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_cash_roundingOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_cash_rounding] 服务对象接口
 */
@Service
public class account_cash_roundingClientServiceImpl implements Iaccount_cash_roundingClientService {
    @Autowired
    private  Iaccount_cash_roundingOdooClient  account_cash_roundingOdooClient;

    public Iaccount_cash_rounding createModel() {		
		return new account_cash_roundingImpl();
	}


        public void get(Iaccount_cash_rounding account_cash_rounding){
            this.account_cash_roundingOdooClient.get(account_cash_rounding) ;
        }
        
        public void remove(Iaccount_cash_rounding account_cash_rounding){
this.account_cash_roundingOdooClient.remove(account_cash_rounding) ;
        }
        
        public void createBatch(List<Iaccount_cash_rounding> account_cash_roundings){
            
        }
        
        public void removeBatch(List<Iaccount_cash_rounding> account_cash_roundings){
            
        }
        
        public void create(Iaccount_cash_rounding account_cash_rounding){
this.account_cash_roundingOdooClient.create(account_cash_rounding) ;
        }
        
        public void update(Iaccount_cash_rounding account_cash_rounding){
this.account_cash_roundingOdooClient.update(account_cash_rounding) ;
        }
        
        public Page<Iaccount_cash_rounding> search(SearchContext context){
            return this.account_cash_roundingOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Iaccount_cash_rounding> account_cash_roundings){
            
        }
        
        public Page<Iaccount_cash_rounding> select(SearchContext context){
            return null ;
        }
        

}

