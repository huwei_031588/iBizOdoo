package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_position;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_fiscal_position] 服务对象客户端接口
 */
public interface Iaccount_fiscal_positionOdooClient {
    
        public void update(Iaccount_fiscal_position account_fiscal_position);

        public void removeBatch(Iaccount_fiscal_position account_fiscal_position);

        public Page<Iaccount_fiscal_position> search(SearchContext context);

        public void remove(Iaccount_fiscal_position account_fiscal_position);

        public void get(Iaccount_fiscal_position account_fiscal_position);

        public void updateBatch(Iaccount_fiscal_position account_fiscal_position);

        public void create(Iaccount_fiscal_position account_fiscal_position);

        public void createBatch(Iaccount_fiscal_position account_fiscal_position);

        public List<Iaccount_fiscal_position> select();


}