package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_payment;
import cn.ibizlab.odoo.core.client.service.Iaccount_paymentClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_paymentImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_paymentOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_paymentOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_payment] 服务对象接口
 */
@Service
public class account_paymentClientServiceImpl implements Iaccount_paymentClientService {
    @Autowired
    private  Iaccount_paymentOdooClient  account_paymentOdooClient;

    public Iaccount_payment createModel() {		
		return new account_paymentImpl();
	}


        public Page<Iaccount_payment> search(SearchContext context){
            return this.account_paymentOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Iaccount_payment> account_payments){
            
        }
        
        public void create(Iaccount_payment account_payment){
this.account_paymentOdooClient.create(account_payment) ;
        }
        
        public void update(Iaccount_payment account_payment){
this.account_paymentOdooClient.update(account_payment) ;
        }
        
        public void remove(Iaccount_payment account_payment){
this.account_paymentOdooClient.remove(account_payment) ;
        }
        
        public void createBatch(List<Iaccount_payment> account_payments){
            
        }
        
        public void removeBatch(List<Iaccount_payment> account_payments){
            
        }
        
        public void get(Iaccount_payment account_payment){
            this.account_paymentOdooClient.get(account_payment) ;
        }
        
        public Page<Iaccount_payment> select(SearchContext context){
            return null ;
        }
        

}

