package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_year;
import cn.ibizlab.odoo.core.client.service.Iaccount_fiscal_yearClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_fiscal_yearImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_fiscal_yearOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_fiscal_yearOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_fiscal_year] 服务对象接口
 */
@Service
public class account_fiscal_yearClientServiceImpl implements Iaccount_fiscal_yearClientService {
    @Autowired
    private  Iaccount_fiscal_yearOdooClient  account_fiscal_yearOdooClient;

    public Iaccount_fiscal_year createModel() {		
		return new account_fiscal_yearImpl();
	}


        public void removeBatch(List<Iaccount_fiscal_year> account_fiscal_years){
            
        }
        
        public void updateBatch(List<Iaccount_fiscal_year> account_fiscal_years){
            
        }
        
        public void get(Iaccount_fiscal_year account_fiscal_year){
            this.account_fiscal_yearOdooClient.get(account_fiscal_year) ;
        }
        
        public void remove(Iaccount_fiscal_year account_fiscal_year){
this.account_fiscal_yearOdooClient.remove(account_fiscal_year) ;
        }
        
        public void createBatch(List<Iaccount_fiscal_year> account_fiscal_years){
            
        }
        
        public void update(Iaccount_fiscal_year account_fiscal_year){
this.account_fiscal_yearOdooClient.update(account_fiscal_year) ;
        }
        
        public Page<Iaccount_fiscal_year> search(SearchContext context){
            return this.account_fiscal_yearOdooClient.search(context) ;
        }
        
        public void create(Iaccount_fiscal_year account_fiscal_year){
this.account_fiscal_yearOdooClient.create(account_fiscal_year) ;
        }
        
        public Page<Iaccount_fiscal_year> select(SearchContext context){
            return null ;
        }
        

}

