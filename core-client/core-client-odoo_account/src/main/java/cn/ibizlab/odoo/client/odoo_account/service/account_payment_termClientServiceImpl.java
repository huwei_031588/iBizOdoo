package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_payment_term;
import cn.ibizlab.odoo.core.client.service.Iaccount_payment_termClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_payment_termImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_payment_termOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_payment_termOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_payment_term] 服务对象接口
 */
@Service
public class account_payment_termClientServiceImpl implements Iaccount_payment_termClientService {
    @Autowired
    private  Iaccount_payment_termOdooClient  account_payment_termOdooClient;

    public Iaccount_payment_term createModel() {		
		return new account_payment_termImpl();
	}


        public Page<Iaccount_payment_term> search(SearchContext context){
            return this.account_payment_termOdooClient.search(context) ;
        }
        
        public void create(Iaccount_payment_term account_payment_term){
this.account_payment_termOdooClient.create(account_payment_term) ;
        }
        
        public void createBatch(List<Iaccount_payment_term> account_payment_terms){
            
        }
        
        public void updateBatch(List<Iaccount_payment_term> account_payment_terms){
            
        }
        
        public void remove(Iaccount_payment_term account_payment_term){
this.account_payment_termOdooClient.remove(account_payment_term) ;
        }
        
        public void removeBatch(List<Iaccount_payment_term> account_payment_terms){
            
        }
        
        public void update(Iaccount_payment_term account_payment_term){
this.account_payment_termOdooClient.update(account_payment_term) ;
        }
        
        public void get(Iaccount_payment_term account_payment_term){
            this.account_payment_termOdooClient.get(account_payment_term) ;
        }
        
        public Page<Iaccount_payment_term> select(SearchContext context){
            return null ;
        }
        

}

