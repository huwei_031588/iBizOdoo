package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_common_report;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_common_report] 服务对象客户端接口
 */
public interface Iaccount_common_reportOdooClient {
    
        public void get(Iaccount_common_report account_common_report);

        public void updateBatch(Iaccount_common_report account_common_report);

        public void create(Iaccount_common_report account_common_report);

        public Page<Iaccount_common_report> search(SearchContext context);

        public void createBatch(Iaccount_common_report account_common_report);

        public void removeBatch(Iaccount_common_report account_common_report);

        public void remove(Iaccount_common_report account_common_report);

        public void update(Iaccount_common_report account_common_report);

        public List<Iaccount_common_report> select();


}