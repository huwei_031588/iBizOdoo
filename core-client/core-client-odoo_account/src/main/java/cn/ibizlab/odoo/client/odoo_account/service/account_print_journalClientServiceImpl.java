package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_print_journal;
import cn.ibizlab.odoo.core.client.service.Iaccount_print_journalClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_print_journalImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_print_journalOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_print_journalOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_print_journal] 服务对象接口
 */
@Service
public class account_print_journalClientServiceImpl implements Iaccount_print_journalClientService {
    @Autowired
    private  Iaccount_print_journalOdooClient  account_print_journalOdooClient;

    public Iaccount_print_journal createModel() {		
		return new account_print_journalImpl();
	}


        public void create(Iaccount_print_journal account_print_journal){
this.account_print_journalOdooClient.create(account_print_journal) ;
        }
        
        public void createBatch(List<Iaccount_print_journal> account_print_journals){
            
        }
        
        public void update(Iaccount_print_journal account_print_journal){
this.account_print_journalOdooClient.update(account_print_journal) ;
        }
        
        public void remove(Iaccount_print_journal account_print_journal){
this.account_print_journalOdooClient.remove(account_print_journal) ;
        }
        
        public void removeBatch(List<Iaccount_print_journal> account_print_journals){
            
        }
        
        public Page<Iaccount_print_journal> search(SearchContext context){
            return this.account_print_journalOdooClient.search(context) ;
        }
        
        public void get(Iaccount_print_journal account_print_journal){
            this.account_print_journalOdooClient.get(account_print_journal) ;
        }
        
        public void updateBatch(List<Iaccount_print_journal> account_print_journals){
            
        }
        
        public Page<Iaccount_print_journal> select(SearchContext context){
            return null ;
        }
        

}

