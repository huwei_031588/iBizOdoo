package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_position_account;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_fiscal_position_account] 服务对象客户端接口
 */
public interface Iaccount_fiscal_position_accountOdooClient {
    
        public void updateBatch(Iaccount_fiscal_position_account account_fiscal_position_account);

        public void create(Iaccount_fiscal_position_account account_fiscal_position_account);

        public void createBatch(Iaccount_fiscal_position_account account_fiscal_position_account);

        public void update(Iaccount_fiscal_position_account account_fiscal_position_account);

        public void get(Iaccount_fiscal_position_account account_fiscal_position_account);

        public void remove(Iaccount_fiscal_position_account account_fiscal_position_account);

        public Page<Iaccount_fiscal_position_account> search(SearchContext context);

        public void removeBatch(Iaccount_fiscal_position_account account_fiscal_position_account);

        public List<Iaccount_fiscal_position_account> select();


}