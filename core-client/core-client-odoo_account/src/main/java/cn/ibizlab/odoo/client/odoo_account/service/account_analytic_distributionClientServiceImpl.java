package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_analytic_distribution;
import cn.ibizlab.odoo.core.client.service.Iaccount_analytic_distributionClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_analytic_distributionImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_analytic_distributionOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_analytic_distributionOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_analytic_distribution] 服务对象接口
 */
@Service
public class account_analytic_distributionClientServiceImpl implements Iaccount_analytic_distributionClientService {
    @Autowired
    private  Iaccount_analytic_distributionOdooClient  account_analytic_distributionOdooClient;

    public Iaccount_analytic_distribution createModel() {		
		return new account_analytic_distributionImpl();
	}


        public void updateBatch(List<Iaccount_analytic_distribution> account_analytic_distributions){
            
        }
        
        public void createBatch(List<Iaccount_analytic_distribution> account_analytic_distributions){
            
        }
        
        public void create(Iaccount_analytic_distribution account_analytic_distribution){
this.account_analytic_distributionOdooClient.create(account_analytic_distribution) ;
        }
        
        public void update(Iaccount_analytic_distribution account_analytic_distribution){
this.account_analytic_distributionOdooClient.update(account_analytic_distribution) ;
        }
        
        public void removeBatch(List<Iaccount_analytic_distribution> account_analytic_distributions){
            
        }
        
        public Page<Iaccount_analytic_distribution> search(SearchContext context){
            return this.account_analytic_distributionOdooClient.search(context) ;
        }
        
        public void get(Iaccount_analytic_distribution account_analytic_distribution){
            this.account_analytic_distributionOdooClient.get(account_analytic_distribution) ;
        }
        
        public void remove(Iaccount_analytic_distribution account_analytic_distribution){
this.account_analytic_distributionOdooClient.remove(account_analytic_distribution) ;
        }
        
        public Page<Iaccount_analytic_distribution> select(SearchContext context){
            return null ;
        }
        

}

