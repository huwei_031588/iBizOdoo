package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_report;
import cn.ibizlab.odoo.core.client.service.Iaccount_invoice_reportClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_invoice_reportImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_invoice_reportOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_invoice_reportOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_invoice_report] 服务对象接口
 */
@Service
public class account_invoice_reportClientServiceImpl implements Iaccount_invoice_reportClientService {
    @Autowired
    private  Iaccount_invoice_reportOdooClient  account_invoice_reportOdooClient;

    public Iaccount_invoice_report createModel() {		
		return new account_invoice_reportImpl();
	}


        public void createBatch(List<Iaccount_invoice_report> account_invoice_reports){
            
        }
        
        public void removeBatch(List<Iaccount_invoice_report> account_invoice_reports){
            
        }
        
        public void updateBatch(List<Iaccount_invoice_report> account_invoice_reports){
            
        }
        
        public void remove(Iaccount_invoice_report account_invoice_report){
this.account_invoice_reportOdooClient.remove(account_invoice_report) ;
        }
        
        public void get(Iaccount_invoice_report account_invoice_report){
            this.account_invoice_reportOdooClient.get(account_invoice_report) ;
        }
        
        public void create(Iaccount_invoice_report account_invoice_report){
this.account_invoice_reportOdooClient.create(account_invoice_report) ;
        }
        
        public void update(Iaccount_invoice_report account_invoice_report){
this.account_invoice_reportOdooClient.update(account_invoice_report) ;
        }
        
        public Page<Iaccount_invoice_report> search(SearchContext context){
            return this.account_invoice_reportOdooClient.search(context) ;
        }
        
        public Page<Iaccount_invoice_report> select(SearchContext context){
            return null ;
        }
        

}

