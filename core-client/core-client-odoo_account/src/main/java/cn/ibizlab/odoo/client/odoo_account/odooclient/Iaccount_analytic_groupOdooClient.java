package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_analytic_group;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_analytic_group] 服务对象客户端接口
 */
public interface Iaccount_analytic_groupOdooClient {
    
        public void update(Iaccount_analytic_group account_analytic_group);

        public void remove(Iaccount_analytic_group account_analytic_group);

        public void createBatch(Iaccount_analytic_group account_analytic_group);

        public void create(Iaccount_analytic_group account_analytic_group);

        public void get(Iaccount_analytic_group account_analytic_group);

        public void updateBatch(Iaccount_analytic_group account_analytic_group);

        public void removeBatch(Iaccount_analytic_group account_analytic_group);

        public Page<Iaccount_analytic_group> search(SearchContext context);

        public List<Iaccount_analytic_group> select();


}