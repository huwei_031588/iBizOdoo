package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_tax_template;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_tax_template] 服务对象客户端接口
 */
public interface Iaccount_tax_templateOdooClient {
    
        public void update(Iaccount_tax_template account_tax_template);

        public void create(Iaccount_tax_template account_tax_template);

        public void get(Iaccount_tax_template account_tax_template);

        public Page<Iaccount_tax_template> search(SearchContext context);

        public void removeBatch(Iaccount_tax_template account_tax_template);

        public void updateBatch(Iaccount_tax_template account_tax_template);

        public void remove(Iaccount_tax_template account_tax_template);

        public void createBatch(Iaccount_tax_template account_tax_template);

        public List<Iaccount_tax_template> select();


}