package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_account_template;
import cn.ibizlab.odoo.core.client.service.Iaccount_account_templateClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_account_templateImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_account_templateOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_account_templateOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_account_template] 服务对象接口
 */
@Service
public class account_account_templateClientServiceImpl implements Iaccount_account_templateClientService {
    @Autowired
    private  Iaccount_account_templateOdooClient  account_account_templateOdooClient;

    public Iaccount_account_template createModel() {		
		return new account_account_templateImpl();
	}


        public void update(Iaccount_account_template account_account_template){
this.account_account_templateOdooClient.update(account_account_template) ;
        }
        
        public void get(Iaccount_account_template account_account_template){
            this.account_account_templateOdooClient.get(account_account_template) ;
        }
        
        public void removeBatch(List<Iaccount_account_template> account_account_templates){
            
        }
        
        public void createBatch(List<Iaccount_account_template> account_account_templates){
            
        }
        
        public void remove(Iaccount_account_template account_account_template){
this.account_account_templateOdooClient.remove(account_account_template) ;
        }
        
        public void create(Iaccount_account_template account_account_template){
this.account_account_templateOdooClient.create(account_account_template) ;
        }
        
        public Page<Iaccount_account_template> search(SearchContext context){
            return this.account_account_templateOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Iaccount_account_template> account_account_templates){
            
        }
        
        public Page<Iaccount_account_template> select(SearchContext context){
            return null ;
        }
        

}

