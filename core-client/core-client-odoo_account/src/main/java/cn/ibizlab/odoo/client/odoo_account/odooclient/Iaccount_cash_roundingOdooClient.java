package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_cash_rounding;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_cash_rounding] 服务对象客户端接口
 */
public interface Iaccount_cash_roundingOdooClient {
    
        public void get(Iaccount_cash_rounding account_cash_rounding);

        public void remove(Iaccount_cash_rounding account_cash_rounding);

        public void createBatch(Iaccount_cash_rounding account_cash_rounding);

        public void removeBatch(Iaccount_cash_rounding account_cash_rounding);

        public void create(Iaccount_cash_rounding account_cash_rounding);

        public void update(Iaccount_cash_rounding account_cash_rounding);

        public Page<Iaccount_cash_rounding> search(SearchContext context);

        public void updateBatch(Iaccount_cash_rounding account_cash_rounding);

        public List<Iaccount_cash_rounding> select();


}