package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_confirm;
import cn.ibizlab.odoo.core.client.service.Iaccount_invoice_confirmClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_invoice_confirmImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_invoice_confirmOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_invoice_confirmOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_invoice_confirm] 服务对象接口
 */
@Service
public class account_invoice_confirmClientServiceImpl implements Iaccount_invoice_confirmClientService {
    @Autowired
    private  Iaccount_invoice_confirmOdooClient  account_invoice_confirmOdooClient;

    public Iaccount_invoice_confirm createModel() {		
		return new account_invoice_confirmImpl();
	}


        public Page<Iaccount_invoice_confirm> search(SearchContext context){
            return this.account_invoice_confirmOdooClient.search(context) ;
        }
        
        public void create(Iaccount_invoice_confirm account_invoice_confirm){
this.account_invoice_confirmOdooClient.create(account_invoice_confirm) ;
        }
        
        public void updateBatch(List<Iaccount_invoice_confirm> account_invoice_confirms){
            
        }
        
        public void removeBatch(List<Iaccount_invoice_confirm> account_invoice_confirms){
            
        }
        
        public void update(Iaccount_invoice_confirm account_invoice_confirm){
this.account_invoice_confirmOdooClient.update(account_invoice_confirm) ;
        }
        
        public void createBatch(List<Iaccount_invoice_confirm> account_invoice_confirms){
            
        }
        
        public void get(Iaccount_invoice_confirm account_invoice_confirm){
            this.account_invoice_confirmOdooClient.get(account_invoice_confirm) ;
        }
        
        public void remove(Iaccount_invoice_confirm account_invoice_confirm){
this.account_invoice_confirmOdooClient.remove(account_invoice_confirm) ;
        }
        
        public Page<Iaccount_invoice_confirm> select(SearchContext context){
            return null ;
        }
        

}

