package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_bank_statement] 服务对象客户端接口
 */
public interface Iaccount_bank_statementOdooClient {
    
        public void remove(Iaccount_bank_statement account_bank_statement);

        public Page<Iaccount_bank_statement> search(SearchContext context);

        public void createBatch(Iaccount_bank_statement account_bank_statement);

        public void get(Iaccount_bank_statement account_bank_statement);

        public void removeBatch(Iaccount_bank_statement account_bank_statement);

        public void create(Iaccount_bank_statement account_bank_statement);

        public void update(Iaccount_bank_statement account_bank_statement);

        public void updateBatch(Iaccount_bank_statement account_bank_statement);

        public List<Iaccount_bank_statement> select();


}