package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_reconcile_model_template;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_reconcile_model_template] 服务对象客户端接口
 */
public interface Iaccount_reconcile_model_templateOdooClient {
    
        public void remove(Iaccount_reconcile_model_template account_reconcile_model_template);

        public void create(Iaccount_reconcile_model_template account_reconcile_model_template);

        public Page<Iaccount_reconcile_model_template> search(SearchContext context);

        public void update(Iaccount_reconcile_model_template account_reconcile_model_template);

        public void updateBatch(Iaccount_reconcile_model_template account_reconcile_model_template);

        public void get(Iaccount_reconcile_model_template account_reconcile_model_template);

        public void removeBatch(Iaccount_reconcile_model_template account_reconcile_model_template);

        public void createBatch(Iaccount_reconcile_model_template account_reconcile_model_template);

        public List<Iaccount_reconcile_model_template> select();


}