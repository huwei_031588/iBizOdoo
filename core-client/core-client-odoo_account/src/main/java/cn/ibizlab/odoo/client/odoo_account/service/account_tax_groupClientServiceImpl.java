package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_tax_group;
import cn.ibizlab.odoo.core.client.service.Iaccount_tax_groupClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_tax_groupImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_tax_groupOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_tax_groupOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_tax_group] 服务对象接口
 */
@Service
public class account_tax_groupClientServiceImpl implements Iaccount_tax_groupClientService {
    @Autowired
    private  Iaccount_tax_groupOdooClient  account_tax_groupOdooClient;

    public Iaccount_tax_group createModel() {		
		return new account_tax_groupImpl();
	}


        public void createBatch(List<Iaccount_tax_group> account_tax_groups){
            
        }
        
        public void create(Iaccount_tax_group account_tax_group){
this.account_tax_groupOdooClient.create(account_tax_group) ;
        }
        
        public void get(Iaccount_tax_group account_tax_group){
            this.account_tax_groupOdooClient.get(account_tax_group) ;
        }
        
        public Page<Iaccount_tax_group> search(SearchContext context){
            return this.account_tax_groupOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Iaccount_tax_group> account_tax_groups){
            
        }
        
        public void remove(Iaccount_tax_group account_tax_group){
this.account_tax_groupOdooClient.remove(account_tax_group) ;
        }
        
        public void update(Iaccount_tax_group account_tax_group){
this.account_tax_groupOdooClient.update(account_tax_group) ;
        }
        
        public void removeBatch(List<Iaccount_tax_group> account_tax_groups){
            
        }
        
        public Page<Iaccount_tax_group> select(SearchContext context){
            return null ;
        }
        

}

