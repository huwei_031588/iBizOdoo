package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_send;
import cn.ibizlab.odoo.core.client.service.Iaccount_invoice_sendClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_invoice_sendImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_invoice_sendOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_invoice_sendOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_invoice_send] 服务对象接口
 */
@Service
public class account_invoice_sendClientServiceImpl implements Iaccount_invoice_sendClientService {
    @Autowired
    private  Iaccount_invoice_sendOdooClient  account_invoice_sendOdooClient;

    public Iaccount_invoice_send createModel() {		
		return new account_invoice_sendImpl();
	}


        public void create(Iaccount_invoice_send account_invoice_send){
this.account_invoice_sendOdooClient.create(account_invoice_send) ;
        }
        
        public void remove(Iaccount_invoice_send account_invoice_send){
this.account_invoice_sendOdooClient.remove(account_invoice_send) ;
        }
        
        public void createBatch(List<Iaccount_invoice_send> account_invoice_sends){
            
        }
        
        public Page<Iaccount_invoice_send> search(SearchContext context){
            return this.account_invoice_sendOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Iaccount_invoice_send> account_invoice_sends){
            
        }
        
        public void get(Iaccount_invoice_send account_invoice_send){
            this.account_invoice_sendOdooClient.get(account_invoice_send) ;
        }
        
        public void updateBatch(List<Iaccount_invoice_send> account_invoice_sends){
            
        }
        
        public void update(Iaccount_invoice_send account_invoice_send){
this.account_invoice_sendOdooClient.update(account_invoice_send) ;
        }
        
        public Page<Iaccount_invoice_send> select(SearchContext context){
            return null ;
        }
        

}

