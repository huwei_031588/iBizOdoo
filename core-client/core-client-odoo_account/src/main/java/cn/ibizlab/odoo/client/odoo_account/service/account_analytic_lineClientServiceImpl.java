package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_analytic_line;
import cn.ibizlab.odoo.core.client.service.Iaccount_analytic_lineClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_analytic_lineImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_analytic_lineOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_analytic_lineOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_analytic_line] 服务对象接口
 */
@Service
public class account_analytic_lineClientServiceImpl implements Iaccount_analytic_lineClientService {
    @Autowired
    private  Iaccount_analytic_lineOdooClient  account_analytic_lineOdooClient;

    public Iaccount_analytic_line createModel() {		
		return new account_analytic_lineImpl();
	}


        public void get(Iaccount_analytic_line account_analytic_line){
            this.account_analytic_lineOdooClient.get(account_analytic_line) ;
        }
        
        public void create(Iaccount_analytic_line account_analytic_line){
this.account_analytic_lineOdooClient.create(account_analytic_line) ;
        }
        
        public void removeBatch(List<Iaccount_analytic_line> account_analytic_lines){
            
        }
        
        public void createBatch(List<Iaccount_analytic_line> account_analytic_lines){
            
        }
        
        public Page<Iaccount_analytic_line> search(SearchContext context){
            return this.account_analytic_lineOdooClient.search(context) ;
        }
        
        public void remove(Iaccount_analytic_line account_analytic_line){
this.account_analytic_lineOdooClient.remove(account_analytic_line) ;
        }
        
        public void updateBatch(List<Iaccount_analytic_line> account_analytic_lines){
            
        }
        
        public void update(Iaccount_analytic_line account_analytic_line){
this.account_analytic_lineOdooClient.update(account_analytic_line) ;
        }
        
        public Page<Iaccount_analytic_line> select(SearchContext context){
            return null ;
        }
        

}

