package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_invoice] 服务对象客户端接口
 */
public interface Iaccount_invoiceOdooClient {
    
        public void update(Iaccount_invoice account_invoice);

        public void removeBatch(Iaccount_invoice account_invoice);

        public void createBatch(Iaccount_invoice account_invoice);

        public Page<Iaccount_invoice> search(SearchContext context);

        public void create(Iaccount_invoice account_invoice);

        public void updateBatch(Iaccount_invoice account_invoice);

        public void remove(Iaccount_invoice account_invoice);

        public void get(Iaccount_invoice account_invoice);

        public List<Iaccount_invoice> select();


}