package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_position_template;
import cn.ibizlab.odoo.core.client.service.Iaccount_fiscal_position_templateClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_fiscal_position_templateImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_fiscal_position_templateOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_fiscal_position_templateOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_fiscal_position_template] 服务对象接口
 */
@Service
public class account_fiscal_position_templateClientServiceImpl implements Iaccount_fiscal_position_templateClientService {
    @Autowired
    private  Iaccount_fiscal_position_templateOdooClient  account_fiscal_position_templateOdooClient;

    public Iaccount_fiscal_position_template createModel() {		
		return new account_fiscal_position_templateImpl();
	}


        public void remove(Iaccount_fiscal_position_template account_fiscal_position_template){
this.account_fiscal_position_templateOdooClient.remove(account_fiscal_position_template) ;
        }
        
        public void createBatch(List<Iaccount_fiscal_position_template> account_fiscal_position_templates){
            
        }
        
        public void update(Iaccount_fiscal_position_template account_fiscal_position_template){
this.account_fiscal_position_templateOdooClient.update(account_fiscal_position_template) ;
        }
        
        public void removeBatch(List<Iaccount_fiscal_position_template> account_fiscal_position_templates){
            
        }
        
        public void get(Iaccount_fiscal_position_template account_fiscal_position_template){
            this.account_fiscal_position_templateOdooClient.get(account_fiscal_position_template) ;
        }
        
        public Page<Iaccount_fiscal_position_template> search(SearchContext context){
            return this.account_fiscal_position_templateOdooClient.search(context) ;
        }
        
        public void create(Iaccount_fiscal_position_template account_fiscal_position_template){
this.account_fiscal_position_templateOdooClient.create(account_fiscal_position_template) ;
        }
        
        public void updateBatch(List<Iaccount_fiscal_position_template> account_fiscal_position_templates){
            
        }
        
        public Page<Iaccount_fiscal_position_template> select(SearchContext context){
            return null ;
        }
        

}

