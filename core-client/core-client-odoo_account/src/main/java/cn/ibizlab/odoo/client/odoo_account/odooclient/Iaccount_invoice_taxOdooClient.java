package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_tax;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_invoice_tax] 服务对象客户端接口
 */
public interface Iaccount_invoice_taxOdooClient {
    
        public Page<Iaccount_invoice_tax> search(SearchContext context);

        public void updateBatch(Iaccount_invoice_tax account_invoice_tax);

        public void removeBatch(Iaccount_invoice_tax account_invoice_tax);

        public void createBatch(Iaccount_invoice_tax account_invoice_tax);

        public void update(Iaccount_invoice_tax account_invoice_tax);

        public void get(Iaccount_invoice_tax account_invoice_tax);

        public void remove(Iaccount_invoice_tax account_invoice_tax);

        public void create(Iaccount_invoice_tax account_invoice_tax);

        public List<Iaccount_invoice_tax> select();


}