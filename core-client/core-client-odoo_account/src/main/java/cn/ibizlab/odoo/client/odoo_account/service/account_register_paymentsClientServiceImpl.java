package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_register_payments;
import cn.ibizlab.odoo.core.client.service.Iaccount_register_paymentsClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_register_paymentsImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_register_paymentsOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_register_paymentsOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_register_payments] 服务对象接口
 */
@Service
public class account_register_paymentsClientServiceImpl implements Iaccount_register_paymentsClientService {
    @Autowired
    private  Iaccount_register_paymentsOdooClient  account_register_paymentsOdooClient;

    public Iaccount_register_payments createModel() {		
		return new account_register_paymentsImpl();
	}


        public void create(Iaccount_register_payments account_register_payments){
this.account_register_paymentsOdooClient.create(account_register_payments) ;
        }
        
        public void createBatch(List<Iaccount_register_payments> account_register_payments){
            
        }
        
        public void removeBatch(List<Iaccount_register_payments> account_register_payments){
            
        }
        
        public void updateBatch(List<Iaccount_register_payments> account_register_payments){
            
        }
        
        public void remove(Iaccount_register_payments account_register_payments){
this.account_register_paymentsOdooClient.remove(account_register_payments) ;
        }
        
        public void update(Iaccount_register_payments account_register_payments){
this.account_register_paymentsOdooClient.update(account_register_payments) ;
        }
        
        public void get(Iaccount_register_payments account_register_payments){
            this.account_register_paymentsOdooClient.get(account_register_payments) ;
        }
        
        public Page<Iaccount_register_payments> search(SearchContext context){
            return this.account_register_paymentsOdooClient.search(context) ;
        }
        
        public Page<Iaccount_register_payments> select(SearchContext context){
            return null ;
        }
        

}

