package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_cashbox_line;
import cn.ibizlab.odoo.core.client.service.Iaccount_cashbox_lineClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_cashbox_lineImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_cashbox_lineOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_cashbox_lineOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_cashbox_line] 服务对象接口
 */
@Service
public class account_cashbox_lineClientServiceImpl implements Iaccount_cashbox_lineClientService {
    @Autowired
    private  Iaccount_cashbox_lineOdooClient  account_cashbox_lineOdooClient;

    public Iaccount_cashbox_line createModel() {		
		return new account_cashbox_lineImpl();
	}


        public void update(Iaccount_cashbox_line account_cashbox_line){
this.account_cashbox_lineOdooClient.update(account_cashbox_line) ;
        }
        
        public Page<Iaccount_cashbox_line> search(SearchContext context){
            return this.account_cashbox_lineOdooClient.search(context) ;
        }
        
        public void createBatch(List<Iaccount_cashbox_line> account_cashbox_lines){
            
        }
        
        public void get(Iaccount_cashbox_line account_cashbox_line){
            this.account_cashbox_lineOdooClient.get(account_cashbox_line) ;
        }
        
        public void create(Iaccount_cashbox_line account_cashbox_line){
this.account_cashbox_lineOdooClient.create(account_cashbox_line) ;
        }
        
        public void updateBatch(List<Iaccount_cashbox_line> account_cashbox_lines){
            
        }
        
        public void removeBatch(List<Iaccount_cashbox_line> account_cashbox_lines){
            
        }
        
        public void remove(Iaccount_cashbox_line account_cashbox_line){
this.account_cashbox_lineOdooClient.remove(account_cashbox_line) ;
        }
        
        public Page<Iaccount_cashbox_line> select(SearchContext context){
            return null ;
        }
        

}

