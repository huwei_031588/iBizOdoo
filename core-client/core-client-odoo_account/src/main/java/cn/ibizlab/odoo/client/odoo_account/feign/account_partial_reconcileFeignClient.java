package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_partial_reconcile;
import cn.ibizlab.odoo.client.odoo_account.model.account_partial_reconcileImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_partial_reconcile] 服务对象接口
 */
public interface account_partial_reconcileFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_partial_reconciles/{id}")
    public account_partial_reconcileImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_partial_reconciles/search")
    public Page<account_partial_reconcileImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_partial_reconciles/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_partial_reconciles/{id}")
    public account_partial_reconcileImpl update(@PathVariable("id") Integer id,@RequestBody account_partial_reconcileImpl account_partial_reconcile);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_partial_reconciles/updatebatch")
    public account_partial_reconcileImpl updateBatch(@RequestBody List<account_partial_reconcileImpl> account_partial_reconciles);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_partial_reconciles")
    public account_partial_reconcileImpl create(@RequestBody account_partial_reconcileImpl account_partial_reconcile);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_partial_reconciles/removebatch")
    public account_partial_reconcileImpl removeBatch(@RequestBody List<account_partial_reconcileImpl> account_partial_reconciles);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_partial_reconciles/createbatch")
    public account_partial_reconcileImpl createBatch(@RequestBody List<account_partial_reconcileImpl> account_partial_reconciles);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_partial_reconciles/select")
    public Page<account_partial_reconcileImpl> select();



}
