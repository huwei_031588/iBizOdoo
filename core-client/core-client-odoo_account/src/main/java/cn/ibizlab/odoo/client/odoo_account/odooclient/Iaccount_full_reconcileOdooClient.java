package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_full_reconcile;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_full_reconcile] 服务对象客户端接口
 */
public interface Iaccount_full_reconcileOdooClient {
    
        public void create(Iaccount_full_reconcile account_full_reconcile);

        public void get(Iaccount_full_reconcile account_full_reconcile);

        public void removeBatch(Iaccount_full_reconcile account_full_reconcile);

        public void remove(Iaccount_full_reconcile account_full_reconcile);

        public Page<Iaccount_full_reconcile> search(SearchContext context);

        public void updateBatch(Iaccount_full_reconcile account_full_reconcile);

        public void update(Iaccount_full_reconcile account_full_reconcile);

        public void createBatch(Iaccount_full_reconcile account_full_reconcile);

        public List<Iaccount_full_reconcile> select();


}