package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_analytic_group;
import cn.ibizlab.odoo.core.client.service.Iaccount_analytic_groupClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_analytic_groupImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_analytic_groupOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_analytic_groupOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_analytic_group] 服务对象接口
 */
@Service
public class account_analytic_groupClientServiceImpl implements Iaccount_analytic_groupClientService {
    @Autowired
    private  Iaccount_analytic_groupOdooClient  account_analytic_groupOdooClient;

    public Iaccount_analytic_group createModel() {		
		return new account_analytic_groupImpl();
	}


        public void update(Iaccount_analytic_group account_analytic_group){
this.account_analytic_groupOdooClient.update(account_analytic_group) ;
        }
        
        public void remove(Iaccount_analytic_group account_analytic_group){
this.account_analytic_groupOdooClient.remove(account_analytic_group) ;
        }
        
        public void createBatch(List<Iaccount_analytic_group> account_analytic_groups){
            
        }
        
        public void create(Iaccount_analytic_group account_analytic_group){
this.account_analytic_groupOdooClient.create(account_analytic_group) ;
        }
        
        public void get(Iaccount_analytic_group account_analytic_group){
            this.account_analytic_groupOdooClient.get(account_analytic_group) ;
        }
        
        public void updateBatch(List<Iaccount_analytic_group> account_analytic_groups){
            
        }
        
        public void removeBatch(List<Iaccount_analytic_group> account_analytic_groups){
            
        }
        
        public Page<Iaccount_analytic_group> search(SearchContext context){
            return this.account_analytic_groupOdooClient.search(context) ;
        }
        
        public Page<Iaccount_analytic_group> select(SearchContext context){
            return null ;
        }
        

}

