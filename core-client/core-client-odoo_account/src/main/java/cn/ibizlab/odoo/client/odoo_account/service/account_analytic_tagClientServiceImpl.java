package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_analytic_tag;
import cn.ibizlab.odoo.core.client.service.Iaccount_analytic_tagClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_analytic_tagImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_analytic_tagOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_analytic_tagOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_analytic_tag] 服务对象接口
 */
@Service
public class account_analytic_tagClientServiceImpl implements Iaccount_analytic_tagClientService {
    @Autowired
    private  Iaccount_analytic_tagOdooClient  account_analytic_tagOdooClient;

    public Iaccount_analytic_tag createModel() {		
		return new account_analytic_tagImpl();
	}


        public void createBatch(List<Iaccount_analytic_tag> account_analytic_tags){
            
        }
        
        public void updateBatch(List<Iaccount_analytic_tag> account_analytic_tags){
            
        }
        
        public void remove(Iaccount_analytic_tag account_analytic_tag){
this.account_analytic_tagOdooClient.remove(account_analytic_tag) ;
        }
        
        public void update(Iaccount_analytic_tag account_analytic_tag){
this.account_analytic_tagOdooClient.update(account_analytic_tag) ;
        }
        
        public void get(Iaccount_analytic_tag account_analytic_tag){
            this.account_analytic_tagOdooClient.get(account_analytic_tag) ;
        }
        
        public void create(Iaccount_analytic_tag account_analytic_tag){
this.account_analytic_tagOdooClient.create(account_analytic_tag) ;
        }
        
        public Page<Iaccount_analytic_tag> search(SearchContext context){
            return this.account_analytic_tagOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Iaccount_analytic_tag> account_analytic_tags){
            
        }
        
        public Page<Iaccount_analytic_tag> select(SearchContext context){
            return null ;
        }
        

}

