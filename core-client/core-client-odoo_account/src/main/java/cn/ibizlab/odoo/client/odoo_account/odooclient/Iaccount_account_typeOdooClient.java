package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_account_type;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_account_type] 服务对象客户端接口
 */
public interface Iaccount_account_typeOdooClient {
    
        public void createBatch(Iaccount_account_type account_account_type);

        public void removeBatch(Iaccount_account_type account_account_type);

        public void create(Iaccount_account_type account_account_type);

        public Page<Iaccount_account_type> search(SearchContext context);

        public void updateBatch(Iaccount_account_type account_account_type);

        public void remove(Iaccount_account_type account_account_type);

        public void update(Iaccount_account_type account_account_type);

        public void get(Iaccount_account_type account_account_type);

        public List<Iaccount_account_type> select();


}