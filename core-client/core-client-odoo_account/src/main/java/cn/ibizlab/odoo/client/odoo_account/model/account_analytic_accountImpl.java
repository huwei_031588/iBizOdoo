package cn.ibizlab.odoo.client.odoo_account.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iaccount_analytic_account;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[account_analytic_account] 对象
 */
public class account_analytic_accountImpl implements Iaccount_analytic_account,Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 余额
     */
    public Double balance;

    @JsonIgnore
    public boolean balanceDirtyFlag;
    
    /**
     * 参考
     */
    public String code;

    @JsonIgnore
    public boolean codeDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 贷方
     */
    public Double credit;

    @JsonIgnore
    public boolean creditDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 借方
     */
    public Double debit;

    @JsonIgnore
    public boolean debitDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 组
     */
    public Integer group_id;

    @JsonIgnore
    public boolean group_idDirtyFlag;
    
    /**
     * 组
     */
    public String group_id_text;

    @JsonIgnore
    public boolean group_id_textDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 分析明细行
     */
    public String line_ids;

    @JsonIgnore
    public boolean line_idsDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 需要一个动作消息的编码
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 是关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要采取行动
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 操作编号
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 分析账户
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 客户
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 客户
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 网站信息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [余额]
     */
    @JsonProperty("balance")
    public Double getBalance(){
        return this.balance ;
    }

    /**
     * 设置 [余额]
     */
    @JsonProperty("balance")
    public void setBalance(Double  balance){
        this.balance = balance ;
        this.balanceDirtyFlag = true ;
    }

     /**
     * 获取 [余额]脏标记
     */
    @JsonIgnore
    public boolean getBalanceDirtyFlag(){
        return this.balanceDirtyFlag ;
    }   

    /**
     * 获取 [参考]
     */
    @JsonProperty("code")
    public String getCode(){
        return this.code ;
    }

    /**
     * 设置 [参考]
     */
    @JsonProperty("code")
    public void setCode(String  code){
        this.code = code ;
        this.codeDirtyFlag = true ;
    }

     /**
     * 获取 [参考]脏标记
     */
    @JsonIgnore
    public boolean getCodeDirtyFlag(){
        return this.codeDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [贷方]
     */
    @JsonProperty("credit")
    public Double getCredit(){
        return this.credit ;
    }

    /**
     * 设置 [贷方]
     */
    @JsonProperty("credit")
    public void setCredit(Double  credit){
        this.credit = credit ;
        this.creditDirtyFlag = true ;
    }

     /**
     * 获取 [贷方]脏标记
     */
    @JsonIgnore
    public boolean getCreditDirtyFlag(){
        return this.creditDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [借方]
     */
    @JsonProperty("debit")
    public Double getDebit(){
        return this.debit ;
    }

    /**
     * 设置 [借方]
     */
    @JsonProperty("debit")
    public void setDebit(Double  debit){
        this.debit = debit ;
        this.debitDirtyFlag = true ;
    }

     /**
     * 获取 [借方]脏标记
     */
    @JsonIgnore
    public boolean getDebitDirtyFlag(){
        return this.debitDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [组]
     */
    @JsonProperty("group_id")
    public Integer getGroup_id(){
        return this.group_id ;
    }

    /**
     * 设置 [组]
     */
    @JsonProperty("group_id")
    public void setGroup_id(Integer  group_id){
        this.group_id = group_id ;
        this.group_idDirtyFlag = true ;
    }

     /**
     * 获取 [组]脏标记
     */
    @JsonIgnore
    public boolean getGroup_idDirtyFlag(){
        return this.group_idDirtyFlag ;
    }   

    /**
     * 获取 [组]
     */
    @JsonProperty("group_id_text")
    public String getGroup_id_text(){
        return this.group_id_text ;
    }

    /**
     * 设置 [组]
     */
    @JsonProperty("group_id_text")
    public void setGroup_id_text(String  group_id_text){
        this.group_id_text = group_id_text ;
        this.group_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [组]脏标记
     */
    @JsonIgnore
    public boolean getGroup_id_textDirtyFlag(){
        return this.group_id_textDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [分析明细行]
     */
    @JsonProperty("line_ids")
    public String getLine_ids(){
        return this.line_ids ;
    }

    /**
     * 设置 [分析明细行]
     */
    @JsonProperty("line_ids")
    public void setLine_ids(String  line_ids){
        this.line_ids = line_ids ;
        this.line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [分析明细行]脏标记
     */
    @JsonIgnore
    public boolean getLine_idsDirtyFlag(){
        return this.line_idsDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [需要一个动作消息的编码]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [需要一个动作消息的编码]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [需要一个动作消息的编码]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [操作编号]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [操作编号]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [操作编号]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [分析账户]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [分析账户]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [分析账户]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [客户]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [客户]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [客户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [客户]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [客户]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [客户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站信息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(map.get("active") instanceof Boolean){
			this.setActive(((Boolean)map.get("active"))? "true" : "false");
		}
		if(!(map.get("balance") instanceof Boolean)&& map.get("balance")!=null){
			this.setBalance((Double)map.get("balance"));
		}
		if(!(map.get("code") instanceof Boolean)&& map.get("code")!=null){
			this.setCode((String)map.get("code"));
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 1){
				this.setCompany_id_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("credit") instanceof Boolean)&& map.get("credit")!=null){
			this.setCredit((Double)map.get("credit"));
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 0){
				this.setCurrency_id((Integer)objs[0]);
			}
		}
		if(!(map.get("debit") instanceof Boolean)&& map.get("debit")!=null){
			this.setDebit((Double)map.get("debit"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("group_id") instanceof Boolean)&& map.get("group_id")!=null){
			Object[] objs = (Object[])map.get("group_id");
			if(objs.length > 0){
				this.setGroup_id((Integer)objs[0]);
			}
		}
		if(!(map.get("group_id") instanceof Boolean)&& map.get("group_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("group_id");
			if(objs.length > 1){
				this.setGroup_id_text((String)objs[1]);
			}
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("line_ids") instanceof Boolean)&& map.get("line_ids")!=null){
			Object[] objs = (Object[])map.get("line_ids");
			if(objs.length > 0){
				Integer[] line_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setLine_ids(Arrays.toString(line_ids));
			}
		}
		if(!(map.get("message_attachment_count") instanceof Boolean)&& map.get("message_attachment_count")!=null){
			this.setMessage_attachment_count((Integer)map.get("message_attachment_count"));
		}
		if(!(map.get("message_channel_ids") instanceof Boolean)&& map.get("message_channel_ids")!=null){
			Object[] objs = (Object[])map.get("message_channel_ids");
			if(objs.length > 0){
				Integer[] message_channel_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_channel_ids(Arrays.toString(message_channel_ids));
			}
		}
		if(!(map.get("message_follower_ids") instanceof Boolean)&& map.get("message_follower_ids")!=null){
			Object[] objs = (Object[])map.get("message_follower_ids");
			if(objs.length > 0){
				Integer[] message_follower_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_follower_ids(Arrays.toString(message_follower_ids));
			}
		}
		if(map.get("message_has_error") instanceof Boolean){
			this.setMessage_has_error(((Boolean)map.get("message_has_error"))? "true" : "false");
		}
		if(!(map.get("message_has_error_counter") instanceof Boolean)&& map.get("message_has_error_counter")!=null){
			this.setMessage_has_error_counter((Integer)map.get("message_has_error_counter"));
		}
		if(!(map.get("message_ids") instanceof Boolean)&& map.get("message_ids")!=null){
			Object[] objs = (Object[])map.get("message_ids");
			if(objs.length > 0){
				Integer[] message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_ids(Arrays.toString(message_ids));
			}
		}
		if(map.get("message_is_follower") instanceof Boolean){
			this.setMessage_is_follower(((Boolean)map.get("message_is_follower"))? "true" : "false");
		}
		if(!(map.get("message_main_attachment_id") instanceof Boolean)&& map.get("message_main_attachment_id")!=null){
			Object[] objs = (Object[])map.get("message_main_attachment_id");
			if(objs.length > 0){
				this.setMessage_main_attachment_id((Integer)objs[0]);
			}
		}
		if(map.get("message_needaction") instanceof Boolean){
			this.setMessage_needaction(((Boolean)map.get("message_needaction"))? "true" : "false");
		}
		if(!(map.get("message_needaction_counter") instanceof Boolean)&& map.get("message_needaction_counter")!=null){
			this.setMessage_needaction_counter((Integer)map.get("message_needaction_counter"));
		}
		if(!(map.get("message_partner_ids") instanceof Boolean)&& map.get("message_partner_ids")!=null){
			Object[] objs = (Object[])map.get("message_partner_ids");
			if(objs.length > 0){
				Integer[] message_partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_partner_ids(Arrays.toString(message_partner_ids));
			}
		}
		if(map.get("message_unread") instanceof Boolean){
			this.setMessage_unread(((Boolean)map.get("message_unread"))? "true" : "false");
		}
		if(!(map.get("message_unread_counter") instanceof Boolean)&& map.get("message_unread_counter")!=null){
			this.setMessage_unread_counter((Integer)map.get("message_unread_counter"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 0){
				this.setPartner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 1){
				this.setPartner_id_text((String)objs[1]);
			}
		}
		if(!(map.get("website_message_ids") instanceof Boolean)&& map.get("website_message_ids")!=null){
			Object[] objs = (Object[])map.get("website_message_ids");
			if(objs.length > 0){
				Integer[] website_message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setWebsite_message_ids(Arrays.toString(website_message_ids));
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getActive()!=null&&this.getActiveDirtyFlag()){
			map.put("active",Boolean.parseBoolean(this.getActive()));		
		}		if(this.getBalance()!=null&&this.getBalanceDirtyFlag()){
			map.put("balance",this.getBalance());
		}else if(this.getBalanceDirtyFlag()){
			map.put("balance",false);
		}
		if(this.getCode()!=null&&this.getCodeDirtyFlag()){
			map.put("code",this.getCode());
		}else if(this.getCodeDirtyFlag()){
			map.put("code",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_id_text()!=null&&this.getCompany_id_textDirtyFlag()){
			//忽略文本外键company_id_text
		}else if(this.getCompany_id_textDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCredit()!=null&&this.getCreditDirtyFlag()){
			map.put("credit",this.getCredit());
		}else if(this.getCreditDirtyFlag()){
			map.put("credit",false);
		}
		if(this.getCurrency_id()!=null&&this.getCurrency_idDirtyFlag()){
			map.put("currency_id",this.getCurrency_id());
		}else if(this.getCurrency_idDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getDebit()!=null&&this.getDebitDirtyFlag()){
			map.put("debit",this.getDebit());
		}else if(this.getDebitDirtyFlag()){
			map.put("debit",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getGroup_id()!=null&&this.getGroup_idDirtyFlag()){
			map.put("group_id",this.getGroup_id());
		}else if(this.getGroup_idDirtyFlag()){
			map.put("group_id",false);
		}
		if(this.getGroup_id_text()!=null&&this.getGroup_id_textDirtyFlag()){
			//忽略文本外键group_id_text
		}else if(this.getGroup_id_textDirtyFlag()){
			map.put("group_id",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getLine_ids()!=null&&this.getLine_idsDirtyFlag()){
			map.put("line_ids",this.getLine_ids());
		}else if(this.getLine_idsDirtyFlag()){
			map.put("line_ids",false);
		}
		if(this.getMessage_attachment_count()!=null&&this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",this.getMessage_attachment_count());
		}else if(this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",false);
		}
		if(this.getMessage_channel_ids()!=null&&this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",this.getMessage_channel_ids());
		}else if(this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",false);
		}
		if(this.getMessage_follower_ids()!=null&&this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",this.getMessage_follower_ids());
		}else if(this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",false);
		}
		if(this.getMessage_has_error()!=null&&this.getMessage_has_errorDirtyFlag()){
			map.put("message_has_error",Boolean.parseBoolean(this.getMessage_has_error()));		
		}		if(this.getMessage_has_error_counter()!=null&&this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",this.getMessage_has_error_counter());
		}else if(this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",false);
		}
		if(this.getMessage_ids()!=null&&this.getMessage_idsDirtyFlag()){
			map.put("message_ids",this.getMessage_ids());
		}else if(this.getMessage_idsDirtyFlag()){
			map.put("message_ids",false);
		}
		if(this.getMessage_is_follower()!=null&&this.getMessage_is_followerDirtyFlag()){
			map.put("message_is_follower",Boolean.parseBoolean(this.getMessage_is_follower()));		
		}		if(this.getMessage_main_attachment_id()!=null&&this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",this.getMessage_main_attachment_id());
		}else if(this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",false);
		}
		if(this.getMessage_needaction()!=null&&this.getMessage_needactionDirtyFlag()){
			map.put("message_needaction",Boolean.parseBoolean(this.getMessage_needaction()));		
		}		if(this.getMessage_needaction_counter()!=null&&this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",this.getMessage_needaction_counter());
		}else if(this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",false);
		}
		if(this.getMessage_partner_ids()!=null&&this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",this.getMessage_partner_ids());
		}else if(this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",false);
		}
		if(this.getMessage_unread()!=null&&this.getMessage_unreadDirtyFlag()){
			map.put("message_unread",Boolean.parseBoolean(this.getMessage_unread()));		
		}		if(this.getMessage_unread_counter()!=null&&this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",this.getMessage_unread_counter());
		}else if(this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getPartner_id()!=null&&this.getPartner_idDirtyFlag()){
			map.put("partner_id",this.getPartner_id());
		}else if(this.getPartner_idDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPartner_id_text()!=null&&this.getPartner_id_textDirtyFlag()){
			//忽略文本外键partner_id_text
		}else if(this.getPartner_id_textDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getWebsite_message_ids()!=null&&this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",this.getWebsite_message_ids());
		}else if(this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
