package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_reconcile_model;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_reconcile_model] 服务对象客户端接口
 */
public interface Iaccount_reconcile_modelOdooClient {
    
        public void remove(Iaccount_reconcile_model account_reconcile_model);

        public void create(Iaccount_reconcile_model account_reconcile_model);

        public void update(Iaccount_reconcile_model account_reconcile_model);

        public void updateBatch(Iaccount_reconcile_model account_reconcile_model);

        public void createBatch(Iaccount_reconcile_model account_reconcile_model);

        public Page<Iaccount_reconcile_model> search(SearchContext context);

        public void get(Iaccount_reconcile_model account_reconcile_model);

        public void removeBatch(Iaccount_reconcile_model account_reconcile_model);

        public List<Iaccount_reconcile_model> select();


}