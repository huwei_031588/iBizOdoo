package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_journal;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_journal] 服务对象客户端接口
 */
public interface Iaccount_journalOdooClient {
    
        public void updateBatch(Iaccount_journal account_journal);

        public void get(Iaccount_journal account_journal);

        public void create(Iaccount_journal account_journal);

        public void createBatch(Iaccount_journal account_journal);

        public Page<Iaccount_journal> search(SearchContext context);

        public void update(Iaccount_journal account_journal);

        public void remove(Iaccount_journal account_journal);

        public void removeBatch(Iaccount_journal account_journal);

        public List<Iaccount_journal> select();


}