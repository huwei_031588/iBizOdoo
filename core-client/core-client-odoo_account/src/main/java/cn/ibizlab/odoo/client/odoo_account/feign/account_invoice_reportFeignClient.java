package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_report;
import cn.ibizlab.odoo.client.odoo_account.model.account_invoice_reportImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_invoice_report] 服务对象接口
 */
public interface account_invoice_reportFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_reports/createbatch")
    public account_invoice_reportImpl createBatch(@RequestBody List<account_invoice_reportImpl> account_invoice_reports);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_reports/removebatch")
    public account_invoice_reportImpl removeBatch(@RequestBody List<account_invoice_reportImpl> account_invoice_reports);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_reports/updatebatch")
    public account_invoice_reportImpl updateBatch(@RequestBody List<account_invoice_reportImpl> account_invoice_reports);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_reports/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_reports/{id}")
    public account_invoice_reportImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_reports")
    public account_invoice_reportImpl create(@RequestBody account_invoice_reportImpl account_invoice_report);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_reports/{id}")
    public account_invoice_reportImpl update(@PathVariable("id") Integer id,@RequestBody account_invoice_reportImpl account_invoice_report);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_reports/search")
    public Page<account_invoice_reportImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_reports/select")
    public Page<account_invoice_reportImpl> select();



}
