package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_tax;
import cn.ibizlab.odoo.client.odoo_account.model.account_invoice_taxImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_invoice_tax] 服务对象接口
 */
public interface account_invoice_taxFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_taxes/search")
    public Page<account_invoice_taxImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_taxes/updatebatch")
    public account_invoice_taxImpl updateBatch(@RequestBody List<account_invoice_taxImpl> account_invoice_taxes);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_taxes/removebatch")
    public account_invoice_taxImpl removeBatch(@RequestBody List<account_invoice_taxImpl> account_invoice_taxes);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_taxes/createbatch")
    public account_invoice_taxImpl createBatch(@RequestBody List<account_invoice_taxImpl> account_invoice_taxes);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_taxes/{id}")
    public account_invoice_taxImpl update(@PathVariable("id") Integer id,@RequestBody account_invoice_taxImpl account_invoice_tax);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_taxes/{id}")
    public account_invoice_taxImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_taxes/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_taxes")
    public account_invoice_taxImpl create(@RequestBody account_invoice_taxImpl account_invoice_tax);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_taxes/select")
    public Page<account_invoice_taxImpl> select();



}
