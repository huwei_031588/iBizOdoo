package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement_import;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_bank_statement_import] 服务对象客户端接口
 */
public interface Iaccount_bank_statement_importOdooClient {
    
        public void update(Iaccount_bank_statement_import account_bank_statement_import);

        public void get(Iaccount_bank_statement_import account_bank_statement_import);

        public void create(Iaccount_bank_statement_import account_bank_statement_import);

        public void createBatch(Iaccount_bank_statement_import account_bank_statement_import);

        public void updateBatch(Iaccount_bank_statement_import account_bank_statement_import);

        public void remove(Iaccount_bank_statement_import account_bank_statement_import);

        public void removeBatch(Iaccount_bank_statement_import account_bank_statement_import);

        public Page<Iaccount_bank_statement_import> search(SearchContext context);

        public List<Iaccount_bank_statement_import> select();


}