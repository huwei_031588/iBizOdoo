package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_move;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_move] 服务对象客户端接口
 */
public interface Iaccount_moveOdooClient {
    
        public void updateBatch(Iaccount_move account_move);

        public void update(Iaccount_move account_move);

        public Page<Iaccount_move> search(SearchContext context);

        public void get(Iaccount_move account_move);

        public void create(Iaccount_move account_move);

        public void remove(Iaccount_move account_move);

        public void removeBatch(Iaccount_move account_move);

        public void createBatch(Iaccount_move account_move);

        public List<Iaccount_move> select();


}