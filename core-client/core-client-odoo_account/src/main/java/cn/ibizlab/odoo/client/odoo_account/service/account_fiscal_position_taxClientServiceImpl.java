package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_position_tax;
import cn.ibizlab.odoo.core.client.service.Iaccount_fiscal_position_taxClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_fiscal_position_taxImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_fiscal_position_taxOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_fiscal_position_taxOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_fiscal_position_tax] 服务对象接口
 */
@Service
public class account_fiscal_position_taxClientServiceImpl implements Iaccount_fiscal_position_taxClientService {
    @Autowired
    private  Iaccount_fiscal_position_taxOdooClient  account_fiscal_position_taxOdooClient;

    public Iaccount_fiscal_position_tax createModel() {		
		return new account_fiscal_position_taxImpl();
	}


        public Page<Iaccount_fiscal_position_tax> search(SearchContext context){
            return this.account_fiscal_position_taxOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Iaccount_fiscal_position_tax> account_fiscal_position_taxes){
            
        }
        
        public void updateBatch(List<Iaccount_fiscal_position_tax> account_fiscal_position_taxes){
            
        }
        
        public void get(Iaccount_fiscal_position_tax account_fiscal_position_tax){
            this.account_fiscal_position_taxOdooClient.get(account_fiscal_position_tax) ;
        }
        
        public void update(Iaccount_fiscal_position_tax account_fiscal_position_tax){
this.account_fiscal_position_taxOdooClient.update(account_fiscal_position_tax) ;
        }
        
        public void createBatch(List<Iaccount_fiscal_position_tax> account_fiscal_position_taxes){
            
        }
        
        public void create(Iaccount_fiscal_position_tax account_fiscal_position_tax){
this.account_fiscal_position_taxOdooClient.create(account_fiscal_position_tax) ;
        }
        
        public void remove(Iaccount_fiscal_position_tax account_fiscal_position_tax){
this.account_fiscal_position_taxOdooClient.remove(account_fiscal_position_tax) ;
        }
        
        public Page<Iaccount_fiscal_position_tax> select(SearchContext context){
            return null ;
        }
        

}

