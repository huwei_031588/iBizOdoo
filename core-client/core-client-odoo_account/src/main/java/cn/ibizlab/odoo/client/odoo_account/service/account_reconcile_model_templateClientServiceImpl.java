package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_reconcile_model_template;
import cn.ibizlab.odoo.core.client.service.Iaccount_reconcile_model_templateClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_reconcile_model_templateImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_reconcile_model_templateOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_reconcile_model_templateOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_reconcile_model_template] 服务对象接口
 */
@Service
public class account_reconcile_model_templateClientServiceImpl implements Iaccount_reconcile_model_templateClientService {
    @Autowired
    private  Iaccount_reconcile_model_templateOdooClient  account_reconcile_model_templateOdooClient;

    public Iaccount_reconcile_model_template createModel() {		
		return new account_reconcile_model_templateImpl();
	}


        public void remove(Iaccount_reconcile_model_template account_reconcile_model_template){
this.account_reconcile_model_templateOdooClient.remove(account_reconcile_model_template) ;
        }
        
        public void create(Iaccount_reconcile_model_template account_reconcile_model_template){
this.account_reconcile_model_templateOdooClient.create(account_reconcile_model_template) ;
        }
        
        public Page<Iaccount_reconcile_model_template> search(SearchContext context){
            return this.account_reconcile_model_templateOdooClient.search(context) ;
        }
        
        public void update(Iaccount_reconcile_model_template account_reconcile_model_template){
this.account_reconcile_model_templateOdooClient.update(account_reconcile_model_template) ;
        }
        
        public void updateBatch(List<Iaccount_reconcile_model_template> account_reconcile_model_templates){
            
        }
        
        public void get(Iaccount_reconcile_model_template account_reconcile_model_template){
            this.account_reconcile_model_templateOdooClient.get(account_reconcile_model_template) ;
        }
        
        public void removeBatch(List<Iaccount_reconcile_model_template> account_reconcile_model_templates){
            
        }
        
        public void createBatch(List<Iaccount_reconcile_model_template> account_reconcile_model_templates){
            
        }
        
        public Page<Iaccount_reconcile_model_template> select(SearchContext context){
            return null ;
        }
        

}

