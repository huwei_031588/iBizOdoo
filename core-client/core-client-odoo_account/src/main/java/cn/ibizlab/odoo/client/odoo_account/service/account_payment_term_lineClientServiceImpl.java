package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_payment_term_line;
import cn.ibizlab.odoo.core.client.service.Iaccount_payment_term_lineClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_payment_term_lineImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_payment_term_lineOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_payment_term_lineOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_payment_term_line] 服务对象接口
 */
@Service
public class account_payment_term_lineClientServiceImpl implements Iaccount_payment_term_lineClientService {
    @Autowired
    private  Iaccount_payment_term_lineOdooClient  account_payment_term_lineOdooClient;

    public Iaccount_payment_term_line createModel() {		
		return new account_payment_term_lineImpl();
	}


        public void createBatch(List<Iaccount_payment_term_line> account_payment_term_lines){
            
        }
        
        public void removeBatch(List<Iaccount_payment_term_line> account_payment_term_lines){
            
        }
        
        public void updateBatch(List<Iaccount_payment_term_line> account_payment_term_lines){
            
        }
        
        public void get(Iaccount_payment_term_line account_payment_term_line){
            this.account_payment_term_lineOdooClient.get(account_payment_term_line) ;
        }
        
        public Page<Iaccount_payment_term_line> search(SearchContext context){
            return this.account_payment_term_lineOdooClient.search(context) ;
        }
        
        public void create(Iaccount_payment_term_line account_payment_term_line){
this.account_payment_term_lineOdooClient.create(account_payment_term_line) ;
        }
        
        public void update(Iaccount_payment_term_line account_payment_term_line){
this.account_payment_term_lineOdooClient.update(account_payment_term_line) ;
        }
        
        public void remove(Iaccount_payment_term_line account_payment_term_line){
this.account_payment_term_lineOdooClient.remove(account_payment_term_line) ;
        }
        
        public Page<Iaccount_payment_term_line> select(SearchContext context){
            return null ;
        }
        

}

