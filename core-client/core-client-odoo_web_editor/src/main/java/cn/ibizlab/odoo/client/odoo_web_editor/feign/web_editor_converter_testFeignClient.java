package cn.ibizlab.odoo.client.odoo_web_editor.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iweb_editor_converter_test;
import cn.ibizlab.odoo.client.odoo_web_editor.model.web_editor_converter_testImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[web_editor_converter_test] 服务对象接口
 */
public interface web_editor_converter_testFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_web_editor/web_editor_converter_tests/search")
    public Page<web_editor_converter_testImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_web_editor/web_editor_converter_tests/removebatch")
    public web_editor_converter_testImpl removeBatch(@RequestBody List<web_editor_converter_testImpl> web_editor_converter_tests);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_web_editor/web_editor_converter_tests/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_web_editor/web_editor_converter_tests/{id}")
    public web_editor_converter_testImpl update(@PathVariable("id") Integer id,@RequestBody web_editor_converter_testImpl web_editor_converter_test);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_web_editor/web_editor_converter_tests/createbatch")
    public web_editor_converter_testImpl createBatch(@RequestBody List<web_editor_converter_testImpl> web_editor_converter_tests);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_web_editor/web_editor_converter_tests")
    public web_editor_converter_testImpl create(@RequestBody web_editor_converter_testImpl web_editor_converter_test);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_web_editor/web_editor_converter_tests/{id}")
    public web_editor_converter_testImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_web_editor/web_editor_converter_tests/updatebatch")
    public web_editor_converter_testImpl updateBatch(@RequestBody List<web_editor_converter_testImpl> web_editor_converter_tests);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_web_editor/web_editor_converter_tests/select")
    public Page<web_editor_converter_testImpl> select();



}
