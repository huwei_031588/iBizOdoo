package cn.ibizlab.odoo.client.odoo_im_livechat.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iim_livechat_channel;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[im_livechat_channel] 对象
 */
public class im_livechat_channelImpl implements Iim_livechat_channel,Serializable{

    /**
     * 您是否在频道中？
     */
    public String are_you_inside;

    @JsonIgnore
    public boolean are_you_insideDirtyFlag;
    
    /**
     * 按钮的文本
     */
    public String button_text;

    @JsonIgnore
    public boolean button_textDirtyFlag;
    
    /**
     * 会话
     */
    public String channel_ids;

    @JsonIgnore
    public boolean channel_idsDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 欢迎信息
     */
    public String default_message;

    @JsonIgnore
    public boolean default_messageDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 图像
     */
    public byte[] image;

    @JsonIgnore
    public boolean imageDirtyFlag;
    
    /**
     * 普通
     */
    public byte[] image_medium;

    @JsonIgnore
    public boolean image_mediumDirtyFlag;
    
    /**
     * 缩略图
     */
    public byte[] image_small;

    @JsonIgnore
    public boolean image_smallDirtyFlag;
    
    /**
     * 聊天输入为空时显示
     */
    public String input_placeholder;

    @JsonIgnore
    public boolean input_placeholderDirtyFlag;
    
    /**
     * 已发布
     */
    public String is_published;

    @JsonIgnore
    public boolean is_publishedDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 对话数
     */
    public Integer nbr_channel;

    @JsonIgnore
    public boolean nbr_channelDirtyFlag;
    
    /**
     * % 高兴
     */
    public Integer rating_percentage_satisfaction;

    @JsonIgnore
    public boolean rating_percentage_satisfactionDirtyFlag;
    
    /**
     * 规则
     */
    public String rule_ids;

    @JsonIgnore
    public boolean rule_idsDirtyFlag;
    
    /**
     * 脚本（外部）
     */
    public String script_external;

    @JsonIgnore
    public boolean script_externalDirtyFlag;
    
    /**
     * 操作员
     */
    public String user_ids;

    @JsonIgnore
    public boolean user_idsDirtyFlag;
    
    /**
     * 网站说明
     */
    public String website_description;

    @JsonIgnore
    public boolean website_descriptionDirtyFlag;
    
    /**
     * 在当前网站显示
     */
    public String website_published;

    @JsonIgnore
    public boolean website_publishedDirtyFlag;
    
    /**
     * 网站网址
     */
    public String website_url;

    @JsonIgnore
    public boolean website_urlDirtyFlag;
    
    /**
     * Web页
     */
    public String web_page;

    @JsonIgnore
    public boolean web_pageDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [您是否在频道中？]
     */
    @JsonProperty("are_you_inside")
    public String getAre_you_inside(){
        return this.are_you_inside ;
    }

    /**
     * 设置 [您是否在频道中？]
     */
    @JsonProperty("are_you_inside")
    public void setAre_you_inside(String  are_you_inside){
        this.are_you_inside = are_you_inside ;
        this.are_you_insideDirtyFlag = true ;
    }

     /**
     * 获取 [您是否在频道中？]脏标记
     */
    @JsonIgnore
    public boolean getAre_you_insideDirtyFlag(){
        return this.are_you_insideDirtyFlag ;
    }   

    /**
     * 获取 [按钮的文本]
     */
    @JsonProperty("button_text")
    public String getButton_text(){
        return this.button_text ;
    }

    /**
     * 设置 [按钮的文本]
     */
    @JsonProperty("button_text")
    public void setButton_text(String  button_text){
        this.button_text = button_text ;
        this.button_textDirtyFlag = true ;
    }

     /**
     * 获取 [按钮的文本]脏标记
     */
    @JsonIgnore
    public boolean getButton_textDirtyFlag(){
        return this.button_textDirtyFlag ;
    }   

    /**
     * 获取 [会话]
     */
    @JsonProperty("channel_ids")
    public String getChannel_ids(){
        return this.channel_ids ;
    }

    /**
     * 设置 [会话]
     */
    @JsonProperty("channel_ids")
    public void setChannel_ids(String  channel_ids){
        this.channel_ids = channel_ids ;
        this.channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [会话]脏标记
     */
    @JsonIgnore
    public boolean getChannel_idsDirtyFlag(){
        return this.channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [欢迎信息]
     */
    @JsonProperty("default_message")
    public String getDefault_message(){
        return this.default_message ;
    }

    /**
     * 设置 [欢迎信息]
     */
    @JsonProperty("default_message")
    public void setDefault_message(String  default_message){
        this.default_message = default_message ;
        this.default_messageDirtyFlag = true ;
    }

     /**
     * 获取 [欢迎信息]脏标记
     */
    @JsonIgnore
    public boolean getDefault_messageDirtyFlag(){
        return this.default_messageDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [图像]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return this.image ;
    }

    /**
     * 设置 [图像]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

     /**
     * 获取 [图像]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return this.imageDirtyFlag ;
    }   

    /**
     * 获取 [普通]
     */
    @JsonProperty("image_medium")
    public byte[] getImage_medium(){
        return this.image_medium ;
    }

    /**
     * 设置 [普通]
     */
    @JsonProperty("image_medium")
    public void setImage_medium(byte[]  image_medium){
        this.image_medium = image_medium ;
        this.image_mediumDirtyFlag = true ;
    }

     /**
     * 获取 [普通]脏标记
     */
    @JsonIgnore
    public boolean getImage_mediumDirtyFlag(){
        return this.image_mediumDirtyFlag ;
    }   

    /**
     * 获取 [缩略图]
     */
    @JsonProperty("image_small")
    public byte[] getImage_small(){
        return this.image_small ;
    }

    /**
     * 设置 [缩略图]
     */
    @JsonProperty("image_small")
    public void setImage_small(byte[]  image_small){
        this.image_small = image_small ;
        this.image_smallDirtyFlag = true ;
    }

     /**
     * 获取 [缩略图]脏标记
     */
    @JsonIgnore
    public boolean getImage_smallDirtyFlag(){
        return this.image_smallDirtyFlag ;
    }   

    /**
     * 获取 [聊天输入为空时显示]
     */
    @JsonProperty("input_placeholder")
    public String getInput_placeholder(){
        return this.input_placeholder ;
    }

    /**
     * 设置 [聊天输入为空时显示]
     */
    @JsonProperty("input_placeholder")
    public void setInput_placeholder(String  input_placeholder){
        this.input_placeholder = input_placeholder ;
        this.input_placeholderDirtyFlag = true ;
    }

     /**
     * 获取 [聊天输入为空时显示]脏标记
     */
    @JsonIgnore
    public boolean getInput_placeholderDirtyFlag(){
        return this.input_placeholderDirtyFlag ;
    }   

    /**
     * 获取 [已发布]
     */
    @JsonProperty("is_published")
    public String getIs_published(){
        return this.is_published ;
    }

    /**
     * 设置 [已发布]
     */
    @JsonProperty("is_published")
    public void setIs_published(String  is_published){
        this.is_published = is_published ;
        this.is_publishedDirtyFlag = true ;
    }

     /**
     * 获取 [已发布]脏标记
     */
    @JsonIgnore
    public boolean getIs_publishedDirtyFlag(){
        return this.is_publishedDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [对话数]
     */
    @JsonProperty("nbr_channel")
    public Integer getNbr_channel(){
        return this.nbr_channel ;
    }

    /**
     * 设置 [对话数]
     */
    @JsonProperty("nbr_channel")
    public void setNbr_channel(Integer  nbr_channel){
        this.nbr_channel = nbr_channel ;
        this.nbr_channelDirtyFlag = true ;
    }

     /**
     * 获取 [对话数]脏标记
     */
    @JsonIgnore
    public boolean getNbr_channelDirtyFlag(){
        return this.nbr_channelDirtyFlag ;
    }   

    /**
     * 获取 [% 高兴]
     */
    @JsonProperty("rating_percentage_satisfaction")
    public Integer getRating_percentage_satisfaction(){
        return this.rating_percentage_satisfaction ;
    }

    /**
     * 设置 [% 高兴]
     */
    @JsonProperty("rating_percentage_satisfaction")
    public void setRating_percentage_satisfaction(Integer  rating_percentage_satisfaction){
        this.rating_percentage_satisfaction = rating_percentage_satisfaction ;
        this.rating_percentage_satisfactionDirtyFlag = true ;
    }

     /**
     * 获取 [% 高兴]脏标记
     */
    @JsonIgnore
    public boolean getRating_percentage_satisfactionDirtyFlag(){
        return this.rating_percentage_satisfactionDirtyFlag ;
    }   

    /**
     * 获取 [规则]
     */
    @JsonProperty("rule_ids")
    public String getRule_ids(){
        return this.rule_ids ;
    }

    /**
     * 设置 [规则]
     */
    @JsonProperty("rule_ids")
    public void setRule_ids(String  rule_ids){
        this.rule_ids = rule_ids ;
        this.rule_idsDirtyFlag = true ;
    }

     /**
     * 获取 [规则]脏标记
     */
    @JsonIgnore
    public boolean getRule_idsDirtyFlag(){
        return this.rule_idsDirtyFlag ;
    }   

    /**
     * 获取 [脚本（外部）]
     */
    @JsonProperty("script_external")
    public String getScript_external(){
        return this.script_external ;
    }

    /**
     * 设置 [脚本（外部）]
     */
    @JsonProperty("script_external")
    public void setScript_external(String  script_external){
        this.script_external = script_external ;
        this.script_externalDirtyFlag = true ;
    }

     /**
     * 获取 [脚本（外部）]脏标记
     */
    @JsonIgnore
    public boolean getScript_externalDirtyFlag(){
        return this.script_externalDirtyFlag ;
    }   

    /**
     * 获取 [操作员]
     */
    @JsonProperty("user_ids")
    public String getUser_ids(){
        return this.user_ids ;
    }

    /**
     * 设置 [操作员]
     */
    @JsonProperty("user_ids")
    public void setUser_ids(String  user_ids){
        this.user_ids = user_ids ;
        this.user_idsDirtyFlag = true ;
    }

     /**
     * 获取 [操作员]脏标记
     */
    @JsonIgnore
    public boolean getUser_idsDirtyFlag(){
        return this.user_idsDirtyFlag ;
    }   

    /**
     * 获取 [网站说明]
     */
    @JsonProperty("website_description")
    public String getWebsite_description(){
        return this.website_description ;
    }

    /**
     * 设置 [网站说明]
     */
    @JsonProperty("website_description")
    public void setWebsite_description(String  website_description){
        this.website_description = website_description ;
        this.website_descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [网站说明]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_descriptionDirtyFlag(){
        return this.website_descriptionDirtyFlag ;
    }   

    /**
     * 获取 [在当前网站显示]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return this.website_published ;
    }

    /**
     * 设置 [在当前网站显示]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

     /**
     * 获取 [在当前网站显示]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return this.website_publishedDirtyFlag ;
    }   

    /**
     * 获取 [网站网址]
     */
    @JsonProperty("website_url")
    public String getWebsite_url(){
        return this.website_url ;
    }

    /**
     * 设置 [网站网址]
     */
    @JsonProperty("website_url")
    public void setWebsite_url(String  website_url){
        this.website_url = website_url ;
        this.website_urlDirtyFlag = true ;
    }

     /**
     * 获取 [网站网址]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_urlDirtyFlag(){
        return this.website_urlDirtyFlag ;
    }   

    /**
     * 获取 [Web页]
     */
    @JsonProperty("web_page")
    public String getWeb_page(){
        return this.web_page ;
    }

    /**
     * 设置 [Web页]
     */
    @JsonProperty("web_page")
    public void setWeb_page(String  web_page){
        this.web_page = web_page ;
        this.web_pageDirtyFlag = true ;
    }

     /**
     * 获取 [Web页]脏标记
     */
    @JsonIgnore
    public boolean getWeb_pageDirtyFlag(){
        return this.web_pageDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(map.get("are_you_inside") instanceof Boolean){
			this.setAre_you_inside(((Boolean)map.get("are_you_inside"))? "true" : "false");
		}
		if(!(map.get("button_text") instanceof Boolean)&& map.get("button_text")!=null){
			this.setButton_text((String)map.get("button_text"));
		}
		if(!(map.get("channel_ids") instanceof Boolean)&& map.get("channel_ids")!=null){
			Object[] objs = (Object[])map.get("channel_ids");
			if(objs.length > 0){
				Integer[] channel_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setChannel_ids(Arrays.toString(channel_ids));
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("default_message") instanceof Boolean)&& map.get("default_message")!=null){
			this.setDefault_message((String)map.get("default_message"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("image") instanceof Boolean)&& map.get("image")!=null){
			//暂时忽略
			//this.setImage(((String)map.get("image")).getBytes("UTF-8"));
		}
		if(!(map.get("image_medium") instanceof Boolean)&& map.get("image_medium")!=null){
			//暂时忽略
			//this.setImage_medium(((String)map.get("image_medium")).getBytes("UTF-8"));
		}
		if(!(map.get("image_small") instanceof Boolean)&& map.get("image_small")!=null){
			//暂时忽略
			//this.setImage_small(((String)map.get("image_small")).getBytes("UTF-8"));
		}
		if(!(map.get("input_placeholder") instanceof Boolean)&& map.get("input_placeholder")!=null){
			this.setInput_placeholder((String)map.get("input_placeholder"));
		}
		if(map.get("is_published") instanceof Boolean){
			this.setIs_published(((Boolean)map.get("is_published"))? "true" : "false");
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("nbr_channel") instanceof Boolean)&& map.get("nbr_channel")!=null){
			this.setNbr_channel((Integer)map.get("nbr_channel"));
		}
		if(!(map.get("rating_percentage_satisfaction") instanceof Boolean)&& map.get("rating_percentage_satisfaction")!=null){
			this.setRating_percentage_satisfaction((Integer)map.get("rating_percentage_satisfaction"));
		}
		if(!(map.get("rule_ids") instanceof Boolean)&& map.get("rule_ids")!=null){
			Object[] objs = (Object[])map.get("rule_ids");
			if(objs.length > 0){
				Integer[] rule_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setRule_ids(Arrays.toString(rule_ids));
			}
		}
		if(!(map.get("script_external") instanceof Boolean)&& map.get("script_external")!=null){
			this.setScript_external((String)map.get("script_external"));
		}
		if(!(map.get("user_ids") instanceof Boolean)&& map.get("user_ids")!=null){
			Object[] objs = (Object[])map.get("user_ids");
			if(objs.length > 0){
				Integer[] user_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setUser_ids(Arrays.toString(user_ids));
			}
		}
		if(!(map.get("website_description") instanceof Boolean)&& map.get("website_description")!=null){
			this.setWebsite_description((String)map.get("website_description"));
		}
		if(map.get("website_published") instanceof Boolean){
			this.setWebsite_published(((Boolean)map.get("website_published"))? "true" : "false");
		}
		if(!(map.get("website_url") instanceof Boolean)&& map.get("website_url")!=null){
			this.setWebsite_url((String)map.get("website_url"));
		}
		if(!(map.get("web_page") instanceof Boolean)&& map.get("web_page")!=null){
			this.setWeb_page((String)map.get("web_page"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAre_you_inside()!=null&&this.getAre_you_insideDirtyFlag()){
			map.put("are_you_inside",Boolean.parseBoolean(this.getAre_you_inside()));		
		}		if(this.getButton_text()!=null&&this.getButton_textDirtyFlag()){
			map.put("button_text",this.getButton_text());
		}else if(this.getButton_textDirtyFlag()){
			map.put("button_text",false);
		}
		if(this.getChannel_ids()!=null&&this.getChannel_idsDirtyFlag()){
			map.put("channel_ids",this.getChannel_ids());
		}else if(this.getChannel_idsDirtyFlag()){
			map.put("channel_ids",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDefault_message()!=null&&this.getDefault_messageDirtyFlag()){
			map.put("default_message",this.getDefault_message());
		}else if(this.getDefault_messageDirtyFlag()){
			map.put("default_message",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getImage()!=null&&this.getImageDirtyFlag()){
			//暂不支持binary类型image
		}else if(this.getImageDirtyFlag()){
			map.put("image",false);
		}
		if(this.getImage_medium()!=null&&this.getImage_mediumDirtyFlag()){
			//暂不支持binary类型image_medium
		}else if(this.getImage_mediumDirtyFlag()){
			map.put("image_medium",false);
		}
		if(this.getImage_small()!=null&&this.getImage_smallDirtyFlag()){
			//暂不支持binary类型image_small
		}else if(this.getImage_smallDirtyFlag()){
			map.put("image_small",false);
		}
		if(this.getInput_placeholder()!=null&&this.getInput_placeholderDirtyFlag()){
			map.put("input_placeholder",this.getInput_placeholder());
		}else if(this.getInput_placeholderDirtyFlag()){
			map.put("input_placeholder",false);
		}
		if(this.getIs_published()!=null&&this.getIs_publishedDirtyFlag()){
			map.put("is_published",Boolean.parseBoolean(this.getIs_published()));		
		}		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getNbr_channel()!=null&&this.getNbr_channelDirtyFlag()){
			map.put("nbr_channel",this.getNbr_channel());
		}else if(this.getNbr_channelDirtyFlag()){
			map.put("nbr_channel",false);
		}
		if(this.getRating_percentage_satisfaction()!=null&&this.getRating_percentage_satisfactionDirtyFlag()){
			map.put("rating_percentage_satisfaction",this.getRating_percentage_satisfaction());
		}else if(this.getRating_percentage_satisfactionDirtyFlag()){
			map.put("rating_percentage_satisfaction",false);
		}
		if(this.getRule_ids()!=null&&this.getRule_idsDirtyFlag()){
			map.put("rule_ids",this.getRule_ids());
		}else if(this.getRule_idsDirtyFlag()){
			map.put("rule_ids",false);
		}
		if(this.getScript_external()!=null&&this.getScript_externalDirtyFlag()){
			map.put("script_external",this.getScript_external());
		}else if(this.getScript_externalDirtyFlag()){
			map.put("script_external",false);
		}
		if(this.getUser_ids()!=null&&this.getUser_idsDirtyFlag()){
			map.put("user_ids",this.getUser_ids());
		}else if(this.getUser_idsDirtyFlag()){
			map.put("user_ids",false);
		}
		if(this.getWebsite_description()!=null&&this.getWebsite_descriptionDirtyFlag()){
			map.put("website_description",this.getWebsite_description());
		}else if(this.getWebsite_descriptionDirtyFlag()){
			map.put("website_description",false);
		}
		if(this.getWebsite_published()!=null&&this.getWebsite_publishedDirtyFlag()){
			map.put("website_published",Boolean.parseBoolean(this.getWebsite_published()));		
		}		if(this.getWebsite_url()!=null&&this.getWebsite_urlDirtyFlag()){
			map.put("website_url",this.getWebsite_url());
		}else if(this.getWebsite_urlDirtyFlag()){
			map.put("website_url",false);
		}
		if(this.getWeb_page()!=null&&this.getWeb_pageDirtyFlag()){
			map.put("web_page",this.getWeb_page());
		}else if(this.getWeb_pageDirtyFlag()){
			map.put("web_page",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
