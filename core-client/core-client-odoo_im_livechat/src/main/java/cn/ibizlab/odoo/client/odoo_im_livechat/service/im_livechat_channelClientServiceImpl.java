package cn.ibizlab.odoo.client.odoo_im_livechat.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iim_livechat_channel;
import cn.ibizlab.odoo.core.client.service.Iim_livechat_channelClientService;
import cn.ibizlab.odoo.client.odoo_im_livechat.model.im_livechat_channelImpl;
import cn.ibizlab.odoo.client.odoo_im_livechat.odooclient.Iim_livechat_channelOdooClient;
import cn.ibizlab.odoo.client.odoo_im_livechat.odooclient.impl.im_livechat_channelOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[im_livechat_channel] 服务对象接口
 */
@Service
public class im_livechat_channelClientServiceImpl implements Iim_livechat_channelClientService {
    @Autowired
    private  Iim_livechat_channelOdooClient  im_livechat_channelOdooClient;

    public Iim_livechat_channel createModel() {		
		return new im_livechat_channelImpl();
	}


        public void createBatch(List<Iim_livechat_channel> im_livechat_channels){
            
        }
        
        public void remove(Iim_livechat_channel im_livechat_channel){
this.im_livechat_channelOdooClient.remove(im_livechat_channel) ;
        }
        
        public Page<Iim_livechat_channel> search(SearchContext context){
            return this.im_livechat_channelOdooClient.search(context) ;
        }
        
        public void create(Iim_livechat_channel im_livechat_channel){
this.im_livechat_channelOdooClient.create(im_livechat_channel) ;
        }
        
        public void removeBatch(List<Iim_livechat_channel> im_livechat_channels){
            
        }
        
        public void update(Iim_livechat_channel im_livechat_channel){
this.im_livechat_channelOdooClient.update(im_livechat_channel) ;
        }
        
        public void updateBatch(List<Iim_livechat_channel> im_livechat_channels){
            
        }
        
        public void get(Iim_livechat_channel im_livechat_channel){
            this.im_livechat_channelOdooClient.get(im_livechat_channel) ;
        }
        
        public Page<Iim_livechat_channel> select(SearchContext context){
            return null ;
        }
        

}

