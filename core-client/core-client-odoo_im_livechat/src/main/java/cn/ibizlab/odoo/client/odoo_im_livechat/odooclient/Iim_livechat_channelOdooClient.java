package cn.ibizlab.odoo.client.odoo_im_livechat.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iim_livechat_channel;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[im_livechat_channel] 服务对象客户端接口
 */
public interface Iim_livechat_channelOdooClient {
    
        public void createBatch(Iim_livechat_channel im_livechat_channel);

        public void remove(Iim_livechat_channel im_livechat_channel);

        public Page<Iim_livechat_channel> search(SearchContext context);

        public void create(Iim_livechat_channel im_livechat_channel);

        public void removeBatch(Iim_livechat_channel im_livechat_channel);

        public void update(Iim_livechat_channel im_livechat_channel);

        public void updateBatch(Iim_livechat_channel im_livechat_channel);

        public void get(Iim_livechat_channel im_livechat_channel);

        public List<Iim_livechat_channel> select();


}