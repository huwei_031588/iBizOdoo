package cn.ibizlab.odoo.client.odoo_im_livechat.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.im.livechat")
@Data
public class odoo_im_livechatClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
