package cn.ibizlab.odoo.client.odoo_crm.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Icrm_lead2opportunity_partner_mass;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[crm_lead2opportunity_partner_mass] 服务对象客户端接口
 */
public interface Icrm_lead2opportunity_partner_massOdooClient {
    
        public void updateBatch(Icrm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass);

        public Page<Icrm_lead2opportunity_partner_mass> search(SearchContext context);

        public void update(Icrm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass);

        public void remove(Icrm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass);

        public void get(Icrm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass);

        public void create(Icrm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass);

        public void removeBatch(Icrm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass);

        public void createBatch(Icrm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass);

        public List<Icrm_lead2opportunity_partner_mass> select();


}