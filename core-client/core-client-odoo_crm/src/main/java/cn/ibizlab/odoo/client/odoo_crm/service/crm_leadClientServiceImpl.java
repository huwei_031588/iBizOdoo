package cn.ibizlab.odoo.client.odoo_crm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icrm_lead;
import cn.ibizlab.odoo.core.client.service.Icrm_leadClientService;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_leadImpl;
import cn.ibizlab.odoo.client.odoo_crm.odooclient.Icrm_leadOdooClient;
import cn.ibizlab.odoo.client.odoo_crm.odooclient.impl.crm_leadOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[crm_lead] 服务对象接口
 */
@Service
public class crm_leadClientServiceImpl implements Icrm_leadClientService {
    @Autowired
    private  Icrm_leadOdooClient  crm_leadOdooClient;

    public Icrm_lead createModel() {		
		return new crm_leadImpl();
	}


        public void update(Icrm_lead crm_lead){
this.crm_leadOdooClient.update(crm_lead) ;
        }
        
        public void updateBatch(List<Icrm_lead> crm_leads){
            
        }
        
        public Page<Icrm_lead> search(SearchContext context){
            return this.crm_leadOdooClient.search(context) ;
        }
        
        public void create(Icrm_lead crm_lead){
this.crm_leadOdooClient.create(crm_lead) ;
        }
        
        public void createBatch(List<Icrm_lead> crm_leads){
            
        }
        
        public void get(Icrm_lead crm_lead){
            this.crm_leadOdooClient.get(crm_lead) ;
        }
        
        public void removeBatch(List<Icrm_lead> crm_leads){
            
        }
        
        public void remove(Icrm_lead crm_lead){
this.crm_leadOdooClient.remove(crm_lead) ;
        }
        
        public Page<Icrm_lead> select(SearchContext context){
            return null ;
        }
        

}

