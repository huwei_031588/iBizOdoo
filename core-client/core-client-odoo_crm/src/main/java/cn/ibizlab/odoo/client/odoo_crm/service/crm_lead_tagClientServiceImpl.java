package cn.ibizlab.odoo.client.odoo_crm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icrm_lead_tag;
import cn.ibizlab.odoo.core.client.service.Icrm_lead_tagClientService;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_lead_tagImpl;
import cn.ibizlab.odoo.client.odoo_crm.odooclient.Icrm_lead_tagOdooClient;
import cn.ibizlab.odoo.client.odoo_crm.odooclient.impl.crm_lead_tagOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[crm_lead_tag] 服务对象接口
 */
@Service
public class crm_lead_tagClientServiceImpl implements Icrm_lead_tagClientService {
    @Autowired
    private  Icrm_lead_tagOdooClient  crm_lead_tagOdooClient;

    public Icrm_lead_tag createModel() {		
		return new crm_lead_tagImpl();
	}


        public void remove(Icrm_lead_tag crm_lead_tag){
this.crm_lead_tagOdooClient.remove(crm_lead_tag) ;
        }
        
        public void get(Icrm_lead_tag crm_lead_tag){
            this.crm_lead_tagOdooClient.get(crm_lead_tag) ;
        }
        
        public void update(Icrm_lead_tag crm_lead_tag){
this.crm_lead_tagOdooClient.update(crm_lead_tag) ;
        }
        
        public void updateBatch(List<Icrm_lead_tag> crm_lead_tags){
            
        }
        
        public void create(Icrm_lead_tag crm_lead_tag){
this.crm_lead_tagOdooClient.create(crm_lead_tag) ;
        }
        
        public void removeBatch(List<Icrm_lead_tag> crm_lead_tags){
            
        }
        
        public Page<Icrm_lead_tag> search(SearchContext context){
            return this.crm_lead_tagOdooClient.search(context) ;
        }
        
        public void createBatch(List<Icrm_lead_tag> crm_lead_tags){
            
        }
        
        public Page<Icrm_lead_tag> select(SearchContext context){
            return null ;
        }
        

}

