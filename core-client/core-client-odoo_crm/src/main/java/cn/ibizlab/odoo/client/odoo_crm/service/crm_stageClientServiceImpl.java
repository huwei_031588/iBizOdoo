package cn.ibizlab.odoo.client.odoo_crm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icrm_stage;
import cn.ibizlab.odoo.core.client.service.Icrm_stageClientService;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_stageImpl;
import cn.ibizlab.odoo.client.odoo_crm.odooclient.Icrm_stageOdooClient;
import cn.ibizlab.odoo.client.odoo_crm.odooclient.impl.crm_stageOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[crm_stage] 服务对象接口
 */
@Service
public class crm_stageClientServiceImpl implements Icrm_stageClientService {
    @Autowired
    private  Icrm_stageOdooClient  crm_stageOdooClient;

    public Icrm_stage createModel() {		
		return new crm_stageImpl();
	}


        public void get(Icrm_stage crm_stage){
            this.crm_stageOdooClient.get(crm_stage) ;
        }
        
        public void createBatch(List<Icrm_stage> crm_stages){
            
        }
        
        public void update(Icrm_stage crm_stage){
this.crm_stageOdooClient.update(crm_stage) ;
        }
        
        public void remove(Icrm_stage crm_stage){
this.crm_stageOdooClient.remove(crm_stage) ;
        }
        
        public Page<Icrm_stage> search(SearchContext context){
            return this.crm_stageOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Icrm_stage> crm_stages){
            
        }
        
        public void create(Icrm_stage crm_stage){
this.crm_stageOdooClient.create(crm_stage) ;
        }
        
        public void removeBatch(List<Icrm_stage> crm_stages){
            
        }
        
        public Page<Icrm_stage> select(SearchContext context){
            return null ;
        }
        

}

