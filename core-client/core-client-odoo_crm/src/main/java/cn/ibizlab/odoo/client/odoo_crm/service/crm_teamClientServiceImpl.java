package cn.ibizlab.odoo.client.odoo_crm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icrm_team;
import cn.ibizlab.odoo.core.client.service.Icrm_teamClientService;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_teamImpl;
import cn.ibizlab.odoo.client.odoo_crm.odooclient.Icrm_teamOdooClient;
import cn.ibizlab.odoo.client.odoo_crm.odooclient.impl.crm_teamOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[crm_team] 服务对象接口
 */
@Service
public class crm_teamClientServiceImpl implements Icrm_teamClientService {
    @Autowired
    private  Icrm_teamOdooClient  crm_teamOdooClient;

    public Icrm_team createModel() {		
		return new crm_teamImpl();
	}


        public void create(Icrm_team crm_team){
this.crm_teamOdooClient.create(crm_team) ;
        }
        
        public void updateBatch(List<Icrm_team> crm_teams){
            
        }
        
        public void removeBatch(List<Icrm_team> crm_teams){
            
        }
        
        public void createBatch(List<Icrm_team> crm_teams){
            
        }
        
        public Page<Icrm_team> search(SearchContext context){
            return this.crm_teamOdooClient.search(context) ;
        }
        
        public void get(Icrm_team crm_team){
            this.crm_teamOdooClient.get(crm_team) ;
        }
        
        public void remove(Icrm_team crm_team){
this.crm_teamOdooClient.remove(crm_team) ;
        }
        
        public void update(Icrm_team crm_team){
this.crm_teamOdooClient.update(crm_team) ;
        }
        
        public Page<Icrm_team> select(SearchContext context){
            return null ;
        }
        

}

