package cn.ibizlab.odoo.client.odoo_crm.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Icrm_activity_report;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[crm_activity_report] 对象
 */
public class crm_activity_reportImpl implements Icrm_activity_report,Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer author_id;

    @JsonIgnore
    public boolean author_idDirtyFlag;
    
    /**
     * 创建人
     */
    public String author_id_text;

    @JsonIgnore
    public boolean author_id_textDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 国家
     */
    public Integer country_id;

    @JsonIgnore
    public boolean country_idDirtyFlag;
    
    /**
     * 国家
     */
    public String country_id_text;

    @JsonIgnore
    public boolean country_id_textDirtyFlag;
    
    /**
     * 日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 线索
     */
    public Integer lead_id;

    @JsonIgnore
    public boolean lead_idDirtyFlag;
    
    /**
     * 线索
     */
    public String lead_id_text;

    @JsonIgnore
    public boolean lead_id_textDirtyFlag;
    
    /**
     * 类型
     */
    public String lead_type;

    @JsonIgnore
    public boolean lead_typeDirtyFlag;
    
    /**
     * 活动类型
     */
    public Integer mail_activity_type_id;

    @JsonIgnore
    public boolean mail_activity_type_idDirtyFlag;
    
    /**
     * 活动类型
     */
    public String mail_activity_type_id_text;

    @JsonIgnore
    public boolean mail_activity_type_id_textDirtyFlag;
    
    /**
     * 业务合作伙伴/客户
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 业务合作伙伴/客户
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 概率
     */
    public Double probability;

    @JsonIgnore
    public boolean probabilityDirtyFlag;
    
    /**
     * 阶段
     */
    public Integer stage_id;

    @JsonIgnore
    public boolean stage_idDirtyFlag;
    
    /**
     * 阶段
     */
    public String stage_id_text;

    @JsonIgnore
    public boolean stage_id_textDirtyFlag;
    
    /**
     * 摘要
     */
    public String subject;

    @JsonIgnore
    public boolean subjectDirtyFlag;
    
    /**
     * 子类型
     */
    public Integer subtype_id;

    @JsonIgnore
    public boolean subtype_idDirtyFlag;
    
    /**
     * 子类型
     */
    public String subtype_id_text;

    @JsonIgnore
    public boolean subtype_id_textDirtyFlag;
    
    /**
     * 销售团队
     */
    public Integer team_id;

    @JsonIgnore
    public boolean team_idDirtyFlag;
    
    /**
     * 销售团队
     */
    public String team_id_text;

    @JsonIgnore
    public boolean team_id_textDirtyFlag;
    
    /**
     * 销售员
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 销售员
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("author_id")
    public Integer getAuthor_id(){
        return this.author_id ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("author_id")
    public void setAuthor_id(Integer  author_id){
        this.author_id = author_id ;
        this.author_idDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getAuthor_idDirtyFlag(){
        return this.author_idDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("author_id_text")
    public String getAuthor_id_text(){
        return this.author_id_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("author_id_text")
    public void setAuthor_id_text(String  author_id_text){
        this.author_id_text = author_id_text ;
        this.author_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getAuthor_id_textDirtyFlag(){
        return this.author_id_textDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [国家]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return this.country_id ;
    }

    /**
     * 设置 [国家]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

     /**
     * 获取 [国家]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return this.country_idDirtyFlag ;
    }   

    /**
     * 获取 [国家]
     */
    @JsonProperty("country_id_text")
    public String getCountry_id_text(){
        return this.country_id_text ;
    }

    /**
     * 设置 [国家]
     */
    @JsonProperty("country_id_text")
    public void setCountry_id_text(String  country_id_text){
        this.country_id_text = country_id_text ;
        this.country_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [国家]脏标记
     */
    @JsonIgnore
    public boolean getCountry_id_textDirtyFlag(){
        return this.country_id_textDirtyFlag ;
    }   

    /**
     * 获取 [日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [线索]
     */
    @JsonProperty("lead_id")
    public Integer getLead_id(){
        return this.lead_id ;
    }

    /**
     * 设置 [线索]
     */
    @JsonProperty("lead_id")
    public void setLead_id(Integer  lead_id){
        this.lead_id = lead_id ;
        this.lead_idDirtyFlag = true ;
    }

     /**
     * 获取 [线索]脏标记
     */
    @JsonIgnore
    public boolean getLead_idDirtyFlag(){
        return this.lead_idDirtyFlag ;
    }   

    /**
     * 获取 [线索]
     */
    @JsonProperty("lead_id_text")
    public String getLead_id_text(){
        return this.lead_id_text ;
    }

    /**
     * 设置 [线索]
     */
    @JsonProperty("lead_id_text")
    public void setLead_id_text(String  lead_id_text){
        this.lead_id_text = lead_id_text ;
        this.lead_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [线索]脏标记
     */
    @JsonIgnore
    public boolean getLead_id_textDirtyFlag(){
        return this.lead_id_textDirtyFlag ;
    }   

    /**
     * 获取 [类型]
     */
    @JsonProperty("lead_type")
    public String getLead_type(){
        return this.lead_type ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("lead_type")
    public void setLead_type(String  lead_type){
        this.lead_type = lead_type ;
        this.lead_typeDirtyFlag = true ;
    }

     /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getLead_typeDirtyFlag(){
        return this.lead_typeDirtyFlag ;
    }   

    /**
     * 获取 [活动类型]
     */
    @JsonProperty("mail_activity_type_id")
    public Integer getMail_activity_type_id(){
        return this.mail_activity_type_id ;
    }

    /**
     * 设置 [活动类型]
     */
    @JsonProperty("mail_activity_type_id")
    public void setMail_activity_type_id(Integer  mail_activity_type_id){
        this.mail_activity_type_id = mail_activity_type_id ;
        this.mail_activity_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [活动类型]脏标记
     */
    @JsonIgnore
    public boolean getMail_activity_type_idDirtyFlag(){
        return this.mail_activity_type_idDirtyFlag ;
    }   

    /**
     * 获取 [活动类型]
     */
    @JsonProperty("mail_activity_type_id_text")
    public String getMail_activity_type_id_text(){
        return this.mail_activity_type_id_text ;
    }

    /**
     * 设置 [活动类型]
     */
    @JsonProperty("mail_activity_type_id_text")
    public void setMail_activity_type_id_text(String  mail_activity_type_id_text){
        this.mail_activity_type_id_text = mail_activity_type_id_text ;
        this.mail_activity_type_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [活动类型]脏标记
     */
    @JsonIgnore
    public boolean getMail_activity_type_id_textDirtyFlag(){
        return this.mail_activity_type_id_textDirtyFlag ;
    }   

    /**
     * 获取 [业务合作伙伴/客户]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [业务合作伙伴/客户]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [业务合作伙伴/客户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [业务合作伙伴/客户]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [业务合作伙伴/客户]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [业务合作伙伴/客户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [概率]
     */
    @JsonProperty("probability")
    public Double getProbability(){
        return this.probability ;
    }

    /**
     * 设置 [概率]
     */
    @JsonProperty("probability")
    public void setProbability(Double  probability){
        this.probability = probability ;
        this.probabilityDirtyFlag = true ;
    }

     /**
     * 获取 [概率]脏标记
     */
    @JsonIgnore
    public boolean getProbabilityDirtyFlag(){
        return this.probabilityDirtyFlag ;
    }   

    /**
     * 获取 [阶段]
     */
    @JsonProperty("stage_id")
    public Integer getStage_id(){
        return this.stage_id ;
    }

    /**
     * 设置 [阶段]
     */
    @JsonProperty("stage_id")
    public void setStage_id(Integer  stage_id){
        this.stage_id = stage_id ;
        this.stage_idDirtyFlag = true ;
    }

     /**
     * 获取 [阶段]脏标记
     */
    @JsonIgnore
    public boolean getStage_idDirtyFlag(){
        return this.stage_idDirtyFlag ;
    }   

    /**
     * 获取 [阶段]
     */
    @JsonProperty("stage_id_text")
    public String getStage_id_text(){
        return this.stage_id_text ;
    }

    /**
     * 设置 [阶段]
     */
    @JsonProperty("stage_id_text")
    public void setStage_id_text(String  stage_id_text){
        this.stage_id_text = stage_id_text ;
        this.stage_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [阶段]脏标记
     */
    @JsonIgnore
    public boolean getStage_id_textDirtyFlag(){
        return this.stage_id_textDirtyFlag ;
    }   

    /**
     * 获取 [摘要]
     */
    @JsonProperty("subject")
    public String getSubject(){
        return this.subject ;
    }

    /**
     * 设置 [摘要]
     */
    @JsonProperty("subject")
    public void setSubject(String  subject){
        this.subject = subject ;
        this.subjectDirtyFlag = true ;
    }

     /**
     * 获取 [摘要]脏标记
     */
    @JsonIgnore
    public boolean getSubjectDirtyFlag(){
        return this.subjectDirtyFlag ;
    }   

    /**
     * 获取 [子类型]
     */
    @JsonProperty("subtype_id")
    public Integer getSubtype_id(){
        return this.subtype_id ;
    }

    /**
     * 设置 [子类型]
     */
    @JsonProperty("subtype_id")
    public void setSubtype_id(Integer  subtype_id){
        this.subtype_id = subtype_id ;
        this.subtype_idDirtyFlag = true ;
    }

     /**
     * 获取 [子类型]脏标记
     */
    @JsonIgnore
    public boolean getSubtype_idDirtyFlag(){
        return this.subtype_idDirtyFlag ;
    }   

    /**
     * 获取 [子类型]
     */
    @JsonProperty("subtype_id_text")
    public String getSubtype_id_text(){
        return this.subtype_id_text ;
    }

    /**
     * 设置 [子类型]
     */
    @JsonProperty("subtype_id_text")
    public void setSubtype_id_text(String  subtype_id_text){
        this.subtype_id_text = subtype_id_text ;
        this.subtype_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [子类型]脏标记
     */
    @JsonIgnore
    public boolean getSubtype_id_textDirtyFlag(){
        return this.subtype_id_textDirtyFlag ;
    }   

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id")
    public Integer getTeam_id(){
        return this.team_id ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id")
    public void setTeam_id(Integer  team_id){
        this.team_id = team_id ;
        this.team_idDirtyFlag = true ;
    }

     /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_idDirtyFlag(){
        return this.team_idDirtyFlag ;
    }   

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id_text")
    public String getTeam_id_text(){
        return this.team_id_text ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id_text")
    public void setTeam_id_text(String  team_id_text){
        this.team_id_text = team_id_text ;
        this.team_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_id_textDirtyFlag(){
        return this.team_id_textDirtyFlag ;
    }   

    /**
     * 获取 [销售员]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [销售员]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(map.get("active") instanceof Boolean){
			this.setActive(((Boolean)map.get("active"))? "true" : "false");
		}
		if(!(map.get("author_id") instanceof Boolean)&& map.get("author_id")!=null){
			Object[] objs = (Object[])map.get("author_id");
			if(objs.length > 0){
				this.setAuthor_id((Integer)objs[0]);
			}
		}
		if(!(map.get("author_id") instanceof Boolean)&& map.get("author_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("author_id");
			if(objs.length > 1){
				this.setAuthor_id_text((String)objs[1]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 1){
				this.setCompany_id_text((String)objs[1]);
			}
		}
		if(!(map.get("country_id") instanceof Boolean)&& map.get("country_id")!=null){
			Object[] objs = (Object[])map.get("country_id");
			if(objs.length > 0){
				this.setCountry_id((Integer)objs[0]);
			}
		}
		if(!(map.get("country_id") instanceof Boolean)&& map.get("country_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("country_id");
			if(objs.length > 1){
				this.setCountry_id_text((String)objs[1]);
			}
		}
		if(!(map.get("date") instanceof Boolean)&& map.get("date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date"));
   			this.setDate(new Timestamp(parse.getTime()));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("lead_id") instanceof Boolean)&& map.get("lead_id")!=null){
			Object[] objs = (Object[])map.get("lead_id");
			if(objs.length > 0){
				this.setLead_id((Integer)objs[0]);
			}
		}
		if(!(map.get("lead_id") instanceof Boolean)&& map.get("lead_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("lead_id");
			if(objs.length > 1){
				this.setLead_id_text((String)objs[1]);
			}
		}
		if(!(map.get("lead_type") instanceof Boolean)&& map.get("lead_type")!=null){
			this.setLead_type((String)map.get("lead_type"));
		}
		if(!(map.get("mail_activity_type_id") instanceof Boolean)&& map.get("mail_activity_type_id")!=null){
			Object[] objs = (Object[])map.get("mail_activity_type_id");
			if(objs.length > 0){
				this.setMail_activity_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("mail_activity_type_id") instanceof Boolean)&& map.get("mail_activity_type_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("mail_activity_type_id");
			if(objs.length > 1){
				this.setMail_activity_type_id_text((String)objs[1]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 0){
				this.setPartner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 1){
				this.setPartner_id_text((String)objs[1]);
			}
		}
		if(!(map.get("probability") instanceof Boolean)&& map.get("probability")!=null){
			this.setProbability((Double)map.get("probability"));
		}
		if(!(map.get("stage_id") instanceof Boolean)&& map.get("stage_id")!=null){
			Object[] objs = (Object[])map.get("stage_id");
			if(objs.length > 0){
				this.setStage_id((Integer)objs[0]);
			}
		}
		if(!(map.get("stage_id") instanceof Boolean)&& map.get("stage_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("stage_id");
			if(objs.length > 1){
				this.setStage_id_text((String)objs[1]);
			}
		}
		if(!(map.get("subject") instanceof Boolean)&& map.get("subject")!=null){
			this.setSubject((String)map.get("subject"));
		}
		if(!(map.get("subtype_id") instanceof Boolean)&& map.get("subtype_id")!=null){
			Object[] objs = (Object[])map.get("subtype_id");
			if(objs.length > 0){
				this.setSubtype_id((Integer)objs[0]);
			}
		}
		if(!(map.get("subtype_id") instanceof Boolean)&& map.get("subtype_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("subtype_id");
			if(objs.length > 1){
				this.setSubtype_id_text((String)objs[1]);
			}
		}
		if(!(map.get("team_id") instanceof Boolean)&& map.get("team_id")!=null){
			Object[] objs = (Object[])map.get("team_id");
			if(objs.length > 0){
				this.setTeam_id((Integer)objs[0]);
			}
		}
		if(!(map.get("team_id") instanceof Boolean)&& map.get("team_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("team_id");
			if(objs.length > 1){
				this.setTeam_id_text((String)objs[1]);
			}
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 0){
				this.setUser_id((Integer)objs[0]);
			}
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 1){
				this.setUser_id_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getActive()!=null&&this.getActiveDirtyFlag()){
			map.put("active",Boolean.parseBoolean(this.getActive()));		
		}		if(this.getAuthor_id()!=null&&this.getAuthor_idDirtyFlag()){
			map.put("author_id",this.getAuthor_id());
		}else if(this.getAuthor_idDirtyFlag()){
			map.put("author_id",false);
		}
		if(this.getAuthor_id_text()!=null&&this.getAuthor_id_textDirtyFlag()){
			//忽略文本外键author_id_text
		}else if(this.getAuthor_id_textDirtyFlag()){
			map.put("author_id",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_id_text()!=null&&this.getCompany_id_textDirtyFlag()){
			//忽略文本外键company_id_text
		}else if(this.getCompany_id_textDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCountry_id()!=null&&this.getCountry_idDirtyFlag()){
			map.put("country_id",this.getCountry_id());
		}else if(this.getCountry_idDirtyFlag()){
			map.put("country_id",false);
		}
		if(this.getCountry_id_text()!=null&&this.getCountry_id_textDirtyFlag()){
			//忽略文本外键country_id_text
		}else if(this.getCountry_id_textDirtyFlag()){
			map.put("country_id",false);
		}
		if(this.getDate()!=null&&this.getDateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate());
			map.put("date",datetimeStr);
		}else if(this.getDateDirtyFlag()){
			map.put("date",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getLead_id()!=null&&this.getLead_idDirtyFlag()){
			map.put("lead_id",this.getLead_id());
		}else if(this.getLead_idDirtyFlag()){
			map.put("lead_id",false);
		}
		if(this.getLead_id_text()!=null&&this.getLead_id_textDirtyFlag()){
			//忽略文本外键lead_id_text
		}else if(this.getLead_id_textDirtyFlag()){
			map.put("lead_id",false);
		}
		if(this.getLead_type()!=null&&this.getLead_typeDirtyFlag()){
			map.put("lead_type",this.getLead_type());
		}else if(this.getLead_typeDirtyFlag()){
			map.put("lead_type",false);
		}
		if(this.getMail_activity_type_id()!=null&&this.getMail_activity_type_idDirtyFlag()){
			map.put("mail_activity_type_id",this.getMail_activity_type_id());
		}else if(this.getMail_activity_type_idDirtyFlag()){
			map.put("mail_activity_type_id",false);
		}
		if(this.getMail_activity_type_id_text()!=null&&this.getMail_activity_type_id_textDirtyFlag()){
			//忽略文本外键mail_activity_type_id_text
		}else if(this.getMail_activity_type_id_textDirtyFlag()){
			map.put("mail_activity_type_id",false);
		}
		if(this.getPartner_id()!=null&&this.getPartner_idDirtyFlag()){
			map.put("partner_id",this.getPartner_id());
		}else if(this.getPartner_idDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPartner_id_text()!=null&&this.getPartner_id_textDirtyFlag()){
			//忽略文本外键partner_id_text
		}else if(this.getPartner_id_textDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getProbability()!=null&&this.getProbabilityDirtyFlag()){
			map.put("probability",this.getProbability());
		}else if(this.getProbabilityDirtyFlag()){
			map.put("probability",false);
		}
		if(this.getStage_id()!=null&&this.getStage_idDirtyFlag()){
			map.put("stage_id",this.getStage_id());
		}else if(this.getStage_idDirtyFlag()){
			map.put("stage_id",false);
		}
		if(this.getStage_id_text()!=null&&this.getStage_id_textDirtyFlag()){
			//忽略文本外键stage_id_text
		}else if(this.getStage_id_textDirtyFlag()){
			map.put("stage_id",false);
		}
		if(this.getSubject()!=null&&this.getSubjectDirtyFlag()){
			map.put("subject",this.getSubject());
		}else if(this.getSubjectDirtyFlag()){
			map.put("subject",false);
		}
		if(this.getSubtype_id()!=null&&this.getSubtype_idDirtyFlag()){
			map.put("subtype_id",this.getSubtype_id());
		}else if(this.getSubtype_idDirtyFlag()){
			map.put("subtype_id",false);
		}
		if(this.getSubtype_id_text()!=null&&this.getSubtype_id_textDirtyFlag()){
			//忽略文本外键subtype_id_text
		}else if(this.getSubtype_id_textDirtyFlag()){
			map.put("subtype_id",false);
		}
		if(this.getTeam_id()!=null&&this.getTeam_idDirtyFlag()){
			map.put("team_id",this.getTeam_id());
		}else if(this.getTeam_idDirtyFlag()){
			map.put("team_id",false);
		}
		if(this.getTeam_id_text()!=null&&this.getTeam_id_textDirtyFlag()){
			//忽略文本外键team_id_text
		}else if(this.getTeam_id_textDirtyFlag()){
			map.put("team_id",false);
		}
		if(this.getUser_id()!=null&&this.getUser_idDirtyFlag()){
			map.put("user_id",this.getUser_id());
		}else if(this.getUser_idDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getUser_id_text()!=null&&this.getUser_id_textDirtyFlag()){
			//忽略文本外键user_id_text
		}else if(this.getUser_id_textDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
