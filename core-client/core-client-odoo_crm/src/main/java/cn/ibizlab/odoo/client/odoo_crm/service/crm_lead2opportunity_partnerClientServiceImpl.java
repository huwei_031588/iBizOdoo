package cn.ibizlab.odoo.client.odoo_crm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icrm_lead2opportunity_partner;
import cn.ibizlab.odoo.core.client.service.Icrm_lead2opportunity_partnerClientService;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_lead2opportunity_partnerImpl;
import cn.ibizlab.odoo.client.odoo_crm.odooclient.Icrm_lead2opportunity_partnerOdooClient;
import cn.ibizlab.odoo.client.odoo_crm.odooclient.impl.crm_lead2opportunity_partnerOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[crm_lead2opportunity_partner] 服务对象接口
 */
@Service
public class crm_lead2opportunity_partnerClientServiceImpl implements Icrm_lead2opportunity_partnerClientService {
    @Autowired
    private  Icrm_lead2opportunity_partnerOdooClient  crm_lead2opportunity_partnerOdooClient;

    public Icrm_lead2opportunity_partner createModel() {		
		return new crm_lead2opportunity_partnerImpl();
	}


        public void createBatch(List<Icrm_lead2opportunity_partner> crm_lead2opportunity_partners){
            
        }
        
        public void update(Icrm_lead2opportunity_partner crm_lead2opportunity_partner){
this.crm_lead2opportunity_partnerOdooClient.update(crm_lead2opportunity_partner) ;
        }
        
        public Page<Icrm_lead2opportunity_partner> search(SearchContext context){
            return this.crm_lead2opportunity_partnerOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Icrm_lead2opportunity_partner> crm_lead2opportunity_partners){
            
        }
        
        public void get(Icrm_lead2opportunity_partner crm_lead2opportunity_partner){
            this.crm_lead2opportunity_partnerOdooClient.get(crm_lead2opportunity_partner) ;
        }
        
        public void create(Icrm_lead2opportunity_partner crm_lead2opportunity_partner){
this.crm_lead2opportunity_partnerOdooClient.create(crm_lead2opportunity_partner) ;
        }
        
        public void updateBatch(List<Icrm_lead2opportunity_partner> crm_lead2opportunity_partners){
            
        }
        
        public void remove(Icrm_lead2opportunity_partner crm_lead2opportunity_partner){
this.crm_lead2opportunity_partnerOdooClient.remove(crm_lead2opportunity_partner) ;
        }
        
        public Page<Icrm_lead2opportunity_partner> select(SearchContext context){
            return null ;
        }
        

}

