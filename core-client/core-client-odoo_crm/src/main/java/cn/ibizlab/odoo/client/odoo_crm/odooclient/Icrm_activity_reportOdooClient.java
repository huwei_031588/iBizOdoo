package cn.ibizlab.odoo.client.odoo_crm.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Icrm_activity_report;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[crm_activity_report] 服务对象客户端接口
 */
public interface Icrm_activity_reportOdooClient {
    
        public void removeBatch(Icrm_activity_report crm_activity_report);

        public Page<Icrm_activity_report> search(SearchContext context);

        public void createBatch(Icrm_activity_report crm_activity_report);

        public void updateBatch(Icrm_activity_report crm_activity_report);

        public void create(Icrm_activity_report crm_activity_report);

        public void get(Icrm_activity_report crm_activity_report);

        public void update(Icrm_activity_report crm_activity_report);

        public void remove(Icrm_activity_report crm_activity_report);

        public List<Icrm_activity_report> select();


}