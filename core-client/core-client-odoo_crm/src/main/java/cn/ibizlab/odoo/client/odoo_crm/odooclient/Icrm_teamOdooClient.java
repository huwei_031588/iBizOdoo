package cn.ibizlab.odoo.client.odoo_crm.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Icrm_team;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[crm_team] 服务对象客户端接口
 */
public interface Icrm_teamOdooClient {
    
        public void create(Icrm_team crm_team);

        public void updateBatch(Icrm_team crm_team);

        public void removeBatch(Icrm_team crm_team);

        public void createBatch(Icrm_team crm_team);

        public Page<Icrm_team> search(SearchContext context);

        public void get(Icrm_team crm_team);

        public void remove(Icrm_team crm_team);

        public void update(Icrm_team crm_team);

        public List<Icrm_team> select();


}