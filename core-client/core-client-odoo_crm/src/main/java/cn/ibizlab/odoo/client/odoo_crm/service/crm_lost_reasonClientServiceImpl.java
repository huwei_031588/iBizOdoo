package cn.ibizlab.odoo.client.odoo_crm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icrm_lost_reason;
import cn.ibizlab.odoo.core.client.service.Icrm_lost_reasonClientService;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_lost_reasonImpl;
import cn.ibizlab.odoo.client.odoo_crm.odooclient.Icrm_lost_reasonOdooClient;
import cn.ibizlab.odoo.client.odoo_crm.odooclient.impl.crm_lost_reasonOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[crm_lost_reason] 服务对象接口
 */
@Service
public class crm_lost_reasonClientServiceImpl implements Icrm_lost_reasonClientService {
    @Autowired
    private  Icrm_lost_reasonOdooClient  crm_lost_reasonOdooClient;

    public Icrm_lost_reason createModel() {		
		return new crm_lost_reasonImpl();
	}


        public Page<Icrm_lost_reason> search(SearchContext context){
            return this.crm_lost_reasonOdooClient.search(context) ;
        }
        
        public void update(Icrm_lost_reason crm_lost_reason){
this.crm_lost_reasonOdooClient.update(crm_lost_reason) ;
        }
        
        public void removeBatch(List<Icrm_lost_reason> crm_lost_reasons){
            
        }
        
        public void updateBatch(List<Icrm_lost_reason> crm_lost_reasons){
            
        }
        
        public void remove(Icrm_lost_reason crm_lost_reason){
this.crm_lost_reasonOdooClient.remove(crm_lost_reason) ;
        }
        
        public void create(Icrm_lost_reason crm_lost_reason){
this.crm_lost_reasonOdooClient.create(crm_lost_reason) ;
        }
        
        public void get(Icrm_lost_reason crm_lost_reason){
            this.crm_lost_reasonOdooClient.get(crm_lost_reason) ;
        }
        
        public void createBatch(List<Icrm_lost_reason> crm_lost_reasons){
            
        }
        
        public Page<Icrm_lost_reason> select(SearchContext context){
            return null ;
        }
        

}

