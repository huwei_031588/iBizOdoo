package cn.ibizlab.odoo.client.odoo_crm.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Icrm_stage;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[crm_stage] 服务对象客户端接口
 */
public interface Icrm_stageOdooClient {
    
        public void get(Icrm_stage crm_stage);

        public void createBatch(Icrm_stage crm_stage);

        public void update(Icrm_stage crm_stage);

        public void remove(Icrm_stage crm_stage);

        public Page<Icrm_stage> search(SearchContext context);

        public void updateBatch(Icrm_stage crm_stage);

        public void create(Icrm_stage crm_stage);

        public void removeBatch(Icrm_stage crm_stage);

        public List<Icrm_stage> select();


}