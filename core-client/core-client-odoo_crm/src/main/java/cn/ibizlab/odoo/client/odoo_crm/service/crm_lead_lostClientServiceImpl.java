package cn.ibizlab.odoo.client.odoo_crm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icrm_lead_lost;
import cn.ibizlab.odoo.core.client.service.Icrm_lead_lostClientService;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_lead_lostImpl;
import cn.ibizlab.odoo.client.odoo_crm.odooclient.Icrm_lead_lostOdooClient;
import cn.ibizlab.odoo.client.odoo_crm.odooclient.impl.crm_lead_lostOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[crm_lead_lost] 服务对象接口
 */
@Service
public class crm_lead_lostClientServiceImpl implements Icrm_lead_lostClientService {
    @Autowired
    private  Icrm_lead_lostOdooClient  crm_lead_lostOdooClient;

    public Icrm_lead_lost createModel() {		
		return new crm_lead_lostImpl();
	}


        public void removeBatch(List<Icrm_lead_lost> crm_lead_losts){
            
        }
        
        public void updateBatch(List<Icrm_lead_lost> crm_lead_losts){
            
        }
        
        public void get(Icrm_lead_lost crm_lead_lost){
            this.crm_lead_lostOdooClient.get(crm_lead_lost) ;
        }
        
        public void remove(Icrm_lead_lost crm_lead_lost){
this.crm_lead_lostOdooClient.remove(crm_lead_lost) ;
        }
        
        public void update(Icrm_lead_lost crm_lead_lost){
this.crm_lead_lostOdooClient.update(crm_lead_lost) ;
        }
        
        public void createBatch(List<Icrm_lead_lost> crm_lead_losts){
            
        }
        
        public Page<Icrm_lead_lost> search(SearchContext context){
            return this.crm_lead_lostOdooClient.search(context) ;
        }
        
        public void create(Icrm_lead_lost crm_lead_lost){
this.crm_lead_lostOdooClient.create(crm_lead_lost) ;
        }
        
        public Page<Icrm_lead_lost> select(SearchContext context){
            return null ;
        }
        

}

