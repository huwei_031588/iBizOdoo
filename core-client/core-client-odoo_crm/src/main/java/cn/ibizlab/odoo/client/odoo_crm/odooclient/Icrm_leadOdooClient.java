package cn.ibizlab.odoo.client.odoo_crm.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Icrm_lead;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[crm_lead] 服务对象客户端接口
 */
public interface Icrm_leadOdooClient {
    
        public void update(Icrm_lead crm_lead);

        public void updateBatch(Icrm_lead crm_lead);

        public Page<Icrm_lead> search(SearchContext context);

        public void create(Icrm_lead crm_lead);

        public void createBatch(Icrm_lead crm_lead);

        public void get(Icrm_lead crm_lead);

        public void removeBatch(Icrm_lead crm_lead);

        public void remove(Icrm_lead crm_lead);

        public List<Icrm_lead> select();


}