package cn.ibizlab.odoo.client.odoo_crm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icrm_activity_report;
import cn.ibizlab.odoo.core.client.service.Icrm_activity_reportClientService;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_activity_reportImpl;
import cn.ibizlab.odoo.client.odoo_crm.odooclient.Icrm_activity_reportOdooClient;
import cn.ibizlab.odoo.client.odoo_crm.odooclient.impl.crm_activity_reportOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[crm_activity_report] 服务对象接口
 */
@Service
public class crm_activity_reportClientServiceImpl implements Icrm_activity_reportClientService {
    @Autowired
    private  Icrm_activity_reportOdooClient  crm_activity_reportOdooClient;

    public Icrm_activity_report createModel() {		
		return new crm_activity_reportImpl();
	}


        public void removeBatch(List<Icrm_activity_report> crm_activity_reports){
            
        }
        
        public Page<Icrm_activity_report> search(SearchContext context){
            return this.crm_activity_reportOdooClient.search(context) ;
        }
        
        public void createBatch(List<Icrm_activity_report> crm_activity_reports){
            
        }
        
        public void updateBatch(List<Icrm_activity_report> crm_activity_reports){
            
        }
        
        public void create(Icrm_activity_report crm_activity_report){
this.crm_activity_reportOdooClient.create(crm_activity_report) ;
        }
        
        public void get(Icrm_activity_report crm_activity_report){
            this.crm_activity_reportOdooClient.get(crm_activity_report) ;
        }
        
        public void update(Icrm_activity_report crm_activity_report){
this.crm_activity_reportOdooClient.update(crm_activity_report) ;
        }
        
        public void remove(Icrm_activity_report crm_activity_report){
this.crm_activity_reportOdooClient.remove(crm_activity_report) ;
        }
        
        public Page<Icrm_activity_report> select(SearchContext context){
            return null ;
        }
        

}

