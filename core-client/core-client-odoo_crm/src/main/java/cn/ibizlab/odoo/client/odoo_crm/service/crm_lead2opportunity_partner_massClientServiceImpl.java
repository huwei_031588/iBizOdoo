package cn.ibizlab.odoo.client.odoo_crm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icrm_lead2opportunity_partner_mass;
import cn.ibizlab.odoo.core.client.service.Icrm_lead2opportunity_partner_massClientService;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_lead2opportunity_partner_massImpl;
import cn.ibizlab.odoo.client.odoo_crm.odooclient.Icrm_lead2opportunity_partner_massOdooClient;
import cn.ibizlab.odoo.client.odoo_crm.odooclient.impl.crm_lead2opportunity_partner_massOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[crm_lead2opportunity_partner_mass] 服务对象接口
 */
@Service
public class crm_lead2opportunity_partner_massClientServiceImpl implements Icrm_lead2opportunity_partner_massClientService {
    @Autowired
    private  Icrm_lead2opportunity_partner_massOdooClient  crm_lead2opportunity_partner_massOdooClient;

    public Icrm_lead2opportunity_partner_mass createModel() {		
		return new crm_lead2opportunity_partner_massImpl();
	}


        public void updateBatch(List<Icrm_lead2opportunity_partner_mass> crm_lead2opportunity_partner_masses){
            
        }
        
        public Page<Icrm_lead2opportunity_partner_mass> search(SearchContext context){
            return this.crm_lead2opportunity_partner_massOdooClient.search(context) ;
        }
        
        public void update(Icrm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass){
this.crm_lead2opportunity_partner_massOdooClient.update(crm_lead2opportunity_partner_mass) ;
        }
        
        public void remove(Icrm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass){
this.crm_lead2opportunity_partner_massOdooClient.remove(crm_lead2opportunity_partner_mass) ;
        }
        
        public void get(Icrm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass){
            this.crm_lead2opportunity_partner_massOdooClient.get(crm_lead2opportunity_partner_mass) ;
        }
        
        public void create(Icrm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass){
this.crm_lead2opportunity_partner_massOdooClient.create(crm_lead2opportunity_partner_mass) ;
        }
        
        public void removeBatch(List<Icrm_lead2opportunity_partner_mass> crm_lead2opportunity_partner_masses){
            
        }
        
        public void createBatch(List<Icrm_lead2opportunity_partner_mass> crm_lead2opportunity_partner_masses){
            
        }
        
        public Page<Icrm_lead2opportunity_partner_mass> select(SearchContext context){
            return null ;
        }
        

}

