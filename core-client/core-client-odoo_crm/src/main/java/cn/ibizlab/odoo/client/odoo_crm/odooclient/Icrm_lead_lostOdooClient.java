package cn.ibizlab.odoo.client.odoo_crm.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Icrm_lead_lost;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[crm_lead_lost] 服务对象客户端接口
 */
public interface Icrm_lead_lostOdooClient {
    
        public void removeBatch(Icrm_lead_lost crm_lead_lost);

        public void updateBatch(Icrm_lead_lost crm_lead_lost);

        public void get(Icrm_lead_lost crm_lead_lost);

        public void remove(Icrm_lead_lost crm_lead_lost);

        public void update(Icrm_lead_lost crm_lead_lost);

        public void createBatch(Icrm_lead_lost crm_lead_lost);

        public Page<Icrm_lead_lost> search(SearchContext context);

        public void create(Icrm_lead_lost crm_lead_lost);

        public List<Icrm_lead_lost> select();


}