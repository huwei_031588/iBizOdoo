package cn.ibizlab.odoo.client.odoo_digest.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Idigest_digest;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[digest_digest] 服务对象客户端接口
 */
public interface Idigest_digestOdooClient {
    
        public void updateBatch(Idigest_digest digest_digest);

        public void createBatch(Idigest_digest digest_digest);

        public void create(Idigest_digest digest_digest);

        public void get(Idigest_digest digest_digest);

        public void removeBatch(Idigest_digest digest_digest);

        public void update(Idigest_digest digest_digest);

        public Page<Idigest_digest> search(SearchContext context);

        public void remove(Idigest_digest digest_digest);

        public List<Idigest_digest> select();


}