package cn.ibizlab.odoo.client.odoo_digest.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.digest")
@Data
public class odoo_digestClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
