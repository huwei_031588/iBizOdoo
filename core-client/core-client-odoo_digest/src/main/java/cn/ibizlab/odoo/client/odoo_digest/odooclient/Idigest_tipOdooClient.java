package cn.ibizlab.odoo.client.odoo_digest.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Idigest_tip;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[digest_tip] 服务对象客户端接口
 */
public interface Idigest_tipOdooClient {
    
        public void updateBatch(Idigest_tip digest_tip);

        public Page<Idigest_tip> search(SearchContext context);

        public void get(Idigest_tip digest_tip);

        public void remove(Idigest_tip digest_tip);

        public void createBatch(Idigest_tip digest_tip);

        public void update(Idigest_tip digest_tip);

        public void create(Idigest_tip digest_tip);

        public void removeBatch(Idigest_tip digest_tip);

        public List<Idigest_tip> select();


}