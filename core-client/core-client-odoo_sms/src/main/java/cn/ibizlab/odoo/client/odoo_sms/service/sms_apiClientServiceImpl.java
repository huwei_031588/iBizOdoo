package cn.ibizlab.odoo.client.odoo_sms.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isms_api;
import cn.ibizlab.odoo.core.client.service.Isms_apiClientService;
import cn.ibizlab.odoo.client.odoo_sms.model.sms_apiImpl;
import cn.ibizlab.odoo.client.odoo_sms.odooclient.Isms_apiOdooClient;
import cn.ibizlab.odoo.client.odoo_sms.odooclient.impl.sms_apiOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[sms_api] 服务对象接口
 */
@Service
public class sms_apiClientServiceImpl implements Isms_apiClientService {
    @Autowired
    private  Isms_apiOdooClient  sms_apiOdooClient;

    public Isms_api createModel() {		
		return new sms_apiImpl();
	}


        public void create(Isms_api sms_api){
this.sms_apiOdooClient.create(sms_api) ;
        }
        
        public void updateBatch(List<Isms_api> sms_apis){
            
        }
        
        public Page<Isms_api> search(SearchContext context){
            return this.sms_apiOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Isms_api> sms_apis){
            
        }
        
        public void remove(Isms_api sms_api){
this.sms_apiOdooClient.remove(sms_api) ;
        }
        
        public void get(Isms_api sms_api){
            this.sms_apiOdooClient.get(sms_api) ;
        }
        
        public void update(Isms_api sms_api){
this.sms_apiOdooClient.update(sms_api) ;
        }
        
        public void createBatch(List<Isms_api> sms_apis){
            
        }
        
        public Page<Isms_api> select(SearchContext context){
            return null ;
        }
        

}

