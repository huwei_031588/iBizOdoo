package cn.ibizlab.odoo.client.odoo_utm.odooclient.impl;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.sql.Timestamp;
import java.security.Principal;
import java.net.URL;
import java.util.*;

import cn.ibizlab.odoo.client.odoo_utm.model.utm_mediumImpl;
import cn.ibizlab.odoo.client.odoo_utm.odooclient.Iutm_mediumOdooClient;
import cn.ibizlab.odoo.core.client.model.Iutm_medium;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.util.helper.OdooSearchContextHelper;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;
import cn.ibizlab.odoo.util.config.OdooClientProperties;

import com.google.common.collect.Lists;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageImpl;

@Component
public class utm_mediumOdooClient implements Iutm_mediumOdooClient {

	@Autowired
	OdooClientProperties odooClientProperties;

	public static String URL ;
	public static String DB ;
	public static Integer USERID  ;
	public static String PASS ;

	public static XmlRpcClient client ;

	public utm_mediumOdooClient(){
		
	}

	public void initClient () {
		if(client == null) {
			URL = odooClientProperties.getUrl();
			DB = odooClientProperties.getDb();
			USERID = odooClientProperties.getUserId();
			PASS = odooClientProperties.getPass();
			XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
			config.setEnabledForExtensions(true);
			client = new XmlRpcClient();
			try{
				config.setServerURL(new URL(String.format("%s/xmlrpc/2/object", URL)));
			}catch(Exception e){
				System.out.println(e.getMessage());
			}
			client.setConfig(config);
		}
	}

	public void removeBatch(Iutm_medium utm_medium){
		initClient();
		try{
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void createBatch(Iutm_medium utm_medium){
		initClient();
		try{
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void create(Iutm_medium utm_medium){
		initClient();
		try{
			Map map = utm_medium.toMap();
			Integer id = (Integer) client.execute(
				"execute_kw", Arrays.asList(DB, USERID, PASS, "utm.medium", "create",Arrays.asList(map)));
			List<Object> maps = Arrays.asList((Object[]) client.execute(
				"execute_kw", Arrays.asList(DB, USERID, PASS, "utm.medium", "search_read",
					Arrays.asList(Arrays.asList(
								 Arrays.asList("id", "=", id)
					)),
					new HashMap() {
						{
								put("limit",1);
						}
			})));
			if (maps != null && maps.size() > 0) {
				utm_medium.fromMap((Map<String, Object>)maps.get(0));
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void remove(Iutm_medium utm_medium){
		initClient();
		try{
			Integer id = utm_medium.getId();
			client.execute("execute_kw", Arrays.asList(
				DB, USERID, PASS,
				"utm.medium", "unlink",
				Arrays.asList(Arrays.asList(id))));
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void updateBatch(Iutm_medium utm_medium){
		initClient();
		try{
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

    public Page<Iutm_medium> search(SearchContext context) {
		initClient();
		List<Iutm_medium> utm_mediumList = new ArrayList<>();
		long totalnum = 0;
		try{
			totalnum = (Integer)client.execute(
				"execute_kw", Arrays.asList(
   						DB, USERID, PASS, "utm.medium", "search_count",
    					OdooSearchContextHelper.getConditions(context)
			));
			if(totalnum<1)
				return new PageImpl(utm_mediumList,context.getPageable(),totalnum);
			List<Object> maps = Arrays.asList((Object[]) client.execute(	
				"execute_kw", Arrays.asList(
						DB, USERID, PASS, "utm.medium", "search_read",
						OdooSearchContextHelper.getConditions(context),
						new HashMap() {
							{				
								put("order",OdooSearchContextHelper.getSorts(context));	
								put("offset",(int)context.getPageable().getOffset());
								put("limit", context.getPageable().getPageSize());
							}
			})));
			utm_mediumList = mapsToObjects(maps);
		}catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		
		return new PageImpl(utm_mediumList,context.getPageable(),totalnum);
	}
	public void update(Iutm_medium utm_medium){
		initClient();
		try{
			Integer id =utm_medium.getId();
			Map map = utm_medium.toMap();
			client.execute(
				"execute_kw", Arrays.asList(DB, USERID, PASS, "utm.medium", "write",Arrays.asList(Arrays.asList(id),map)));
			List<Object> maps = Arrays.asList((Object[]) client.execute(
				"execute_kw", Arrays.asList(DB, USERID, PASS, "utm.medium", "search_read",
					Arrays.asList(Arrays.asList(
								 Arrays.asList("id", "=", id)
					)),
					new HashMap() {
						{
								put("limit",1);
						}
			})));
			if (maps != null && maps.size() > 0) {
				utm_medium.fromMap((Map<String, Object>)maps.get(0));
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void get(Iutm_medium utm_medium){
		initClient();
		try{
		 	List<Object> maps = Arrays.asList((Object[]) client.execute(
				"execute_kw", Arrays.asList(DB, USERID, PASS, "utm.medium", "search_read",
					Arrays.asList(Arrays.asList(
								 Arrays.asList("id", "=", utm_medium.getId())
					)),
					new HashMap() {
						{
								put("limit",1);
						}
			})));
			if (maps != null && maps.size() > 0) {
				utm_medium.fromMap((Map<String, Object>)maps.get(0));
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

    public List<Iutm_medium> select() {
			return null;
	}




	private List<Iutm_medium> mapsToObjects(List<Object> maps) throws Exception {
		List<Iutm_medium> list = Lists.newArrayList();
		if (maps != null && maps.size() > 0) {
			Map<String, Object> map = null;
			for (int i = 0,size = maps.size(); i < size; i++) {
				map = (Map<String, Object>)maps.get(i);
				Iutm_medium utm_medium = new utm_mediumImpl();
				utm_medium.fromMap(map);
				list.add(utm_medium);
			}
		}
		return list;
	}

	public void preAction (Principal principal,OdooClientHelper.CurOdooUser curUser) {
		String curUserName = principal.getName();
		int curUserId = 0;
		String dyncPassWord = OdooClientHelper.getCurUserdynaPass(curUserName,PASS);
		try{
			final XmlRpcClientConfigImpl common_config = new XmlRpcClientConfigImpl();
			common_config.setServerURL(
				new URL(String.format("%s/xmlrpc/2/common", URL)));
			//获取用户id
			curUserId = (int)client.execute(
				common_config, "authenticate", Arrays.asList(
						DB, curUserName, dyncPassWord, new HashMap()));
		}catch(Exception e){
			System.out.println("用户校验失败，可能无用户或密码错误"+ e.getMessage());
		}

		if(curUserId == 0) {
			try {
				//修改动态密码
				client.execute("execute_kw", Arrays.asList(
						DB, USERID, PASS,
						"res.users", "write",
						Arrays.asList(Arrays.asList(6),
							new HashMap() {{
								put("name", curUserName);
								put("new_password", dyncPassWord);
							}}
						)
				));
			} catch (Exception e) {
				System.out.println("用户密码重置，可能无用户" + e.getMessage());
			}
		}
		curUser.setUserId(curUserId);
		curUser.setPass(dyncPassWord);
	}

}
