package cn.ibizlab.odoo.client.odoo_utm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iutm_source;
import cn.ibizlab.odoo.core.client.service.Iutm_sourceClientService;
import cn.ibizlab.odoo.client.odoo_utm.model.utm_sourceImpl;
import cn.ibizlab.odoo.client.odoo_utm.odooclient.Iutm_sourceOdooClient;
import cn.ibizlab.odoo.client.odoo_utm.odooclient.impl.utm_sourceOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[utm_source] 服务对象接口
 */
@Service
public class utm_sourceClientServiceImpl implements Iutm_sourceClientService {
    @Autowired
    private  Iutm_sourceOdooClient  utm_sourceOdooClient;

    public Iutm_source createModel() {		
		return new utm_sourceImpl();
	}


        public void createBatch(List<Iutm_source> utm_sources){
            
        }
        
        public void updateBatch(List<Iutm_source> utm_sources){
            
        }
        
        public void create(Iutm_source utm_source){
this.utm_sourceOdooClient.create(utm_source) ;
        }
        
        public void get(Iutm_source utm_source){
            this.utm_sourceOdooClient.get(utm_source) ;
        }
        
        public void removeBatch(List<Iutm_source> utm_sources){
            
        }
        
        public Page<Iutm_source> search(SearchContext context){
            return this.utm_sourceOdooClient.search(context) ;
        }
        
        public void remove(Iutm_source utm_source){
this.utm_sourceOdooClient.remove(utm_source) ;
        }
        
        public void update(Iutm_source utm_source){
this.utm_sourceOdooClient.update(utm_source) ;
        }
        
        public Page<Iutm_source> select(SearchContext context){
            return null ;
        }
        

}

