package cn.ibizlab.odoo.client.odoo_utm.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iutm_mixin;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[utm_mixin] 服务对象客户端接口
 */
public interface Iutm_mixinOdooClient {
    
        public void createBatch(Iutm_mixin utm_mixin);

        public void removeBatch(Iutm_mixin utm_mixin);

        public void remove(Iutm_mixin utm_mixin);

        public void updateBatch(Iutm_mixin utm_mixin);

        public void create(Iutm_mixin utm_mixin);

        public void get(Iutm_mixin utm_mixin);

        public Page<Iutm_mixin> search(SearchContext context);

        public void update(Iutm_mixin utm_mixin);

        public List<Iutm_mixin> select();


}