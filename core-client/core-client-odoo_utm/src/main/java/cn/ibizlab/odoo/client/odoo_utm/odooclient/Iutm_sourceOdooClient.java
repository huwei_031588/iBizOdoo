package cn.ibizlab.odoo.client.odoo_utm.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iutm_source;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[utm_source] 服务对象客户端接口
 */
public interface Iutm_sourceOdooClient {
    
        public void createBatch(Iutm_source utm_source);

        public void updateBatch(Iutm_source utm_source);

        public void create(Iutm_source utm_source);

        public void get(Iutm_source utm_source);

        public void removeBatch(Iutm_source utm_source);

        public Page<Iutm_source> search(SearchContext context);

        public void remove(Iutm_source utm_source);

        public void update(Iutm_source utm_source);

        public List<Iutm_source> select();


}