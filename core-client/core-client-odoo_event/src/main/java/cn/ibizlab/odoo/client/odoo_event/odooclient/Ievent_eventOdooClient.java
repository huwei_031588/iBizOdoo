package cn.ibizlab.odoo.client.odoo_event.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ievent_event;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[event_event] 服务对象客户端接口
 */
public interface Ievent_eventOdooClient {
    
        public Page<Ievent_event> search(SearchContext context);

        public void create(Ievent_event event_event);

        public void get(Ievent_event event_event);

        public void createBatch(Ievent_event event_event);

        public void updateBatch(Ievent_event event_event);

        public void remove(Ievent_event event_event);

        public void update(Ievent_event event_event);

        public void removeBatch(Ievent_event event_event);

        public List<Ievent_event> select();


}