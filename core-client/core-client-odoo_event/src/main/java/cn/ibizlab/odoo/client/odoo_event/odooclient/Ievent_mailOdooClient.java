package cn.ibizlab.odoo.client.odoo_event.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ievent_mail;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[event_mail] 服务对象客户端接口
 */
public interface Ievent_mailOdooClient {
    
        public void removeBatch(Ievent_mail event_mail);

        public void remove(Ievent_mail event_mail);

        public void create(Ievent_mail event_mail);

        public void createBatch(Ievent_mail event_mail);

        public void updateBatch(Ievent_mail event_mail);

        public Page<Ievent_mail> search(SearchContext context);

        public void get(Ievent_mail event_mail);

        public void update(Ievent_mail event_mail);

        public List<Ievent_mail> select();


}