package cn.ibizlab.odoo.client.odoo_event.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ievent_confirm;
import cn.ibizlab.odoo.core.client.service.Ievent_confirmClientService;
import cn.ibizlab.odoo.client.odoo_event.model.event_confirmImpl;
import cn.ibizlab.odoo.client.odoo_event.odooclient.Ievent_confirmOdooClient;
import cn.ibizlab.odoo.client.odoo_event.odooclient.impl.event_confirmOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[event_confirm] 服务对象接口
 */
@Service
public class event_confirmClientServiceImpl implements Ievent_confirmClientService {
    @Autowired
    private  Ievent_confirmOdooClient  event_confirmOdooClient;

    public Ievent_confirm createModel() {		
		return new event_confirmImpl();
	}


        public Page<Ievent_confirm> search(SearchContext context){
            return this.event_confirmOdooClient.search(context) ;
        }
        
        public void create(Ievent_confirm event_confirm){
this.event_confirmOdooClient.create(event_confirm) ;
        }
        
        public void update(Ievent_confirm event_confirm){
this.event_confirmOdooClient.update(event_confirm) ;
        }
        
        public void removeBatch(List<Ievent_confirm> event_confirms){
            
        }
        
        public void updateBatch(List<Ievent_confirm> event_confirms){
            
        }
        
        public void get(Ievent_confirm event_confirm){
            this.event_confirmOdooClient.get(event_confirm) ;
        }
        
        public void remove(Ievent_confirm event_confirm){
this.event_confirmOdooClient.remove(event_confirm) ;
        }
        
        public void createBatch(List<Ievent_confirm> event_confirms){
            
        }
        
        public Page<Ievent_confirm> select(SearchContext context){
            return null ;
        }
        

}

