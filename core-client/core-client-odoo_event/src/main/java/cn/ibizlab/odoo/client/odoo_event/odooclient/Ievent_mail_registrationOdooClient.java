package cn.ibizlab.odoo.client.odoo_event.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ievent_mail_registration;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[event_mail_registration] 服务对象客户端接口
 */
public interface Ievent_mail_registrationOdooClient {
    
        public void update(Ievent_mail_registration event_mail_registration);

        public void remove(Ievent_mail_registration event_mail_registration);

        public void createBatch(Ievent_mail_registration event_mail_registration);

        public void removeBatch(Ievent_mail_registration event_mail_registration);

        public void updateBatch(Ievent_mail_registration event_mail_registration);

        public void create(Ievent_mail_registration event_mail_registration);

        public void get(Ievent_mail_registration event_mail_registration);

        public Page<Ievent_mail_registration> search(SearchContext context);

        public List<Ievent_mail_registration> select();


}