package cn.ibizlab.odoo.client.odoo_event.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ievent_registration;
import cn.ibizlab.odoo.core.client.service.Ievent_registrationClientService;
import cn.ibizlab.odoo.client.odoo_event.model.event_registrationImpl;
import cn.ibizlab.odoo.client.odoo_event.odooclient.Ievent_registrationOdooClient;
import cn.ibizlab.odoo.client.odoo_event.odooclient.impl.event_registrationOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[event_registration] 服务对象接口
 */
@Service
public class event_registrationClientServiceImpl implements Ievent_registrationClientService {
    @Autowired
    private  Ievent_registrationOdooClient  event_registrationOdooClient;

    public Ievent_registration createModel() {		
		return new event_registrationImpl();
	}


        public void update(Ievent_registration event_registration){
this.event_registrationOdooClient.update(event_registration) ;
        }
        
        public void updateBatch(List<Ievent_registration> event_registrations){
            
        }
        
        public void remove(Ievent_registration event_registration){
this.event_registrationOdooClient.remove(event_registration) ;
        }
        
        public void get(Ievent_registration event_registration){
            this.event_registrationOdooClient.get(event_registration) ;
        }
        
        public void create(Ievent_registration event_registration){
this.event_registrationOdooClient.create(event_registration) ;
        }
        
        public void createBatch(List<Ievent_registration> event_registrations){
            
        }
        
        public void removeBatch(List<Ievent_registration> event_registrations){
            
        }
        
        public Page<Ievent_registration> search(SearchContext context){
            return this.event_registrationOdooClient.search(context) ;
        }
        
        public Page<Ievent_registration> select(SearchContext context){
            return null ;
        }
        

}

