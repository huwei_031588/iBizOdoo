package cn.ibizlab.odoo.client.odoo_event.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ievent_type;
import cn.ibizlab.odoo.core.client.service.Ievent_typeClientService;
import cn.ibizlab.odoo.client.odoo_event.model.event_typeImpl;
import cn.ibizlab.odoo.client.odoo_event.odooclient.Ievent_typeOdooClient;
import cn.ibizlab.odoo.client.odoo_event.odooclient.impl.event_typeOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[event_type] 服务对象接口
 */
@Service
public class event_typeClientServiceImpl implements Ievent_typeClientService {
    @Autowired
    private  Ievent_typeOdooClient  event_typeOdooClient;

    public Ievent_type createModel() {		
		return new event_typeImpl();
	}


        public void updateBatch(List<Ievent_type> event_types){
            
        }
        
        public void createBatch(List<Ievent_type> event_types){
            
        }
        
        public void removeBatch(List<Ievent_type> event_types){
            
        }
        
        public Page<Ievent_type> search(SearchContext context){
            return this.event_typeOdooClient.search(context) ;
        }
        
        public void get(Ievent_type event_type){
            this.event_typeOdooClient.get(event_type) ;
        }
        
        public void create(Ievent_type event_type){
this.event_typeOdooClient.create(event_type) ;
        }
        
        public void remove(Ievent_type event_type){
this.event_typeOdooClient.remove(event_type) ;
        }
        
        public void update(Ievent_type event_type){
this.event_typeOdooClient.update(event_type) ;
        }
        
        public Page<Ievent_type> select(SearchContext context){
            return null ;
        }
        

}

