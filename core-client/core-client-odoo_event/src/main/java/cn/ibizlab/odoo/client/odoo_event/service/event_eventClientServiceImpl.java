package cn.ibizlab.odoo.client.odoo_event.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ievent_event;
import cn.ibizlab.odoo.core.client.service.Ievent_eventClientService;
import cn.ibizlab.odoo.client.odoo_event.model.event_eventImpl;
import cn.ibizlab.odoo.client.odoo_event.odooclient.Ievent_eventOdooClient;
import cn.ibizlab.odoo.client.odoo_event.odooclient.impl.event_eventOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[event_event] 服务对象接口
 */
@Service
public class event_eventClientServiceImpl implements Ievent_eventClientService {
    @Autowired
    private  Ievent_eventOdooClient  event_eventOdooClient;

    public Ievent_event createModel() {		
		return new event_eventImpl();
	}


        public Page<Ievent_event> search(SearchContext context){
            return this.event_eventOdooClient.search(context) ;
        }
        
        public void create(Ievent_event event_event){
this.event_eventOdooClient.create(event_event) ;
        }
        
        public void get(Ievent_event event_event){
            this.event_eventOdooClient.get(event_event) ;
        }
        
        public void createBatch(List<Ievent_event> event_events){
            
        }
        
        public void updateBatch(List<Ievent_event> event_events){
            
        }
        
        public void remove(Ievent_event event_event){
this.event_eventOdooClient.remove(event_event) ;
        }
        
        public void update(Ievent_event event_event){
this.event_eventOdooClient.update(event_event) ;
        }
        
        public void removeBatch(List<Ievent_event> event_events){
            
        }
        
        public Page<Ievent_event> select(SearchContext context){
            return null ;
        }
        

}

