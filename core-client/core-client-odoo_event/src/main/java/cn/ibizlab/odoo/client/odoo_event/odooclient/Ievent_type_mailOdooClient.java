package cn.ibizlab.odoo.client.odoo_event.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ievent_type_mail;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[event_type_mail] 服务对象客户端接口
 */
public interface Ievent_type_mailOdooClient {
    
        public void createBatch(Ievent_type_mail event_type_mail);

        public void update(Ievent_type_mail event_type_mail);

        public Page<Ievent_type_mail> search(SearchContext context);

        public void remove(Ievent_type_mail event_type_mail);

        public void updateBatch(Ievent_type_mail event_type_mail);

        public void removeBatch(Ievent_type_mail event_type_mail);

        public void get(Ievent_type_mail event_type_mail);

        public void create(Ievent_type_mail event_type_mail);

        public List<Ievent_type_mail> select();


}