package cn.ibizlab.odoo.client.odoo_calendar.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icalendar_alarm;
import cn.ibizlab.odoo.core.client.service.Icalendar_alarmClientService;
import cn.ibizlab.odoo.client.odoo_calendar.model.calendar_alarmImpl;
import cn.ibizlab.odoo.client.odoo_calendar.odooclient.Icalendar_alarmOdooClient;
import cn.ibizlab.odoo.client.odoo_calendar.odooclient.impl.calendar_alarmOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[calendar_alarm] 服务对象接口
 */
@Service
public class calendar_alarmClientServiceImpl implements Icalendar_alarmClientService {
    @Autowired
    private  Icalendar_alarmOdooClient  calendar_alarmOdooClient;

    public Icalendar_alarm createModel() {		
		return new calendar_alarmImpl();
	}


        public void updateBatch(List<Icalendar_alarm> calendar_alarms){
            
        }
        
        public void create(Icalendar_alarm calendar_alarm){
this.calendar_alarmOdooClient.create(calendar_alarm) ;
        }
        
        public void update(Icalendar_alarm calendar_alarm){
this.calendar_alarmOdooClient.update(calendar_alarm) ;
        }
        
        public void remove(Icalendar_alarm calendar_alarm){
this.calendar_alarmOdooClient.remove(calendar_alarm) ;
        }
        
        public Page<Icalendar_alarm> search(SearchContext context){
            return this.calendar_alarmOdooClient.search(context) ;
        }
        
        public void createBatch(List<Icalendar_alarm> calendar_alarms){
            
        }
        
        public void removeBatch(List<Icalendar_alarm> calendar_alarms){
            
        }
        
        public void get(Icalendar_alarm calendar_alarm){
            this.calendar_alarmOdooClient.get(calendar_alarm) ;
        }
        
        public Page<Icalendar_alarm> select(SearchContext context){
            return null ;
        }
        

}

