package cn.ibizlab.odoo.client.odoo_calendar.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Icalendar_event;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[calendar_event] 服务对象客户端接口
 */
public interface Icalendar_eventOdooClient {
    
        public void get(Icalendar_event calendar_event);

        public void update(Icalendar_event calendar_event);

        public Page<Icalendar_event> search(SearchContext context);

        public void createBatch(Icalendar_event calendar_event);

        public void updateBatch(Icalendar_event calendar_event);

        public void removeBatch(Icalendar_event calendar_event);

        public void create(Icalendar_event calendar_event);

        public void remove(Icalendar_event calendar_event);

        public List<Icalendar_event> select();


}