package cn.ibizlab.odoo.client.odoo_calendar.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icalendar_event_type;
import cn.ibizlab.odoo.core.client.service.Icalendar_event_typeClientService;
import cn.ibizlab.odoo.client.odoo_calendar.model.calendar_event_typeImpl;
import cn.ibizlab.odoo.client.odoo_calendar.odooclient.Icalendar_event_typeOdooClient;
import cn.ibizlab.odoo.client.odoo_calendar.odooclient.impl.calendar_event_typeOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[calendar_event_type] 服务对象接口
 */
@Service
public class calendar_event_typeClientServiceImpl implements Icalendar_event_typeClientService {
    @Autowired
    private  Icalendar_event_typeOdooClient  calendar_event_typeOdooClient;

    public Icalendar_event_type createModel() {		
		return new calendar_event_typeImpl();
	}


        public void createBatch(List<Icalendar_event_type> calendar_event_types){
            
        }
        
        public void update(Icalendar_event_type calendar_event_type){
this.calendar_event_typeOdooClient.update(calendar_event_type) ;
        }
        
        public void get(Icalendar_event_type calendar_event_type){
            this.calendar_event_typeOdooClient.get(calendar_event_type) ;
        }
        
        public Page<Icalendar_event_type> search(SearchContext context){
            return this.calendar_event_typeOdooClient.search(context) ;
        }
        
        public void create(Icalendar_event_type calendar_event_type){
this.calendar_event_typeOdooClient.create(calendar_event_type) ;
        }
        
        public void remove(Icalendar_event_type calendar_event_type){
this.calendar_event_typeOdooClient.remove(calendar_event_type) ;
        }
        
        public void updateBatch(List<Icalendar_event_type> calendar_event_types){
            
        }
        
        public void removeBatch(List<Icalendar_event_type> calendar_event_types){
            
        }
        
        public Page<Icalendar_event_type> select(SearchContext context){
            return null ;
        }
        

}

