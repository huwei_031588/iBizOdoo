package cn.ibizlab.odoo.client.odoo_calendar.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icalendar_alarm_manager;
import cn.ibizlab.odoo.core.client.service.Icalendar_alarm_managerClientService;
import cn.ibizlab.odoo.client.odoo_calendar.model.calendar_alarm_managerImpl;
import cn.ibizlab.odoo.client.odoo_calendar.odooclient.Icalendar_alarm_managerOdooClient;
import cn.ibizlab.odoo.client.odoo_calendar.odooclient.impl.calendar_alarm_managerOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[calendar_alarm_manager] 服务对象接口
 */
@Service
public class calendar_alarm_managerClientServiceImpl implements Icalendar_alarm_managerClientService {
    @Autowired
    private  Icalendar_alarm_managerOdooClient  calendar_alarm_managerOdooClient;

    public Icalendar_alarm_manager createModel() {		
		return new calendar_alarm_managerImpl();
	}


        public void updateBatch(List<Icalendar_alarm_manager> calendar_alarm_managers){
            
        }
        
        public void update(Icalendar_alarm_manager calendar_alarm_manager){
this.calendar_alarm_managerOdooClient.update(calendar_alarm_manager) ;
        }
        
        public void removeBatch(List<Icalendar_alarm_manager> calendar_alarm_managers){
            
        }
        
        public void get(Icalendar_alarm_manager calendar_alarm_manager){
            this.calendar_alarm_managerOdooClient.get(calendar_alarm_manager) ;
        }
        
        public Page<Icalendar_alarm_manager> search(SearchContext context){
            return this.calendar_alarm_managerOdooClient.search(context) ;
        }
        
        public void remove(Icalendar_alarm_manager calendar_alarm_manager){
this.calendar_alarm_managerOdooClient.remove(calendar_alarm_manager) ;
        }
        
        public void create(Icalendar_alarm_manager calendar_alarm_manager){
this.calendar_alarm_managerOdooClient.create(calendar_alarm_manager) ;
        }
        
        public void createBatch(List<Icalendar_alarm_manager> calendar_alarm_managers){
            
        }
        
        public Page<Icalendar_alarm_manager> select(SearchContext context){
            return null ;
        }
        

}

