package cn.ibizlab.odoo.client.odoo_calendar.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icalendar_attendee;
import cn.ibizlab.odoo.core.client.service.Icalendar_attendeeClientService;
import cn.ibizlab.odoo.client.odoo_calendar.model.calendar_attendeeImpl;
import cn.ibizlab.odoo.client.odoo_calendar.odooclient.Icalendar_attendeeOdooClient;
import cn.ibizlab.odoo.client.odoo_calendar.odooclient.impl.calendar_attendeeOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[calendar_attendee] 服务对象接口
 */
@Service
public class calendar_attendeeClientServiceImpl implements Icalendar_attendeeClientService {
    @Autowired
    private  Icalendar_attendeeOdooClient  calendar_attendeeOdooClient;

    public Icalendar_attendee createModel() {		
		return new calendar_attendeeImpl();
	}


        public void update(Icalendar_attendee calendar_attendee){
this.calendar_attendeeOdooClient.update(calendar_attendee) ;
        }
        
        public void createBatch(List<Icalendar_attendee> calendar_attendees){
            
        }
        
        public void create(Icalendar_attendee calendar_attendee){
this.calendar_attendeeOdooClient.create(calendar_attendee) ;
        }
        
        public void removeBatch(List<Icalendar_attendee> calendar_attendees){
            
        }
        
        public void get(Icalendar_attendee calendar_attendee){
            this.calendar_attendeeOdooClient.get(calendar_attendee) ;
        }
        
        public void updateBatch(List<Icalendar_attendee> calendar_attendees){
            
        }
        
        public void remove(Icalendar_attendee calendar_attendee){
this.calendar_attendeeOdooClient.remove(calendar_attendee) ;
        }
        
        public Page<Icalendar_attendee> search(SearchContext context){
            return this.calendar_attendeeOdooClient.search(context) ;
        }
        
        public Page<Icalendar_attendee> select(SearchContext context){
            return null ;
        }
        

}

