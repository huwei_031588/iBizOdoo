package cn.ibizlab.odoo.client.odoo_snailmail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Isnailmail_letter;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[snailmail_letter] 服务对象客户端接口
 */
public interface Isnailmail_letterOdooClient {
    
        public void create(Isnailmail_letter snailmail_letter);

        public void get(Isnailmail_letter snailmail_letter);

        public void update(Isnailmail_letter snailmail_letter);

        public Page<Isnailmail_letter> search(SearchContext context);

        public void remove(Isnailmail_letter snailmail_letter);

        public void removeBatch(Isnailmail_letter snailmail_letter);

        public void createBatch(Isnailmail_letter snailmail_letter);

        public void updateBatch(Isnailmail_letter snailmail_letter);

        public List<Isnailmail_letter> select();


}