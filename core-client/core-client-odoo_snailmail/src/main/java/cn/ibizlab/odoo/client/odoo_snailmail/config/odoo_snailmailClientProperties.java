package cn.ibizlab.odoo.client.odoo_snailmail.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.snailmail")
@Data
public class odoo_snailmailClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
