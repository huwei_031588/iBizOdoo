package cn.ibizlab.odoo.client.odoo_maintenance.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imaintenance_request;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[maintenance_request] 服务对象客户端接口
 */
public interface Imaintenance_requestOdooClient {
    
        public Page<Imaintenance_request> search(SearchContext context);

        public void updateBatch(Imaintenance_request maintenance_request);

        public void create(Imaintenance_request maintenance_request);

        public void remove(Imaintenance_request maintenance_request);

        public void createBatch(Imaintenance_request maintenance_request);

        public void get(Imaintenance_request maintenance_request);

        public void update(Imaintenance_request maintenance_request);

        public void removeBatch(Imaintenance_request maintenance_request);

        public List<Imaintenance_request> select();


}