package cn.ibizlab.odoo.client.odoo_maintenance.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imaintenance_equipment;
import cn.ibizlab.odoo.client.odoo_maintenance.model.maintenance_equipmentImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[maintenance_equipment] 服务对象接口
 */
public interface maintenance_equipmentFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_maintenance/maintenance_equipments/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_maintenance/maintenance_equipments/removebatch")
    public maintenance_equipmentImpl removeBatch(@RequestBody List<maintenance_equipmentImpl> maintenance_equipments);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_maintenance/maintenance_equipments")
    public maintenance_equipmentImpl create(@RequestBody maintenance_equipmentImpl maintenance_equipment);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_maintenance/maintenance_equipments/updatebatch")
    public maintenance_equipmentImpl updateBatch(@RequestBody List<maintenance_equipmentImpl> maintenance_equipments);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_maintenance/maintenance_equipments/{id}")
    public maintenance_equipmentImpl update(@PathVariable("id") Integer id,@RequestBody maintenance_equipmentImpl maintenance_equipment);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_maintenance/maintenance_equipments/{id}")
    public maintenance_equipmentImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_maintenance/maintenance_equipments/search")
    public Page<maintenance_equipmentImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_maintenance/maintenance_equipments/createbatch")
    public maintenance_equipmentImpl createBatch(@RequestBody List<maintenance_equipmentImpl> maintenance_equipments);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_maintenance/maintenance_equipments/select")
    public Page<maintenance_equipmentImpl> select();



}
