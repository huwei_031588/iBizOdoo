package cn.ibizlab.odoo.client.odoo_maintenance.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imaintenance_equipment_category;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[maintenance_equipment_category] 服务对象客户端接口
 */
public interface Imaintenance_equipment_categoryOdooClient {
    
        public void createBatch(Imaintenance_equipment_category maintenance_equipment_category);

        public void create(Imaintenance_equipment_category maintenance_equipment_category);

        public Page<Imaintenance_equipment_category> search(SearchContext context);

        public void update(Imaintenance_equipment_category maintenance_equipment_category);

        public void get(Imaintenance_equipment_category maintenance_equipment_category);

        public void updateBatch(Imaintenance_equipment_category maintenance_equipment_category);

        public void removeBatch(Imaintenance_equipment_category maintenance_equipment_category);

        public void remove(Imaintenance_equipment_category maintenance_equipment_category);

        public List<Imaintenance_equipment_category> select();


}