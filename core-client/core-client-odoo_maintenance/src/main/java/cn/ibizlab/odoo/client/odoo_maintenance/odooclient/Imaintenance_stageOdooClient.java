package cn.ibizlab.odoo.client.odoo_maintenance.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imaintenance_stage;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[maintenance_stage] 服务对象客户端接口
 */
public interface Imaintenance_stageOdooClient {
    
        public void get(Imaintenance_stage maintenance_stage);

        public void create(Imaintenance_stage maintenance_stage);

        public void remove(Imaintenance_stage maintenance_stage);

        public void update(Imaintenance_stage maintenance_stage);

        public Page<Imaintenance_stage> search(SearchContext context);

        public void removeBatch(Imaintenance_stage maintenance_stage);

        public void updateBatch(Imaintenance_stage maintenance_stage);

        public void createBatch(Imaintenance_stage maintenance_stage);

        public List<Imaintenance_stage> select();


}