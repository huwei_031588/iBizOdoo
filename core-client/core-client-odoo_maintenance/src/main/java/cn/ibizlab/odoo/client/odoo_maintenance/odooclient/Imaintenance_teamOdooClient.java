package cn.ibizlab.odoo.client.odoo_maintenance.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imaintenance_team;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[maintenance_team] 服务对象客户端接口
 */
public interface Imaintenance_teamOdooClient {
    
        public void get(Imaintenance_team maintenance_team);

        public void remove(Imaintenance_team maintenance_team);

        public void update(Imaintenance_team maintenance_team);

        public void createBatch(Imaintenance_team maintenance_team);

        public Page<Imaintenance_team> search(SearchContext context);

        public void updateBatch(Imaintenance_team maintenance_team);

        public void create(Imaintenance_team maintenance_team);

        public void removeBatch(Imaintenance_team maintenance_team);

        public List<Imaintenance_team> select();


}