package cn.ibizlab.odoo.client.odoo_maintenance.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imaintenance_request;
import cn.ibizlab.odoo.core.client.service.Imaintenance_requestClientService;
import cn.ibizlab.odoo.client.odoo_maintenance.model.maintenance_requestImpl;
import cn.ibizlab.odoo.client.odoo_maintenance.odooclient.Imaintenance_requestOdooClient;
import cn.ibizlab.odoo.client.odoo_maintenance.odooclient.impl.maintenance_requestOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[maintenance_request] 服务对象接口
 */
@Service
public class maintenance_requestClientServiceImpl implements Imaintenance_requestClientService {
    @Autowired
    private  Imaintenance_requestOdooClient  maintenance_requestOdooClient;

    public Imaintenance_request createModel() {		
		return new maintenance_requestImpl();
	}


        public Page<Imaintenance_request> search(SearchContext context){
            return this.maintenance_requestOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Imaintenance_request> maintenance_requests){
            
        }
        
        public void create(Imaintenance_request maintenance_request){
this.maintenance_requestOdooClient.create(maintenance_request) ;
        }
        
        public void remove(Imaintenance_request maintenance_request){
this.maintenance_requestOdooClient.remove(maintenance_request) ;
        }
        
        public void createBatch(List<Imaintenance_request> maintenance_requests){
            
        }
        
        public void get(Imaintenance_request maintenance_request){
            this.maintenance_requestOdooClient.get(maintenance_request) ;
        }
        
        public void update(Imaintenance_request maintenance_request){
this.maintenance_requestOdooClient.update(maintenance_request) ;
        }
        
        public void removeBatch(List<Imaintenance_request> maintenance_requests){
            
        }
        
        public Page<Imaintenance_request> select(SearchContext context){
            return null ;
        }
        

}

