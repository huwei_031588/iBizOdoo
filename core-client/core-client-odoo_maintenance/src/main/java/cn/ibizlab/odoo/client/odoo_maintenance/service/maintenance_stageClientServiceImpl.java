package cn.ibizlab.odoo.client.odoo_maintenance.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imaintenance_stage;
import cn.ibizlab.odoo.core.client.service.Imaintenance_stageClientService;
import cn.ibizlab.odoo.client.odoo_maintenance.model.maintenance_stageImpl;
import cn.ibizlab.odoo.client.odoo_maintenance.odooclient.Imaintenance_stageOdooClient;
import cn.ibizlab.odoo.client.odoo_maintenance.odooclient.impl.maintenance_stageOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[maintenance_stage] 服务对象接口
 */
@Service
public class maintenance_stageClientServiceImpl implements Imaintenance_stageClientService {
    @Autowired
    private  Imaintenance_stageOdooClient  maintenance_stageOdooClient;

    public Imaintenance_stage createModel() {		
		return new maintenance_stageImpl();
	}


        public void get(Imaintenance_stage maintenance_stage){
            this.maintenance_stageOdooClient.get(maintenance_stage) ;
        }
        
        public void create(Imaintenance_stage maintenance_stage){
this.maintenance_stageOdooClient.create(maintenance_stage) ;
        }
        
        public void remove(Imaintenance_stage maintenance_stage){
this.maintenance_stageOdooClient.remove(maintenance_stage) ;
        }
        
        public void update(Imaintenance_stage maintenance_stage){
this.maintenance_stageOdooClient.update(maintenance_stage) ;
        }
        
        public Page<Imaintenance_stage> search(SearchContext context){
            return this.maintenance_stageOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Imaintenance_stage> maintenance_stages){
            
        }
        
        public void updateBatch(List<Imaintenance_stage> maintenance_stages){
            
        }
        
        public void createBatch(List<Imaintenance_stage> maintenance_stages){
            
        }
        
        public Page<Imaintenance_stage> select(SearchContext context){
            return null ;
        }
        

}

