package cn.ibizlab.odoo.client.odoo_project.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iproject_project;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[project_project] 服务对象客户端接口
 */
public interface Iproject_projectOdooClient {
    
        public void updateBatch(Iproject_project project_project);

        public Page<Iproject_project> search(SearchContext context);

        public void createBatch(Iproject_project project_project);

        public void removeBatch(Iproject_project project_project);

        public void remove(Iproject_project project_project);

        public void create(Iproject_project project_project);

        public void get(Iproject_project project_project);

        public void update(Iproject_project project_project);

        public List<Iproject_project> select();


}