package cn.ibizlab.odoo.client.odoo_project.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproject_project;
import cn.ibizlab.odoo.core.client.service.Iproject_projectClientService;
import cn.ibizlab.odoo.client.odoo_project.model.project_projectImpl;
import cn.ibizlab.odoo.client.odoo_project.odooclient.Iproject_projectOdooClient;
import cn.ibizlab.odoo.client.odoo_project.odooclient.impl.project_projectOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[project_project] 服务对象接口
 */
@Service
public class project_projectClientServiceImpl implements Iproject_projectClientService {
    @Autowired
    private  Iproject_projectOdooClient  project_projectOdooClient;

    public Iproject_project createModel() {		
		return new project_projectImpl();
	}


        public void updateBatch(List<Iproject_project> project_projects){
            
        }
        
        public Page<Iproject_project> search(SearchContext context){
            return this.project_projectOdooClient.search(context) ;
        }
        
        public void createBatch(List<Iproject_project> project_projects){
            
        }
        
        public void removeBatch(List<Iproject_project> project_projects){
            
        }
        
        public void remove(Iproject_project project_project){
this.project_projectOdooClient.remove(project_project) ;
        }
        
        public void create(Iproject_project project_project){
this.project_projectOdooClient.create(project_project) ;
        }
        
        public void get(Iproject_project project_project){
            this.project_projectOdooClient.get(project_project) ;
        }
        
        public void update(Iproject_project project_project){
this.project_projectOdooClient.update(project_project) ;
        }
        
        public Page<Iproject_project> select(SearchContext context){
            return null ;
        }
        

}

