package cn.ibizlab.odoo.client.odoo_sale.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Isale_order_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[sale_order_line] 服务对象客户端接口
 */
public interface Isale_order_lineOdooClient {
    
        public Page<Isale_order_line> search(SearchContext context);

        public void removeBatch(Isale_order_line sale_order_line);

        public void update(Isale_order_line sale_order_line);

        public void createBatch(Isale_order_line sale_order_line);

        public void remove(Isale_order_line sale_order_line);

        public void get(Isale_order_line sale_order_line);

        public void create(Isale_order_line sale_order_line);

        public void updateBatch(Isale_order_line sale_order_line);

        public List<Isale_order_line> select();


}