package cn.ibizlab.odoo.client.odoo_sale.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Isale_order_template;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[sale_order_template] 对象
 */
public class sale_order_templateImpl implements Isale_order_template,Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 确认邮件
     */
    public Integer mail_template_id;

    @JsonIgnore
    public boolean mail_template_idDirtyFlag;
    
    /**
     * 确认邮件
     */
    public String mail_template_id_text;

    @JsonIgnore
    public boolean mail_template_id_textDirtyFlag;
    
    /**
     * 报价单模板
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 条款和条件
     */
    public String note;

    @JsonIgnore
    public boolean noteDirtyFlag;
    
    /**
     * 报价单时长
     */
    public Integer number_of_days;

    @JsonIgnore
    public boolean number_of_daysDirtyFlag;
    
    /**
     * 在线支付
     */
    public String require_payment;

    @JsonIgnore
    public boolean require_paymentDirtyFlag;
    
    /**
     * 在线签名
     */
    public String require_signature;

    @JsonIgnore
    public boolean require_signatureDirtyFlag;
    
    /**
     * 明细行
     */
    public String sale_order_template_line_ids;

    @JsonIgnore
    public boolean sale_order_template_line_idsDirtyFlag;
    
    /**
     * 可选产品
     */
    public String sale_order_template_option_ids;

    @JsonIgnore
    public boolean sale_order_template_option_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [确认邮件]
     */
    @JsonProperty("mail_template_id")
    public Integer getMail_template_id(){
        return this.mail_template_id ;
    }

    /**
     * 设置 [确认邮件]
     */
    @JsonProperty("mail_template_id")
    public void setMail_template_id(Integer  mail_template_id){
        this.mail_template_id = mail_template_id ;
        this.mail_template_idDirtyFlag = true ;
    }

     /**
     * 获取 [确认邮件]脏标记
     */
    @JsonIgnore
    public boolean getMail_template_idDirtyFlag(){
        return this.mail_template_idDirtyFlag ;
    }   

    /**
     * 获取 [确认邮件]
     */
    @JsonProperty("mail_template_id_text")
    public String getMail_template_id_text(){
        return this.mail_template_id_text ;
    }

    /**
     * 设置 [确认邮件]
     */
    @JsonProperty("mail_template_id_text")
    public void setMail_template_id_text(String  mail_template_id_text){
        this.mail_template_id_text = mail_template_id_text ;
        this.mail_template_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [确认邮件]脏标记
     */
    @JsonIgnore
    public boolean getMail_template_id_textDirtyFlag(){
        return this.mail_template_id_textDirtyFlag ;
    }   

    /**
     * 获取 [报价单模板]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [报价单模板]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [报价单模板]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [条款和条件]
     */
    @JsonProperty("note")
    public String getNote(){
        return this.note ;
    }

    /**
     * 设置 [条款和条件]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

     /**
     * 获取 [条款和条件]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return this.noteDirtyFlag ;
    }   

    /**
     * 获取 [报价单时长]
     */
    @JsonProperty("number_of_days")
    public Integer getNumber_of_days(){
        return this.number_of_days ;
    }

    /**
     * 设置 [报价单时长]
     */
    @JsonProperty("number_of_days")
    public void setNumber_of_days(Integer  number_of_days){
        this.number_of_days = number_of_days ;
        this.number_of_daysDirtyFlag = true ;
    }

     /**
     * 获取 [报价单时长]脏标记
     */
    @JsonIgnore
    public boolean getNumber_of_daysDirtyFlag(){
        return this.number_of_daysDirtyFlag ;
    }   

    /**
     * 获取 [在线支付]
     */
    @JsonProperty("require_payment")
    public String getRequire_payment(){
        return this.require_payment ;
    }

    /**
     * 设置 [在线支付]
     */
    @JsonProperty("require_payment")
    public void setRequire_payment(String  require_payment){
        this.require_payment = require_payment ;
        this.require_paymentDirtyFlag = true ;
    }

     /**
     * 获取 [在线支付]脏标记
     */
    @JsonIgnore
    public boolean getRequire_paymentDirtyFlag(){
        return this.require_paymentDirtyFlag ;
    }   

    /**
     * 获取 [在线签名]
     */
    @JsonProperty("require_signature")
    public String getRequire_signature(){
        return this.require_signature ;
    }

    /**
     * 设置 [在线签名]
     */
    @JsonProperty("require_signature")
    public void setRequire_signature(String  require_signature){
        this.require_signature = require_signature ;
        this.require_signatureDirtyFlag = true ;
    }

     /**
     * 获取 [在线签名]脏标记
     */
    @JsonIgnore
    public boolean getRequire_signatureDirtyFlag(){
        return this.require_signatureDirtyFlag ;
    }   

    /**
     * 获取 [明细行]
     */
    @JsonProperty("sale_order_template_line_ids")
    public String getSale_order_template_line_ids(){
        return this.sale_order_template_line_ids ;
    }

    /**
     * 设置 [明细行]
     */
    @JsonProperty("sale_order_template_line_ids")
    public void setSale_order_template_line_ids(String  sale_order_template_line_ids){
        this.sale_order_template_line_ids = sale_order_template_line_ids ;
        this.sale_order_template_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [明细行]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_template_line_idsDirtyFlag(){
        return this.sale_order_template_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [可选产品]
     */
    @JsonProperty("sale_order_template_option_ids")
    public String getSale_order_template_option_ids(){
        return this.sale_order_template_option_ids ;
    }

    /**
     * 设置 [可选产品]
     */
    @JsonProperty("sale_order_template_option_ids")
    public void setSale_order_template_option_ids(String  sale_order_template_option_ids){
        this.sale_order_template_option_ids = sale_order_template_option_ids ;
        this.sale_order_template_option_idsDirtyFlag = true ;
    }

     /**
     * 获取 [可选产品]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_template_option_idsDirtyFlag(){
        return this.sale_order_template_option_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(map.get("active") instanceof Boolean){
			this.setActive(((Boolean)map.get("active"))? "true" : "false");
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("mail_template_id") instanceof Boolean)&& map.get("mail_template_id")!=null){
			Object[] objs = (Object[])map.get("mail_template_id");
			if(objs.length > 0){
				this.setMail_template_id((Integer)objs[0]);
			}
		}
		if(!(map.get("mail_template_id") instanceof Boolean)&& map.get("mail_template_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("mail_template_id");
			if(objs.length > 1){
				this.setMail_template_id_text((String)objs[1]);
			}
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("note") instanceof Boolean)&& map.get("note")!=null){
			this.setNote((String)map.get("note"));
		}
		if(!(map.get("number_of_days") instanceof Boolean)&& map.get("number_of_days")!=null){
			this.setNumber_of_days((Integer)map.get("number_of_days"));
		}
		if(map.get("require_payment") instanceof Boolean){
			this.setRequire_payment(((Boolean)map.get("require_payment"))? "true" : "false");
		}
		if(map.get("require_signature") instanceof Boolean){
			this.setRequire_signature(((Boolean)map.get("require_signature"))? "true" : "false");
		}
		if(!(map.get("sale_order_template_line_ids") instanceof Boolean)&& map.get("sale_order_template_line_ids")!=null){
			Object[] objs = (Object[])map.get("sale_order_template_line_ids");
			if(objs.length > 0){
				Integer[] sale_order_template_line_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setSale_order_template_line_ids(Arrays.toString(sale_order_template_line_ids));
			}
		}
		if(!(map.get("sale_order_template_option_ids") instanceof Boolean)&& map.get("sale_order_template_option_ids")!=null){
			Object[] objs = (Object[])map.get("sale_order_template_option_ids");
			if(objs.length > 0){
				Integer[] sale_order_template_option_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setSale_order_template_option_ids(Arrays.toString(sale_order_template_option_ids));
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getActive()!=null&&this.getActiveDirtyFlag()){
			map.put("active",Boolean.parseBoolean(this.getActive()));		
		}		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getMail_template_id()!=null&&this.getMail_template_idDirtyFlag()){
			map.put("mail_template_id",this.getMail_template_id());
		}else if(this.getMail_template_idDirtyFlag()){
			map.put("mail_template_id",false);
		}
		if(this.getMail_template_id_text()!=null&&this.getMail_template_id_textDirtyFlag()){
			//忽略文本外键mail_template_id_text
		}else if(this.getMail_template_id_textDirtyFlag()){
			map.put("mail_template_id",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getNote()!=null&&this.getNoteDirtyFlag()){
			map.put("note",this.getNote());
		}else if(this.getNoteDirtyFlag()){
			map.put("note",false);
		}
		if(this.getNumber_of_days()!=null&&this.getNumber_of_daysDirtyFlag()){
			map.put("number_of_days",this.getNumber_of_days());
		}else if(this.getNumber_of_daysDirtyFlag()){
			map.put("number_of_days",false);
		}
		if(this.getRequire_payment()!=null&&this.getRequire_paymentDirtyFlag()){
			map.put("require_payment",Boolean.parseBoolean(this.getRequire_payment()));		
		}		if(this.getRequire_signature()!=null&&this.getRequire_signatureDirtyFlag()){
			map.put("require_signature",Boolean.parseBoolean(this.getRequire_signature()));		
		}		if(this.getSale_order_template_line_ids()!=null&&this.getSale_order_template_line_idsDirtyFlag()){
			map.put("sale_order_template_line_ids",this.getSale_order_template_line_ids());
		}else if(this.getSale_order_template_line_idsDirtyFlag()){
			map.put("sale_order_template_line_ids",false);
		}
		if(this.getSale_order_template_option_ids()!=null&&this.getSale_order_template_option_idsDirtyFlag()){
			map.put("sale_order_template_option_ids",this.getSale_order_template_option_ids());
		}else if(this.getSale_order_template_option_idsDirtyFlag()){
			map.put("sale_order_template_option_ids",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
