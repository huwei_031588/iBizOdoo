package cn.ibizlab.odoo.client.odoo_sale.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isale_order_line;
import cn.ibizlab.odoo.core.client.service.Isale_order_lineClientService;
import cn.ibizlab.odoo.client.odoo_sale.model.sale_order_lineImpl;
import cn.ibizlab.odoo.client.odoo_sale.odooclient.Isale_order_lineOdooClient;
import cn.ibizlab.odoo.client.odoo_sale.odooclient.impl.sale_order_lineOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[sale_order_line] 服务对象接口
 */
@Service
public class sale_order_lineClientServiceImpl implements Isale_order_lineClientService {
    @Autowired
    private  Isale_order_lineOdooClient  sale_order_lineOdooClient;

    public Isale_order_line createModel() {		
		return new sale_order_lineImpl();
	}


        public Page<Isale_order_line> search(SearchContext context){
            return this.sale_order_lineOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Isale_order_line> sale_order_lines){
            
        }
        
        public void update(Isale_order_line sale_order_line){
this.sale_order_lineOdooClient.update(sale_order_line) ;
        }
        
        public void createBatch(List<Isale_order_line> sale_order_lines){
            
        }
        
        public void remove(Isale_order_line sale_order_line){
this.sale_order_lineOdooClient.remove(sale_order_line) ;
        }
        
        public void get(Isale_order_line sale_order_line){
            this.sale_order_lineOdooClient.get(sale_order_line) ;
        }
        
        public void create(Isale_order_line sale_order_line){
this.sale_order_lineOdooClient.create(sale_order_line) ;
        }
        
        public void updateBatch(List<Isale_order_line> sale_order_lines){
            
        }
        
        public Page<Isale_order_line> select(SearchContext context){
            return null ;
        }
        

}

