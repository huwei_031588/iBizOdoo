package cn.ibizlab.odoo.client.odoo_sale.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isale_order;
import cn.ibizlab.odoo.core.client.service.Isale_orderClientService;
import cn.ibizlab.odoo.client.odoo_sale.model.sale_orderImpl;
import cn.ibizlab.odoo.client.odoo_sale.odooclient.Isale_orderOdooClient;
import cn.ibizlab.odoo.client.odoo_sale.odooclient.impl.sale_orderOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[sale_order] 服务对象接口
 */
@Service
public class sale_orderClientServiceImpl implements Isale_orderClientService {
    @Autowired
    private  Isale_orderOdooClient  sale_orderOdooClient;

    @Autowired
    private cn.ibizlab.odoo.core.client.service.Isale_order_lineClientService sale_order_lineClientService;
    
    public Isale_order createModel() {		
		return new sale_orderImpl();
	}


        public void updateBatch(List<Isale_order> sale_orders){
            
        }
        
        public void update(Isale_order sale_order){
this.sale_orderOdooClient.update(sale_order) ;
        }
        
        public void removeBatch(List<Isale_order> sale_orders){
            
        }
        
        public void create(Isale_order sale_order){
this.sale_orderOdooClient.create(sale_order) ;
        }
        
        public Page<Isale_order> search(SearchContext context){
            return this.sale_orderOdooClient.search(context) ;
        }
        
        public void createBatch(List<Isale_order> sale_orders){
            
        }
        
        public void get(Isale_order sale_order){
            this.sale_orderOdooClient.get(sale_order) ;
            //查询嵌套数据
            SearchContext context = new SearchContext();
            cn.ibizlab.odoo.util.SearchFieldFilter fieldFilter=new cn.ibizlab.odoo.util.SearchFieldFilter();
            fieldFilter.setParam("order_id");
            fieldFilter.setCondition(cn.ibizlab.odoo.util.enums.SearchFieldType.EQ);
            fieldFilter.setValue(sale_order.getId());
            context.getCondition().add(fieldFilter);
            Pageable pageable = PageRequest.of(0, 1000);
            context.setPageable(pageable);
            sale_order.setSale_order_lines(sale_order_lineClientService.search(context).getContent());
        }
        
        public void remove(Isale_order sale_order){
this.sale_orderOdooClient.remove(sale_order) ;
        }
        
        public Page<Isale_order> select(SearchContext context){
            return null ;
        }
        

}

