package cn.ibizlab.odoo.client.odoo_sale.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Isale_order;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[sale_order] 服务对象客户端接口
 */
public interface Isale_orderOdooClient {
    
        public void updateBatch(Isale_order sale_order);

        public void update(Isale_order sale_order);

        public void removeBatch(Isale_order sale_order);

        public void create(Isale_order sale_order);

        public Page<Isale_order> search(SearchContext context);

        public void createBatch(Isale_order sale_order);

        public void get(Isale_order sale_order);

        public void remove(Isale_order sale_order);

        public List<Isale_order> select();


}