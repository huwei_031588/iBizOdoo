package cn.ibizlab.odoo.client.odoo_sale.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isale_order_template;
import cn.ibizlab.odoo.core.client.service.Isale_order_templateClientService;
import cn.ibizlab.odoo.client.odoo_sale.model.sale_order_templateImpl;
import cn.ibizlab.odoo.client.odoo_sale.odooclient.Isale_order_templateOdooClient;
import cn.ibizlab.odoo.client.odoo_sale.odooclient.impl.sale_order_templateOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[sale_order_template] 服务对象接口
 */
@Service
public class sale_order_templateClientServiceImpl implements Isale_order_templateClientService {
    @Autowired
    private  Isale_order_templateOdooClient  sale_order_templateOdooClient;

    public Isale_order_template createModel() {		
		return new sale_order_templateImpl();
	}


        public Page<Isale_order_template> search(SearchContext context){
            return this.sale_order_templateOdooClient.search(context) ;
        }
        
        public void create(Isale_order_template sale_order_template){
this.sale_order_templateOdooClient.create(sale_order_template) ;
        }
        
        public void remove(Isale_order_template sale_order_template){
this.sale_order_templateOdooClient.remove(sale_order_template) ;
        }
        
        public void updateBatch(List<Isale_order_template> sale_order_templates){
            
        }
        
        public void update(Isale_order_template sale_order_template){
this.sale_order_templateOdooClient.update(sale_order_template) ;
        }
        
        public void get(Isale_order_template sale_order_template){
            this.sale_order_templateOdooClient.get(sale_order_template) ;
        }
        
        public void removeBatch(List<Isale_order_template> sale_order_templates){
            
        }
        
        public void createBatch(List<Isale_order_template> sale_order_templates){
            
        }
        
        public Page<Isale_order_template> select(SearchContext context){
            return null ;
        }
        

}

