package cn.ibizlab.odoo.client.odoo_sale.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isale_report;
import cn.ibizlab.odoo.core.client.service.Isale_reportClientService;
import cn.ibizlab.odoo.client.odoo_sale.model.sale_reportImpl;
import cn.ibizlab.odoo.client.odoo_sale.odooclient.Isale_reportOdooClient;
import cn.ibizlab.odoo.client.odoo_sale.odooclient.impl.sale_reportOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[sale_report] 服务对象接口
 */
@Service
public class sale_reportClientServiceImpl implements Isale_reportClientService {
    @Autowired
    private  Isale_reportOdooClient  sale_reportOdooClient;

    public Isale_report createModel() {		
		return new sale_reportImpl();
	}


        public Page<Isale_report> search(SearchContext context){
            return this.sale_reportOdooClient.search(context) ;
        }
        
        public void get(Isale_report sale_report){
            this.sale_reportOdooClient.get(sale_report) ;
        }
        
        public void removeBatch(List<Isale_report> sale_reports){
            
        }
        
        public void update(Isale_report sale_report){
this.sale_reportOdooClient.update(sale_report) ;
        }
        
        public void remove(Isale_report sale_report){
this.sale_reportOdooClient.remove(sale_report) ;
        }
        
        public void create(Isale_report sale_report){
this.sale_reportOdooClient.create(sale_report) ;
        }
        
        public void createBatch(List<Isale_report> sale_reports){
            
        }
        
        public void updateBatch(List<Isale_report> sale_reports){
            
        }
        
        public Page<Isale_report> select(SearchContext context){
            return null ;
        }
        

}

