package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_module_update;
import cn.ibizlab.odoo.core.client.service.Ibase_module_updateClientService;
import cn.ibizlab.odoo.client.odoo_base.model.base_module_updateImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ibase_module_updateOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.base_module_updateOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_module_update] 服务对象接口
 */
@Service
public class base_module_updateClientServiceImpl implements Ibase_module_updateClientService {
    @Autowired
    private  Ibase_module_updateOdooClient  base_module_updateOdooClient;

    public Ibase_module_update createModel() {		
		return new base_module_updateImpl();
	}


        public void updateBatch(List<Ibase_module_update> base_module_updates){
            
        }
        
        public void update(Ibase_module_update base_module_update){
this.base_module_updateOdooClient.update(base_module_update) ;
        }
        
        public void createBatch(List<Ibase_module_update> base_module_updates){
            
        }
        
        public void create(Ibase_module_update base_module_update){
this.base_module_updateOdooClient.create(base_module_update) ;
        }
        
        public void remove(Ibase_module_update base_module_update){
this.base_module_updateOdooClient.remove(base_module_update) ;
        }
        
        public Page<Ibase_module_update> search(SearchContext context){
            return this.base_module_updateOdooClient.search(context) ;
        }
        
        public void get(Ibase_module_update base_module_update){
            this.base_module_updateOdooClient.get(base_module_update) ;
        }
        
        public void removeBatch(List<Ibase_module_update> base_module_updates){
            
        }
        
        public Page<Ibase_module_update> select(SearchContext context){
            return null ;
        }
        

}

