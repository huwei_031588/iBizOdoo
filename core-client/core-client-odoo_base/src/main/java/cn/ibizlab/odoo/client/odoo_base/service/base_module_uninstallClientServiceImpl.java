package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_module_uninstall;
import cn.ibizlab.odoo.core.client.service.Ibase_module_uninstallClientService;
import cn.ibizlab.odoo.client.odoo_base.model.base_module_uninstallImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ibase_module_uninstallOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.base_module_uninstallOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_module_uninstall] 服务对象接口
 */
@Service
public class base_module_uninstallClientServiceImpl implements Ibase_module_uninstallClientService {
    @Autowired
    private  Ibase_module_uninstallOdooClient  base_module_uninstallOdooClient;

    public Ibase_module_uninstall createModel() {		
		return new base_module_uninstallImpl();
	}


        public void update(Ibase_module_uninstall base_module_uninstall){
this.base_module_uninstallOdooClient.update(base_module_uninstall) ;
        }
        
        public void updateBatch(List<Ibase_module_uninstall> base_module_uninstalls){
            
        }
        
        public void createBatch(List<Ibase_module_uninstall> base_module_uninstalls){
            
        }
        
        public void get(Ibase_module_uninstall base_module_uninstall){
            this.base_module_uninstallOdooClient.get(base_module_uninstall) ;
        }
        
        public void removeBatch(List<Ibase_module_uninstall> base_module_uninstalls){
            
        }
        
        public Page<Ibase_module_uninstall> search(SearchContext context){
            return this.base_module_uninstallOdooClient.search(context) ;
        }
        
        public void remove(Ibase_module_uninstall base_module_uninstall){
this.base_module_uninstallOdooClient.remove(base_module_uninstall) ;
        }
        
        public void create(Ibase_module_uninstall base_module_uninstall){
this.base_module_uninstallOdooClient.create(base_module_uninstall) ;
        }
        
        public Page<Ibase_module_uninstall> select(SearchContext context){
            return null ;
        }
        

}

