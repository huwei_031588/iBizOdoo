package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_users;
import cn.ibizlab.odoo.core.client.service.Ires_usersClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_usersImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ires_usersOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.res_usersOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[res_users] 服务对象接口
 */
@Service
public class res_usersClientServiceImpl implements Ires_usersClientService {
    @Autowired
    private  Ires_usersOdooClient  res_usersOdooClient;

    public Ires_users createModel() {		
		return new res_usersImpl();
	}


        public void get(Ires_users res_users){
            this.res_usersOdooClient.get(res_users) ;
        }
        
        public void removeBatch(List<Ires_users> res_users){
            
        }
        
        public void remove(Ires_users res_users){
this.res_usersOdooClient.remove(res_users) ;
        }
        
        public void create(Ires_users res_users){
this.res_usersOdooClient.create(res_users) ;
        }
        
        public void updateBatch(List<Ires_users> res_users){
            
        }
        
        public void createBatch(List<Ires_users> res_users){
            
        }
        
        public void update(Ires_users res_users){
this.res_usersOdooClient.update(res_users) ;
        }
        
        public Page<Ires_users> search(SearchContext context){
            return this.res_usersOdooClient.search(context) ;
        }
        
        public Page<Ires_users> select(SearchContext context){
            return null ;
        }
        

}

