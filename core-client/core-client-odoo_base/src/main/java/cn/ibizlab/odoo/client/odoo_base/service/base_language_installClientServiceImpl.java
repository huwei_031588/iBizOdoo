package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_language_install;
import cn.ibizlab.odoo.core.client.service.Ibase_language_installClientService;
import cn.ibizlab.odoo.client.odoo_base.model.base_language_installImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ibase_language_installOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.base_language_installOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_language_install] 服务对象接口
 */
@Service
public class base_language_installClientServiceImpl implements Ibase_language_installClientService {
    @Autowired
    private  Ibase_language_installOdooClient  base_language_installOdooClient;

    public Ibase_language_install createModel() {		
		return new base_language_installImpl();
	}


        public void updateBatch(List<Ibase_language_install> base_language_installs){
            
        }
        
        public void update(Ibase_language_install base_language_install){
this.base_language_installOdooClient.update(base_language_install) ;
        }
        
        public void removeBatch(List<Ibase_language_install> base_language_installs){
            
        }
        
        public Page<Ibase_language_install> search(SearchContext context){
            return this.base_language_installOdooClient.search(context) ;
        }
        
        public void createBatch(List<Ibase_language_install> base_language_installs){
            
        }
        
        public void create(Ibase_language_install base_language_install){
this.base_language_installOdooClient.create(base_language_install) ;
        }
        
        public void remove(Ibase_language_install base_language_install){
this.base_language_installOdooClient.remove(base_language_install) ;
        }
        
        public void get(Ibase_language_install base_language_install){
            this.base_language_installOdooClient.get(base_language_install) ;
        }
        
        public Page<Ibase_language_install> select(SearchContext context){
            return null ;
        }
        

}

