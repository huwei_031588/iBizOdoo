package cn.ibizlab.odoo.client.odoo_base.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ires_country;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_country] 服务对象客户端接口
 */
public interface Ires_countryOdooClient {
    
        public void update(Ires_country res_country);

        public void updateBatch(Ires_country res_country);

        public void createBatch(Ires_country res_country);

        public void create(Ires_country res_country);

        public void removeBatch(Ires_country res_country);

        public void remove(Ires_country res_country);

        public Page<Ires_country> search(SearchContext context);

        public void get(Ires_country res_country);

        public List<Ires_country> select();


}