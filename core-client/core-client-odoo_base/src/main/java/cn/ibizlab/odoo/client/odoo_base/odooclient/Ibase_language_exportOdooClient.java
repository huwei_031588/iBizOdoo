package cn.ibizlab.odoo.client.odoo_base.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ibase_language_export;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_language_export] 服务对象客户端接口
 */
public interface Ibase_language_exportOdooClient {
    
        public void create(Ibase_language_export base_language_export);

        public void removeBatch(Ibase_language_export base_language_export);

        public void createBatch(Ibase_language_export base_language_export);

        public void get(Ibase_language_export base_language_export);

        public void update(Ibase_language_export base_language_export);

        public void remove(Ibase_language_export base_language_export);

        public Page<Ibase_language_export> search(SearchContext context);

        public void updateBatch(Ibase_language_export base_language_export);

        public List<Ibase_language_export> select();


}