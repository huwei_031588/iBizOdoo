package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_currency_rate;
import cn.ibizlab.odoo.core.client.service.Ires_currency_rateClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_currency_rateImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ires_currency_rateOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.res_currency_rateOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[res_currency_rate] 服务对象接口
 */
@Service
public class res_currency_rateClientServiceImpl implements Ires_currency_rateClientService {
    @Autowired
    private  Ires_currency_rateOdooClient  res_currency_rateOdooClient;

    public Ires_currency_rate createModel() {		
		return new res_currency_rateImpl();
	}


        public void get(Ires_currency_rate res_currency_rate){
            this.res_currency_rateOdooClient.get(res_currency_rate) ;
        }
        
        public void createBatch(List<Ires_currency_rate> res_currency_rates){
            
        }
        
        public void create(Ires_currency_rate res_currency_rate){
this.res_currency_rateOdooClient.create(res_currency_rate) ;
        }
        
        public void update(Ires_currency_rate res_currency_rate){
this.res_currency_rateOdooClient.update(res_currency_rate) ;
        }
        
        public Page<Ires_currency_rate> search(SearchContext context){
            return this.res_currency_rateOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Ires_currency_rate> res_currency_rates){
            
        }
        
        public void remove(Ires_currency_rate res_currency_rate){
this.res_currency_rateOdooClient.remove(res_currency_rate) ;
        }
        
        public void updateBatch(List<Ires_currency_rate> res_currency_rates){
            
        }
        
        public Page<Ires_currency_rate> select(SearchContext context){
            return null ;
        }
        

}

