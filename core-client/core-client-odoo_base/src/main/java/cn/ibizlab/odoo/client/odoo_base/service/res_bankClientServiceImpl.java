package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_bank;
import cn.ibizlab.odoo.core.client.service.Ires_bankClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_bankImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ires_bankOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.res_bankOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[res_bank] 服务对象接口
 */
@Service
public class res_bankClientServiceImpl implements Ires_bankClientService {
    @Autowired
    private  Ires_bankOdooClient  res_bankOdooClient;

    public Ires_bank createModel() {		
		return new res_bankImpl();
	}


        public void remove(Ires_bank res_bank){
this.res_bankOdooClient.remove(res_bank) ;
        }
        
        public void createBatch(List<Ires_bank> res_banks){
            
        }
        
        public Page<Ires_bank> search(SearchContext context){
            return this.res_bankOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Ires_bank> res_banks){
            
        }
        
        public void update(Ires_bank res_bank){
this.res_bankOdooClient.update(res_bank) ;
        }
        
        public void updateBatch(List<Ires_bank> res_banks){
            
        }
        
        public void get(Ires_bank res_bank){
            this.res_bankOdooClient.get(res_bank) ;
        }
        
        public void create(Ires_bank res_bank){
this.res_bankOdooClient.create(res_bank) ;
        }
        
        public Page<Ires_bank> select(SearchContext context){
            return null ;
        }
        

}

