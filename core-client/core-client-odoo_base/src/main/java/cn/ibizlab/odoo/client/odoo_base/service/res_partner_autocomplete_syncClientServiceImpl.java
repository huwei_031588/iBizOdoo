package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_partner_autocomplete_sync;
import cn.ibizlab.odoo.core.client.service.Ires_partner_autocomplete_syncClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_partner_autocomplete_syncImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ires_partner_autocomplete_syncOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.res_partner_autocomplete_syncOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[res_partner_autocomplete_sync] 服务对象接口
 */
@Service
public class res_partner_autocomplete_syncClientServiceImpl implements Ires_partner_autocomplete_syncClientService {
    @Autowired
    private  Ires_partner_autocomplete_syncOdooClient  res_partner_autocomplete_syncOdooClient;

    public Ires_partner_autocomplete_sync createModel() {		
		return new res_partner_autocomplete_syncImpl();
	}


        public void update(Ires_partner_autocomplete_sync res_partner_autocomplete_sync){
this.res_partner_autocomplete_syncOdooClient.update(res_partner_autocomplete_sync) ;
        }
        
        public void updateBatch(List<Ires_partner_autocomplete_sync> res_partner_autocomplete_syncs){
            
        }
        
        public void get(Ires_partner_autocomplete_sync res_partner_autocomplete_sync){
            this.res_partner_autocomplete_syncOdooClient.get(res_partner_autocomplete_sync) ;
        }
        
        public Page<Ires_partner_autocomplete_sync> search(SearchContext context){
            return this.res_partner_autocomplete_syncOdooClient.search(context) ;
        }
        
        public void create(Ires_partner_autocomplete_sync res_partner_autocomplete_sync){
this.res_partner_autocomplete_syncOdooClient.create(res_partner_autocomplete_sync) ;
        }
        
        public void createBatch(List<Ires_partner_autocomplete_sync> res_partner_autocomplete_syncs){
            
        }
        
        public void remove(Ires_partner_autocomplete_sync res_partner_autocomplete_sync){
this.res_partner_autocomplete_syncOdooClient.remove(res_partner_autocomplete_sync) ;
        }
        
        public void removeBatch(List<Ires_partner_autocomplete_sync> res_partner_autocomplete_syncs){
            
        }
        
        public Page<Ires_partner_autocomplete_sync> select(SearchContext context){
            return null ;
        }
        

}

