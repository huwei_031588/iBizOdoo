package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_module_upgrade;
import cn.ibizlab.odoo.client.odoo_base.model.base_module_upgradeImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_module_upgrade] 服务对象接口
 */
public interface base_module_upgradeFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_module_upgrades/{id}")
    public base_module_upgradeImpl update(@PathVariable("id") Integer id,@RequestBody base_module_upgradeImpl base_module_upgrade);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_module_upgrades/createbatch")
    public base_module_upgradeImpl createBatch(@RequestBody List<base_module_upgradeImpl> base_module_upgrades);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_module_upgrades/search")
    public Page<base_module_upgradeImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_module_upgrades/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_module_upgrades/{id}")
    public base_module_upgradeImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_module_upgrades/removebatch")
    public base_module_upgradeImpl removeBatch(@RequestBody List<base_module_upgradeImpl> base_module_upgrades);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_module_upgrades")
    public base_module_upgradeImpl create(@RequestBody base_module_upgradeImpl base_module_upgrade);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_module_upgrades/updatebatch")
    public base_module_upgradeImpl updateBatch(@RequestBody List<base_module_upgradeImpl> base_module_upgrades);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_module_upgrades/select")
    public Page<base_module_upgradeImpl> select();



}
