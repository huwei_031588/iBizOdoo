package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_automation_line_test;
import cn.ibizlab.odoo.core.client.service.Ibase_automation_line_testClientService;
import cn.ibizlab.odoo.client.odoo_base.model.base_automation_line_testImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ibase_automation_line_testOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.base_automation_line_testOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_automation_line_test] 服务对象接口
 */
@Service
public class base_automation_line_testClientServiceImpl implements Ibase_automation_line_testClientService {
    @Autowired
    private  Ibase_automation_line_testOdooClient  base_automation_line_testOdooClient;

    public Ibase_automation_line_test createModel() {		
		return new base_automation_line_testImpl();
	}


        public void removeBatch(List<Ibase_automation_line_test> base_automation_line_tests){
            
        }
        
        public void updateBatch(List<Ibase_automation_line_test> base_automation_line_tests){
            
        }
        
        public void remove(Ibase_automation_line_test base_automation_line_test){
this.base_automation_line_testOdooClient.remove(base_automation_line_test) ;
        }
        
        public void createBatch(List<Ibase_automation_line_test> base_automation_line_tests){
            
        }
        
        public void get(Ibase_automation_line_test base_automation_line_test){
            this.base_automation_line_testOdooClient.get(base_automation_line_test) ;
        }
        
        public void create(Ibase_automation_line_test base_automation_line_test){
this.base_automation_line_testOdooClient.create(base_automation_line_test) ;
        }
        
        public void update(Ibase_automation_line_test base_automation_line_test){
this.base_automation_line_testOdooClient.update(base_automation_line_test) ;
        }
        
        public Page<Ibase_automation_line_test> search(SearchContext context){
            return this.base_automation_line_testOdooClient.search(context) ;
        }
        
        public Page<Ibase_automation_line_test> select(SearchContext context){
            return null ;
        }
        

}

