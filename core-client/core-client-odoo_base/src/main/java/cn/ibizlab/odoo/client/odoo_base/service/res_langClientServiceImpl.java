package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_lang;
import cn.ibizlab.odoo.core.client.service.Ires_langClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_langImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ires_langOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.res_langOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[res_lang] 服务对象接口
 */
@Service
public class res_langClientServiceImpl implements Ires_langClientService {
    @Autowired
    private  Ires_langOdooClient  res_langOdooClient;

    public Ires_lang createModel() {		
		return new res_langImpl();
	}


        public void get(Ires_lang res_lang){
            this.res_langOdooClient.get(res_lang) ;
        }
        
        public void remove(Ires_lang res_lang){
this.res_langOdooClient.remove(res_lang) ;
        }
        
        public void update(Ires_lang res_lang){
this.res_langOdooClient.update(res_lang) ;
        }
        
        public void updateBatch(List<Ires_lang> res_langs){
            
        }
        
        public void removeBatch(List<Ires_lang> res_langs){
            
        }
        
        public void create(Ires_lang res_lang){
this.res_langOdooClient.create(res_lang) ;
        }
        
        public void createBatch(List<Ires_lang> res_langs){
            
        }
        
        public Page<Ires_lang> search(SearchContext context){
            return this.res_langOdooClient.search(context) ;
        }
        
        public Page<Ires_lang> select(SearchContext context){
            return null ;
        }
        

}

