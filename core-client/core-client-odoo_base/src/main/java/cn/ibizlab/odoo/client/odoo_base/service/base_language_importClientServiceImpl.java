package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_language_import;
import cn.ibizlab.odoo.core.client.service.Ibase_language_importClientService;
import cn.ibizlab.odoo.client.odoo_base.model.base_language_importImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ibase_language_importOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.base_language_importOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_language_import] 服务对象接口
 */
@Service
public class base_language_importClientServiceImpl implements Ibase_language_importClientService {
    @Autowired
    private  Ibase_language_importOdooClient  base_language_importOdooClient;

    public Ibase_language_import createModel() {		
		return new base_language_importImpl();
	}


        public Page<Ibase_language_import> search(SearchContext context){
            return this.base_language_importOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Ibase_language_import> base_language_imports){
            
        }
        
        public void create(Ibase_language_import base_language_import){
this.base_language_importOdooClient.create(base_language_import) ;
        }
        
        public void update(Ibase_language_import base_language_import){
this.base_language_importOdooClient.update(base_language_import) ;
        }
        
        public void remove(Ibase_language_import base_language_import){
this.base_language_importOdooClient.remove(base_language_import) ;
        }
        
        public void get(Ibase_language_import base_language_import){
            this.base_language_importOdooClient.get(base_language_import) ;
        }
        
        public void createBatch(List<Ibase_language_import> base_language_imports){
            
        }
        
        public void updateBatch(List<Ibase_language_import> base_language_imports){
            
        }
        
        public Page<Ibase_language_import> select(SearchContext context){
            return null ;
        }
        

}

