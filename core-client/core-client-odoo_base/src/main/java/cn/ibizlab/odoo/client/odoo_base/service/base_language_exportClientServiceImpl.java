package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_language_export;
import cn.ibizlab.odoo.core.client.service.Ibase_language_exportClientService;
import cn.ibizlab.odoo.client.odoo_base.model.base_language_exportImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ibase_language_exportOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.base_language_exportOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_language_export] 服务对象接口
 */
@Service
public class base_language_exportClientServiceImpl implements Ibase_language_exportClientService {
    @Autowired
    private  Ibase_language_exportOdooClient  base_language_exportOdooClient;

    public Ibase_language_export createModel() {		
		return new base_language_exportImpl();
	}


        public void create(Ibase_language_export base_language_export){
this.base_language_exportOdooClient.create(base_language_export) ;
        }
        
        public void removeBatch(List<Ibase_language_export> base_language_exports){
            
        }
        
        public void createBatch(List<Ibase_language_export> base_language_exports){
            
        }
        
        public void get(Ibase_language_export base_language_export){
            this.base_language_exportOdooClient.get(base_language_export) ;
        }
        
        public void update(Ibase_language_export base_language_export){
this.base_language_exportOdooClient.update(base_language_export) ;
        }
        
        public void remove(Ibase_language_export base_language_export){
this.base_language_exportOdooClient.remove(base_language_export) ;
        }
        
        public Page<Ibase_language_export> search(SearchContext context){
            return this.base_language_exportOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Ibase_language_export> base_language_exports){
            
        }
        
        public Page<Ibase_language_export> select(SearchContext context){
            return null ;
        }
        

}

