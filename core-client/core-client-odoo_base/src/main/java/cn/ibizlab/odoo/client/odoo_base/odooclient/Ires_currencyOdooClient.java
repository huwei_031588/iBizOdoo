package cn.ibizlab.odoo.client.odoo_base.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ires_currency;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_currency] 服务对象客户端接口
 */
public interface Ires_currencyOdooClient {
    
        public void get(Ires_currency res_currency);

        public void updateBatch(Ires_currency res_currency);

        public Page<Ires_currency> search(SearchContext context);

        public void remove(Ires_currency res_currency);

        public void update(Ires_currency res_currency);

        public void create(Ires_currency res_currency);

        public void removeBatch(Ires_currency res_currency);

        public void createBatch(Ires_currency res_currency);

        public List<Ires_currency> select();


}