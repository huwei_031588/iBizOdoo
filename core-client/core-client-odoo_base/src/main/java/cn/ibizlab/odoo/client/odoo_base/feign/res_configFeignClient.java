package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ires_config;
import cn.ibizlab.odoo.client.odoo_base.model.res_configImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[res_config] 服务对象接口
 */
public interface res_configFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_configs/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_configs")
    public res_configImpl create(@RequestBody res_configImpl res_config);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_configs/updatebatch")
    public res_configImpl updateBatch(@RequestBody List<res_configImpl> res_configs);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_configs/{id}")
    public res_configImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_configs/{id}")
    public res_configImpl update(@PathVariable("id") Integer id,@RequestBody res_configImpl res_config);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_configs/createbatch")
    public res_configImpl createBatch(@RequestBody List<res_configImpl> res_configs);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_configs/search")
    public Page<res_configImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_configs/removebatch")
    public res_configImpl removeBatch(@RequestBody List<res_configImpl> res_configs);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_configs/select")
    public Page<res_configImpl> select();



}
