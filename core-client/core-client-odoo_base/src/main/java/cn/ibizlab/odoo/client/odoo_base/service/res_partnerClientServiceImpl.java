package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_partner;
import cn.ibizlab.odoo.core.client.service.Ires_partnerClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_partnerImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ires_partnerOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.res_partnerOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[res_partner] 服务对象接口
 */
@Service
public class res_partnerClientServiceImpl implements Ires_partnerClientService {
    @Autowired
    private  Ires_partnerOdooClient  res_partnerOdooClient;

    public Ires_partner createModel() {		
		return new res_partnerImpl();
	}


        public void get(Ires_partner res_partner){
            this.res_partnerOdooClient.get(res_partner) ;
        }
        
        public void update(Ires_partner res_partner){
this.res_partnerOdooClient.update(res_partner) ;
        }
        
        public Page<Ires_partner> search(SearchContext context){
            return this.res_partnerOdooClient.search(context) ;
        }
        
        public void create(Ires_partner res_partner){
this.res_partnerOdooClient.create(res_partner) ;
        }
        
        public void createBatch(List<Ires_partner> res_partners){
            
        }
        
        public void removeBatch(List<Ires_partner> res_partners){
            
        }
        
        public void updateBatch(List<Ires_partner> res_partners){
            
        }
        
        public void remove(Ires_partner res_partner){
this.res_partnerOdooClient.remove(res_partner) ;
        }
        
        public Page<Ires_partner> select(SearchContext context){
            return null ;
        }
        

}

