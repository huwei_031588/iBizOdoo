package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_country;
import cn.ibizlab.odoo.core.client.service.Ires_countryClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_countryImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ires_countryOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.res_countryOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[res_country] 服务对象接口
 */
@Service
public class res_countryClientServiceImpl implements Ires_countryClientService {
    @Autowired
    private  Ires_countryOdooClient  res_countryOdooClient;

    public Ires_country createModel() {		
		return new res_countryImpl();
	}


        public void update(Ires_country res_country){
this.res_countryOdooClient.update(res_country) ;
        }
        
        public void updateBatch(List<Ires_country> res_countries){
            
        }
        
        public void createBatch(List<Ires_country> res_countries){
            
        }
        
        public void create(Ires_country res_country){
this.res_countryOdooClient.create(res_country) ;
        }
        
        public void removeBatch(List<Ires_country> res_countries){
            
        }
        
        public void remove(Ires_country res_country){
this.res_countryOdooClient.remove(res_country) ;
        }
        
        public Page<Ires_country> search(SearchContext context){
            return this.res_countryOdooClient.search(context) ;
        }
        
        public void get(Ires_country res_country){
            this.res_countryOdooClient.get(res_country) ;
        }
        
        public Page<Ires_country> select(SearchContext context){
            return null ;
        }
        

}

