package cn.ibizlab.odoo.client.odoo_base.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.base")
@Data
public class odoo_baseClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
