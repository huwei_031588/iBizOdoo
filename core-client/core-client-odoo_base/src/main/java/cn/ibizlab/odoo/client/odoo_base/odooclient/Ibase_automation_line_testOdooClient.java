package cn.ibizlab.odoo.client.odoo_base.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ibase_automation_line_test;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_automation_line_test] 服务对象客户端接口
 */
public interface Ibase_automation_line_testOdooClient {
    
        public void removeBatch(Ibase_automation_line_test base_automation_line_test);

        public void updateBatch(Ibase_automation_line_test base_automation_line_test);

        public void remove(Ibase_automation_line_test base_automation_line_test);

        public void createBatch(Ibase_automation_line_test base_automation_line_test);

        public void get(Ibase_automation_line_test base_automation_line_test);

        public void create(Ibase_automation_line_test base_automation_line_test);

        public void update(Ibase_automation_line_test base_automation_line_test);

        public Page<Ibase_automation_line_test> search(SearchContext context);

        public List<Ibase_automation_line_test> select();


}