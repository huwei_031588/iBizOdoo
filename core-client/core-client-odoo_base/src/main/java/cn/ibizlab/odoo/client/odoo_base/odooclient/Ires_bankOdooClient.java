package cn.ibizlab.odoo.client.odoo_base.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ires_bank;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_bank] 服务对象客户端接口
 */
public interface Ires_bankOdooClient {
    
        public void remove(Ires_bank res_bank);

        public void createBatch(Ires_bank res_bank);

        public Page<Ires_bank> search(SearchContext context);

        public void removeBatch(Ires_bank res_bank);

        public void update(Ires_bank res_bank);

        public void updateBatch(Ires_bank res_bank);

        public void get(Ires_bank res_bank);

        public void create(Ires_bank res_bank);

        public List<Ires_bank> select();


}