package cn.ibizlab.odoo.client.odoo_base.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ibase_partner_merge_automatic_wizard;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_partner_merge_automatic_wizard] 服务对象客户端接口
 */
public interface Ibase_partner_merge_automatic_wizardOdooClient {
    
        public void remove(Ibase_partner_merge_automatic_wizard base_partner_merge_automatic_wizard);

        public Page<Ibase_partner_merge_automatic_wizard> search(SearchContext context);

        public void update(Ibase_partner_merge_automatic_wizard base_partner_merge_automatic_wizard);

        public void createBatch(Ibase_partner_merge_automatic_wizard base_partner_merge_automatic_wizard);

        public void updateBatch(Ibase_partner_merge_automatic_wizard base_partner_merge_automatic_wizard);

        public void get(Ibase_partner_merge_automatic_wizard base_partner_merge_automatic_wizard);

        public void create(Ibase_partner_merge_automatic_wizard base_partner_merge_automatic_wizard);

        public void removeBatch(Ibase_partner_merge_automatic_wizard base_partner_merge_automatic_wizard);

        public List<Ibase_partner_merge_automatic_wizard> select();


}