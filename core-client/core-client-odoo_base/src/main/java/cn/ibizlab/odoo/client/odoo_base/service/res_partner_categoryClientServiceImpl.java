package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_partner_category;
import cn.ibizlab.odoo.core.client.service.Ires_partner_categoryClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_partner_categoryImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ires_partner_categoryOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.res_partner_categoryOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[res_partner_category] 服务对象接口
 */
@Service
public class res_partner_categoryClientServiceImpl implements Ires_partner_categoryClientService {
    @Autowired
    private  Ires_partner_categoryOdooClient  res_partner_categoryOdooClient;

    public Ires_partner_category createModel() {		
		return new res_partner_categoryImpl();
	}


        public void get(Ires_partner_category res_partner_category){
            this.res_partner_categoryOdooClient.get(res_partner_category) ;
        }
        
        public void createBatch(List<Ires_partner_category> res_partner_categories){
            
        }
        
        public void updateBatch(List<Ires_partner_category> res_partner_categories){
            
        }
        
        public void update(Ires_partner_category res_partner_category){
this.res_partner_categoryOdooClient.update(res_partner_category) ;
        }
        
        public void removeBatch(List<Ires_partner_category> res_partner_categories){
            
        }
        
        public Page<Ires_partner_category> search(SearchContext context){
            return this.res_partner_categoryOdooClient.search(context) ;
        }
        
        public void create(Ires_partner_category res_partner_category){
this.res_partner_categoryOdooClient.create(res_partner_category) ;
        }
        
        public void remove(Ires_partner_category res_partner_category){
this.res_partner_categoryOdooClient.remove(res_partner_category) ;
        }
        
        public Page<Ires_partner_category> select(SearchContext context){
            return null ;
        }
        

}

