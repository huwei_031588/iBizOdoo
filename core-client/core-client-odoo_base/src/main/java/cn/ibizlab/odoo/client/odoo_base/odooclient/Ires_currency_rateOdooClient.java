package cn.ibizlab.odoo.client.odoo_base.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ires_currency_rate;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_currency_rate] 服务对象客户端接口
 */
public interface Ires_currency_rateOdooClient {
    
        public void get(Ires_currency_rate res_currency_rate);

        public void createBatch(Ires_currency_rate res_currency_rate);

        public void create(Ires_currency_rate res_currency_rate);

        public void update(Ires_currency_rate res_currency_rate);

        public Page<Ires_currency_rate> search(SearchContext context);

        public void removeBatch(Ires_currency_rate res_currency_rate);

        public void remove(Ires_currency_rate res_currency_rate);

        public void updateBatch(Ires_currency_rate res_currency_rate);

        public List<Ires_currency_rate> select();


}