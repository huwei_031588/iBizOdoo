package cn.ibizlab.odoo.client.odoo_base.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ires_config;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_config] 服务对象客户端接口
 */
public interface Ires_configOdooClient {
    
        public void remove(Ires_config res_config);

        public void create(Ires_config res_config);

        public void updateBatch(Ires_config res_config);

        public void get(Ires_config res_config);

        public void update(Ires_config res_config);

        public void createBatch(Ires_config res_config);

        public Page<Ires_config> search(SearchContext context);

        public void removeBatch(Ires_config res_config);

        public List<Ires_config> select();


}