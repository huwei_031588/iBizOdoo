package cn.ibizlab.odoo.client.odoo_base.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ibase_module_upgrade;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_module_upgrade] 服务对象客户端接口
 */
public interface Ibase_module_upgradeOdooClient {
    
        public void update(Ibase_module_upgrade base_module_upgrade);

        public void createBatch(Ibase_module_upgrade base_module_upgrade);

        public Page<Ibase_module_upgrade> search(SearchContext context);

        public void remove(Ibase_module_upgrade base_module_upgrade);

        public void get(Ibase_module_upgrade base_module_upgrade);

        public void removeBatch(Ibase_module_upgrade base_module_upgrade);

        public void create(Ibase_module_upgrade base_module_upgrade);

        public void updateBatch(Ibase_module_upgrade base_module_upgrade);

        public List<Ibase_module_upgrade> select();


}