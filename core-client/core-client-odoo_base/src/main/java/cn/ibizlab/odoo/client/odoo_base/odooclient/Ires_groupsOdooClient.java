package cn.ibizlab.odoo.client.odoo_base.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ires_groups;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_groups] 服务对象客户端接口
 */
public interface Ires_groupsOdooClient {
    
        public void removeBatch(Ires_groups res_groups);

        public void remove(Ires_groups res_groups);

        public void create(Ires_groups res_groups);

        public void update(Ires_groups res_groups);

        public void createBatch(Ires_groups res_groups);

        public void get(Ires_groups res_groups);

        public Page<Ires_groups> search(SearchContext context);

        public void updateBatch(Ires_groups res_groups);

        public List<Ires_groups> select();


}