package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_update_translations;
import cn.ibizlab.odoo.core.client.service.Ibase_update_translationsClientService;
import cn.ibizlab.odoo.client.odoo_base.model.base_update_translationsImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ibase_update_translationsOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.base_update_translationsOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_update_translations] 服务对象接口
 */
@Service
public class base_update_translationsClientServiceImpl implements Ibase_update_translationsClientService {
    @Autowired
    private  Ibase_update_translationsOdooClient  base_update_translationsOdooClient;

    public Ibase_update_translations createModel() {		
		return new base_update_translationsImpl();
	}


        public void remove(Ibase_update_translations base_update_translations){
this.base_update_translationsOdooClient.remove(base_update_translations) ;
        }
        
        public Page<Ibase_update_translations> search(SearchContext context){
            return this.base_update_translationsOdooClient.search(context) ;
        }
        
        public void update(Ibase_update_translations base_update_translations){
this.base_update_translationsOdooClient.update(base_update_translations) ;
        }
        
        public void create(Ibase_update_translations base_update_translations){
this.base_update_translationsOdooClient.create(base_update_translations) ;
        }
        
        public void updateBatch(List<Ibase_update_translations> base_update_translations){
            
        }
        
        public void createBatch(List<Ibase_update_translations> base_update_translations){
            
        }
        
        public void removeBatch(List<Ibase_update_translations> base_update_translations){
            
        }
        
        public void get(Ibase_update_translations base_update_translations){
            this.base_update_translationsOdooClient.get(base_update_translations) ;
        }
        
        public Page<Ibase_update_translations> select(SearchContext context){
            return null ;
        }
        

}

