package cn.ibizlab.odoo.client.odoo_base.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ires_users_log;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_users_log] 服务对象客户端接口
 */
public interface Ires_users_logOdooClient {
    
        public Page<Ires_users_log> search(SearchContext context);

        public void removeBatch(Ires_users_log res_users_log);

        public void remove(Ires_users_log res_users_log);

        public void create(Ires_users_log res_users_log);

        public void update(Ires_users_log res_users_log);

        public void get(Ires_users_log res_users_log);

        public void updateBatch(Ires_users_log res_users_log);

        public void createBatch(Ires_users_log res_users_log);

        public List<Ires_users_log> select();


}