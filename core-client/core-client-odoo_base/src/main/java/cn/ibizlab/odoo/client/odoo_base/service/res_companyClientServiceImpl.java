package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_company;
import cn.ibizlab.odoo.core.client.service.Ires_companyClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_companyImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ires_companyOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.res_companyOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[res_company] 服务对象接口
 */
@Service
public class res_companyClientServiceImpl implements Ires_companyClientService {
    @Autowired
    private  Ires_companyOdooClient  res_companyOdooClient;

    public Ires_company createModel() {		
		return new res_companyImpl();
	}


        public void removeBatch(List<Ires_company> res_companies){
            
        }
        
        public Page<Ires_company> search(SearchContext context){
            return this.res_companyOdooClient.search(context) ;
        }
        
        public void create(Ires_company res_company){
this.res_companyOdooClient.create(res_company) ;
        }
        
        public void remove(Ires_company res_company){
this.res_companyOdooClient.remove(res_company) ;
        }
        
        public void updateBatch(List<Ires_company> res_companies){
            
        }
        
        public void update(Ires_company res_company){
this.res_companyOdooClient.update(res_company) ;
        }
        
        public void createBatch(List<Ires_company> res_companies){
            
        }
        
        public void get(Ires_company res_company){
            this.res_companyOdooClient.get(res_company) ;
        }
        
        public Page<Ires_company> select(SearchContext context){
            return null ;
        }
        

}

