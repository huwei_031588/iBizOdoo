package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_config_settings;
import cn.ibizlab.odoo.core.client.service.Ires_config_settingsClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_config_settingsImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ires_config_settingsOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.res_config_settingsOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[res_config_settings] 服务对象接口
 */
@Service
public class res_config_settingsClientServiceImpl implements Ires_config_settingsClientService {
    @Autowired
    private  Ires_config_settingsOdooClient  res_config_settingsOdooClient;

    public Ires_config_settings createModel() {		
		return new res_config_settingsImpl();
	}


        public void createBatch(List<Ires_config_settings> res_config_settings){
            
        }
        
        public Page<Ires_config_settings> search(SearchContext context){
            return this.res_config_settingsOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Ires_config_settings> res_config_settings){
            
        }
        
        public void removeBatch(List<Ires_config_settings> res_config_settings){
            
        }
        
        public void update(Ires_config_settings res_config_settings){
this.res_config_settingsOdooClient.update(res_config_settings) ;
        }
        
        public void remove(Ires_config_settings res_config_settings){
this.res_config_settingsOdooClient.remove(res_config_settings) ;
        }
        
        public void create(Ires_config_settings res_config_settings){
this.res_config_settingsOdooClient.create(res_config_settings) ;
        }
        
        public void get(Ires_config_settings res_config_settings){
            this.res_config_settingsOdooClient.get(res_config_settings) ;
        }
        
        public Page<Ires_config_settings> select(SearchContext context){
            return null ;
        }
        

}

