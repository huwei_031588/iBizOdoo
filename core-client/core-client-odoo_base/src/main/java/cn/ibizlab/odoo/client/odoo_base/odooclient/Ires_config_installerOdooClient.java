package cn.ibizlab.odoo.client.odoo_base.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ires_config_installer;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_config_installer] 服务对象客户端接口
 */
public interface Ires_config_installerOdooClient {
    
        public void updateBatch(Ires_config_installer res_config_installer);

        public void remove(Ires_config_installer res_config_installer);

        public void create(Ires_config_installer res_config_installer);

        public void removeBatch(Ires_config_installer res_config_installer);

        public Page<Ires_config_installer> search(SearchContext context);

        public void update(Ires_config_installer res_config_installer);

        public void get(Ires_config_installer res_config_installer);

        public void createBatch(Ires_config_installer res_config_installer);

        public List<Ires_config_installer> select();


}