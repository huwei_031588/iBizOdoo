package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_automation_lead_test;
import cn.ibizlab.odoo.client.odoo_base.model.base_automation_lead_testImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_automation_lead_test] 服务对象接口
 */
public interface base_automation_lead_testFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_automation_lead_tests/createbatch")
    public base_automation_lead_testImpl createBatch(@RequestBody List<base_automation_lead_testImpl> base_automation_lead_tests);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_automation_lead_tests/updatebatch")
    public base_automation_lead_testImpl updateBatch(@RequestBody List<base_automation_lead_testImpl> base_automation_lead_tests);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_automation_lead_tests/{id}")
    public base_automation_lead_testImpl update(@PathVariable("id") Integer id,@RequestBody base_automation_lead_testImpl base_automation_lead_test);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_automation_lead_tests/search")
    public Page<base_automation_lead_testImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_automation_lead_tests")
    public base_automation_lead_testImpl create(@RequestBody base_automation_lead_testImpl base_automation_lead_test);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_automation_lead_tests/removebatch")
    public base_automation_lead_testImpl removeBatch(@RequestBody List<base_automation_lead_testImpl> base_automation_lead_tests);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_automation_lead_tests/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_automation_lead_tests/{id}")
    public base_automation_lead_testImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_automation_lead_tests/select")
    public Page<base_automation_lead_testImpl> select();



}
