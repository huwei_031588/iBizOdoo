package cn.ibizlab.odoo.client.odoo_rating.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Irating_mixin;
import cn.ibizlab.odoo.core.client.service.Irating_mixinClientService;
import cn.ibizlab.odoo.client.odoo_rating.model.rating_mixinImpl;
import cn.ibizlab.odoo.client.odoo_rating.odooclient.Irating_mixinOdooClient;
import cn.ibizlab.odoo.client.odoo_rating.odooclient.impl.rating_mixinOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[rating_mixin] 服务对象接口
 */
@Service
public class rating_mixinClientServiceImpl implements Irating_mixinClientService {
    @Autowired
    private  Irating_mixinOdooClient  rating_mixinOdooClient;

    public Irating_mixin createModel() {		
		return new rating_mixinImpl();
	}


        public void updateBatch(List<Irating_mixin> rating_mixins){
            
        }
        
        public void create(Irating_mixin rating_mixin){
this.rating_mixinOdooClient.create(rating_mixin) ;
        }
        
        public void get(Irating_mixin rating_mixin){
            this.rating_mixinOdooClient.get(rating_mixin) ;
        }
        
        public void removeBatch(List<Irating_mixin> rating_mixins){
            
        }
        
        public Page<Irating_mixin> search(SearchContext context){
            return this.rating_mixinOdooClient.search(context) ;
        }
        
        public void update(Irating_mixin rating_mixin){
this.rating_mixinOdooClient.update(rating_mixin) ;
        }
        
        public void createBatch(List<Irating_mixin> rating_mixins){
            
        }
        
        public void remove(Irating_mixin rating_mixin){
this.rating_mixinOdooClient.remove(rating_mixin) ;
        }
        
        public Page<Irating_mixin> select(SearchContext context){
            return null ;
        }
        

}

