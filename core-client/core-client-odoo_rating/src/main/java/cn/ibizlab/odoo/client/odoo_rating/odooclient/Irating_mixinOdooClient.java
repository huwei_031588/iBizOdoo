package cn.ibizlab.odoo.client.odoo_rating.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Irating_mixin;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[rating_mixin] 服务对象客户端接口
 */
public interface Irating_mixinOdooClient {
    
        public void updateBatch(Irating_mixin rating_mixin);

        public void create(Irating_mixin rating_mixin);

        public void get(Irating_mixin rating_mixin);

        public void removeBatch(Irating_mixin rating_mixin);

        public Page<Irating_mixin> search(SearchContext context);

        public void update(Irating_mixin rating_mixin);

        public void createBatch(Irating_mixin rating_mixin);

        public void remove(Irating_mixin rating_mixin);

        public List<Irating_mixin> select();


}