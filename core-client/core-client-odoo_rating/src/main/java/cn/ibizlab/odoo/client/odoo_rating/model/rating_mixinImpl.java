package cn.ibizlab.odoo.client.odoo_rating.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Irating_mixin;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[rating_mixin] 对象
 */
public class rating_mixinImpl implements Irating_mixin,Serializable{

    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 评级数
     */
    public Integer rating_count;

    @JsonIgnore
    public boolean rating_countDirtyFlag;
    
    /**
     * 评级
     */
    public String rating_ids;

    @JsonIgnore
    public boolean rating_idsDirtyFlag;
    
    /**
     * 最新反馈评级
     */
    public String rating_last_feedback;

    @JsonIgnore
    public boolean rating_last_feedbackDirtyFlag;
    
    /**
     * 最新图像评级
     */
    public byte[] rating_last_image;

    @JsonIgnore
    public boolean rating_last_imageDirtyFlag;
    
    /**
     * 最新值评级
     */
    public Double rating_last_value;

    @JsonIgnore
    public boolean rating_last_valueDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [评级数]
     */
    @JsonProperty("rating_count")
    public Integer getRating_count(){
        return this.rating_count ;
    }

    /**
     * 设置 [评级数]
     */
    @JsonProperty("rating_count")
    public void setRating_count(Integer  rating_count){
        this.rating_count = rating_count ;
        this.rating_countDirtyFlag = true ;
    }

     /**
     * 获取 [评级数]脏标记
     */
    @JsonIgnore
    public boolean getRating_countDirtyFlag(){
        return this.rating_countDirtyFlag ;
    }   

    /**
     * 获取 [评级]
     */
    @JsonProperty("rating_ids")
    public String getRating_ids(){
        return this.rating_ids ;
    }

    /**
     * 设置 [评级]
     */
    @JsonProperty("rating_ids")
    public void setRating_ids(String  rating_ids){
        this.rating_ids = rating_ids ;
        this.rating_idsDirtyFlag = true ;
    }

     /**
     * 获取 [评级]脏标记
     */
    @JsonIgnore
    public boolean getRating_idsDirtyFlag(){
        return this.rating_idsDirtyFlag ;
    }   

    /**
     * 获取 [最新反馈评级]
     */
    @JsonProperty("rating_last_feedback")
    public String getRating_last_feedback(){
        return this.rating_last_feedback ;
    }

    /**
     * 设置 [最新反馈评级]
     */
    @JsonProperty("rating_last_feedback")
    public void setRating_last_feedback(String  rating_last_feedback){
        this.rating_last_feedback = rating_last_feedback ;
        this.rating_last_feedbackDirtyFlag = true ;
    }

     /**
     * 获取 [最新反馈评级]脏标记
     */
    @JsonIgnore
    public boolean getRating_last_feedbackDirtyFlag(){
        return this.rating_last_feedbackDirtyFlag ;
    }   

    /**
     * 获取 [最新图像评级]
     */
    @JsonProperty("rating_last_image")
    public byte[] getRating_last_image(){
        return this.rating_last_image ;
    }

    /**
     * 设置 [最新图像评级]
     */
    @JsonProperty("rating_last_image")
    public void setRating_last_image(byte[]  rating_last_image){
        this.rating_last_image = rating_last_image ;
        this.rating_last_imageDirtyFlag = true ;
    }

     /**
     * 获取 [最新图像评级]脏标记
     */
    @JsonIgnore
    public boolean getRating_last_imageDirtyFlag(){
        return this.rating_last_imageDirtyFlag ;
    }   

    /**
     * 获取 [最新值评级]
     */
    @JsonProperty("rating_last_value")
    public Double getRating_last_value(){
        return this.rating_last_value ;
    }

    /**
     * 设置 [最新值评级]
     */
    @JsonProperty("rating_last_value")
    public void setRating_last_value(Double  rating_last_value){
        this.rating_last_value = rating_last_value ;
        this.rating_last_valueDirtyFlag = true ;
    }

     /**
     * 获取 [最新值评级]脏标记
     */
    @JsonIgnore
    public boolean getRating_last_valueDirtyFlag(){
        return this.rating_last_valueDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("rating_count") instanceof Boolean)&& map.get("rating_count")!=null){
			this.setRating_count((Integer)map.get("rating_count"));
		}
		if(!(map.get("rating_ids") instanceof Boolean)&& map.get("rating_ids")!=null){
			Object[] objs = (Object[])map.get("rating_ids");
			if(objs.length > 0){
				Integer[] rating_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setRating_ids(Arrays.toString(rating_ids));
			}
		}
		if(!(map.get("rating_last_feedback") instanceof Boolean)&& map.get("rating_last_feedback")!=null){
			this.setRating_last_feedback((String)map.get("rating_last_feedback"));
		}
		if(!(map.get("rating_last_image") instanceof Boolean)&& map.get("rating_last_image")!=null){
			//暂时忽略
			//this.setRating_last_image(((String)map.get("rating_last_image")).getBytes("UTF-8"));
		}
		if(!(map.get("rating_last_value") instanceof Boolean)&& map.get("rating_last_value")!=null){
			this.setRating_last_value((Double)map.get("rating_last_value"));
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getRating_count()!=null&&this.getRating_countDirtyFlag()){
			map.put("rating_count",this.getRating_count());
		}else if(this.getRating_countDirtyFlag()){
			map.put("rating_count",false);
		}
		if(this.getRating_ids()!=null&&this.getRating_idsDirtyFlag()){
			map.put("rating_ids",this.getRating_ids());
		}else if(this.getRating_idsDirtyFlag()){
			map.put("rating_ids",false);
		}
		if(this.getRating_last_feedback()!=null&&this.getRating_last_feedbackDirtyFlag()){
			map.put("rating_last_feedback",this.getRating_last_feedback());
		}else if(this.getRating_last_feedbackDirtyFlag()){
			map.put("rating_last_feedback",false);
		}
		if(this.getRating_last_image()!=null&&this.getRating_last_imageDirtyFlag()){
			//暂不支持binary类型rating_last_image
		}else if(this.getRating_last_imageDirtyFlag()){
			map.put("rating_last_image",false);
		}
		if(this.getRating_last_value()!=null&&this.getRating_last_valueDirtyFlag()){
			map.put("rating_last_value",this.getRating_last_value());
		}else if(this.getRating_last_valueDirtyFlag()){
			map.put("rating_last_value",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
