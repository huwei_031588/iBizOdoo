package cn.ibizlab.odoo.client.odoo_portal.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iportal_share;
import cn.ibizlab.odoo.core.client.service.Iportal_shareClientService;
import cn.ibizlab.odoo.client.odoo_portal.model.portal_shareImpl;
import cn.ibizlab.odoo.client.odoo_portal.odooclient.Iportal_shareOdooClient;
import cn.ibizlab.odoo.client.odoo_portal.odooclient.impl.portal_shareOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[portal_share] 服务对象接口
 */
@Service
public class portal_shareClientServiceImpl implements Iportal_shareClientService {
    @Autowired
    private  Iportal_shareOdooClient  portal_shareOdooClient;

    public Iportal_share createModel() {		
		return new portal_shareImpl();
	}


        public void create(Iportal_share portal_share){
this.portal_shareOdooClient.create(portal_share) ;
        }
        
        public Page<Iportal_share> search(SearchContext context){
            return this.portal_shareOdooClient.search(context) ;
        }
        
        public void get(Iportal_share portal_share){
            this.portal_shareOdooClient.get(portal_share) ;
        }
        
        public void removeBatch(List<Iportal_share> portal_shares){
            
        }
        
        public void updateBatch(List<Iportal_share> portal_shares){
            
        }
        
        public void createBatch(List<Iportal_share> portal_shares){
            
        }
        
        public void update(Iportal_share portal_share){
this.portal_shareOdooClient.update(portal_share) ;
        }
        
        public void remove(Iportal_share portal_share){
this.portal_shareOdooClient.remove(portal_share) ;
        }
        
        public Page<Iportal_share> select(SearchContext context){
            return null ;
        }
        

}

