package cn.ibizlab.odoo.client.odoo_portal.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iportal_wizard_user;
import cn.ibizlab.odoo.core.client.service.Iportal_wizard_userClientService;
import cn.ibizlab.odoo.client.odoo_portal.model.portal_wizard_userImpl;
import cn.ibizlab.odoo.client.odoo_portal.odooclient.Iportal_wizard_userOdooClient;
import cn.ibizlab.odoo.client.odoo_portal.odooclient.impl.portal_wizard_userOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[portal_wizard_user] 服务对象接口
 */
@Service
public class portal_wizard_userClientServiceImpl implements Iportal_wizard_userClientService {
    @Autowired
    private  Iportal_wizard_userOdooClient  portal_wizard_userOdooClient;

    public Iportal_wizard_user createModel() {		
		return new portal_wizard_userImpl();
	}


        public void get(Iportal_wizard_user portal_wizard_user){
            this.portal_wizard_userOdooClient.get(portal_wizard_user) ;
        }
        
        public void create(Iportal_wizard_user portal_wizard_user){
this.portal_wizard_userOdooClient.create(portal_wizard_user) ;
        }
        
        public void createBatch(List<Iportal_wizard_user> portal_wizard_users){
            
        }
        
        public void updateBatch(List<Iportal_wizard_user> portal_wizard_users){
            
        }
        
        public Page<Iportal_wizard_user> search(SearchContext context){
            return this.portal_wizard_userOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Iportal_wizard_user> portal_wizard_users){
            
        }
        
        public void remove(Iportal_wizard_user portal_wizard_user){
this.portal_wizard_userOdooClient.remove(portal_wizard_user) ;
        }
        
        public void update(Iportal_wizard_user portal_wizard_user){
this.portal_wizard_userOdooClient.update(portal_wizard_user) ;
        }
        
        public Page<Iportal_wizard_user> select(SearchContext context){
            return null ;
        }
        

}

