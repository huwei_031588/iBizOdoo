package cn.ibizlab.odoo.client.odoo_uom.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iuom_uom;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[uom_uom] 服务对象客户端接口
 */
public interface Iuom_uomOdooClient {
    
        public void createBatch(Iuom_uom uom_uom);

        public void remove(Iuom_uom uom_uom);

        public void updateBatch(Iuom_uom uom_uom);

        public void create(Iuom_uom uom_uom);

        public void get(Iuom_uom uom_uom);

        public void removeBatch(Iuom_uom uom_uom);

        public void update(Iuom_uom uom_uom);

        public Page<Iuom_uom> search(SearchContext context);

        public List<Iuom_uom> select();


}