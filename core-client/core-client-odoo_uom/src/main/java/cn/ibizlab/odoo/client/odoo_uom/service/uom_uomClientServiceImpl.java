package cn.ibizlab.odoo.client.odoo_uom.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iuom_uom;
import cn.ibizlab.odoo.core.client.service.Iuom_uomClientService;
import cn.ibizlab.odoo.client.odoo_uom.model.uom_uomImpl;
import cn.ibizlab.odoo.client.odoo_uom.odooclient.Iuom_uomOdooClient;
import cn.ibizlab.odoo.client.odoo_uom.odooclient.impl.uom_uomOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[uom_uom] 服务对象接口
 */
@Service
public class uom_uomClientServiceImpl implements Iuom_uomClientService {
    @Autowired
    private  Iuom_uomOdooClient  uom_uomOdooClient;

    public Iuom_uom createModel() {		
		return new uom_uomImpl();
	}


        public void createBatch(List<Iuom_uom> uom_uoms){
            
        }
        
        public void remove(Iuom_uom uom_uom){
this.uom_uomOdooClient.remove(uom_uom) ;
        }
        
        public void updateBatch(List<Iuom_uom> uom_uoms){
            
        }
        
        public void create(Iuom_uom uom_uom){
this.uom_uomOdooClient.create(uom_uom) ;
        }
        
        public void get(Iuom_uom uom_uom){
            this.uom_uomOdooClient.get(uom_uom) ;
        }
        
        public void removeBatch(List<Iuom_uom> uom_uoms){
            
        }
        
        public void update(Iuom_uom uom_uom){
this.uom_uomOdooClient.update(uom_uom) ;
        }
        
        public Page<Iuom_uom> search(SearchContext context){
            return this.uom_uomOdooClient.search(context) ;
        }
        
        public Page<Iuom_uom> select(SearchContext context){
            return null ;
        }
        

}

