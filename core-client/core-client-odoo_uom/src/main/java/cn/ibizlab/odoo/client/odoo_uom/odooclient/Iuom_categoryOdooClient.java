package cn.ibizlab.odoo.client.odoo_uom.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iuom_category;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[uom_category] 服务对象客户端接口
 */
public interface Iuom_categoryOdooClient {
    
        public void createBatch(Iuom_category uom_category);

        public void get(Iuom_category uom_category);

        public void updateBatch(Iuom_category uom_category);

        public void remove(Iuom_category uom_category);

        public void create(Iuom_category uom_category);

        public Page<Iuom_category> search(SearchContext context);

        public void removeBatch(Iuom_category uom_category);

        public void update(Iuom_category uom_category);

        public List<Iuom_category> select();


}