package cn.ibizlab.odoo.client.odoo_uom.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iuom_category;
import cn.ibizlab.odoo.client.odoo_uom.model.uom_categoryImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[uom_category] 服务对象接口
 */
public interface uom_categoryFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_uom/uom_categories/createbatch")
    public uom_categoryImpl createBatch(@RequestBody List<uom_categoryImpl> uom_categories);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_uom/uom_categories/{id}")
    public uom_categoryImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_uom/uom_categories/updatebatch")
    public uom_categoryImpl updateBatch(@RequestBody List<uom_categoryImpl> uom_categories);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_uom/uom_categories/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_uom/uom_categories")
    public uom_categoryImpl create(@RequestBody uom_categoryImpl uom_category);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_uom/uom_categories/search")
    public Page<uom_categoryImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_uom/uom_categories/removebatch")
    public uom_categoryImpl removeBatch(@RequestBody List<uom_categoryImpl> uom_categories);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_uom/uom_categories/{id}")
    public uom_categoryImpl update(@PathVariable("id") Integer id,@RequestBody uom_categoryImpl uom_category);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_uom/uom_categories/select")
    public Page<uom_categoryImpl> select();



}
