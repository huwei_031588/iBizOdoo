package cn.ibizlab.odoo.client.odoo_fleet.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_odometer;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[fleet_vehicle_odometer] 服务对象客户端接口
 */
public interface Ifleet_vehicle_odometerOdooClient {
    
        public Page<Ifleet_vehicle_odometer> search(SearchContext context);

        public void removeBatch(Ifleet_vehicle_odometer fleet_vehicle_odometer);

        public void createBatch(Ifleet_vehicle_odometer fleet_vehicle_odometer);

        public void remove(Ifleet_vehicle_odometer fleet_vehicle_odometer);

        public void updateBatch(Ifleet_vehicle_odometer fleet_vehicle_odometer);

        public void create(Ifleet_vehicle_odometer fleet_vehicle_odometer);

        public void get(Ifleet_vehicle_odometer fleet_vehicle_odometer);

        public void update(Ifleet_vehicle_odometer fleet_vehicle_odometer);

        public List<Ifleet_vehicle_odometer> select();


}