package cn.ibizlab.odoo.client.odoo_fleet.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_cost;
import cn.ibizlab.odoo.core.client.service.Ifleet_vehicle_costClientService;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_costImpl;
import cn.ibizlab.odoo.client.odoo_fleet.odooclient.Ifleet_vehicle_costOdooClient;
import cn.ibizlab.odoo.client.odoo_fleet.odooclient.impl.fleet_vehicle_costOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[fleet_vehicle_cost] 服务对象接口
 */
@Service
public class fleet_vehicle_costClientServiceImpl implements Ifleet_vehicle_costClientService {
    @Autowired
    private  Ifleet_vehicle_costOdooClient  fleet_vehicle_costOdooClient;

    public Ifleet_vehicle_cost createModel() {		
		return new fleet_vehicle_costImpl();
	}


        public void createBatch(List<Ifleet_vehicle_cost> fleet_vehicle_costs){
            
        }
        
        public Page<Ifleet_vehicle_cost> search(SearchContext context){
            return this.fleet_vehicle_costOdooClient.search(context) ;
        }
        
        public void create(Ifleet_vehicle_cost fleet_vehicle_cost){
this.fleet_vehicle_costOdooClient.create(fleet_vehicle_cost) ;
        }
        
        public void remove(Ifleet_vehicle_cost fleet_vehicle_cost){
this.fleet_vehicle_costOdooClient.remove(fleet_vehicle_cost) ;
        }
        
        public void update(Ifleet_vehicle_cost fleet_vehicle_cost){
this.fleet_vehicle_costOdooClient.update(fleet_vehicle_cost) ;
        }
        
        public void get(Ifleet_vehicle_cost fleet_vehicle_cost){
            this.fleet_vehicle_costOdooClient.get(fleet_vehicle_cost) ;
        }
        
        public void removeBatch(List<Ifleet_vehicle_cost> fleet_vehicle_costs){
            
        }
        
        public void updateBatch(List<Ifleet_vehicle_cost> fleet_vehicle_costs){
            
        }
        
        public Page<Ifleet_vehicle_cost> select(SearchContext context){
            return null ;
        }
        

}

