package cn.ibizlab.odoo.client.odoo_fleet.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_log_services;
import cn.ibizlab.odoo.core.client.service.Ifleet_vehicle_log_servicesClientService;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_log_servicesImpl;
import cn.ibizlab.odoo.client.odoo_fleet.odooclient.Ifleet_vehicle_log_servicesOdooClient;
import cn.ibizlab.odoo.client.odoo_fleet.odooclient.impl.fleet_vehicle_log_servicesOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[fleet_vehicle_log_services] 服务对象接口
 */
@Service
public class fleet_vehicle_log_servicesClientServiceImpl implements Ifleet_vehicle_log_servicesClientService {
    @Autowired
    private  Ifleet_vehicle_log_servicesOdooClient  fleet_vehicle_log_servicesOdooClient;

    public Ifleet_vehicle_log_services createModel() {		
		return new fleet_vehicle_log_servicesImpl();
	}


        public void remove(Ifleet_vehicle_log_services fleet_vehicle_log_services){
this.fleet_vehicle_log_servicesOdooClient.remove(fleet_vehicle_log_services) ;
        }
        
        public void create(Ifleet_vehicle_log_services fleet_vehicle_log_services){
this.fleet_vehicle_log_servicesOdooClient.create(fleet_vehicle_log_services) ;
        }
        
        public void get(Ifleet_vehicle_log_services fleet_vehicle_log_services){
            this.fleet_vehicle_log_servicesOdooClient.get(fleet_vehicle_log_services) ;
        }
        
        public void updateBatch(List<Ifleet_vehicle_log_services> fleet_vehicle_log_services){
            
        }
        
        public Page<Ifleet_vehicle_log_services> search(SearchContext context){
            return this.fleet_vehicle_log_servicesOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Ifleet_vehicle_log_services> fleet_vehicle_log_services){
            
        }
        
        public void update(Ifleet_vehicle_log_services fleet_vehicle_log_services){
this.fleet_vehicle_log_servicesOdooClient.update(fleet_vehicle_log_services) ;
        }
        
        public void createBatch(List<Ifleet_vehicle_log_services> fleet_vehicle_log_services){
            
        }
        
        public Page<Ifleet_vehicle_log_services> select(SearchContext context){
            return null ;
        }
        

}

