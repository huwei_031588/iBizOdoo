package cn.ibizlab.odoo.client.odoo_fleet.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_log_contract;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[fleet_vehicle_log_contract] 服务对象客户端接口
 */
public interface Ifleet_vehicle_log_contractOdooClient {
    
        public void remove(Ifleet_vehicle_log_contract fleet_vehicle_log_contract);

        public Page<Ifleet_vehicle_log_contract> search(SearchContext context);

        public void update(Ifleet_vehicle_log_contract fleet_vehicle_log_contract);

        public void get(Ifleet_vehicle_log_contract fleet_vehicle_log_contract);

        public void updateBatch(Ifleet_vehicle_log_contract fleet_vehicle_log_contract);

        public void createBatch(Ifleet_vehicle_log_contract fleet_vehicle_log_contract);

        public void create(Ifleet_vehicle_log_contract fleet_vehicle_log_contract);

        public void removeBatch(Ifleet_vehicle_log_contract fleet_vehicle_log_contract);

        public List<Ifleet_vehicle_log_contract> select();


}