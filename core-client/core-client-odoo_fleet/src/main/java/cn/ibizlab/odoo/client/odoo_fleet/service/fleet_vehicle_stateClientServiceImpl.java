package cn.ibizlab.odoo.client.odoo_fleet.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_state;
import cn.ibizlab.odoo.core.client.service.Ifleet_vehicle_stateClientService;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_stateImpl;
import cn.ibizlab.odoo.client.odoo_fleet.odooclient.Ifleet_vehicle_stateOdooClient;
import cn.ibizlab.odoo.client.odoo_fleet.odooclient.impl.fleet_vehicle_stateOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[fleet_vehicle_state] 服务对象接口
 */
@Service
public class fleet_vehicle_stateClientServiceImpl implements Ifleet_vehicle_stateClientService {
    @Autowired
    private  Ifleet_vehicle_stateOdooClient  fleet_vehicle_stateOdooClient;

    public Ifleet_vehicle_state createModel() {		
		return new fleet_vehicle_stateImpl();
	}


        public void createBatch(List<Ifleet_vehicle_state> fleet_vehicle_states){
            
        }
        
        public void create(Ifleet_vehicle_state fleet_vehicle_state){
this.fleet_vehicle_stateOdooClient.create(fleet_vehicle_state) ;
        }
        
        public void updateBatch(List<Ifleet_vehicle_state> fleet_vehicle_states){
            
        }
        
        public void removeBatch(List<Ifleet_vehicle_state> fleet_vehicle_states){
            
        }
        
        public Page<Ifleet_vehicle_state> search(SearchContext context){
            return this.fleet_vehicle_stateOdooClient.search(context) ;
        }
        
        public void remove(Ifleet_vehicle_state fleet_vehicle_state){
this.fleet_vehicle_stateOdooClient.remove(fleet_vehicle_state) ;
        }
        
        public void update(Ifleet_vehicle_state fleet_vehicle_state){
this.fleet_vehicle_stateOdooClient.update(fleet_vehicle_state) ;
        }
        
        public void get(Ifleet_vehicle_state fleet_vehicle_state){
            this.fleet_vehicle_stateOdooClient.get(fleet_vehicle_state) ;
        }
        
        public Page<Ifleet_vehicle_state> select(SearchContext context){
            return null ;
        }
        

}

