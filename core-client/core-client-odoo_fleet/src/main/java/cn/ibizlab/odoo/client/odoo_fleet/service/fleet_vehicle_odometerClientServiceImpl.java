package cn.ibizlab.odoo.client.odoo_fleet.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_odometer;
import cn.ibizlab.odoo.core.client.service.Ifleet_vehicle_odometerClientService;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_odometerImpl;
import cn.ibizlab.odoo.client.odoo_fleet.odooclient.Ifleet_vehicle_odometerOdooClient;
import cn.ibizlab.odoo.client.odoo_fleet.odooclient.impl.fleet_vehicle_odometerOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[fleet_vehicle_odometer] 服务对象接口
 */
@Service
public class fleet_vehicle_odometerClientServiceImpl implements Ifleet_vehicle_odometerClientService {
    @Autowired
    private  Ifleet_vehicle_odometerOdooClient  fleet_vehicle_odometerOdooClient;

    public Ifleet_vehicle_odometer createModel() {		
		return new fleet_vehicle_odometerImpl();
	}


        public Page<Ifleet_vehicle_odometer> search(SearchContext context){
            return this.fleet_vehicle_odometerOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Ifleet_vehicle_odometer> fleet_vehicle_odometers){
            
        }
        
        public void createBatch(List<Ifleet_vehicle_odometer> fleet_vehicle_odometers){
            
        }
        
        public void remove(Ifleet_vehicle_odometer fleet_vehicle_odometer){
this.fleet_vehicle_odometerOdooClient.remove(fleet_vehicle_odometer) ;
        }
        
        public void updateBatch(List<Ifleet_vehicle_odometer> fleet_vehicle_odometers){
            
        }
        
        public void create(Ifleet_vehicle_odometer fleet_vehicle_odometer){
this.fleet_vehicle_odometerOdooClient.create(fleet_vehicle_odometer) ;
        }
        
        public void get(Ifleet_vehicle_odometer fleet_vehicle_odometer){
            this.fleet_vehicle_odometerOdooClient.get(fleet_vehicle_odometer) ;
        }
        
        public void update(Ifleet_vehicle_odometer fleet_vehicle_odometer){
this.fleet_vehicle_odometerOdooClient.update(fleet_vehicle_odometer) ;
        }
        
        public Page<Ifleet_vehicle_odometer> select(SearchContext context){
            return null ;
        }
        

}

