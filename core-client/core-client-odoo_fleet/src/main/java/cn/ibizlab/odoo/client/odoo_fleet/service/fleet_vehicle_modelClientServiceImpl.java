package cn.ibizlab.odoo.client.odoo_fleet.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_model;
import cn.ibizlab.odoo.core.client.service.Ifleet_vehicle_modelClientService;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_modelImpl;
import cn.ibizlab.odoo.client.odoo_fleet.odooclient.Ifleet_vehicle_modelOdooClient;
import cn.ibizlab.odoo.client.odoo_fleet.odooclient.impl.fleet_vehicle_modelOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[fleet_vehicle_model] 服务对象接口
 */
@Service
public class fleet_vehicle_modelClientServiceImpl implements Ifleet_vehicle_modelClientService {
    @Autowired
    private  Ifleet_vehicle_modelOdooClient  fleet_vehicle_modelOdooClient;

    public Ifleet_vehicle_model createModel() {		
		return new fleet_vehicle_modelImpl();
	}


        public void remove(Ifleet_vehicle_model fleet_vehicle_model){
this.fleet_vehicle_modelOdooClient.remove(fleet_vehicle_model) ;
        }
        
        public void create(Ifleet_vehicle_model fleet_vehicle_model){
this.fleet_vehicle_modelOdooClient.create(fleet_vehicle_model) ;
        }
        
        public void update(Ifleet_vehicle_model fleet_vehicle_model){
this.fleet_vehicle_modelOdooClient.update(fleet_vehicle_model) ;
        }
        
        public void get(Ifleet_vehicle_model fleet_vehicle_model){
            this.fleet_vehicle_modelOdooClient.get(fleet_vehicle_model) ;
        }
        
        public void updateBatch(List<Ifleet_vehicle_model> fleet_vehicle_models){
            
        }
        
        public Page<Ifleet_vehicle_model> search(SearchContext context){
            return this.fleet_vehicle_modelOdooClient.search(context) ;
        }
        
        public void createBatch(List<Ifleet_vehicle_model> fleet_vehicle_models){
            
        }
        
        public void removeBatch(List<Ifleet_vehicle_model> fleet_vehicle_models){
            
        }
        
        public Page<Ifleet_vehicle_model> select(SearchContext context){
            return null ;
        }
        

}

