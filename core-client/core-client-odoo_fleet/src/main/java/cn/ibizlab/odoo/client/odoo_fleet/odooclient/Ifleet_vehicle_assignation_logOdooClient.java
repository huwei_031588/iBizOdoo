package cn.ibizlab.odoo.client.odoo_fleet.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_assignation_log;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[fleet_vehicle_assignation_log] 服务对象客户端接口
 */
public interface Ifleet_vehicle_assignation_logOdooClient {
    
        public void removeBatch(Ifleet_vehicle_assignation_log fleet_vehicle_assignation_log);

        public Page<Ifleet_vehicle_assignation_log> search(SearchContext context);

        public void updateBatch(Ifleet_vehicle_assignation_log fleet_vehicle_assignation_log);

        public void update(Ifleet_vehicle_assignation_log fleet_vehicle_assignation_log);

        public void createBatch(Ifleet_vehicle_assignation_log fleet_vehicle_assignation_log);

        public void get(Ifleet_vehicle_assignation_log fleet_vehicle_assignation_log);

        public void create(Ifleet_vehicle_assignation_log fleet_vehicle_assignation_log);

        public void remove(Ifleet_vehicle_assignation_log fleet_vehicle_assignation_log);

        public List<Ifleet_vehicle_assignation_log> select();


}