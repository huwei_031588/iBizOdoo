package cn.ibizlab.odoo.client.odoo_fleet.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_log_services;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_log_servicesImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[fleet_vehicle_log_services] 服务对象接口
 */
public interface fleet_vehicle_log_servicesFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_log_services/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_log_services")
    public fleet_vehicle_log_servicesImpl create(@RequestBody fleet_vehicle_log_servicesImpl fleet_vehicle_log_services);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_log_services/{id}")
    public fleet_vehicle_log_servicesImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_log_services/updatebatch")
    public fleet_vehicle_log_servicesImpl updateBatch(@RequestBody List<fleet_vehicle_log_servicesImpl> fleet_vehicle_log_services);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_log_services/search")
    public Page<fleet_vehicle_log_servicesImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_log_services/removebatch")
    public fleet_vehicle_log_servicesImpl removeBatch(@RequestBody List<fleet_vehicle_log_servicesImpl> fleet_vehicle_log_services);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_log_services/{id}")
    public fleet_vehicle_log_servicesImpl update(@PathVariable("id") Integer id,@RequestBody fleet_vehicle_log_servicesImpl fleet_vehicle_log_services);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_log_services/createbatch")
    public fleet_vehicle_log_servicesImpl createBatch(@RequestBody List<fleet_vehicle_log_servicesImpl> fleet_vehicle_log_services);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_log_services/select")
    public Page<fleet_vehicle_log_servicesImpl> select();



}
