package cn.ibizlab.odoo.client.odoo_fleet.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_assignation_log;
import cn.ibizlab.odoo.core.client.service.Ifleet_vehicle_assignation_logClientService;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_assignation_logImpl;
import cn.ibizlab.odoo.client.odoo_fleet.odooclient.Ifleet_vehicle_assignation_logOdooClient;
import cn.ibizlab.odoo.client.odoo_fleet.odooclient.impl.fleet_vehicle_assignation_logOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[fleet_vehicle_assignation_log] 服务对象接口
 */
@Service
public class fleet_vehicle_assignation_logClientServiceImpl implements Ifleet_vehicle_assignation_logClientService {
    @Autowired
    private  Ifleet_vehicle_assignation_logOdooClient  fleet_vehicle_assignation_logOdooClient;

    public Ifleet_vehicle_assignation_log createModel() {		
		return new fleet_vehicle_assignation_logImpl();
	}


        public void removeBatch(List<Ifleet_vehicle_assignation_log> fleet_vehicle_assignation_logs){
            
        }
        
        public Page<Ifleet_vehicle_assignation_log> search(SearchContext context){
            return this.fleet_vehicle_assignation_logOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Ifleet_vehicle_assignation_log> fleet_vehicle_assignation_logs){
            
        }
        
        public void update(Ifleet_vehicle_assignation_log fleet_vehicle_assignation_log){
this.fleet_vehicle_assignation_logOdooClient.update(fleet_vehicle_assignation_log) ;
        }
        
        public void createBatch(List<Ifleet_vehicle_assignation_log> fleet_vehicle_assignation_logs){
            
        }
        
        public void get(Ifleet_vehicle_assignation_log fleet_vehicle_assignation_log){
            this.fleet_vehicle_assignation_logOdooClient.get(fleet_vehicle_assignation_log) ;
        }
        
        public void create(Ifleet_vehicle_assignation_log fleet_vehicle_assignation_log){
this.fleet_vehicle_assignation_logOdooClient.create(fleet_vehicle_assignation_log) ;
        }
        
        public void remove(Ifleet_vehicle_assignation_log fleet_vehicle_assignation_log){
this.fleet_vehicle_assignation_logOdooClient.remove(fleet_vehicle_assignation_log) ;
        }
        
        public Page<Ifleet_vehicle_assignation_log> select(SearchContext context){
            return null ;
        }
        

}

