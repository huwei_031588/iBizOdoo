package cn.ibizlab.odoo.client.odoo_fleet.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_log_contract;
import cn.ibizlab.odoo.core.client.service.Ifleet_vehicle_log_contractClientService;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_log_contractImpl;
import cn.ibizlab.odoo.client.odoo_fleet.odooclient.Ifleet_vehicle_log_contractOdooClient;
import cn.ibizlab.odoo.client.odoo_fleet.odooclient.impl.fleet_vehicle_log_contractOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[fleet_vehicle_log_contract] 服务对象接口
 */
@Service
public class fleet_vehicle_log_contractClientServiceImpl implements Ifleet_vehicle_log_contractClientService {
    @Autowired
    private  Ifleet_vehicle_log_contractOdooClient  fleet_vehicle_log_contractOdooClient;

    public Ifleet_vehicle_log_contract createModel() {		
		return new fleet_vehicle_log_contractImpl();
	}


        public void remove(Ifleet_vehicle_log_contract fleet_vehicle_log_contract){
this.fleet_vehicle_log_contractOdooClient.remove(fleet_vehicle_log_contract) ;
        }
        
        public Page<Ifleet_vehicle_log_contract> search(SearchContext context){
            return this.fleet_vehicle_log_contractOdooClient.search(context) ;
        }
        
        public void update(Ifleet_vehicle_log_contract fleet_vehicle_log_contract){
this.fleet_vehicle_log_contractOdooClient.update(fleet_vehicle_log_contract) ;
        }
        
        public void get(Ifleet_vehicle_log_contract fleet_vehicle_log_contract){
            this.fleet_vehicle_log_contractOdooClient.get(fleet_vehicle_log_contract) ;
        }
        
        public void updateBatch(List<Ifleet_vehicle_log_contract> fleet_vehicle_log_contracts){
            
        }
        
        public void createBatch(List<Ifleet_vehicle_log_contract> fleet_vehicle_log_contracts){
            
        }
        
        public void create(Ifleet_vehicle_log_contract fleet_vehicle_log_contract){
this.fleet_vehicle_log_contractOdooClient.create(fleet_vehicle_log_contract) ;
        }
        
        public void removeBatch(List<Ifleet_vehicle_log_contract> fleet_vehicle_log_contracts){
            
        }
        
        public Page<Ifleet_vehicle_log_contract> select(SearchContext context){
            return null ;
        }
        

}

