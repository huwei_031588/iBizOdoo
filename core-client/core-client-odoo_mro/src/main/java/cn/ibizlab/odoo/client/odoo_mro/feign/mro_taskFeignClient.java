package cn.ibizlab.odoo.client.odoo_mro.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imro_task;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_taskImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mro_task] 服务对象接口
 */
public interface mro_taskFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_tasks/updatebatch")
    public mro_taskImpl updateBatch(@RequestBody List<mro_taskImpl> mro_tasks);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_tasks/createbatch")
    public mro_taskImpl createBatch(@RequestBody List<mro_taskImpl> mro_tasks);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_tasks/search")
    public Page<mro_taskImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_tasks/{id}")
    public mro_taskImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_tasks")
    public mro_taskImpl create(@RequestBody mro_taskImpl mro_task);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_tasks/{id}")
    public mro_taskImpl update(@PathVariable("id") Integer id,@RequestBody mro_taskImpl mro_task);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_tasks/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_tasks/removebatch")
    public mro_taskImpl removeBatch(@RequestBody List<mro_taskImpl> mro_tasks);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_tasks/select")
    public Page<mro_taskImpl> select();



}
