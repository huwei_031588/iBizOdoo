package cn.ibizlab.odoo.client.odoo_mro.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imro_request_reject;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_request_rejectImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mro_request_reject] 服务对象接口
 */
public interface mro_request_rejectFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_request_rejects/search")
    public Page<mro_request_rejectImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_request_rejects/{id}")
    public mro_request_rejectImpl update(@PathVariable("id") Integer id,@RequestBody mro_request_rejectImpl mro_request_reject);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_request_rejects/updatebatch")
    public mro_request_rejectImpl updateBatch(@RequestBody List<mro_request_rejectImpl> mro_request_rejects);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_request_rejects")
    public mro_request_rejectImpl create(@RequestBody mro_request_rejectImpl mro_request_reject);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_request_rejects/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_request_rejects/removebatch")
    public mro_request_rejectImpl removeBatch(@RequestBody List<mro_request_rejectImpl> mro_request_rejects);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_request_rejects/createbatch")
    public mro_request_rejectImpl createBatch(@RequestBody List<mro_request_rejectImpl> mro_request_rejects);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_request_rejects/{id}")
    public mro_request_rejectImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_request_rejects/select")
    public Page<mro_request_rejectImpl> select();



}
