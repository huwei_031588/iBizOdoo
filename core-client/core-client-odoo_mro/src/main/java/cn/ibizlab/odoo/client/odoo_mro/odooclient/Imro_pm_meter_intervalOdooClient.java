package cn.ibizlab.odoo.client.odoo_mro.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imro_pm_meter_interval;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mro_pm_meter_interval] 服务对象客户端接口
 */
public interface Imro_pm_meter_intervalOdooClient {
    
        public void update(Imro_pm_meter_interval mro_pm_meter_interval);

        public void removeBatch(Imro_pm_meter_interval mro_pm_meter_interval);

        public void remove(Imro_pm_meter_interval mro_pm_meter_interval);

        public void createBatch(Imro_pm_meter_interval mro_pm_meter_interval);

        public void create(Imro_pm_meter_interval mro_pm_meter_interval);

        public Page<Imro_pm_meter_interval> search(SearchContext context);

        public void get(Imro_pm_meter_interval mro_pm_meter_interval);

        public void updateBatch(Imro_pm_meter_interval mro_pm_meter_interval);

        public List<Imro_pm_meter_interval> select();


}