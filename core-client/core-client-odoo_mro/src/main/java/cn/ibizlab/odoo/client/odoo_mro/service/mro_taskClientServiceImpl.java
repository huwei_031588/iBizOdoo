package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_task;
import cn.ibizlab.odoo.core.client.service.Imro_taskClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_taskImpl;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.Imro_taskOdooClient;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.impl.mro_taskOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mro_task] 服务对象接口
 */
@Service
public class mro_taskClientServiceImpl implements Imro_taskClientService {
    @Autowired
    private  Imro_taskOdooClient  mro_taskOdooClient;

    public Imro_task createModel() {		
		return new mro_taskImpl();
	}


        public void updateBatch(List<Imro_task> mro_tasks){
            
        }
        
        public void createBatch(List<Imro_task> mro_tasks){
            
        }
        
        public Page<Imro_task> search(SearchContext context){
            return this.mro_taskOdooClient.search(context) ;
        }
        
        public void get(Imro_task mro_task){
            this.mro_taskOdooClient.get(mro_task) ;
        }
        
        public void create(Imro_task mro_task){
this.mro_taskOdooClient.create(mro_task) ;
        }
        
        public void update(Imro_task mro_task){
this.mro_taskOdooClient.update(mro_task) ;
        }
        
        public void remove(Imro_task mro_task){
this.mro_taskOdooClient.remove(mro_task) ;
        }
        
        public void removeBatch(List<Imro_task> mro_tasks){
            
        }
        
        public Page<Imro_task> select(SearchContext context){
            return null ;
        }
        

}

