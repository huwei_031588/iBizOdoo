package cn.ibizlab.odoo.client.odoo_mro.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imro_pm_rule;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mro_pm_rule] 服务对象客户端接口
 */
public interface Imro_pm_ruleOdooClient {
    
        public void create(Imro_pm_rule mro_pm_rule);

        public Page<Imro_pm_rule> search(SearchContext context);

        public void get(Imro_pm_rule mro_pm_rule);

        public void removeBatch(Imro_pm_rule mro_pm_rule);

        public void createBatch(Imro_pm_rule mro_pm_rule);

        public void remove(Imro_pm_rule mro_pm_rule);

        public void update(Imro_pm_rule mro_pm_rule);

        public void updateBatch(Imro_pm_rule mro_pm_rule);

        public List<Imro_pm_rule> select();


}