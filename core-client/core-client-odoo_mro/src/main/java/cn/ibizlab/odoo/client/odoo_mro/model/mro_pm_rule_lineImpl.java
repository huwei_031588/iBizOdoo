package cn.ibizlab.odoo.client.odoo_mro.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Imro_pm_rule_line;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[mro_pm_rule_line] 对象
 */
public class mro_pm_rule_lineImpl implements Imro_pm_rule_line,Serializable{

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * Meter Interval
     */
    public Integer meter_interval_id;

    @JsonIgnore
    public boolean meter_interval_idDirtyFlag;
    
    /**
     * Meter Interval
     */
    public String meter_interval_id_text;

    @JsonIgnore
    public boolean meter_interval_id_textDirtyFlag;
    
    /**
     * PM Rule
     */
    public Integer pm_rule_id;

    @JsonIgnore
    public boolean pm_rule_idDirtyFlag;
    
    /**
     * PM Rule
     */
    public String pm_rule_id_text;

    @JsonIgnore
    public boolean pm_rule_id_textDirtyFlag;
    
    /**
     * Task
     */
    public Integer task_id;

    @JsonIgnore
    public boolean task_idDirtyFlag;
    
    /**
     * Task
     */
    public String task_id_text;

    @JsonIgnore
    public boolean task_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [Meter Interval]
     */
    @JsonProperty("meter_interval_id")
    public Integer getMeter_interval_id(){
        return this.meter_interval_id ;
    }

    /**
     * 设置 [Meter Interval]
     */
    @JsonProperty("meter_interval_id")
    public void setMeter_interval_id(Integer  meter_interval_id){
        this.meter_interval_id = meter_interval_id ;
        this.meter_interval_idDirtyFlag = true ;
    }

     /**
     * 获取 [Meter Interval]脏标记
     */
    @JsonIgnore
    public boolean getMeter_interval_idDirtyFlag(){
        return this.meter_interval_idDirtyFlag ;
    }   

    /**
     * 获取 [Meter Interval]
     */
    @JsonProperty("meter_interval_id_text")
    public String getMeter_interval_id_text(){
        return this.meter_interval_id_text ;
    }

    /**
     * 设置 [Meter Interval]
     */
    @JsonProperty("meter_interval_id_text")
    public void setMeter_interval_id_text(String  meter_interval_id_text){
        this.meter_interval_id_text = meter_interval_id_text ;
        this.meter_interval_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [Meter Interval]脏标记
     */
    @JsonIgnore
    public boolean getMeter_interval_id_textDirtyFlag(){
        return this.meter_interval_id_textDirtyFlag ;
    }   

    /**
     * 获取 [PM Rule]
     */
    @JsonProperty("pm_rule_id")
    public Integer getPm_rule_id(){
        return this.pm_rule_id ;
    }

    /**
     * 设置 [PM Rule]
     */
    @JsonProperty("pm_rule_id")
    public void setPm_rule_id(Integer  pm_rule_id){
        this.pm_rule_id = pm_rule_id ;
        this.pm_rule_idDirtyFlag = true ;
    }

     /**
     * 获取 [PM Rule]脏标记
     */
    @JsonIgnore
    public boolean getPm_rule_idDirtyFlag(){
        return this.pm_rule_idDirtyFlag ;
    }   

    /**
     * 获取 [PM Rule]
     */
    @JsonProperty("pm_rule_id_text")
    public String getPm_rule_id_text(){
        return this.pm_rule_id_text ;
    }

    /**
     * 设置 [PM Rule]
     */
    @JsonProperty("pm_rule_id_text")
    public void setPm_rule_id_text(String  pm_rule_id_text){
        this.pm_rule_id_text = pm_rule_id_text ;
        this.pm_rule_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [PM Rule]脏标记
     */
    @JsonIgnore
    public boolean getPm_rule_id_textDirtyFlag(){
        return this.pm_rule_id_textDirtyFlag ;
    }   

    /**
     * 获取 [Task]
     */
    @JsonProperty("task_id")
    public Integer getTask_id(){
        return this.task_id ;
    }

    /**
     * 设置 [Task]
     */
    @JsonProperty("task_id")
    public void setTask_id(Integer  task_id){
        this.task_id = task_id ;
        this.task_idDirtyFlag = true ;
    }

     /**
     * 获取 [Task]脏标记
     */
    @JsonIgnore
    public boolean getTask_idDirtyFlag(){
        return this.task_idDirtyFlag ;
    }   

    /**
     * 获取 [Task]
     */
    @JsonProperty("task_id_text")
    public String getTask_id_text(){
        return this.task_id_text ;
    }

    /**
     * 设置 [Task]
     */
    @JsonProperty("task_id_text")
    public void setTask_id_text(String  task_id_text){
        this.task_id_text = task_id_text ;
        this.task_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [Task]脏标记
     */
    @JsonIgnore
    public boolean getTask_id_textDirtyFlag(){
        return this.task_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("meter_interval_id") instanceof Boolean)&& map.get("meter_interval_id")!=null){
			Object[] objs = (Object[])map.get("meter_interval_id");
			if(objs.length > 0){
				this.setMeter_interval_id((Integer)objs[0]);
			}
		}
		if(!(map.get("meter_interval_id") instanceof Boolean)&& map.get("meter_interval_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("meter_interval_id");
			if(objs.length > 1){
				this.setMeter_interval_id_text((String)objs[1]);
			}
		}
		if(!(map.get("pm_rule_id") instanceof Boolean)&& map.get("pm_rule_id")!=null){
			Object[] objs = (Object[])map.get("pm_rule_id");
			if(objs.length > 0){
				this.setPm_rule_id((Integer)objs[0]);
			}
		}
		if(!(map.get("pm_rule_id") instanceof Boolean)&& map.get("pm_rule_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("pm_rule_id");
			if(objs.length > 1){
				this.setPm_rule_id_text((String)objs[1]);
			}
		}
		if(!(map.get("task_id") instanceof Boolean)&& map.get("task_id")!=null){
			Object[] objs = (Object[])map.get("task_id");
			if(objs.length > 0){
				this.setTask_id((Integer)objs[0]);
			}
		}
		if(!(map.get("task_id") instanceof Boolean)&& map.get("task_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("task_id");
			if(objs.length > 1){
				this.setTask_id_text((String)objs[1]);
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getMeter_interval_id()!=null&&this.getMeter_interval_idDirtyFlag()){
			map.put("meter_interval_id",this.getMeter_interval_id());
		}else if(this.getMeter_interval_idDirtyFlag()){
			map.put("meter_interval_id",false);
		}
		if(this.getMeter_interval_id_text()!=null&&this.getMeter_interval_id_textDirtyFlag()){
			//忽略文本外键meter_interval_id_text
		}else if(this.getMeter_interval_id_textDirtyFlag()){
			map.put("meter_interval_id",false);
		}
		if(this.getPm_rule_id()!=null&&this.getPm_rule_idDirtyFlag()){
			map.put("pm_rule_id",this.getPm_rule_id());
		}else if(this.getPm_rule_idDirtyFlag()){
			map.put("pm_rule_id",false);
		}
		if(this.getPm_rule_id_text()!=null&&this.getPm_rule_id_textDirtyFlag()){
			//忽略文本外键pm_rule_id_text
		}else if(this.getPm_rule_id_textDirtyFlag()){
			map.put("pm_rule_id",false);
		}
		if(this.getTask_id()!=null&&this.getTask_idDirtyFlag()){
			map.put("task_id",this.getTask_id());
		}else if(this.getTask_idDirtyFlag()){
			map.put("task_id",false);
		}
		if(this.getTask_id_text()!=null&&this.getTask_id_textDirtyFlag()){
			//忽略文本外键task_id_text
		}else if(this.getTask_id_textDirtyFlag()){
			map.put("task_id",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
