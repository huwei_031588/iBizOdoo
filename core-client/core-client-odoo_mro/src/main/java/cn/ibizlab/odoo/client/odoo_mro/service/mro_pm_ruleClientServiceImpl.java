package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_pm_rule;
import cn.ibizlab.odoo.core.client.service.Imro_pm_ruleClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_pm_ruleImpl;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.Imro_pm_ruleOdooClient;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.impl.mro_pm_ruleOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mro_pm_rule] 服务对象接口
 */
@Service
public class mro_pm_ruleClientServiceImpl implements Imro_pm_ruleClientService {
    @Autowired
    private  Imro_pm_ruleOdooClient  mro_pm_ruleOdooClient;

    public Imro_pm_rule createModel() {		
		return new mro_pm_ruleImpl();
	}


        public void create(Imro_pm_rule mro_pm_rule){
this.mro_pm_ruleOdooClient.create(mro_pm_rule) ;
        }
        
        public Page<Imro_pm_rule> search(SearchContext context){
            return this.mro_pm_ruleOdooClient.search(context) ;
        }
        
        public void get(Imro_pm_rule mro_pm_rule){
            this.mro_pm_ruleOdooClient.get(mro_pm_rule) ;
        }
        
        public void removeBatch(List<Imro_pm_rule> mro_pm_rules){
            
        }
        
        public void createBatch(List<Imro_pm_rule> mro_pm_rules){
            
        }
        
        public void remove(Imro_pm_rule mro_pm_rule){
this.mro_pm_ruleOdooClient.remove(mro_pm_rule) ;
        }
        
        public void update(Imro_pm_rule mro_pm_rule){
this.mro_pm_ruleOdooClient.update(mro_pm_rule) ;
        }
        
        public void updateBatch(List<Imro_pm_rule> mro_pm_rules){
            
        }
        
        public Page<Imro_pm_rule> select(SearchContext context){
            return null ;
        }
        

}

