package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_pm_parameter;
import cn.ibizlab.odoo.core.client.service.Imro_pm_parameterClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_pm_parameterImpl;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.Imro_pm_parameterOdooClient;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.impl.mro_pm_parameterOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mro_pm_parameter] 服务对象接口
 */
@Service
public class mro_pm_parameterClientServiceImpl implements Imro_pm_parameterClientService {
    @Autowired
    private  Imro_pm_parameterOdooClient  mro_pm_parameterOdooClient;

    public Imro_pm_parameter createModel() {		
		return new mro_pm_parameterImpl();
	}


        public void removeBatch(List<Imro_pm_parameter> mro_pm_parameters){
            
        }
        
        public void get(Imro_pm_parameter mro_pm_parameter){
            this.mro_pm_parameterOdooClient.get(mro_pm_parameter) ;
        }
        
        public void create(Imro_pm_parameter mro_pm_parameter){
this.mro_pm_parameterOdooClient.create(mro_pm_parameter) ;
        }
        
        public void updateBatch(List<Imro_pm_parameter> mro_pm_parameters){
            
        }
        
        public void createBatch(List<Imro_pm_parameter> mro_pm_parameters){
            
        }
        
        public void remove(Imro_pm_parameter mro_pm_parameter){
this.mro_pm_parameterOdooClient.remove(mro_pm_parameter) ;
        }
        
        public Page<Imro_pm_parameter> search(SearchContext context){
            return this.mro_pm_parameterOdooClient.search(context) ;
        }
        
        public void update(Imro_pm_parameter mro_pm_parameter){
this.mro_pm_parameterOdooClient.update(mro_pm_parameter) ;
        }
        
        public Page<Imro_pm_parameter> select(SearchContext context){
            return null ;
        }
        

}

