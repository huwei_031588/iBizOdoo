package cn.ibizlab.odoo.client.odoo_mro.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imro_workorder;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mro_workorder] 服务对象客户端接口
 */
public interface Imro_workorderOdooClient {
    
        public void create(Imro_workorder mro_workorder);

        public Page<Imro_workorder> search(SearchContext context);

        public void update(Imro_workorder mro_workorder);

        public void get(Imro_workorder mro_workorder);

        public void removeBatch(Imro_workorder mro_workorder);

        public void remove(Imro_workorder mro_workorder);

        public void updateBatch(Imro_workorder mro_workorder);

        public void createBatch(Imro_workorder mro_workorder);

        public List<Imro_workorder> select();


}