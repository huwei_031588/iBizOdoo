package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_request_reject;
import cn.ibizlab.odoo.core.client.service.Imro_request_rejectClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_request_rejectImpl;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.Imro_request_rejectOdooClient;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.impl.mro_request_rejectOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mro_request_reject] 服务对象接口
 */
@Service
public class mro_request_rejectClientServiceImpl implements Imro_request_rejectClientService {
    @Autowired
    private  Imro_request_rejectOdooClient  mro_request_rejectOdooClient;

    public Imro_request_reject createModel() {		
		return new mro_request_rejectImpl();
	}


        public Page<Imro_request_reject> search(SearchContext context){
            return this.mro_request_rejectOdooClient.search(context) ;
        }
        
        public void update(Imro_request_reject mro_request_reject){
this.mro_request_rejectOdooClient.update(mro_request_reject) ;
        }
        
        public void updateBatch(List<Imro_request_reject> mro_request_rejects){
            
        }
        
        public void create(Imro_request_reject mro_request_reject){
this.mro_request_rejectOdooClient.create(mro_request_reject) ;
        }
        
        public void remove(Imro_request_reject mro_request_reject){
this.mro_request_rejectOdooClient.remove(mro_request_reject) ;
        }
        
        public void removeBatch(List<Imro_request_reject> mro_request_rejects){
            
        }
        
        public void createBatch(List<Imro_request_reject> mro_request_rejects){
            
        }
        
        public void get(Imro_request_reject mro_request_reject){
            this.mro_request_rejectOdooClient.get(mro_request_reject) ;
        }
        
        public Page<Imro_request_reject> select(SearchContext context){
            return null ;
        }
        

}

