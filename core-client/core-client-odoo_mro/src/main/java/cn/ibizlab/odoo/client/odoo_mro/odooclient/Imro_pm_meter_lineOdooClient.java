package cn.ibizlab.odoo.client.odoo_mro.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imro_pm_meter_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mro_pm_meter_line] 服务对象客户端接口
 */
public interface Imro_pm_meter_lineOdooClient {
    
        public void create(Imro_pm_meter_line mro_pm_meter_line);

        public void get(Imro_pm_meter_line mro_pm_meter_line);

        public void removeBatch(Imro_pm_meter_line mro_pm_meter_line);

        public void update(Imro_pm_meter_line mro_pm_meter_line);

        public void updateBatch(Imro_pm_meter_line mro_pm_meter_line);

        public void remove(Imro_pm_meter_line mro_pm_meter_line);

        public void createBatch(Imro_pm_meter_line mro_pm_meter_line);

        public Page<Imro_pm_meter_line> search(SearchContext context);

        public List<Imro_pm_meter_line> select();


}