package cn.ibizlab.odoo.client.odoo_mro.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imro_task_parts_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mro_task_parts_line] 服务对象客户端接口
 */
public interface Imro_task_parts_lineOdooClient {
    
        public Page<Imro_task_parts_line> search(SearchContext context);

        public void updateBatch(Imro_task_parts_line mro_task_parts_line);

        public void removeBatch(Imro_task_parts_line mro_task_parts_line);

        public void create(Imro_task_parts_line mro_task_parts_line);

        public void get(Imro_task_parts_line mro_task_parts_line);

        public void createBatch(Imro_task_parts_line mro_task_parts_line);

        public void remove(Imro_task_parts_line mro_task_parts_line);

        public void update(Imro_task_parts_line mro_task_parts_line);

        public List<Imro_task_parts_line> select();


}