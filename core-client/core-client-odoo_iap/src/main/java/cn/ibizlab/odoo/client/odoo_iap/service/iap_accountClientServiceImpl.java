package cn.ibizlab.odoo.client.odoo_iap.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iiap_account;
import cn.ibizlab.odoo.core.client.service.Iiap_accountClientService;
import cn.ibizlab.odoo.client.odoo_iap.model.iap_accountImpl;
import cn.ibizlab.odoo.client.odoo_iap.odooclient.Iiap_accountOdooClient;
import cn.ibizlab.odoo.client.odoo_iap.odooclient.impl.iap_accountOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[iap_account] 服务对象接口
 */
@Service
public class iap_accountClientServiceImpl implements Iiap_accountClientService {
    @Autowired
    private  Iiap_accountOdooClient  iap_accountOdooClient;

    public Iiap_account createModel() {		
		return new iap_accountImpl();
	}


        public void createBatch(List<Iiap_account> iap_accounts){
            
        }
        
        public void create(Iiap_account iap_account){
this.iap_accountOdooClient.create(iap_account) ;
        }
        
        public void removeBatch(List<Iiap_account> iap_accounts){
            
        }
        
        public void updateBatch(List<Iiap_account> iap_accounts){
            
        }
        
        public void remove(Iiap_account iap_account){
this.iap_accountOdooClient.remove(iap_account) ;
        }
        
        public void update(Iiap_account iap_account){
this.iap_accountOdooClient.update(iap_account) ;
        }
        
        public Page<Iiap_account> search(SearchContext context){
            return this.iap_accountOdooClient.search(context) ;
        }
        
        public void get(Iiap_account iap_account){
            this.iap_accountOdooClient.get(iap_account) ;
        }
        
        public Page<Iiap_account> select(SearchContext context){
            return null ;
        }
        

}

