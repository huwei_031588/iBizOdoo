package cn.ibizlab.odoo.client.odoo_iap.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.iap")
@Data
public class odoo_iapClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
