package cn.ibizlab.odoo.client.odoo_bus.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibus_presence;
import cn.ibizlab.odoo.core.client.service.Ibus_presenceClientService;
import cn.ibizlab.odoo.client.odoo_bus.model.bus_presenceImpl;
import cn.ibizlab.odoo.client.odoo_bus.odooclient.Ibus_presenceOdooClient;
import cn.ibizlab.odoo.client.odoo_bus.odooclient.impl.bus_presenceOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[bus_presence] 服务对象接口
 */
@Service
public class bus_presenceClientServiceImpl implements Ibus_presenceClientService {
    @Autowired
    private  Ibus_presenceOdooClient  bus_presenceOdooClient;

    public Ibus_presence createModel() {		
		return new bus_presenceImpl();
	}


        public void updateBatch(List<Ibus_presence> bus_presences){
            
        }
        
        public void get(Ibus_presence bus_presence){
            this.bus_presenceOdooClient.get(bus_presence) ;
        }
        
        public void create(Ibus_presence bus_presence){
this.bus_presenceOdooClient.create(bus_presence) ;
        }
        
        public void createBatch(List<Ibus_presence> bus_presences){
            
        }
        
        public void removeBatch(List<Ibus_presence> bus_presences){
            
        }
        
        public Page<Ibus_presence> search(SearchContext context){
            return this.bus_presenceOdooClient.search(context) ;
        }
        
        public void update(Ibus_presence bus_presence){
this.bus_presenceOdooClient.update(bus_presence) ;
        }
        
        public void remove(Ibus_presence bus_presence){
this.bus_presenceOdooClient.remove(bus_presence) ;
        }
        
        public Page<Ibus_presence> select(SearchContext context){
            return null ;
        }
        

}

