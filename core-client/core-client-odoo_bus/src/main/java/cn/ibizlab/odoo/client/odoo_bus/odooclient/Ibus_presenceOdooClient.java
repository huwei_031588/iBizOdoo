package cn.ibizlab.odoo.client.odoo_bus.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ibus_presence;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[bus_presence] 服务对象客户端接口
 */
public interface Ibus_presenceOdooClient {
    
        public void updateBatch(Ibus_presence bus_presence);

        public void get(Ibus_presence bus_presence);

        public void create(Ibus_presence bus_presence);

        public void createBatch(Ibus_presence bus_presence);

        public void removeBatch(Ibus_presence bus_presence);

        public Page<Ibus_presence> search(SearchContext context);

        public void update(Ibus_presence bus_presence);

        public void remove(Ibus_presence bus_presence);

        public List<Ibus_presence> select();


}