package cn.ibizlab.odoo.client.odoo_bus.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibus_bus;
import cn.ibizlab.odoo.core.client.service.Ibus_busClientService;
import cn.ibizlab.odoo.client.odoo_bus.model.bus_busImpl;
import cn.ibizlab.odoo.client.odoo_bus.odooclient.Ibus_busOdooClient;
import cn.ibizlab.odoo.client.odoo_bus.odooclient.impl.bus_busOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[bus_bus] 服务对象接口
 */
@Service
public class bus_busClientServiceImpl implements Ibus_busClientService {
    @Autowired
    private  Ibus_busOdooClient  bus_busOdooClient;

    public Ibus_bus createModel() {		
		return new bus_busImpl();
	}


        public void remove(Ibus_bus bus_bus){
this.bus_busOdooClient.remove(bus_bus) ;
        }
        
        public void update(Ibus_bus bus_bus){
this.bus_busOdooClient.update(bus_bus) ;
        }
        
        public Page<Ibus_bus> search(SearchContext context){
            return this.bus_busOdooClient.search(context) ;
        }
        
        public void create(Ibus_bus bus_bus){
this.bus_busOdooClient.create(bus_bus) ;
        }
        
        public void createBatch(List<Ibus_bus> bus_buses){
            
        }
        
        public void get(Ibus_bus bus_bus){
            this.bus_busOdooClient.get(bus_bus) ;
        }
        
        public void updateBatch(List<Ibus_bus> bus_buses){
            
        }
        
        public void removeBatch(List<Ibus_bus> bus_buses){
            
        }
        
        public Page<Ibus_bus> select(SearchContext context){
            return null ;
        }
        

}

