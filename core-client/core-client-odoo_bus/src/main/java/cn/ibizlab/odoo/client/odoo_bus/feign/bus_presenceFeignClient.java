package cn.ibizlab.odoo.client.odoo_bus.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibus_presence;
import cn.ibizlab.odoo.client.odoo_bus.model.bus_presenceImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[bus_presence] 服务对象接口
 */
public interface bus_presenceFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_bus/bus_presences/updatebatch")
    public bus_presenceImpl updateBatch(@RequestBody List<bus_presenceImpl> bus_presences);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_bus/bus_presences/{id}")
    public bus_presenceImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_bus/bus_presences")
    public bus_presenceImpl create(@RequestBody bus_presenceImpl bus_presence);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_bus/bus_presences/createbatch")
    public bus_presenceImpl createBatch(@RequestBody List<bus_presenceImpl> bus_presences);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_bus/bus_presences/removebatch")
    public bus_presenceImpl removeBatch(@RequestBody List<bus_presenceImpl> bus_presences);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_bus/bus_presences/search")
    public Page<bus_presenceImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_bus/bus_presences/{id}")
    public bus_presenceImpl update(@PathVariable("id") Integer id,@RequestBody bus_presenceImpl bus_presence);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_bus/bus_presences/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_bus/bus_presences/select")
    public Page<bus_presenceImpl> select();



}
