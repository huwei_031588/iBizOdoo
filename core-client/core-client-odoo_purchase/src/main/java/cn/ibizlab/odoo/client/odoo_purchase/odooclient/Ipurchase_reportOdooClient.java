package cn.ibizlab.odoo.client.odoo_purchase.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ipurchase_report;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[purchase_report] 服务对象客户端接口
 */
public interface Ipurchase_reportOdooClient {
    
        public void removeBatch(Ipurchase_report purchase_report);

        public void update(Ipurchase_report purchase_report);

        public void updateBatch(Ipurchase_report purchase_report);

        public void get(Ipurchase_report purchase_report);

        public void create(Ipurchase_report purchase_report);

        public void createBatch(Ipurchase_report purchase_report);

        public Page<Ipurchase_report> search(SearchContext context);

        public void remove(Ipurchase_report purchase_report);

        public List<Ipurchase_report> select();


}