package cn.ibizlab.odoo.client.odoo_purchase.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ipurchase_bill_union;
import cn.ibizlab.odoo.core.client.service.Ipurchase_bill_unionClientService;
import cn.ibizlab.odoo.client.odoo_purchase.model.purchase_bill_unionImpl;
import cn.ibizlab.odoo.client.odoo_purchase.odooclient.Ipurchase_bill_unionOdooClient;
import cn.ibizlab.odoo.client.odoo_purchase.odooclient.impl.purchase_bill_unionOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[purchase_bill_union] 服务对象接口
 */
@Service
public class purchase_bill_unionClientServiceImpl implements Ipurchase_bill_unionClientService {
    @Autowired
    private  Ipurchase_bill_unionOdooClient  purchase_bill_unionOdooClient;

    public Ipurchase_bill_union createModel() {		
		return new purchase_bill_unionImpl();
	}


        public void remove(Ipurchase_bill_union purchase_bill_union){
this.purchase_bill_unionOdooClient.remove(purchase_bill_union) ;
        }
        
        public void create(Ipurchase_bill_union purchase_bill_union){
this.purchase_bill_unionOdooClient.create(purchase_bill_union) ;
        }
        
        public Page<Ipurchase_bill_union> search(SearchContext context){
            return this.purchase_bill_unionOdooClient.search(context) ;
        }
        
        public void get(Ipurchase_bill_union purchase_bill_union){
            this.purchase_bill_unionOdooClient.get(purchase_bill_union) ;
        }
        
        public void createBatch(List<Ipurchase_bill_union> purchase_bill_unions){
            
        }
        
        public void removeBatch(List<Ipurchase_bill_union> purchase_bill_unions){
            
        }
        
        public void updateBatch(List<Ipurchase_bill_union> purchase_bill_unions){
            
        }
        
        public void update(Ipurchase_bill_union purchase_bill_union){
this.purchase_bill_unionOdooClient.update(purchase_bill_union) ;
        }
        
        public Page<Ipurchase_bill_union> select(SearchContext context){
            return null ;
        }
        

}

