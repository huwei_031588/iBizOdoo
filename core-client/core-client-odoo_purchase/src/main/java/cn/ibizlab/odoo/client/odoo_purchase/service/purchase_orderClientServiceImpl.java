package cn.ibizlab.odoo.client.odoo_purchase.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ipurchase_order;
import cn.ibizlab.odoo.core.client.service.Ipurchase_orderClientService;
import cn.ibizlab.odoo.client.odoo_purchase.model.purchase_orderImpl;
import cn.ibizlab.odoo.client.odoo_purchase.odooclient.Ipurchase_orderOdooClient;
import cn.ibizlab.odoo.client.odoo_purchase.odooclient.impl.purchase_orderOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[purchase_order] 服务对象接口
 */
@Service
public class purchase_orderClientServiceImpl implements Ipurchase_orderClientService {
    @Autowired
    private  Ipurchase_orderOdooClient  purchase_orderOdooClient;

    public Ipurchase_order createModel() {		
		return new purchase_orderImpl();
	}


        public void remove(Ipurchase_order purchase_order){
this.purchase_orderOdooClient.remove(purchase_order) ;
        }
        
        public Page<Ipurchase_order> search(SearchContext context){
            return this.purchase_orderOdooClient.search(context) ;
        }
        
        public void update(Ipurchase_order purchase_order){
this.purchase_orderOdooClient.update(purchase_order) ;
        }
        
        public void removeBatch(List<Ipurchase_order> purchase_orders){
            
        }
        
        public void createBatch(List<Ipurchase_order> purchase_orders){
            
        }
        
        public void updateBatch(List<Ipurchase_order> purchase_orders){
            
        }
        
        public void get(Ipurchase_order purchase_order){
            this.purchase_orderOdooClient.get(purchase_order) ;
        }
        
        public void create(Ipurchase_order purchase_order){
this.purchase_orderOdooClient.create(purchase_order) ;
        }
        
        public Page<Ipurchase_order> select(SearchContext context){
            return null ;
        }
        

}

