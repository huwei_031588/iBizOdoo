package cn.ibizlab.odoo.client.odoo_purchase.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ipurchase_order_line;
import cn.ibizlab.odoo.core.client.service.Ipurchase_order_lineClientService;
import cn.ibizlab.odoo.client.odoo_purchase.model.purchase_order_lineImpl;
import cn.ibizlab.odoo.client.odoo_purchase.odooclient.Ipurchase_order_lineOdooClient;
import cn.ibizlab.odoo.client.odoo_purchase.odooclient.impl.purchase_order_lineOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[purchase_order_line] 服务对象接口
 */
@Service
public class purchase_order_lineClientServiceImpl implements Ipurchase_order_lineClientService {
    @Autowired
    private  Ipurchase_order_lineOdooClient  purchase_order_lineOdooClient;

    public Ipurchase_order_line createModel() {		
		return new purchase_order_lineImpl();
	}


        public void createBatch(List<Ipurchase_order_line> purchase_order_lines){
            
        }
        
        public void updateBatch(List<Ipurchase_order_line> purchase_order_lines){
            
        }
        
        public void remove(Ipurchase_order_line purchase_order_line){
this.purchase_order_lineOdooClient.remove(purchase_order_line) ;
        }
        
        public void removeBatch(List<Ipurchase_order_line> purchase_order_lines){
            
        }
        
        public void get(Ipurchase_order_line purchase_order_line){
            this.purchase_order_lineOdooClient.get(purchase_order_line) ;
        }
        
        public void create(Ipurchase_order_line purchase_order_line){
this.purchase_order_lineOdooClient.create(purchase_order_line) ;
        }
        
        public Page<Ipurchase_order_line> search(SearchContext context){
            return this.purchase_order_lineOdooClient.search(context) ;
        }
        
        public void update(Ipurchase_order_line purchase_order_line){
this.purchase_order_lineOdooClient.update(purchase_order_line) ;
        }
        
        public Page<Ipurchase_order_line> select(SearchContext context){
            return null ;
        }
        

}

