package cn.ibizlab.odoo.client.odoo_purchase.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.purchase")
@Data
public class odoo_purchaseClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
