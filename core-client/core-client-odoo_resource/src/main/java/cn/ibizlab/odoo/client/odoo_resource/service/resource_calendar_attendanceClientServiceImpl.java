package cn.ibizlab.odoo.client.odoo_resource.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iresource_calendar_attendance;
import cn.ibizlab.odoo.core.client.service.Iresource_calendar_attendanceClientService;
import cn.ibizlab.odoo.client.odoo_resource.model.resource_calendar_attendanceImpl;
import cn.ibizlab.odoo.client.odoo_resource.odooclient.Iresource_calendar_attendanceOdooClient;
import cn.ibizlab.odoo.client.odoo_resource.odooclient.impl.resource_calendar_attendanceOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[resource_calendar_attendance] 服务对象接口
 */
@Service
public class resource_calendar_attendanceClientServiceImpl implements Iresource_calendar_attendanceClientService {
    @Autowired
    private  Iresource_calendar_attendanceOdooClient  resource_calendar_attendanceOdooClient;

    public Iresource_calendar_attendance createModel() {		
		return new resource_calendar_attendanceImpl();
	}


        public void removeBatch(List<Iresource_calendar_attendance> resource_calendar_attendances){
            
        }
        
        public void update(Iresource_calendar_attendance resource_calendar_attendance){
this.resource_calendar_attendanceOdooClient.update(resource_calendar_attendance) ;
        }
        
        public void get(Iresource_calendar_attendance resource_calendar_attendance){
            this.resource_calendar_attendanceOdooClient.get(resource_calendar_attendance) ;
        }
        
        public void remove(Iresource_calendar_attendance resource_calendar_attendance){
this.resource_calendar_attendanceOdooClient.remove(resource_calendar_attendance) ;
        }
        
        public void updateBatch(List<Iresource_calendar_attendance> resource_calendar_attendances){
            
        }
        
        public void create(Iresource_calendar_attendance resource_calendar_attendance){
this.resource_calendar_attendanceOdooClient.create(resource_calendar_attendance) ;
        }
        
        public Page<Iresource_calendar_attendance> search(SearchContext context){
            return this.resource_calendar_attendanceOdooClient.search(context) ;
        }
        
        public void createBatch(List<Iresource_calendar_attendance> resource_calendar_attendances){
            
        }
        
        public Page<Iresource_calendar_attendance> select(SearchContext context){
            return null ;
        }
        

}

