package cn.ibizlab.odoo.client.odoo_resource.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iresource_calendar_leaves;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[resource_calendar_leaves] 服务对象客户端接口
 */
public interface Iresource_calendar_leavesOdooClient {
    
        public void updateBatch(Iresource_calendar_leaves resource_calendar_leaves);

        public void update(Iresource_calendar_leaves resource_calendar_leaves);

        public void remove(Iresource_calendar_leaves resource_calendar_leaves);

        public void get(Iresource_calendar_leaves resource_calendar_leaves);

        public Page<Iresource_calendar_leaves> search(SearchContext context);

        public void create(Iresource_calendar_leaves resource_calendar_leaves);

        public void createBatch(Iresource_calendar_leaves resource_calendar_leaves);

        public void removeBatch(Iresource_calendar_leaves resource_calendar_leaves);

        public List<Iresource_calendar_leaves> select();


}