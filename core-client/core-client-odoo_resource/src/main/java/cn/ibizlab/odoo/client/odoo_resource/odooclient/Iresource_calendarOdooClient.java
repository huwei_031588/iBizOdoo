package cn.ibizlab.odoo.client.odoo_resource.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iresource_calendar;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[resource_calendar] 服务对象客户端接口
 */
public interface Iresource_calendarOdooClient {
    
        public void update(Iresource_calendar resource_calendar);

        public void removeBatch(Iresource_calendar resource_calendar);

        public void remove(Iresource_calendar resource_calendar);

        public void get(Iresource_calendar resource_calendar);

        public void createBatch(Iresource_calendar resource_calendar);

        public void updateBatch(Iresource_calendar resource_calendar);

        public void create(Iresource_calendar resource_calendar);

        public Page<Iresource_calendar> search(SearchContext context);

        public List<Iresource_calendar> select();


}