package cn.ibizlab.odoo.client.odoo_resource.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iresource_calendar_attendance;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[resource_calendar_attendance] 服务对象客户端接口
 */
public interface Iresource_calendar_attendanceOdooClient {
    
        public void removeBatch(Iresource_calendar_attendance resource_calendar_attendance);

        public void update(Iresource_calendar_attendance resource_calendar_attendance);

        public void get(Iresource_calendar_attendance resource_calendar_attendance);

        public void remove(Iresource_calendar_attendance resource_calendar_attendance);

        public void updateBatch(Iresource_calendar_attendance resource_calendar_attendance);

        public void create(Iresource_calendar_attendance resource_calendar_attendance);

        public Page<Iresource_calendar_attendance> search(SearchContext context);

        public void createBatch(Iresource_calendar_attendance resource_calendar_attendance);

        public List<Iresource_calendar_attendance> select();


}