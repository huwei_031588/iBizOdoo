package cn.ibizlab.odoo.client.odoo_resource.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iresource_test;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[resource_test] 服务对象客户端接口
 */
public interface Iresource_testOdooClient {
    
        public void remove(Iresource_test resource_test);

        public void createBatch(Iresource_test resource_test);

        public void create(Iresource_test resource_test);

        public void update(Iresource_test resource_test);

        public void get(Iresource_test resource_test);

        public void removeBatch(Iresource_test resource_test);

        public Page<Iresource_test> search(SearchContext context);

        public void updateBatch(Iresource_test resource_test);

        public List<Iresource_test> select();


}