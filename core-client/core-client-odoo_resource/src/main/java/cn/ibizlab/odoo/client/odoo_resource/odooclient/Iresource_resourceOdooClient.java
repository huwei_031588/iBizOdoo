package cn.ibizlab.odoo.client.odoo_resource.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iresource_resource;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[resource_resource] 服务对象客户端接口
 */
public interface Iresource_resourceOdooClient {
    
        public void updateBatch(Iresource_resource resource_resource);

        public void remove(Iresource_resource resource_resource);

        public void removeBatch(Iresource_resource resource_resource);

        public void get(Iresource_resource resource_resource);

        public void createBatch(Iresource_resource resource_resource);

        public Page<Iresource_resource> search(SearchContext context);

        public void update(Iresource_resource resource_resource);

        public void create(Iresource_resource resource_resource);

        public List<Iresource_resource> select();


}