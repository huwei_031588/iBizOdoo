package cn.ibizlab.odoo.client.odoo_resource.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iresource_mixin;
import cn.ibizlab.odoo.core.client.service.Iresource_mixinClientService;
import cn.ibizlab.odoo.client.odoo_resource.model.resource_mixinImpl;
import cn.ibizlab.odoo.client.odoo_resource.odooclient.Iresource_mixinOdooClient;
import cn.ibizlab.odoo.client.odoo_resource.odooclient.impl.resource_mixinOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[resource_mixin] 服务对象接口
 */
@Service
public class resource_mixinClientServiceImpl implements Iresource_mixinClientService {
    @Autowired
    private  Iresource_mixinOdooClient  resource_mixinOdooClient;

    public Iresource_mixin createModel() {		
		return new resource_mixinImpl();
	}


        public void create(Iresource_mixin resource_mixin){
this.resource_mixinOdooClient.create(resource_mixin) ;
        }
        
        public void createBatch(List<Iresource_mixin> resource_mixins){
            
        }
        
        public void remove(Iresource_mixin resource_mixin){
this.resource_mixinOdooClient.remove(resource_mixin) ;
        }
        
        public void removeBatch(List<Iresource_mixin> resource_mixins){
            
        }
        
        public Page<Iresource_mixin> search(SearchContext context){
            return this.resource_mixinOdooClient.search(context) ;
        }
        
        public void get(Iresource_mixin resource_mixin){
            this.resource_mixinOdooClient.get(resource_mixin) ;
        }
        
        public void update(Iresource_mixin resource_mixin){
this.resource_mixinOdooClient.update(resource_mixin) ;
        }
        
        public void updateBatch(List<Iresource_mixin> resource_mixins){
            
        }
        
        public Page<Iresource_mixin> select(SearchContext context){
            return null ;
        }
        

}

