package com.sinosig.activiti.resource;

import com.alibaba.fastjson.JSONObject;
import com.sinosig.activiti.service.IProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 当前运行流程信息服务对象
 */
@RestController
@RequestMapping("/process")
public class ProcessResource{

	@Autowired
	IProcessService processService;

	/**
	 * 部署工作流
	 * @param deployFilePath
	 * @return
	 */
	@RequestMapping(value = "/deploy")
	public String deployWF(@RequestParam("deployfilepath") String deployFilePath) {

		String deployId;
		if (StringUtils.isEmpty(deployFilePath)) {
			return "deploy error";
		}
		deployId=processService.deployWF(deployFilePath);

		return deployId;
	}

	/**
	 * 取消已经部署的工作流
	 * @param deployId
	 * @return
	 */
	@RequestMapping("/deDeploy/{deployId}")
	public String deDeployWF(@PathVariable("deployId") String deployId) {

		if (StringUtils.isEmpty(deployId)) {
			return "unDeploy error";
		}
		processService.deDeployWF(deployId);
		return "unDeploy success";
	}

	/**
	 * 启动工作流-创建流程实例
	 * @param params  processId:工作流标识  、variables：流程参数
	 * @return
	 */
	@RequestMapping(value = "/start")
	public String startWF(@RequestBody JSONObject params) {

		String processId=params.getString("processid");

		Map<String, Object> variables=params.getJSONObject("variables");

		if (StringUtils.isEmpty(processId)) {
			return "processId error";
		}
		processService.startWF(processId,variables);
		return "success";
	}

	/**
	 * 删除工作流-删除流程实例
	 * @param instanceId
	 * @return
	 */
	@RequestMapping(value = "/delete/{instanceId}")
	public String deleteWF(@PathVariable("instanceId") String instanceId) {

		if (StringUtils.isEmpty(instanceId)) {
			return "instanceId error";
		}
		processService.deleteWF(instanceId);
		return "success";
	}

}
