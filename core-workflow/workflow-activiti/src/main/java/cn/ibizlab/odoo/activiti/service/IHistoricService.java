package cn.ibizlab.odoo.activiti.service;

import org.activiti.engine.history.HistoricActivityInstance;

import java.util.List;

public interface IHistoricService {

    /**
     * 查询流程历史步骤
     * @param instanceId
     * @return
     */
    List<HistoricActivityInstance> historicWFStep(String instanceId);

}
