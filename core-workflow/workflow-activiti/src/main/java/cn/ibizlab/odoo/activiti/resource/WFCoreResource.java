package cn.ibizlab.odoo.activiti.resource;

import cn.ibizlab.odoo.activiti.entity.TaskInfo;
import cn.ibizlab.odoo.activiti.service.IWFCoreService;
import com.alibaba.fastjson.JSONObject;
import org.activiti.engine.history.HistoricActivityInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class WFCoreResource {

    @Autowired
    IWFCoreService wfCoreService;

    /**
     * 工作流启动
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfstart")
    public String wfStart(@RequestBody JSONObject et){
        return wfCoreService.wfStart(et);
    }

    /**
     * 工作流提交处理
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfsubmit")
    public String wfSubmit(@RequestBody JSONObject et){
        return wfCoreService.wfSubmit(et);
    }

    /**
     * 工作流关闭
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfclose")
    public void wfClose(@RequestBody JSONObject et){
        wfCoreService.wfClose(et);
    }

    /**
     * 工作流跳转
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfgoto")
    public void wfGoto(@RequestBody JSONObject et){
        wfCoreService.wfGoto(et);
    }

    /**
     * 工作流重新启动
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfrestart")
    public void wfRestart(@RequestBody JSONObject et){
        wfCoreService.wfRestart(et);
    }

    /**
     * 工作流撤回
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfrollback")
    public void wfRollback(@RequestBody JSONObject et){
        wfCoreService.wfRollback(et);
    }

    /**
     * 工作流退回
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfsendback")
    public void wfSendBack(@RequestBody JSONObject et){
        wfCoreService.wfSendBack(et);
    }

    /**
     * 工作流重新分配（处理人）
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfreassign")
    public void wfReassign(@RequestBody JSONObject et){
        wfCoreService.wfReassign(et);
    }

    /**
     * 工作流标记为已读
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfmarkread")
    public void wfMarkRead(@RequestBody JSONObject et){
        wfCoreService.wfMarkRead(et);
    }

    /**
     * 工作流部署
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfdeploy")
    public String wfDeploy(@RequestBody JSONObject et){
        return wfCoreService.wfDeploy(et);
    }

    /**
     * 获取流程当前步骤
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfcurstep")
    public TaskInfo getWFCurStep(@RequestBody JSONObject et){
        return wfCoreService.getWFCurStep(et);
    }

    /**
     * 获取流程历史步骤
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfhisstep")
    public List<HistoricActivityInstance> getWFHisStep(@RequestBody JSONObject et){
        return wfCoreService.getWFHisStep(et);
    }
}
