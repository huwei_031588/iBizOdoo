package cn.ibizlab.odoo.activiti.entity;


import lombok.Data;

import java.util.Date;

@Data
public class TaskInfo {

    protected String id;
    protected String name;
    protected String assignee;
    protected Date createTime;
    protected String processInstanceId;
    protected String executionId;
    protected String processDefinitionId;

}
