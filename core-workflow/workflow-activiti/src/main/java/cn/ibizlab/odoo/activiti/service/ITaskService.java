package cn.ibizlab.odoo.activiti.service;


import cn.ibizlab.odoo.activiti.entity.TaskInfo;
import java.util.Map;

public interface ITaskService {

    /**
     * 提交流程
     * @param taskId
     * @param variables
     * @return
     */
    String complete(String taskId, Map variables);

    /**
     * 查询流程当前任务
     * @param processInstanceId
     * @return
     */
    TaskInfo getCurentTask(String processInstanceId);

}
